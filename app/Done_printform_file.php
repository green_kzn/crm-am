<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Done_printform_file extends Model
{
    protected $fillable = [
        'file', 'project_print_form_id', 'g_file', 'download_url', 'public_url'
    ];
}
