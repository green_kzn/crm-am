<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project_print_form extends Model
{
    protected $fillable = [
        'raw',
        'attendant',
        'number',
        'project_id',
        'project_task_id',
        'user_id',
        'observer_id',
        'name',
        'category_printform_ref_id',
        'max_hours',
        'is_printform',
        'is_screenform',
        'free',
        'paid',
        'module_rule_id',
        'module_id',
        'additional_id',
        'printform_status_ref_id',
        'norm_contacts',
        'number_contacts',
        'date_implement',
        'real_date',
        'date_implement_for_client',
    ];
}
