<?php

namespace App;


use Carbon\Carbon;

class ProjectWaitingList extends BaseModel {
    protected $table = 'project_waiting_list';

    protected $fillable = ['project_id', 'waiting_list_id', 'info', 'redmine_task_id', 'is_solved', 'solved_at'];

    function project() {
        return $this->belongsTo(Project::class);
    }

    function waitingList() {
        return $this->belongsTo(WaitingList::class);
    }

    public function getSolvedAtAttribute($value) {
        return $value ? Carbon::parse($value)->format('d.m.Y H:i') : '';
    }

}
