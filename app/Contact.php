<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //
    protected $fillable = [
        'first_name',
        'last_name',
        'patronymic',
        'post_id',
        'phone',
        'email',
        'client_id',
        'user_id'
    ];
}
