<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Printform_comment extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id', 'form_id', 'comment'];
}
