<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment_task extends Model
{
    protected $fillable = [
        'project_id',
        'user_id',
        'task_id',
        'date',
        'time',
        'text',
    ];
}
