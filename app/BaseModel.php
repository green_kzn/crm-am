<?php


namespace App;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model {

    function scopeWithAll($query){}

    public function getCreatedAtAttribute($value) {
        return Carbon::parse($value)->format('d.m.Y H:i');
    }

}