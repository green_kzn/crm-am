<?php

namespace App\Listeners;

use App\Events\HelloPusherEvent;
use App\Notifications\ProjectTask;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class HelloPusherEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  HelloPusherEvent  $event
     * @return void
     */
    public function handle(HelloPusherEvent $event)
    {
        //dd($event);
        Log::info('Project save', ['project' => $event]);
        //$event->user->notify(new ProjectTask($event));
    }
}
