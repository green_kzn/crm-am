<?php

namespace App\Listeners;

use App\Events\onAddProjectEvent;
use App\Notifications\ProjectTask;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class AddProjectListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  onAddProjectEvent  $event
     * @return void
     */
    public function handle(onAddProjectEvent $event)
    {
        //dd($event->user);
        Log::info('Project save', ['project' => 'Ok']);
        //$event->user->notify(new ProjectTask($event));
    }
}
