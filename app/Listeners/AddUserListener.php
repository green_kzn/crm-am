<?php

namespace App\Listeners;

use App\Events\onAddNewUser;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\History;

class AddUserListener
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  onAddNewUser $event
     * @return void
     */
    public function handle(onAddNewUser $event)
    {
        History::create([
            'message' => $event->cur_user->first_name.' '.$event->cur_user->last_name.' добавил нового пользователя'
        ]);
        //Log::info('Project save', ['message' => $event->cur_user->first_name.' '.$event->cur_user->last_name.' добавил нового пользователя']);
    }
}
