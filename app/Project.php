<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'start_date', 'finish_date', 'category_id'];

    function projectCategory() {
        return $this->belongsTo(ProjectCategory::class,'category_id');
    }

    function waitingLists() {
        return $this->belongsToMany(WaitingList::class)
            ->withPivot('info', 'redmine_task_id', 'is_solved', 'solved_at');
    }

    function projectWaitingLists() {
        return $this->hasMany(ProjectWaitingList::class);
    }
}
