<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clarifying_answer extends Model
{
    protected $fillable = [
        'answer',
        'question_id',
        'project_id'
    ];
}
