<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module_rule extends Model
{
    protected $fillable = ['module_id', 'task', 'grouptasks_id', 'additional', 'number_contacts'];
}
