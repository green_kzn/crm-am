<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Cartalyst\Sentinel\Users\EloquentUser;
use Cache;

class User extends EloquentUser
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'last_name',
        'first_name',
        'permissions',
        'is_ban',
        'foto'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function byEmail($email) {
        return static::whereEmail($email)->first();
    }

    public function isOnline() {
        dd(Cache::has('user-is-online-' . $this->id));
        return Cache::has('user-is-online-' . $this->id);
    }


    public function role(){
        return $this->belongsToMany(Role::class, 'role_users');
    }

    /**
     * @return User[]|\Illuminate\Database\Eloquent\Collection
     */
    static public function getImplementers() {
        return static::whereNull('users.deleted_at')
            ->select('users.*')
            ->leftJoin('role_users', 'role_users.user_id', '=',  'users.id')
            ->where('users.id', '!=', 0)
            ->where('role_users.role_id', 4)
            ->get();
    }
}
