<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sentinel;

class Role_user extends Model
{
    protected $table = 'role_users';

    public static function getByUserId($id) {
        $roleId = static::whereUserId($id)->first();
        $role = Sentinel::findRoleById($roleId->role_id)->first()->name;
        //dd($role);

        return $role;
    }
}
