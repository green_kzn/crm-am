<?php


namespace App;


use Carbon\Carbon;

class WaitingList extends BaseModel {

    protected $fillable = ['name', 'waiting_list_type_id', 'has_redmine', 'has_info'];


    function waitingListType(){
        return $this->belongsTo(WaitingListType::class);
    }

    function solved(){
        $this->update(['solved_at' => Carbon::now()]);
    }

    public function scopeWithAll($query)
    {
        $query->with('waitingListType');
    }

}