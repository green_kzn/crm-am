<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;

class CheckRoles
{
    public function handle($request, Closure $next, $roles)
    {
        if (preg_match('/\|/', $roles))
        {
            $roles = explode('|', $roles);
        }
        else
        {
            $roles = [];
            $roles[] = $roles;
        }
        foreach ($roles as $role)
        {
            try
            {
                if (Sentinel::check() && Sentinel::inRole($role))
                {
                    return $next($request);
                }
            }
            catch (ModelNotFoundException $exception)
            {
                dd('Could not find role ' . $role);
            }
        }
        return redirect()->back()->with(['message' => 'У вас недостаточно прав для просмотра этой страницы']);
    }
}