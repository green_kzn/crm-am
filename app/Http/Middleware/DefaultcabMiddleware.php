<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
use Activation;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use App\User;

class DefaultcabMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        try {
            if (Sentinel::check() &&
                Sentinel::getUser()->roles()->first()->slug != 'admin' &&
                Sentinel::getUser()->roles()->first()->slug != 'manager' &&
                Sentinel::getUser()->roles()->first()->slug != 'printform' &&
                Sentinel::getUser()->roles()->first()->slug != 'implementer' &&
                Sentinel::getUser()->is_ban == 1 &&
                Sentinel::getUser()->deleted_at == null) {

                $cur_user = Sentinel::getUser();
                $request->session()->put('user', $cur_user);
                $role = Sentinel::getUser()->roles()->first();
                $request->session()->put('role', $role->name);
                $request->session()->put('online', '1');

                $perm = Sentinel::findRoleById($role->id);
                //dd($role->id);
                $request->session()->put('perm', $perm->permissions);

                return $next($request);
            } else {
                return abort(503);
            }
        } catch (NotActivatedException $e) {
            Sentinel::logout();
            return redirect('login');
        }
    }
}
