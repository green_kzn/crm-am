<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
use Activation;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use App\User;
use App\Project_task;
use App\Project_print_form;

class Print_formMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            if (Sentinel::check()) {
                if (Sentinel::getUser()->roles()->first()->slug == 'printform' &&
                    Sentinel::getUser()->is_ban == 1 &&
                    Sentinel::getUser()->deleted_at == null) {

                    $cur_user = Sentinel::getUser();
                    $request->session()->put('user', $cur_user);
                    $role = Sentinel::getUser()->roles()->first();
                    $request->session()->put('role', $role->name);
                    $request->session()->put('online', '1');

                    $perm = Sentinel::findRoleById($role->id);

                    $request->session()->put('perm', $perm->permissions);

                    $raw_forms = Project_print_form::where('raw', '=', 1)
                        ->orWhere('printform_status_ref_id', '=', 9)
                        ->whereNull('deleted_at')
                        ->count();

                    $request->session()->put('raw_forms', $raw_forms);

                    return $next($request);
                } else {
                    return abort(503);
                }
            } else {
                Sentinel::logout();
                return redirect('login');
            }
        } catch (NotActivatedException $e) {
            Sentinel::logout();
            return redirect('login');
        }
    }
}
