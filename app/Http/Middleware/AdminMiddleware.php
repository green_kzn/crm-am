<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
use Activation;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use App\User;
use App\Module_rule;
use App\Project;
use App\Project_print_form;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            if (Sentinel::check()) {
                if (Sentinel::getUser()->roles()->first()->slug == 'admin' &&
                    Sentinel::getUser()->is_ban == 1 &&
                    Sentinel::getUser()->deleted_at == null) {

                    $cur_user = Sentinel::getUser();
                    $request->session()->put('user', $cur_user);
                    $u = User::find($cur_user->id);
                    $u_notifications = $u->unreadNotifications;
                    if (count($u_notifications) > 0) {
                        foreach ($u_notifications as $un) {
                            $project = $un->data['project'];

                            $project_n[] = [
                                'name' => $project['name'],
                                'project' => $project
                            ];
                        }
                    } else {
                        $project_n[] = '';
                    }

                    $notifications_ar = [
                        'count' => count($u_notifications),
                        'notifications' => $project_n
                    ];
                    $request->session()->put('notifacations', $notifications_ar);
                    $role = Sentinel::getUser()->roles()->first();
                    $request->session()->put('role', $role->name);
                    $request->session()->put('online', '1');

                    $perm = Sentinel::findRoleById($role->id);

                    $request->session()->put('perm', $perm->permissions);

                    $raw_forms = Project_print_form::whereNull('deleted_at')
                        ->where('observer_id', '=', $cur_user->id)
                        ->where('printform_status_ref_id', '=', 8)
                        ->count();

                    $request->session()->put('raw_forms', $raw_forms);

                    return $next($request);
                } else {
                    Sentinel::logout();
                    return redirect()->back()->with(['error' => 'Доступ заблокирован']);
                }
            } else {
                Sentinel::logout();
                return redirect('login');
            }
        } catch (NotActivatedException $e) {
            Sentinel::logout();
            return redirect('login');
        }
    }
}
