<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
use Activation;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use App\User;
use App\Project_task;
use App\Project;

class ManagerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            if (Sentinel::check()) {
                if (Sentinel::getUser()->roles()->first()->slug == 'manager' &&
                    Sentinel::getUser()->is_ban == 1 &&
                    Sentinel::getUser()->deleted_at == null) {

                    $cur_user = Sentinel::getUser();
                    $request->session()->put('user', $cur_user);
                    $role = Sentinel::getUser()->roles()->first();
                    $request->session()->put('role', $role->name);
                    $request->session()->put('online', '1');

                    $perm = Sentinel::findRoleById($role->id);

                    $raw_projects = Project::where('raw', '=', 1)
                        ->whereNull('deleted_at')
                        ->count();

                    $request->session()->put('perm', $perm->permissions);
                    $request->session()->put('raw_projects', $raw_projects);
                    
                    return $next($request);
                } else {
                    return abort(503);
                }
            } else {
                Sentinel::logout();
                return redirect('login');
            }
        } catch (NotActivatedException $e) {
            Sentinel::logout();
            return redirect('login');
        }
    }
}
