<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use Sentinel;
use Cache;

class UserActivity
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Sentinel::check()) {
            $expiresAt = Carbon::now()->addMinutes(5);
            Cache::put('user-is-online-' . Sentinel::getUser()->id, true, $expiresAt);
        }
        return $next($request);
    }
}