<?php

namespace App\Http\Controllers;

use Yandex;
use Flysystem;
use Filesystem;

class YandexFileAdapter {
    private $client;
    private $filesystem;

    public function __construct() {
        $this->client = new Yandex('AgAAAABBAu7rAAZa-DHY6ktZ0EC8kIIaT5XWq8w');
        $adapter = new Flysystem($this->client);

        $this->filesystem = new Filesystem($adapter);
    }

    function put($filename, $content) {
        $upload = $this->filesystem->put($filename, $content);
        if ($upload){
            $f = $this->getFile($filename);
            return array(
                'public_url' => $f['public_url'],
                'download_url' => $f['file'],
            );
        }
        return false;
    }

    function getFile($filename) {
        $resource = $this->client->getResource($filename);
        if ($resource->has()) {
            $resource->setPublish(true);
            return ($resource->toArray());
        }
        return '';
    }

    function delete ($filename) {
        $resource = $this->client->getResource($filename);
        return $resource->delete(true);
    }

}