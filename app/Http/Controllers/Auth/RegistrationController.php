<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use Activation;
use App\User;
use Mail;

class RegistrationController extends Controller
{
    public function register() {
        return view('auth.register');
    }

    public function postRegister(Request $request) {
        $email = User::byEmail($request->email);
        if (!$email) {
            $user = Sentinel::register($request->all());
            $new_user = User::where('id', '=', $user->id)->firstOrFail();
            $new_user->first_name = $request->first_name;
            $new_user->last_name = $request->last_name;
            $new_user->foto = 'camera_200.png';
            $new_user->is_ban = 1;
            $new_user->save();
            $activation = Activation::create($user);
            $role = Sentinel::findRoleBySlug('manager');
            $role->users()->attach($user);
            $this->sendMail($user, $activation->code);

            return redirect('/');
        } else {
            return redirect()->back()->with([
                'error' => 'Пользователь с таким email уже зарегестрирован'
            ]);
        }
    }

    private function sendMail($user, $code) {
        Mail::send('emails.activation', [
            'user' => $user,
            'code' => $code
        ], function ($message) use ($user){
            $message->to($user->email);
            $message->subject("Активация аккаунта на сайте ".config('app.name'));
        });
    }
}
