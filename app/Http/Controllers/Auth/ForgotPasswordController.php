<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\User;
use Reminder;
use Mail;
use Sentinel;
use NotFoundHttpException;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function forgotPassword() {
        return view('auth.forgot-password');
    }

    public function postForgotPassword(Request $request) {
        $user = User::whereEmail($request->email)->first();

        if ($user) {
            if (count($user) == 0)
                return redirect()->back()->with([
                    'success' => 'Код для восстановления пароля отправлен на ваш email'
                ]);

            $reminder = Reminder::exists($user) ?: Reminder::create($user);

            $this->sendEmail($user, $reminder->code);

            return redirect()->back()->with([
                'success' => 'Код для восстановления пароля отправлен на ваш email'
            ]);
        } else {
            return redirect()->back()->with([
                'error' => 'Email не найден'
            ]);
        }
    }

    public function resetPassword($email, $resetCode) {
        $user = User::byEmail($email);
        if (count($user) == 0)
            abort(404);
        if ($reminder = Reminder::exists($user)) {
            if ($resetCode == $reminder->code)
                return view('auth.reset-password');
            else
                return redirect('/');
        } else {
            return redirect('/login')->with('success', 'Ваш аккаунт уже активирован');
        }
    }

    public function postResetPassword(Request $request, $email, $resetCode) {
        $this->validate($request, [
            'password' => 'confirmed|required|min:5|max:10',
            'password_confirmation' => 'required|min:5|max:10',
        ]);

        $user = User::byEmail($email);
        if (count($user) == 0)
            abort(404);
        if ($reminder = Reminder::exists($user)) {
            if ($resetCode == $reminder->code) {
                Reminder::complete($user, $resetCode, $request->password);

                return redirect('/login')->with('success', 'Ваш пароль успешно изменен');
            } else
                return redirect('/');
        } else {
            return redirect('/');
        }
    }

    private function sendEmail($user, $code) {
        Mail::send('emails.forgot-password', [
            'user' => $user,
            'code' => $code
        ], function ($message) use ($user) {
            $message->to($user->email);

            $message->subject('Восстановление пароля');
        });
    }
}
