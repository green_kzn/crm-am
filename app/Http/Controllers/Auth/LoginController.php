<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Psy\Exception\ThrowUpException;
use Sentinel;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Reminder;

class LoginController extends Controller
{
    public function login() {
        return view('auth.login');
    }

    public function postLogin(Request $request) {

        try {
            $rememberMe = false;

            if (isset($request->remember_me))
                $rememberMe = true;
            $auth = Sentinel::authenticate($request->all(), $rememberMe);
            
            if ($auth) {
                $slug = Sentinel::getUser()->roles()->first()->slug;

                switch ($slug) {
                    case 'admin':
                        return redirect('/admin');
                        break;
                    case 'manager':
                        return redirect('/manager');
                        break;
                    case 'printform':
                        return redirect('/print_form');
                        break;
                    case 'implementer':
                        return redirect('/implementer');
                        break;
                    case 'dir':
                        return redirect('/dir');
                    case 'tp':
                        return redirect('/tp');
                        break;
                    default:
                        return redirect('/defaultcab');
                }
            } else {
                return redirect()->back()->with(['error' => 'Ошибка авторизации']);
            }
        } catch(ThrottlingException $e) {
            $delay = $e->getDelay();
            return redirect()->back()->with(['error' => 'Превышено число попыток входа. Попробуйте войти через '.$delay.' секунд']);
        } catch(NotActivatedException $e) {
            return redirect()->back()->with(['error' => 'Ваш аккаунт еще не активирован']);
        } catch(TokenMismatchException $e) {
            return redirect()->back()->with(['error' => 'Ваш аккаунт еще не активирован']);
        }
    }

    public function logout() {
        Sentinel::logout();

        return redirect('/login');
    }
}
