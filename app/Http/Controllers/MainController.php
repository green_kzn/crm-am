<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use App\User;
use App\Project;
use App\Comment;
use App\Comment_task;
use App\Clients_reference;
use App\Project_task;
use App\Module_rule;
use App\Clients;
use App\Citys;
use App\Task;
use App\Event as Event2;
use App\Project_task_contact;
use App\City_reference;
use App\Project_module;
use App\Modules_reference;
use App\Grouptasks_reference;
use App\Events\addImpEvent;
use Event;
use DB;
use Carbon\Carbon;

class MainController extends Controller
{
    public function index() {
        if (Sentinel::check()) {
            $role = Sentinel::getUser()->roles()->first()->slug;
            switch ($role) {
                case 'admin':
                    return redirect('/admin');
                    break;
                case 'manager':
                    return redirect('/manager');
                    break;
                case 'printform':
                    return redirect('/print_form');
                    break;
                case 'implementer':
                    return redirect('/implementer');
                    break;
                case 'dir':
                    return redirect('/dir');
                    break;
                case 'tp':
                    return redirect('/tp');
                    break;
                default:
                    return redirect('/defaultcab');
            }
        } else {
            return redirect('/login');
        }
    }

    public function CalendarRedirect($user_id = null, $date = null) {
        $role = Sentinel::getUser()->roles()->first()->slug;
        switch ($role) {
            case 'admin':
                return redirect('/admin/calendar');
                break;
            case 'manager':
                return redirect('/manager/calendar');
                break;
            case 'printform':
                return redirect('/print_form/calendar');
                break;
            case 'implementer':
                return redirect('/implementer/calendar/'.$user_id.'/'.$date);
                break;
            default:
                return redirect('/defaultcab/calendar');
        }
    }

    public function getClients(Request $request) {
        $clients = Clients_reference::whereNull('clients_references.deleted_at')
            ->leftJoin('city_references', 'clients_references.city_id', '=', 'city_references.id')
            ->leftJoin('projects', 'projects.client_id', '=', 'clients_references.id')
            ->select(['clients_references.id as id', 'clients_references.name as client_name', 'city_references.name as city_name'])
            ->where('projects.done', 0)
            ->whereNull('projects.raw') 
            ->whereNull('projects.deleted_at')
            ->distinct()
            ->get();

        foreach ($clients as $c) {
            $clients_ar[] = [
                'code' => $c['id'],
                'label' => $c['client_name'].' ('.$c['city_name'].')'
            ];
        }
        return response()->json($clients_ar);
    }

    public function getClientProject($id) {
        // dd($id);
        // $projects = Project::where('projects.client_id', '=', $id)
        //     ->whereNULL('raw')
        //     ->where('done', '=', 0)
        //     ->leftJoin('clients_references', 'projects.client_id', '=', 'clients_references.id')
        //     ->leftJoin('city_references', 'clients_references.city_id', '=', 'city_references.id')
        //     ->select(['projects.name as p_name', 'projects.id as p_id', 'clients_references.name as client_name', 'city_references.name as city_name'])
        //     ->get();

        $projects = DB::select("select `p`.`name` as `p_name`, `p`.`id` as `p_id`, `cr`.`name` as `client_name`,
            `city`.`name` as `city_name`
            from `projects` `p`
            left join `clients_references` `cr` on `p`.`client_id` = `cr`.`id`
            left join `city_references` `city` on `city`.`id` = `cr`.`city_id`
            where
            `p`.`deleted_at` is null and
            `p`.`raw` is null and
            `p`.`done` = 0 and
            `p`.`client_id` = ".$id);
            
        
        if (count($projects) != 0) {
            foreach ($projects as $c) {
                // dd($c->p_id);
                $projects_ar[] = [
                    'value' => $c->p_id,
                    'label' => $c->p_name.' ('.$c->client_name.', '.$c->city_name.')'
                ];
            }
        } else {
            $projects_ar[] = [];
        };

        return response()->json($projects_ar);
    }

    public function getClientById($id) {
        $client = Clients_reference::where('id', '=', $id)
            ->whereNull('deleted_at')->firstOrFail();
        return $client;
    }

    public function getCityById($id) {
        $city = City_reference::where('id', '=', $id)
            ->whereNull('deleted_at')
            ->firstOrFail();

        return $city->name;
    }

    public function getProjectTask(Request $request, $id) {
        $project_tasks = Project_task::where('project_id', '=', $id)
            ->where('tasksstatus_ref_id', '!=', 2)
            ->whereNull('deleted_at')
            ->whereNotNull('module_rule_id')
            ->get();

    //    print_r($project_tasks);

        if (count((array)$project_tasks) > 0) {
            foreach ($project_tasks as $pt) {
                $name = Module_rule::find($pt->module_rule_id);
                $p_name = Project::find($pt->project_id);
                $p_client = self::getClientById($p_name['client_id']);
                $p_city = self::getCityById($p_client->city_id);
                $ptc = Project_task_contact::where('project_task_id', '=', $pt->id)
                    ->whereNull('deleted_at')
                    ->first();
                
                $user = User::whereNull('deleted_at')->get();
                foreach ($user as $u) {
                    if ($u->id == $p_name->user_id) {
                        $observer[] = [
                            'default' => 1,
                            'id' => $u->id,
                            'first_name' => $u->first_name,
                            'last_name' => $u->last_name
                        ];
                    } else {
                        $observer[] = [
                            'default' => 0,
                            'id' => $u->id,
                            'first_name' => $u->first_name,
                            'last_name' => $u->last_name
                        ];
                    }
                }
                // dd(count($ptc));

                if (count((array)$ptc) > 0 && $request->get('pt_id') != $pt->id) {
//                    if ($pt['prev_contact'] != null) {
//                        $p_tasks[] = [
//                            'id' => $pt['id'],
//                            'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
//                            'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
//                            'client' => $p_client->name,
//                            'name' => $name->task,
//                            'observer' => $observer,
//                            'p_name' => $p_name['name'],
//                            'city' => $p_city,
//                            'next_contact' => $ptc['next_contact'],
//                            'prev_contact' => $pt['prev_contact'],
//                        ];
//                    } else {
//                        $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
//                            ->whereNull('deleted_at')
//                            //->whereNull('status')
//                            ->max('prev_contact');
//
//                        $p_tasks[] = [
//                            'id' => $pt['id'],
//                            'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
//                            'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
//                            'client' => $p_client->name,
//                            'name' => $name->task,
//                            'observer' => $observer,
//                            'p_name' => $p_name['name'],
//                            'city' => $p_city,
//                            'next_contact' => $ptc['next_contact'],
//                            'prev_contact' => $max_date,
//                        ];
//                    }
                } else {
                    if ($pt['prev_contact'] != null) {
                        $p_tasks[] = [
                            'id' => $pt['id'],
                            'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                            'client' => $p_client->name,
                            'name' => $name->task,
//                            'observer' => $observer,
                            'p_name' => $p_name['name'],
                            'city' => $p_city,
                            'next_contact' => null,
                            'prev_contact' => $pt['prev_contact'],
                        ];
                    } else {
                        $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                            ->whereNull('deleted_at')
                            //->whereNull('status')
                            ->max('prev_contact');

                        $p_tasks[] = [
                            'id' => $pt['id'],
                            'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                            'client' => $p_client->name,
                            'name' => $name->task,
//                            'observer' => $observer,
                            'p_name' => $p_name['name'],
                            'city' => $p_city,
                            'next_contact' => null,
                            'prev_contact' => $max_date,
                        ];
                    }
                }
            }
        }

        return response()->json($p_tasks);
    }

    public function addTaskFromCalendar(Request $request) {
        $data = json_decode($request->data);
        
        // dd($data);
        $date =  explode(' ', date("Y-m-d H:i:s"));
        $cur_user = Sentinel::getUser();
        switch ($data->res) {
            case 'project':
//                $res = strtotime($request->time) + strtotime('00:59')-strtotime("00:00:00");
                $project_task_contact = new Project_task_contact();
                $project_task_contact->project_id = $data->project->value;
                $project_task_contact->user_id = $data->user_id;
                $project_task_contact->observer_id = $cur_user->id;
                $project_task_contact->date_production = date("Y-m-d H:i:s");
                $project_task_contact->project_task_id = $data->task->id;
                $project_task_contact->next_contact = $data->date.' '.$data->time;
                $project_task_contact->end = $data->date.' '.$data->timeEnd;

                if ($data->comment != null) {
                    $comment = new Comment_task();
                    $comment->project_id = $data->project->value;
                    $comment->user_id = $cur_user->id;
                    $comment->task_id = $data->task->id;
                    // $comment->for_client = NULL;
                    // $comment->close = NULL;
                    $comment->date = $date[0];
                    $comment->time = $date[1];
                    $comment->text = $data->comment;
                    $comment->save();
                }
                
                if ($project_task_contact->save()) {
                    // print_r($data);
//                     $project_task_contacts = Project_task_contact::where('user_id', '=', $data->user_id)
//                         //->where('next_contact', '!=', NULL)
//                         ->whereNull('deleted_at')
//                         //->whereNull('status')
//                         ->get();

//                     if (count((array)$project_task_contacts) > 0) {
//                         foreach ($project_task_contacts as $ptc) {
                            
//                             if ($ptc->next_contact) {
//                                 $p_name = Project::find($ptc->project_id);
//                                 $pt = Project_task::find($ptc->project_task_id);
//                                 $name = Module_rule::find($pt->module_rule_id);
//                                 // dd($ptc->end);
//                                 $p_tasks[] = [
//                                     'id' => $pt->id,
//                                     'name' => $name['task']. "\r\n".$p_name['name'],
//                                     'p_name' => $p_name['name'],
//                                     // 'city' => $p_name['city'],
//                                     'city' => '',
//                                     'duration' => $ptc->duration,
//                                     'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
//                                     'callbackstatus_ref_id' => $ptc->callbackstatus_ref_id,
//                                     'next_contact' => $ptc->next_contact,
//                                     'prev_contact' => $ptc->prev_contact,
//                                     'end' => $ptc->end,
//                                 ];
//                             } else {
//                                 $p_name = Project::find($ptc->project_id);
//                                 $pt = Project_task::find($ptc->project_task_id);
//                                 $name = Module_rule::find($pt->module_rule_id);
//                                 $p_tasks[] = [
//                                     'id' => $pt->id,
//                                     'name' => $name['name']. "\r\n".$p_name['task'],
//                                     'p_name' => $p_name['name'],
//                                     // 'city' => $p_name['city'],
//                                     'city' => '',
//                                     'duration' => $ptc->duration,
//                                     'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
//                                     'callbackstatus_ref_id' => $ptc->callbackstatus_ref_id,
//                                     'next_contact' => $ptc->prev_contact,
//                                     'prev_contact' => $ptc->prev_contact,
//                                     'end' => $ptc->end,
//                                 ];
//                             }
//                         }
//             //    print_r($p_tasks);
//                         foreach ($p_tasks as $p_task) {
//                             $end = explode(' ', $p_task['next_contact']);

//                             $date = explode(' ', $p_task['next_contact']);
//                             if (!$p_task['callbackstatus_ref_id']) {
//                                 switch ($p_task['tasksstatus_ref_id']) {
//                                     case '1':
//                                         $json[] = array(
//                                             'id' => $p_task['id'],
//                                             'title' => $p_task['name'],
//                                             'start' => $p_task['next_contact'],
//                                             //'end' => $end[0] . ' ' . date('H:i:s', strtotime($end[1]) + strtotime($pt['duration']) - strtotime("00:00:00")),
//                                             'end' => $p_task['end'],
//                                             'allDay' => false,
//                                             'color' => '#00a65a',
//                                             'url' => '/implementer/project-task/edit/' . $p_task['id'] . '/' . $date[0],
//                                             //'timeFormat' => 'H:mm'
//                                         );
//                                         break;
//                                     case '2':
//                                         $json[] = array(
//                                             'id' => $p_task['id'],
//                                             'title' => $p_task['name'],
//                                             'start' => $p_task['next_contact'],
// //                                    'end' => $end[0] . ' ' . date('H:i:s', strtotime($end[1]) + strtotime($p_task['duration']) - strtotime("00:00:00")),
//                                             'end' => $p_task['end'],
//                                             //'end' => $p_task['next_contact'],
//                                             'allDay' => false,
//                                             'color' => '#f39c12',
//                                             'url' => '/implementer/project-task/edit/' . $p_task['id'] . '/' . $date[0],
//                                             //'timeFormat' => 'H(:mm)'
//                                         );
//                                         break;
//                                     case '3':
//                                         $json[] = array(
//                                             'id' => $p_task['id'],
//                                             'title' => $p_task['name'],
//                                             'start' => $p_task['next_contact'],
// //                                    'end' => $end[0] . ' ' . date('H:i:s', strtotime($end[1]) + strtotime($p_task['duration']) - strtotime("00:00:00")),
//                                             'end' => $p_task['end'],
//                                             //'end' => $p_task['next_contact'],
//                                             'allDay' => false,
//                                             'color' => '#00c0ef',
//                                             'url' => '/implementer/project-task/edit/' . $p_task['id'] . '/' . $date[0],
//                                             //'timeFormat' => 'H(:mm)'
//                                         );
//                                         break;
//                                 }
//                             } else {
//                                 $json[] = array(
//                                     'id' => $p_task['id'],
//                                     'title' => $p_task['name'],
//                                     'start' => $p_task['next_contact'],
// //                            'end' => $end[0].' '.date('H:i:s',strtotime($end[1]) + strtotime($p_task['duration'])-strtotime("00:00:00")),
//                                     'end' => $p_task['end'],
//                                     'allDay' => false,
//                                     'color' => '#dd4b39',
//                                     'url' => '/implementer/project-task/edit/'.$p_task['id'] . '/' . $date[0],
//                                     //'timeFormat' => 'H(:mm)'
//                                 );
//                             }
//                         }
//                     } else {
//                         $json[] = '';
//                     }

//                     $event = Event2::whereNull('deleted_at')
//                         ->where('user_id', '=', $data->user_id)
//                         ->get();
//                     foreach($event as $e) {
//                         $json[] = array(
//                             'id' => $e['id'],
//                             'title' => $e['title'],
//                             'start' => $e['start'],
// //                            'end' => $end[0].' '.date('H:i:s',strtotime($end[1]) + strtotime($p_task['duration'])-strtotime("00:00:00")),
//                             'end' => $e['end'],
//                             'allDay' => false,
//                             'color' => $e['color'],
//                             'className' => 'edit',
// //                'url' => '/implementer/project-task/edit/'.$p_task['id'] . '/' . $date[0],
//                             //'timeFormat' => 'H(:mm)'
//                         );
//                     };

//                     event(new addImpEvent(json_encode($json)));

                    return response()->json(['status' => 'Ok']);
                } else {
                    return response()->json(['status' => 'Error']);
                }
                break;
            case 'dop':
                $module_rule = new Module_rule();
                $module_rule->module_id = $data->module->id;
                $module_rule->task = $data->task->value;
                $module_rule->grouptasks_id = $data->groupTask->id;
                $module_rule->additional = 1;
                $module_rule->number_contacts = $data->numCont;


                if ($module_rule->save()) {
                    $cur_user = Sentinel::getUser();
                    try {
                        $project_task = new Project_task();
                        $project_task->project_id = $data->project->value;
                        $project_task->user_id = $data->user_id;
                        $project_task->observer_id = $cur_user->id;
                        $project_task->module_rule_id = $module_rule->id;
                        $project_task->number_contacts = $data->numCont;
                        $project_task->grouptasks_id = $data->groupTask->id;
                        $project_task->module_id = $data->module->id;
                        $project_task->tasksstatus_ref_id = 1;
                        $project_task->save();

                        $project_task_contact = new Project_task_contact();
                        $project_task_contact->project_id = $data->project->value;
                        $project_task_contact->user_id = $data->user_id;
                        $project_task_contact->observer_id = $cur_user->id;
                        $project_task_contact->date_production = date("Y-m-d H:i:s");
                        $project_task_contact->project_task_id = $project_task->id;
                        $project_task_contact->next_contact = $data->date.' '.$data->time;
                        $project_task_contact->end = $data->date.' '.$data->timeEnd;
                        $project_task_contact->save();

                        if ($data->comment != null) {
                            $comment = new Comment();
                            $comment->project_id = $data->project->value;
                            $comment->user_id = $cur_user->id;
                            $comment->for_client = NULL;
                            $comment->date = $date[0];
                            $comment->time = $date[1];
                            $comment->text = $data->comment;
                            $comment->save();
                        }

                        $success = true;
                    } catch (\Exception $e) {
                        $success = false;
                    }

                    if ($success) {

//                         $project_task_contacts = Project_task_contact::where('user_id', '=', $data->user_id)
//                             //->where('next_contact', '!=', NULL)
//                             ->whereNull('deleted_at')
//                             //->whereNull('status')
//                             ->get();

//                         if (count($project_task_contacts) > 0) {
//                             foreach ($project_task_contacts as $ptc) {
//                                 if ($ptc->next_contact) {
//                                     $p_name = Project::find($ptc->project_id);
//                                     $pt = Project_task::find($ptc->project_task_id);
//                                     $name = Module_rule::find($pt->module_rule_id);
//                                     $p_tasks[] = [
//                                         'id' => $pt->id,
//                                         'name' => $name['task']. "\r\n".$p_name['name'],
//                                         'p_name' => $p_name['name'],
//                                         'city' => $p_name['city'],
//                                         'duration' => $ptc->duration,
//                                         'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
//                                         'callbackstatus_ref_id' => $ptc->callbackstatus_ref_id,
//                                         'next_contact' => $ptc->next_contact,
//                                         'prev_contact' => $ptc->prev_contact,
//                                         'end' => $ptc->end,
//                                     ];
//                                 } else {
//                                     $p_name = Project::find($ptc->project_id);
//                                     $pt = Project_task::find($ptc->project_task_id);
//                                     $name = Module_rule::find($pt->module_rule_id);
//                                     $p_tasks[] = [
//                                         'id' => $pt->id,
//                                         'name' => $name['name']. "\r\n".$p_name['task'],
//                                         'p_name' => $p_name['name'],
//                                         'city' => $p_name['city'],
//                                         'duration' => $ptc->duration,
//                                         'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
//                                         'callbackstatus_ref_id' => $ptc->callbackstatus_ref_id,
//                                         'next_contact' => $ptc->prev_contact,
//                                         'prev_contact' => $ptc->prev_contact,
//                                         'end' => $ptc->end,
//                                     ];
//                                 }
//                             }
// //                print_r($p_tasks);
//                             foreach ($p_tasks as $p_task) {
//                                 $end = explode(' ', $p_task['next_contact']);

//                                 $date = explode(' ', $p_task['next_contact']);
//                                 if (!$p_task['callbackstatus_ref_id']) {
//                                     switch ($p_task['tasksstatus_ref_id']) {
//                                         case '1':
//                                             $json[] = array(
//                                                 'id' => $p_task['id'],
//                                                 'title' => $p_task['name'],
//                                                 'start' => $p_task['next_contact'],
//                                                 //'end' => $end[0] . ' ' . date('H:i:s', strtotime($end[1]) + strtotime($pt['duration']) - strtotime("00:00:00")),
//                                                 'end' => $p_task['end'],
//                                                 'allDay' => false,
//                                                 'color' => '#00a65a',
//                                                 'url' => '/implementer/project-task/edit/' . $p_task['id'] . '/' . $date[0],
//                                                 //'timeFormat' => 'H:mm'
//                                             );
//                                             break;
//                                         case '2':
//                                             $json[] = array(
//                                                 'id' => $p_task['id'],
//                                                 'title' => $p_task['name'],
//                                                 'start' => $p_task['next_contact'],
// //                                    'end' => $end[0] . ' ' . date('H:i:s', strtotime($end[1]) + strtotime($p_task['duration']) - strtotime("00:00:00")),
//                                                 'end' => $p_task['end'],
//                                                 //'end' => $p_task['next_contact'],
//                                                 'allDay' => false,
//                                                 'color' => '#f39c12',
//                                                 'url' => '/implementer/project-task/edit/' . $p_task['id'] . '/' . $date[0],
//                                                 //'timeFormat' => 'H(:mm)'
//                                             );
//                                             break;
//                                         case '3':
//                                             $json[] = array(
//                                                 'id' => $p_task['id'],
//                                                 'title' => $p_task['name'],
//                                                 'start' => $p_task['next_contact'],
// //                                    'end' => $end[0] . ' ' . date('H:i:s', strtotime($end[1]) + strtotime($p_task['duration']) - strtotime("00:00:00")),
//                                                 'end' => $p_task['end'],
//                                                 //'end' => $p_task['next_contact'],
//                                                 'allDay' => false,
//                                                 'color' => '#00c0ef',
//                                                 'url' => '/implementer/project-task/edit/' . $p_task['id'] . '/' . $date[0],
//                                                 //'timeFormat' => 'H(:mm)'
//                                             );
//                                             break;
//                                     }
//                                 } else {
//                                     $json[] = array(
//                                         'id' => $p_task['id'],
//                                         'title' => $p_task['name'],
//                                         'start' => $p_task['next_contact'],
// //                            'end' => $end[0].' '.date('H:i:s',strtotime($end[1]) + strtotime($p_task['duration'])-strtotime("00:00:00")),
//                                         'end' => $p_task['end'],
//                                         'allDay' => false,
//                                         'color' => '#dd4b39',
//                                         'url' => '/implementer/project-task/edit/'.$p_task['id'] . '/' . $date[0],
//                                         //'timeFormat' => 'H(:mm)'
//                                     );
//                                 }
//                             }
//                         } else {
//                             $json[] = '';
//                         }

//                         $event = Event2::whereNull('deleted_at')
//                             ->where('user_id', '=', $data->user_id)
//                             ->get();
//                         foreach($event as $e) {
//                             $json[] = array(
//                                 'id' => $e['id'],
//                                 'title' => $e['title'],
//                                 'start' => $e['start'],
// //                            'end' => $end[0].' '.date('H:i:s',strtotime($end[1]) + strtotime($p_task['duration'])-strtotime("00:00:00")),
//                                 'end' => $e['end'],
//                                 'allDay' => false,
//                                 'color' => $e['color'],
//                                 'className' => 'edit',
// //                'url' => '/implementer/project-task/edit/'.$p_task['id'] . '/' . $date[0],
//                                 //'timeFormat' => 'H(:mm)'
//                             );
//                         };

//                         event(new addImpEvent(json_encode($json)));

                        return response()->json(['status' => 'Ok']);
                    } else {
                        return response()->json(['status' => 'Error']);
                    }
                    break;
                }
        }
    }

    public function addTaskFromCalendar2(Request $request) {
        $data = json_decode($request->data);
//        print_r($data);
        $date =  explode(' ', date("Y-m-d H:i:s"));
        $cur_user = Sentinel::getUser();
        switch ($data->res) {
            case 'project':
//                $res = strtotime($request->time) + strtotime('00:59')-strtotime("00:00:00");
                $project_task_contact = new Project_task_contact();
                $project_task_contact->project_id = $data->project->value;
                $project_task_contact->user_id = $data->user_id;
                $project_task_contact->project_task_id = $data->task->id;
                $project_task_contact->next_contact = $data->date.' '.$data->time;
                $project_task_contact->end = $data->date.' '.$data->timeEnd.':00';

                if ($data->comment != null) {
                    $comment = new Comment();
                    $comment->project_id = $data->project->value;
                    $comment->user_id = $cur_user->id;
                    $comment->for_client = NULL;
                    $comment->close = NULL;
                    $comment->date = $date[0];
                    $comment->time = $date[1];
                    $comment->text = $data->comment;
                    $comment->save();
                }

                if ($project_task_contact->save()) {
                    return response()->json(['status' => 'Ok']);
                } else {
                    return response()->json(['status' => 'Error']);
                }
                break;
            case 'dop':
                $module_rule = new Module_rule();
                $module_rule->project_tasks_id = $data->task->id;
                $module_rule->task = $data->task->value;
//                $module_rule->grouptasks_id = $data->groupTask->id;
                $module_rule->additional = 1;
                $module_rule->number_contacts = $data->numCont;


                if ($module_rule->save()) {
                    $cur_user = Sentinel::getUser();
                    try {
                        $project_task = new Project_task();
                        $project_task->project_id = $data->project->value;
                        $project_task->user_id = $data->user_id;
                        $project_task->observer_id = $cur_user->id;
//                        $project_task->module_rule_id = $module_rule->id;
                        $project_task->number_contacts = $data->numCont;
//                        $project_task->grouptasks_id = $data->groupTask->id;
//                        $project_task->module_id = $data->module->id;
                        $project_task->tasksstatus_ref_id = 1;
                        $project_task->save();

                        $project_task_contact = new Project_task_contact();
                        $project_task_contact->project_id = $data->project->value;
                        $project_task_contact->user_id = $data->user_id;
                        $project_task_contact->project_task_id = $project_task->id;
                        $project_task_contact->next_contact = $data->date.' '.$data->time;
                        $project_task_contact->end = $data->date.' '.$data->timeEnd;
                        $project_task_contact->save();

                        if ($data->comment != null) {
                            $comment = new Comment();
                            $comment->project_id = $data->project->value;
                            $comment->user_id = $cur_user->id;
                            $comment->for_client = NULL;
                            $comment->date = $date[0];
                            $comment->time = $date[1];
                            $comment->text = $data->comment;
                            $comment->save();
                        }

                        $success = true;
                    } catch (\Exception $e) {
                        $success = false;
                    }

                    if ($success) {
                        return response()->json(['status' => 'Ok']);
                    } else {
                        return response()->json(['status' => 'Error']);
                    }
                    break;
                }
        }
    }

    public function getProjectModules($id) {
        $modules = Modules_reference::where('project_modules.project_id', '=', $id)
            ->join('project_modules', 'project_modules.module_id', '=', 'modules_references.id')
            ->select('modules_references.name', 'modules_references.id')
            ->get();

        return response()->json($modules);
    }

    public function getGroupTask() {
        $groupTask = Grouptasks_reference::whereNull('deleted_at')
            ->get();

        return response()->json($groupTask);
    }

    public function getImplementers() {
        $users = User::whereNull('users.deleted_at')
            ->join('role_users', 'users.id', '=', 'role_users.user_id')
            ->join('roles', 'role_users.role_id', '=', 'roles.id')
            ->where('roles.name', '=', 'Отдел внедрения')
            ->select('users.id',
                'users.email',
                'users.is_ban',
                'users.foto',
                'users.first_name',
                'users.last_name')
            ->get();
        $freshTime = Carbon::now()->subWeeks(2);

        foreach ($users as $k => $u) {
            $project_all = Project::where('user_id', '=', $u->id)
                ->where('done', '!=', 1)
                ->whereNull('deleted_at')
                ->count();

            $project_work = DB::select(
                "select count(p.id) as count
                from projects p
                where p.user_id = {$u->id} and p.done != 1 and p.deleted_at is null
                and (exists (select 1 from comments where project_id = p.id and deleted_at is null and updated_at >= '$freshTime')
                or exists (select 1 from printform_comments pc
                                    left join project_print_forms ppf on ppf.id = pc.form_id
                                    where ppf.project_id = p.id and ppf.deleted_at is null and pc.deleted_at is null
                                    and pc.updated_at >= '$freshTime'))"
            )[0]->count;

            $project_close = Project::where('user_id', '=', $u->id)
                ->where('done', '=', 1)
                ->whereNull('deleted_at')
                ->count();

            // $task = Project_task::where('user_id', '=', $u->id)
            //     ->where('tasksstatus_ref_id', '!=', 2)
            //     ->whereNull('deleted_at')
            //     ->count();

            $users[$k]['project_all'] = $project_all;
            $users[$k]['project_work'] = $project_work;
            $users[$k]['project_sleep'] = $project_all - $project_work;
            $users[$k]['project_close'] = $project_close;

        }

        return response()->json($users);
    }

    public function addEvent(Request $request) {
        $data = json_decode($request->data);
        $curUser = Sentinel::getUser();

        $event = new Event2();
        $event->title = $data->name;
        $event->user_id = $curUser->id;
        $event->start = $data->dateStart.' '.$data->timeStart;
        $event->end = $data->dateStart.' '.$data->timeEnd;


        if ($event->save()) {
            $project_task_contacts = Project_task_contact::where('user_id', '=', $curUser->id)
                //->where('next_contact', '!=', NULL)
                ->whereNull('deleted_at')
                //->whereNull('status')
                ->get();

            if (count($project_task_contacts) > 0) {
                foreach ($project_task_contacts as $ptc) {
                    if ($ptc->next_contact) {
                        $p_name = Project::find($ptc->project_id);
                        $pt = Project_task::find($ptc->project_task_id);
                        $name = Module_rule::find($pt->module_rule_id);
                        $p_tasks[] = [
                            'id' => $pt->id,
                            'name' => $name['task']. "\r\n".$p_name['name'],
                            'p_name' => $p_name['name'],
                            'city' => $p_name['city'],
                            'duration' => $ptc->duration,
                            'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                            'callbackstatus_ref_id' => $ptc->callbackstatus_ref_id,
                            'next_contact' => $ptc->next_contact,
                            'prev_contact' => $ptc->prev_contact,
                            'end' => $ptc->end,
                        ];
                    } else {
                        $p_name = Project::find($ptc->project_id);
                        $pt = Project_task::find($ptc->project_task_id);
                        $name = Module_rule::find($pt->module_rule_id);
                        $p_tasks[] = [
                            'id' => $pt->id,
                            'name' => $name['name']. "\r\n".$p_name['task'],
                            'p_name' => $p_name['name'],
                            'city' => $p_name['city'],
                            'duration' => $ptc->duration,
                            'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                            'callbackstatus_ref_id' => $ptc->callbackstatus_ref_id,
                            'next_contact' => $ptc->prev_contact,
                            'prev_contact' => $ptc->prev_contact,
                            'end' => $ptc->end,
                        ];
                    }
                }
//                print_r($p_tasks);
                foreach ($p_tasks as $p_task) {
                    $end = explode(' ', $p_task['next_contact']);

                    $date = explode(' ', $p_task['next_contact']);
                    if (!$p_task['callbackstatus_ref_id']) {
                        switch ($p_task['tasksstatus_ref_id']) {
                            case '1':
                                $json[] = array(
                                    'id' => $p_task['id'],
                                    'title' => $p_task['name'],
                                    'start' => $p_task['next_contact'],
                                    //'end' => $end[0] . ' ' . date('H:i:s', strtotime($end[1]) + strtotime($pt['duration']) - strtotime("00:00:00")),
                                    'end' => $p_task['end'],
                                    'allDay' => false,
                                    'color' => '#00a65a',
                                    'url' => '/implementer/project-task/edit/' . $p_task['id'] . '/' . $date[0],
                                    //'timeFormat' => 'H:mm'
                                );
                                break;
                            case '2':
                                $json[] = array(
                                    'id' => $p_task['id'],
                                    'title' => $p_task['name'],
                                    'start' => $p_task['next_contact'],
//                                    'end' => $end[0] . ' ' . date('H:i:s', strtotime($end[1]) + strtotime($p_task['duration']) - strtotime("00:00:00")),
                                    'end' => $p_task['end'],
                                    //'end' => $p_task['next_contact'],
                                    'allDay' => false,
                                    'color' => '#f39c12',
                                    'url' => '/implementer/project-task/edit/' . $p_task['id'] . '/' . $date[0],
                                    //'timeFormat' => 'H(:mm)'
                                );
                                break;
                            case '3':
                                $json[] = array(
                                    'id' => $p_task['id'],
                                    'title' => $p_task['name'],
                                    'start' => $p_task['next_contact'],
//                                    'end' => $end[0] . ' ' . date('H:i:s', strtotime($end[1]) + strtotime($p_task['duration']) - strtotime("00:00:00")),
                                    'end' => $p_task['end'],
                                    //'end' => $p_task['next_contact'],
                                    'allDay' => false,
                                    'color' => '#00c0ef',
                                    'url' => '/implementer/project-task/edit/' . $p_task['id'] . '/' . $date[0],
                                    //'timeFormat' => 'H(:mm)'
                                );
                                break;
                        }
                    } else {
                        $json[] = array(
                            'id' => $p_task['id'],
                            'title' => $p_task['name'],
                            'start' => $p_task['next_contact'],
//                            'end' => $end[0].' '.date('H:i:s',strtotime($end[1]) + strtotime($p_task['duration'])-strtotime("00:00:00")),
                            'end' => $p_task['end'],
                            'allDay' => false,
                            'color' => '#dd4b39',
                            'url' => '/implementer/project-task/edit/'.$p_task['id'] . '/' . $date[0],
                            //'timeFormat' => 'H(:mm)'
                        );
                    }
                }
            } else {
                $json[] = '';
            }

            $event = Event2::whereNull('deleted_at')
                ->where('user_id', '=', $curUser->id)
                ->get();
            foreach($event as $e) {
                $json[] = array(
                    'id' => $e['id'],
                    'title' => $e['title'],
                    'start' => $e['start'],
//                            'end' => $end[0].' '.date('H:i:s',strtotime($end[1]) + strtotime($p_task['duration'])-strtotime("00:00:00")),
                    'end' => $e['end'],
                    'allDay' => false,
                    'color' => $e['color'],
                    'className' => 'edit',
//                'url' => '/implementer/project-task/edit/'.$p_task['id'] . '/' . $date[0],
                    //'timeFormat' => 'H(:mm)'
                );
            };

            event(new addImpEvent(json_encode($json)));
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function addEvent2(Request $request) {
        $data = json_decode($request->data);
        $curUser = Sentinel::getUser();

        // dd(Carbon::parse($data->dateStart)->addDays(0)->format('Y.m.d'));

        $i = 0;
        $status = false;
        do {
            $next_date = Carbon::parse($data->dateStart)->addDays($i)->format('Y-m-d');
            $event = new Event2();
            $event->title = $data->name;
            $event->user_id = $data->imp;
            $event->observer_id = $curUser->id;
            $event->date_production = date("Y-m-d H:i:s");
            $event->start = $next_date.' '.$data->timeStart;
            $event->end = $next_date.' '.$data->timeEnd;

            if ($event->save()) {
                $status = true;
            } else {
                $status = false;
            }
            $i++;
        } while (Carbon::parse($data->dateStart)->addDays($i)->format('Y-m-d') <= $data->dateEnd);

        if ($status == true) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function getContact(Request $request, $id) {
        $contact = DB::select("select `c`.`first_name`, `c`.`last_name`, `c`.`phone`, `c`.`email`
            from `projects` `p`
            left join `project_contacts` `pc` on `p`.`id` = `pc`.`project_id`
            left join `contacts` `c` on `c`.`id` = `pc`.`contact_id`
            where `p`.`id` = ".$id." and
            `c`.`deleted_at` IS NULL and
            `pc`.`deleted_at` IS NULL");

        return response()->json($contact);
    }

    public function getUsers() {
        $users = DB::select("select `u`.`id`, `u`.`email`, `u`.`foto`,
            `u`.`first_name`, `u`.`last_name`, `r`.`name`
            from `users` `u`
            left join `role_users` `ru` on `ru`.`user_id` = `u`.`id`
            left join `roles` `r` on `r`.`id` = `ru`.`role_id`
            where `u`.`deleted_at` is null and
            `u`.`id` != 0
            order by `u`.`first_name`");

        return response()->json($users);
    }

    public function getQuality() {
        $projects = DB::select("select distinct `p`.`id` as `p_id`, `p`.`name` as `p_name`, 
            `client`.`name` as `client_name`, `city`.`name` as `city_name`, 
            DATE_FORMAT(`p`.`quality_date`, '%d.%m.%Y %H:%i') as `created_at`
            from `projects` `p`
            left join `callback_work_answers` `answer` on `answer`.`project_id` = `p`.`id`
            left join `clients_references` `client` on `client`.`id` = `p`.`client_id`
            left join `city_references` `city` on `city`.`id` = `client`.`city_id`
            where `p`.`done` = 0 and
            `p`.`deleted_at` is null 
            limit 10");

        return response()->json($projects);
    }

    public function getQualityAll() {
        $projects = DB::select("select distinct `p`.`id` as `p_id`, `p`.`name` as `p_name`, 
            `client`.`name` as `client_name`, `city`.`name` as `city_name`, 
            DATE_FORMAT(`p`.`quality_date`, '%d.%m.%Y %H:%i') as `created_at`
            from `projects` `p`
            left join `callback_work_answers` `answer` on `answer`.`project_id` = `p`.`id`
            left join `clients_references` `client` on `client`.`id` = `p`.`client_id`
            left join `city_references` `city` on `city`.`id` = `client`.`city_id`
            where `p`.`done` = 0 and
            `p`.`deleted_at` is null");

        return response()->json($projects);
    }
}
