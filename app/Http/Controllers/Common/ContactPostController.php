<?php

namespace App\Http\Controllers\Common;

use App\Posts_reference;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use DB;

class ContactPostController extends Controller {

    function get(){
        return Posts_reference::all(['id', 'name']);
    }
    function save(Request $request){
        $data = json_decode($request->data);
        Posts_reference::create((array)$data->item);
        return ['status' => 'Ok'];
    }
    function update(Request $request, $id){
        $data = json_decode($request->data);
        $post = Posts_reference::find($id);
        $post->update((array)$data->item);
        return ['status' => 'Ok'];
    }
    function delete($id){
        $post = Posts_reference::find($id);
        $post->delete();
        return ['status' => 'Ok'];
    }
}
