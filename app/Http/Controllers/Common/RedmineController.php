<?php


namespace App\Http\Controllers\Common;


use Illuminate\Http\Request;

class RedmineController extends Controller {

    protected $key = 'key=ad6ae562e8896529e3f06aef9cf532797aa8ddee';

    function getIssue($id) {

        try {
            if (!is_numeric($id)){
                throw new \Exception('Номер задачи должен содержать только цифры');
            }
            $ch = curl_init($this->getUrl() . "issues/$id.json?" . $this->key);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_TIMEOUT, 5);

            $result = json_decode(curl_exec($ch), true);
            if (isset($result['issue'])) {
                return $this->responseOk($result);
            } else {
                throw new \Exception('Задача не найдена');
            }
        } catch (\Exception $exception){
            return $this->responseError($exception->getMessage());
        }
    }

    static function getUrl($local = false) {
        if ($local){
            return 'http://192.168.1.253/';
        }
        return env('REDMINE_URL', 'http://192.168.1.253/');
//        return 'http://94.180.249.180:21100/';
    }

}