<?php


namespace App\Http\Controllers\Common;


use App\ProjectCategory;
use Illuminate\Http\Request;

class ProjectCategoriesController extends Controller {
    function get(){
        return ProjectCategory::all(['id', 'name']);
    }
    function save(Request $request){

        ProjectCategory::create($request->all());
        return $this->responseOk();
    }
    function update(Request $request, $id){
        $category = ProjectCategory::find($id);
        $category->update($request->all());
        return $this->responseOk();
    }
    function delete($id){
        $category = ProjectCategory::find($id);
        if ($category->delete()){
            return $this->responseOk();
        }
        else {
            return $this->responseError('Нельзя удалить категорию');
        }
    }
}