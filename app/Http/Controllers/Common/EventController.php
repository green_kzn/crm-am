<?php


namespace App\Http\Controllers\Common;


use App\Event as Event2;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Sentinel;

class EventController extends Controller {

    function save(Request $request, $id) {

        $curUser = Sentinel::getUser();

        $oldEvent = Event2::find($id);

        $oldDate = $oldEvent ? Carbon::parse($oldEvent->start)->format('Y-m-d') : null;
        $oldStart = $oldEvent ? Carbon::parse($oldEvent->start)->format('H:i:s') : null;
        $oldEnd = $oldEvent ? Carbon::parse($oldEvent->end)->format('H:i:s') : null;

        $i = 0;
        $status = true;

        $deleteOld = true;

        do {
            $next_date = Carbon::parse($request->dateStart)->addDays($i)->format('Y-m-d');

            $deleteOld = !($oldDate == $next_date) && $deleteOld;

            $nextOldEvent = $oldEvent ? Event2::where('start','like','%'. $oldStart)
                ->where('end','like','%'.$oldEnd)
                ->where('user_id', $request->imp)
                ->where('start','like',$next_date.'%')
                ->first() : null;

            $event = $nextOldEvent ?? new Event2();

            $event->title = $request->name;
            $event->user_id = $request->imp;
            $event->observer_id = $curUser->id;
            $event->date_production = date("Y-m-d H:i:s");
            $event->start = $next_date . ' ' . $request->timeStart;
            $event->end = $next_date . ' ' . $request->timeEnd;

            $status = $event->save() ? $status : false;

            $i++;
        } while (Carbon::parse($request->dateStart)->addDays($i)->format('Y-m-d') <= $request->dateEnd);

        if ($deleteOld && $oldEvent){
            $oldEvent->delete();
        }

        if ($status == true) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

}