<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Contact;
use App\Project_contact;
use App\Project;
use App\Posts_reference;
use Sentinel;

class ContactController extends Controller
{



    public function del($project_id, $id){
        $p_contact = Project_contact::where('contact_id', $id)
            ->where('project_id', $project_id)
            ->first();

        if ($p_contact->delete()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }
    public function update(Request $request, $project_id, $id){
        $data = json_decode($request->data);

        $contact = Contact::find($id);

        $contact->update([
            'first_name' => $data->contact->first_name,
            'last_name' => $data->contact->last_name,
            'patronymic' => $data->contact->patronymic,
            'post_id' => $data->contact->post_id,
            'phone' => $data->contact->phone,
            'email' => $data->contact->email,
        ]);

        $p_contact = Project_contact::where('contact_id', $id)
                ->where('project_id', $project_id)
                ->first();
        $p_contact->update(['main'=> $data->contact->main, 'mail_rep' => $data->contact->mail_rep]);

        if ($p_contact->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function add(Request $request, $project_id) {
        $data = json_decode($request->data);
        $cur_user = Sentinel::getUser();

        $contact = Contact::create([
            'first_name' => $data->newContact->firstName ?? '',
            'last_name' => $data->newContact->lastName,
            'patronymic' => $data->newContact->patronymic ?? '',
            'post_id' => $data->newContact->post_id,
            'phone' => $data->newContact->phone,
            'email' => $data->newContact->email ?? '',
            'client_id' => $data->client_id,
            'user_id' => $cur_user->id
        ]);

        $p_contact = new Project_contact();
        $p_contact->contact_id = $contact->id;
        $p_contact->project_id = $project_id;
        $p_contact->main = $data->newContact->main ?? false;
        $p_contact->mail_rep = $data->newContact->mail_rep ?? false;

        if ($p_contact->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function getAll($id){
        $contacts = DB::select('select `c`.`id` as `id`, `pc`.`main`, `pc`.`mail_rep`, `c`.`first_name`, 
            `c`.`last_name`, `c`.`patronymic`, `pr`.`name` as `post_name`, `pr`.`id` as `post_id`, `c`.`phone`, `c`.`email`
            from `project_contacts` `pc`
            left join `contacts` `c` on `pc`.`contact_id` = `c`.`id`
            left join `posts_references` `pr` on `pr`.`id` = `c`.`post_id`
            where
            `pc`.`deleted_at` is null and
            `c`.`deleted_at` is null and
            `pc`.`project_id` = '.$id);
        return $contacts;
    }
}
