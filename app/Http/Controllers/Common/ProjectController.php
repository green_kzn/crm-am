<?php


namespace App\Http\Controllers\Common;


use App\Project;
use Illuminate\Http\Request;
use Sentinel;

class ProjectController extends Controller {

    function update(Request $request, $id){
        $project = Project::find($id);
        if (!$project) $this->responseError('Проект не найден');

        $user = Sentinel::getUser();

        if (!$user->head ?? false) $this->responseError('Ошибка доступа');

        if ($project->update($request->all())){
            return $this->responseOk();
        } else {
            return $this->responseError('Ошибка сохранения');
        }


    }

}