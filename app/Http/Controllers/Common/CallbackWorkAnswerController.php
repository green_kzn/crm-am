<?php


namespace App\Http\Controllers\Common;


use Illuminate\Support\Facades\DB;

class CallbackWorkAnswerController {

    function getByProjectId($id){

        $answers = DB::select("select `question`.`id`, `question`.`question`, `answer`.`answer`, 
            DATE_FORMAT(`answer`.`created_at`,'%d.%m.%Y %H:%i') as `date`, c.text
            from `callback_work_questions` `question`
            left join `callback_work_answers` `answer` on `answer`.`question_id` = `question`.`id`
            left join `comments` `c` on `c`.`created_at` = `answer`.`created_at`
            where
            `answer`.`project_id` = ?
            order by answer.created_at desc", [$id]);

        $result = [];
        $comments = [];
        foreach ($answers as $answer) {
            $result[$answer->date][] = $answer;
            if ($answer->text) {
                $comments[$answer->date][$answer->text] = ['question' => 'Комментарий', 'answer' => $answer->text];
            }

        }

        $result2 = [];
        foreach ($result as $date => $answers) {
            $answers = array_merge($answers, array_values($comments[$date]));
            $result2[] = (object)['date' => $date,'answers' => $answers];
        }
        foreach ($result2 as $item) {
            $item->date = substr($item->date, 0, 10);
        }

        return response()->json($result2);
    }

}