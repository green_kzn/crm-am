<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Print_form\User2Controller as Users;
use App\Printform_comment;
use App\Project_print_form;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Sentinel;

class PrintFormComments extends Controller {

    function get(Request $request, $id) {
        $comment_q = Printform_comment::where('form_id', '=', $id)
            ->whereNull('deleted_at')
            ->orderBy('created_at', 'desc');
        if (isset($request->limit)){
            $comment_q->limit($request->limit);
        }
        $comment_q = $comment_q->get();

        $cur_user = Sentinel::getUser();

        $comments = [];
        foreach ($comment_q as $c) {
            $user2 = Users::getUserById($c->user_id);
            $comments[] = [
                'id' => $c->id,
                'text' => $c->comment,
                'user_foto' => $user2['foto'],
                'user_name' => $user2['first_name'],
                'user_surname' => $user2['last_name'],
                'date' => Carbon::parse($c->created_at)->format('d.m.Y'),
                'time' => Carbon::parse($c->created_at)->format('H:m'),
                'autor' => intval($cur_user->id == $user2['id'])
            ];
        }
        return $comments;
    }

    function save(Request $request, $id){
        $requestData =  json_decode($request->data);
        if (!isset($requestData->comment)) return ['status' => 'Error', 'msg' => 'No data'];
        $cur_user = Sentinel::getUser();
        $data = [
            'user_id' => $cur_user->id,
            'form_id' => $id,
            'comment' => $requestData->comment
        ];
        $comment = Printform_comment::create($data);
        if ($comment->id){
            return  ['status' => 'Ok'];
        } else {
            return ['status' => 'Error', 'msg' => 'Creation error'];
        }
    }

    function delete(Request $request, $formId, $commentId){
        $comment = Printform_comment::find($commentId);
        $comment->delete();
        return  ['status' => 'Ok'];
    }

    function update(Request $request, $formId, $commentId){
        $requestData =  json_decode($request->data);
        if (!isset($requestData->comment->text)) return ['status' => 'Error'];
        $comment = Printform_comment::find($commentId);
        $comment->update(['comment' =>$requestData->comment->text]);
        return  ['status' => 'Ok'];
    }

}