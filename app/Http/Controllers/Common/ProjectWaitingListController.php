<?php

namespace App\Http\Controllers\Common;

use App\ProjectWaitingList;
use App\WaitingList;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Project;
use Sentinel;

class ProjectWaitingListController extends Controller {

    public function del($project_id, $id) {
        try {

            if (ProjectWaitingList::find($id)->delete()){
                return $this->responseOk();
            }
            throw new \Exception('Ошибка удаления');
        } catch (\Exception $e){
            return $this->responseError($e->getMessage());
        }
    }

    public function update(Request $request, $project_id, $id) {
        try {
            $projectWaitingList = ProjectWaitingList::find($id);
            $projectWaitingList->update($request->all());
            return $this->responseOk();
        } catch (\Exception $e){
            return $this->responseError($e->getMessage());
        }
    }

    public function add(Request $request, $project_id) {

        try {
            $projectWaitingList = new ProjectWaitingList($request->all());
            $projectWaitingList->waiting_list_id = $request->waiting_list['id'];
            $projectWaitingList->project_id = $project_id;
            $projectWaitingList->save();

            return $this->responseOk();
        } catch (\Exception $e){
            return $this->responseError($e->getMessage());
        }
    }

    public function getAll(Request $request, $id) {
        /** @var Project $project */
        $project = Project::find($id);
        $query = $project->projectWaitingLists()
                ->with('waitingList.waitingListType')
                ->latest();
        if ($request->get('activeOnly')){
            $query->whereNull('solved_at');
        }
        $all = $query->get();

        foreach ($all as &$item) {
            if ($item->waitingList->has_redmine){
                $redmine = new RedmineController();
                $issue = $redmine->getIssue($item->redmine_task_id);
                if ($issue['status'] == 'Ok'){
                    $info = $issue['data']['issue']['subject'];
                    $info .= "<br>Статус: " . $issue['data']['issue']['status']['name'];
                    $item->redmine_info = $info;
                }

            }
        }
        return $this->responseOk($all);
    }

    public function solved($project_id, $id) {
        try {
            $projectWaitingList = ProjectWaitingList::find($id);
            $projectWaitingList->update([
                'is_solved' => 1,
                'solved_at' => Carbon::now()
            ]);
            return $this->responseOk();
        } catch (\Exception $e){
            return $this->responseError($e->getMessage());
        }
    }
}
