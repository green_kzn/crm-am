<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Project_print_form;
use Illuminate\Http\Request;
use DB;

class PrintFormController extends Controller
{
    public function getAll($id) {
        $forms = DB::select("select 
                ppf.id
                , `pf`.`file` as `pf_file`
                , `pf`.`g_file` as `pf_g_file`
                , `u`.`first_name` as `first_name`
                , `u`.`last_name` as `last_name`
                , concat(`obs`.`first_name`, ' ', `obs`.`last_name`) as `obs_name`
                , `df`.`file` as `df_file`
                , `df`.`g_file` as `df_g_file`
                , `sr`.`id` as `status_id`
                , `sr`.`name` as `status`
                , `pf`.`download_url` as pf_download_url
                , `pf`.`public_url` as pf_public_url
                , `df`.`download_url` as df_download_url
                , `df`.`public_url` as df_public_url
                , `ppf`.`date_implement` 
                , `ppf`.`real_date` 
                , `ppf`.`date_implement_for_client` 
                , `ppf`.`created_at` 
                , `ppf`.`free` 

        from `project_print_forms` `ppf`
        left join `printform_files` `pf` on `pf`.`project_print_form_id` = `ppf`.`id`
        left join `done_printform_files` `df` on `df`.`project_print_form_id` = `ppf`.`id`
        left join `printform_status_references` `sr` on `sr`.`id` = `ppf`.`printform_status_ref_id`
        left join `users` `u` on `u`.`id` = `ppf`.`user_id`
        left join `users` `obs` on `obs`.`id` = `ppf`.`observer_id`
        where `ppf`.`deleted_at` is null 
        and `ppf`.`project_id` = ".$id);

        return response()->json($forms);
    }

    public function update(Request $request, $projectId, $formId) {
//        $data = json_decode($request->data);
//        $form = Project_print_form::find($formId);
//        if ($form->update($data)){
//            return response()->json(['status' => 'Ok']);
//        } else {
//            return response()->json(['status' => 'Error']);
//        }
    }

    public function getStatuses(){
        $statuses = DB::select("select id, name from printform_status_references");
        return response()->json($statuses);
    }
    public function close($projectId, $formId){
        $form = Project_print_form::find($formId);
        if ($form && $form->update(['printform_status_ref_id' => 9])){
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

}
