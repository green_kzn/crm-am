<?php


namespace App\Http\Controllers\Common;


use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ModelController extends Controller {

    protected $class;

    public function __construct(Request $request) {
        $this->class = 'App\\'.Str::ucfirst(Str::camel(Str::singular($request->model)));
    }

    function get(){
        return $this->class::withAll()->get();
    }
    function save(Request $request){
        try {
            $this->class::create($request->all());
            return $this->responseOk();
        } catch (\Exception $exception){
            return $this->responseError($exception->getMessage());
        }
    }
    function update(Request $request, $model,  $id){
        $category = $this->class::find($id);
        $category->update($request->all());
        return $this->responseOk();
    }
    function delete($model, $id){
        $category = $this->class::find($id);
        if ($category->delete()){
            return $this->responseOk();
        }
        else {
            return $this->responseError('Нельзя удалить');
        }
    }
}