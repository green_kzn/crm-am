<?php


namespace App\Http\Controllers\Common;


use App\Comment_task;
use App\Module_rule;
use App\Project_task;
use App\Project_task_contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Sentinel;

class TaskContactController {

    function get($id) {
        return Project_task_contact::select([
            'project_task_contacts.next_contact',
            'project_task_contacts.end',

            'project_tasks.id as task_id',

            'module_rules.id as module_rule_id',
            'module_rules.grouptasks_id',
            'grouptasks_references.name as grouptask',
            'module_rules.additional',
            'module_rules.number_contacts',
            'module_rules.task as module_task',

            'modules_references.id as module_id',
            'modules_references.name as module',

            'projects.id as project_id',
            'projects.name as project',

            'clients_references.id as client_id',
            'clients_references.name as client',

            'city_references.id as city_id',
            'city_references.name as city',

            'comment_tasks.text as comment',

        ])
            ->leftJoin('projects', 'projects.id', 'project_id')
            ->leftJoin('project_tasks', 'project_tasks.id', 'project_task_id')
            ->leftJoin('clients_references', 'clients_references.id', 'projects.client_id')
            ->leftJoin('city_references', 'city_references.id', 'clients_references.city_id')
            ->leftJoin('comment_tasks', 'comment_tasks.task_id', 'project_tasks.id')
            ->leftJoin('module_rules', 'module_rules.id', 'project_tasks.module_rule_id')
            ->leftJoin('grouptasks_references', 'grouptasks_references.id', 'module_rules.grouptasks_id')
            ->leftJoin('modules_references', 'modules_references.id', 'project_tasks.module_id')
            ->where('project_task_contacts.id', $id)
            ->first();
    }

    function save(Request $request) {
        $data = json_decode($request->data);

        $date = explode(' ', date("Y-m-d H:i:s"));
        $cur_user = Sentinel::getUser();

        if (!in_array($data->res, ['project', 'dop'])) {
            return response()->json(['status' => 'Error']);
        }
        try {

            if ($data->ptc_id > 0) {
                $projectTaskContact = Project_task_contact::find($data->ptc_id);

                $projectTask = $projectTaskContact->projectTask;
                $moduleRule = $projectTask->moduleRule;
                $comment = $projectTask->commentTask;
                $projectTaskDelete = false;
                if ($data->res == 'dop'){
                    if (!isset($moduleRule) || !$moduleRule->additional) {
                        $moduleRule = $this->createModuleRule($data);
                    } else {
                        $moduleRule->update([
                            'module_id' => $data->module->id,
                            'task' => $data->task->value,
                            'grouptasks_id' => $data->groupTask->id,
                            'additional' => 1,
                            'number_contacts' => $data->numCont,
                        ]);
                    }

                    $projectTask->update([
                        'module_rule_id' => $moduleRule->id,
                        'number_contacts' => $data->numCont,
                        'grouptasks_id' => $data->groupTask->id,
                        'module_id' => $data->module->id,
                    ]);
                    $newPtId = $projectTask->id;
                } else {
                    if ((!$projectTask->module_id && $projectTask->id != $data->task->id) || $moduleRule->additional){
                        $projectTaskDelete = true;
                    }
                    if (isset($moduleRule) && $moduleRule->additional && $projectTask->module_id){
                        $moduleRule->delete();
                    }
                    $newPtId = $data->task->id;
                }

                $projectTaskContact->update([
                    'next_contact' => $data->date . ' ' . $data->time,
                    'end' => $data->date . ' ' . $data->timeEnd,
                    'project_task_id' => $newPtId,
                ]);

                if ($projectTaskDelete){
                    $projectTask->delete();
                }

                if (!$comment){
                    $this->createComment($data, $cur_user, $date, $projectTask->id ?? $data->task->id);
                } else {
                    if ($comment->text != $data->comment) {
                        $comment->update([
                            'date' => $date[0],
                            'time' => $date[1],
                            'text' => $data->comment,
                        ]);
                    }
                }



            } else {
                if ($data->res == 'dop') {
                    $moduleRule = $this->createModuleRule($data);

                    $projectTask = Project_task::create([
                        'project_id' => $data->project->value,
                        'user_id' => $data->user_id,
                        'observer_id' => $cur_user->id,
                        'module_rule_id' => $moduleRule->id,
                        'number_contacts' => $data->numCont,
                        'grouptasks_id' => $data->groupTask->id,
                        'module_id' => $data->module->id,
                        'tasksstatus_ref_id' => 1,
                    ]);
                }

                $projectTaskContact = Project_task_contact::create([
                    'project_id' => $data->project->value,
                    'user_id' => $data->user_id,
                    'observer_id' => $cur_user->id,
                    'date_production' => date("Y-m-d H:i:s"),
                    'project_task_id' => $projectTask->id ?? $data->task->id,
                    'next_contact' => $data->date . ' ' . $data->time,
                    'end' => $data->date . ' ' . $data->timeEnd,
                ]);



                if ($data->comment != null) {
                    $this->createComment($data, $cur_user, $date, $projectTask->id ?? $data->task->id);
                }
            }

        } catch (\Exception $e){
            return response()->json(['status' => 'Error', 'msg' => $e->getMessage()]);
        }

        $projectTaskContact->load('projectTask');

        return response()->json(['status' => 'Ok', 'pt_id' => $projectTaskContact->projectTask->id]);
    }

    function createModuleRule($data){
        return Module_rule::create([
            'module_id' => $data->module->id,
            'task' => $data->task->value,
            'grouptasks_id' => $data->groupTask->id,
            'additional' => 1,
            'number_contacts' => $data->numCont,
        ]);
    }

    function createComment($data, $cur_user, $date, $ptId){
        if ($data->comment){
            return Comment_task::create([
                'project_id' => $data->project->value,
                'user_id' => $cur_user->id,
                'task_id' => $ptId,
                'date' => $date[0],
                'time' => $date[1],
                'text' => $data->comment,
            ]);
        } else return null;
    }

}