<?php


namespace App\Http\Controllers\Common;


trait ResponseTrait {

    function responseError($msg){
        return ['status' => 'Error', 'message' => $msg];
    }

    function responseOk($data = null){
        return ['status' => 'Ok', 'data' => $data];
    }

}