<?php


namespace App\Http\Controllers\Common\Search;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SearchController extends Controller {

    function get(Request $request, $table){
        return $this->getInstance($table)->get($request);
    }
    function config(Request $request, $table){
        return $this->getInstance($table)->config();
    }

    function excel(Request $request, $table){
        $ins = $this->getInstance($table);
        return Excel::get($ins->get($request)['data'], $request->columns ?? $ins->config()['columns'], $table);
    }

    /**
     * @param string $name
     * @return SearchInterface
     */
    private function getInstance($name){
        $names = explode('_', $name);
        $names = array_map(function ($n){return Str::ucfirst($n);}, $names);
        $name = 'App\Http\Controllers\Common\Search\\'.implode('\\', $names);
        return new $name();
    }

}