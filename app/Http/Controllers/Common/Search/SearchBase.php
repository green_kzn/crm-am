<?php


namespace App\Http\Controllers\Common\Search;


use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

abstract class SearchBase {

    protected string $dateColWidth = '80px';
    protected Request $request;
    protected Builder $query;
    protected $notAllowedToSort = [];
    protected $searchList = [];

    public function __construct() {
        $this->request = Request::capture();
        $this->query = $this->setQuery();
    }

    protected abstract function setQuery();

    protected function dateFormat(&$array, $fields, $format = 'd.m.Y'){
        foreach ($array as &$item) {
            $item = (array)$item;
            foreach ($fields as $field) {
                $item[$field] = Carbon::parse($item[$field])->format($format);
            }
        }
    }

    protected function handleField(&$array, $fields, $handler){
        foreach ($array as &$item) {
            $item = (array)$item;
            foreach ($fields as $field) {
                $item[$field] = $handler($item[$field]);
            }
        }
    }

    protected function appendField(&$array, $field, $handler){
        foreach ($array as &$item) {
            $item = (array)$item;
            $item[$field] = $handler($item);
        }
    }

    protected function order(){
        $request = $this->request;
        if (isset($request->sortBy) && $request->sortBy && !in_array($request->sortBy, $this->notAllowedToSort)) {
            $sortBy = $this->sortMap[$request->sortBy] ?? $request->sortBy;
            $this->query->orderBy($sortBy, !isset($request->sortType) || $request->sortType ? 'asc' : 'desc');
        }
    }

    protected function pagination(){
        $request = $this->request;
        if (isset($request->page) && isset($request->count) && $request->count > 0) {
            $this->query->forPage($request->page, $request->count);
        }

    }

    protected function filterSearch(){
        $request = $this->request;
        if (isset($request->search)) {
            $search = $request->search;
            $this->query->where(function ($query) use ($search) {
                foreach ($this->searchList as $item) {
                    $query->orWhere($item, 'like', "%$search%");
                }
            });
        }
    }

    protected function filterStatus($name = 'status'){
        $funcName = 'filter'.Str::ucfirst(Str::camel($name)).'Options';
        if (method_exists($this, $funcName)){
            $options = $this->{$funcName}();
            foreach ($this->request->filters[$name] ?? [] as $key => $val) {
                if ($val == 'true' && is_object($options[$key] ?? null)){
                    $options[$key]();
                }
            }
        }
    }
}