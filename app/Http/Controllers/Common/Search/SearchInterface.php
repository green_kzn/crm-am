<?php


namespace App\Http\Controllers\Common\Search;


use Illuminate\Http\Request;

interface SearchInterface {
    function get(Request $request);
    function config();
}