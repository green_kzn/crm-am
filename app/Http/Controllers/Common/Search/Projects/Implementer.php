<?php


namespace App\Http\Controllers\Common\Search\Projects;


use App\Http\Controllers\Common\RedmineController;
use App\Http\Controllers\Common\Search\SearchInterface;
use App\Modules_reference;
use App\Project;
use App\Project_task;
use App\Project_task_contact;
use App\ProjectWaitingList;
use App\User;
use App\WaitingList;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Sentinel;

class Implementer implements SearchInterface {

    private $sortMap = [
        'period' => 'projects.start_date',
        '90_day' => 'projects.start_date',
    ];

    private $notAllowedToSort = [
        'w_list', 'w_list_info'
    ];
    
    private $dateColWidth = '80px';

    function get(Request $request) {
        $query = Project::query();

        $query->select([
            'projects.id',
            'projects.name as project',
            'c.name as client',
            DB::raw('concat(u.last_name, \' \',u.first_name) as user'),
            'city.name as city',
            'projects.start_date',
            'projects.finish_date',
            'projects.done_datetime',
            'projects.done',
//            'wl.name as w_list',
//            'projects.waiting_list_ref_id',
            'pc.name as category'
        ])
            ->leftJoin(DB::raw('clients_references c'), 'c.id', 'projects.client_id')
            ->leftJoin(DB::raw('users u'), 'u.id', 'projects.user_id')
            ->leftJoin(DB::raw('role_users ru'), 'ru.user_id', 'u.id')
            ->leftJoin(DB::raw('city_references city'), 'city.id', 'c.city_id')
//            ->leftJoin(DB::raw('waiting_list_references wl'), 'wl.id', 'projects.waiting_list_ref_id')
            ->leftJoin(DB::raw('project_categories pc'),'pc.id','projects.category_id')
            ->whereNull('raw')
//            ->where('ru.role_id', 4)
        ;

        $this->filterStatus($query, $request);
        $this->filterDate($query, $request);
        $this->filterSearch($query, $request);
        $this->filterImplementer($query, $request);
        $this->filterModules($query, $request);
        $this->filterWList($query, $request);

        if (isset($request->sortBy) && !in_array($request->sortBy, $this->notAllowedToSort)) {
            $sortBy = $this->sortMap[$request->sortBy] ?? $request->sortBy;
            $query->orderBy($sortBy, !isset($request->sortType) || $request->sortType ? 'asc' : 'desc');
        }
        $query->orderBy('projects.waiting_list_ref_id')
            ->orderBy('done')
            ->orderBy('start_date');

        $count = count($query->get());

        if (isset($request->page) && isset($request->count) && $request->count > 0) {
            $query->forPage($request->page, $request->count);
        }

        $result = $query->get()->toArray();


        $ids = array_map(function ($i){return $i['id'];}, $result);

        $tasks = Project_task::whereIn('project_id', $ids)
            ->whereNull('deleted_at')
            ->get();

        $done = [];
        foreach ($tasks as $task) {
            if (!isset($done[$task->project_id])) $done[$task->project_id] = ['t'=>0,'d'=>0];
            $done[$task->project_id]['t']++;
            if ($task->tasksstatus_ref_id == 2){
                $done[$task->project_id]['d']++;
            }
        }

        $tasks_contacts = Project_task_contact::select([
            'user_id',
            'project_id',
//            DB::raw('max(prev_contact) as prev_contact'),
            DB::raw('min(next_contact) as next_contact'),
        ])
            ->whereIn('project_id', $ids)
            ->groupBy(['project_id', 'user_id'])
            ->whereNull('deleted_at')
            ->where('next_contact', '>=' , date('Y-m-d H:i:s'))
            ->get();

        $contacts = [];
        foreach ($tasks_contacts->toArray() as $i) {
            $contacts[$i['project_id']] = $i;
        }

        $this->getWList($result, $ids);

        $data = [];
        foreach ($result as $item) {
            $tasks = $done[$item['id']]['t'] ?? 0;
            $doneTasks = $done[$item['id']]['d'] ?? 0;

            $next_contact = null;
            $next_contact_url = '';
            if (isset($contacts[$item['id']])){
                $c = $contacts[$item['id']];
//                $next_contact = $c['next_contact'];
                $next_contact = substr($c['next_contact'],0,10);
                $next_contact_url = "/implementer/calendar/{$c['user_id']}/{$next_contact}";
            }


            $period = (new Carbon($item['start_date']))->diff(Carbon::now());
            if(!$period->invert)
                $period = ($period->y?$this->getYearString($period->y):'').($period->m?$period->m.' мес ':'').($period->d?$period->d.' дней ':'');
            else
                $period = '';

            $data[] = array_merge($item, [
                '90_day' => (new Carbon($item['start_date']))->addMonths(3)->format('d.m.Y'),
                'start_date' => (new Carbon($item['start_date']))->format('d.m.Y'),
                'finish_date' => (new Carbon($item['finish_date']))->format('d.m.Y'),
                'done_datetime' => !empty($item['done_datetime']) ? (new Carbon($item['done_datetime']))->format('d.m.Y H:i:s') : '',
                'period' => $period,
                'done' =>  ($tasks ? round($doneTasks / $tasks * 100) : 0).'%',
                'style' => $this->getStyle($item),
                'next_contact' => $next_contact ? (new Carbon($next_contact))->format('d.m.Y') : '',
                'next_contact_url' => $next_contact_url,
            ]);
        }


        return ['data' => $data, 'count' => $count];
    }

    function config() {
        $curUser = User::find(Sentinel::getUser()->id);
        $role = $curUser->role[0];

        $imp = array_map(function ($i) { return ['id' => $i['id'], 'label' => $i['last_name'] . ' ' . $i['first_name']]; }, User::getImplementers()->toArray());
        $modules = array_map(function ($i) { return ['id' => $i['id'], 'label' => $i['name']]; }, Modules_reference::all()->toArray());
        $wList = array_map(function ($i) { return ['id' => $i['id'], 'label' => $i['name']]; }, WaitingList::all()->toArray());

        return [
            'columns' => [

                ['label' => 'Название клиента', 'field' => 'client'],
                ['label' => 'Название проекта', 'field' => 'project'],
                ['label' => 'Внедренец', 'field' => 'user','width' => '15%'],
//                ['label' => 'Предыдущий контакт', 'field' => 'prev_contact',],
                ['label' => 'Следующий контакт', 'field' => 'next_contact','url' => true, 'target' => '_blank', 'width' => $this->dateColWidth],
                ['label' => 'Город', 'field' => 'city',],
                ['label' => 'Готовность', 'field' => 'done',],
//                ['label' => 'Активность', 'field' => 'activity',],
                ['label' => 'Дата начала', 'field' => 'start_date', 'width' => $this->dateColWidth],
                ['label' => '90 дней', 'field' => '90_day', 'width' => $this->dateColWidth],
                ['label' => 'Планируемая дата закрытия', 'field' => 'finish_date', 'width' => $this->dateColWidth],
                ['label' => 'Дата закрытия', 'field' => 'done_datetime', 'width' => $this->dateColWidth],
//                ['label' => 'Лист ожидания', 'field' => 'w_list',],
                ['label' => 'Срок внедрения проекта', 'field' => 'period',],
                ['label' => 'Категория', 'field' => 'category',],
                ['label' => 'Лист ожидания', 'field' => 'w_list'],
                ['label' => 'Лист ожидания инфо', 'field' => 'w_list_info', 'url' => true, 'target' => '_blank'],
            ],
            'actions' => [
                'pencil' => [
                    'to' => '/implementer/project/edit/'
                ]
            ],
            'nextPageNames' => ['Projects_edit'],
            'changeable_columns' => true,
            'hide_filters' => true,
            'excel' => true,
            'clearFilters' => true,
            'filters' => [
                'status_filters' => [
                    'status' => [
                        ['label' => 'Закрытые', 'value' => 'closed'],
                        ['label' => 'Не закрытые', 'value' => 'open', 'default' => true],
                        ['label' => 'Спящие', 'value' => 'sleeping'],
                        ['label' => 'Горящие', 'value' => 'hot'],
                        ['label' => 'Сегодня', 'value' => 'today'],
                    ],
                ],
                'dates' => [
                    'date_close' => [
                        'label' => 'Планируемая дата закрытия',
                        'label_date1' => 'c',
                        'label_date2' => 'по',
                        'default' => [
                            'date1' => Carbon::now()->firstOfMonth()->format('Y-m-d'),
                            'date2' => Carbon::now()->lastOfMonth()->format('Y-m-d'),
                        ],
                    ],
                    'date_start' => [
                        'label' => 'Дата начала',
                        'label_date1' => 'c',
                        'label_date2' => 'по',
                        'default' => [
                            'date1' => Carbon::now()->firstOfMonth()->format('Y-m-d'),
                            'date2' => Carbon::now()->lastOfMonth()->format('Y-m-d'),
                        ],
                    ],
                ],
                'list' => [
                    'implementer' => [
                        'data' => $imp,
                        'label' => 'Внедренец',
                        'default' => !$curUser->head && $role->id == 4 ? $curUser->id : 0
                    ]
                ],
                'multi_lists' => [
                    'modules' => [
                        'data' => $modules,
                        'label' => 'Модули',
                    ],
                    'w_list' => [
                        'data' => $wList,
                        'label' => 'Лист ожидания',
                    ]
                ]
            ]
        ];
    }

    /**
     * @param Builder $query
     * @param Request $request
     */
    private function filterStatus($query, $request){
        $todayStr = Carbon::today()->format('Y-m-d');

        if (isset($request->filters['status'])) {
            $status = $request->filters['status'];

            if (isset($status['closed']) && $status['closed'] == 'true') {;
                $query->where('projects.done', 1);
            }
            if (isset($status['open']) && $status['open'] == 'true') {;
                $query->where('projects.done', 0);
            }

            if (isset($status['today']) && $status['today'] == 'true') {
                $query->where('projects.done', 0)
                    ->leftJoin('project_task_contacts', 'projects.id', '=', 'project_task_contacts.project_id')
                    ->where('project_task_contacts.next_contact', '>=', $todayStr . ' 00:00:00')
                    ->where('project_task_contacts.next_contact', '<=', $todayStr . ' 23:59:59')
                    ->whereNull('project_task_contacts.deleted_at')
                    ->distinct();
            }
            if (isset($status['sleeping']) && $status['sleeping'] == 'true') {
                $date = Carbon::today()->subDays(14)->format('Y-m-d');
                $query->leftJoin('comments', 'projects.id', '=', 'comments.project_id')
                    ->groupBy(
                        'projects.id',
                        'projects.name',
                        'c.name',
                        'u.last_name',
                        'u.first_name',
                        'city.name',
                        'projects.start_date',
                        'projects.finish_date',
                        'projects.done_datetime',
                        'projects.done',
                        'pc.name'
                    )
                    ->selectRaw('max(`comments`.`date`)')
                    ->where('projects.done', 0)
                    ->havingRaw('max(`comments`.`date`) <= ?', [$date]);
            }
            if (isset($status['hot']) && $status['hot'] == 'true') {
                $date = Carbon::today()->addDays(10)->format('Y-m-d');
                $query->where('projects.finish_date', '>=', Carbon::today()->format('Y-m-d'))
                    ->where('projects.finish_date', '<=', $date)
                    ->where('projects.done', 0);
            }
        }
    }

    /**
     * @param Builder $query
     * @param Request $request
     */
    private function filterDate($query, $request){
        if (isset($request->filters['date_close']) && $request->filters['date_close']['checked'] == 'true'){
            $date = $request->filters['date_close'];
            $query->where('projects.finish_date', '>=', $date['date1'])
                ->where('projects.finish_date', '<=', $date['date2']);
        }
        if (isset($request->filters['date_start']) && $request->filters['date_start']['checked'] == 'true'){
            $date = $request->filters['date_start'];
            $query->where('projects.start_date', '>=', $date['date1'])
                ->where('projects.start_date', '<=', $date['date2']);
        }
    }

    /**
     * @param Builder $query
     * @param Request $request
     */
    private function filterSearch($query, $request){
        if (isset($request->search)) {
            $search = $request->search;
            $query->where(function ($query) use ($search) {
                $query
                    ->where('c.name', 'like', "%$search%")
                    ->orWhere('projects.name', 'like', "%$search%")
                    ->orWhere('city.name', 'like', "%$search%")
                    ->orWhere('u.last_name', 'like', "%$search%")
                    ->orWhere('u.first_name', 'like', "%$search%");
//                    ->orWhere('project_task_contacts.next_for_cont', 'like', "%$search%")
//                    ->orWhere('project_task_contacts.prev_contact', 'like', "%$search%");
            });
        }
    }
    /**
     * @param Builder $query
     * @param Request $request
     */
    private function filterImplementer($query, $request){
        if (isset($request->filters['implementer']) && $imp = $request->filters['implementer']) {
            $query->where('u.id', $imp);
        }
    }

    /**
     * @param Builder $query
     * @param Request $request
     */
    private function filterModules($query, $request){
        if (isset($request->filters['modules']) && !empty($request->filters['modules'])) {
            $modules = implode(', ', $request->filters['modules'] );
            $query->whereRaw("exists(select 1 from project_modules pm where pm.project_id = projects.id and pm.module_id in ($modules) limit 1)");
        }
    }

    private function filterWList($query, $request){
        if (isset($request->filters['w_list']) && !empty($request->filters['w_list'])) {
            $ids = implode(', ', $request->filters['w_list'] );
            $query->whereRaw("exists(select 1 from project_waiting_list pwl where pwl.project_id = projects.id and pwl.is_solved = 0 and pwl.waiting_list_id in ($ids) limit 1)");
        }
    }

    private function getStyle($item){
        $res = [];
        if ($item['done']){
            $res[] = 'text-decoration: line-through';
        }
        if ($item['w_list']){
            $res[] = 'background-color: #f5debf';
        }
        return implode(';', $res);
    }

    private function getYearString($y){
        if ($y == 1){
            $s = 'год ';
        } else if ( $y <= 4) {
            $s = 'года ';
        } else {
            $s = 'лет ';
        }
        return "$y $s";
    }

    private function getWList(&$result, $ids) {

        $allTemp = ProjectWaitingList::select('project_id', 'redmine_task_id', 'info', 'waiting_list_id')
            ->whereIn('project_id', $ids)
            ->where('is_solved', 0)
            ->with(['waitingList:id,name'])
            ->get();

        $all = [];
        foreach ($allTemp as $item) {
            $all[$item->project_id][] = $item;
        }

        foreach ($result as &$item) {
            $i = $all[$item['id']] ?? null;
            if ($i){
                $wLists = array_map(function ($a){ return $a->waitingList->name; }, $i);
                $infos = array_map(function ($a){ return $a->info.' '.$a->redmine_task_id; }, $i);
                $refs = array_map(function ($a){ return $a->redmine_task_id ? RedmineController::getUrl(true).'issues/'.$a->redmine_task_id : ''; }, $i);
                $refs = array_filter($refs, function ($a){return $a != null;});

                $item['w_list'] = implode(', ', $wLists);
                $item['w_list_info'] = implode(', ', $infos);
                if (count($refs)) $item['w_list_info_url'] = current($refs);
            } else {
                $item['w_list'] = '';
                $item['w_list_info'] = '';
            }

        }

    }
}