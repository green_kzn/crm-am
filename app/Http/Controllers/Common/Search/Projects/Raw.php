<?php


namespace App\Http\Controllers\Common\Search\Projects;


use App\Http\Controllers\Common\Search\SearchInterface;
use App\Modules_reference;
use App\Project;
use App\Project_task;
use App\Project_task_contact;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\DocBlock\Tags\Reference\Url;
use Sentinel;

class Raw implements SearchInterface {

    function get(Request $request) {
        $query = Project::query();

        $query->select([
            'projects.id',
            'projects.name as project',
            'c.name as client',
            'city.name as city',
            'projects.created_at',
        ])
            ->leftJoin(DB::raw('clients_references c'), 'c.id', 'projects.client_id')
            ->leftJoin(DB::raw('city_references city'), 'city.id', 'c.city_id')
            ->where('raw', 1)
        ;


        if (isset($request->sortBy)) {
            $sortBy = $this->sortMap[$request->sortBy] ?? $request->sortBy;
            $query->orderBy($sortBy, !isset($request->sortType) || $request->sortType ? 'asc' : 'desc');
        }

        $count = $query->count();

        if (isset($request->page) && isset($request->count) && $request->count > 0) {
            $query->forPage($request->page, $request->count);
        }

        $result = $query->get()->toArray();


        $data = [];
        foreach ($result as $item) {
            $data[] = array_merge($item, [
                'created_date' => Carbon::parse($item['created_at'])->format('d.m.Y H:i')
            ]);
        }


        return ['data' => $data, 'count' => $count];
    }

    function config() {

        return [
            'columns' => [

                ['label' => 'Название клиента', 'field' => 'client'],
                ['label' => 'Название проекта', 'field' => 'project'],
                ['label' => 'Город', 'field' => 'city',],
                ['label' => 'Дата создания', 'field' => 'created_date',],
            ],
            'actions' => [
                'pencil' => [
                    'to' => '/implementer/project/raw/'
                ]
            ],
            'nextPageNames' => ['Projects_raw'],
        ];
    }

}