<?php


namespace App\Http\Controllers\Common\Search;


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Excel {

    /**
     * @param array $data
     * @param array $columns
     * @param $name
     * @return string
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    static function get($data, $columns, $name) {

        $file = $name.'-'.date('Y-m-d H:i:s').'.xlsx';

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();


        $rowIterator = $sheet->getRowIterator();
        $columnIterator = $sheet->getColumnIterator();

        foreach ($columns as $col) {
            if ($col['visible']) {
                $sheet->setCellValue($columnIterator->key() . $rowIterator->key(), $col['label']);
                $columnIterator->next();
            }
        }
        $rowIterator->next();

        foreach ($data as $item) {
            $columnIterator->resetStart();

            foreach ($columns as $col) {
                if ($col['visible']) {
                    $sheet->setCellValue($columnIterator->key() . $rowIterator->key(), $item[$col['field']] ?? '');
                    $columnIterator->next();
                }
            }
            $rowIterator->next();
        }

        for ($i = 'A'; $i <= $sheet->getHighestColumn(); $i++) {
            $sheet->getColumnDimension($i)->setAutoSize(true);
        }

        $sheet->calculateColumnWidths();

        for ($i = 'A'; $i <= $sheet->getHighestColumn(); $i++) {
            $c = $sheet->getColumnDimension($i);
            if ($c->getWidth() > 50){
                $c->setAutoSize(false);
                $c->setWidth(50);
            }
        }

        $sheet->getStyle('A1:'.$sheet->getHighestColumn().'1')->getFont()->setBold(true)->setSize(13);

        $writer = new Xlsx($spreadsheet);

        header('Content-Disposition: attachment; filename="' . basename($file) . '"');

        $writer->save("php://output");

    }
}