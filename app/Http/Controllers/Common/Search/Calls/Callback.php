<?php

namespace App\Http\Controllers\Common\Search\Calls;

use App\Http\Controllers\Common\Search\SearchInterface;
use App\Project;
use App\Project_task_contact;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Callback implements SearchInterface {

    private $actions = [
        'working' => [
            'search' => [
                'to' => '/manager/callback/quality-edit/'
            ]
        ],
        'closing' => [
            'search' => [
                'to' => '/manager/callback/edit/'
            ]
        ],
        'feedback' => [
            'search' => [
                'to' => '/manager/request/edit/'
            ]
        ],
    ];

    function get(Request $request) {

        $query = Project::query();

        $date = Carbon::now()->subWeeks(2)->format("Y-m-d");

        $query->select([
                'projects.id',
//                'answer.id as a_id',
                'projects.name as project',
                'projects.request',
                'client.name as client',
                'city.name as city',
                'projects.quality_date',
                'done_datetime',
//                DB::raw('"" AS done_datetime'),
            ]
        )
//            ->leftJoin( DB::raw('callback_work_answers answer'), 'answer.project_id', 'projects.id')
            ->leftJoin(DB::raw('clients_references client'), 'client.id', 'projects.client_id')
            ->leftJoin(DB::raw('city_references city'), 'city.id', 'client.city_id');

        $actions = null;

        if (isset($request->filters['status'])) {
            $status = $request->filters['status'];
            $actions = $this->actions[$status] ?? null;
            if ($status == 'working') {
                $query->where('projects.done', 0)
                    ->where('projects.start_date', '<=', $date)
                    ->whereRaw('exists(select 1 from comments where project_id = projects.id and text is not null)');
                if (isset($request->filters['working-status'])){
                    $fStatus = $request->filters['working-status'];
                    if ($fStatus == 'done'){
                        $query->whereNotNull('projects.quality_date');
                    } elseif ($fStatus == 'other'){
                        $query->whereNull('projects.quality_date');
                    }
                }
            } else if ($status == 'closing') {
                $query->where('callback_status', 6);
            } else if ($status == 'feedback') {
                $query->where('projects.start_date', '<=', $date)
                    ->where('projects.done', 1);
                if (isset($request->filters['feedback-status'])){
                    $fStatus = $request->filters['feedback-status'];
                    if ($fStatus == 'done'){
                        $query->where('projects.request', 1);
                    } elseif ($fStatus == 'other'){
                        $query->whereNull('projects.request');
                    }
                }
            }
        }

        if (isset($request->search)) {
            $search = $request->search;
            $query->where(function ($query) use ($search) {
                $query->where('projects.id', 'like', "%$search%")
                    ->orWhere('projects.name', 'like', "%$search%")
                    ->orWhere('client.name', 'like', "%$search%")
                    ->orWhere('city.name', 'like', "%$search%")
                    ->orWhere('projects.quality_date', 'like', "%$search%")
                    ->orWhere('done_datetime', 'like', "%$search%");
            });
        }

        if (isset($request->sortBy)) {
            $query->orderBy($request->sortBy, $request->sortType ? 'asc' : 'desc');
        }

        $count = $query->count();

        if (isset($request->page) && isset($request->count) && $request->count > 0) {
            $query->forPage($request->page, $request->count);
        }

//        return $query->toSql();
        $data = $query->get();


        return ['data' => $data, 'count' => $count, 'actions' => $actions];
    }

    function config() {
        return [
            'columns' => [
                ['label' => 'Клиент', 'field' => 'client', 'width' => '20%'],
                ['label' => 'Наименование проекта', 'field' => 'project', 'width' => '25%'],
                ['label' => 'Последний контакт внедренца', 'field' => 'done_datetime', 'width' => '15%'],
                ['label' => 'С какими задачами работали', 'field' => 'tasks', 'width' => '10%'],
                ['label' => 'Последняя обратная связь', 'field' => 'quality_date', 'width' => '15%'],
                ['label' => 'Город', 'field' => 'city', 'width' => '10%'],
            ],
            'actions' => [
                'search' => [
                    'to' => '/manager/callback/quality-edit/'
                ]
            ],
            'nextPageNames' => ['manager_calls_edit','Manager_quality_edit','manager_callback_edit','manager_request_edit','manager_request_view'],
            'filters' => [
                'radio' => [
                    'status' => [
                        ['label' => 'На внедрении', 'value' => 'working'],
                        ['label' => 'Ожидают закрытия', 'value' => 'closing'],
                        ['label' => 'Сбор отзывов', 'value' => 'feedback'],
                    ],
                ],
                'radio_second' => [
                    'feedback-status' => [
                        'parent_field' => 'status',
                        'parent_value' => 'feedback',
                        'items' => [
                            ['label' => 'Все', 'value' => 'all'],
                            ['label' => 'Обработанные', 'value' => 'done'],
                            ['label' => 'Необработанные', 'value' => 'other'],
                        ]
                    ],
                    'working-status' => [
                        'parent_field' => 'status',
                        'parent_value' => 'working',
                        'items' => [
                            ['label' => 'Все', 'value' => 'all'],
                            ['label' => 'Обработанные', 'value' => 'done'],
                            ['label' => 'Необработанные', 'value' => 'other'],
                        ]
                    ]
                ]
            ],
            'excel' => true,
        ];
    }
}