<?php

namespace App\Http\Controllers\Common\Search\Calls;

use App\Http\Controllers\Common\Search\SearchInterface;
use App\Http\Controllers\Manager\CitysController as Citys;
use App\Http\Controllers\Manager\ClientsController as Clients;
use App\Http\Controllers\Manager\Projects2Controller as Projects;
use App\Http\Controllers\Manager\UserController as Users;
use App\Module_rule;
use App\Project_task;
use App\Project_task_contact;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Contact implements SearchInterface {

    private $sortMap = [
        'next_contact' => 'next_for_cont'
    ];

    function get(Request $request) {
        $query = Project_task_contact::query();

        $query->select([
            'project_task_contacts.*',
//            'project_tasks.id as id',
            'projects.name as project',
            'clients_references.name as client',
            'city_references.name as city',
            DB::raw('CONCAT(users.last_name, " ", users.first_name) AS user'),
            DB::raw('"" AS tasks'),
        ])
            ->leftJoin('projects', 'projects.id', 'project_id')
            ->leftJoin('project_tasks', 'project_tasks.id', 'project_task_id')
            ->leftJoin('clients_references', 'clients_references.id', 'projects.client_id')
            ->leftJoin('city_references', 'city_references.id', 'clients_references.city_id')
            ->leftJoin('users', 'users.id', 'project_task_contacts.user_id')
            ->whereNotNull('project_task_contacts.status')
            ->where('project_task_contacts.callbackstatus_ref_id', '1')
            ->where('projects.done', '0')
            ->where('project_task_contacts.next_for_cont', '<=', Carbon::today()->format('Y-m-d'));

        if (isset($request->search)) {
            $search = $request->search;
            $query->where(function ($query) use ($search) {
                $query->where('clients_references.name', 'like', "%$search%")
                    ->orWhere('projects.name', 'like', "%$search%")
                    ->orWhere('city_references.name', 'like', "%$search%")
                    ->orWhere('users.last_name', 'like', "%$search%")
                    ->orWhere('users.first_name', 'like', "%$search%")
                    ->orWhere('project_task_contacts.next_for_cont', 'like', "%$search%")
                    ->orWhere('project_task_contacts.prev_contact', 'like', "%$search%");
            });
        }

        if (isset($request->sortBy)) {
            $sortBy = $this->sortMap[$request->sortBy] ?? $request->sortBy;
            $query->orderBy($sortBy, $request->sortType ? 'asc' : 'desc');
        }

        $count = $query->count();

        if (isset($request->page) && isset($request->count) && $request->count > 0) {
            $query->forPage($request->page, $request->count);
        }

        $data = [];
        foreach ($query->get() as $pt) {
            $data[] = [
                'id' => $pt->id,
//                'name' => $name->task,
                'client' => $pt->client,
                'project' => $pt->project,
                'city' => $pt->city,
                'user' => $pt->user, //"$pt->last_name $pt->first_name",
                'prev_contact' => $pt->prev_contact ? substr($pt->prev_contact, 0, -3) : '',
                'next_contact' => $pt->next_for_cont ? substr($pt->next_for_cont, 0, -3) : ''
            ];
        }

        return ['data' => $data, 'count' => $count];
    }

    function config() {
        return [
            'columns' => [
                ['label' => 'Клиент', 'field' => 'client', 'width' => '20%'],
                ['label' => 'Проект', 'field' => 'project', 'width' => '20%'],
                ['label' => 'Город', 'field' => 'city', 'width' => '10%'],
                ['label' => 'Ответственный внедренец', 'field' => 'user', 'width' => '15%'],
                ['label' => 'Последний контакт', 'field' => 'prev_contact', 'width' => '10%'],
                ['label' => 'С какими задачами работали', 'field' => 'tasks', 'width' => '10%'],
                ['label' => 'Просили перезвонить', 'field' => 'next_contact', 'width' => '10%'],
            ],
            'actions' => [
                'search' => [
                    'to' => '/manager/calls/edit/'
                ]
            ],
            'nextPageNames' => ['manager_calls_edit'],
            'excel' => true,
//            'changeable_columns' => true,
        ];
    }
}