<?php


namespace App\Http\Controllers\Common\Search\Forms;


use App\Http\Controllers\Common\Search\SearchBase;
use App\Http\Controllers\Common\Search\SearchInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Raw extends SearchBase implements SearchInterface {

    protected $searchList = [
        'ppf.name',
        'ppf.updated_at',
        'p.name',
        'c.name',
        'u.last_name',
        'u.first_name',
        's.name',
    ];

    protected function setQuery() {
        return DB::table(DB::raw('project_print_forms ppf'));
    }

    function get(Request $request) {
        $query = $this->query;

        $query->select([
            'ppf.id',
            'ppf.name',
            'ppf.updated_at as created',
            'p.name as project',
            'c.name as client',
            DB::raw('concat(u.last_name, \' \',u.first_name) as observer'),
            's.name as status',
        ])
            ->leftJoin(DB::raw('projects p'), 'p.id', 'ppf.project_id')
            ->leftJoin(DB::raw('clients_references c'), 'c.id', 'p.client_id')
            ->leftJoin(DB::raw('users u'), 'u.id', 'ppf.observer_id')
            ->leftJoin(DB::raw('printform_status_references s'), 's.id', 'ppf.printform_status_ref_id')

            ->leftJoin(DB::raw('city_references city'), 'city.id', 'c.city_id')
            ->where('ppf.raw', 1)
            ->where('ppf.printform_status_ref_id', 5)
            ->whereRaw('(ppf.free = 1 OR ppf.paid = 1)')
            ->whereNull('ppf.deleted_at')
            ->whereNull('ppf.attendant')
        ;

        $this->filterSearch();

        $this->order();
        $count = $query->count();
        $this->pagination();
        $result = $query->get()->toArray();
        $this->dateFormat($result, ['created']);
        return ['data' => $result, 'count' => $count];
    }

    function config() {

        return [
            'columns' => [
                ['label' => 'Дата постановки', 'field' => 'created', 'width' => $this->dateColWidth],
                ['label' => 'Заказчик', 'field' => 'client', 'width' => '20%'],
                ['label' => 'Проект', 'field' => 'project', 'width' => '20%'],
                ['label' => 'Наименование работы', 'field' => 'name'],
                ['label' => 'Статус', 'field' => 'status'],
                ['label' => 'Внедренец', 'field' => 'observer'],
            ],
            'actions' => [
                'pencil' => [
                    'to' => '/print_form/forms/raw/'
                ]
            ],
            'nextPageNames' => ['print_form_raw', 'Print_form_attendant_edit', 'print_form_pay', 'print_form_edit'],
            'changeable_columns' => true,
        ];
    }

    function order() {
        if (empty($this->request->sortBy)) {
            $this->request->sortBy = 'created';
        }
        parent::order();
    }
}