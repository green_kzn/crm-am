<?php


namespace App\Http\Controllers\Common\Search\Forms;


use App\Http\Controllers\Common\Search\SearchInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class Hot extends FormBase implements SearchInterface {

    protected $searchList = [
        'ppf.id',
        'ppf.name',
        'ppf.updated_at',
        'ppf.number',
        'p.name',
        'c.name',
        'u.last_name',
        'u.first_name',
        's.name',
    ];

    protected function setQuery() {
        return DB::table(DB::raw('project_print_forms ppf'));
    }

    function get(Request $request) {
        $query = $this->query;

        $query->select([
            'ppf.id',
            'ppf.name',
            'ppf.number',
            'ppf.free',
            'ppf.max_hours',
            'ppf.date_implement_for_client as for_client',
            'ppf.updated_at as date_created',
            'ppf.date_implement',
            'ppf.date_implement_for_client',
            'ppf.real_date',
            'p.name as project',
            'c.name as client',
            DB::raw('concat(o.last_name, \' \',o.first_name) as observer'),
            DB::raw('concat(u.last_name, \' \',u.first_name) as user'),
            's.name as status',
        ])
            ->leftJoin(DB::raw('projects p'), 'p.id', 'ppf.project_id')
            ->leftJoin(DB::raw('clients_references c'), 'c.id', 'p.client_id')
            ->leftJoin(DB::raw('users o'), 'o.id', 'ppf.observer_id')
            ->leftJoin(DB::raw('users u'), 'u.id', 'ppf.user_id')
            ->leftJoin(DB::raw('printform_status_references s'), 's.id', 'ppf.printform_status_ref_id')

            ->whereNull('ppf.raw')
            ->whereNotIn('ppf.printform_status_ref_id', [7, 8, 9])
            ->whereNull('ppf.deleted_at')
            ->where('date_implement_for_client', '<=', Carbon::now()->addDays(3))
        ;

        $this->filterSearch();
        $this->filterStatus();

        $this->order();
        $count = $query->count();
        $this->pagination();
        $result = $query->get()->toArray();

        $this->dateFormat($result, ['date_created'], 'd.m.Y H:i');
        $this->dateFormat($result, ['date_implement', 'date_implement_for_client', 'real_date'], 'd.m.Y');
        $this->handleField($result, ['free'], function ($i){ return $i ? 'Нет' : 'Да'; });

        $this->appendField($result, 'style', function ($item){
            return $this->getStyle($item);
        });

        return ['data' => $result, 'count' => $count];
    }

    function config() {
        $config = parent::config();
        $config['actions'] = [
                'pencil' => [
                    'to' => '/print_form/forms/edit/'
                ],
//                'trash' => [
//                    'to' => '/print_form/forms/del/',
//                    'class' => 'btn-danger'
//                ],
        ];
        return $config;
    }



}