<?php


namespace App\Http\Controllers\Common\Search\Forms;


use App\Http\Controllers\Common\Search\SearchBase;
use App\Http\Controllers\Common\Search\SearchInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Attendant extends FormBase implements SearchInterface {

    protected $searchList = [
        'ppf.name',
        'ppf.updated_at',
        'ppf.number',
        'ppf.max_hours',
        'p.name',
        'c.name',
        'u.last_name',
        'u.first_name',
        's.name',
    ];

    protected function setQuery() {
        return DB::table(DB::raw('project_print_forms ppf'));
    }

    function get(Request $request) {
        $query = $this->query;

        $query->select([
            'ppf.id',
            'ppf.name',
            'ppf.number',
            'ppf.free',
            'ppf.max_hours',
            'ppf.updated_at as created',
            'p.name as project',
            'c.name as client',
            DB::raw('concat(o.last_name, \' \',o.first_name) as observer'),
            DB::raw('concat(u.last_name, \' \',u.first_name) as user'),
            's.name as status',
        ])
            ->leftJoin(DB::raw('projects p'), 'p.id', 'ppf.project_id')
            ->leftJoin(DB::raw('clients_references c'), 'c.id', 'p.client_id')
            ->leftJoin(DB::raw('users o'), 'o.id', 'ppf.observer_id')
            ->leftJoin(DB::raw('users u'), 'u.id', 'ppf.user_id')
            ->leftJoin(DB::raw('printform_status_references s'), 's.id', 'ppf.printform_status_ref_id')

            ->whereNull('ppf.raw')
            ->whereIn('ppf.printform_status_ref_id', [6,8])
            ->whereNull('ppf.deleted_at')
            ->where('ppf.attendant' ,1)
        ;

        $this->filterSearch();
        $this->filterStatus();

        $this->order();
        $count = $query->count();
        $this->pagination();
        $result = $query->get()->toArray();
        $this->dateFormat($result, ['created']);
        $this->handleField($result, ['free'], function ($i){ return $i ? 'Нет' : 'Да'; });
        return ['data' => $result, 'count' => $count];
    }

    function config() {

        return [
            'columns' => [
                ['label' => 'Номер задачи', 'field' => 'id'],
                ['label' => 'Очередь', 'field' => 'number'],

                ['label' => 'Заказчик', 'field' => 'client'],
                ['label' => 'Проект', 'field' => 'project'],
                ['label' => 'Наименование работы', 'field' => 'name'],
                ['label' => 'Макс. кол-во часов', 'field' => 'max_hours', 'width' => '60px'],
                ['label' => 'Внедренец', 'field' => 'observer'],

                ['label' => 'Дополнительная плата', 'field' => 'free'],
                ['label' => 'Дата постановки', 'field' => 'created', 'width' => $this->dateColWidth],

                ['label' => 'Статус', 'field' => 'status'],
                ['label' => 'Ответственный', 'field' => 'user'],
            ],
            'actions' => [
                'pencil' => [
                    'to' => '/print_form/forms/attendant/edit/'
                ]
            ],
            'nextPageNames' => ['print_form_raw', 'Print_form_attendant_edit', 'print_form_pay', 'print_form_edit'],
            'changeable_columns' => true,
            'filters' => [
                'status_filters' => [
                    'status' => [
                        ['label' => 'Все', 'value' => 'all'],
                        ['label' => 'Мои формы', 'value' => 'my'],
                        ['label' => 'Мои закрытые формы', 'value' => 'my_close'],
                    ],
                ],
            ],
        ];
    }

}