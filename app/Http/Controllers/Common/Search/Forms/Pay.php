<?php


namespace App\Http\Controllers\Common\Search\Forms;


use App\Http\Controllers\Common\Search\SearchInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class Pay extends FormBase implements SearchInterface {

    protected $searchList = [
        'ppf.name',
        'p.name',
        'c.name',
        'u.last_name',
        'u.first_name',
    ];

    protected function setQuery() {
        return DB::table(DB::raw('project_print_forms ppf'));
    }

    function get(Request $request) {
        $query = $this->query;

        $query->select([
            'ppf.id',
            'ppf.name',
            'p.name as project',
            'c.name as client',
            DB::raw('concat(o.last_name, \' \',o.first_name) as observer'),
        ])
            ->leftJoin(DB::raw('projects p'), 'p.id', 'ppf.project_id')
            ->leftJoin(DB::raw('clients_references c'), 'c.id', 'p.client_id')
            ->leftJoin(DB::raw('users o'), 'o.id', 'ppf.observer_id')
            ->leftJoin(DB::raw('users u'), 'u.id', 'ppf.user_id')
            ->leftJoin(DB::raw('printform_status_references s'), 's.id', 'ppf.printform_status_ref_id')

            ->whereIn('ppf.printform_status_ref_id', [5])
            ->where('ppf.free', 0)
            ->where('ppf.paid', 0)
        ;

        $this->filterSearch();

        $this->order();
        $count = $query->count();
        $this->pagination();
        $result = $query->get()->toArray();

        return ['data' => $result, 'count' => $count];
    }

    function config() {

        return [
            'columns' => [
                ['label' => 'Заказчик', 'field' => 'client'],
                ['label' => 'Проект', 'field' => 'project'],
                ['label' => 'Наименование работы', 'field' => 'name'],
                ['label' => 'Внедренец', 'field' => 'observer'],
            ],
            'actions' => [
                'pencil' => [
                    'to' => '/print_form/forms/pay/'
                ]
            ],
            'nextPageNames' => ['print_form_raw', 'Print_form_attendant_edit', 'print_form_pay', 'print_form_edit'],
        ];
    }

}