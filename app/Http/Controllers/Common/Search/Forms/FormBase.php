<?php


namespace App\Http\Controllers\Common\Search\Forms;


use App\Http\Controllers\Common\Search\SearchBase;
use Carbon\Carbon;
use Sentinel;

abstract class FormBase extends SearchBase {

    protected function filterStatusOptions(){
        return [
            'my' => function(){
                $this->query->where('ppf.user_id', Sentinel::getUser()->id)
                    ->where('printform_status_ref_id', '<>', 9);
            },
            'my_close' => function(){
                $this->query->where('ppf.user_id', Sentinel::getUser()->id)
                    ->where('printform_status_ref_id', 9);
            }
        ];
    }

    protected function getStyle($item){
        $res = [];
        try {
            $diff = Carbon::parse($item['for_client'])->diff(Carbon::now());
            $diff = $diff->invert ? -$diff->days : $diff->days;
            if ($diff > 0) {
                $res[] = 'background: #f9040436;';

            } else if ($diff >= -3) {
                $res[] = 'background-color: #f5debf';
            }
            return implode(';', $res);
        } catch (\Exception $e){
            dd($e);
            return '';
        }
    }

    function config() {

        return [
            'columns' => [
                ['label' => 'Номер задачи', 'field' => 'id'],
                ['label' => 'Очередь', 'field' => 'number'],

                ['label' => 'Заказчик', 'field' => 'client', 'width' => '90px'],
                ['label' => 'Проект', 'field' => 'project', 'width' => '90px'],
                ['label' => 'Наименование работы', 'field' => 'name'],
                ['label' => 'Макс. кол-во часов', 'field' => 'max_hours', 'width' => '60px'],
                ['label' => 'Внедренец', 'field' => 'observer'],

                ['label' => 'Дополнительная плата', 'field' => 'free'],
                ['label' => 'Дата постановки', 'field' => 'date_created', 'width' => $this->dateColWidth],
                ['label' => 'Дата реализации', 'field' => 'date_implement', 'width' => $this->dateColWidth],
                ['label' => 'Дата реализации для заказчика', 'field' => 'date_implement_for_client', 'width' => $this->dateColWidth],
                ['label' => 'Реальная дата реализации', 'field' => 'real_date', 'width' => $this->dateColWidth],

                ['label' => 'Статус', 'field' => 'status'],
                ['label' => 'Ответственный', 'field' => 'user'],

            ],

            'nextPageNames' => ['print_form_raw', 'Print_form_attendant_edit', 'print_form_pay', 'print_form_edit'],
            'changeable_columns' => true,
            'filters' => [
                'status_filters' => [
                    'status' => [
                        ['label' => 'Все', 'value' => 'all'],
                        ['label' => 'Мои формы', 'value' => 'my'],
                        ['label' => 'Мои закрытые формы', 'value' => 'my_close'],
                    ],
                ],
            ],
        ];
    }

}