<?php


namespace App\Http\Controllers\Common;


use App\Project_module;
use App\Project_task;
use App\Project_task_contact;
use Illuminate\Http\Request;

class ModuleController extends Controller {

    function delete(Request $request, $projectId, $moduleId){
        try {
            $projectModule = Project_module::where('project_id', $projectId)
                ->find($moduleId);

            if($request->withTasks){
                $tasks = Project_task::where('project_id', $projectId)
                    ->where('module_id', $projectModule->module_id)
                    ->get();

                foreach ($tasks as $task) {
                    $task->delete();
                }
                $projectModule->delete();
                return $this->responseOk();
            } else {
                $projectModule->delete();
                return $this->responseOk();
            }



        } catch (\Exception $e){
            return $this->responseError($e->getMessage());
        }

    }

}