<?php

namespace App\Http\Controllers\Print_form;

use Illuminate\Http\Request;
use Sentinel;
use Activation;
use Mail;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\PermissionsController as Permissions;
use App\Http\Controllers\Print_form\ProjectsController as Projects;
use App\Http\Controllers\Controller;
use App\User;
use App\Role_user;
use App\Role;
use DB;

class User2Controller extends Controller
{
    public function users() {
        if (session('perm')['user.view']) {
            $users = User::all();
            foreach ($users as $u) {
                $u['role'] = Role_user::getByUserId($u->id);
                if ($u->is_ban == 1)
                    $u['status'] = 1;
                else
                    $u['status'] = 0;
            }

            foreach ($users as $u) {
                if (Activation::completed($u))
                    $u['active'] = 1;
                else
                    $u['active'] = 0;
            }

            return view('Print_form.users.users', [
                'users' => $users,
            ]);
        } else {
            abort(503);
        }
    }

    public function switchStatus(Request $request) {
        if ($request->ajax()) {
            $user = User::where('id', '=', $request->id)->firstOrFail();
            if ($request->status == 1)
                $user->is_ban = 0;
            else
                $user->is_ban = 1;

            if ($user->save()) {
                $resp = [
                    'status' => 1
                ];
            } else {
                $resp = [
                    'status' => 0
                ];
            }

            return $resp;
        }
    }

    public function userDel($id) {
        if (session('perm')['user.delete']) {
            $user = Sentinel::findById($id);
            if ($user->delete()) {
                return redirect()->back()->with('success', 'Пользователь успешно удален');
            } else {
                return redirect()->back()->with('error', 'Произошла ошибка при удалении пользователя');
            }
        } else {
            abort(503);
        }
    }

    public function add() {
        if (session('perm')['user.create']) {
            $roles = Role::all();

            return view('Print_form.users.add', [
                'roles' => $roles,
            ]);
        } else {
            abort(503);
        }
    }

    public function userEdit($id) {
        if (session('perm')['user.update']) {
            $roles = Role::all();

            $u = User::find($id);

            return view('Print_form.users.edit', [
                'roles' => $roles,
                'u' => $u
            ]);
        } else {
            abort(503);
        }
    }

    //TODO Доделать загрузку фото
    public function upload($img) {
        $photoName = time().'.'.$img->getClientOriginalExtension();
        $foto = $img->move(public_path('avatars'), $photoName);

        return $photoName;
    }

    public function store(Request $request) {
        $credentials = [
            'email'    => $request->user_email,
            'password' => $request->user_pass,
            'last_name' => $request->user_surname,
            'first_name' => $request->user_name
        ];
        $user = Sentinel::register($credentials);
        if ($request->img) {
            $upload = $this->upload($request->file('img'));
            $new_user = User::where('id', '=', $user->id)->firstOrFail();
            $new_user->foto = $upload;
            $new_user->save();
        } else {
            $new_user = User::where('id', '=', $user->id)->firstOrFail();
            $new_user->foto = 'camera_200.png';
            $new_user->save();
        }

        Mail::send('emails.test', array('key' => 'value'), function($message)
        {
            $message->to('yamaletdinov.rusl@yandex.ru', 'Джон Смит')->subject('Привет!');
        });

        $activation = Activation::create($user);
        $role = Sentinel::findRoleById($request->user_role);
        $role->users()->attach($user);

        return redirect('/print_form/users')->with('success', 'Пользователь успешно добавлен');
    }

    public function userUpdate($id, Request $request) {
        dd($request->all());
    }

    public function getUserInfo($id) {
        dd(User::find($id));
    }

    static public function getAllUsers() {
        $users = User::whereNull('deleted_at')
            ->where('id', '!=', 0)
            ->get();
        return $users;
    }

    static public function getUserById ($id) {
        $user = User::where('id', '=', $id)
            ->whereNull('deleted_at')
            ->get();
        return $user[0];
    }

    public function getUserProjects(Request $request) {
        if ($request->ajax()) {
            $projects = Projects::getUserProjects($request->id);
            $tasks = Projects::getUserTasks($request->id);
            $allTimeTask = 0;
            foreach ($tasks as $t) {
                $allTimeTask += $t['number_contacts'];
            }

            return response()->json(['projects' => count($projects), 'tasks' => $allTimeTask]);
        }

    }

    public function getUser(Request $request) {
        if ($request->ajax()) {
            $users = User::whereNull('deleted_at')
                ->where('id', '!=', 0)
                ->get();
            return response()->json($users);
        }
    }

    public function getPFormUser() {
        $users = DB::select("select `u`.*
            from `users` `u`
            left join `role_users` `ru` on `u`.`id` = `ru`.`user_id`
            left join `roles` `r` on `ru`.`role_id` = `r`.`id`
            where  `r`.`name` = 'Отдел разработки форм'");

        return response()->json($users);
    }

    static public function getPFormUser2() {
        $users = DB::select("select `u`.*
            from `users` `u`
            left join `role_users` `ru` on `u`.`id` = `ru`.`user_id`
            left join `roles` `r` on `ru`.`role_id` = `r`.`id`
            where  `r`.`name` = 'Отдел разработки форм'");

        return response()->json($users);
    }
}
