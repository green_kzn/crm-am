<?php

namespace App\Http\Controllers\Print_form;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Settings;

class Settings2Controller extends Controller
{
    public function index() {
        $settings = Settings::whereNull('deleted_at')->get();

        foreach ($settings as $s) {
            $settings_ar[] = [
                'slug' => $s->slug,
                'value' => $s->value
            ];
        }
        //dd($settings_ar[1]['value']);
        
        return view('Print_form.settings.settings', [
            'settings' => $settings_ar,
            'production_calendar' => $settings_ar[1]['value']
        ]);
    }

    static public function getSettings($slug) {
        $settings = Settings::where('slug', '=', $slug)
            ->get();

        return $settings[0];
    }

    static public function saveSettings($array) {
        foreach ($array as $k => $ar) {
            try {
                $value = Settings::where('slug', '=', $k)
                    ->get();

                if (count($value) > 0) {
                    $value[0]->value = $ar;

                    if ($value[0]->save()) {
                        $success = true;
                    } else {
                        $success = false;
                    }
                } else {
                    $success = false;
                }

                $success = true;
            } catch (\Exception $e) {
                $success = false;
            }
        }

        return $success;
    }

    public function save(Request $request) {
        try {
            $settings = Settings::where('slug', '=', 'production_calendar')
                ->first();
            $settings->value = $request->link;
            $settings->save();

            $success = true;
        } catch (\Exception $e) {
            $success = false;
        }

        if ($success) {
            return redirect()->back()->with('success', 'Данные успешно сохранены');
        } else {
            return redirect()->back()->with('error', 'Произошла ошибка при сохранении данных');
        }
    }

    public function parse($xmlstring) {
        $xml = xml_parser_create();
        xml_parse_into_struct($xml, $xmlstring, $parsedSim);
        xml_parser_free($xml);
        unset($dataxml);
        //print_r($parsedSim);
        $parsedSim = $this->convertXarToPar($parsedSim);
        return $parsedSim;
    }

    public function convertXarToPar(&$parsed) {
        $data = array();
        $cursor = 0;
        $this->convertExplore($data, $parsed, $cursor);
        return $data;
    }

    public function convertExplore(&$data, &$parsed, &$cursor) {
        while (isset($parsed[$cursor])) {
            $v = &$parsed[$cursor ++];
            //print_r($v['type']);
            //type value analysis
            switch($v['type']) {
                case 'open':
                    $tag = $v['tag'];
                    $j = &$data[];
                    $j['tag'] = $tag;
                    $j['value'] = isset($v['value']) ? $v['value'] : '';
                    if (isset($v['attributes'])) $data += $v['attributes'];
                    $this->convertExplore($j, $parsed, $cursor);
                    break;
                case 'close':
                    return;
                case 'cdata':
                    if (empty($v['value']) || trim($v['value']) == '') break;
                    else {
                        $j = &$data[];
                        $j['value'] = trim($v['value']);
                    }
                    break;
                case 'complete':
                    $tag = $v['tag'];
                    $j = &$data[];

                    $j['tag'] = $tag;
                    $j['value'] = isset($v['value']) ? $v['value'] : '';

                    if (isset($v['attributes'])) $j += $v['attributes'];
                    break;
            }
        }
    }

    public function getWorkCalendar(Request $request) {
        if ($request->ajax()) {
            $myxmlfilecontent = file_get_contents($request->link);
            $xml_ar = $this->parse($myxmlfilecontent);

            foreach ($xml_ar[0] as $xml) {
                $data[] = [
                    'year' => $xml_ar['YEAR'],
                    'loaded_at' => $xml_ar['DATE'],
                    'holiday' => $xml,
                ];
            }

            /*switch ($xml_ar[0]['tag']) {
                case 'HOLIDAY':
                    $data[] = [
                        'year' => $xml_ar['YEAR'],
                        'loaded_at' => $xml_ar['DATE'],
                        'holiday' => $xml_ar[0],
                    ];
                    break;
                case 'DAYS':
                    $data[] = [
                        'year' => $xml_ar['YEAR'],
                        'loaded_at' => $xml_ar['DATE'],
                        'day' => $xml_ar[0],
                    ];
                    break;
            }*/

            return response()->json($xml_ar);
        }
    }
}
