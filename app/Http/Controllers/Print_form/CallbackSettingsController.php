<?php

namespace App\Http\Controllers\Print_form;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Print_form\Settings2Controller as Settings2;
use App\Callback_close_question;
use App\Callback_work_question;

class CallbackSettingsController extends Controller
{
    public function index() {
        if (session('perm')['callback_settings.view']) {
            $work_questions = Callback_work_question::whereNull('deleted_at')
                ->get();
            if (count($work_questions) > 0) {
                foreach ($work_questions as $wq) {
                    $work_q[] = [
                        'id' => $wq->id,
                        'question' => $wq->question
                    ];
                }
            } else {
                $work_q[] = '';
            }

            $close_questions = Callback_close_question::whereNull('deleted_at')
                ->get();
            if (count($close_questions) > 0) {
                foreach ($close_questions as $cq) {
                    $close_q[] = [
                        'id' => $cq->id,
                        'question' => $cq->question
                    ];
                }
            } else {
                $close_q[] = '';
            }

            $frequency_feedback = Settings2::getSettings('frequency_feedback');

            return view('Print_form.callback.callback', [
                'work_q' => $work_q,
                'close_q' => $close_q,
                'frequency_feedback' => $frequency_feedback->value
            ]);
        } else {
            abort(503);
        }
    }

    public function addWorkQuestions() {
        if (session('perm')['callback_settings.create']) {
            return view('Print_form.callback.addWorkQuestions');
        } else {
            abort(503);
        }
    }

    public function addCloseQuestions() {
        if (session('perm')['callback_settings.create']) {
            return view('Print_form.callback.addCloseQuestions');
        } else {
            abort(503);
        }
    }

    public function postAddWorkQuestions(Request $request) {
        if (session('perm')['callback_settings.create']) {
            if (!$request->question) {
                return redirect()->back()->with('error', 'Укажите вопрос');
            }

            $work_question = new Callback_work_question();
            $work_question->question = $request->question;

            if ($work_question->save()) {
                return redirect('/print_form/callback')->with('success', 'Данные успешно сохранены');
            } else {
                return redirect('/print_form/callback')->with('error', 'Произошла ошибка при сохранении данных');
            }
        } else {
            abort(503);
        }
    }

    public function postAddCloseQuestions(Request $request) {
        if (session('perm')['callback_settings.create']) {
            if (!$request->question) {
                return redirect()->back()->with('error', 'Укажите вопрос');
            }

            $close_question = new Callback_close_question();
            $close_question->question = $request->question;

            if ($close_question->save()) {
                return redirect('/print_form/callback')->with('success', 'Данные успешно сохранены');
            } else {
                return redirect('/print_form/callback')->with('error', 'Произошла ошибка при сохранении данных');
            }
        } else {
            abort(503);
        }
    }

    public function editWorkQuestions($id) {
        if (session('perm')['callback_settings.update']) {
            $work_question = Callback_work_question::where('id', '=', $id)
                ->whereNull('deleted_at')
                ->first();

            return view('Print_form.callback.editWorkQuestions', [
                'question' => $work_question
            ]);
        } else {
            abort(503);
        }
    }

    public function editCloseQuestions($id) {
        if (session('perm')['callback_settings.update']) {
            $work_question = Callback_close_question::where('id', '=', $id)
                ->whereNull('deleted_at')
                ->first();

            return view('Print_form.callback.editCloseQuestions', [
                'question' => $work_question
            ]);
        } else {
            abort(503);
        }
    }

    public function postEditWorkQuestions(Request $request, $id) {
        if (session('perm')['callback_settings.update']) {
            $work_question = Callback_work_question::where('id', '=', $id)
                ->whereNull('deleted_at')
                ->first();

            $work_question->question = $request->question;

            if ($work_question->save()) {
                return redirect('/print_form/callback')->with('success', 'Данные успешно сохранены');
            } else {
                return redirect('/print_form/callback')->with('error', 'Произошла ошибка при сохранении данных');
            }
        } else {
            abort(503);
        }
    }

    public function postEditCloseQuestions(Request $request, $id) {
        if (session('perm')['callback_settings.update']) {
            $work_question = Callback_close_question::where('id', '=', $id)
                ->whereNull('deleted_at')
                ->first();

            $work_question->question = $request->question;

            if ($work_question->save()) {
                return redirect('/print_form/callback')->with('success', 'Данные успешно сохранены');
            } else {
                return redirect('/print_form/callback')->with('error', 'Произошла ошибка при сохранении данных');
            }
        } else {
            abort(503);
        }
    }

    public function delWorkQuestions($id) {
        if (session('perm')['callback_settings.delete']) {
            $work_question = Callback_work_question::where('id', '=', $id)
                ->whereNull('deleted_at')
                ->first();

            $work_question->deleted_at = date("Y-m-d H:i:s");

            if ($work_question->save()) {
                return redirect()->back()->with('success', 'Данные успешно удалены');
            } else {
                return redirect()->back()->with('error', 'Произошла ошибка при удалении данных');
            }
        } else {
            abort(503);
        }
    }

    public function delCloseQuestions($id) {
        if (session('perm')['callback_settings.delete']) {
            $work_question = Callback_close_question::where('id', '=', $id)
                ->whereNull('deleted_at')
                ->first();

            $work_question->deleted_at = date("Y-m-d H:i:s");

            if ($work_question->save()) {
                return redirect()->back()->with('success', 'Данные успешно удалены');
            } else {
                return redirect()->back()->with('error', 'Произошла ошибка при удалении данных');
            }
        } else {
            abort(503);
        }
    }

    public function seveSettings(Request $request) {
        if ($request->frequency_feedback == null) {
            return redirect()->back()->with('error', 'Укажите периодичность сбора отзывов');
        }

        $rez = Settings2::saveSettings(['frequency_feedback' => $request->frequency_feedback]);
        if ($rez) {
            return redirect()->back()->with('success', 'Данные успешно обновлены');
        } else {
            return redirect()->back()->with('error', 'Произошла ошибка при сохранении данных');
        }
    }
}
