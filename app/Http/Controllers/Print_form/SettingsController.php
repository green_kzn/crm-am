<?php

namespace App\Http\Controllers\Print_form;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Settings;

class SettingsController extends Controller
{
    public function index() {
        $settings = Settings::whereNull('deleted_at')->get();

        return view('Print_form.settings.settings', [
            'settings' => $settings
        ]);
    }
}
