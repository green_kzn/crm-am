<?php

namespace App\Http\Controllers\Print_form;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact;
use App\Project_contact;
use App\Posts_reference;
use Sentinel;

class ContactController extends Controller
{
    public function add(Request $request) {
        if ($request->ajax()) {
            $cur_user = Sentinel::getUser();

            $contact = new Contact();
            $contact->first_name = $request->name;
            $contact->last_name = $request->last_name;
            $contact->patronymic = $request->patronymic;
            $contact->post = $request->post;
            $contact->phone = $request->phone;
            $contact->email = $request->email;
            $contact->project_id = $request->p_id;
            $contact->user_id = $cur_user->id;

            if ($contact->save()) {
                return redirect()->back()->with('success', 'Контакт успешно добавлен');
            } else {
                return redirect()->back()->with('error', 'Произошла ошибка при добавлении контакта');
            }
        }
    }

    public function del($id) {
        $contact = Contact::find($id);
        $contact->deleted_at = date("Y-m-d H:i:s");

        if ($contact->save()) {
            return redirect()->back()->with('success', 'Контакт успешно удален');
        } else {
            return redirect()->back()->with('error', 'Произошла ошибка при удалении контакта');
        }
    }

    static public function getProjectMainContact($pid) {
        $contact_id = Project_contact::where('project_id', '=', $pid)
            ->where('main', '=', 1)
            ->whereNull('deleted_at')
            ->first();

        if ($contact_id) {
            $contact = Contact::find($contact_id->contact_id);

            if (count($contact) > 0) {
                $res = (object)[
                    "id" => $contact['id'],
                    "first_name" => $contact['first_name'],
                    "last_name" => $contact['last_name'],
                    "patronymic" => $contact['patronymic'],
                    "post_id" => $contact['post_id'],
                    "phone" => $contact['phone'],
                    "email" => $contact['email'],
                    "project_id" => $contact['project_id'],
                    "mail_rep" => $contact['mail_rep'],
                    "user_id" => $contact['user_id'],
                    "created_at" => $contact['created_at'],
                    "updated_at" => $contact['updated_at'],
                    "deleted_at" => $contact['deleted_at']
                ];
            } else {
                $res = '';
            }
        } else {
            $res = false;
        }

        return $res;
    }

    static public function getProjectContacts($pid) {
        $contacts = Project_contact::where('project_id', '=', $pid)
            ->whereNull('deleted_at')
            ->get();

        foreach ($contacts as $c) {
            $contact = Contact::find($c->contact_id);
            $p_contact[] = [
                "id" => $contact['id'],
                "first_name" => $contact['first_name'],
                "last_name" => $contact['last_name'],
                "patronymic" => $contact['patronymic'],
                "post_id" => $contact['post_id'],
                "phone" => $contact['phone'],
                "email" => $contact['email'],
                "project_id" => $contact['project_id'],
                "mail_rep" => $contact['mail_rep'],
                "user_id" => $contact['user_id'],
                "created_at" => $contact['created_at'],
                "updated_at" => $contact['updated_at'],
                "deleted_at" => $contact['deleted_at']
            ];
        }

        return $p_contact;
    }

    public function update(Request $request, $id) {
        $contact = Contact::find($id);

        return view('Print_form.contacts.edit', [
            'contact' => $contact
        ]);
    }

    static public function getClientAllContacts($id) {
        $contacts = Contact::where('client_id', '=', $id)
            ->whereNull('deleted_at')
            ->get();

        return $contacts;
    }

    public function getClientsContact(Request $request) {
        if ($request->ajax()) {
            $contacts_q = Contact::where('client_id', '=', $request->id)
                ->whereNull('deleted_at')
                ->get();
            foreach ($contacts_q as $c) {
                $post = Posts_reference::find($c->post_id);
                $contacts[] = [
                    'id' => $c->id,
                    'first_name' => $c->first_name,
                    'last_name' => $c->last_name,
                    'patronymic' => $c->patronymic,
                    'phone' => $c->phone,
                    'email' => $c->email,
                    'main' => $c->main,
                    'mail_rep' => $c->mail_rep,
                    'post' => $post->name,
                ];
            }
            return response()->json($contacts);
        }
    }

    public function saveContact(Request $request) {
        if ($request->ajax()) {
            $contact = Contact::find($request->id);
            $contact->first_name = $request->name;
            $contact->last_name = $request->surname;
            $contact->patronymic = $request->patronymic;
            $contact->post_id = $request->post;
            $contact->phone = $request->phone;
            $contact->email = $request->email;
            $contact->email = $request->email;

            if($contact->save()) {
                return response()->json(['success' => 'Ok']);
            } else {
                return response()->json(['success' => 'Error']);
            }
        }
    }
}
