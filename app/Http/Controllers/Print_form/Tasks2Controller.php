<?php

namespace App\Http\Controllers\Print_form;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Print_form\EventController as EventController;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Print_form\GroupTasksController as GroupTasks;
use App\Http\Controllers\Print_form\Modules2Controller as Modules;
use App\Http\Controllers\Print_form\Contact2Controller as Contact;
use App\Http\Controllers\Print_form\ProjectsController as Projects;
use App\Http\Controllers\Print_form\User2Controller as Users;
use App\Http\Controllers\Print_form\ClientsController as Clients;
use App\Http\Controllers\Controller;
use Monolog\Handler\IFTTTHandler;
use Sentinel;
use App\Task;
use App\User;
use App\Project_task;
use App\Project_task_contact;
use App\Project_print_form;
use App\Printform_status_reference;
use App\Project;
use App\Project_module;
use App\Module_rule;
use App\Comment;
use App\Tasksstatus_reference;
use App\Clients_reference;
use App\Clarifying_question;
use App\Clarifying_answer;

class Tasks2Controller extends Controller
{
    public function index() {
        if (session('perm')['task.view']) {
            $cur_user = Sentinel::getUser();
            $res = '';

            $tasks = Task::where('contractor_id', '=', $cur_user->id)
                ->whereNULL('deleted_at')
                ->get();

            $p_tasks = Project_task::where('user_id', '=', $cur_user->id)
                //->whereNull('status')
                ->whereNull('deleted_at')
                ->get();

            if (count($p_tasks) > 0) {
                foreach ($p_tasks as $pt) {
                    $name = Module_rule::find($pt->module_rule_id);
                    $user = User::find($pt->observer_id);
                    $res[] = [
                        'name' => $name->task,
                        'created' => $pt->created_at,
                        'observer' => $user->last_name.' '.$user->first_name
                    ];
                }
            }


            if (count($tasks) > 0) {
                foreach ($tasks as $t) {
                    $user = User::find($t->observer_id);
                    $res[] = [
                        'name' => $t->title,
                        'created' => $t->created_at,
                        'observer' => $user->last_name.' '.$user->first_name,
                        'start_date' => $t->start_date,
                        'start_time' => $t->start_time,
                        'end_date' => $t->end_date,
                        'end_time' => $t->end_time,
                    ];
                }
            }
            //dd($res);

            return view('Print_form.tasks.tasks', [
                'tasks' => $res,
            ]);
        } else {
            abort(503);
        }
    }

    public function addTask() {
        if (session('perm')['task.create']) {

            return view('Print_form.tasks.add', [
                'users' => Users::getAllUsers(),
                'clients' => Clients::getAllClients(),
                'projects' => Projects::getAllProjects(),
            ]);
        } else {
            abort(503);
        }
    }

    public function time_add_min($time, $min) {
        list($h, $m) = explode(':', $time);
        $h = ($h + floor($m / 60)) % 24;
        $m = ($m + $min) % 60;
        return str_pad($h, 2, "0", STR_PAD_LEFT).':'.str_pad($m, 2, '0');
    }

    public function getTasks(Request $request) {
        if ($request->ajax()) {
            $cur_user = Sentinel::getUser();

            $project_task_contacts = Project_task_contact::where('user_id', '=', $cur_user->id)
                //->where('next_contact', '!=', NULL)
                ->whereNull('deleted_at')
                //->whereNull('status')
                ->get();

            if (count($project_task_contacts) > 0) {
                foreach ($project_task_contacts as $ptc) {
                    if ($ptc->next_contact) {
                        $p_name = Project::find($ptc->project_id);
                        $pt = Project_task::find($ptc->project_task_id);
                        $name = Module_rule::find($pt->module_rule_id);
                        $p_tasks[] = [
                            'id' => $pt->id,
                            'name' => $name['task']. "\r\n".$p_name['name'],
                            'p_name' => $p_name['name'],
                            'city' => $p_name['city'],
                            'duration' => $ptc->duration,
                            'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                            'callbackstatus_ref_id' => $ptc->callbackstatus_ref_id,
                            'next_contact' => $ptc->next_contact,
                            'prev_contact' => $ptc->prev_contact,
                        ];
                    } else {
                        $p_name = Project::find($ptc->project_id);
                        $pt = Project_task::find($ptc->project_task_id);
                        $name = Module_rule::find($pt->module_rule_id);
                        $p_tasks[] = [
                            'id' => $pt->id,
                            'name' => $name['task']. "\r\n".$p_name['name'],
                            'p_name' => $p_name['name'],
                            'city' => $p_name['city'],
                            'duration' => $ptc->duration,
                            'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                            'callbackstatus_ref_id' => $ptc->callbackstatus_ref_id,
                            'next_contact' => $ptc->prev_contact,
                            'prev_contact' => $ptc->prev_contact,
                        ];
                    }
                }

                foreach ($p_tasks as $p_task) {
                    $end = explode(' ', $p_task['next_contact']);

                    if (!$p_task['callbackstatus_ref_id']) {
                        switch ($p_task['tasksstatus_ref_id']) {
                            case '1':
                                $json[] = array(
                                    'id' => $p_task['id'],
                                    'title' => $p_task['name'],
                                    'start' => $p_task['next_contact'],
                                    'end' => $end[0] . ' ' . date('H:i:s', strtotime($end[1]) + strtotime($pt['duration']) - strtotime("00:00:00")),
                                    'allDay' => false,
                                    'color' => '#00a65a',
                                    'url' => '/print_form/project-task/edit/' . $p_task['id']
                                );
                                break;
                            case '2':
                                $json[] = array(
                                    'id' => $p_task['id'],
                                    'title' => $p_task['name'],
                                    'start' => $p_task['next_contact'],
                                    'end' => $end[0] . ' ' . date('H:i:s', strtotime($end[1]) + strtotime($pt['duration']) - strtotime("00:00:00")),
                                    'allDay' => false,
                                    'color' => '#f39c12',
                                    'url' => '/print_form/project-task/edit/' . $p_task['id']
                                );
                                break;
                            case '3':
                                $json[] = array(
                                    'id' => $p_task['id'],
                                    'title' => $p_task['name'],
                                    'start' => $p_task['next_contact'],
                                    'end' => $end[0] . ' ' . date('H:i:s', strtotime($end[1]) + strtotime($pt['duration']) - strtotime("00:00:00")),
                                    'allDay' => false,
                                    'color' => '#00c0ef',
                                    'url' => '/print_form/project-task/edit/' . $p_task['id']
                                );
                                break;
                        }
                    } else {
                        $json[] = array(
                            'id' => $p_task['id'],
                            'title' => $p_task['name'],
                            'start' => $p_task['next_contact'],
                            'end' => $end[0].' '.date('H:i:s',strtotime($end[1]) + strtotime($pt['duration'])-strtotime("00:00:00")),
                            'allDay' => false,
                            'color' => '#dd4b39',
                            'url' => '/print_form/project-task/edit/'.$p_task['id']
                        );
                    }

                    for($i = 1; $i <= date("t"); $i++) {
                        $weekend = date("w",strtotime($i.'.01.'.date("Y")));
                        if($weekend==0 || $weekend==6) {
                            if ($i<10) {
                                $json[] = [
                                    'start' => date("Y-01-").'0'.$i,
                                    'rendering' => 'background',
                                    'backgroundColor' => '#F00',
                                    'textColor' => '#000'
                                ];
                            } else {
                                $json[] = array(
                                    'start' => date("Y-01-").$i,
                                    'rendering' => 'background',
                                    'backgroundColor' => '#F00',
                                    'textColor' => '#000'
                                );
                            }
                        };
                    };

                    for($i = 1; $i <= date("t"); $i++) {
                        $weekend = date("w",strtotime($i.'.01.'.date("Y")));
                        if($weekend==0 || $weekend==6) {
                            if ($i<10) {
                                $json[] = [
                                    'start' => date("Y-01-").'0'.$i,
                                    'rendering' => 'background',
                                    'backgroundColor' => '#f4f4f4',
                                    'textColor' => '#000'
                                ];
                            } else {
                                $json[] = array(
                                    'start' => date("Y-01-").$i,
                                    'rendering' => 'background',
                                    'backgroundColor' => '#f4f4f4',
                                    'textColor' => '#000'
                                );
                            }
                        };
                    };
                }
            } else {
                //$json[] = '';

                for($i = 1; $i <= date("t"); $i++) {
                    $weekend = date("w",strtotime($i.'.02.'.date("Y")));
                    if($weekend==0 || $weekend==6) {
                        if ($i<10) {
                            $json[] = [
                                'start' => date("Y-022-").'0'.$i,
                                'rendering' => 'background',
                                'backgroundColor' => '#F00',
                                'textColor' => '#000'
                            ];
                        } else {
                            $json[] = array(
                                'start' => date("Y-02-").$i,
                                'rendering' => 'background',
                                'backgroundColor' => '#F00',
                                'textColor' => '#000'
                            );
                        }
                    };
                };

                for($i = 1; $i <= date("t"); $i++) {
                    $weekend = date("w",strtotime($i.'.01.'.date("Y")));
                    if($weekend==0 || $weekend==6) {
                        if ($i<10) {
                            $json[] = [
                                'start' => date("Y-01-").'0'.$i,
                                'rendering' => 'background',
                                'backgroundColor' => '#F00',
                                'textColor' => '#000'
                            ];
                        } else {
                            $json[] = array(
                                'start' => date("Y-01-").$i,
                                'rendering' => 'background',
                                'backgroundColor' => '#F00',
                                'textColor' => '#000'
                            );
                        }
                    };
                };
            }
        }

        return response()->json($json);
    }

    public function postAddEvent() {
        echo "Ok";
    }

    public function editTask($id) {
        if (session('perm')['task.update']) {
            $cur_task = Task::find($id);

            $cur_date = date("j").'.'.date("n").'.'.date("Y");
            $cur_date_val = date("Y").'-'.date("m").'-'.date("d");
            $tomorrow = date('Y-m-d', strtotime($cur_date . ' +1 day'));
            $tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +1 day'));
            $plus_one_day = date('Y-m-d', strtotime($cur_date . ' +2 day'));
            $plus_one_day_val = date('Y-m-d', strtotime($cur_date . ' +2 day'));
            $plus_two_day = date('d.m.Y', strtotime($cur_date . ' +3 day'));
            $plus_two_day_val = date('Y-m-d', strtotime($cur_date . ' +3 day'));
            $plus_three_day = date('d.m.Y', strtotime($cur_date . ' +4 day'));
            $plus_three_day_val = date('Y-m-d', strtotime($cur_date . ' +4 day'));

            return view('Print_form.tasks.edit', [
                'task' => $cur_task,
                'cur_date' => $cur_date,
                'cur_date_val' => $cur_date_val,
                'tomorrow' => $tomorrow,
                'tomorrow_val' => $tomorrow_val,
                'plus_one_day' => $plus_one_day,
                'plus_one_day_val' => $plus_one_day_val,
                'plus_two_day' => $plus_two_day,
                'plus_two_day_val' => $plus_two_day_val,
                'plus_three_day' => $plus_three_day,
                'plus_three_day_val' => $plus_three_day_val,
                'task_list' => GroupTasks::getAllTasks(),
                'task_status' => GroupTasks::getAllTasksStatus(),
                'modules' => Modules::getAllModule(),
                'group_task' => GroupTasks::getAllTaskGroup(),
            ]);
        } else {
            abort(503);
        }
    }

    public function editProjectTask($id) {
        if (session('perm')['task.update']) {
            $cur_p_task = Project_task::find($id);

            if ($cur_p_task) {
                $name = Module_rule::find($cur_p_task['module_rule_id']);
                $p_name = Project::find($cur_p_task['project_id']);

                if (count($cur_p_task) > 0) {
                    $p_tasks[] = [
                        'id' => $cur_p_task['id'],
                        'title' => $name['task'],
                        'p_name' => $p_name['name'],
                        'city' => $p_name['city'],
                        'start_date' => $cur_p_task['next_contact'],
                        'prev_contact' => $cur_p_task['prev_contact'],
                        'main_contact' => Contact::getProjectMainContact($cur_p_task['project_id']),
                        'project_contacts' => Contact::getProjectContacts($cur_p_task['project_id']),
                    ];
                } else {
                    $p_tasks = '';
                }
                //dd($p_tasks[0]);

                $p_tasks = (object)$p_tasks[0];

                $cur_date = date("j").'.'.date("n").'.'.date("Y");
                $cur_date_val = date("Y").'-'.date("m").'-'.date("d");
                $tomorrow = date('Y-m-d', strtotime($cur_date . ' +1 day'));
                $tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +1 day'));
                $plus_one_day = date('Y-m-d', strtotime($cur_date . ' +2 day'));
                $plus_one_day_val = date('Y-m-d', strtotime($cur_date . ' +2 day'));
                $plus_two_day = date('d.m.Y', strtotime($cur_date . ' +3 day'));
                $plus_two_day_val = date('Y-m-d', strtotime($cur_date . ' +3 day'));
                $plus_three_day = date('d.m.Y', strtotime($cur_date . ' +4 day'));
                $plus_three_day_val = date('Y-m-d', strtotime($cur_date . ' +4 day'));

                $module_id = Project_module::where('project_id', '=', $p_name['id'])
                    ->whereNull('deleted_at')
                    ->firstOrFail();

                $comments_q = Comment::where('project_id', '=', $cur_p_task->project_id)
                    ->whereNull('deleted_at')
                    ->orderBy('created_at', 'desc')
                    ->get();

                if (count($comments_q) > 0) {
                    foreach ($comments_q as $c) {
                        $user = User::find($c->user_id);
                        $comments[] = [
                            'text' => strip_tags($c->text),
                            'date' => $c->date,
                            'time' => $c->time,
                            'id' => $c->id,
                            'user_name' => $user->first_name,
                            'user_surname' => $user->last_name,
                            'user_foto' => $user->foto,
                        ];
                    }
                } else {
                    $comments = '';
                }

                //dd($comments);

                $project_task = Project_task::where('project_id', '=', $cur_p_task->project_id)
                    //->where('tasksstatus_ref_id', '<>', 2)
                    ->whereNull('deleted_at')
                    ->get();

                //dd($project_task);

                $task_status = GroupTasks::getAllTasksStatus();

                if (count($project_task) > 0) {
                    foreach ($project_task as $pt) {
                        $name = Module_rule::find($pt->module_rule_id);
                        $status = Tasksstatus_reference::find($pt->tasksstatus_ref_id);


                        $tasks[] = [
                            'id' => $pt->id,
                            'name' => $name->task,
                            'status_id' => $status['id'],
                            'status_name' => $status['name'],
                            'questions' => $name->questions,
                            'form' => 0
                        ];
                    }
                } else {
                    $tasks = '';
                }

                /*$project_form = Project_print_form::where('project_id', '=', $cur_p_task->project_id)
                    ->whereNull('deleted_at')
                    ->get();

                if (count($project_form) > 0) {
                    foreach ($project_form as $pf) {
                        $name = Module_rule::find($pf->module_rule_id);
                        $status = Printform_status_reference::find($pf->printform_status_ref_id);

                        $tasks[] = [
                            'id' => $pf->id,
                            'name' => $pf->name,
                            'status_id' => $status->id,
                            'status_name' => $status->name,
                            //'questions' => $name->questions,
                            'form' => 1
                        ];
                    }
                } else {
                    $tasks = '';
                }
                //dd($tasks);*/

                $project = Project::find($cur_p_task->project_id);
                $client = Clients_reference::find($project->client_id);

                $observer = Users::getAllUsers();
                foreach ($observer as $o) {
                    if ($o->id == $project->user_id) {
                        $observer_delault[] = [
                            'id' => $o->id,
                            'email' => $o->email,
                            'foto' => $o->foto,
                            'first_name' => $o->first_name,
                            'last_name' => $o->last_name,
                            'patronymic' => $o->last_name,
                            'default' => 1
                        ];
                    } else {
                        $observer_delault[] = [
                            'id' => $o->id,
                            'email' => $o->email,
                            'foto' => $o->foto,
                            'first_name' => $o->first_name,
                            'last_name' => $o->last_name,
                            'default' => 0
                        ];
                    }
                }

                return view('Print_form.tasks.edit', [
                    'task' => $p_tasks,
                    'cur_date' => $cur_date,
                    'cur_date_val' => $cur_date_val,
                    'tomorrow' => $tomorrow,
                    'tomorrow_val' => $tomorrow_val,
                    'plus_one_day' => $plus_one_day,
                    'plus_one_day_val' => $plus_one_day_val,
                    'plus_two_day' => $plus_two_day,
                    'plus_two_day_val' => $plus_two_day_val,
                    'plus_three_day' => $plus_three_day,
                    'plus_three_day_val' => $plus_three_day_val,
                    'task_list' => GroupTasks::getAllProjectTasks($module_id->module_id),
                    'task_status' => $task_status,
                    'modules' => Modules::getModulesByProjectId($cur_p_task->project_id),
                    'group_task' => GroupTasks::getAllTaskGroup(),
                    'comments' => $comments,
                    'tasks' => $tasks,
                    'client' => $client,
                    'observers' => $observer_delault,
                ]);
            } else {
                abort(404);
            }
        } else {
            abort(503);
        }
    }

    public function postEditProjectTask(Request $request) {
        switch ($request->res) {
            case 'work':
                $data = json_decode($request->data);
                $data = $data[0];

                if ($data->nextContact == 0) {
                    //dd($request->all());
                    DB::transaction(function () use ($request, $data) {
                        $project = Projects::getProjectByTaskId($request->id);
                        $date =  explode(' ', date("Y-m-d H:i:s"));
                        $cur_user = Sentinel::getUser();
                        try {
                            $comment = new Comment();
                            $comment->project_id = $project->id;
                            $comment->user_id = $cur_user->id;
                            $comment->for_client = NULL;
                            $comment->date = $date[0];
                            $comment->time = $date[1];
                            $comment->text = $request->comment6;
                            $comment->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }

                        try {
                            $comment_client = new Comment();
                            $comment_client->project_id = $project->id;
                            $comment_client->user_id = $cur_user->id;
                            $comment_client->for_client = 1;
                            $comment_client->date = $date[0];
                            $comment_client->time = $date[1];
                            $comment_client->text = $request->comment5;
                            $comment_client->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }

                        try {
                            $cur_task = Project_task::find($request->id);
                            $cur_task->tasksstatus_ref_id = $data->curTaskStatus;
                            $cur_task->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }

                        try {
                            $cur_task_contact = Project_task_contact::where('project_task_id', '=', $request->id)
                                ->whereNull('deleted_at')
                                ->get();
                            $cur_task_contact[0]->duration = $data->durationTime;
                            $cur_task_contact[0]->status = 1;
                            $cur_task_contact[0]->callbackstatus_ref_id = 1;
                            $cur_task_contact[0]->prev_contact = date("Y-m-d H:i:s");
                            $cur_task_contact[0]->next_contact = NULL;
                            $cur_task_contact[0]->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }

                        if (isset($data->otherTask)) {
                            foreach ($data->otherTask as $ot) {
                                //dd($ot->id);
                                try {
                                    $other_task = Project_task::find($ot->taskId);
                                    $other_task->tasksstatus_ref_id = $ot->status;
                                    $other_task->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }

                        if (isset($data->dopTask)) {
                            foreach ($data->dopTask as $dt) {
                                try {
                                    $dopTask = new Module_rule();
                                    $dopTask->task = $dt->name;
                                    $dopTask->module_id = $dt->module;
                                    $dopTask->number_contacts = $dt->numCont;
                                    $dopTask->grouptasks_id = $dt->group;
                                    $dopTask->additional = 1;
                                    $dopTask->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }

                                try {
                                    $newDopTask = new Project_task();
                                    $newDopTask->project_id = $project->id;
                                    $newDopTask->user_id = $cur_user->id;
                                    $newDopTask->observer_id = $cur_user->id;
                                    $newDopTask->module_rule_id = $dopTask->id;
                                    $newDopTask->number_contacts = $dt->numCont;
                                    $newDopTask->module_id = $dt->module;
                                    $newDopTask->tasksstatus_ref_id = 1;
                                    $newDopTask->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }

                        if (isset($data->cq)) {
                            foreach ($data->cq as $ca) {
                                try {
                                    $answer = new Clarifying_answer();
                                    $answer->question_id = $ca->id;
                                    $answer->project_id = $project->id;
                                    $answer->answer = $ca->answer;
                                    $answer->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }
                    });

                    if ($this->success) {
                        return redirect('/print_form/calendar')->with('success', 'Данные успешно сохранены');
                    } else {
                        return redirect()->back()->with('error', 'Произошла ошибка при обновлении статуса задачи');
                    }
                } else {
                    //dd(count($data->cq));
                    //dd($data);
                    DB::transaction(function () use ($request, $data) {
                        $project = Projects::getProjectByTaskId($request->id);
                        $date =  explode(' ', date("Y-m-d H:i:s"));
                        $cur_user = Sentinel::getUser();

                        if (isset($request->comment6)) {
                            try {
                                $comment = new Comment();
                                $comment->project_id = $project->id;
                                $comment->user_id = $cur_user->id;
                                $comment->for_client = NULL;
                                $comment->date = $date[0];
                                $comment->time = $date[1];
                                $comment->text = $request->comment6;
                                $comment->save();

                                $this->success = true;
                            } catch (\Exception $e) {
                                $this->success = false;
                            }
                        }

                        if (isset($request->comment5)) {
                            try {
                                $comment_client = new Comment();
                                $comment_client->project_id = $project->id;
                                $comment_client->user_id = $cur_user->id;
                                $comment_client->for_client = 1;
                                $comment_client->date = $date[0];
                                $comment_client->time = $date[1];
                                $comment_client->text = $request->comment5;
                                $comment_client->save();

                                $this->success = true;
                            } catch (\Exception $e) {
                                $this->success = false;
                            }
                        }

                        if (isset($data->otherTask)) {
                            foreach ($data->otherTask as $ot) {
                                //dd($ot->taskId);
                                try {
                                    $other_task = Project_task::find($ot->taskId);
                                    $other_task->tasksstatus_ref_id = $ot->status;
                                    //$other_task->prev_contact = date("Y-m-d H:i:s");
                                    $other_task->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }


                        if (count($data->dopTask) > 0) {
                            foreach ($data->dopTask as $dt) {
                                try {
                                    $dopTask = new Module_rule();
                                    $dopTask->task = $dt->name;
                                    $dopTask->module_id = $dt->module;
                                    $dopTask->number_contacts = $dt->numCont;
                                    $dopTask->grouptasks_id = $dt->group;
                                    $dopTask->additional = 1;
                                    $dopTask->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }

                                try {
                                    $newDopTask = new Project_task();
                                    $newDopTask->project_id = $project->id;
                                    $newDopTask->user_id = $cur_user->id;
                                    $newDopTask->observer_id = $cur_user->id;
                                    $newDopTask->module_rule_id = $dopTask->id;
                                    $newDopTask->number_contacts = $dt->numCont;
                                    $newDopTask->module_id = $dt->module;
                                    $newDopTask->tasksstatus_ref_id = 1;
                                    $newDopTask->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }

                        if (count($data->cq) > 0) {
                            foreach ($data->cq as $ca) {
                                try {
                                    $answer = new Clarifying_answer();
                                    $answer->question_id = $ca->id;
                                    $answer->project_id = $project->id;
                                    $answer->answer = $ca->answer;
                                    $answer->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }

                        if (isset($data->cqOtherTask)) {
                            foreach ($data->cqOtherTask as $ca2) {
                                try {
                                    $answer = new Clarifying_answer();
                                    $answer->question_id = $ca2->id;
                                    $answer->project_id = $project->id;
                                    $answer->answer = $ca2->answer;
                                    $answer->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }

                        try {
                            $newContTask = Project_task_contact::where('project_task_id', '=', $data->nextContTask)
                                ->whereNull('deleted_at')
                                ->get();

                            //dd($newContTask[0]);

                            if (count($newContTask) > 0) {
                                $newContTask[0]->next_contact = $data->nextContDateStart.' '.$data->nextContTimeStart;
                                $newContTask[0]->user_id = $data->nextContUser;
                                $newContTask[0]->prev_contact = date("Y-m-d H:i:s");
                                $newContTask[0]->save();
                            } else {
                                //dd($data->nextContUser);
                                $newContTask2 = new Project_task_contact();
                                $newContTask2->project_id = $project->id;
                                $newContTask2->user_id = $data->nextContUser;
                                $newContTask2->project_task_id = $data->nextContTask;
                                $newContTask2->duration = $data->nextContDuration;
                                $newContTask2->next_contact = $data->nextContDateStart.' '.$data->nextContTimeStart;
                                $newContTask2->prev_contact = date("Y-m-d H:i:s");

                                $newContTask2->save();
                            }

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }
                    });

                    try {
                        $cur_task = Project_task::find($request->id);
                        $cur_task->tasksstatus_ref_id = $data->curTaskStatus;
                        $cur_task->save();

                        $this->success = true;
                    } catch (\Exception $e) {
                        $this->success = false;
                    }

                    try {
                        $cur_task_contact = Project_task_contact::where('project_task_id', '=', $request->id)
                            ->whereNull('deleted_at')
                            ->get();
                        $cur_task_contact[0]->duration = $data->durationTime;
                        $cur_task_contact[0]->prev_contact = date("Y-m-d H:i:s");
                        $cur_task_contact[0]->save();

                        $this->success = true;
                    } catch (\Exception $e) {
                        $this->success = false;
                    }

                    if ($this->success) {
                        return redirect('/print_form/calendar')->with('success', 'Данные успешно сохранены');
                    } else {
                        return redirect()->back()->with('error', 'Произошла ошибка при перенесении задачи на другое время');
                    }
                }
                break;
            case 'next':
                DB::transaction(function () use ($request) {
                    $project = Projects::getProjectByTaskId($request->id);
                    $date =  explode(' ', date("Y-m-d H:i:s"));
                    $cur_user = Sentinel::getUser();
                    try {
                        $comment = new Comment();
                        $comment->project_id = $project->id;
                        $comment->user_id = $cur_user->id;
                        $comment->for_client = NULL;
                        $comment->date = $date[0];
                        $comment->time = $date[1];
                        $comment->text = $request->comment6;
                        $comment->save();

                        $this->success = true;
                    } catch (\Exception $e) {
                        $this->success = false;
                    }

                    try {
                        $comment_client = new Comment();
                        $comment_client->project_id = $project->id;
                        $comment_client->user_id = $cur_user->id;
                        $comment_client->for_client = 1;
                        $comment_client->date = $date[0];
                        $comment_client->time = $date[1];
                        $comment_client->text = $request->comment5;
                        $comment_client->save();

                        $this->success = true;
                    } catch (\Exception $e) {
                        $this->success = false;
                    }

                    $data = json_decode($request->data);
                    try {
                        $project_task = Project_task_contact::where('project_task_id', '=', $request->id)
                            ->whereNull('deleted_at')
                            ->get();
                        $project_task[0]->prev_contact = $project_task[0]->next_contact;
                        $project_task[0]->next_contact = $data[0]->date.' '.$data[0]->time;
                        $project_task[0]->save();

                        $this->success = true;
                    } catch (\Exception $e) {
                        $this->success = false;
                    }
                });

                if ($this->success) {
                    return redirect('/print_form/calendar')->with('success', 'Данные успешно сохранены');
                } else {
                    return redirect()->back()->with('error', 'Произошла ошибка при перенесении задачи на другое время');
                }
                break;
            case 'later':
                //dd($request->id);
                $cur_user = Sentinel::getUser();
                $project = Projects::getProjectByTaskId($request->id);
                $date =  explode(' ', date("Y-m-d H:i:s"));
                try {
                    $p_task = Project_task_contact::where('project_task_id', '=', $request->id)
                        ->whereNull('deleted_at')
                        ->get();
                    //dd($p_task[0]);
                    $p_task[0]->status = 1;
                    $p_task[0]->callbackstatus_ref_id = 1;
                    $p_task[0]->prev_contact = date("Y-m-d H:i:s");
                    $p_task[0]->next_contact = NULL;
                    $p_task[0]->save();

                    $comment = new Comment();
                    $comment->project_id = $project->id;
                    $comment->user_id = 0;
                    $comment->for_client = NULL;
                    $comment->date = $date[0];
                    $comment->time = $date[1];
                    $comment->text = 'До клиента не дозвонились';
                    $comment->save();

                    try {
                        $comment = new Comment();
                        $comment->project_id = $project->id;
                        $comment->user_id = $cur_user->id;
                        $comment->for_client = NULL;
                        $comment->date = $date[0];
                        $comment->time = $date[1];
                        $comment->text = $request->comment6;
                        $comment->save();

                        $this->success = true;
                    } catch (\Exception $e) {
                        $this->success = false;
                    }

                    try {
                        $comment = new Comment();
                        $comment->project_id = $project->id;
                        $comment->user_id = $cur_user->id;
                        $comment->for_client = NULL;
                        $comment->date = $date[0];
                        $comment->time = $date[1];
                        $comment->text = $request->comment5;
                        $comment->save();

                        $this->success = true;
                    } catch (\Exception $e) {
                        $this->success = false;
                    }

                    $this->success = true;
                } catch (\Exception $e) {
                    $this->success = false;
                }

                if ($this->success) {
                    return redirect('/print_form/calendar')->with('success', 'Данные успешно сохранены');
                } else {
                    return redirect()->back()->with('error', 'Произошла ошибка при перенесении задачи в клиентский отделл');
                }
                break;
        }
    }

    public function getTasksForAmcalendar(Request $request) {
        if ($request->ajax()) {
            $project_task_contact = Project_task_contact::where('user_id', '=', $request->user_id)
                ->where('next_contact', 'like', $request->date.'%')
                //->where('tasksstatus_ref_id', '!=', 2)
                ->whereNull('deleted_at')
                ->get();

            if (count($project_task_contact) > 0) {
                foreach ($project_task_contact as $ptc) {
                    $pt = Project_task::find($ptc->project_task_id);
                    $task_name = Module_rule::find($pt->module_rule_id);
                    $date = explode(' ', $ptc->next_contact);
                    $tasks[] = [
                        'id' => $ptc->id,
                        'time' => $date[1],
                        'task' => $task_name->task
                    ];
                }
            } else {
                $tasks[] = '';
            }

            return response()->json($tasks);
        }
    }

    static public function getTaskById($id) {
        $task = Project_task::whereNull('deleted_at')
            ->where('id', '=', $id)
            ->firstOrFail();

        return $task;
    }

    public function getClarificationQuestions(Request $request) {
        if ($request->ajax()) {
            $p_task = Project_task::find($request->tid);
            $c_question = Clarifying_question::where('task_id', '=', $p_task->module_rule_id)
                ->whereNull('deleted_at')
                ->get();

            if (count($c_question) > 0) {
                foreach ($c_question as $q) {
                    $answer = Clarifying_answer::where('question_id', '=', $q->id)
                        ->where('project_id', '=', $p_task->project_id)
                        ->whereNull('deleted_at')
                        ->first();
                    if ($answer) {
                        $question[] = [
                            'id' => $q->id,
                            'question' => $q->question,
                            'task_id' => $q->task_id,
                            'answer' => $answer->answer
                        ];
                    } else {
                        $question[] = [
                            'id' => $q->id,
                            'question' => $q->question,
                            'task_id' => $q->task_id,
                            'answer' => ''
                        ];
                    }
                }
            } else {
                $question[] = '';
            }

            return response()->json($question);
        }
    }

    public function postAddTask(Request $request) {
        switch ($request->typeTask) {
            case 'single_task':
                dd($request->all());
                break;
            case 'introduction':
                dd($request->all());
                break;
        }
    }

    public function getTasksByDateTime(Request $request) {
        if ($request->ajax()) {
            $count = Project_task_contact::where('end', '>', $request->date.' '.$request->time)
                //->where('tasksstatus_ref_id', '!=', 2)
                ->whereNull('deleted_at')
                ->count();

            return response()->json($count);
        }
    }

    public function getStatus(Request $request) {
        if ($request->ajax()) {
            $status = Tasksstatus_reference::whereNull('deleted_at')->get();
            return response()->json($status);
        }
    }

    public function getWeekends(Request $request) {
        if ($request->ajax()) {
            for($i = 1; $i <= date("t"); $i++) {
                $weekend = date("w",strtotime($i.date(".m.Y")));
                if($weekend==0 || $weekend==6) {
                    if ($i<10) {
                        $weekends[] = [
                            'date' => date("Y-m-").'0'.$i
                        ];
                    } else {
                        $weekends[] = [
                            'date' => date("Y-m-").$i
                        ];
                    }
                }
            }

            return response()->json($weekends);
        }
    }
}
