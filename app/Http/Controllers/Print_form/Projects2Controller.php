<?php

namespace App\Http\Controllers\Print_form;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Print_form\Comment2Controller as Comments;
use App\Http\Controllers\Print_form\User2Controller as Users2;
use App\Http\Controllers\Print_form\Clients2Controller as Clients;
use App\Http\Controllers\Print_form\CitysController as Citys;
use App\Http\Controllers\Print_form\AMDateTimeController as AMDateTime;
use App\Http\Controllers\Print_form\GroupTasksController as GroupTasks;
use App\Http\Controllers\Print_form\Modules2Controller as Modules;
use App\Http\Controllers\Print_form\Documents2Controller as Documents;
use App\Project;
use App\Comment;
use App\Project_contact;
use App\Project_module;
use App\Project_task;
use App\Project_print_form;
use App\Project_task_contact;
use App\Project_additional_task;
use App\Projects_doc;
use App\Contact;
use App\Posts_reference;
use App\User;
use App\Modules_reference;
use App\Module_rule;
use App\Tasksstatus_reference;
use App\Grouptasks_reference;
use App\Clients_reference;
use App\Clarifying_question;
use App\Clarifying_answer;
use Sentinel;
use Event;
use App\Events\onAddProjectEvent;
use App\Notifications\WorkoutAssigned;
use Notification;
use Auth;
use App\Notifications\ProjectTask;

class Projects2Controller extends Controller
{
    public function index() {
        if (session('perm')['project.view']) {
            $cur_user = Sentinel::getUser();
            $projects_q = Project::where('user_id', '=', $cur_user->id)
                ->whereNULL('deleted_at')
                //->whereNULL('callback_status')
                ->get();

            $cur_date = date("j").'.'.date("n").'.'.date("Y");
            $cur_date_val = date("Y").'-'.date("m").'-'.date("d");
            $tomorrow = date('Y-m-d', strtotime($cur_date . ' +1 day'));
            $tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +1 day'));
            $plus_one_day = date('Y-m-d', strtotime($cur_date . ' +2 day'));
            $plus_one_day_val = date('Y-m-d', strtotime($cur_date . ' +2 day'));
            $plus_two_day = date('d.m.Y', strtotime($cur_date . ' +3 day'));
            $plus_two_day_val = date('Y-m-d', strtotime($cur_date . ' +3 day'));
            $plus_three_day = date('d.m.Y', strtotime($cur_date . ' +4 day'));
            $plus_three_day_val = date('Y-m-d', strtotime($cur_date . ' +4 day'));

            if (count($projects_q) > 0) {
                foreach ($projects_q as $p) {
                    $task = Project_task::where('project_id', '=', $p->id)
                        ->where('user_id', '=', $cur_user->id)
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($task) > 0) {
                        $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                            ->whereNull('deleted_at')
                            ->get();

                        if (count($ptc) > 0) {
                            $client = Clients::getClientById($p->client_id);
                            $city = Citys::getCityById($client->city_id);

                            foreach ($task as $t) {
                                if ($t->tasksstatus_ref_id == 2) {
                                    $done2[$p->id][] = $t->id;
                                }
                            }

                            if (isset($done2[$p->id])) {
                                $user = Users2::getUserById($p->user_id);
                                $client = Clients::getClientById($p->client_id);
                                $projects[] = [
                                    'id' => $p->id,
                                    'close' => $p->done,
                                    'user' => $user->last_name . ' ' . $user->first_name,
                                    'name' => $p->name,
                                    'client' => $client->name,
                                    'city' => $city,
                                    'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                    'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                    'start_date' => $p->start_date,
                                    'finish_date' => $p->finish_date,
                                    'created_at' => $p->created_at,
                                    'active' => $p->active,
                                    'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                ];
                            } else {
                                $user = Users2::getUserById($p->user_id);
                                $client = Clients::getClientById($p->client_id);
                                $projects[] = [
                                    'id' => $p->id,
                                    'close' => $p->done,
                                    'user' => $user->last_name . ' ' . $user->first_name,
                                    'name' => $p->name,
                                    'client' => $client->name,
                                    'city' => $city,
                                    'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                    'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                    'start_date' => $p->start_date,
                                    'finish_date' => $p->finish_date,
                                    'created_at' => $p->created_at,
                                    'active' => $p->active,
                                    //'done' => 0,
                                    'done' => round(((0 / count($task)) * 100), 2),
                                ];
                            }
                        } else {
                            $client = Clients::getClientById($p->client_id);
                            $city = Citys::getCityById($client->city_id);
                            //dd($client);
                            foreach ($task as $t) {
                                if ($t->tasksstatus_ref_id == 2) {
                                    $done2[$p->id][] = $t->id;
                                }
                            }

                            if (isset($done2[$p->id])) {
                                $user = Users2::getUserById($p->user_id);
                                $client = Clients::getClientById($p->client_id);
                                $projects[] = [
                                    'id' => $p->id,
                                    'close' => $p->done,
                                    'user' => $user->last_name . ' ' . $user->first_name,
                                    'name' => $p->name,
                                    'client' => $client->name,
                                    'city' => $city,
                                    'prev_contact' => NULL,
                                    'next_contact' => NULL,
                                    'start_date' => $p->start_date,
                                    'finish_date' => $p->finish_date,
                                    'created_at' => $p->created_at,
                                    'active' => $p->active,
                                    'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                ];
                            } else {
                                $user = Users2::getUserById($p->user_id);
                                $client = Clients::getClientById($p->client_id);
                                $projects[] = [
                                    'id' => $p->id,
                                    'close' => $p->done,
                                    'user' => $user->last_name . ' ' . $user->first_name,
                                    'name' => $p->name,
                                    'client' => $client->name,
                                    'city' => $city,
                                    'prev_contact' => NULL,
                                    'next_contact' => NULL,
                                    'start_date' => $p->start_date,
                                    'finish_date' => $p->finish_date,
                                    'created_at' => $p->created_at,
                                    'active' => $p->active,
                                    //'done' => 0,
                                    'done' => round(((0 / count($task)) * 100), 2),
                                ];
                            }
                        }
                    } else {
                        $task = Project_print_form::where('project_id', '=', $p->id)
                            ->where('user_id', '=', $cur_user->id)
                            ->whereNull('deleted_at')
                            ->get();

                        $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                            ->whereNull('deleted_at')
                            ->get();

                        if (count($ptc) > 0) {
                            $client = Clients::getClientById($p->client_id);
                            $city = Citys::getCityById($client->city_id);

                            foreach ($task as $t) {
                                if ($t->tasksstatus_ref_id == 2) {
                                    $done2[$p->id][] = $t->id;
                                }
                            }

                            if (isset($done2[$p->id])) {
                                $user = Users2::getUserById($p->user_id);
                                $client = Clients::getClientById($p->client_id);
                                $projects[] = [
                                    'id' => $p->id,
                                    'close' => $p->done,
                                    'user' => $user->last_name . ' ' . $user->first_name,
                                    'name' => $p->name,
                                    'client' => $client->name,
                                    'city' => $city,
                                    'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                    'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                    'start_date' => $p->start_date,
                                    'finish_date' => $p->finish_date,
                                    'created_at' => $p->created_at,
                                    'active' => $p->active,
                                    'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                ];
                            } else {
                                $user = Users2::getUserById($p->user_id);
                                $client = Clients::getClientById($p->client_id);
                                $projects[] = [
                                    'id' => $p->id,
                                    'close' => $p->done,
                                    'user' => $user->last_name . ' ' . $user->first_name,
                                    'name' => $p->name,
                                    'client' => $client->name,
                                    'city' => $city,
                                    'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                    'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                    'start_date' => $p->start_date,
                                    'finish_date' => $p->finish_date,
                                    'created_at' => $p->created_at,
                                    'active' => $p->active,
                                    //'done' => 0,
                                    'done' => round(((0 / count($task)) * 100), 2),
                                ];
                            }
                        } else {
                            $client = Clients::getClientById($p->client_id);
                            $city = Citys::getCityById($client->city_id);
                            //dd($client);
                            foreach ($task as $t) {
                                if ($t->tasksstatus_ref_id == 2) {
                                    $done2[$p->id][] = $t->id;
                                }
                            }

                            if (isset($done2[$p->id])) {
                                $user = Users2::getUserById($p->user_id);
                                $client = Clients::getClientById($p->client_id);
                                $projects[] = [
                                    'id' => $p->id,
                                    'close' => $p->done,
                                    'user' => $user->last_name . ' ' . $user->first_name,
                                    'name' => $p->name,
                                    'client' => $client->name,
                                    'city' => $city,
                                    'prev_contact' => NULL,
                                    'next_contact' => NULL,
                                    'start_date' => $p->start_date,
                                    'finish_date' => $p->finish_date,
                                    'created_at' => $p->created_at,
                                    'active' => $p->active,
                                    'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                ];
                            } else {
                                $user = Users2::getUserById($p->user_id);
                                $client = Clients::getClientById($p->client_id);
                                $projects[] = [
                                    'id' => $p->id,
                                    'close' => $p->done,
                                    'user' => $user->last_name . ' ' . $user->first_name,
                                    'name' => $p->name,
                                    'client' => $client->name,
                                    'city' => $city,
                                    'prev_contact' => NULL,
                                    'next_contact' => NULL,
                                    'start_date' => $p->start_date,
                                    'finish_date' => $p->finish_date,
                                    'created_at' => $p->created_at,
                                    'active' => $p->active,
                                    //'done' => 0,
                                    'done' => round(((0 / count($task)) * 100), 2),
                                ];
                            }
                        }
                    }
                }
            } else {
                $projects = '';
            }


            $project_tasks = Project_task::where('user_id', '=', $cur_user->id)
                ->whereNull('deleted_at')
                ->whereIn('tasksstatus_ref_id', [1,3])
                ->get();

            if (count($project_tasks) > 0) {
                foreach ($project_tasks as $pt) {
                    $name = Module_rule::find($pt->module_rule_id);
                    $p_name = Project::find($pt->project_id);
                    $p_client = Clients::getClientById($p_name['client_id']);
                    $p_city = Citys::getCityById($p_client->city_id);
                    $ptc = Project_task_contact::where('project_task_id', '=', $pt->id)
                        ->whereNull('deleted_at')
                        ->first();
                    $user = User::whereNull('deleted_at')->get();
                    foreach ($user as $u) {
                        if ($u->id == $p_name->user_id) {
                            $observer[] = [
                                'default' => 1,
                                'id' => $u->id,
                                'first_name' => $u->first_name,
                                'last_name' => $u->last_name
                            ];
                        } else {
                            $observer[] = [
                                'default' => 0,
                                'id' => $u->id,
                                'first_name' => $u->first_name,
                                'last_name' => $u->last_name
                            ];
                        }
                    }

                    if (count($ptc) > 0) {
                        if ($pt['prev_contact'] != null) {
                            $p_tasks[] = [
                                'id' => $pt['id'],
                                'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                'client' => $p_client->name,
                                'name' => $name->task,
                                'observer' => $observer,
                                'p_name' => $p_name['name'],
                                'city' => $p_city,
                                'next_contact' => substr($ptc['next_contact'], 0, 16),
                                'prev_contact' => substr($pt['prev_contact'], 0, 16),
                            ];
                        } else {
                            $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                                ->whereNull('deleted_at')
                                //->whereNull('status')
                                ->max('prev_contact');

                            $p_tasks[] = [
                                'id' => $pt['id'],
                                'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                'client' => $p_client->name,
                                'name' => $name->task,
                                'observer' => $observer,
                                'p_name' => $p_name['name'],
                                'city' => $p_city,
                                'next_contact' => substr($ptc['next_contact'], 0, 16),
                                'prev_contact' => substr($max_date, 0, 16),
                            ];
                        }
                    } else {
                        if ($pt['prev_contact'] != null) {
                            $p_tasks[] = [
                                'id' => $pt['id'],
                                'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                'client' => $p_client->name,
                                'name' => $name->task,
                                'observer' => $observer,
                                'p_name' => $p_name['name'],
                                'city' => $p_city,
                                'next_contact' => null,
                                'prev_contact' => substr($pt['prev_contact'], 0, 16),
                            ];
                        } else {
                            $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                                ->whereNull('deleted_at')
                                //->whereNull('status')
                                ->max('prev_contact');

                            $p_tasks[] = [
                                'id' => $pt['id'],
                                'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                'client' => $p_client->name,
                                'name' => $name->task,
                                'observer' => $observer,
                                'p_name' => $p_name['name'],
                                'city' => $p_city,
                                'next_contact' => null,
                                'prev_contact' => substr($max_date, 0, 16),
                            ];
                        }
                    }
                }
            } else {
                $p_tasks[] = '';
                $observer2[] = '';
            }

            //dd($p_tasks);


            $p_id = Project::where('user_id', '=', $cur_user->id)
                ->whereNull('deleted_at')
                ->get();
            foreach ($p_id as $pid) {
                $user2 = User::where('id', '!=', 0)
                    ->get();
                foreach ($user2 as $u2) {
                    if ($u2->id == $pid->user_id) {
                        $observer2[] = [
                            'default' => 1,
                            'id' => $u2->id,
                            'first_name' => $u2->first_name,
                            'last_name' => $u2->last_name
                        ];
                    } else {
                        $observer2[] = [
                            'default' => 0,
                            'id' => $u2->id,
                            'first_name' => $u2->first_name,
                            'last_name' => $u2->last_name
                        ];
                    }
                }
                break;
            }
            //dd($projects);

            return view('Print_form.projects.projects', [
                'projects' => $projects,
                'project_task' => $p_tasks,
                'observer' => $observer2,

                'cur_date' => $cur_date,
                'cur_date_val' => $cur_date_val,
                'tomorrow' => $tomorrow,
                'tomorrow_val' => $tomorrow_val,
                'plus_one_day' => $plus_one_day,
                'plus_one_day_val' => $plus_one_day_val,
                'plus_two_day' => $plus_two_day,
                'plus_two_day_val' => $plus_two_day_val,
                'plus_three_day' => $plus_three_day,
                'plus_three_day_val' => $plus_three_day_val
            ]);
        } else {
            abort(503);
        }
    }

    public function edit($id) {
        if (session('perm')['project.update']) {
            $project = Project::find($id);

            if ($project) {
                $cur_date = date("j").'.'.date("n").'.'.date("Y");
                $cur_date_val = date("Y").'-'.date("m").'-'.date("d");
                $tomorrow = date('Y-m-d', strtotime($cur_date . ' +1 day'));
                $tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +1 day'));
                $plus_one_day = date('Y-m-d', strtotime($cur_date . ' +2 day'));
                $plus_one_day_val = date('Y-m-d', strtotime($cur_date . ' +2 day'));
                $plus_two_day = date('d.m.Y', strtotime($cur_date . ' +3 day'));
                $plus_two_day_val = date('Y-m-d', strtotime($cur_date . ' +3 day'));
                $plus_three_day = date('d.m.Y', strtotime($cur_date . ' +4 day'));
                $plus_three_day_val = date('Y-m-d', strtotime($cur_date . ' +4 day'));

                $comments = Comments::getCommentsByProjectId($id);

                $contacts = Project_contact::where('project_id', '=', $id)
                    ->whereNULL('deleted_at')
                    ->orderBy('id', 'asc')
                    ->get();

                foreach($contacts as $c) {
                    $cont = Contact::where('id', '=', $c->contact_id)
                        ->whereNull('deleted_at')
                        ->first();

                    //dd($cont);
                    if (!is_null($cont)) {
                        $post = Posts_reference::find($cont->post_id);

                        if ($cont->post_id == 1) {
                            $ar_contact[] = [
                                'first_name' => $cont->first_name,
                                'last_name' => $cont->last_name,
                                'patronymic' => $cont->patronymic,
                                'phone' => $cont->phone,
                                'email' => $cont->email,
                                'post' => $post->name,
                                'id' => $cont->id,
                                'director' => 1,
                                'main' => $c->main,
                            ];
                        } else {
                            $ar_contact[] = [
                                'first_name' => $cont->first_name,
                                'last_name' => $cont->last_name,
                                'patronymic' => $cont->patronymic,
                                'phone' => $cont->phone,
                                'email' => $cont->email,
                                'post' => $post['name'],
                                'id' => $cont->id,
                                'director' => 0,
                                'main' => $c->main,
                            ];
                        }
                    } else {
                        $ar_contact[] = '';
                    }

                }

                $modules = Project_module::where('project_id', '=', $id)
                    ->whereNull('deleted_at')
                    ->get();

                foreach ($modules as $m) {
                    $name = Modules_reference::find($m->module_id);

                    $all_modules[] = [
                        'id' => $name['id'],
                        'name' => $name['name'],
                        'num' => $m->num
                    ];
                }

                foreach ($all_modules as $all) {
                    $questions_q = Module_rule::where('module_id', '=', $all['id'])
                        ->whereNull('deleted_at')
                        ->get();

                    foreach ($questions_q as $q) {
                        if (count($q->questions) > 0) {
                            $questions_ar[] = [
                                'm_id' => $all['id'],
                                'id' => $q->id,
                                'questions' => $q->questions
                            ];
                        } else {
                            $questions_ar = [];
                        }
                    }
                }

                $cur_user = Sentinel::getUser();
                $count_norm = 0;
                $count_number = 0;
                $count_print_norm = 0;
                $count_print_number = 0;
                $count_screen_norm = 0;
                $count_screen_number = 0;

                $print_form_id = Grouptasks_reference::select('id')
                    ->where('is_print_form', '=', 1)
                    ->get();

                $screen_form_id = Grouptasks_reference::select('id')
                    ->where('is_screen_form', '=', 1)
                    ->get();

                $project_tasks = Project_task::where('user_id', '=', $cur_user->id)
                    ->where('project_id', '=', $id)
                    ->whereNull('deleted_at')
                    //->whereNull('status')
                    //->whereNull('additional_id')
                    ->get();

                if (count($project_tasks) > 0) {
                    $observer_id = $project_tasks[0]['observer_id'];
                    foreach ($project_tasks as $pt) {
                        $name = Module_rule::where('id', '=', $pt->module_rule_id)
                            ->whereNull('deleted_at')
                            ->first();
                        $ptc = Project_task_contact::where('project_task_id', '=', $pt->id)
                            ->whereNull('deleted_at')
                            ->first();
                        $p_name = Project::find($pt->project_id);
                        $m_name = Modules_reference::find($pt->module_id);
                        $g_task = Grouptasks_reference::find($name['grouptasks_id']);

                        if ($g_task['is_print_form'] == 1) {
                            $type = 'print_form';
                        } elseif ($g_task['is_screen_form'] == 1) {
                            $type = 'screen_form';
                        } else {
                            $type = 'task';
                        }

                        if (count($ptc) > 0) {
                            $p_tasks[] = [
                                'id' => $pt->id,
                                'name' => $name['task'],
                                'p_name' => $p_name->name,
                                'observer_id' => $pt->observer_id,
                                'city' => $p_name->city,
                                'm_name' => $m_name->name,
                                'callbackstatus_ref_id' => $ptc['callbackstatus_ref_id'],
                                'g_task' => $g_task['name'],
                                'type' => $type,
                                'additional' => $name['additional'],
                                'status' => $pt->tasksstatus_ref_id,
                                'next_contact' => $ptc['next_contact']
                            ];
                        } else {
                            $p_tasks[] = [
                                'id' => $pt->id,
                                'name' => $name['task'],
                                'p_name' => $p_name->name,
                                'observer_id' => $pt->observer_id,
                                'city' => $p_name->city,
                                'm_name' => $m_name->name,
                                'callbackstatus_ref_id' => $ptc['callbackstatus_ref_id'],
                                'g_task' => $g_task['name'],
                                'type' => $type,
                                'additional' => $name['additional'],
                                'status' => $pt->tasksstatus_ref_id,
                                'next_contact' => null
                            ];
                        }

                        foreach ($print_form_id as $pfid) {
                            if($name->grouptasks_id != $pfid->id) {
                                $count_norm += $pt->norm_contacts;
                                $count_number += $pt->number_contacts;
                            } else {
                                $count_print_norm += $pt->norm_contacts;
                                $count_print_number += $pt->number_contacts;
                            }
                        }

                        foreach ($screen_form_id as $sfid) {
                            if($name->grouptasks_id != $sfid->id) {
                                $count_norm += $pt->norm_contacts;
                                $count_number += $pt->number_contacts;
                            } else {
                                $count_screen_norm += $pt->norm_contacts;
                                $count_screen_number += $pt->number_contacts;
                            }
                        }
                    }
                } else {
                    $observer_id = '';
                    $p_tasks = '';
                }

                $project_form = Project_print_form::where('project_id', '=', $id)
                    ->whereNull('deleted_at')
                    ->get();

                if (count($project_form) > 0) {
                    foreach ($project_form as $pf) {
                        $name = Module_rule::where('id', '=', $pf->module_rule_id)
                            ->whereNull('deleted_at')
                            ->first();
                        $ptc = Project_task_contact::where('project_task_id', '=', $pf->id)
                            ->whereNull('deleted_at')
                            ->first();
                        $p_name = Project::find($pf->project_id);
                        $m_name = Modules_reference::find($pf->module_id);
                        $g_task = Grouptasks_reference::find(11);

                        $q_ar[] = '';

                        if (count($ptc) > 0) {
                            $p_form[] = [
                                'id' => $pf->id,
                                'name' => $pf->name,
                                'p_name' => $p_name->name,
                                'observer_id' => $pf->observer_id,
                                'city' => $p_name->city,
                                'm_name' => $m_name->name,
                                'callbackstatus_ref_id' => $ptc['callbackstatus_ref_id'],
                                'g_task' => $g_task['name'],
                                'additional' => $name['additional'],
                                'status' => $pf->tasksstatus_ref_id,
                                'next_contact' => $ptc['next_contact']
                            ];
                        } else {
                            $p_form[] = [
                                'id' => $pf->id,
                                'name' => $pf->name,
                                'p_name' => $p_name->name,
                                'observer_id' => $pf->observer_id,
                                'city' => $p_name->city,
                                'm_name' => $m_name['name'],
                                'callbackstatus_ref_id' => $ptc['callbackstatus_ref_id'],
                                'g_task' => $g_task['name'],
                                'additional' => $name['additional'],
                                'status' => $pf->tasksstatus_ref_id,
                                'next_contact' => null
                            ];
                        }

                    }
                } else {
                    $p_form = '';
                }

                $count_fact = Project_task_contact::where('project_id', '=', $project->id)
                    ->whereNull('deleted_at')
                    ->whereNotNull('duration')
                    ->get();

                $remained = 0;
                foreach ($count_fact as $cf) {
                    switch ($cf->duration) {
                        case '00:15:00':
                            $remained += 0.5;
                            break;
                        case '00:30:00':
                            $remained += 1;
                            break;
                        case '00:45:00':
                            $remained += 1;
                            break;
                        case '01:00:00':
                            $remained += 1;
                            break;
                        case '01:30:00':
                            $remained += 1.5;
                            break;
                    }
                }
                //dd($remained);

                $project_tasks2 = Project_task::where('user_id', '=', $cur_user->id)
                    ->where('project_id', '=', $id)
                    ->whereNull('deleted_at')
                    ->get();

                if (count($project_tasks2) > 0) {
                    foreach ($project_tasks2 as $pt) {
                        $name = Module_rule::where('id', '=', $pt->module_rule_id)
                            ->whereNull('deleted_at')
                            ->first();

                        if ($name['additional'] == null) {
                            $c_question = Clarifying_question::where('task_id', '=', $name['id'])
                                ->whereNull('deleted_at')
                                ->get();

                            foreach ($c_question as $q) {
                                $q_ar = [
                                    'id' => $q->id,
                                    'question' => $q->question
                                ];
                                $q_ar2[] = $q_ar;
                            }
                        }
                    }
                }

                if($q_ar2 != '') {
                    foreach ($q_ar2 as $cq) {
                        $c_answer = Clarifying_answer::where('question_id', '=', $cq['id'])
                            ->where('project_id', '=', $id)
                            ->whereNull('deleted_at')
                            ->first();

                        $question[] = [
                            'id' => $cq['id'],
                            'question' => $cq['question'],
                            'answer' => $c_answer['answer']
                        ];

                    }
                } else {
                    $question = [];
                }

                if (count($question) > 0) {
                    foreach ($question as $q) {
                        $c_answer = Clarifying_answer::where('question_id', '=', $q['id'])
                            ->where('project_id', '=', $id)
                            ->whereNull('deleted_at')
                            ->first();

                        //dd($c_answer['answer']);

                        if ($c_answer['answer'] != 'null') {
                            $answer[] = [
                                'q_id' => $q['id'],
                                'answer' => $c_answer['answer']
                            ];
                        } else {
                            $answer = [];
                        }
                    }
                } else {
                    $answer = [];
                }

                //dd($project_tasks2);

                $user = User::whereNull('deleted_at')->get();
                foreach ($user as $u) {
                    if ($u->id == $project->user_id) {
                        $observer[] = [
                            'default' => 1,
                            'id' => $u->id,
                            'first_name' => $u->first_name,
                            'last_name' => $u->last_name
                        ];
                    } else {
                        $observer[] = [
                            'default' => 0,
                            'id' => $u->id,
                            'first_name' => $u->first_name,
                            'last_name' => $u->last_name
                        ];
                    }
                }

                $doc_types = Documents::getAllDocumentsType();
                $doc_input = Documents::getProjectDocsByType(1, $id);
                $doc_output = Documents::getProjectDocsByType(2, $id);

                //dd($p_tasks);
                $client = Clients_reference::find($project->client_id);
                return view('Print_form.projects.edit', [
                    'project' => $project,
                    'observer_id' => $observer_id,
                    'comments' => $comments,
                    'contacts' => $ar_contact,
                    'modules' => $all_modules,
                    'all_modules' => Modules::getModulesByProjectId($project->id),
                    'project_task' => $p_tasks,
                    'project_form' => $p_form,
                    'group_task' => GroupTasks::getAllTasks(),
                    'all_group_task' => GroupTasks::getAllTaskGroup(),
                    'observer' => $observer,
                    'cur_date' => $cur_date,
                    'cur_date_val' => $cur_date_val,
                    'tomorrow' => $tomorrow,
                    'tomorrow_val' => $tomorrow_val,
                    'plus_one_day' => $plus_one_day,
                    'plus_one_day_val' => $plus_one_day_val,
                    'plus_two_day' => $plus_two_day,
                    'plus_two_day_val' => $plus_two_day_val,
                    'plus_three_day' => $plus_three_day,
                    'plus_three_day_val' => $plus_three_day_val,
                    'client' => $client,
                    'questions' => $questions_ar,
                    'question' => $question,
                    'answer' => $answer,
                    'count_norm' => $count_norm,
                    'count_number' => $count_number,
                    'count_print_norm' => $count_print_norm,
                    'count_print_number' => $count_print_number,
                    'count_screen_norm' => $count_screen_norm,
                    'count_screen_number' => $count_screen_number,
                    'count_fact' => count($count_fact),
                    'remained' => $remained,
                    'doc_types' => $doc_types,
                    'doc_input' => $doc_input,
                    'doc_output' => $doc_output
                ]);
            } else {
                abort(404);
            }
        } else {
            abort(503);
        }
    }

    public function add() {
        if (session('perm')['project.create']) {
            $users = Users2::getAllUsers();
            $modules = Modules_reference::whereNULL('deleted_at')->get();
            $posts = Posts_reference::whereNull('deleted_at')->get();
            $cur_user = Sentinel::getUser();

            $documents = Documents::getDocumentsForAllProjects();

            return view('Print_form.projects.add', [
                'users' => $users,
                'cur_user' => $cur_user,
                'modules' => $modules,
                'posts' => $posts,
                'documents' => $documents,
                'clients' => Clients::getAllClients()
            ]);
        } else {
            abort(503);
        }
    }

    public function postAdd(Request $request) {
        if (session('perm')['project.create']) {
            if (!$request->projectName) {
                return redirect()->back()->with('error', 'Укажите название проекта');
            } else {
                $count_project = Project::where('name', '=', $request->projectName)
                    ->whereNull('deleted_at')
                    ->count();
                if ($count_project > 0) {
                    return redirect()->back()->with('error', 'Проект с данным названием уже существует');
                }
            }
            if (!$request->startDate) {
                return redirect()->back()->with('error', 'Укажите дату начала');
            } else {
                $startDate = AMDateTime::startProjectDate($request->startDate);
                if (!$startDate) {
                    return redirect()->back()->with('error', 'Дата создания проекта меньше текущей даты');
                }
            }
            if (!$request->finishDate) {
                return redirect()->back()->with('error', 'Укажите дату окончания');
            } else {
                $finishDate = AMDateTime::finishProjectDate($request->finishDate);
                if (!$finishDate) {
                    return redirect()->back()->with('error', 'Дата окончания проекта меньше текущей даты');
                }
            }
            if ($request->startDate >= $request->finishDate) {
                return redirect()->back()->with('error', 'Дата окончания проекта меньше даты начала');
            }
            if ($request->optionsRadios != "on") {
                return redirect()->back()->with('error', 'Отсутствует информация об отправке документов');
            }
            if ($request->row_modules == '[]') {
                return redirect()->back()->with('error', 'Выберите модули');
            }
            $cur_user = Sentinel::getUser();
            $contacts = json_decode($request->contacts);
            $modules = json_decode($request->row_modules);
            $module_rule = json_decode($request->module_rule);
            $documents = json_decode($request->documents);

            //dd($documents);

            DB::transaction(function () use ($request, $contacts, $cur_user, $modules, $module_rule, $documents) {
                try {
                    $this->project = new Project();
                    $this->project->name = $request->projectName;
                    $this->project->done = 0;
                    $this->project->active = 1;
                    $this->project->transport_company = $request->transComp;
                    $this->project->letter_id = $request->MessageId;
                    $this->project->start_date = $request->startDate;
                    $this->project->finish_date = $request->finishDate;
                    $this->project->user_id = $request->user;
                    $this->project->client_id = $request->client;
                    $this->project->observer_id = $cur_user->id;
                    $this->project->save();

                    $cu = User::find($request->user);
                    $cu->notify(new ProjectTask($this->project));
                    $this->success = true;
                } catch (\Exception $e) {
                    DB::rollback();
                    $this->success = false;
                }

                foreach ($documents as $doc) {
                    $is_first = Documents::isFirstDoc($doc->name);

                    if ($is_first) {
                        try {
                            $type = Documents::getDocTypeByName($doc->type);
                            $doc_name = Documents::getDocumentByName($doc->name);

                            $project_doc = new Projects_doc();
                            $project_doc->project_id = $this->project->id;
                            $project_doc->doc_id = $doc_name->id;
                            $project_doc->type_id = $type->id;
                            $project_doc->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            DB::rollback();
                            $this->success = false;
                        }
                    }
                }

                foreach ($contacts as $c) {
                    if ($c->id != '') {
                        try {
                            $contact = Contact::find($c->id);
                            $p_contact = new Project_contact();
                            $p_contact->contact_id = $contact->id;
                            $p_contact->project_id = $this->project->id;
                            if ($c->main == 'true') {
                                $p_contact->main = 1;
                            } else {
                                $p_contact->main = 0;
                            }
                            if ($c->email_rep == 'true') {
                                $p_contact->mail_rep = 1;
                            } else {
                                $p_contact->mail_rep = 0;
                            }
                            $p_contact->save();
                            $this->success = true;
                        } catch (\Exception $e) {
                            DB::rollback();
                            $this->success = false;
                        }
                    } else {
                        try {
                            $post = Posts_reference::where('name', '=', $c->post)->firstOrFail();
                            $contact = new Contact();
                            $contact->first_name = $c->first_name;
                            $contact->last_name = $c->last_name;
                            $contact->patronymic = $c->patronymic;
                            $contact->post_id = $post->id;
                            $contact->phone = $c->phone;
                            $contact->email = $c->email;
                            $contact->client_id = $request->client;
                            $contact->user_id = $cur_user->id;
                            $contact->save();

                            $p_contact = new Project_contact();
                            $p_contact->contact_id = $contact->id;
                            $p_contact->project_id = $this->project->id;
                            if ($c->main == 'true') {
                                $p_contact->main = 1;
                            } else {
                                $p_contact->main = 0;
                            }
                            if ($c->email_rep == 'true') {
                                $p_contact->mail_rep = 1;
                            } else {
                                $p_contact->mail_rep = 0;
                            }
                            $p_contact->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            DB::rollback();
                            $this->success = false;
                        }
                    }
                }

                foreach ($modules as $m) {
                    try {
                        $modules = new Project_module();
                        $modules->module_id = (int)$m->id;
                        $modules->project_id = $this->project->id;
                        $modules->num = (int)$m->num;
                        $modules->save();

                        $m_rule = Module_rule::where('module_id', '=', (int)$m->id)
                            ->whereNull('deleted_at')
                            ->whereNull('additional')
                            ->get();

                        foreach ($m_rule as $mr) {
                            //if ($mr->grouptasks_id != 11) {
                                $p_task = new Project_task();
                                $p_task->project_id = $this->project->id;
                                $p_task->user_id = $request->user;
                                $p_task->observer_id = $cur_user->id;
                                $p_task->module_rule_id = $mr->id;
                                $p_task->norm_contacts = $mr->norm_contacts;
                                $p_task->number_contacts = $mr->number_contacts;
                                $p_task->module_id = $m->id;
                                $p_task->tasksstatus_ref_id = 1;
                                $p_task->save();
                            /*} else {
                                $number = Project_print_form::whereNull('deleted_at')
                                    ->max('number');

                                if (isset($number)) {
                                    $p_form = new Project_print_form();
                                    $p_form->project_id = $this->project->id;
                                    $p_form->number = $number + 1;
                                    $p_form->name = $mr->task;
                                    $p_form->user_id = $request->user;
                                    $p_form->observer_id = $cur_user->id;
                                    $p_form->module_rule_id = $mr->id;
                                    $p_form->norm_contacts = $mr->norm_contacts;
                                    $p_form->number_contacts = $mr->number_contacts;
                                    $p_form->module_id = $m->id;
                                    $p_form->printform_status_ref_id = 1;
                                    $p_form->save();

                                    $cu = User::find($request->user);
                                    $cu->notify(new ProjectTask($p_form));
                                } else {
                                    //dd($mr->number_contacts);
                                    $p_form_new = new Project_print_form();
                                    $p_form_new->project_id = $this->project->id;
                                    $p_form_new->number = 1;
                                    $p_form_new->name = $mr->task;
                                    $p_form_new->user_id = (int)$request->user;
                                    $p_form_new->observer_id = (int)$cur_user->id;
                                    $p_form_new->module_rule_id = (int)$mr->id;
                                    $p_form_new->norm_contacts = $mr->norm_contacts;
                                    $p_form_new->number_contacts = $mr->number_contacts;
                                    $p_form_new->module_id = (int)$m->id;
                                    $p_form_new->printform_status_ref_id = 1;
                                    $p_form_new->save();
                                }
                            }*/
                        }

                        foreach ($module_rule as $new_mr) {
                            try {
                                $new_p_task = Project_task::where('project_id', '=', $project->id)
                                    ->where('module_rule_id', '=', $new_mr->id)
                                    ->firstOrFail();

                                $new_p_task->norm_contacts = (float)$new_mr->norm;
                                $new_p_task->number_contacts = (int)$new_mr->number;
                                $new_p_task->save();

                                $this->success = true;
                            } catch (\Exception $e) {
                                $this->success = false;
                            }
                        }

                        //

                        $this->success = true;
                    } catch (\Exception $e) {
                        DB::rollback();
                        $this->success = false;
                    }
                }
            });

            if ($this->success) {
                return redirect('/print_form/projects')->with('success', 'Проект успешно создан');
            } else {
                return redirect('/print_form/projects')->with('error', 'Произошла ошибка при создании проекта');
            }

        } else {
            abort(503);
        }
    }

    public function close($id) {
        $project = Project::find($id);
        $client = Clients_reference::find($project->client_id);

        return view('Print_form.projects.close', [
            'project' => $project,
            'client' => $client
        ]);
    }

    public function postClose(Request $request, $id) {
        DB::transaction(function () use ($request, $id) {
            $date =  explode(' ', date("Y-m-d H:i:s"));
            $cur_user = Sentinel::getUser();
            try {
                $comment = new Comment();
                $comment->project_id = $id;
                $comment->user_id = $cur_user->id;
                $comment->for_client = NULL;
                $comment->close = 1;
                $comment->date = $date[0];
                $comment->time = $date[1];
                $comment->text = $request->comment;
                $comment->save();

                $this->success = true;
            } catch (\Exception $e) {
                $this->success = false;
            }

            try {
                $project = Project::find($id);
                $project->callback_status = 6;
                $project->done_datetime = date("Y-m-d H:i:s");
                $project->save();

                $this->success = true;
            } catch (\Exception $e) {
                $this->success = false;
            }
        });

        if ($this->success) {
            return redirect('/print_form/projects')->with('success', 'Данные успешно сохранены');
        } else {
            return redirect()->back()->with('error', 'Произошла ошибка при сохранении данных');
        }
    }

    public function sortProjectTasks(Request $request) {
        if ($request->ajax()) {
            $cur_user = Sentinel::getUser();

            unset($p_tasks);
            switch ($request->status) {
                case 'all':
                    $project_tasks_all = Project_task::where('user_id', '=', $cur_user->id)
                        ->where('project_id', '=', $request->pid)
                        //->whereNull('status')
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($project_tasks_all) > 0) {
                        foreach ($project_tasks_all as $pt1) {
                            $name = Module_rule::find($pt1->module_rule_id);
                            $p_name = Project::find($pt1->project_id);
                            $m_name = Modules_reference::find($pt1->module_id);
                            $g_task = Grouptasks_reference::find($name->grouptasks_id);

                            $ptc = Project_task_contact::where('project_task_id', '=', $pt1->id)
                                ->whereNull('deleted_at')
                                ->first();

                            if ($g_task['is_print_form'] == 1) {
                                $type = 'print_form';
                            } elseif ($g_task['is_screen_form'] == 1) {
                                $type = 'screen_form';
                            } else {
                                $type = 'task';
                            }

                            if (count($ptc) > 0) {
                                $p_tasks[] = [
                                    'id' => $pt1->id,
                                    'name' => $name->task,
                                    'p_name' => $p_name->name,
                                    'p_id' => $pt1->project_id,
                                    'city' => $p_name->city,
                                    'm_name' => $m_name->name,
                                    'g_task' => $g_task->name,
                                    'type' => $type,
                                    'additional' => $name->additional,
                                    'status' => $pt1->tasksstatus_ref_id,
                                    'next_contact' => $ptc['next_contact'],
                                    'print_form' => 0
                                ];
                            } else {
                                $p_tasks[] = [
                                    'id' => $pt1->id,
                                    'name' => $name->task,
                                    'p_name' => $p_name->name,
                                    'p_id' => $pt1->project_id,
                                    'city' => $p_name->city,
                                    'm_name' => $m_name->name,
                                    'g_task' => $g_task->name,
                                    'type' => $type,
                                    'additional' => $name->additional,
                                    'status' => $pt1->tasksstatus_ref_id,
                                    'next_contact' => null,
                                    'print_form' => 0
                                ];
                            }
                        }
                    } else {
                        $p_tasks[] = '';
                    }

                    $data = [
                        'p_task' => $p_tasks
                    ];

                    break;
                case 'main':
                    $project_tasks_main = Project_task::select('project_tasks.id',
                        'project_tasks.module_rule_id',
                        'project_tasks.module_id',
                        'project_tasks.project_id',
                        'project_tasks.tasksstatus_ref_id')
                        ->where('project_tasks.user_id', '=', $cur_user->id)
                        ->join('module_rules', 'project_tasks.module_rule_id', '=', 'module_rules.id')
                        ->where('project_tasks.project_id', '=', $request->pid)
                        ->whereNull('module_rules.additional')
                        ->whereNull('project_tasks.deleted_at')
                        //->whereNull('status')
                        ->get();

                    if (count($project_tasks_main) > 0) {
                        foreach ($project_tasks_main as $pt2) {
                            $name = Module_rule::find($pt2->module_rule_id);
                            $p_name = Project::find($pt2->project_id);
                            $m_name = Modules_reference::find($pt2->module_id);
                            $g_task = Grouptasks_reference::find($name->grouptasks_id);

                            $ptc = Project_task_contact::where('project_task_id', '=', $pt2->id)
                                ->whereNull('deleted_at')
                                ->first();

                            if ($g_task['is_print_form'] == 1) {
                                $type = 'print_form';
                            } elseif ($g_task['is_screen_form'] == 1) {
                                $type = 'screen_form';
                            } else {
                                $type = 'task';
                            }

                            if (count($ptc) > 0) {
                                $p_tasks[] = [
                                    'id' => $pt2->id,
                                    'name' => $name->task,
                                    'p_name' => $p_name->name,
                                    'p_id' => $pt2->project_id,
                                    'city' => $p_name->city,
                                    'm_name' => $m_name->name,
                                    'g_task' => $g_task->name,
                                    'type' => $type,
                                    'additional' => $name->additional,
                                    'status' => $pt2->tasksstatus_ref_id,
                                    'next_contact' => $ptc['next_contact'],
                                    'print_form' => 0
                                ];
                            } else {
                                $p_tasks[] = [
                                    'id' => $pt2->id,
                                    'name' => $name->task,
                                    'p_name' => $p_name->name,
                                    'p_id' => $pt2->project_id,
                                    'city' => $p_name->city,
                                    'm_name' => $m_name->name,
                                    'g_task' => $g_task->name,
                                    'type' => $type,
                                    'additional' => $name->additional,
                                    'status' => $pt2->tasksstatus_ref_id,
                                    'next_contact' => null,
                                    'print_form' => 0
                                ];
                            }
                        }
                    } else {
                        $p_tasks[] = '';
                    }

                    $data = [
                        'p_task' => $p_tasks
                    ];

                    break;
                case 'additional':
                    $count_norm = 0;
                    $count_number = 0;
                    $count_print_norm = 0;
                    $count_print_number = 0;
                    $count_screen_norm = 0;
                    $count_screen_number = 0;
                    $pf_ar = [];
                    $sf_ar = [];

                    $print_form_id = Grouptasks_reference::select('id')
                        ->where('is_print_form', '=', 1)
                        ->get();

                    $screen_form_id = Grouptasks_reference::select('id')
                        ->where('is_screen_form', '=', 1)
                        ->get();

                    $project_tasks_additional = Project_task::select('project_tasks.id',
                        'project_tasks.module_rule_id',
                        'project_tasks.module_id',
                        'project_tasks.project_id',
                        'project_tasks.tasksstatus_ref_id',
                        'project_tasks.norm_contacts',
                        'project_tasks.number_contacts')
                        ->where('project_tasks.user_id', '=', $cur_user->id)
                        ->join('module_rules', 'project_tasks.module_rule_id', '=', 'module_rules.id')
                        ->where('project_tasks.project_id', '=', $request->pid)
                        ->where('module_rules.additional', '=', 1)
                        ->whereNull('project_tasks.deleted_at')
                        //->whereNull('status')
                        ->get();

                    if (count($project_tasks_additional) > 0) {
                        foreach ($project_tasks_additional as $pt3) {
                            $name = Module_rule::find($pt3->module_rule_id);
                            $p_name = Project::find($pt3->project_id);
                            $m_name = Modules_reference::find($pt3->module_id);
                            $g_task = Grouptasks_reference::find($name->grouptasks_id);

                            $ptc = Project_task_contact::where('project_task_id', '=', $pt3->id)
                                ->whereNull('deleted_at')
                                ->first();

                            if ($g_task['is_print_form'] == 1) {
                                $type = 'print_form';
                            } elseif ($g_task['is_screen_form'] == 1) {
                                $type = 'screen_form';
                            } else {
                                $type = 'task';
                            }

                            foreach ($print_form_id as $pfid) {
                                if($name->grouptasks_id != $pfid->id) {
                                    /*$count_norm += $pt3->norm_contacts;
                                    $count_number += $pt3->number_contacts;*/
                                } else {
                                    $count_print_norm += $pt3->norm_contacts;
                                    $count_print_number += $pt3->number_contacts;
                                    $pf_ar[] = $pfid->id;
                                }
                            }

                            foreach ($screen_form_id as $sfid) {
                                if($name->grouptasks_id != $sfid->id) {
                                    /*$count_norm += $pt3->norm_contacts;
                                    $count_number += $pt3->number_contacts;*/
                                } else {
                                    $count_screen_norm += $pt3->norm_contacts;
                                    $count_screen_number += $pt3->number_contacts;
                                    $sf_ar[] = $sfid->id;
                                }
                            }

                            if (count($ptc) > 0) {
                                $p_tasks[] = [
                                    'id' => $pt3->id,
                                    'name' => $name->task,
                                    'p_name' => $p_name->name,
                                    'p_id' => $pt3->project_id,
                                    'city' => $p_name->city,
                                    'm_name' => $m_name->name,
                                    'g_task' => $g_task->name,
                                    'g_task_id' => $name->grouptasks_id,
                                    'type' => $type,
                                    'additional' => $name->additional,
                                    'status' => $pt3->tasksstatus_ref_id,
                                    'next_contact' => $ptc['next_contact'],
                                    'print_form' => 0,
                                    'norm_contacts' => $pt3->norm_contacts,
                                    'number_contacts' => $pt3->number_contacts
                                ];
                            } else {
                                $p_tasks[] = [
                                    'id' => $pt3->id,
                                    'name' => $name->task,
                                    'p_name' => $p_name->name,
                                    'p_id' => $pt3->project_id,
                                    'city' => $p_name->city,
                                    'm_name' => $m_name->name,
                                    'g_task' => $g_task->name,
                                    'g_task_id' => $name->grouptasks_id,
                                    'type' => $type,
                                    'additional' => $name->additional,
                                    'status' => $pt3->tasksstatus_ref_id,
                                    'next_contact' => null,
                                    'print_form' => 0,
                                    'norm_contacts' => $pt3->norm_contacts,
                                    'number_contacts' => $pt3->number_contacts
                                ];
                            }
                        }
                    } else {
                        $p_tasks[] = '';
                    }

                    $form_ar = array_merge($pf_ar, $sf_ar);
                    foreach ($p_tasks as $tp) {
                        if(!in_array($tp['g_task_id'], $form_ar)) {
                            $count_norm += $tp['norm_contacts'];
                            $count_number += $tp['number_contacts'];
                        }
                    }

                    $count_fact = Project_task_contact::where('project_id', '=', $request->pid)
                        ->whereNull('deleted_at')
                        ->whereNotNull('duration')
                        ->get();

                    $remained = 0;
                    foreach ($count_fact as $cf) {
                        switch ($cf->duration) {
                            case '00:15:00':
                                $remained += 0.5;
                                break;
                            case '00:30:00':
                                $remained += 1;
                                break;
                            case '00:45:00':
                                $remained += 1;
                                break;
                            case '01:00:00':
                                $remained += 1;
                                break;
                            case '01:30:00':
                                $remained += 1.5;
                                break;
                        }
                    }

                    $data = [
                        'p_task' => $p_tasks,
                        'count_norm' => $count_norm,
                        'count_number' => $count_number,
                        'count_print_norm' => $count_print_norm,
                        'count_print_number' => $count_print_number,
                        'count_screen_norm' => $count_screen_norm,
                        'count_screen_number' => $count_screen_number
                    ];

                    break;
            }
            return response()->json($data);
        }
    }

    public function sortTasks(Request $request) {
        if ($request->ajax()) {
            $cur_user = Sentinel::getUser();
            switch ($request->status) {
                case 'notExecute':
                    $project_tasks = Project_task::where('user_id', '=', $cur_user->id)
                        ->whereIn('tasksstatus_ref_id', [1,3])
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($project_tasks) > 0) {
                        foreach ($project_tasks as $pt) {
                            $name = Module_rule::find($pt->module_rule_id);
                            $p_name = Project::find($pt->project_id);
                            $p_client = Clients::getClientById($p_name['client_id']);
                            $p_city = Citys::getCityById($p_client->city_id);

                            $ptc = Project_task_contact::where('project_task_id', '=', $pt->id)
                                ->whereNull('deleted_at')
                                ->first();

                            $user = User::whereNull('deleted_at')->get();
                            foreach ($user as $u) {
                                if ($u->id == $p_name->user_id) {
                                    $observer[] = [
                                        'default' => 1,
                                        'id' => $u->id,
                                        'first_name' => $u->first_name,
                                        'last_name' => $u->last_name
                                    ];
                                } else {
                                    $observer[] = [
                                        'default' => 0,
                                        'id' => $u->id,
                                        'first_name' => $u->first_name,
                                        'last_name' => $u->last_name
                                    ];
                                }
                            }

                            if (count($ptc) > 0) {
                                if ($pt['prev_contact'] != null) {
                                    $p_tasks[] = [
                                        'id' => $pt['id'],
                                        'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                        'client' => $p_client->name,
                                        'name' => $name->task,
                                        'observer' => $observer,
                                        'p_name' => $p_name['name'],
                                        'city' => $p_city,
                                        'next_contact' => $ptc['next_contact'],
                                        'prev_contact' => $pt['prev_contact'],
                                    ];
                                } else {
                                    $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                                        ->whereNull('deleted_at')
                                        //->whereNull('status')
                                        ->max('prev_contact');

                                    $p_tasks[] = [
                                        'id' => $pt['id'],
                                        'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                        'client' => $p_client->name,
                                        'name' => $name->task,
                                        'observer' => $observer,
                                        'p_name' => $p_name['name'],
                                        'city' => $p_city,
                                        'next_contact' => $ptc['next_contact'],
                                        'prev_contact' => $max_date,
                                    ];
                                }
                            } else {
                                if ($pt['prev_contact'] != null) {
                                    $p_tasks[] = [
                                        'id' => $pt['id'],
                                        'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                        'client' => $p_client->name,
                                        'name' => $name->task,
                                        'observer' => $observer,
                                        'p_name' => $p_name['name'],
                                        'city' => $p_city,
                                        'next_contact' => null,
                                        'prev_contact' => $pt['prev_contact'],
                                    ];
                                } else {
                                    $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                                        ->whereNull('deleted_at')
                                        //->whereNull('status')
                                        ->max('prev_contact');

                                    $p_tasks[] = [
                                        'id' => $pt['id'],
                                        'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                        'client' => $p_client->name,
                                        'name' => $name->task,
                                        'observer' => $observer,
                                        'p_name' => $p_name['name'],
                                        'city' => $p_city,
                                        'next_contact' => null,
                                        'prev_contact' => $max_date,
                                    ];
                                }
                            }
                        }
                    } else {
                        $p_tasks[] = '';
                    }
                    return response()->json($p_tasks);
                    break;

                case 'all':
                    $project_tasks = Project_task::where('user_id', '=', $cur_user->id)
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($project_tasks) > 0) {
                        foreach ($project_tasks as $pt) {
                            $name = Module_rule::find($pt->module_rule_id);
                            $p_name = Project::find($pt->project_id);
                            $p_client = Clients::getClientById($p_name['client_id']);
                            $p_city = Citys::getCityById($p_client->city_id);

                            $ptc = Project_task_contact::where('project_task_id', '=', $pt->id)
                                ->whereNull('deleted_at')
                                ->get();

                            $user = User::whereNull('deleted_at')->get();
                            foreach ($user as $u) {
                                if ($u->id == $p_name->user_id) {
                                    $observer[] = [
                                        'default' => 1,
                                        'id' => $u->id,
                                        'first_name' => $u->first_name,
                                        'last_name' => $u->last_name
                                    ];
                                } else {
                                    $observer[] = [
                                        'default' => 0,
                                        'id' => $u->id,
                                        'first_name' => $u->first_name,
                                        'last_name' => $u->last_name
                                    ];
                                }
                            }

                            if (count($ptc) > 0) {
                                if ($pt['prev_contact'] != null) {
                                    $p_tasks[] = [
                                        'id' => $pt['id'],
                                        'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                        'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                                        'client' => $p_client->name,
                                        'name' => $name->task,
                                        'observer' => $observer,
                                        'p_name' => $p_name['name'],
                                        'city' => $p_city,
                                        'next_contact' => $ptc[0]['next_contact'],
                                        'prev_contact' => $pt['prev_contact'],
                                    ];
                                } else {
                                    $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                                        ->whereNull('deleted_at')
                                        //->whereNull('status')
                                        ->max('prev_contact');

                                    $p_tasks[] = [
                                        'id' => $pt['id'],
                                        'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                        'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                                        'client' => $p_client->name,
                                        'name' => $name->task,
                                        'observer' => $observer,
                                        'p_name' => $p_name['name'],
                                        'city' => $p_city,
                                        'next_contact' => $ptc[0]['next_contact'],
                                        'prev_contact' => $max_date,
                                    ];
                                }
                            } else {
                                if ($pt['prev_contact'] != null) {
                                    $p_tasks[] = [
                                        'id' => $pt['id'],
                                        'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                        'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                                        'client' => $p_client->name,
                                        'name' => $name->task,
                                        'observer' => $observer,
                                        'p_name' => $p_name['name'],
                                        'city' => $p_city,
                                        'next_contact' => null,
                                        'prev_contact' => $pt['prev_contact'],
                                    ];
                                } else {
                                    $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                                        ->whereNull('deleted_at')
                                        //->whereNull('status')
                                        ->max('prev_contact');

                                    $p_tasks[] = [
                                        'id' => $pt['id'],
                                        'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                        'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                                        'client' => $p_client->name,
                                        'name' => $name->task,
                                        'observer' => $observer,
                                        'p_name' => $p_name['name'],
                                        'city' => $p_city,
                                        'next_contact' => null,
                                        'prev_contact' => $max_date,
                                    ];
                                }
                            }
                        }
                    } else {
                        $p_tasks[] = '';
                    }
                    return response()->json($p_tasks);
                    break;
            }
        }
    }

    public function calc($new_hour = 0) {
        $p_form = Project_print_form::where('printform_status_ref_id', '!=', 5)
            ->whereNull('deleted_at')
            ->get();

        $hour = 0;
        if (count($p_form) > 0) {
            foreach ($p_form as $pf) {
                $hour += $pf->max_hours;
            }
            $res = (($hour + $new_hour) / 8) / 2;

            $cur_date = date("j").'.'.date("n").'.'.date("Y");
            $date = date('Y-m-d', strtotime($cur_date . ' +'.round($res).' day'));
            //$tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +'.round($res).' day'));
        } else {
            $date = date("Y").'-'.date("m").'-'.date("d");
        }

        return $date;
    }

    public function addAdditionalTask(Request $request) {
        //dd($request->all());
        $module_rule = new Module_rule();
        $module_rule->module_id = $request->selectModule;
        $module_rule->task = $request->name_task;
        $module_rule->grouptasks_id = $request->selectGroupTask;
        $module_rule->additional = 1;
        $module_rule->number_contacts = $request->cont;


        if ($module_rule->save()) {
            $cur_user = Sentinel::getUser();

            if ($request->selectGroupTask != 11) {
                try {
                    $project_task = new Project_task();
                    $project_task->project_id = $request->pid;
                    $project_task->user_id = $cur_user->id;
                    $project_task->observer_id = $request->observer_id;
                    $project_task->module_rule_id = $module_rule->id;
                    $project_task->number_contacts = $request->cont;
                    $project_task->grouptasks_id = $request->selectGroupTask;
                    $project_task->module_id = $request->selectModule;
                    $project_task->tasksstatus_ref_id = 1;
                    $project_task->save();

                    $success = true;
                } catch (\Exception $e) {
                    $success = false;
                }
            } else {
                $number = Project_print_form::whereNull('deleted_at')
                    ->max('number');
                $date = $this->calc();

                try {
                    /*$p_form = new Project_print_form();
                    $p_form->number = $number+1;
                    $p_form->project_id = $request->pid;
                    $p_form->user_id = $request->user_id;
                    $p_form->observer_id = $request->observer_id;
                    $p_form->name = $request->name_task;
                    $p_form->category_printform_ref_id = 1;
                    $p_form->module_rule_id = $module_rule->id;
                    $p_form->max_hours = 5;
                    $p_form->printform_status_ref_id = 1;
                    $p_form->date_implement = $date;
                    $p_form->date_implement_for_client = $date;
                    $p_form->is_printform = 0;
                    $p_form->is_free = 0;
                    $p_form->paid = 0;
                    $p_form->save();*/

                    $project_task = new Project_task();
                    $project_task->project_id = $request->pid;
                    $project_task->user_id = $cur_user->id;
                    $project_task->observer_id = $request->observer_id;
                    $project_task->module_rule_id = $module_rule->id;
                    $project_task->number_contacts = $request->cont;
                    $project_task->grouptasks_id = $request->selectGroupTask;
                    $project_task->module_id = $request->selectModule;
                    $project_task->tasksstatus_ref_id = 1;
                    $project_task->save();

                    $success = true;
                } catch (\Exception $e) {
                    $success = false;
                }
            }

            if ($success) {
                return redirect()->back()->with('success', 'Дополнительная задача успешно добавлена');
            }else {
                return redirect()->back()->with('error', 'Произошла ошибка при добавлении дополнительной задачи');
            }
        } else {
            return redirect()->back()->with('error', 'Произошла ошибка при добавлении дополнительной задачи');
        }
    }

    public function searchTasks(Request $request) {
        if ($request->ajax()) {
            $cur_user = Sentinel::getUser();
            $project_tasks = Project_task::where('user_id', '=', $cur_user->id)
                ->whereNull('deleted_at')
                ->get();

            if (count($project_tasks) > 0) {
                foreach ($project_tasks as $pt) {
                    $name = Module_rule::find($pt->module_rule_id);
                    $p_name = Project::find($pt->project_id);
                    $p_client = Clients::getClientById($p_name['client_id']);
                    $p_city = Citys::getCityById($p_client->city_id);
                    $ptc = Project_task_contact::where('project_task_id', '=', $pt->id)
                        ->whereNull('deleted_at')
                        ->first();

                    $user = User::whereNull('deleted_at')->get();
                    foreach ($user as $u) {
                        if ($u->id == $p_name->user_id) {
                            $observer[] = [
                                'default' => 1,
                                'id' => $u->id,
                                'first_name' => $u->first_name,
                                'last_name' => $u->last_name
                            ];
                        } else {
                            $observer[] = [
                                'default' => 0,
                                'id' => $u->id,
                                'first_name' => $u->first_name,
                                'last_name' => $u->last_name
                            ];
                        }
                    }

                    if (count($ptc) > 0) {
                        if ($pt['prev_contact'] != null) {
                            $p_tasks[] = [
                                'id' => $pt['id'],
                                'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                                'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                'client' => $p_client->name,
                                'name' => $name->task,
                                'observer' => $observer,
                                'p_name' => $p_name['name'],
                                'city' => $p_city,
                                'next_contact' => $ptc['next_contact'],
                                'prev_contact' => $pt['prev_contact'],
                            ];
                        } else {
                            $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                                ->whereNull('deleted_at')
                                //->whereNull('status')
                                ->max('prev_contact');

                            $p_tasks[] = [
                                'id' => $pt['id'],
                                'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                                'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                'client' => $p_client->name,
                                'name' => $name->task,
                                'observer' => $observer,
                                'p_name' => $p_name['name'],
                                'city' => $p_city,
                                'next_contact' => $ptc['next_contact'],
                                'prev_contact' => $max_date,
                            ];
                        }
                    } else {
                        if ($pt['prev_contact'] != null) {
                            $p_tasks[] = [
                                'id' => $pt['id'],
                                'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                'client' => $p_client->name,
                                'name' => $name->task,
                                'observer' => $observer,
                                'p_name' => $p_name['name'],
                                'city' => $p_city,
                                'next_contact' => null,
                                'prev_contact' => $pt['prev_contact'],
                            ];
                        } else {
                            $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                                ->whereNull('deleted_at')
                                //->whereNull('status')
                                ->max('prev_contact');

                            $p_tasks[] = [
                                'id' => $pt['id'],
                                'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                'client' => $p_client->name,
                                'name' => $name->task,
                                'observer' => $observer,
                                'p_name' => $p_name['name'],
                                'city' => $p_city,
                                'next_contact' => null,
                                'prev_contact' => $max_date,
                            ];
                        }
                    }
                }
            }

            $res = '';
            foreach ($p_tasks as $p_task) {
                if (strpos($p_task['client'], $request->text) || strpos($p_task['name'], $request->text)) {
                    $res[] = $p_task;
                }
            }

            return response()->json($res);
        }
    }

    public function searchProjects(Request $request) {
        if ($request->ajax()) {
            $cur_user = Sentinel::getUser();
            $projects_q = Project::where('user_id', '=', $cur_user->id)
                ->whereNULL('deleted_at')
                ->get();

            if (count($projects_q) > 0) {
                foreach ($projects_q as $p) {
                    $task = Project_task::where('project_id', '=', $p->id)
                        ->where('user_id', '=', $cur_user->id)
                        ->whereNull('deleted_at')
                        ->get();

                    $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                        ->whereNull('deleted_at')
                        ->get();

                    $client = Clients::getClientById($p->client_id);
                    $city = Citys::getCityById($client->city_id);

                    foreach ($task as $t) {
                        if ($t->tasksstatus_ref_id == 2) {
                            $done2[$p->id][] = $t->id;
                        }
                    }

                    if (isset($done2[$p->id])) {
                        $user = Users2::getUserById($p->user_id);
                        $client = Clients::getClientById($p->client_id);
                        $projects[] = [
                            'id' => $p->id,
                            'user' => $user->last_name.' '.$user->first_name,
                            'name' => $p->name,
                            'client' => $client->name,
                            'city' => $city,
                            'prev_contact' => $ptc[0]['prev_contact'],
                            'next_contact' => $ptc[0]['next_contact'],
                            'start_date' => $p->start_date,
                            'finish_date' => $p->finish_date,
                            'created_at' => $p->created_at,
                            'active' => $p->active,
                            'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                        ];
                    } else {
                        $user = Users2::getUserById($p->user_id);
                        $client = Clients::getClientById($p->client_id);
                        $projects[] = [
                            'id' => $p->id,
                            'user' => $user->last_name.' '.$user->first_name,
                            'name' => $p->name,
                            'client' => $client->name,
                            'city' => $city,
                            'prev_contact' => $ptc[0]['prev_contact'],
                            'next_contact' => $ptc[0]['next_contact'],
                            'start_date' => $p->start_date,
                            'finish_date' => $p->finish_date,
                            'created_at' => $p->created_at,
                            'active' => $p->active,
                            //'done' => 0,
                            'done' => round(((0 / count($task)) * 100), 2),
                        ];
                    }
                }
            } else {
                $projects[] = '';
            }

            $res = '';
            foreach ($projects as $p) {
                if ($p['client'] == $request->text ||
                    $p['name'] == $request->text ||
                    $p['user'] == $request->text) {
                    $res[] = $p;
                }
            }

            return response()->json($res);
        }
    }

    public function getProjectByClientId(Request $request) {
        if ($request->ajax()) {
            $projects = Project::where('client_id', '=', $request->id)
                ->whereNull('deleted_at')
                ->get();
            return response()->json($projects);
        }
    }

    public function getProjectByClientName(Request $request) {
        if ($request->ajax()) {
            $client = Clients_reference::where('name', '=', $request->client)
                ->whereNull('deleted_at')
                ->first();
            $projects = Project::where('client_id', '=', $client['id'])
                ->whereNull('deleted_at')
                ->get();
            return response()->json($projects);
        }
    }

    static public function getProjectById($id) {
        $project = Project::whereNull('deleted_at')
            ->where('id', '=', $id)
            ->firstOrFail();
        return $project;
    }

    static public function getProjectByTaskId($task_id) {
        $task = Project_task::find($task_id);
        $project = Project::find($task->project_id);

        return $project;
    }

    static public function getAllProjects() {
        $projects = Project::whereNull('deleted_at')->get();
        return $projects;
    }

    static public function getUserProjects($user_id) {
        $projects = Project::where('user_id', '=', $user_id)
            ->where('active', '=', 1)
            ->whereNull('deleted_at')
            ->get();

        return $projects;
    }

    static public function getUserTasks($user_id) {
        $tasks = Project_task::where('user_id', '=', $user_id)
            ->whereNull('deleted_at')
            ->get();

        if (count($tasks) > 0) {
            foreach ($tasks as $t) {
                $module_rule = Module_rule::find($t->module_rule_id);
                $grouptask = Grouptasks_reference::find($module_rule->grouptask_id);
                if ($grouptask['is_print_form'] != 1) {
                    $tasks_ar[] = [
                        'id' => $t['id'],
                        'number_contacts' => $t['number_contacts'],
                    ];
                }
            }
        } else {
            $tasks_ar = [];
        }

        return $tasks_ar;
    }

    static public function getClientProjects($cid) {
        $projects = Project::where('client_id', '=', $cid)
            ->whereNull('deleted_at')
            ->get();

        return $projects;
    }
}
