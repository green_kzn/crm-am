<?php

namespace App\Http\Controllers\Print_form;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Comment;
use App\Printform_comment;
use App\Printform_history;
use App\User;
use App\Printform_status_reference;
use Sentinel;

class Comment2Controller extends Controller
{
    public static function getCommentsByProjectId($id) {
        $comments_q = Comment::where('project_id', '=', $id)
            ->orderBy('created_at', 'desc')
            ->whereNULL('deleted_at')
            ->get();

        if (count($comments_q) > 0) {
            foreach ($comments_q as $c) {
                $user = User::find($c->user_id);
                $cur_user = Sentinel::getUser();
                
                if ($c->for_client != NULL) {
                    if ($user->id == $cur_user->id) {
                        $comments[] = [
                            'text' => strip_tags($c->text),
                            'date' => $c->date,
                            'time' => $c->time,
                            'id' => $c->id,
                            'for_client' => 1,
                            'user_name' => $user->first_name,
                            'user_surname' => $user->last_name,
                            'user_foto' => $user->foto,
                            'autor' => true
                        ];
                    } else {
                        $comments[] = [
                            'text' => strip_tags($c->text),
                            'date' => $c->date,
                            'time' => $c->time,
                            'id' => $c->id,
                            'for_client' => 1,
                            'user_name' => $user->first_name,
                            'user_surname' => $user->last_name,
                            'user_foto' => $user->foto,
                            'autor' => false
                        ];
                    }
                } else {
                    if ($user['id'] == $cur_user->id) {
                        $comments[] = [
                            'text' => strip_tags($c->text),
                            'date' => $c->date,
                            'time' => $c->time,
                            'id' => $c->id,
                            'for_client' => 0,
                            'user_name' => $user->first_name,
                            'user_surname' => $user->last_name,
                            'user_foto' => $user->foto,
                            'autor' => true
                        ];
                    } else {
                        $comments[] = [
                            'text' => strip_tags($c->text),
                            'date' => $c->date,
                            'time' => $c->time,
                            'id' => $c->id,
                            'for_client' => 0,
                            'user_name' => $user['first_name'],
                            'user_surname' => $user['last_name'],
                            'user_foto' => $user['foto'],
                            'autor' => false
                        ];
                    }
                }
            }
        } else {
            $comments = '';
        }
        
        return $comments;
    }

    public function add(Request $request) {
        if ($request->ajax()) {
            $status = Printform_status_reference::where('name', '=', $request->status)
                ->first();

            $cur_user = Sentinel::getUser();
            try {
                $comment = new Printform_comment();
                $comment->user_id = $cur_user->id;
                $comment->comment = $request->text;
                $comment->form_id = $request->form_id;
                $comment->save();

                $success = true;
            } catch (\Exception $e) {
                $success = false;
            }

            try {
                $p_history = new Printform_history();
                $p_history->printform_id = $request->form_id;
                $p_history->user_id = $cur_user->id;
                $p_history->printform_status_ref_id = $status['id'];
                $p_history->comment_id = $comment->id;
                $p_history->save();

                $success = true;
            } catch (\Exception $e) {
                $success = false;
            }

            if ($success) {
                return redirect()->back()->with('success', 'Коментарий успешно добавлен');
            } else {
                return redirect()->back()->with('error', 'Произошла ошибка при добавлении коментария');
            }
        }
    }

    public function del($id) {
        $comment = Comment::find($id);
        $comment->deleted_at = date("Y-m-d H:i:s");

        if ($comment->save()) {
            return redirect()->back()->with('success', 'Коментарий успешно удален');
        } else {
            return redirect()->back()->with('error', 'Произошла ошибка при удалении коментария');
        }
    }

    static public function getProjectCloseComment($pid) {
        $comment = Comment::where('project_id', '=', $pid)
            ->where('close', '=', 1)
            ->whereNull('deleted_at')
            ->get();

        return $comment;
    }
}
