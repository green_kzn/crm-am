<?php

namespace App\Http\Controllers\Print_form;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Http\Controllers\Print_form\MailboxController as Mailbox;
use App\Http\Controllers\Print_form\UserController as Users;
use App\Http\Controllers\Print_form\Clients2Controller as Clients;
use App\Contact;
use App\Project;
use App\Clients_reference;
use App\Callback_other_task;
use App\Category_printform_reference;
use Storage;
use File;
use Mail;

class Mail2Controller extends Controller
{
    private $listBox;

    public function __construct() {
        $this->mbox = new Mailbox('{94.181.191.99:143/imap/tls/novalidate-cert}', 'tp@archimed-soft.ru', 'mevM53w7Tk', Storage::disk('local')->url(''));
    }

    public function connect($folder = 'INBOX') {
        try {
            $this->mbox = imap_open('{'. $this->host.':'.$this->port.'/imap/'.$this->encryption.'/'.$this->validate_cert.'}'.$folder, $this->username, $this->password);
            return $this->mbox;
        } catch (\Exception $e) {
            return "Ошибка подключения к серверу imap: ".$e->getMessage();
        }
    }

    public function getFolders() {
        $mailbox = new Mailbox('{94.181.191.99:143/imap/tls/novalidate-cert}', 'tp@archimed-soft.ru', 'mevM53w7Tk', Storage::disk('local')->url(''));
        return response()->json($mailbox->getMailboxes());
    }

    public function mail2() {
        print_r('Ok');
    }

    public function mail(AMImap $imap, $folder = 'INBOX') {
        $flag = 0;
        if ($folder == 'inbox') {
            $folder = strtoupper($folder);
        } else {
            $folder = ucfirst($folder);
        }

        foreach ($this->listBox as $lb) {
            if ($folder == $lb['shortpath']) {
                $flag = 1;
                $mailbox = $lb;
            }
        }

        if ($flag == 1) {
            dd($imap->getMailInBox($folder));
        } else {
            abort(404);
        }

    }

    public function inboxMail() {
        $mailsIds = $this->mbox->searchMailbox('ALL');

        foreach ($this->mbox->getMailsInfo($mailsIds) as $m) {
            $date = Carbon::parse($m->date);
            $email = $this->mbox->getMail($m->uid)->fromAddress;
            $client_q = Clients::getClientByEmail($email);
            if ($client_q) {
                $client_name = $client_q->name;
            } else {
                $client_name = '';
            }

            if ($date->minute < 10) {
                $min = '0'.$date->minute;
            } else {
                $min = $date->minute;
            }
            $attachment = $this->mbox->getMail($m->uid)->getAttachments();

            $mail[] = [
                'uid' => $m->uid,
                'seen' => $m->seen,
                'subject' => $m->subject,
                'sender' => $m->from,
                'attachment' => $attachment,
                'date' => $date->day.'.'.$date->month.'.'.$date->year.' '.$date->hour.':'.$min,
                'client' => $client_name,
            ];
        }
        usort($mail, function ($a, $b) {
            return strcmp($b["date"], $a["date"]);
        });

        //dd($this->mbox->getMailsInfo($mailsIds));

        return view('Print_form.mail.mail', [
            'header' => 'Входящие',
            'folders' => $this->getFolders(),
            'mail' => $mail
        ]);
    }

    public function getMessage($id)
    {
        $message = $this->mbox->getMail($id);
        //dd($message->getAttachments());
        $date = Carbon::createFromTimestamp(strtotime($message->headers->Date))
            ->timezone('Europe/Moscow')
            ->toDateTimeString();
        $text = htmlspecialchars_decode($message->textHtml);
        if ($message->getAttachments()) {
            foreach ($message->getAttachments() as $attachment) {
                $path = explode('/', $attachment->filePath);
                $file = File::extension($path[5]);

                $attachment_ar[] = [
                    'id' => $attachment->id,
                    'name' => $attachment->name,
                    'filePath' => $attachment->filePath,
                    'type' => $file
                ];
            }
        } else {
            $attachment_ar[] = '';
        }
        $task_exists = Callback_other_task::where('message_id', '=', $id)->count();

        $clients = Clients_reference::whereNull('deleted_at')->get();
        $project = Project::whereNull('deleted_at')->get();
        $category = Category_printform_reference::whereNull('deleted_at')
            ->get();

        if (count($category) > 0) {
            foreach ($category as $c) {
                $cat_ar[] = [
                    'id' => $c->id,
                    'name' => $c->category,
                ];
            }
        } else {
            $cat_ar = '';
        }


        return view('Print_form.mail.message', [
            'folders' => $this->mbox->getMailboxes(),
            'subject' => $message->subject,
            'fromaddress' => $message->headers->fromaddress,
            'date' => $date,
            'text' => $text,
            'attachment' => $attachment_ar,
            'clients' => $clients,
            'project' => $project,
            'message_id' => $id,
            'task_exists' => $task_exists,
            'category' => $cat_ar,
            'user' => Users::getAllUsers()
        ]);
    }

    public function archiveMail() {
        $mailbox = new Mailbox('{94.181.191.99:143/imap/tls/novalidate-cert}Archive', 'yamaletdinov@archimed-soft.ru', 'yMCKECh345', Storage::disk('local')->url(''));
        $mailsIds = $mailbox->searchMailbox('ALL');

        if ($mailsIds) {
            foreach ($mailsIds as $m_id) {
                $m = $this->mailbox->getMail($m_id);
                $date = Carbon::createFromTimestamp(strtotime($m->headers->Date))
                    ->timezone('Europe/Moscow')
                    ->toDateTimeString();

                $contact = Contact::where('email', '=', $m->headers->sender[0]->mailbox.'@'.$m->headers->sender[0]->host)
                    ->first();

                if (isset($contact)) {
                    $client = Clients_reference::find($contact['client_id']);
                    $client_name = $client->name;
                } else {
                    $client_name = '';
                }

                $mail[] = [
                    'id' => $m->id,
                    'personal' => $this->mailbox->decodeMimeStr($m->headers->sender[0]->personal, 'utf-8'),
                    'subject' => $this->mailbox->decodeMimeStr($m->headers->Subject, 'utf-8'),
                    'sender' => $m->headers->sender[0]->mailbox.'@'.$m->headers->sender[0]->host,
                    'date' => $date,
                    'client' => $client_name,
                    'short_text' => strip_tags($m->textHtml)
                ];
            }
        } else {
            $mail[] = '';
        }

        //dd($mail);

        return view('Print_form.mail.mail', [
            'header' => 'Архив',
            'folders' => $this->getFolders(),
            'mail' => $mail
        ]);
    }

    public function trashMail() {
        $mailbox = new Mailbox('{94.181.191.99:143/imap/tls/novalidate-cert}Trash', 'yamaletdinov@archimed-soft.ru', 'yMCKECh345', Storage::disk('local')->url(''));
        $mailsIds = $mailbox->searchMailbox('ALL');
        foreach ($mailbox->getMailsInfo($mailsIds) as $m) {
            $date = Carbon::parse($m->date);

            if ($date->minute < 10) {
                $min = '0'.$date->minute;
            } else {
                $min = $date->minute;
            }
            $attachment = $mailbox->getMail($m->uid)->getAttachments();

            $mail[] = [
                'uid' => $m->uid,
                'seen' => $m->seen,
                'subject' => $m->subject,
                'sender' => $m->from,
                'attachment' => $attachment,
                'date' => $date->day.'.'.$date->month.'.'.$date->year.' '.$date->hour.':'.$min,
                //'client' => $client_name,
            ];
        }
        //dd($mailsIds);
        usort($mail, function ($a, $b) {
            return strcmp($b["date"], $a["date"]);
        });

        return view('Print_form.mail.mail', [
            'header' => 'Корзина',
            'folders' => $this->getFolders(),
            'mail' => $mail
        ]);
    }

    public function sentMail() {
        $mailbox = new Mailbox('{94.181.191.99:143/imap/tls/novalidate-cert}Sent', 'yamaletdinov@archimed-soft.ru', 'yMCKECh345', Storage::disk('local')->url(''));
        $mailsIds = $mailbox->searchMailbox('ALL');
        foreach ($mailbox->getMailsInfo($mailsIds) as $m) {
            $date = Carbon::parse($m->date);

            if ($date->minute < 10) {
                $min = '0'.$date->minute;
            } else {
                $min = $date->minute;
            }
            $attachment = $mailbox->getMail($m->uid)->getAttachments();

            $mail[] = [
                'uid' => $m->uid,
                'seen' => $m->seen,
                'subject' => $m->subject,
                'sender' => $m->from,
                'attachment' => $attachment,
                'date' => $date->day.'.'.$date->month.'.'.$date->year.' '.$date->hour.':'.$min,
                //'client' => $client_name,
            ];
        }
        //dd($mailsIds);
        usort($mail, function ($a, $b) {
            return strcmp($b["date"], $a["date"]);
        });

        return view('Print_form.mail.mail', [
            'header' => 'Отправленные',
            'folders' => $this->getFolders(),
            'mail' => $mail
        ]);
    }

    public function junkMail() {
        $mailbox = new Mailbox('{94.181.191.99:143/imap/tls/novalidate-cert}Junk', 'yamaletdinov@archimed-soft.ru', 'yMCKECh345', Storage::disk('local')->url(''));
        $mailsIds = $mailbox->searchMailbox('ALL');

        if ($mailsIds) {
            foreach ($mailsIds as $m_id) {
                $m = $this->mailbox->getMail($m_id);
                $date = Carbon::createFromTimestamp(strtotime($m->headers->Date))
                    ->timezone('Europe/Moscow')
                    ->toDateTimeString();

                $contact = Contact::where('email', '=', $m->headers->sender[0]->mailbox.'@'.$m->headers->sender[0]->host)
                    ->first();

                if (isset($contact)) {
                    $client = Clients_reference::find($contact['client_id']);
                    $client_name = $client->name;
                } else {
                    $client_name = '';
                }

                $mail[] = [
                    'id' => $m->id,
                    'personal' => $this->mailbox->decodeMimeStr($m->headers->sender[0]->personal, 'utf-8'),
                    'subject' => $this->mailbox->decodeMimeStr($m->headers->Subject, 'utf-8'),
                    'sender' => $m->headers->sender[0]->mailbox.'@'.$m->headers->sender[0]->host,
                    'date' => $date,
                    'client' => $client_name,
                    'short_text' => strip_tags($m->textHtml)
                ];
            }
        } else {
            $mail[] = '';
        }

        //dd($mail);

        return view('Print_form.mail.mail', [
            'header' => 'Спам',
            'folders' => $this->getFolders(),
            'mail' => $mail
        ]);
    }

    public function draftsMail() {
        $mailbox = new Mailbox('{94.181.191.99:143/imap/tls/novalidate-cert}Drafts', 'yamaletdinov@archimed-soft.ru', 'yMCKECh345', Storage::disk('local')->url(''));
        $mailsIds = $mailbox->searchMailbox('ALL');

        if ($mailsIds) {
            foreach ($mailsIds as $m_id) {
                $m = $this->mailbox->getMail($m_id);
                $date = Carbon::createFromTimestamp(strtotime($m->headers->Date))
                    ->timezone('Europe/Moscow')
                    ->toDateTimeString();

                $contact = Contact::where('email', '=', $m->headers->sender[0]->mailbox.'@'.$m->headers->sender[0]->host)
                    ->first();

                if (isset($contact)) {
                    $client = Clients_reference::find($contact['client_id']);
                    $client_name = $client->name;
                } else {
                    $client_name = '';
                }

                $mail[] = [
                    'id' => $m->id,
                    'personal' => $this->mailbox->decodeMimeStr($m->headers->sender[0]->personal, 'utf-8'),
                    'subject' => $this->mailbox->decodeMimeStr($m->headers->Subject, 'utf-8'),
                    'sender' => $m->headers->sender[0]->mailbox.'@'.$m->headers->sender[0]->host,
                    'date' => $date,
                    'client' => $client_name,
                    'short_text' => strip_tags($m->textHtml)
                ];
            }
        } else {
            $mail[] = '';
        }

        //dd($mail);

        return view('Print_form.mail.mail', [
            'header' => 'Ченовики',
            'folders' => $this->getFolders(),
            'mail' => $mail
        ]);
    }

    public function getMailInFolder(Request $request) {
        if ($request->ajax()) {
            switch ($request->slug) {
                case 'inbox':
                    $mailsIds = $this->mbox->searchMailbox('ALL');

                    foreach ($mailsIds as $m_id) {
                        $m = $this->mbox->getMail($m_id);
                        $date = Carbon::createFromTimestamp(strtotime($m->headers->Date))
                            ->timezone('Europe/Moscow')
                            ->toDateTimeString();

                        $mail[] = [
                            'id' => $m->id,
                            'personal' => $this->mbox->decodeMimeStr($m->headers->sender[0]->personal, 'utf-8'),
                            'subject' => $this->mbox->decodeMimeStr($m->headers->Subject, 'utf-8'),
                            'sender' => $m->headers->sender[0]->mailbox.'@'.$m->headers->sender[0]->host,
                            'date' => $date,
                            'client' => 123,
                            'short_text' => strip_tags($m->textHtml)
                        ];
                    }
                    break;
                case 'archive':
                    $mailbox = new Mailbox('{94.181.191.99:143/imap/tls/novalidate-cert}Archive', 'yamaletdinov@archimed-soft.ru', 'yMCKECh345', Storage::disk('local')->url(''));
                    $mailsIds = $mailbox->searchMailbox('ALL');

                    if ($mailsIds) {
                        foreach ($mailsIds as $m_id) {
                            $m = $mailbox->getMail($m_id);
                            $date = Carbon::createFromTimestamp(strtotime($m->headers->Date))
                                ->timezone('Europe/Moscow')
                                ->toDateTimeString();
                            $mail[] = [
                                'id' => $m->id,
                                'personal' => $mailbox->decodeMimeStr($m->headers->sender[0]->personal, 'utf-8'),
                                'subject' => $mailbox->decodeMimeStr($m->headers->Subject, 'utf-8'),
                                'sender' => $m->headers->sender[0]->mailbox.'@'.$m->headers->sender[0]->host,
                                'date' => $date,
                                'client' => 123,
                                'short_text' => strip_tags($m->textHtml)
                            ];
                        }
                    } else {
                        $mail[] = '';
                    }
                    break;
                case 'trash':
                    $mailbox = new Mailbox('{94.181.191.99:143/imap/tls/novalidate-cert}Trash', 'yamaletdinov@archimed-soft.ru', 'yMCKECh345', Storage::disk('local')->url(''));
                    $mailsIds = $mailbox->searchMailbox('ALL');

                    if ($mailsIds) {
                        foreach ($mailsIds as $m_id) {
                            $m = $mailbox->getMail($m_id);
                            $date = Carbon::createFromTimestamp(strtotime($m->headers->Date))
                                ->timezone('Europe/Moscow')
                                ->toDateTimeString();
                            $mail[] = [
                                'id' => $m->id,
                                'personal' => $mailbox->decodeMimeStr($m->headers->sender[0]->personal, 'utf-8'),
                                'subject' => $mailbox->decodeMimeStr($m->headers->Subject, 'utf-8'),
                                'sender' => $m->headers->sender[0]->mailbox.'@'.$m->headers->sender[0]->host,
                                'date' => $date,
                                'client' => 123,
                                'short_text' => strip_tags($m->textHtml)
                            ];
                        }
                    } else {
                        $mail[] = '';
                    }
                    break;
                case 'sent':
                    $mailbox = new Mailbox('{94.181.191.99:143/imap/tls/novalidate-cert}Sent', 'yamaletdinov@archimed-soft.ru', 'yMCKECh345', Storage::disk('local')->url(''));
                    $mailsIds = $mailbox->searchMailbox('ALL');

                    if ($mailsIds) {
                        foreach ($mailsIds as $m_id) {
                            $m = $mailbox->getMail($m_id);
                            $date = Carbon::createFromTimestamp(strtotime($m->headers->Date))
                                ->timezone('Europe/Moscow')
                                ->toDateTimeString();
                            $mail[] = [
                                'id' => $m->id,
                                //'personal' => $mailbox->decodeMimeStr($m->headers->sender[0]->personal, 'utf-8'),
                                'subject' => $mailbox->decodeMimeStr($m->headers->Subject, 'utf-8'),
                                'sender' => $m->headers->sender[0]->mailbox.'@'.$m->headers->sender[0]->host,
                                'date' => $date,
                                'client' => 123,
                                'short_text' => strip_tags($m->textHtml)
                            ];
                        }
                    } else {
                        $mail[] = '';
                    }
                    break;
                case 'junk':
                    $mailbox = new Mailbox('{94.181.191.99:143/imap/tls/novalidate-cert}Junk', 'yamaletdinov@archimed-soft.ru', 'yMCKECh345', Storage::disk('local')->url(''));
                    $mailsIds = $mailbox->searchMailbox('ALL');

                    if ($mailsIds) {
                        foreach ($mailsIds as $m_id) {
                            $m = $mailbox->getMail($m_id);
                            $date = Carbon::createFromTimestamp(strtotime($m->headers->Date))
                                ->timezone('Europe/Moscow')
                                ->toDateTimeString();
                            $mail[] = [
                                'id' => $m->id,
                                'personal' => $mailbox->decodeMimeStr($m->headers->sender[0]->personal, 'utf-8'),
                                'subject' => $mailbox->decodeMimeStr($m->headers->Subject, 'utf-8'),
                                'sender' => $m->headers->sender[0]->mailbox.'@'.$m->headers->sender[0]->host,
                                'date' => $date,
                                'client' => 123,
                                'short_text' => strip_tags($m->textHtml)
                            ];
                        }
                    } else {
                        $mail[] = '';
                    }
                    break;
                case 'drafts':
                    $mailbox = new Mailbox('{94.181.191.99:143/imap/tls/novalidate-cert}Drafts', 'yamaletdinov@archimed-soft.ru', 'yMCKECh345', Storage::disk('local')->url(''));
                    $mailsIds = $mailbox->searchMailbox('ALL');

                    if ($mailsIds) {
                        foreach ($mailsIds as $m_id) {
                            $m = $mailbox->getMail($m_id);
                            $date = Carbon::createFromTimestamp(strtotime($m->headers->Date))
                                ->timezone('Europe/Moscow')
                                ->toDateTimeString();
                            $mail[] = [
                                'id' => $m->id,
                                'personal' => $mailbox->decodeMimeStr($m->headers->sender[0]->personal, 'utf-8'),
                                'subject' => $mailbox->decodeMimeStr($m->headers->Subject, 'utf-8'),
                                'sender' => $m->headers->sender[0]->mailbox.'@'.$m->headers->sender[0]->host,
                                'date' => $date,
                                'client' => 123,
                                'short_text' => strip_tags($m->textHtml)
                            ];
                        }
                    } else {
                        $mail[] = '';
                    }
                    break;
            }
            return response()->json($mail);
        }
    }

    public function newMessage() {
        return view('Print_form.mail.new', [
            'folders' => $this->mbox->getMailboxes()
        ]);
    }

    public function postNewMessage(Request $request) {
        $from = $request->session()->get('user')->email;

        $to  = $request->toMessage;
        $subject = $request->themeMessage;
        $message = $request->message;

        $headers  = "Content-type: text/html; charset=UTF-8 \r\n";
        $headers .= "From: Birthday Reminder <birthday@example.com>\r\n";

        $res = mail($to, $subject, $message, $headers);

        if ($res) {
            return redirect('/print_form/mail/inbox')->with(['success' => 'Письмо успешно отправлено']);
        } else {
            return redirect('/print_form/mail/inbox')->with(['error' => 'Произошла ошибка при отправке письма']);
        }
    }
}
