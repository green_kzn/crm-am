<?php

namespace App\Http\Controllers\Print_form;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Print_form\ProjectsController as Projects;
use App\Http\Controllers\Print_form\ClientsController as Clients;
use App\Http\Controllers\Print_form\CitysController as Citys;
use App\Http\Controllers\Print_form\UserController as User;
use App\Http\Controllers\Print_form\TasksController as Tasks;
use App\Http\Controllers\Print_form\CommentController as Comments;
use App\Project_task;
use App\Printform_file;
use App\Module_rule;
use App\Callback_other_task;
use App\Project_print_form;
use App\Comment;
use Sentinel;

class Calls2Controller extends Controller
{
    public function index() {
        if (session('perm')['calls.view']) {
            $p_tasks = Project_task::where('status', '=', 1)
                ->get();

            if (count($p_tasks) > 0) {
                foreach ($p_tasks as $pt) {
                    $name = Module_rule::find($pt->module_rule_id);
                    $project = Projects::getProjectById($pt->project_id);
                    $client = Clients::getClientById($project->client_id);
                    $city = Citys::getCityById($client->city_id);
                    $user = User::getUserById($pt->user_id);
                    //dd($client);

                    $tasks[] = [
                        'id' => $pt->id,
                        'prev_contact' => $pt->prev_contact,
                        'name' => $name->task,
                        'client' => $client,
                        'project' => $project,
                        'city' => $city,
                        'user' => $user,
                        'next_contact' => $pt->next_contact
                    ];
                }
            } else {
                $tasks = '';
            }

            //dd($tasks);

            return view('Print_form.calls.calls', [
                'tasks' => $tasks
            ]);
        } else {
            abort(503);
        }
    }

    public function edit($id) {
        $task = Tasks::getTaskById($id);
        $project = Projects::getProjectByTaskId($id);
        $client = Clients::getClientById($project->client_id);
        $comments = Comments::getCommentsByProjectId($project->id);

        $cur_date = date("j").'.'.date("n").'.'.date("Y");
        $cur_date_val = date("Y").'-'.date("m").'-'.date("d");
        $tomorrow = (date("j")+1).'.'.date("n").'.'.date("Y");
        $tomorrow_val = date("Y").'-'.date("m").'-'.(date("d")+1);
        $plus_one_day = (date("j")+2).'.'.date("n").'.'.date("Y");
        $plus_one_day_val = date("Y").'-'.date("m").'-'.(date("d")+2);
        $plus_two_day = (date("j")+3).'.'.date("n").'.'.date("Y");
        $plus_two_day_val = date("Y").'-'.date("m").'-'.(date("d")+3);
        $plus_three_day = (date("j")+4).'.'.date("n").'.'.date("Y");
        $plus_three_day_val = date("Y").'-'.date("m").'-'.(date("d")+4);

        return view('Print_form.calls.edit', [
            'task' => $task,
            'client' => $client,
            'comments' => $comments,

            'cur_date' => $cur_date,
            'cur_date_val' => $cur_date_val,
            'tomorrow' => $tomorrow,
            'tomorrow_val' => $tomorrow_val,
            'plus_one_day' => $plus_one_day,
            'plus_one_day_val' => $plus_one_day_val,
            'plus_two_day' => $plus_two_day,
            'plus_two_day_val' => $plus_two_day_val,
            'plus_three_day' => $plus_three_day,
            'plus_three_day_val' => $plus_three_day_val,
        ]);
    }

    public function createClientFromMail(Request $request) {
        //dd($request->all());
        if ($request->optionsRadios == 'client') {
            $data = json_decode($request->data);

            $other_task = new Callback_other_task();
            $other_task->client_id = $data->clientId;
            $other_task->project_id = $data->projectId;
            $other_task->type = 'mail';
            $other_task->message_id = $data->messageId;

            if ($other_task->save()) {
                $success = true;
            } else {
                $success = false;
            }

            $date =  explode(' ', date("Y-m-d H:i:s"));
            $cur_user = Sentinel::getUser();
            try {
                $comment = new Comment();
                $comment->project_id = $data->projectId;
                $comment->user_id = $cur_user->id;
                $comment->mail_client = 1;
                $comment->date = $date[0];
                $comment->time = $date[1];
                $comment->text = $data->comment;
                $comment->save();

                $success = true;
            } catch (\Exception $e) {
                $success = false;
            }
        } else {
            $data = json_decode($request->data);
            $files = json_decode($data->files);
            //dd($files);

            /*$number = Project_print_form::where('number', '>=', $request->queue_introduction)
                ->whereNull('deleted_at')
                ->get();*/
            $cur_user = Sentinel::getUser();
            //dd($data);

            $form = new Project_print_form();
            $form->number += 1;
            $form->project_id = $data->projectId;
            $form->user_id = $data->user;
            $form->observer_id = $cur_user->id;
            $form->name = $data->task_name;
            $form->category_printform_ref_id = $data->category;
            $form->max_hours = $data->max_hours;
            $form->is_printform = 1;
            $form->is_free = 1;
            $form->paid = 1;
            $form->module_rule_id = NULL;
            $form->module_id = NULL;
            $form->additional_id = NULL;
            $form->printform_status_ref_id = 1;
            $form->norm_contacts = NULL;
            $form->number_contacts = NULL;
            $form->date_implement = $data->finish_date;
            $form->date_implement_for_client = $data->finish_date_for_client;

            if ($form->save()) {
                $success = true;
            } else {
                $success = false;
            }

            foreach ($files as $f) {
                $name = explode('/', $f->filePath);

                $p_files = new Printform_file();
                $p_files->file = $name[5];
                $p_files->project_print_form_id = $form->id;
                $p_files->save();
            }

            $date =  explode(' ', date("Y-m-d H:i:s"));
            $cur_user = Sentinel::getUser();
            try {
                $comment = new Comment();
                $comment->project_id = $data->projectId;
                $comment->user_id = $cur_user->id;
                $comment->mail_form = 1;
                $comment->date = $date[0];
                $comment->time = $date[1];
                $comment->text = $data->comment;
                $comment->save();

                $success = true;
            } catch (\Exception $e) {
                $success = false;
            }
        }

        if ($success) {
            return redirect()->back()->with('success', 'Данные успешно обновлены');
        } else {
            return redirect()->back()->with('error', 'Произошла ошибка при обновлении данных');
        }
    }
}
