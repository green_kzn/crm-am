<?php

namespace App\Http\Controllers\Print_form;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Sentinel;

class NotificationController extends Controller
{
    public function getNotification(Request $request) {
       if ($request->ajax()) {
           /*$data[] = [
               'title' => 'Верните Линуса!',
               'body' => 'Тестирование HTML5 Notifications',
               'icon' => '/public/img/logo.jpg',
               'dir' => 'auto'
           ];
           return response()->json($data);*/
       }
    }

    public function markAsRead() {
        $cu = Sentinel::getUser();
        $u = User::find($cu->id);
        $u->unreadNotifications->markAsRead();
    }
}
