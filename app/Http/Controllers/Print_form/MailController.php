<?php

namespace App\Http\Controllers\Print_form;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Print_form\MailboxController as Mailbox;
use Carbon\Carbon;
use Storage;
// use App\Helpers\Contracts\AMImap;

class MailController extends Controller
{
    private $mbox;

    public function __construct() {
        $this->mbox = new Mailbox('{p130323.mail.ihc.ru:993/imap/ssl/novalidate-cert}INBOX', "forms@archimed-soft.ru", "GwrU5nD4A2", Storage::disk('local')->url(''));
    }
    
    public function getFolders() {
        return response()->json($this->mbox->getMailboxes());
    }

    public function getLeters() {
        // print_r($this->mbox->getMailHeader(6));
        $mbox = imap_open("{p130323.mail.ihc.ru:993/imap/ssl/novalidate-cert}INBOX", "forms@archimed-soft.ru", "GwrU5nD4A2")
            or die("не удалось подключиться: " . imap_last_error());
        
        $MC = imap_check($mbox);
        
        $result = imap_fetch_overview($mbox,"1:{$MC->Nmsgs}",0);
        // print_r($result);
        foreach ($result as $overview) {
            if(isset($overview->subject)) {
                $ar[] = array(
                    // 'subject' => utf8_decode(imap_utf8($overview[0]->subject)),
                    'subject' => iconv_mime_decode($overview->subject,0,"UTF-8"),
                    'from' => iconv_mime_decode($overview->from,0,"UTF-8"),
                    'size' => $overview->size,
                    'seen' => $overview->seen,
                    'flagged' => $overview->flagged,
                    'udate' => date("d.m.Y H:i", $overview->udate),
                    'to' => iconv_mime_decode($overview->to,0,"UTF-8"),
                    'message_id' => $overview->uid
                );
            } else {
                $ar[] = array(
                    // 'subject' => utf8_decode(imap_utf8($overview[0]->subject)),
                    'subject' => '',
                    'from' => iconv_mime_decode($overview->from,0,"UTF-8"),
                    'size' => $overview->size,
                    'seen' => $overview->seen,
                    'flagged' => $overview->flagged,
                    'udate' => date("d.m.Y H:i", $overview->udate),
                    'to' => iconv_mime_decode($overview->to,0,"UTF-8"),
                    'message_id' => $overview->uid
                );
            };
        }
        imap_close($mbox);
        return response()->json($ar);
    }

    public function getLeter(Request $request, $id) {
        $mbox = imap_open("{p130323.mail.ihc.ru:993/imap/ssl/novalidate-cert}INBOX", "forms@archimed-soft.ru", "GwrU5nD4A2")
            or die("не удалось подключиться: " . imap_last_error());
        $structure = imap_fetchstructure($mbox, $id);

        if(isset($structure->parts)){
            $parse = $this->parse_parts($structure->parts, $id);
        }else{
            $parse = $this->parse_parts($structure, $id);
        }
            
        if($parse){
            return $parse;
        }
    }
        
    private function parse_parts($parts, $msgno){
        $mbox = imap_open("{p130323.mail.ihc.ru:993/imap/ssl/novalidate-cert}INBOX", "forms@archimed-soft.ru", "GwrU5nD4A2")
            or die("не удалось подключиться: " . imap_last_error());
        $message = imap_fetchbody($mbox, $msgno, '1');
        
        if(!empty($parts->encoding)){
        switch($parts->encoding){
        /*case 1:
        $message = imap_8bit($message);
        break;*/
        
        case 3:
        $message = imap_base64($message);
        break;
        
        case 4:
        $message = imap_qprint($message);
        break;
        }
        }
        
        return $message;
    }
}
