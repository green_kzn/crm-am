<?php

namespace App\Http\Controllers\Print_form;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DocController extends Controller
{
    public function index() {
        if (session('perm')['doc.view']) {
            return view('Print_form.doc.doc');
        } else {
            abort(503);
        }
    }
}
