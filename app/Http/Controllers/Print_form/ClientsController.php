<?php

namespace App\Http\Controllers\Print_form;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Print_form\CitysController as Citys;
use App\Http\Controllers\Print_form\PostsController as Posts;
use App\Http\Controllers\Print_form\ContactController as Contacts;
use Sentinel;
use App\Clients_reference;
use App\Contact;
use App\Posts_reference;

class ClientsController extends Controller
{
    public function index() {
        if (session('perm')['clients_ref.view']) {
            $clients_q = Clients_reference::whereNull('deleted_at')
                ->get();

            if (count($clients_q) > 0) {
                foreach ($clients_q as $c) {
                    $clients[] = [
                        'id' => $c->id,
                        'name' => $c->name,
                        'city' => Citys::getCityById($c->city_id),
                    ];
                }
            } else {
                $clients = '';
            }

            return view('Print_form.references.clients.clients', [
                'clients' => $clients
            ]);
        } else {
            abort(503);
        }
    }

    public function add() {
        if (session('perm')['clients_ref.create']) {

            return view('Print_form.references.clients.add', [
                'citys' => Citys::getAllCitys(),
                'posts' => Posts::getAllPosts(),
            ]);
        } else {
            abort(503);
        }
    }

    public function postAdd(Request $request) {
        if (session('perm')['clients_ref.create']) {
            if (!$request->name_client) {
                return redirect()->back()->with('error', 'Укажите наименование клиента');
            }
            //$success = false;
            $cur_user = Sentinel::getUser();
            $clients_ar = json_decode($request->contacts);

            DB::transaction(function () use ($request, $clients_ar, $cur_user) {
                try {
                    $client = new Clients_reference();
                    $client->name = $request->name_client;
                    $client->city_id = $request->client_city;
                    $client->save();

                    foreach ($clients_ar as $ca) {
                        try {
                            $post = Posts_reference::where('name', '=', $ca->post)->firstOrFail();
                            $contact = new Contact();
                            $contact->first_name = $ca->name;
                            $contact->last_name = $ca->surname;
                            $contact->patronymic = $ca->patronymic;
                            $contact->post_id = $post->id;
                            $contact->phone = $ca->phone;
                            $contact->email = $ca->email;
                            $contact->client_id = $client->id;
                            $contact->user_id = $cur_user->id;
                            $contact->save();
                            $this->success = true;
                        } catch (\Exception $e) {
                            DB::rollback();
                            $success = false;
                        }
                    }
                } catch (\Exception $e) {
                    DB::rollback();
                    $this->success = false;
                }
            });

            if ($this->success) {
                return redirect('/print_form/clients')->with('success', 'Клиент успешно создан');
            } else {
                return redirect('/print_form/clients')->with('error', 'Произошла ошибка при создании клиента');
            }
        } else {
            abort(503);
        }
    }

    public function del($id) {
        if (session('perm')['clients_ref.delete']) {
            $client = Clients_reference::find($id);
            $client->deleted_at = date("Y-m-d H:i:s");

            if ($client->save()) {
                return redirect()->back()->with('success', 'Клиент успешно удален');
            } else {
                return redirect()->back()->with('error', 'Произошла ошибка при удалении клиента');
            }
        } else {
            abort(503);
        }
    }

    public function edit($id) {
        if (session('perm')['clients_ref.update']) {
            $client = Clients_reference::where('id', '=', $id)
                ->whereNull('deleted_at')
                ->firstOrFail();

            $contacts_q = Contacts::getClientAllContacts($client->id);
            foreach ($contacts_q as $c) {
                $post = Posts::getPostById($c->post_id);
                $contacts[] = [
                    'id' => $c->id,
                    'first_name' => $c->first_name,
                    'last_name' => $c->last_name,
                    'patronymic' => $c->patronymic,
                    'post' => $post->name,
                    'phone' => $c->phone,
                    'email' => $c->email,
                    'main' => $c->main,
                    'mail_rep' => $c->mail_rep,
                ];
            }

            return view('Print_form.references.clients.edit', [
                'citys' => Citys::getAllCitys(),
                'posts' => Posts::getAllPosts(),
                'client' => $client,
                'contacts' => $contacts
            ]);
        } else {
            abort(503);
        }
    }

    public function postEdit(Request $request, $id) {
        if (session('perm')['clients_ref.update']) {
            if (!$request->name_client) {
                return redirect()->back()->with('error', 'Укажите наименование клиента');
            }
            $cur_user = Sentinel::getUser();
            $clients_ar = json_decode($request->contacts);

            DB::transaction(function () use ($request, $clients_ar, $id, $cur_user) {
                try {
                    $client = Clients_reference::find($id);
                    $client->name = $request->name_client;
                    $client->city_id = $request->client_city;
                    $client->save();

                    foreach ($clients_ar as $ca) {
                        try {
                            $post = Posts_reference::where('name', '=', $ca->post)->firstOrFail();
                            if (isset($ca->id)) {
                                $contact = Contact::find($ca->id);
                                $contact->first_name = $ca->name;
                                $contact->last_name = $ca->surname;
                                $contact->patronymic = $ca->patronymic;
                                $contact->post_id = $post->id;
                                $contact->phone = $ca->phone;
                                $contact->email = $ca->email;
                                $contact->client_id = $client->id;
                                $contact->user_id = $cur_user->id;
                                $contact->save();
                                $this->success = true;
                            } else {
                                $contact = new Contact();
                                $contact->first_name = $ca->name;
                                $contact->last_name = $ca->surname;
                                $contact->patronymic = $ca->patronymic;
                                $contact->post_id = $post->id;
                                $contact->phone = $ca->phone;
                                $contact->email = $ca->email;
                                $contact->client_id = $client->id;
                                $contact->user_id = $cur_user->id;
                                $contact->save();
                                $this->success = true;
                            }

                        } catch (\Exception $e) {
                            DB::rollback();
                            $this->success = false;
                        }
                    }
                } catch (\Exception $e) {
                    DB::rollback();
                    $this->success = false;
                }
            });

            if ($this->success) {
                return redirect('/print_form/clients')->with('success', 'Клиент успешно создан');
            } else {
                return redirect('/print_form/clients')->with('error', 'Произошла ошибка при создании клиента');
            }
        } else {
            abort(503);
        }
    }

    static public function getAllClients() {
        $clients = Clients_reference::whereNull('deleted_at')->get();
        return $clients;
    }

    static public function getClientById($id) {
        $client = Clients_reference::whereNull('deleted_at')->firstOrFail();
        return $client;
    }
}
