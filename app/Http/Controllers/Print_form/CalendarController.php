<?php

namespace App\Http\Controllers\Print_form;

use Illuminate\Http\Request;
use App\Http\Controllers\PermissionsController as Permissions;
use App\Http\Controllers\Controller;
use Sentinel;
use App\Event;
use App\Project_task;

class CalendarController extends Controller
{
    public function index() {
        $perm = session('perm');

        if ($perm['calendar.view']) {
            $cur_user = Sentinel::getUser();

            $event = Event::where('user_id', '=', $cur_user->id)
                ->whereNULL('deleted_at')
                ->get();
            
            return view('Print_form.calendars.calendar', [
                'events' => $event,
                'perm' => $perm
            ]);
        } else {
            abort(503);
        }
    }

    public function addEvent(Request $request) {
        if (!$request->nameEvent) {
            return redirect()->back()->with('error', 'Не указано название события');
        }
        if (!$request->color) {
            return redirect()->back()->with('error', 'Не выбран цвет');
        }

        $cur_user = Sentinel::getUser();

        $event = new Event();
        $event->name = $request->nameEvent;
        $event->color = $request->color;
        $event->user_id = $cur_user->id;

        if ($event->save()) {
            return redirect()->back();
        } else {
            return redirect()->back()->with('error', 'Событие не сохранено');
        }
    }

    public function delEvent(Request $request, $id) {
        $event = Event::find($id);
        $event->deleted_at = date("Y-m-d H:i:s");
        if ($event->save()) {
            return redirect()->back();
        } else {
            return redirect()->back()->with('error', 'Ошибка при попытке удаления события');
        }
    }

    public function addProjectTask(Request $request) {
        if ($request->ajax()) {
            $endTime = strtotime($request->time) + strtotime('01:00');
            $count = Project_task::where('end', '>', $request->date.' '.$request->time)
                ->whereNull('deleted_at')
                ->count();

            if ($count == 0) {
                $project_task = Project_task::find($request->tid);
                $project_task->next_contact = $request->date.' '.$request->time;
                $project_task->end = $request->date.' '.date('H:i:s',$endTime);

                if ($project_task->save()) {
                    return response()->json(['status' => 'Ok']);
                } else {
                    return redirect()->back()->with('error', 'Ошибка при попытке удаления события');
                }
            } else {
                return response()->json(['status' => 'error', 'error' => 'Ошибка! На данную дату/время уже запланирована задача']);
            }
        }
    }
}
