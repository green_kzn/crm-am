<?php

namespace App\Http\Controllers\Printform;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrintFormsController extends Controller
{
    public function index() {
        return view('Printform.printforms.printforms');
    }
}
