<?php

namespace App\Http\Controllers\Print_form;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AMDateTimeController extends Controller
{
    static public function startProjectDate($startDate) {
        $nowDate = date("Y-m-d");
        if ($nowDate > $startDate) {
            return false;
        } else {
            return true;
        }
    }

    static public function finishProjectDate($finishDate) {
        $nowDate = date("Y-m-d");
        if ($nowDate > $finishDate) {
            return false;
        } else {
            return true;
        }
    }
}
