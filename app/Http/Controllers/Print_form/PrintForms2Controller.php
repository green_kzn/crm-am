<?php

namespace App\Http\Controllers\Print_form;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Print_form\Projects2Controller as Projects;
use App\Http\Controllers\Print_form\Comment2Controller as Comments;
use App\Http\Controllers\Print_form\User2Controller as Users;
use App\Http\Controllers\Print_form\Clients2Controller as Clients;
use App\Http\Controllers\Print_form\CitysController as Citys;
use App\Http\Controllers\Print_form\AMDateTimeController as AMDateTime;
use App\Project_task;
use App\Project_print_form;
use App\Category_printform_reference;
use App\Project_task_contact;
use App\Printform_file;
use App\Done_printform_file;
use App\Module_rule;
use App\Comment;
use App\Printform_history;
use App\Printform_comment;
use App\Printform_status_reference;
use App\Events\addFormToQueue;
use App\Events\addFormError;
use App\Events\FormChangeStatus;
use Sentinel;
use Illuminate\Support\Facades\Storage;
use App\Project;
use Carbon\Carbon;
use DB;
use Event;

class PrintForms2Controller extends Controller
{
    public function getRaw() {
        if (isset($_GET['count'])) $per_page = (int)$_GET['count']; else $per_page = 10;
        // $per_page = 10;
        // $per_page = (int)$per_page;
        
        if (isset($_GET['page'])) $page = ($_GET['page']-1); else $page=0;

        $count = DB::select('select count(`id`) as `count` from `project_print_forms`
        where `raw` = 1 and (`printform_status_ref_id` = 5 and `free` = 1 or (`paid` = 1 or `free` = 1)) and `attendant` IS NULL and `printform_status_ref_id` = 5 and `deleted_at` IS NULL');

        if ($per_page == 0) {
            $p_forms_raw = DB::select('select * from `project_print_forms`
                where `raw` = 1 and (`printform_status_ref_id` = 5 and `free` = 1 or (`paid` = 1 or `free` = 1)) and `attendant` IS NULL and `printform_status_ref_id` = 5 and `deleted_at` IS NULL');
                $num_pages = 1;
        } else {
            $start = abs($page*$per_page);
            $p_forms_raw = DB::select('select * from `project_print_forms`
            where `raw` = 1 and (`printform_status_ref_id` = 5 and `free` = 1 or (`paid` = 1 or `free` = 1)) and `attendant` IS NULL and `printform_status_ref_id` = 5 and `deleted_at` IS NULL
                limit '.$start.','.$per_page);
                $num_pages = ceil($count[0]->count/$per_page);
        }

        
        if (count($p_forms_raw) > 0) {
            foreach ($p_forms_raw as $fr) {
                $project = Projects::getProjectById($fr->project_id);
                $client = Clients::getClientById($project->client_id);
                $observer = Users::getUserById($fr->observer_id);
                $status = Printform_status_reference::find($fr->printform_status_ref_id);
                

                $form_raw[] = [
                    'created' => Carbon::parse($fr->updated_at)->format('d.m.Y'),
                    'id' => $fr->id,
                    'name' => $fr->name,
                    'client' => $client->name,
                    'project' => $project->name,
                    'status' => $status->name,
                    'observer' => $observer->last_name . ' ' . $observer->first_name,
                ];
            }
        } else {
            $form_raw = [];
        }

        return response()->json(['forms' => $form_raw, 'count' => $count[0]->count, 'count_pages' => $num_pages]);
    }

    public function getClose() {
        if (isset($_GET['count'])) $per_page = (int)$_GET['count']; else $per_page = 10;
        // $per_page = 10;
        // $per_page = (int)$per_page;
        
        if (isset($_GET['page'])) $page = ($_GET['page']-1); else $page=0;

        $count = DB::select('select count(`ppf`.`id`) as `count`
            from `project_print_forms` `ppf`
            where
            `ppf`.`printform_status_ref_id` = 9 and
            `ppf`.`raw` is null and
            `ppf`.`deleted_at` is null');

        if ($per_page == 0) {
            $p_forms_date = DB::select('select `ppf`.* 
                from `project_print_forms` `ppf`
                where
                `ppf`.`printform_status_ref_id` = 9 and
                `ppf`.`raw` is null and
                `ppf`.`deleted_at` is null
                order by `ppf`.`number` asc');
            $num_pages = 1;
        } else {
            $start = abs($page*$per_page);
            $p_forms_date = DB::select('select `ppf`.* 
                from `project_print_forms` `ppf`
                where
                `ppf`.`printform_status_ref_id` = 9 and
                `ppf`.`raw` is null and
                `ppf`.`deleted_at` is null
                order by `ppf`.`number` asc
                limit '.$start.','.$per_page);
            $num_pages = ceil($count[0]->count/$per_page);
        }

        // $p_forms_date = Project_print_form::whereNull('deleted_at')
        //     ->whereNull('raw')
        //     ->where('printform_status_ref_id', '=', 9)
        //     ->get();

        if (count($p_forms_date) > 0) {
            foreach ($p_forms_date as $pf) {
                $project = Projects::getProjectById($pf->project_id);
                $client = Clients::getClientById($project->client_id);
                $city = Citys::getCityById($client->city_id);
                if ($pf->user_id) {
                    $user = Users::getUserById($pf->user_id);
                } else {
                    $user = '';
                }
                $observer = Users::getUserById($pf->observer_id);
                $status = Printform_status_reference::find($pf->printform_status_ref_id);

                if ($pf->user_id) {
                    if ($pf->real_date) {
                        $form_date[] = [
                            'id' => $pf->id,
                            'max_hours' => $pf->max_hours,
                            'name' => $pf->name,
                            'client' => $client->name,
                            'project' => $project->name,
                            'city' => $city,
                            'user' => $user->last_name . ' ' . $user->first_name,
                            'is_free' => $pf->free,
                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                            'real_date' => Carbon::parse($pf->real_date)->format('d.m.Y'),
                            'status' => $status->name,
                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                            'number' => $pf->number,
                        ];
                    } else {
                        $form_date[] = [
                            'id' => $pf->id,
                            'max_hours' => $pf->max_hours,
                            'name' => $pf->name,
                            'client' => $client->name,
                            'project' => $project->name,
                            'city' => $city,
                            'user' => $user->last_name . ' ' . $user->first_name,
                            'is_free' => $pf->free,
                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                            'real_date' => '',
                            'status' => $status->name,
                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                            'number' => $pf->number,
                        ];
                    }
                } else {
                    if ($pf->real_date) {
                        $form_date[] = [
                            'id' => $pf->id,
                            'max_hours' => $pf->max_hours,
                            'name' => $pf->name,
                            'client' => $client->name,
                            'project' => $project->name,
                            'city' => $city,
                            'user' => '',
                            'is_free' => $pf->free,
                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                            'real_date' => Carbon::parse($pf->real_date)->format('d.m.Y'),
                            'status' => $status->name,
                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                            'number' => $pf->number,
                        ];
                    } else {
                        $form_date[] = [
                            'id' => $pf->id,
                            'max_hours' => $pf->max_hours,
                            'name' => $pf->name,
                            'client' => $client->name,
                            'project' => $project->name,
                            'city' => $city,
                            'user' => '',
                            'is_free' => $pf->free,
                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                            'real_date' => '',
                            'status' => $status->name,
                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                            'number' => $pf->number,
                        ];
                    }
                }
            }
        } else {
            $form_date[] = '';
        }

        return response()->json(['forms' => $form_date, 'count' => $count[0]->count, 'count_pages' => $num_pages]);
    }

    public function getPay() {
        $p_forms_pay = DB::select('select * from `project_print_forms`
            where `printform_status_ref_id` = 5 and `free` = 0 and `paid` = 0');

        if (count($p_forms_pay) > 0) {
            foreach ($p_forms_pay as $fr) {
                $project = Projects::getProjectById($fr->project_id);
                $client = Clients::getClientById($project->client_id);
                $observer = Users::getUserById($fr->observer_id);
                $status = Printform_status_reference::find($fr->printform_status_ref_id);
                //dd($project);

                $form_raw[] = [
                    'id' => $fr->id,
                    'name' => $fr->name,
                    'client' => $client->name,
                    'project' => $project->name,
                    'status' => $status->name,
                    'observer' => $observer->last_name . ' ' . $observer->first_name,
                ];
            }
        } else {
            $form_raw = [];
        }

        return response()->json($form_raw);
    }

    public function getAttendant() {
        $p_forms_count = DB::select("select count(`ppf`.`id`)
                                    from `project_print_forms` `ppf`
                                    left join `projects` `p` on `p`.`id` = `ppf`.`project_id`
                                    left join `printform_status_references` `psr` on `psr`.`id` = `ppf`.`printform_status_ref_id`
                                    left join `clients_references` `cr` on `p`.`client_id` = `cr`.`id`
                                    left join `users` `u` on `u`.`id` = `ppf`.`observer_id`
                                    where
                                    `ppf`.`raw` IS NULL and
                                    (`ppf`.`printform_status_ref_id` = 8 or
                                    `ppf`.`printform_status_ref_id` = 6) and
                                    `ppf`.`attendant` = 1 and
                                    `ppf`.`deleted_at` is null");

        if (count($p_forms_count) > 0) {
            $p_forms_date = DB::select("select `ppf`.*,
                                                `ppf`.`name` as `f_name`, 
                                                `p`.`name` as `p_name`,
                                                `psr`.`name` as `status`,
                                                `cr`.`name` as `client`,
                                                `u`.`first_name`,
                                                `u`.`last_name`,
                                                `uf`.`first_name` as `user_first_name`,
                                                `uf`.`last_name` as `user_last_name`
                                        from `project_print_forms` `ppf`
                                        left join `projects` `p` on `p`.`id` = `ppf`.`project_id`
                                        left join `printform_status_references` `psr` on `psr`.`id` = `ppf`.`printform_status_ref_id`
                                        left join `clients_references` `cr` on `p`.`client_id` = `cr`.`id`
                                        left join `users` `u` on `u`.`id` = `ppf`.`observer_id`
                                        left join `users` `uf` on `uf`.`id` = `ppf`.`user_id`
                                        where
                                        `ppf`.`raw` IS NULL and
                                        (`ppf`.`printform_status_ref_id` = 8 or
                                        `ppf`.`printform_status_ref_id` = 6) and
                                        `ppf`.`attendant` = 1 and
                                        `ppf`.`deleted_at` is null");
        }

        return response()->json($p_forms_date);
    }

    public function getQForms() {
        if (isset($_GET['count'])) $per_page = (int)$_GET['count']; else $per_page = 10;
        // $per_page = 10;
        // $per_page = (int)$per_page;
        
        if (isset($_GET['page'])) $page = ($_GET['page']-1); else $page=0;

        // $p_forms_date = Project_print_form::whereNull('deleted_at')
        //     ->where('printform_status_ref_id', '!=', 8)
        //     ->where('printform_status_ref_id', '!=', 7)
        //     ->where('printform_status_ref_id', '!=', 9)
        //     ->whereNull('raw')
        //     ->orderBy('date_implement', 'asc')
        //     ->paginate(10);

        $count = DB::select('select count(`ppf`.`id`) as `count`
            from `project_print_forms` `ppf`
            where
            `ppf`.`printform_status_ref_id` != 7 and
            `ppf`.`printform_status_ref_id` != 8 and
            `ppf`.`printform_status_ref_id` != 9 and
            `ppf`.`raw` is null and
            `ppf`.`deleted_at` is null');

        if ($per_page == 0) {
            $p_forms_date = DB::select('select `ppf`.* 
                from `project_print_forms` `ppf`
                where
                `ppf`.`printform_status_ref_id` != 7 and
                `ppf`.`printform_status_ref_id` != 8 and
                `ppf`.`printform_status_ref_id` != 9 and
                `ppf`.`raw` is null and
                `ppf`.`deleted_at` is null
                order by `ppf`.`number` asc');
            $num_pages = 1;
        } else {
            $start = abs($page*$per_page);
            $p_forms_date = DB::select('select `ppf`.* 
                from `project_print_forms` `ppf`
                where
                `ppf`.`printform_status_ref_id` != 7 and
                `ppf`.`printform_status_ref_id` != 8 and
                `ppf`.`printform_status_ref_id` != 9 and
                `ppf`.`raw` is null and
                `ppf`.`deleted_at` is null
                order by `ppf`.`number` asc
                limit '.$start.','.$per_page);
            $num_pages = ceil($count[0]->count/$per_page);
        }

        if (count($p_forms_date) > 0) {
            foreach ($p_forms_date as $pf) {
                $project = Projects::getProjectById($pf->project_id);
                $client = Clients::getClientById($project->client_id);
                $city = Citys::getCityById($client->city_id);
                if ($pf->user_id) {
                    $user = Users::getUserById($pf->user_id);
                } else {
                    $user = '';
                }
                $observer = Users::getUserById($pf->observer_id);
                $status = Printform_status_reference::find($pf->printform_status_ref_id);

                if ($pf->user_id) {
                    if ($pf->real_date) {
                        $form_date[] = [
                            'id' => $pf->id,
                            'max_hours' => $pf->max_hours,
                            'name' => $pf->name,
                            'client' => $client->name,
                            'project' => $project->name,
                            'city' => $city,
                            'user' => $user->last_name . ' ' . $user->first_name,
                            'is_free' => $pf->free,
                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                            'real_date' => Carbon::parse($pf->real_date)->format('d.m.Y'),
                            'status' => $status->name,
                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                            'number' => $pf->number,
                        ];
                    } else {
                        $form_date[] = [
                            'id' => $pf->id,
                            'max_hours' => $pf->max_hours,
                            'name' => $pf->name,
                            'client' => $client->name,
                            'project' => $project->name,
                            'city' => $city,
                            'user' => $user->last_name . ' ' . $user->first_name,
                            'is_free' => $pf->free,
                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                            'real_date' => '',
                            'status' => $status->name,
                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                            'number' => $pf->number,
                        ];
                    }
                } else {
                    if ($pf->real_date) {
                        $form_date[] = [
                            'id' => $pf->id,
                            'max_hours' => $pf->max_hours,
                            'name' => $pf->name,
                            'client' => $client->name,
                            'project' => $project->name,
                            'city' => $city,
                            'user' => '',
                            'is_free' => $pf->free,
                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                            'real_date' => Carbon::parse($pf->real_date)->format('d.m.Y'),
                            'status' => $status->name,
                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                            'number' => $pf->number,
                        ];
                    } else {
                        $form_date[] = [
                            'id' => $pf->id,
                            'max_hours' => $pf->max_hours,
                            'name' => $pf->name,
                            'client' => $client->name,
                            'project' => $project->name,
                            'city' => $city,
                            'user' => '',
                            'is_free' => $pf->free,
                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                            'real_date' => '',
                            'status' => $status->name,
                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                            'number' => $pf->number,
                        ];
                    }
                }
            }
        } else {
            $form_date[] = '';
        }
        
        return response()->json(['forms' => $form_date, 'count' => $count[0]->count, 'count_pages' => $num_pages]);
    }

    public function returnInQueue(Request $request, $id) {
        $form = Project_print_form::find($id);
        $form->user_id = NULL;
        $form->printform_status_ref_id = 5;

        if ($form->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    /**
     * return paginated records of users
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function getHotForms() {
        if (isset($_GET['count'])) $per_page = (int)$_GET['count']; else $per_page = 10;
        // $per_page = 10;
        // $per_page = (int)$per_page;
        
        if (isset($_GET['page'])) $page = ($_GET['page']-1); else $page=0;

        // $p_forms_date = Project_print_form::whereNull('deleted_at')
        //     ->where('printform_status_ref_id', '!=', 8)
        //     ->where('printform_status_ref_id', '!=', 7)
        //     ->where('printform_status_ref_id', '!=', 9)
        //     ->whereNull('raw')
        //     ->orderBy('date_implement', 'asc')
        //     ->paginate(10);

        $count = DB::select('select count(`ppf`.`id`) as `count`
            from `project_print_forms` `ppf`
            where
            `ppf`.`printform_status_ref_id` != 7 and
            `ppf`.`printform_status_ref_id` != 8 and
            `ppf`.`printform_status_ref_id` != 9 and
            `ppf`.`raw` is null and
            `ppf`.`deleted_at` is null');

        if ($per_page == 0) {
            $p_forms_date = DB::select('select `ppf`.* 
                from `project_print_forms` `ppf`
                where
                `ppf`.`printform_status_ref_id` != 7 and
                `ppf`.`printform_status_ref_id` != 8 and
                `ppf`.`printform_status_ref_id` != 9 and
                `ppf`.`raw` is null and
                `ppf`.`deleted_at` is null
                order by `ppf`.`date_implement` asc');
                $num_pages = 1;
        } else {
            $start = abs($page*$per_page);
            $p_forms_date = DB::select('select `ppf`.* 
                from `project_print_forms` `ppf`
                where
                `ppf`.`printform_status_ref_id` != 7 and
                `ppf`.`printform_status_ref_id` != 8 and
                `ppf`.`printform_status_ref_id` != 9 and
                `ppf`.`raw` is null and
                `ppf`.`deleted_at` is null
                order by `ppf`.`date_implement` asc
                limit '.$start.','.$per_page);
                $num_pages = ceil($count[0]->count/$per_page);
        }

        

        if (count($p_forms_date) > 0) {
            foreach ($p_forms_date as $pf) {
                // dd(Carbon::parse('')->diffInDays(Carbon::now()->addDay()));
                if (Carbon::parse($pf->date_implement_for_client)->diffInDays(Carbon::now()) == 0) {
                    $warning = 'yes';
                } else {
                    $warning = 'no';
                };
                if (Carbon::parse($pf->date_implement_for_client)->diffInDays(Carbon::now()->addDay()) == 0) {
                    $warning = 'yes';
                } else {
                    $warning = 'no';
                };
                if (Carbon::parse($pf->date_implement_for_client)->diffInDays(Carbon::now()->addDays(2)) == 0) {
                    $warning = 'yes';
                } else {
                    $warning = 'no';
                };
                if (Carbon::parse($pf->date_implement_for_client)->diffInDays(Carbon::now()->addDays(3)) == 0) {
                    $warning = 'yes';
                } else {
                    $warning = 'no';
                };
                
                $d = Carbon::now()->diffForHumans(Carbon::parse($pf->date_implement_for_client));
                $ar = explode(' ', $d);
                if ($ar[2] == 'после') {
                    $danger = 'yes';
                } else {
                    $danger = 'no';
                };
                $project = Projects::getProjectById($pf->project_id);
                $client = Clients::getClientById($project->client_id);
                $city = Citys::getCityById($client->city_id);
                
                if ($pf->user_id) {
                    $user = Users::getUserById($pf->user_id);
                } else {
                    $user = '';
                }
                $observer = Users::getUserById($pf->observer_id);
                $status = Printform_status_reference::find($pf->printform_status_ref_id);
                
                if ($pf->user_id) {
                    if ($pf->real_date) {
                    //     print_r($pf->free);
                    // die;
                        $form_date[] = [
                            'id' => $pf->id,
                            'max_hours' => $pf->max_hours,
                            // 'prev_contact' => $pf->prev_contact,
                            'name' => $pf->name,
                            'client' => $client->name,
                            'project' => $project->name,
                            'city' => $city,
                            'user' => $user->last_name . ' ' . $user->first_name,
                            'is_free' => $pf->free,
                            // 'next_contact' => $pf->next_contact,
                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                            'real_date' => Carbon::parse($pf->real_date)->format('d.m.Y'),
                            'status' => $status->name,
                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                            'number' => $pf->number,
                            'warning' => $warning,
                            'danger' => $danger
                        ];
                        
                    } else {
                        $form_date[] = [
                            'id' => $pf->id,
                            'max_hours' => $pf->max_hours,
                            // 'prev_contact' => $pf->prev_contact,
                            'name' => $pf->name,
                            'client' => $client->name,
                            'project' => $project->name,
                            'city' => $city,
                            'user' => $user->last_name . ' ' . $user->first_name,
                            'is_free' => $pf->free,
                            // 'next_contact' => $pf->next_contact,
                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                            'real_date' => '',
                            'status' => $status->name,
                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                            'number' => $pf->number,
                            'warning' => $warning,
                            'danger' => $danger
                        ];
                    }
                } else {
                    if ($pf->real_date) {
                        $form_date[] = [
                            'id' => $pf->id,
                            'max_hours' => $pf->max_hours,
                            // 'prev_contact' => $pf->prev_contact,
                            'name' => $pf->name,
                            'client' => $client->name,
                            'project' => $project->name,
                            'city' => $city,
                            'user' => '',
                            'is_free' => $pf->free,
                            // 'next_contact' => $pf->next_contact,
                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                            'real_date' => Carbon::parse($pf->real_date)->format('d.m.Y'), 
                            'status' => $status->name,
                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                            'number' => $pf->number,
                            'warning' => $warning,
                            'danger' => $danger
                        ];
                    } else {
                        $form_date[] = [
                            'id' => $pf->id,
                            'max_hours' => $pf->max_hours,
                            // 'prev_contact' => $pf->prev_contact,
                            'name' => $pf->name,
                            'client' => $client->name,
                            'project' => $project->name,
                            'city' => $city,
                            'user' => '',
                            'is_free' => $pf->free,
                            // 'next_contact' => $pf->next_contact,
                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                            'real_date' => '', 
                            'status' => $status->name,
                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                            'number' => $pf->number,
                            'warning' => $warning,
                            'danger' => $danger
                        ];
                    }
                }
                
            }
        } else {
            $form_date[] = '';
        }

        $p_forms_date2 = $p_forms_date;
        $p_forms_date2['data2'] = $form_date;

        // print_r($form_date);
        // die;
        return response()->json(['forms' => $form_date, 'count' => $count[0]->count, 'count_pages' => $num_pages]);
    }

    public function postEdit(Request $request, $id) {
        $data = json_decode($request->data);
        

        $p_form = Project_print_form::find($id);
        $p_form->printform_status_ref_id = $data->status;
        $p_form->name = $data->new_name;
        switch ($data->type) {
            case 1:
                $p_form->is_printform = 1;
                $p_form->is_screenform = 0;
                break;
            case 2:
                $p_form->is_printform = 0;
                $p_form->is_screenform = 1;
                break;
        }
        $p_form->category_printform_ref_id = $data->category;
        $p_form->max_hours = $data->norm;
        $p_form->date_implement = $data->date2;
        $p_form->date_implement_for_client = $data->date;
        $p_form->real_date = $data->date3;
        $p_form->number = $data->number;
        $p_form->user_id = $data->new_user;

        if ($p_form->save()) {
            $cur_user = Sentinel::getUser();
            $status = Printform_status_reference::find($data->status);
            
            broadcast(new FormChangeStatus($status->name, $p_form->name));
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function index() {
        $p_forms_raw = Project_print_form::whereNull('deleted_at')
            ->where('raw', '=', 1)
            ->orWhere('printform_status_ref_id', '=', 9)
            ->get();

        $all_statuses = Printform_status_reference::whereNull('deleted_at')
            ->get();

        $performers = Users::getAllUsers();

        if (count($p_forms_raw) > 0) {
            foreach ($p_forms_raw as $fr) {
                $project = Projects::getProjectById($fr->project_id);
                $client = Clients::getClientById($project->client_id);
                $observer = Users::getUserById($fr->observer_id);
                $status = Printform_status_reference::find($fr->printform_status_ref_id);
                //dd($project);

                $form_raw[] = [
                    'id' => $fr->id,
                    'name' => $fr->name,
                    'client' => $client->name,
                    'project' => $project->name,
                    'status' => $status->name,
                    'observer' => $observer->last_name . ' ' . $observer->first_name,
                ];
            }
        } else {
            $form_raw[] = '';
        }

        $p_forms_date = Project_print_form::whereNull('deleted_at')
            ->whereNull('raw')
            ->orderBy('date_implement', 'asc')
            ->get();

        //dd($p_forms_date);

        if (count($p_forms_date) > 0) {
            foreach ($p_forms_date as $pf) {
                $project = Projects::getProjectById($pf->project_id);
                $client = Clients::getClientById($project->client_id);
                $city = Citys::getCityById($client->city_id);
                if ($pf->user_id) {
                    $user = Users::getUserById($pf->user_id);
                } else {
                    $user = '';
                }
                $observer = Users::getUserById($pf->observer_id);
                $status = Printform_status_reference::find($pf->printform_status_ref_id);
                //dd($project);

                if ($pf->user_id) {
                    $form_date[] = [
                        'id' => $pf->id,
                        'max_hours' => $pf->max_hours,
                        'prev_contact' => $pf->prev_contact,
                        'name' => $pf->name,
                        'client' => $client->name,
                        'project' => $project->name,
                        'city' => $city,
                        'user' => $user->last_name . ' ' . $user->first_name,
                        'is_free' => $pf->is_free,
                        'next_contact' => $pf->next_contact,
                        'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                        'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                        'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                        'status' => $status->name,
                        'observer' => $observer->last_name . ' ' . $observer->first_name,
                        'number' => $pf->number,
                    ];
                } else {
                    $form_date[] = [
                        'id' => $pf->id,
                        'max_hours' => $pf->max_hours,
                        'prev_contact' => $pf->prev_contact,
                        'name' => $pf->name,
                        'client' => $client->name,
                        'project' => $project->name,
                        'city' => $city,
                        'user' => '',
                        'is_free' => $pf->is_free,
                        'next_contact' => $pf->next_contact,
                        'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                        'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                        'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                        'status' => $status->name,
                        'observer' => $observer->last_name . ' ' . $observer->first_name,
                        'number' => $pf->number,
                    ];
                }
            }
        } else {
            $form_date[] = '';
        }
        //dd($form);

        $p_forms_number = Project_print_form::whereNull('deleted_at')
            ->whereNull('raw')
            ->orderBy('number', 'asc')
            ->get();

        //dd($p_forms);

        if (count($p_forms_number) > 0) {
            foreach ($p_forms_number as $pf) {
                $project = Projects::getProjectById($pf->project_id);
                $client = Clients::getClientById($project->client_id);
                $city = Citys::getCityById($client->city_id);
                if ($pf->user_id) {
                    $user = Users::getUserById($pf->user_id);
                } else {
                    $user = '';
                }
                $observer = Users::getUserById($pf->observer_id);
                $status = Printform_status_reference::find($pf->printform_status_ref_id);
                //dd($client);

                if ($pf->user_id) {
                    $form_number[] = [
                        'id' => $pf->id,
                        'max_hours' => $pf->max_hours,
                        'prev_contact' => $pf->prev_contact,
                        'name' => $pf->name,
                        'client' => $client->name,
                        'project' => $project->name,
                        'city' => $city,
                        'user' => $user->last_name . ' ' . $user->first_name,
                        'is_free' => $pf->is_free,
                        'next_contact' => $pf->next_contact,
                        'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                        'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                        'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                        'status' => $status->name,
                        'observer' => $observer->last_name . ' ' . $observer->first_name,
                        'number' => $pf->number,
                    ];
                } else {
                    $form_number[] = [
                        'id' => $pf->id,
                        'max_hours' => $pf->max_hours,
                        'prev_contact' => $pf->prev_contact,
                        'name' => $pf->name,
                        'client' => $client->name,
                        'project' => $project->name,
                        'city' => $city,
                        'user' => '',
                        'is_free' => $pf->is_free,
                        'next_contact' => $pf->next_contact,
                        'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                        'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                        'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                        'status' => $status->name,
                        'observer' => $observer->last_name . ' ' . $observer->first_name,
                        'number' => $pf->number,
                    ];
                }
            }
        } else {
            $form_number[] = '';
        }

        //dd($form_date);
        return view('Print_form.printforms.printforms', [
            'tasks_date' => $form_date,
            'tasks_number' => $form_number,
            'form_raw' => $form_raw,
            'all_statuses' => $all_statuses,
            'performers' => $performers
        ]);
    }

    public function add() {
        $count_tasks = Project_print_form::whereNull('deleted_at')
            ->max('number');

        $category = Category_printform_reference::whereNull('deleted_at')
            ->get();

        $status = Printform_status_reference::whereNull('deleted_at')
            ->get();

        return view('Print_form.printforms.add', [
            'count_task' => $count_tasks,
            'users' => Users::getAllUsers(),
            'clients' => Clients::getAllClients(),
            'projects' => Projects::getAllProjects(),
            'category' => $category,
            'status' => $status
        ]);
    }

    public function calc($new_hour = 0) {
        $p_form = Project_print_form::where('printform_status_ref_id', '!=', 5)
            ->whereNull('deleted_at')
            ->get();

        $hour = 0;
        if (count($p_form) > 0) {
            foreach ($p_form as $pf) {
                $hour += $pf->max_hours;
            }
            $res = (($hour + $new_hour) / 8) / 2;

            $cur_date = date("j").'.'.date("n").'.'.date("Y");
            $date = date('Y-m-d', strtotime($cur_date . ' +'.round($res).' day'));
            //$tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +'.round($res).' day'));
        } else {
            $date = date("Y").'-'.date("m").'-'.date("d");
        }

        return $date;
    }

    public function postAdd(Request $request) {
        switch ($request->typeTask) {
            case 'introduction':
                if (!$request->taskName_introduction) {
                    return redirect()->back()->with('error', 'Укажите название задачи');
                }
                if (!$request->dateFinish_introduction) {
                    return redirect()->back()->with('error', 'Укажите дату реализации');
                }
                if (!$request->dateFinishForCustomers_introduction) {
                    return redirect()->back()->with('error', 'Укажите дату реализации для заказчика');
                }

                $count = Project_print_form::where('number', '=', $request->queue_introduction)
                    ->whereNull('deleted_at')
                    ->count();
                //dd($count);

                if ($count > 0) {
                    $number = Project_print_form::where('number', '>=', $request->queue_introduction)
                        ->whereNull('deleted_at')
                        ->get();

                    $new_date = $this->calc((int)$request->queue_introduction);

                    foreach ($number as $n) {
                        $new_number = Project_print_form::find($n->id);
                        $new_number->number += 1;

                        /*$res = ($request->maxCountHours_introduction / 8) / 2;
                        $cur_date = date("j").'.'.date("n").'.'.date("Y");
                        $date = date('Y-m-d', strtotime($request->dateFinish_introduction . ' +'.round($res).' day'));

                        dd($date);*/

                        $new_number->save();
                    }

                    if ($request->comment) {
                        $date =  explode(' ', date("Y-m-d H:i:s"));
                        $cur_user = Sentinel::getUser();
                        try {
                            $comment = new Comment();
                            $comment->project_id = $request->projectName_introduction;
                            $comment->user_id = $cur_user->id;
                            $comment->for_client = NULL;
                            $comment->close = 0;
                            $comment->date = $new_date;
                            $comment->time = $date[1];
                            $comment->text = $request->comment;
                            $comment->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }
                    }

                    try {
                        $p_form = new Project_print_form();
                        $p_form->number = $request->queue_introduction;
                        $p_form->project_id = $request->projectName_introduction;
                        $p_form->user_id = $request->user_introduction;
                        $p_form->observer_id = $request->implementer_introduction;
                        $p_form->name = $request->taskName_introduction;
                        $p_form->category_printform_ref_id = $request->category_introduction;
                        $p_form->max_hours = $request->maxCountHours_introduction;
                        $p_form->printform_status_ref_id = $request->status_introduction;
                        $p_form->date_implement = $request->dateFinish_introduction;
                        $p_form->date_implement_for_client = $request->dateFinishForCustomers_introduction;
                        if ($request->isPrintForm_introduction == 'on') {
                            $p_form->is_printform = 1;
                        } else {
                            $p_form->is_printform = 0;
                        }
                        if (isset($request->forPaid_introduction)) {
                            $p_form->is_free = 0;
                        } else {
                            $p_form->is_free = 1;
                        }
                        if (isset($request->paided_introduction)) {
                            $p_form->paid = 1;
                        } else {
                            $p_form->paid = 0;
                        }
                        $p_form->save();


                        $success = true;
                    } catch (\Exception $e) {
                        $success = false;
                    }

                    if ($request->hasFile('img')) {
                        $file = $request->file('img');
                        foreach ($file as $f) {
                            if ($request->hasFile('img')) {
                                //$f->move($destinationPath, $filename);
                                $content = file_get_contents($f);
                                $t = Storage::disk('local')->put($f->getClientOriginalName(), $content);

                                $pf_file = new Printform_file();
                                $pf_file->file = $f->getClientOriginalName();
                                $pf_file->project_print_form_id = $p_form->id;
                                $pf_file->save();
                            }
                        }
                    }
                } else {
                    if ($request->comment) {
                        $date =  explode(' ', date("Y-m-d H:i:s"));
                        $cur_user = Sentinel::getUser();
                        try {
                            $comment = new Comment();
                            $comment->project_id = $request->projectName_introduction;
                            $comment->user_id = $cur_user->id;
                            $comment->for_client = NULL;
                            $comment->close = 0;
                            $comment->date = $date[0];
                            $comment->time = $date[1];
                            $comment->text = $request->comment;
                            $comment->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }
                    }
                    
                    try {
                        $p_form = new Project_print_form();
                        $p_form->number = $request->queue_introduction;
                        $p_form->project_id = $request->projectName_introduction;
                        $p_form->user_id = $request->user_introduction;
                        $p_form->observer_id = $request->implementer_introduction;
                        $p_form->name = $request->taskName_introduction;
                        $p_form->category_printform_ref_id = $request->category_introduction;
                        $p_form->max_hours = $request->maxCountHours_introduction;
                        $p_form->printform_status_ref_id = $request->status_introduction;
                        $p_form->date_implement = $request->dateFinish_introduction;
                        $p_form->date_implement_for_client = $request->dateFinishForCustomers_introduction;
                        if ($request->isPrintForm_introduction == 'on') {
                            $p_form->is_printform = 1;
                        } else {
                            $p_form->is_printform = 0;
                        }
                        if (isset($request->forPaid_introduction)) {
                            $p_form->is_free = 1;
                        } else {
                            $p_form->is_free = 0;
                        }
                        if (isset($request->paided_introduction)) {
                            $p_form->paid = 1;
                        } else {
                            $p_form->paid = 0;
                        }
                        $p_form->save();

                        $success = true;
                    } catch (\Exception $e) {
                        $success = false;
                    }

                    if ($request->hasFile('img')) {
                        $file = $request->file('img');
                        foreach ($file as $f) {
                            if ($request->hasFile('img')) {
                                //$f->move($destinationPath, $filename);
                                $content = file_get_contents($f);
                                Storage::disk('local')->put($f->getClientOriginalName(), $content);

                                $pf_file = new Printform_file();
                                $pf_file->file = $f->getClientOriginalName();
                                $pf_file->project_print_form_id = $p_form->id;
                                $pf_file->save();
                            }
                        }
                    }
                }

                if ($success) {
                    return redirect('/print_form/print-form')->with('success', 'Данные успешно сохранены');
                } else {
                    return redirect('/print_form/print-form')->with('error', 'Произошла ошибка при сохранении данных');
                }

                break;
        }
    }

    public function upload(Request $request){
        dd($request->all());
        $page= new Page($request->except('img'));
        if ($request->hasFile('img')) {
            $file = $request->file('img');
            $destinationPath =  public_path().'/house/uploads/';
            $filename = str_random(20) .'.' . $file->getClientOriginalExtension() ?: 'png';
            $page->img = $filename;
            if ($request->hasFile('img')) {
                $request->file('img')->move($destinationPath, $filename);
            }
        }
        $page->save();

        return redirect()->route('page.index');
    }

    public function categoryRef() {
        $category = Category_printform_reference::whereNull('deleted_at')
            ->get();

        return view('Print_form.references.print_form.print_form', [
            'category' => $category
        ]);
    }

    public function addCategoryRef() {
        return view('Print_form.references.print_form.add');
    }

    public function postAddCategoryRef(Request $request) {
        if (!$request->name_category) {
            return redirect()->back()->with('error', 'Укажите категорию');
        }

        if (!$request->norm_category) {
            return redirect()->back()->with('error', 'Укажите норму для категории');
        }

        try {
            $category = new Category_printform_reference();
            $category->category = $request->name_category;
            $category->norm = $request->norm_category;
            $category->save();

            $success = true;
        } catch (\Exception $e) {
            $success = false;
        }

        if ($success) {
            return redirect('/print_form/printform-category')->with('success', 'Данные успешно сохранены');
        } else {
            return redirect('/print_form/printform-category')->with('error', 'Произошла ошибка при сохранении данных');
        }
    }

    public function delCategoryRef($id) {
        if (!$id) {
            return redirect()->back()->with('error', 'Произошла ошибка при сохранении данных');
        }

        try {
            $category = Category_printform_reference::find($id);
            $category->deleted_at = date("Y-m-d H:i:s");
            $category->save();

            $success = true;
        } catch (\Exception $e) {
            $success = false;
        }

        if ($success) {
            return redirect()->back()->with('success', 'Данные успешно сохранены');
        } else {
            return redirect()->back()->with('error', 'Произошла ошибка при сохранении данных');
        }
    }

    public function editCategoryRef($id) {
        $category = Category_printform_reference::find($id);

        return view('Print_form.references.print_form.edit', [
            'category' => $category
        ]);
    }

    public function postEdittCategoryRef(Request $request, $id) {
        if (!$id) {
            return redirect()->back()->with('error', 'Произошла ошибка при сохранении данных');
        }

        if (!$request->name_category) {
            return redirect()->back()->with('error', 'Укажите категорию');
        }

        if (!$request->norm_category) {
            return redirect()->back()->with('error', 'Укажите норму для категории');
        }

        try {
            $category = Category_printform_reference::find($id);
            $category->category = $request->name_category;
            $category->norm = $request->norm_category;
            $category->save();

            $success = true;
        } catch (\Exception $e) {
            $success = false;
        }

        if ($success) {
            return redirect('/print_form/printform-category')->with('success', 'Данные успешно сохранены');
        } else {
            return redirect('/print_form/printform-category')->with('error', 'Произошла ошибка при сохранении данных');
        }
    }

    public function getCategoryNorm(Request $request) {
        if ($request->ajax()) {
            $norm = Category_printform_reference::find($request->category);
            return response()->json($norm->norm);
        }
    }

    public function edit(Request $request, $id) {
        $f_task = Project_print_form::where('id', '=', $id)
            ->whereNull('deleted_at')
            ->first();
        $status = Printform_status_reference::find($f_task->printform_status_ref_id);
        $project = Projects::getProjectById($f_task->project_id);
        $client = Clients::getClientById($project->client_id);
        $category = Category_printform_reference::where('id', '=', $f_task->category_printform_ref_id)
            ->whereNull('deleted_at')
            ->first();
        if ($f_task->user_id) {
            $user = Users::getUserById($f_task->user_id);
        } else {
            $user = '';
        }
        $implement = Users::getUserById($f_task->observer_id);
        //$comments = Comments::getCommentsByProjectId($f_task->project_id);
        $file = Printform_file::where('project_print_form_id', '=', $id)
            ->whereNull('deleted_at')
            ->get();
        /*foreach ($file as $f) {
            $filePath[] = Storage::disk('google')->getIds($f->file);
        }*/
        //dd($user);
        if (count($file) > 0) {
            foreach ($file as $f) {
                $url = Storage::disk('local')->url($f->file);
                $size = Storage::size($f->file);
                $files[] = [
                    'name' => $f->file,
                    'img' => $url,
                    'size' => $size
                ];
            }
        } else {
            $files = '';
        }

        $done_file = Done_printform_file::where('project_print_form_id', '=', $id)
            ->whereNull('deleted_at')
            ->get();
        /*foreach ($file as $f) {
            $filePath[] = Storage::disk('google')->getIds($f->file);
        }*/
        if (count($done_file) > 0) {
            foreach ($done_file as $f) {
                $url = Storage::disk('local')->url($f->file);
                $size = Storage::size($f->file);
                $done_files[] = [
                    'name' => $f->file,
                    'img' => $url,
                    'size' => $size
                ];
            }
        } else {
            $done_files = '';
        }
        //dd($files);

        $comment_g = Printform_comment::where('form_id', '=', $id)
            ->whereNull('deleted_at')
            ->orderBy('created_at', 'desc')
            ->get();

        $cur_user = Sentinel::getUser();

        if (count($comment_g) > 0) {
            foreach ($comment_g as $c) {
                $user2 = Users::getUserById($c->user_id);
                if ($cur_user->id == $user2['id']) {
                    $comments[] = [
                        'id' => $c->id,
                        'text' => $c->comment,
                        'user_foto' => $user2['foto'],
                        'user_name' => $user2['first_name'],
                        'user_surname' => $user2['last_name'],
                        'date' => Carbon::parse($c->created_at)->format('d.m.Y'),
                        'time' => Carbon::parse($c->created_at)->format('H:m'),
                        'autor' => 1
                    ];
                } else {
                    $comments[] = [
                        'id' => $c->id,
                        'text' => $c->comment,
                        'user_foto' => $user2['foto'],
                        'user_name' => $user2['first_name'],
                        'user_surname' => $user2['last_name'],
                        'date' => Carbon::parse($c->created_at)->format('d.m.Y'),
                        'time' => Carbon::parse($c->created_at)->format('H:m'),
                        'autor' => 0
                    ];
                }
            }
        } else {
            $comments = '';
        }

        return view('Print_form.printforms.edit', [
            'form' => $f_task,
            'status' => $status->name,
            'project' => $project,
            'client' => $client,
            'category' => $category,
            'user' => $user,
            'implement' => $implement,
            'comments' => $comments,
            'file' => $files,
            'done_files' => $done_files
        ]);
    }

    public function delete(Request $request, $id) {
        $f_task = Project_print_form::find($id);

        $f_task->deleted_at = Carbon::now();

        if ($f_task->save()) {
            return redirect()->back()->with('success', 'Данные успешно обновлены');
        } else {
            return redirect()->back()->with('error', 'Произошла ошибка при удалении данных');
        }
    }

    public function saveField(Request $request) {
        if ($request->ajax()) {
            try {
                $form = Project_print_form::find($request->fid);
                $form->number = $request->new_number;
                $form->printform_status_ref_id = $request->new_status;
                $form->project_id = $request->new_project_id;
                $form->name = $request->new_form_name;
                if ($request->new_is_form == 'off') {
                    $form->is_printform = 0;
                } else {
                    $form->is_printform = 1;
                }
                $form->category_printform_ref_id = $request->new_category;
                $form->max_hours = $request->new_max_hour;
                $form->user_id = $request->new_user;
                $form->date_implement = $request->new_date_implement.' 00:00:00';
                $form->date_implement_for_client = $request->new_date_implement_for_client.' 00:00:00';
                $form->observer_id = $request->new_implement;
                if ($request->new_is_paid == 'off') {
                    $form->is_free = 0;
                } else {
                    $form->is_free = 1;
                }
                if ($request->new_ia_paided == 'off') {
                    $form->paid = 0;
                } else {
                    $form->paid = 1;
                }
                $form->save();
                $success = true;
            } catch (\Exception $e) {
                $success = false;
            }

            $cur_user = Sentinel::getUser();
            try {
                $p_history = new Printform_history();
                $p_history->printform_id = $form->id;
                $p_history->user_id = $cur_user->id;
                $p_history->printform_status_ref_id = $request->new_status;
                $p_history->comment_id = '640';
                $p_history->save();

                $success = true;
            } catch (\Exception $e) {
                $success = false;
            }

            if ($success) {
                return response()->json("Ok");
            } else {
                return response()->json("No");
            }
        }
    }

    public function getStatus(Request $request) {
        if ($request->ajax()) {
            $status = Printform_status_reference::whereNull('deleted_at')->get();
            return response()->json($status);
        }
    }

    public function getCategory(Request $request) {
        if ($request->ajax()) {
            $category = Category_printform_reference::whereNull('deleted_at')->get();
            return response()->json($category);
        }
    }

    public function calculate(Request $request, $id) {
        if ($request->ajax()) {
            $p_form = Project_print_form::where('printform_status_ref_id', '!=', 5)
                ->whereNull('attendant')
                ->whereNull('deleted_at')
                ->get();

            $form = Project_print_form::find($id);
            $hours = DB::select("SELECT SUM(`max_hours`) as sum FROM `project_print_forms` WHERE `printform_status_ref_id` != '5' AND `deleted_at` IS NULL AND `attendant` IS NULL");
            $d = Carbon::parse($form->created_at)
                ->addDay('7')
                // ->addHours($hours[0]->sum)
                ->format('Y-m-d');

            $d2 = Carbon::parse($form->created_at)
                ->addDay('14')
                // ->addHours($hours[0]->sum)
                ->format('Y-m-d');

            $d3 = Carbon::parse($form->created_at)
                ->addDay('7')
                ->addHours($hours[0]->sum)
                ->format('Y-m-d');

            // print_r($d);
            // if (count($p_form) > 0) {
            //     foreach ($p_form as $pf) {
            //         $hour += $pf->max_hours;
            //     }
            //     $res = ($hour / 8) / 2;

            //     // $cur_date = date("j").'.'.date("n").'.'.date("Y");
            //     $date = date('Y-m-d', strtotime($d . ' +'.round($res).' day'));
            //     //$tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +'.round($res).' day'));
            // } else {
            //     $date = date("Y").'-'.date("m").'-'.date("d");
            // }

            // for($i = 1; $i <= date("t"); $i++) {
            //     $weekend = date("w",strtotime($i.'.01.'.date("Y")));
            //     if($weekend==0 || $weekend==6) {
            //         if ($i<10) {
            //             $w_date[] = date("Y-01-").'0'.$i;
            //         } else {
            //             $w_date[] = date("Y-01-").$i;
            //         }
            //     };
            // };

            // foreach ($w_date as $wd) {
            //     for ($j=0; $j<=28; $j++) {
            //         $d = date('Y-m-d', strtotime($date . ' +'.$j.' day'));
            //         if ($d == $wd) {
            //             continue;
            //         } else {
            //             $res = $d;
            //             break;
            //         }
            //     }
            // }

            return response()->json(['d1' => $d, 'd2' => $d2, 'd3' => $d3]);
        }
    }

    public function getPForm($id) {
        $f_task = Project_print_form::where('id', '=', $id)
            ->whereNull('deleted_at')
            ->first();

        $statuses = Printform_status_reference::whereNull('deleted_at')
            ->get();
        $status = Printform_status_reference::find($f_task->printform_status_ref_id);
        $project = Projects::getProjectById($f_task->project_id);
        $client = Clients::getClientById($project->client_id);
        $implement = Users::getUserById($f_task->observer_id);
        //$comments = Comments::getCommentsByProjectId($f_task->project_id);
        $category = Category_printform_reference::whereNull('deleted_at')
            ->get();
        $file = Printform_file::where('project_print_form_id', '=', $id)
            ->whereNull('deleted_at')
            ->get();
        $count_tasks = Project_print_form::whereNull('deleted_at')
            ->max('number');
        /*foreach ($file as $f) {
            $filePath[] = Storage::disk('local')->getIds($f->file);
        }*/

        if (count($file) > 0) {
            foreach ($file as $f) {
                if ($f->file != NULL) {
                    $f2 = explode('.', $f->file);
                    
                    $files[] = [
                        'name' => $f->file,
                        'img' => $f->g_file,
                        'ext' => $f2[1],
                        'public_url' => $f->public_url,
                        'download_url' => $f->download_url,
                //                    'size' => $size
                    ];
                } else {
                    $files = '';
                }
            }
        } else {
            $files = '';
        }


        $comment_g = Printform_comment::where('form_id', '=', $id)
            ->whereNull('deleted_at')
            ->orderBy('id', 'desc')
            ->get();

        $cur_user = Sentinel::getUser();

        if (count($comment_g) > 0) {
            foreach ($comment_g as $c) {
                $user = Users::getUserById($c->user_id);
                if ($cur_user->id == $user['id']) {
                    $comments[] = [
                        'id' => $c->id,
                        'text' => $c->comment,
                        'user_foto' => $user['foto'],
                        'user_name' => $user['first_name'],
                        'user_surname' => $user['last_name'],
                        'date' => Carbon::parse($c->created_at)->format('d.m.Y'),
                        'time' => Carbon::parse($c->created_at)->format('H:m'),
                        'autor' => 1
                    ];
                } else {
                    $comments[] = [
                        'id' => $c->id,
                        'text' => $c->comment,
                        'user_foto' => $user['foto'],
                        'user_name' => $user['first_name'],
                        'user_surname' => $user['last_name'],
                        'date' => Carbon::parse($c->created_at)->format('d.m.Y'),
                        'time' => Carbon::parse($c->created_at)->format('H:m'),
                        'autor' => 0
                    ];
                }
            }
        } else {
            $comments = '';
        }

        return response()->json([
            'count_task' => $count_tasks,
            'form' => $f_task,
            'status' => $status,
            'projects' => Projects::getClientProjects($client->id),
            'project' => $project,
            'client' => $client,
            'clients' => Clients::getAllClients(),
            'implement' => $implement,
            'comments' => $comments,
            'file' => $files,
            'statuses' => $statuses,
            'category' => $category,
            'users' => Users::getAllUsers(),
        ]);
    }

    public function getEditForm($id) {
        $f_task = DB::select('select `pf`.*, `cpf`.`category`, `u`.`first_name`, `u`.`last_name`
            from `project_print_forms` `pf`
            left join `category_printform_references` `cpf` on `pf`.`category_printform_ref_id` = `cpf`.`id`
            left join `users` `u` on `u`.`id` = `pf`.`observer_id`
            where `pf`.`id` = '.$id);

        $f_task = $f_task[0];
        $f_task->date_implement_f = Carbon::parse($f_task->date_implement)->format('d.m.Y');
        $f_task->date_implement_for_client_f = Carbon::parse($f_task->date_implement_for_client)->format('d.m.Y');
        $f_task->date_implement = Carbon::parse($f_task->date_implement)->format('Y-m-d');
        $f_task->date_implement_for_client = Carbon::parse($f_task->date_implement_for_client)->format('Y-m-d');
        $f_task->real_date_f = Carbon::parse($f_task->real_date)->format('d.m.Y');
        $f_task->real_date = Carbon::parse($f_task->real_date)->format('Y-m-d');

        $statuses = Printform_status_reference::whereNull('deleted_at')
            ->get();
        $status = Printform_status_reference::find($f_task->printform_status_ref_id);
        $project = Projects::getProjectById($f_task->project_id);
        $client = Clients::getClientById($project->client_id);
        $implement = Users::getUserById($f_task->observer_id);
        //$comments = Comments::getCommentsByProjectId($f_task->project_id);
        $category = Category_printform_reference::whereNull('deleted_at')
            ->get();
        $file = Printform_file::where('project_print_form_id', '=', $id)
            ->whereNull('deleted_at')
            ->get();
        $count_tasks = Project_print_form::whereNull('deleted_at')
            ->max('number');
        $doneFiles = Done_printform_file::where('project_print_form_id', '=', $id)
            ->whereNull('deleted_at')
            ->get();
        /*foreach ($file as $f) {
            $filePath[] = Storage::disk('local')->getIds($f->file);
        }*/

        if (count($file) > 0) {
            foreach ($file as $f) {
                if ($f->file != NULL) {
                    $f2 = explode('.', $f->file);
                    
                    $files[] = [
                        'name' => $f->file,
                        'img' => $f->g_file,
                        'ext' => $f2[1],
                        'public_url' => $f->public_url,
                        'download_url' => $f->download_url,
                //                    'size' => $size
                    ];
                } else {
                    $files = '';
                }
            }
        } else {
            $files = '';
        }


        $comment_g = Printform_comment::where('form_id', '=', $id)
            ->whereNull('deleted_at')
            ->orderBy('id', 'desc')
            ->get();

        $cur_user = Sentinel::getUser();

        if (count($comment_g) > 0) {
            foreach ($comment_g as $c) {
                $user = Users::getUserById($c->user_id);
                if ($cur_user->id == $user['id']) {
                    $comments[] = [
                        'id' => $c->id,
                        'text' => $c->comment,
                        'user_foto' => $user['foto'],
                        'user_name' => $user['first_name'],
                        'user_surname' => $user['last_name'],
                        'date' => Carbon::parse($c->created_at)->format('d.m.Y'),
                        'time' => Carbon::parse($c->created_at)->format('H:m'),
                        'autor' => 1
                    ];
                } else {
                    $comments[] = [
                        'id' => $c->id,
                        'text' => $c->comment,
                        'user_foto' => $user['foto'],
                        'user_name' => $user['first_name'],
                        'user_surname' => $user['last_name'],
                        'date' => Carbon::parse($c->created_at)->format('d.m.Y'),
                        'time' => Carbon::parse($c->created_at)->format('H:m'),
                        'autor' => 0
                    ];
                }
            }
        } else {
            $comments = '';
        }

        if ($f_task->user_id != NULL) {
            $user = Users::getUserById($f_task->user_id);
        } else {
            $user = '';
        }

        return response()->json([
            'count_task' => $count_tasks,
            'form' => $f_task,
            'status' => $status,
            'projects' => Projects::getClientProjects($client->id),
            'project' => $project,
            'client' => $client,
            'clients' => Clients::getAllClients(),
            'implement' => $implement,
            'comments' => $comments,
            'file' => $files,
            'statuses' => $statuses,
            'category' => $category,
            'users' => Users::getAllUsers(),
            'user' => $user,
            'doneFiles' => $doneFiles
        ]);
    }

    public function getPayForm($id) {
        $f_task = DB::select('select `pf`.*, `cpf`.`category`
            from `project_print_forms` `pf`
            left join `category_printform_references` `cpf` on `pf`.`category_printform_ref_id` = `cpf`.`id`
            where `pf`.`id` = '.$id);

        $statuses = Printform_status_reference::whereNull('deleted_at')
            ->get();

        $status = Printform_status_reference::find($f_task[0]->printform_status_ref_id);
        $project = Projects::getProjectById($f_task[0]->project_id);
        $client = Clients::getClientById($project->client_id);
        $implement = Users::getUserById($f_task[0]->observer_id);
        //$comments = Comments::getCommentsByProjectId($f_task->project_id);
        $category = Category_printform_reference::whereNull('deleted_at')
            ->get();
        $file = Printform_file::where('project_print_form_id', '=', $id)
            ->whereNull('deleted_at')
            ->get();
        $count_tasks = Project_print_form::whereNull('deleted_at')
            ->max('number');
        /*foreach ($file as $f) {
            $filePath[] = Storage::disk('local')->getIds($f->file);
        }*/

        if (count($file) > 0) {
            foreach ($file as $f) {
                $url = Storage::disk('local')->url($f->file);
                $files[] = [
                    'name' => $f->file,
                    'img' => $url,
                    'ext' => explode('.', $f->file)[0],
                    'public_url' => $f->public_url,
                    'download_url' => $f->download_url,
                ];

            }
        } else {
            $files = '';
        }


        $comment_g = Printform_comment::where('form_id', '=', $id)
            ->whereNull('deleted_at')
            ->orderBy('id', 'desc')
            ->get();

        $cur_user = Sentinel::getUser();

        if (count($comment_g) > 0) {
            foreach ($comment_g as $c) {
                $user = Users::getUserById($c->user_id);
                if ($cur_user->id == $user['id']) {
                    $comments[] = [
                        'id' => $c->id,
                        'text' => $c->comment,
                        'user_foto' => $user['foto'],
                        'user_name' => $user['first_name'],
                        'user_surname' => $user['last_name'],
                        'date' => Carbon::parse($c->created_at)->format('d.m.Y'),
                        'time' => Carbon::parse($c->created_at)->format('H:m'),
                        'autor' => 1
                    ];
                } else {
                    $comments[] = [
                        'id' => $c->id,
                        'text' => $c->comment,
                        'user_foto' => $user['foto'],
                        'user_name' => $user['first_name'],
                        'user_surname' => $user['last_name'],
                        'date' => Carbon::parse($c->created_at)->format('d.m.Y'),
                        'time' => Carbon::parse($c->created_at)->format('H:m'),
                        'autor' => 0
                    ];
                }
            }
        } else {
            $comments = '';
        }

        return response()->json([
            'count_task' => $count_tasks,
            'form' => $f_task[0],
            'status' => $status,
            'projects' => Projects::getClientProjects($client->id),
            'project' => $project,
            'client' => $client,
            'clients' => Clients::getAllClients(),
            'implement' => $implement,
            'comments' => $comments,
            'file' => $files,
            'statuses' => $statuses,
            'category' => $category,
            'users' => Users::getAllUsers(),
        ]);
    }

    public function getAttendantById($id) {
        $f_task = DB::select('select `pf`.*, `cpf`.`category`, `u`.`first_name`, `u`.`last_name`
            from `project_print_forms` `pf`
            left join `category_printform_references` `cpf` on `pf`.`category_printform_ref_id` = `cpf`.`id`
            left join `users` `u` on `u`.`id` = `pf`.`observer_id`
            where `pf`.`id` = '.$id);

        $statuses = Printform_status_reference::whereNull('deleted_at')
            ->get();

        $status = Printform_status_reference::find($f_task[0]->printform_status_ref_id);
        $project = Projects::getProjectById($f_task[0]->project_id);
        $client = Clients::getClientById($project->client_id);
        $implement = Users::getUserById($f_task[0]->observer_id);
        //$comments = Comments::getCommentsByProjectId($f_task->project_id);
        $category = Category_printform_reference::whereNull('deleted_at')
            ->get();
        $file = Printform_file::where('project_print_form_id', '=', $id)
            ->whereNull('deleted_at')
            ->get();

        $count_tasks = Project_print_form::whereNull('deleted_at')
            ->max('number');
        /*foreach ($file as $f) {
            $filePath[] = Storage::disk('local')->getIds($f->file);
        }*/

        if (count($file) > 0) {
            foreach ($file as $f) {
                if ($f->file != NULL) {
                    $f2 = explode('.', $f->file);
                    
                    $files[] = [
                        'name' => $f->file,
                        'img' => $f->g_file,
                        'ext' => $f2[1],
                        'public_url' => $f->public_url,
                        'download_url' => $f->download_url,
                    ];
                } else {
                    $files = '';
                }
            }
        } else {
            $files = '';
        }
        


        $comment_g = Printform_comment::where('form_id', '=', $id)
            ->whereNull('deleted_at')
            ->orderBy('id', 'desc')
            ->get();

        $cur_user = Sentinel::getUser();

        if (count($comment_g) > 0) {
            foreach ($comment_g as $c) {
                $user = Users::getUserById($c->user_id);
                if ($cur_user->id == $user['id']) {
                    $comments[] = [
                        'id' => $c->id,
                        'text' => $c->comment,
                        'user_foto' => $user['foto'],
                        'user_name' => $user['first_name'],
                        'user_surname' => $user['last_name'],
                        'date' => Carbon::parse($c->created_at)->format('d.m.Y'),
                        'time' => Carbon::parse($c->created_at)->format('H:m'),
                        'autor' => 1
                    ];
                } else {
                    $comments[] = [
                        'id' => $c->id,
                        'text' => $c->comment,
                        'user_foto' => $user['foto'],
                        'user_name' => $user['first_name'],
                        'user_surname' => $user['last_name'],
                        'date' => Carbon::parse($c->created_at)->format('d.m.Y'),
                        'time' => Carbon::parse($c->created_at)->format('H:m'),
                        'autor' => 0
                    ];
                }
            }
        } else {
            $comments = '';
        }

        return response()->json([
            'count_task' => $count_tasks,
            'form' => $f_task[0],
            'status' => $status,
            'projects' => Projects::getClientProjects($client->id),
            'project' => $project,
            'client' => $client,
            'clients' => Clients::getAllClients(),
            'implement' => $implement,
            'comments' => $comments,
            'file' => $files,
            'statuses' => $statuses,
            'category' => $category,
            'users' => Users::getPFormUser2(),
        ]);
    }

    public function postAttendantEdit(Request $request) {
        $data = json_decode($request->data);
        
        $p_form = Project_print_form::find($data->id);

        $p_form->printform_status_ref_id = $data->curStatus;
        $p_form->user_id = $data->user;
        if ($p_form->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function addComment(Request $request) {
        $data = json_decode($request->data);
        $cur_user = Sentinel::getUser();

        $p_comment = new Printform_comment();
        $p_comment->user_id = $cur_user->id;
        $p_comment->form_id = $data->form_id;
        $p_comment->comment = $data->comment;

        if ($p_comment->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function raw($id) {
        $f_task = Project_print_form::where('id', '=', $id)
            ->whereNull('deleted_at')
            ->first();
        $statuses = Printform_status_reference::whereNull('deleted_at')
            ->get();
        $status = Printform_status_reference::find($f_task->printform_status_ref_id);
        $project = Projects::getProjectById($f_task->project_id);
        $client = Clients::getClientById($project->client_id);
        $implement = Users::getUserById($f_task->observer_id);
        //$comments = Comments::getCommentsByProjectId($f_task->project_id);
        $category = Category_printform_reference::whereNull('deleted_at')
            ->get();
        $file = Printform_file::where('project_print_form_id', '=', $id)
            ->whereNull('deleted_at')
            ->get();
        $count_tasks = Project_print_form::whereNull('deleted_at')
            ->max('number');
        /*foreach ($file as $f) {
            $filePath[] = Storage::disk('local')->getIds($f->file);
        }*/
        if (count($file) > 0) {
            foreach ($file as $f) {
                $url = Storage::disk('local')->url($f->file);
                $size = Storage::size($f->file);
                $files[] = [
                    'name' => $f->file,
                    'img' => $url,
                    'size' => $size
                ];
            }
        } else {
            $files = '';
        }

        $comment_g = Printform_comment::where('form_id', '=', $id)
            ->whereNull('deleted_at')
            ->orderBy('id', 'desc')
            ->get();

        $cur_user = Sentinel::getUser();

        if (count($comment_g) > 0) {
            foreach ($comment_g as $c) {
                $user = Users::getUserById($c->user_id);
                if ($cur_user->id == $user['id']) {
                    $comments[] = [
                        'id' => $c->id,
                        'text' => $c->comment,
                        'user_foto' => $user['foto'],
                        'user_name' => $user['first_name'],
                        'user_surname' => $user['last_name'],
                        'date' => Carbon::parse($c->created_at)->format('d.m.Y'),
                        'time' => Carbon::parse($c->created_at)->format('H:m'),
                        'autor' => 1
                    ];
                } else {
                    $comments[] = [
                        'id' => $c->id,
                        'text' => $c->comment,
                        'user_foto' => $user['foto'],
                        'user_name' => $user['first_name'],
                        'user_surname' => $user['last_name'],
                        'date' => Carbon::parse($c->created_at)->format('d.m.Y'),
                        'time' => Carbon::parse($c->created_at)->format('H:m'),
                        'autor' => 0
                    ];
                }
            }
        } else {
            $comments = '';
        }

        return view('Print_form.printforms.raw',[
            'count_task' => $count_tasks,
            'form' => $f_task,
            'status' => $status->id,
            'projects' => Projects::getClientProjects($client->id),
            'project' => $project,
            'client' => $client,
            'clients' => Clients::getAllClients(),
            'implement' => $implement,
            'comments' => $comments,
            'file' => $files,
            'statuses' => $statuses,
            'category' => $category,
            'users' => Users::getAllUsers(),
        ]);
    }

    public function postPay(Request $request) {
        $data = json_decode($request->data);
        $f_task = Project_print_form::where('id', '=', $data->id)
            ->whereNull('deleted_at')
            ->first();

        $f_task->category_printform_ref_id = $data->cur_category;
        $f_task->max_hours = $data->norm;
        switch ($data->type) {
            case 1:
                $f_task->is_printform = 1;
                $f_task->is_screenform = 0;
                break;
            case 2:
                $f_task->is_printform = 0;
                $f_task->is_screenform = 1;
                break;
        }

        if ($f_task->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function postRaw(Request $request, $id) {
        $data = json_decode($request->data);
        
        try {
            $f_task = Project_print_form::where('id', '=', $data->id)
                ->whereNull('deleted_at')
                ->first();

            if ($data->status != 10) {
                $f_task->raw = NULL;
                $f_task->number = $data->number;
                $f_task->printform_status_ref_id = $data->status;
                $f_task->category_printform_ref_id = $data->cur_category;
                $f_task->max_hours = $data->norm;
                switch ($data->type) {
                    case 1:
                        $f_task->is_printform = 1;
                        $f_task->is_screenform = 0;
                        break;
                    case 2:
                        $f_task->is_printform = 0;
                        $f_task->is_screenform = 1;
                        break;
                }
                $f_task->date_implement = $data->date2;
                $f_task->real_date = $data->date3;
                $f_task->date_implement_for_client = $data->date;
                $f_task->save();

                $cur_user = Sentinel::getUser();
                // print_r($cur_user);
                broadcast(new addFormToQueue($cur_user->first_name.' '.$cur_user->last_name));
            } else {
                $cur_user = Sentinel::getUser();
                broadcast(new addFormError($cur_user->first_name.' '.$cur_user->last_name));
                $f_task->printform_status_ref_id = $data->status;
                $f_task->save();
            }

            $success = true;
        } catch (\Exception $e) {
            $success = false;
        }

        // $cur_user = Sentinel::getUser();
        // try {
        //     $p_history = new Printform_history();
        //     $p_history->printform_id = $id;
        //     $p_history->user_id = $cur_user->id;
        //     $p_history->printform_status_ref_id = $data->status_introduction;
        //     $p_history->comment_id = '640';
        //     $p_history->save();

        //     $success = true;
        // } catch (\Exception $e) {
        //     $success = false;
        // }


        if ($success) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function searchRawForms(Request $request) {
        if ($request->ajax()) {
            $p_forms_raw = Project_print_form::whereNull('deleted_at')
                ->where('raw', '=', 1)
                ->get();

            if (count($p_forms_raw) > 0) {
                foreach ($p_forms_raw as $r) {
                    $project = Projects::getProjectById($r->project_id);
                    $client = Clients::getClientById($project->client_id);
                    $observer = Users::getUserById($r->observer_id);
                    $status = Printform_status_reference::find($r->printform_status_ref_id);
                    //dd($project);

                    if ($r->name) {
                        $form_raw[] = [
                            'id' => $r->id,
                            'name' => $r->name,
                            'client' => $client->name,
                            'project' => $project->name,
                            'status' => $status->name,
                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                        ];
                    } else {
                        $form_raw[] = [
                            'id' => $r->id,
                            'name' => '',
                            'client' => $client->name,
                            'project' => $project->name,
                            'status' => $status->name,
                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                        ];
                    }
                }
            } else {
                $form_raw[] = '';
            }

            $res = '';
            foreach ($form_raw as $f_raw) {
                if (preg_match('/'.$request->text.'/iU', $f_raw['name']) ||
                    preg_match('/'.$request->text.'/iU', $f_raw['id']) ||
                    preg_match('/'.$request->text.'/iU', $f_raw['client']) ||
                    preg_match('/'.$request->text.'/iU', $f_raw['project']) ||
                    preg_match('/'.$request->text.'/iU', $f_raw['status']) ||
                    preg_match('/'.$request->text.'/iU', $f_raw['observer'])
                ) {
                    $res[] = $f_raw;
                }
            }

            return response()->json($res);
        }
    }

    public function searchHotForms(Request $request) {
        if ($request->ajax()) {
            $p_forms_date = Project_print_form::whereNull('deleted_at')
                ->whereNull('raw')
                ->orderByDesc('date_implement')
                ->get();

            if (count($p_forms_date) > 0) {
                foreach ($p_forms_date as $pf) {
                    $project = Projects::getProjectById($pf->project_id);
                    $client = Clients::getClientById($project->client_id);
                    $city = Citys::getCityById($client->city_id);
                    if ($pf->user_id) {
                        $user = Users::getUserById($pf->user_id);
                    } else {
                        $user = '';
                    }
                    $observer = Users::getUserById($pf->observer_id);
                    $status = Printform_status_reference::find($pf->printform_status_ref_id);
                    //dd($project);

                    if ($pf->user_id) {
                        $form_date[] = [
                            'id' => $pf->id,
                            'max_hours' => $pf->max_hours,
                            'prev_contact' => $pf->prev_contact,
                            'name' => $pf->name,
                            'client' => $client->name,
                            'project' => $project->name,
                            'city' => $city,
                            'user' => $user->last_name . ' ' . $user->first_name,
                            'is_free' => $pf->is_free,
                            'next_contact' => $pf->next_contact,
                            'date2' => $pf->created_at,
                            'date_implement' => $pf->date_implement,
                            'date_implement_for_client' => $pf->date_implement_for_client,
                            'status' => $status->name,
                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                            'number' => $pf->number,
                        ];
                    } else {
                        $form_date[] = [
                            'id' => $pf->id,
                            'max_hours' => $pf->max_hours,
                            'prev_contact' => $pf->prev_contact,
                            'name' => $pf->name,
                            'client' => $client->name,
                            'project' => $project->name,
                            'city' => $city,
                            'user' => '',
                            'is_free' => $pf->is_free,
                            'next_contact' => $pf->next_contact,
                            'date2' => $pf->created_at,
                            'date_implement' => $pf->date_implement,
                            'date_implement_for_client' => $pf->date_implement_for_client,
                            'status' => $status->name,
                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                            'number' => $pf->number,
                        ];
                    }
                }
            } else {
                $form_date[] = '';
            }

            $res = '';
            foreach ($form_date as $f_date) {
                if (preg_match('/'.$request->text.'/iU', $f_date['name']) ||
                    preg_match('/'.$request->text.'/iU', $f_date['id']) ||
                    preg_match('/'.$request->text.'/iU', $f_date['max_hours']) ||
                    preg_match('/'.$request->text.'/iU', $f_date['prev_contact']) ||
                    preg_match('/'.$request->text.'/iU', $f_date['client']) ||
                    preg_match('/'.$request->text.'/iU', $f_date['project']) ||
                    preg_match('/'.$request->text.'/iU', $f_date['city']) ||
                    preg_match('/'.$request->text.'/iU', $f_date['user']) ||
                    preg_match('/'.$request->text.'/iU', $f_date['next_contact']) ||
                    preg_match('/'.$request->text.'/iU', $f_date['date2']) ||
                    preg_match('/'.$request->text.'/iU', $f_date['date_implement']) ||
                    preg_match('/'.$request->text.'/iU', $f_date['date_implement_for_client']) ||
                    preg_match('/'.$request->text.'/iU', $f_date['status']) ||
                    preg_match('/'.$request->text.'/iU', $f_date['observer']) ||
                    preg_match('/'.$request->text.'/iU', $f_date['number'])
                ) {
                    $res[] = $f_date;
                }
            }

            return response()->json($res);
        }
    }

    public function searchQueueForms(Request $request) {
        if ($request->ajax()) {
            $p_forms_number = Project_print_form::whereNull('deleted_at')
                ->whereNull('raw')
                ->orderBy('number', 'asc')
                ->get();

            //dd($p_forms);

            if (count($p_forms_number) > 0) {
                foreach ($p_forms_number as $pf) {
                    $project = Projects::getProjectById($pf->project_id);
                    $client = Clients::getClientById($project->client_id);
                    $city = Citys::getCityById($client->city_id);
                    if ($pf->user_id) {
                        $user = Users::getUserById($pf->user_id);
                    } else {
                        $user = '';
                    }
                    $observer = Users::getUserById($pf->observer_id);
                    $status = Printform_status_reference::find($pf->printform_status_ref_id);
                    //dd($client);

                    if ($pf->user_id) {
                        $form_number[] = [
                            'id' => $pf->id,
                            'max_hours' => $pf->max_hours,
                            'prev_contact' => $pf->prev_contact,
                            'name' => $pf->name,
                            'client' => $client->name,
                            'project' => $project->name,
                            'city' => $city,
                            'user' => $user->last_name . ' ' . $user->first_name,
                            'is_free' => $pf->is_free,
                            'next_contact' => $pf->next_contact,
                            'date2' => $pf->created_at,
                            'date_implement' => $pf->date_implement,
                            'date_implement_for_client' => $pf->date_implement_for_client,
                            'status' => $status->name,
                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                            'number' => $pf->number,
                        ];
                    } else {
                        $form_number[] = [
                            'id' => $pf->id,
                            'max_hours' => $pf->max_hours,
                            'prev_contact' => $pf->prev_contact,
                            'name' => $pf->name,
                            'client' => $client->name,
                            'project' => $project->name,
                            'city' => $city,
                            'user' => '',
                            'is_free' => $pf->is_free,
                            'next_contact' => $pf->next_contact,
                            'date2' => $pf->created_at,
                            'date_implement' => $pf->date_implement,
                            'date_implement_for_client' => $pf->date_implement_for_client,
                            'status' => $status->name,
                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                            'number' => $pf->number,
                        ];
                    }
                }
            } else {
                $form_number[] = '';
            }

            $res = '';
            foreach ($form_number as $f_num) {
                if (preg_match('/'.$request->text.'/iU', $f_num['name']) ||
                    preg_match('/'.$request->text.'/iU', $f_num['id']) ||
                    preg_match('/'.$request->text.'/iU', $f_num['max_hours']) ||
                    preg_match('/'.$request->text.'/iU', $f_num['prev_contact']) ||
                    preg_match('/'.$request->text.'/iU', $f_num['client']) ||
                    preg_match('/'.$request->text.'/iU', $f_num['project']) ||
                    preg_match('/'.$request->text.'/iU', $f_num['city']) ||
                    preg_match('/'.$request->text.'/iU', $f_num['user']) ||
                    preg_match('/'.$request->text.'/iU', $f_num['next_contact']) ||
                    preg_match('/'.$request->text.'/iU', $f_num['date2']) ||
                    preg_match('/'.$request->text.'/iU', $f_num['date_implement']) ||
                    preg_match('/'.$request->text.'/iU', $f_num['date_implement_for_client']) ||
                    preg_match('/'.$request->text.'/iU', $f_num['status']) ||
                    preg_match('/'.$request->text.'/iU', $f_num['observer']) ||
                    preg_match('/'.$request->text.'/iU', $f_num['number'])
                ) {
                    $res[] = $f_num;
                }
            }

            return response()->json($res);
        }
    }

    public function filterHotForms(Request $request) {
        if ($request->ajax()) {
            if ($request->status == 0 && $request->user == 0) {
                $p_forms_date = Project_print_form::whereNull('deleted_at')
                    ->whereNull('raw')
                    ->orderByDesc('date_implement')
                    ->get();
            }

            if ($request->status && $request->user == 0) {
                $p_forms_date = Project_print_form::whereNull('deleted_at')
                    ->where('printform_status_ref_id', '=', $request->status)
                    ->whereNull('raw')
                    ->orderByDesc('date_implement')
                    ->get();
            }

            if ($request->status == 0 && $request->user) {
                $p_forms_date = Project_print_form::whereNull('deleted_at')
                    ->where('user_id', '=', $request->user)
                    ->whereNull('raw')
                    ->orderByDesc('date_implement')
                    ->get();
            }

            if ($request->status && $request->user) {
                $p_forms_date = Project_print_form::whereNull('deleted_at')
                    ->where('printform_status_ref_id', '=', $request->status)
                    ->where('user_id', '=', $request->user)
                    ->whereNull('raw')
                    ->orderByDesc('date_implement')
                    ->get();
            }

            if (count($p_forms_date) > 0) {
                foreach ($p_forms_date as $pf) {
                    $project = Projects::getProjectById($pf->project_id);
                    $client = Clients::getClientById($project->client_id);
                    $city = Citys::getCityById($client->city_id);
                    if ($pf->user_id) {
                        $user = Users::getUserById($pf->user_id);
                    } else {
                        $user = '';
                    }
                    $observer = Users::getUserById($pf->observer_id);
                    $status = Printform_status_reference::find($pf->printform_status_ref_id);
                    //dd($project);

                    if ($pf->user_id) {
                        $form_date[] = [
                            'id' => $pf->id,
                            'max_hours' => $pf->max_hours,
                            'prev_contact' => $pf->prev_contact,
                            'name' => $pf->name,
                            'client' => $client->name,
                            'project' => $project->name,
                            'city' => $city,
                            'user' => $user->last_name . ' ' . $user->first_name,
                            'is_free' => $pf->is_free,
                            'next_contact' => $pf->next_contact,
                            'date2' => $pf->created_at,
                            'date_implement' => $pf->date_implement,
                            'date_implement_for_client' => $pf->date_implement_for_client,
                            'status' => $status->name,
                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                            'number' => $pf->number,
                        ];
                    } else {
                        $form_date[] = [
                            'id' => $pf->id,
                            'max_hours' => $pf->max_hours,
                            'prev_contact' => $pf->prev_contact,
                            'name' => $pf->name,
                            'client' => $client->name,
                            'project' => $project->name,
                            'city' => $city,
                            'user' => '',
                            'is_free' => $pf->is_free,
                            'next_contact' => $pf->next_contact,
                            'date2' => $pf->created_at,
                            'date_implement' => $pf->date_implement,
                            'date_implement_for_client' => $pf->date_implement_for_client,
                            'status' => $status->name,
                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                            'number' => $pf->number,
                        ];
                    }
                }
            } else {
                $form_date[] = '';
            }

            return response()->json($form_date);
        }
    }

    public function filterQueueForms(Request $request) {
        if ($request->ajax()) {
            if ($request->status == 0 && $request->user == 0) {
                $p_forms_date = Project_print_form::whereNull('deleted_at')
                    ->whereNull('raw')
                    ->orderBy('number', 'asc')
                    ->get();
            }

            if ($request->status && $request->user == 0) {
                $p_forms_date = Project_print_form::whereNull('deleted_at')
                    ->where('printform_status_ref_id', '=', $request->status)
                    ->whereNull('raw')
                    ->orderBy('number', 'asc')
                    ->get();
            }

            if ($request->status == 0 && $request->user) {
                $p_forms_date = Project_print_form::whereNull('deleted_at')
                    ->where('user_id', '=', $request->user)
                    ->whereNull('raw')
                    ->orderBy('number', 'asc')
                    ->get();
            }

            if ($request->status && $request->user) {
                $p_forms_date = Project_print_form::whereNull('deleted_at')
                    ->where('printform_status_ref_id', '=', $request->status)
                    ->where('user_id', '=', $request->user)
                    ->whereNull('raw')
                    ->orderBy('number', 'asc')
                    ->get();
            }

            if (count($p_forms_date) > 0) {
                foreach ($p_forms_date as $pf) {
                    $project = Projects::getProjectById($pf->project_id);
                    $client = Clients::getClientById($project->client_id);
                    $city = Citys::getCityById($client->city_id);
                    if ($pf->user_id) {
                        $user = Users::getUserById($pf->user_id);
                    } else {
                        $user = '';
                    }
                    $observer = Users::getUserById($pf->observer_id);
                    $status = Printform_status_reference::find($pf->printform_status_ref_id);
                    //dd($project);

                    if ($pf->user_id) {
                        $form_date[] = [
                            'id' => $pf->id,
                            'max_hours' => $pf->max_hours,
                            'prev_contact' => $pf->prev_contact,
                            'name' => $pf->name,
                            'client' => $client->name,
                            'project' => $project->name,
                            'city' => $city,
                            'user' => $user->last_name . ' ' . $user->first_name,
                            'is_free' => $pf->is_free,
                            'next_contact' => $pf->next_contact,
                            'date2' => $pf->created_at,
                            'date_implement' => $pf->date_implement,
                            'date_implement_for_client' => $pf->date_implement_for_client,
                            'status' => $status->name,
                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                            'number' => $pf->number,
                        ];
                    } else {
                        $form_date[] = [
                            'id' => $pf->id,
                            'max_hours' => $pf->max_hours,
                            'prev_contact' => $pf->prev_contact,
                            'name' => $pf->name,
                            'client' => $client->name,
                            'project' => $project->name,
                            'city' => $city,
                            'user' => '',
                            'is_free' => $pf->is_free,
                            'next_contact' => $pf->next_contact,
                            'date2' => $pf->created_at,
                            'date_implement' => $pf->date_implement,
                            'date_implement_for_client' => $pf->date_implement_for_client,
                            'status' => $status->name,
                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                            'number' => $pf->number,
                        ];
                    }
                }
            } else {
                $form_date[] = '';
            }

            return response()->json($form_date);
        }
    }

    public function upload2(Request $request, $id) {
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            foreach ($file as $f) {
                if ($request->hasFile('file')) {
                    //$f->move($destinationPath, $filename);
                    $content = file_get_contents($f);
                    $t = Storage::disk('local')->put($f->getClientOriginalName(), $content);

                    $pf_file = new Done_printform_file();
                    $pf_file->file = $f->getClientOriginalName();
                    $pf_file->project_print_form_id = $id;
                    $pf_file->save();
                }
            }
        }

        return redirect()->back();
    }

    public function filterForms(Request $request) {
        $data = json_decode($request->data);
        switch ($data->type) {
            case 'hot': 
                switch ($data->param) {
                    case 'all':
                        $p_forms_date = Project_print_form::whereNull('deleted_at')
                            ->where('printform_status_ref_id', '!=', 8)
                            ->where('printform_status_ref_id', '!=', 7)
                            ->where('printform_status_ref_id', '!=', 9)
                            ->whereNull('raw')
                            ->orderBy('date_implement', 'asc')
                            ->get();
                
                        // print_r($p_forms_date);
                
                        if (count($p_forms_date) > 0) {
                            foreach ($p_forms_date as $pf) {
                                // dd(Carbon::parse('')->diffInDays(Carbon::now()->addDay()));
                                if (Carbon::parse($pf->date_implement_for_client)->diffInDays(Carbon::now()) == 0) {
                                    $warning = 'yes';
                                } else {
                                    $warning = 'no';
                                };
                                if (Carbon::parse($pf->date_implement_for_client)->diffInDays(Carbon::now()->addDay()) == 0) {
                                    $warning = 'yes';
                                } else {
                                    $warning = 'no';
                                };
                                if (Carbon::parse($pf->date_implement_for_client)->diffInDays(Carbon::now()->addDays(2)) == 0) {
                                    $warning = 'yes';
                                } else {
                                    $warning = 'no';
                                };
                                if (Carbon::parse($pf->date_implement_for_client)->diffInDays(Carbon::now()->addDays(3)) == 0) {
                                    $warning = 'yes';
                                } else {
                                    $warning = 'no';
                                };
                                $d = Carbon::now()->diffForHumans(Carbon::parse($pf->date_implement_for_client));
                                $ar = explode(' ', $d);
                                if ($ar[2] == 'после') {
                                    $danger = 'yes';
                                } else {
                                    $danger = 'no';
                                };
                                $project = Projects::getProjectById($pf->project_id);
                                $client = Clients::getClientById($project->client_id);
                                $city = Citys::getCityById($client->city_id);
                                if ($pf->user_id) {
                                    $user = Users::getUserById($pf->user_id);
                                } else {
                                    $user = '';
                                }
                                $observer = Users::getUserById($pf->observer_id);
                                $status = Printform_status_reference::find($pf->printform_status_ref_id);
                
                                if ($pf->user_id) {
                                    if ($pf->real_date) {
                                        $form_date[] = [
                                            'id' => $pf->id,
                                            'max_hours' => $pf->max_hours,
                                            'prev_contact' => $pf->prev_contact,
                                            'name' => $pf->name,
                                            'client' => $client->name,
                                            'project' => $project->name,
                                            'city' => $city,
                                            'user' => $user->last_name . ' ' . $user->first_name,
                                            'is_free' => $pf->is_free,
                                            'next_contact' => $pf->next_contact,
                                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                            'real_date' => Carbon::parse($pf->real_date)->format('d.m.Y'),
                                            'status' => $status->name,
                                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                                            'number' => $pf->number,
                                            'warning' => $warning,
                                            'danger' => $danger
                                        ];
                                    } else {
                                        $form_date[] = [
                                            'id' => $pf->id,
                                            'max_hours' => $pf->max_hours,
                                            'prev_contact' => $pf->prev_contact,
                                            'name' => $pf->name,
                                            'client' => $client->name,
                                            'project' => $project->name,
                                            'city' => $city,
                                            'user' => $user->last_name . ' ' . $user->first_name,
                                            'is_free' => $pf->is_free,
                                            'next_contact' => $pf->next_contact,
                                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                            'real_date' => '',
                                            'status' => $status->name,
                                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                                            'number' => $pf->number,
                                            'warning' => $warning,
                                            'danger' => $danger
                                        ];
                                    }
                                } else {
                                    if ($pf->real_date) {
                                        $form_date[] = [
                                            'id' => $pf->id,
                                            'max_hours' => $pf->max_hours,
                                            'prev_contact' => $pf->prev_contact,
                                            'name' => $pf->name,
                                            'client' => $client->name,
                                            'project' => $project->name,
                                            'city' => $city,
                                            'user' => '',
                                            'is_free' => $pf->is_free,
                                            'next_contact' => $pf->next_contact,
                                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                            'real_date' => Carbon::parse($pf->real_date)->format('d.m.Y'), 
                                            'status' => $status->name,
                                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                                            'number' => $pf->number,
                                            'warning' => $warning,
                                            'danger' => $danger
                                        ];
                                    } else {
                                        $form_date[] = [
                                            'id' => $pf->id,
                                            'max_hours' => $pf->max_hours,
                                            'prev_contact' => $pf->prev_contact,
                                            'name' => $pf->name,
                                            'client' => $client->name,
                                            'project' => $project->name,
                                            'city' => $city,
                                            'user' => '',
                                            'is_free' => $pf->is_free,
                                            'next_contact' => $pf->next_contact,
                                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                            'real_date' => '', 
                                            'status' => $status->name,
                                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                                            'number' => $pf->number,
                                            'warning' => $warning,
                                            'danger' => $danger
                                        ];
                                    }
                                }
                            }
                        } else {
                            $form_date[] = '';
                        }
                        break;
                    case 'myForms':
                        $user = Sentinel::getUser();
                        $p_forms_date = Project_print_form::whereNull('deleted_at')
                            ->where('printform_status_ref_id', '!=', 8)
                            ->where('printform_status_ref_id', '!=', 7)
                            ->where('printform_status_ref_id', '!=', 9)
                            ->where('user_id', '=', $user->id)
                            ->whereNull('raw')
                            ->orderBy('date_implement', 'asc')
                            ->get();
            
                        if (count($p_forms_date) > 0) {
                            foreach ($p_forms_date as $pf) {
                                // dd(Carbon::parse('')->diffInDays(Carbon::now()->addDay()));
                                if (Carbon::parse($pf->date_implement_for_client)->diffInDays(Carbon::now()) == 0) {
                                    $warning = 'yes';
                                } else {
                                    $warning = 'no';
                                };
                                if (Carbon::parse($pf->date_implement_for_client)->diffInDays(Carbon::now()->addDay()) == 0) {
                                    $warning = 'yes';
                                } else {
                                    $warning = 'no';
                                };
                                if (Carbon::parse($pf->date_implement_for_client)->diffInDays(Carbon::now()->addDays(2)) == 0) {
                                    $warning = 'yes';
                                } else {
                                    $warning = 'no';
                                };
                                if (Carbon::parse($pf->date_implement_for_client)->diffInDays(Carbon::now()->addDays(3)) == 0) {
                                    $warning = 'yes';
                                } else {
                                    $warning = 'no';
                                };
                                $d = Carbon::now()->diffForHumans(Carbon::parse($pf->date_implement_for_client));
                                $ar = explode(' ', $d);
                                if ($ar[2] == 'после') {
                                    $danger = 'yes';
                                } else {
                                    $danger = 'no';
                                };
                                $project = Projects::getProjectById($pf->project_id);
                                $client = Clients::getClientById($project->client_id);
                                $city = Citys::getCityById($client->city_id);
                                if ($pf->user_id) {
                                    $user = Users::getUserById($pf->user_id);
                                } else {
                                    $user = '';
                                }
                                $observer = Users::getUserById($pf->observer_id);
                                $status = Printform_status_reference::find($pf->printform_status_ref_id);
                
                                if ($pf->user_id) {
                                    if ($pf->real_date) {
                                        $form_date[] = [
                                            'id' => $pf->id,
                                            'max_hours' => $pf->max_hours,
                                            'prev_contact' => $pf->prev_contact,
                                            'name' => $pf->name,
                                            'client' => $client->name,
                                            'project' => $project->name,
                                            'city' => $city,
                                            'user' => $user->last_name . ' ' . $user->first_name,
                                            'is_free' => $pf->is_free,
                                            'next_contact' => $pf->next_contact,
                                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                            'real_date' => Carbon::parse($pf->real_date)->format('d.m.Y'),
                                            'status' => $status->name,
                                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                                            'number' => $pf->number,
                                            'warning' => $warning,
                                            'danger' => $danger
                                        ];
                                    } else {
                                        $form_date[] = [
                                            'id' => $pf->id,
                                            'max_hours' => $pf->max_hours,
                                            'prev_contact' => $pf->prev_contact,
                                            'name' => $pf->name,
                                            'client' => $client->name,
                                            'project' => $project->name,
                                            'city' => $city,
                                            'user' => $user->last_name . ' ' . $user->first_name,
                                            'is_free' => $pf->is_free,
                                            'next_contact' => $pf->next_contact,
                                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                            'real_date' => '',
                                            'status' => $status->name,
                                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                                            'number' => $pf->number,
                                            'warning' => $warning,
                                            'danger' => $danger
                                        ];
                                    }
                                } else {
                                    if ($pf->real_date) {
                                        $form_date[] = [
                                            'id' => $pf->id,
                                            'max_hours' => $pf->max_hours,
                                            'prev_contact' => $pf->prev_contact,
                                            'name' => $pf->name,
                                            'client' => $client->name,
                                            'project' => $project->name,
                                            'city' => $city,
                                            'user' => '',
                                            'is_free' => $pf->is_free,
                                            'next_contact' => $pf->next_contact,
                                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                            'real_date' => Carbon::parse($pf->real_date)->format('d.m.Y'), 
                                            'status' => $status->name,
                                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                                            'number' => $pf->number,
                                            'warning' => $warning,
                                            'danger' => $danger
                                        ];
                                    } else {
                                        $form_date[] = [
                                            'id' => $pf->id,
                                            'max_hours' => $pf->max_hours,
                                            'prev_contact' => $pf->prev_contact,
                                            'name' => $pf->name,
                                            'client' => $client->name,
                                            'project' => $project->name,
                                            'city' => $city,
                                            'user' => '',
                                            'is_free' => $pf->is_free,
                                            'next_contact' => $pf->next_contact,
                                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                            'real_date' => '', 
                                            'status' => $status->name,
                                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                                            'number' => $pf->number,
                                            'warning' => $warning,
                                            'danger' => $danger
                                        ];
                                    }
                                }
                            }
                        } else {
                            $form_date[] = '';
                        }
                        break;
                    case 'myCloseForms':
                        $user = Sentinel::getUser();
                        $p_forms_date = Project_print_form::whereNull('deleted_at')
                            ->where('printform_status_ref_id', '=', 9)
                            ->where('user_id', '=', $user->id)
                            ->whereNull('raw')
                            ->orderBy('date_implement', 'asc')
                            ->get();
            
                        if (count($p_forms_date) > 0) {
                            foreach ($p_forms_date as $pf) {
                                // dd(Carbon::parse('')->diffInDays(Carbon::now()->addDay()));
                                if (Carbon::parse($pf->date_implement_for_client)->diffInDays(Carbon::now()) == 0) {
                                    $warning = 'yes';
                                } else {
                                    $warning = 'no';
                                };
                                if (Carbon::parse($pf->date_implement_for_client)->diffInDays(Carbon::now()->addDay()) == 0) {
                                    $warning = 'yes';
                                } else {
                                    $warning = 'no';
                                };
                                if (Carbon::parse($pf->date_implement_for_client)->diffInDays(Carbon::now()->addDays(2)) == 0) {
                                    $warning = 'yes';
                                } else {
                                    $warning = 'no';
                                };
                                if (Carbon::parse($pf->date_implement_for_client)->diffInDays(Carbon::now()->addDays(3)) == 0) {
                                    $warning = 'yes';
                                } else {
                                    $warning = 'no';
                                };
                                $d = Carbon::now()->diffForHumans(Carbon::parse($pf->date_implement_for_client));
                                $ar = explode(' ', $d);
                                if ($ar[2] == 'после') {
                                    $danger = 'yes';
                                } else {
                                    $danger = 'no';
                                };
                                $project = Projects::getProjectById($pf->project_id);
                                $client = Clients::getClientById($project->client_id);
                                $city = Citys::getCityById($client->city_id);
                                if ($pf->user_id) {
                                    $user = Users::getUserById($pf->user_id);
                                } else {
                                    $user = '';
                                }
                                $observer = Users::getUserById($pf->observer_id);
                                $status = Printform_status_reference::find($pf->printform_status_ref_id);
                
                                if ($pf->user_id) {
                                    if ($pf->real_date) {
                                        $form_date[] = [
                                            'id' => $pf->id,
                                            'max_hours' => $pf->max_hours,
                                            'prev_contact' => $pf->prev_contact,
                                            'name' => $pf->name,
                                            'client' => $client->name,
                                            'project' => $project->name,
                                            'city' => $city,
                                            'user' => $user->last_name . ' ' . $user->first_name,
                                            'is_free' => $pf->is_free,
                                            'next_contact' => $pf->next_contact,
                                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                            'real_date' => Carbon::parse($pf->real_date)->format('d.m.Y'),
                                            'status' => $status->name,
                                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                                            'number' => $pf->number,
                                            'warning' => $warning,
                                            'danger' => $danger
                                        ];
                                    } else {
                                        $form_date[] = [
                                            'id' => $pf->id,
                                            'max_hours' => $pf->max_hours,
                                            'prev_contact' => $pf->prev_contact,
                                            'name' => $pf->name,
                                            'client' => $client->name,
                                            'project' => $project->name,
                                            'city' => $city,
                                            'user' => $user->last_name . ' ' . $user->first_name,
                                            'is_free' => $pf->is_free,
                                            'next_contact' => $pf->next_contact,
                                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                            'real_date' => '',
                                            'status' => $status->name,
                                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                                            'number' => $pf->number,
                                            'warning' => $warning,
                                            'danger' => $danger
                                        ];
                                    }
                                } else {
                                    if ($pf->real_date) {
                                        $form_date[] = [
                                            'id' => $pf->id,
                                            'max_hours' => $pf->max_hours,
                                            'prev_contact' => $pf->prev_contact,
                                            'name' => $pf->name,
                                            'client' => $client->name,
                                            'project' => $project->name,
                                            'city' => $city,
                                            'user' => '',
                                            'is_free' => $pf->is_free,
                                            'next_contact' => $pf->next_contact,
                                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                            'real_date' => Carbon::parse($pf->real_date)->format('d.m.Y'), 
                                            'status' => $status->name,
                                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                                            'number' => $pf->number,
                                            'warning' => $warning,
                                            'danger' => $danger
                                        ];
                                    } else {
                                        $form_date[] = [
                                            'id' => $pf->id,
                                            'max_hours' => $pf->max_hours,
                                            'prev_contact' => $pf->prev_contact,
                                            'name' => $pf->name,
                                            'client' => $client->name,
                                            'project' => $project->name,
                                            'city' => $city,
                                            'user' => '',
                                            'is_free' => $pf->is_free,
                                            'next_contact' => $pf->next_contact,
                                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                            'real_date' => '', 
                                            'status' => $status->name,
                                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                                            'number' => $pf->number,
                                            'warning' => $warning,
                                            'danger' => $danger
                                        ];
                                    }
                                }
                            }
                        } else {
                            $form_date[] = '';
                        }
                        break;
                }
                break;
            case 'queue':
                switch ($data->param) {
                    case 'all':
                        $p_forms_date = Project_print_form::whereNull('deleted_at')
                            ->whereNull('raw')
                            ->where('printform_status_ref_id', '!=', 8)
                            ->where('printform_status_ref_id', '!=', 7)
                            ->where('printform_status_ref_id', '!=', 9)
                            ->orderBy('number', 'asc')
                            ->get();
                
                        if (count($p_forms_date) > 0) {
                            foreach ($p_forms_date as $pf) {
                                $project = Projects::getProjectById($pf->project_id);
                                $client = Clients::getClientById($project->client_id);
                                $city = Citys::getCityById($client->city_id);
                                if ($pf->user_id) {
                                    $user = Users::getUserById($pf->user_id);
                                } else {
                                    $user = '';
                                }
                                $observer = Users::getUserById($pf->observer_id);
                                $status = Printform_status_reference::find($pf->printform_status_ref_id);
                
                                if ($pf->user_id) {
                                    if ($pf->real_date) {
                                        $form_date[] = [
                                            'id' => $pf->id,
                                            'max_hours' => $pf->max_hours,
                                            'prev_contact' => $pf->prev_contact,
                                            'name' => $pf->name,
                                            'client' => $client->name,
                                            'project' => $project->name,
                                            'city' => $city,
                                            'user' => $user->last_name . ' ' . $user->first_name,
                                            'is_free' => $pf->is_free,
                                            'next_contact' => $pf->next_contact,
                                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                            'real_date' => Carbon::parse($pf->real_date)->format('d.m.Y'),
                                            'status' => $status->name,
                                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                                            'number' => $pf->number,
                                        ];
                                    } else {
                                        $form_date[] = [
                                            'id' => $pf->id,
                                            'max_hours' => $pf->max_hours,
                                            'prev_contact' => $pf->prev_contact,
                                            'name' => $pf->name,
                                            'client' => $client->name,
                                            'project' => $project->name,
                                            'city' => $city,
                                            'user' => $user->last_name . ' ' . $user->first_name,
                                            'is_free' => $pf->is_free,
                                            'next_contact' => $pf->next_contact,
                                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                            'real_date' => '',
                                            'status' => $status->name,
                                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                                            'number' => $pf->number,
                                        ];
                                    }
                                } else {
                                    if ($pf->real_date) {
                                        $form_date[] = [
                                            'id' => $pf->id,
                                            'max_hours' => $pf->max_hours,
                                            'prev_contact' => $pf->prev_contact,
                                            'name' => $pf->name,
                                            'client' => $client->name,
                                            'project' => $project->name,
                                            'city' => $city,
                                            'user' => '',
                                            'is_free' => $pf->is_free,
                                            'next_contact' => $pf->next_contact,
                                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                            'real_date' => Carbon::parse($pf->real_date)->format('d.m.Y'),
                                            'status' => $status->name,
                                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                                            'number' => $pf->number,
                                        ];
                                    } else {
                                        $form_date[] = [
                                            'id' => $pf->id,
                                            'max_hours' => $pf->max_hours,
                                            'prev_contact' => $pf->prev_contact,
                                            'name' => $pf->name,
                                            'client' => $client->name,
                                            'project' => $project->name,
                                            'city' => $city,
                                            'user' => '',
                                            'is_free' => $pf->is_free,
                                            'next_contact' => $pf->next_contact,
                                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                            'real_date' => '',
                                            'status' => $status->name,
                                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                                            'number' => $pf->number,
                                        ];
                                    }
                                }
                            }
                        } else {
                            $form_date[] = '';
                        }
                        break;
                    case 'myForms':
                        $user = Sentinel::getUser();
                        $p_forms_date = Project_print_form::whereNull('deleted_at')
                            ->whereNull('raw')
                            ->where('printform_status_ref_id', '!=', 8)
                            ->where('printform_status_ref_id', '!=', 7)
                            ->where('printform_status_ref_id', '!=', 9)
                            ->where('user_id', '=', $user->id)
                            ->orderBy('number', 'asc')
                            ->get();

                        if (count($p_forms_date) > 0) {
                            foreach ($p_forms_date as $pf) {
                                $project = Projects::getProjectById($pf->project_id);
                                $client = Clients::getClientById($project->client_id);
                                $city = Citys::getCityById($client->city_id);
                                if ($pf->user_id) {
                                    $user = Users::getUserById($pf->user_id);
                                } else {
                                    $user = '';
                                }
                                $observer = Users::getUserById($pf->observer_id);
                                $status = Printform_status_reference::find($pf->printform_status_ref_id);

                                if ($pf->user_id) {
                                    if ($pf->real_date) {
                                        $form_date[] = [
                                            'id' => $pf->id,
                                            'max_hours' => $pf->max_hours,
                                            'prev_contact' => $pf->prev_contact,
                                            'name' => $pf->name,
                                            'client' => $client->name,
                                            'project' => $project->name,
                                            'city' => $city,
                                            'user' => $user->last_name . ' ' . $user->first_name,
                                            'is_free' => $pf->is_free,
                                            'next_contact' => $pf->next_contact,
                                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                            'real_date' => Carbon::parse($pf->real_date)->format('d.m.Y'),
                                            'status' => $status->name,
                                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                                            'number' => $pf->number,
                                        ];
                                    } else {
                                        $form_date[] = [
                                            'id' => $pf->id,
                                            'max_hours' => $pf->max_hours,
                                            'prev_contact' => $pf->prev_contact,
                                            'name' => $pf->name,
                                            'client' => $client->name,
                                            'project' => $project->name,
                                            'city' => $city,
                                            'user' => $user->last_name . ' ' . $user->first_name,
                                            'is_free' => $pf->is_free,
                                            'next_contact' => $pf->next_contact,
                                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                            'real_date' => '',
                                            'status' => $status->name,
                                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                                            'number' => $pf->number,
                                        ];
                                    }
                                } else {
                                    if ($pf->real_date) {
                                        $form_date[] = [
                                            'id' => $pf->id,
                                            'max_hours' => $pf->max_hours,
                                            'prev_contact' => $pf->prev_contact,
                                            'name' => $pf->name,
                                            'client' => $client->name,
                                            'project' => $project->name,
                                            'city' => $city,
                                            'user' => '',
                                            'is_free' => $pf->is_free,
                                            'next_contact' => $pf->next_contact,
                                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                            'real_date' => Carbon::parse($pf->real_date)->format('d.m.Y'),
                                            'status' => $status->name,
                                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                                            'number' => $pf->number,
                                        ];
                                    } else {
                                        $form_date[] = [
                                            'id' => $pf->id,
                                            'max_hours' => $pf->max_hours,
                                            'prev_contact' => $pf->prev_contact,
                                            'name' => $pf->name,
                                            'client' => $client->name,
                                            'project' => $project->name,
                                            'city' => $city,
                                            'user' => '',
                                            'is_free' => $pf->is_free,
                                            'next_contact' => $pf->next_contact,
                                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                            'real_date' => '',
                                            'status' => $status->name,
                                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                                            'number' => $pf->number,
                                        ];
                                    }
                                }
                            }
                        } else {
                            $form_date[] = '';
                        }
                        break;
                    case 'myCloseForms':
                        $user = Sentinel::getUser();
                        $p_forms_date = Project_print_form::whereNull('deleted_at')
                            ->whereNull('raw')
                            ->where('printform_status_ref_id', '=', 9)
                            ->where('user_id', '=', $user->id)
                            ->orderBy('number', 'asc')
                            ->get();

                        if (count($p_forms_date) > 0) {
                            foreach ($p_forms_date as $pf) {
                                $project = Projects::getProjectById($pf->project_id);
                                $client = Clients::getClientById($project->client_id);
                                $city = Citys::getCityById($client->city_id);
                                if ($pf->user_id) {
                                    $user = Users::getUserById($pf->user_id);
                                } else {
                                    $user = '';
                                }
                                $observer = Users::getUserById($pf->observer_id);
                                $status = Printform_status_reference::find($pf->printform_status_ref_id);

                                if ($pf->user_id) {
                                    if ($pf->real_date) {
                                        $form_date[] = [
                                            'id' => $pf->id,
                                            'max_hours' => $pf->max_hours,
                                            'prev_contact' => $pf->prev_contact,
                                            'name' => $pf->name,
                                            'client' => $client->name,
                                            'project' => $project->name,
                                            'city' => $city,
                                            'user' => $user->last_name . ' ' . $user->first_name,
                                            'is_free' => $pf->is_free,
                                            'next_contact' => $pf->next_contact,
                                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                            'real_date' => Carbon::parse($pf->real_date)->format('d.m.Y'),
                                            'status' => $status->name,
                                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                                            'number' => $pf->number,
                                        ];
                                    } else {
                                        $form_date[] = [
                                            'id' => $pf->id,
                                            'max_hours' => $pf->max_hours,
                                            'prev_contact' => $pf->prev_contact,
                                            'name' => $pf->name,
                                            'client' => $client->name,
                                            'project' => $project->name,
                                            'city' => $city,
                                            'user' => $user->last_name . ' ' . $user->first_name,
                                            'is_free' => $pf->is_free,
                                            'next_contact' => $pf->next_contact,
                                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                            'real_date' => '',
                                            'status' => $status->name,
                                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                                            'number' => $pf->number,
                                        ];
                                    }
                                } else {
                                    if ($pf->real_date) {
                                        $form_date[] = [
                                            'id' => $pf->id,
                                            'max_hours' => $pf->max_hours,
                                            'prev_contact' => $pf->prev_contact,
                                            'name' => $pf->name,
                                            'client' => $client->name,
                                            'project' => $project->name,
                                            'city' => $city,
                                            'user' => '',
                                            'is_free' => $pf->is_free,
                                            'next_contact' => $pf->next_contact,
                                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                            'real_date' => Carbon::parse($pf->real_date)->format('d.m.Y'),
                                            'status' => $status->name,
                                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                                            'number' => $pf->number,
                                        ];
                                    } else {
                                        $form_date[] = [
                                            'id' => $pf->id,
                                            'max_hours' => $pf->max_hours,
                                            'prev_contact' => $pf->prev_contact,
                                            'name' => $pf->name,
                                            'client' => $client->name,
                                            'project' => $project->name,
                                            'city' => $city,
                                            'user' => '',
                                            'is_free' => $pf->is_free,
                                            'next_contact' => $pf->next_contact,
                                            'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                            'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                            'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                            'real_date' => '',
                                            'status' => $status->name,
                                            'observer' => $observer->last_name . ' ' . $observer->first_name,
                                            'number' => $pf->number,
                                        ];
                                    }
                                }
                            }
                        } else {
                            $form_date[] = '';
                        }
                        break;
                }
                break;
        };
        return response()->json($form_date);
    }

    public function filterFormsCallback(Request $request) {
        if ($request->ajax()) {
            $resp = '';
            switch ($request->flag) {
                case 'all':
                    $p_forms_date = Project_print_form::whereNull('deleted_at')
                        ->whereNull('raw')
                        ->orderBy('number', 'asc')
                        ->get();

                    //dd($p_forms_date);

                    if (count($p_forms_date) > 0) {
                        foreach ($p_forms_date as $pf) {
                            $project = Projects::getProjectById($pf->project_id);
                            $client = Clients::getClientById($project->client_id);
                            $city = Citys::getCityById($client->city_id);
                            if ($pf->user_id) {
                                $user = Users::getUserById($pf->user_id);
                            } else {
                                $user = '';
                            }
                            $observer = Users::getUserById($pf->observer_id);
                            $status = Printform_status_reference::find($pf->printform_status_ref_id);
                            //dd($project);

                            if ($pf->user_id) {
                                $form_date[] = [
                                    'id' => $pf->id,
                                    'max_hours' => $pf->max_hours,
                                    'prev_contact' => $pf->prev_contact,
                                    'name' => $pf->name,
                                    'client' => $client->name,
                                    'project' => $project->name,
                                    'city' => $city,
                                    'user' => $user->last_name . ' ' . $user->first_name,
                                    'is_free' => $pf->is_free,
                                    'next_contact' => $pf->next_contact,
                                    'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                    'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                    'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                    'status' => $status->name,
                                    'observer' => $observer->last_name . ' ' . $observer->first_name,
                                    'number' => $pf->number,
                                ];
                            } else {
                                $form_date[] = [
                                    'id' => $pf->id,
                                    'max_hours' => $pf->max_hours,
                                    'prev_contact' => $pf->prev_contact,
                                    'name' => $pf->name,
                                    'client' => $client->name,
                                    'project' => $project->name,
                                    'city' => $city,
                                    'user' => '',
                                    'is_free' => $pf->is_free,
                                    'next_contact' => $pf->next_contact,
                                    'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                    'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                    'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                    'status' => $status->name,
                                    'observer' => $observer->last_name . ' ' . $observer->first_name,
                                    'number' => $pf->number,
                                ];
                            }
                        }
                    } else {
                        $form_date[] = '';
                    }

                    $resp = $form_date;
                    break;
                case 'myForms':
                    $cur_user = Sentinel::getUser();
                    $p_forms_date = Project_print_form::whereNull('deleted_at')
                        ->where('user_id', '=', $cur_user->id)
                        ->whereNull('raw')
                        ->orderBy('number', 'asc')
                        ->get();

                    //dd($p_forms_date);

                    if (count($p_forms_date) > 0) {
                        foreach ($p_forms_date as $pf) {
                            $project = Projects::getProjectById($pf->project_id);
                            $client = Clients::getClientById($project->client_id);
                            $city = Citys::getCityById($client->city_id);
                            if ($pf->user_id) {
                                $user = Users::getUserById($pf->user_id);
                            } else {
                                $user = '';
                            }
                            $observer = Users::getUserById($pf->observer_id);
                            $status = Printform_status_reference::find($pf->printform_status_ref_id);
                            //dd($project);

                            if ($pf->user_id) {
                                $form_date[] = [
                                    'id' => $pf->id,
                                    'max_hours' => $pf->max_hours,
                                    'prev_contact' => $pf->prev_contact,
                                    'name' => $pf->name,
                                    'client' => $client->name,
                                    'project' => $project->name,
                                    'city' => $city,
                                    'user' => $user->last_name . ' ' . $user->first_name,
                                    'is_free' => $pf->is_free,
                                    'next_contact' => $pf->next_contact,
                                    'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                    'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                    'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                    'status' => $status->name,
                                    'observer' => $observer->last_name . ' ' . $observer->first_name,
                                    'number' => $pf->number,
                                ];
                            } else {
                                $form_date[] = [
                                    'id' => $pf->id,
                                    'max_hours' => $pf->max_hours,
                                    'prev_contact' => $pf->prev_contact,
                                    'name' => $pf->name,
                                    'client' => $client->name,
                                    'project' => $project->name,
                                    'city' => $city,
                                    'user' => '',
                                    'is_free' => $pf->is_free,
                                    'next_contact' => $pf->next_contact,
                                    'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                    'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                    'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                    'status' => $status->name,
                                    'observer' => $observer->last_name . ' ' . $observer->first_name,
                                    'number' => $pf->number,
                                ];
                            }
                        }
                    } else {
                        $form_date[] = '';
                    }

                    $resp = $form_date;
                    break;
                case 'myCloseForms':
                    $cur_user = Sentinel::getUser();
                    $p_forms_date = Project_print_form::whereNull('deleted_at')
                        ->where('user_id', '=', $cur_user->id)
                        ->where('printform_status_ref_id', '=', 10)
                        ->whereNull('raw')
                        ->orderBy('number', 'asc')
                        ->get();

                    //dd($p_forms_date);

                    if (count($p_forms_date) > 0) {
                        foreach ($p_forms_date as $pf) {
                            $project = Projects::getProjectById($pf->project_id);
                            $client = Clients::getClientById($project->client_id);
                            $city = Citys::getCityById($client->city_id);
                            if ($pf->user_id) {
                                $user = Users::getUserById($pf->user_id);
                            } else {
                                $user = '';
                            }
                            $observer = Users::getUserById($pf->observer_id);
                            $status = Printform_status_reference::find($pf->printform_status_ref_id);
                            //dd($project);

                            if ($pf->user_id) {
                                $form_date[] = [
                                    'id' => $pf->id,
                                    'max_hours' => $pf->max_hours,
                                    'prev_contact' => $pf->prev_contact,
                                    'name' => $pf->name,
                                    'client' => $client->name,
                                    'project' => $project->name,
                                    'city' => $city,
                                    'user' => $user->last_name . ' ' . $user->first_name,
                                    'is_free' => $pf->is_free,
                                    'next_contact' => $pf->next_contact,
                                    'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                    'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                    'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                    'status' => $status->name,
                                    'observer' => $observer->last_name . ' ' . $observer->first_name,
                                    'number' => $pf->number,
                                ];
                            } else {
                                $form_date[] = [
                                    'id' => $pf->id,
                                    'max_hours' => $pf->max_hours,
                                    'prev_contact' => $pf->prev_contact,
                                    'name' => $pf->name,
                                    'client' => $client->name,
                                    'project' => $project->name,
                                    'city' => $city,
                                    'user' => '',
                                    'is_free' => $pf->is_free,
                                    'next_contact' => $pf->next_contact,
                                    'date' => Carbon::parse($pf->created_at)->format('d.m.Y H:i'),
                                    'date_implement' => Carbon::parse($pf->date_implement)->format('d.m.Y'),
                                    'date_implement_for_client' => Carbon::parse($pf->date_implement_for_client)->format('d.m.Y'),
                                    'status' => $status->name,
                                    'observer' => $observer->last_name . ' ' . $observer->first_name,
                                    'number' => $pf->number,
                                ];
                            }
                        }
                    } else {
                        $form_date[] = '';
                    }

                    $resp = $form_date;
                    break;
            }

            return response()->json($resp);
        }
    }
}
