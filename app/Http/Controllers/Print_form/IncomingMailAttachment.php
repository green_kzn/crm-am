<?php

namespace App\Http\Controllers\Print_form;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IncomingMailAttachment extends Controller
{
    public $id;
    public $contentId;
    public $name;
    public $filePath;
    public $disposition;
}
