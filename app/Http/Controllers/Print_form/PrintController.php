<?php

namespace App\Http\Controllers\Print_form;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use App\Http\Controllers\PermissionsController as Permissions;
use App\Http\Controllers\Print_form\User2Controller as Users;
use App\User;
use Cache;

class PrintController extends Controller
{
    public function admin() {
        return view('Print_form.dashboard');
    }

    public function getPermForms() {
        $user = Sentinel::getUser();
        foreach($user->permissions as $k => $v) {
            $d = explode('.', $k);
            $ar[$d[0]][$d[1]][$d[2]] = $v;
        };
        // print_r($ar);
        return response()->json($ar);
    }

    public function changeFormPerm(Request $request) {
        $data = json_decode($request->data);
        $user = Sentinel::getUser();
        $user->updatePermission('forms.'.$data->type.'.'.$data->name, $data->value);
        
        $user->save();
        // $ar = array(
        //     'forms' => array(
        //         $data->type => array(
        //             $data->name => $data->value
        //         )
        //     )
        // );
        // // print_r(json_encode($ar));
        // $user->permissions = json_encode($ar);
        // $user->save();
    }
}
