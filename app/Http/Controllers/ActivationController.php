<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Activation;
use App\User;
use Sentinel;

class ActivationController extends Controller
{
    public function activate($email, $activationCode) {
        $user = User::whereEmail($email)->first();
        if (Activation::complete($user, $activationCode)) {
            return redirect('/login')->with([
                'success' => 'Ваш аккаунт успешно активирован'
            ]);
        } else {
            return redirect()->back()->with(['error_activate' => 'Ваша учетная запись еще не активирована']);
        }
    }
}
