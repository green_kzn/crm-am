<?php

namespace App\Http\Controllers\Dir;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Clients_reference;

class ClientsController extends Controller
{
    static public function getClientById($id) {
        $client = Clients_reference::where('id', '=', $id)
            ->whereNull('deleted_at')->firstOrFail();
        return $client;
    }
}
