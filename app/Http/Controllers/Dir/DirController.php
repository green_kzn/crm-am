<?php

namespace App\Http\Controllers\Dir;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DirController extends Controller
{
    public function index(Request $request) {
        return view('Dir.dashboard');
    }
}
