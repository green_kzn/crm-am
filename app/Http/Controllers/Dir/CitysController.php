<?php

namespace App\Http\Controllers\Dir;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\City_reference;

class CitysController extends Controller
{
    static public function getCityById($id) {
        $city = City_reference::where('id', '=', $id)
            ->whereNull('deleted_at')
            ->firstOrFail();

        return $city->name;
    }
}
