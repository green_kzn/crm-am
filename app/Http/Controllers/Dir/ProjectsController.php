<?php

namespace App\Http\Controllers\Dir;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Dir\CommentsController as Comments;
use App\Http\Controllers\Dir\ClientsController as Clients;
use App\Http\Controllers\Dir\CitysController as Citys;
use App\Http\Controllers\Dir\ModulesController as Modules;
use App\Http\Controllers\Dir\GroupTasksController as GroupTasks;
use App\Http\Controllers\Dir\DocumentsController as Documents;
use App\Project;
use App\Project_contact;
use App\Project_module;
use App\Project_task;
use App\Project_task_contact;
use App\Projects_doc;
use App\Contact;
use App\Comment;
use App\Posts_reference;
use App\User;
use App\Modules_reference;
use App\Module_rule;
use App\Tasksstatus_reference;
use App\Grouptasks_reference;
use App\Clients_reference;
use App\Clarifying_question;
use App\Clarifying_answer;
use App\Callback_close_question;
use App\Callback_work_question;
use App\Callback_work_answer;
use App\Callback_close_answer;
use App\Callback_other_task;
use App\Project_print_form;
use App\Check_list_reference;
use App\Check_list_answer;
use Sentinel;
use Event;
use App\Events\onAddProjectEvent;
use App\Notifications\WorkoutAssigned;
use Notification;
use Auth;
use App\Notifications\ProjectTask;
use Carbon\Carbon;
use App\Events\ManagerCreateProject;

class ProjectsController extends Controller
{
    public function getProjects() {
        $projects_q = Project::whereNULL('deleted_at')
            //->whereNULL('callback_status')
            ->get();

        if (count($projects_q) > 0) {
            foreach ($projects_q as $p) {
                $task = Project_task::where('project_id', '=', $p['id'])
//                    ->where('observer_id', '=', $cur_user->id)
                    ->whereNull('deleted_at')
                    ->get();

                if (count($task) > 0) {
                    foreach ($task as $t) {
                        if ($t->tasksstatus_ref_id == 2) {
                            //$done2[$p['id']][] = $t->id;
                            $done2 = 0;
                        } else {
                            $done2 = 0;
                        }
                    }
                }

                $client = Clients::getClientById($p['client_id']);
                $city = Citys::getCityById($client->city_id);

                if ($p['raw'] == 1) {
                    $projects[] = [
                        'client_name' => $client['name'],
                        'name' => $p['name'],
                        'raw' => 1,
                        'id' => $p['id'],
                        'prev_contact' => $p['prev_contact'],
                        'next_contact' => $p['next_contact'],
                        'active' => $p['active'],
                        'city' => $city,
                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                        //'done' => 0
                    ];
                } else {
                    $projects[] = [
                        'client_name' => $client['name'],
                        'name' => $p['name'],
                        'id' => $p['id'],
                        'raw' => 0,
                        'prev_contact' => $p['prev_contact'],
                        'next_contact' => $p['next_contact'],
                        'start_date' => $p->start_date,
                        'finish_date' => $p->finish_date,
                        'active' => $p['active'],
                        'city' => $city,
                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                    ];
                }
            }
        } else {
            $projects = '';
        }

        return response()->json($projects);
    }

    public function getProject(Request $request, $id) {
        $project = Project::find($id);
        $project_start_date = Carbon::parse($project->start_date)->format('d.m.Y');
        $project_finish_date = Carbon::parse($project->finish_date)->format('d.m.Y');

        if ($project) {
            $cur_date = date("j") . '.' . date("n") . '.' . date("Y");
            $cur_date_val = date("Y") . '-' . date("m") . '-' . date("d");
            $tomorrow = date('Y-m-d', strtotime($cur_date . ' +1 day'));
            $tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +1 day'));
            $plus_one_day = date('Y-m-d', strtotime($cur_date . ' +2 day'));
            $plus_one_day_val = date('Y-m-d', strtotime($cur_date . ' +2 day'));
            $plus_two_day = date('d.m.Y', strtotime($cur_date . ' +3 day'));
            $plus_two_day_val = date('Y-m-d', strtotime($cur_date . ' +3 day'));
            $plus_three_day = date('d.m.Y', strtotime($cur_date . ' +4 day'));
            $plus_three_day_val = date('Y-m-d', strtotime($cur_date . ' +4 day'));

            $comments = Comments::getCommentsByProjectId($id);

            $contacts = Project_contact::where('project_id', '=', $id)
                ->whereNULL('deleted_at')
                ->orderBy('id', 'asc')
                ->get();

            foreach ($contacts as $c) {
                $cont = Contact::where('id', '=', $c->contact_id)
                    ->whereNull('deleted_at')
                    ->first();

                //dd($cont);
                if (!is_null($cont)) {
                    $post = Posts_reference::find($cont->post_id);

                    if ($cont->post_id == 1) {
                        $ar_contact[] = [
                            'first_name' => $cont->first_name,
                            'last_name' => $cont->last_name,
                            'patronymic' => $cont->patronymic,
                            'phone' => $cont->phone,
                            'email' => $cont->email,
                            'post' => $post->name,
                            'id' => $cont->id,
                            'director' => 1,
                            'main' => $c->main,
                        ];
                    } else {
                        $ar_contact[] = [
                            'first_name' => $cont->first_name,
                            'last_name' => $cont->last_name,
                            'patronymic' => $cont->patronymic,
                            'phone' => $cont->phone,
                            'email' => $cont->email,
                            'post' => $post['name'],
                            'id' => $cont->id,
                            'director' => 0,
                            'main' => $c->main,
                        ];
                    }
                } else {
                    $ar_contact[] = '';
                }

            }

            $modules = Project_module::where('project_id', '=', $id)
                ->whereNull('deleted_at')
                ->get();

            foreach ($modules as $m) {
                $name = Modules_reference::find($m->module_id);

                $all_modules[] = [
                    'id' => $name['id'],
                    'name' => $name['name'],
                    'num' => $m->num
                ];
            }

            foreach ($all_modules as $all) {
                $task_q = Module_rule::where('module_id', '=', $all['id'])
                    ->whereNull('deleted_at')
                    ->get();

                foreach ($task_q as $q) {
                    if (count($q->questions) > 0) {
                        $questions_ar[] = [
                            'm_id' => $all['id'],
                            'id' => $q->id,
                            'questions' => $q->questions
                        ];
                    } else {
                        $questions_ar = [];
                    }
                }
            }

            $cur_user = Sentinel::getUser();
            $count_norm = 0;
            $count_number = 0;
            $count_print_norm = 0;
            $count_print_number = 0;
            $count_screen_norm = 0;
            $count_screen_number = 0;

            $project_tasks = Project_task::where('project_id', '=', $id)
                ->whereNull('deleted_at')
                //->whereNull('status')
                //->whereNull('additional_id')
                ->get();

            if (count($project_tasks) > 0) {
                $observer_id = $project_tasks[0]['observer_id'];
                $print_form_id = Grouptasks_reference::select('id')
                    ->where('is_print_form', '=', 1)
                    ->get();

                $screen_form_id = Grouptasks_reference::select('id')
                    ->where('is_screen_form', '=', 1)
                    ->get();

                foreach ($project_tasks as $pt) {
                    $name = Module_rule::where('id', '=', $pt->module_rule_id)
                        ->whereNull('deleted_at')
                        ->first();

                    $test[] = array($name['grouptasks_id']);
                    $ptc = Project_task_contact::where('project_task_id', '=', $pt->id)
                        ->whereNull('deleted_at')
                        ->first();
                    $p_name = Project::find($pt->project_id);
                    $m_name = Modules_reference::find($pt->module_id);
                    $g_task = Grouptasks_reference::find($name['grouptasks_id']);
                    //dd($pt->module_id);

                    if ($g_task['is_print_form'] == 1) {
                        $type = 'print_form';
                    } elseif ($g_task['is_screen_form'] == 1) {
                        $type = 'screen_form';
                    } else {
                        $type = 'task';
                    }

                    $date = explode(' ', $ptc['next_contact']);
                    if (count($ptc) > 0) {
                        $p_tasks[] = [
                            'id' => $pt->id,
                            'name' => $name['task'],
                            'p_name' => $p_name->name,
                            'observer_id' => $pt->observer_id,
                            'city' => $p_name->city,
                            'm_name' => $m_name->name,
                            'callbackstatus_ref_id' => $ptc['callbackstatus_ref_id'],
                            'g_task' => $g_task['name'],
                            'type' => $type,
                            'additional' => $name['additional'],
                            'status' => $pt->tasksstatus_ref_id,
                            'next_contact' => $date[0]
                        ];
                    } else {
                        $p_tasks[] = [
                            'id' => $pt->id,
                            'name' => $name['task'],
                            'p_name' => $p_name->name,
                            'observer_id' => $pt->observer_id,
                            'city' => $p_name->city,
                            'm_name' => $m_name->name,
                            'callbackstatus_ref_id' => $ptc['callbackstatus_ref_id'],
                            'g_task' => $g_task['name'],
                            'type' => $type,
                            'additional' => $name['additional'],
                            'status' => $pt->tasksstatus_ref_id,
                            'next_contact' => null
                        ];
                    }
                    //dd($p_tasks);

                    foreach ($print_form_id as $pfid) {
                        if ($name['grouptasks_id'] != $pfid->id) {
                            $count_norm += $pt->norm_contacts;
                            $count_number += $pt->number_contacts;
                        } else {
                            $count_print_norm += $pt->norm_contacts;
                            $count_print_number += $pt->number_contacts;
                        }
                    }

                    foreach ($screen_form_id as $sfid) {
                        if ($name['grouptasks_id'] != $sfid->id) {
                            $count_norm += $pt->norm_contacts;
                            $count_number += $pt->number_contacts;
                        } else {
                            $count_screen_norm += $pt->norm_contacts;
                            $count_screen_number += $pt->number_contacts;
                        }
                    }
                }
            } else {
                $observer_id = '';
                $p_tasks = '';
            }

            //foreach ()
            //dd($screen_form_id);

            $project_form = Project_print_form::where('project_id', '=', $id)
                ->whereNull('deleted_at')
                ->get();

            if (count($project_form) > 0) {
                foreach ($project_form as $pf) {
                    $name = Module_rule::where('id', '=', $pf->module_rule_id)
                        ->whereNull('deleted_at')
                        ->first();
                    $ptc = Project_task_contact::where('project_task_id', '=', $pf->id)
                        ->whereNull('deleted_at')
                        ->first();
                    $p_name = Project::find($pf->project_id);
                    $m_name = Modules_reference::find($pf->module_id);
                    $g_task = Grouptasks_reference::find(11);

                    $q_ar[] = '';

                    $date = explode(' ', $ptc['next_contact']);
                    if (count($ptc) > 0) {
                        $p_form[] = [
                            'id' => $pf->id,
                            'name' => $pf->name,
                            'p_name' => $p_name->name,
                            'observer_id' => $pf->observer_id,
                            'city' => $p_name->city,
                            'm_name' => $m_name->name,
                            'callbackstatus_ref_id' => $ptc['callbackstatus_ref_id'],
                            'g_task' => $g_task['name'],
                            'additional' => $name['additional'],
                            'status' => $pf->tasksstatus_ref_id,
                            'next_contact' => $date[0]
                        ];
                    } else {
                        $p_form[] = [
                            'id' => $pf->id,
                            'name' => $pf->name,
                            'p_name' => $p_name->name,
                            'observer_id' => $pf->observer_id,
                            'city' => $p_name->city,
                            'm_name' => $m_name['name'],
                            'callbackstatus_ref_id' => $ptc['callbackstatus_ref_id'],
                            'g_task' => $g_task['name'],
                            'additional' => $name['additional'],
                            'status' => $pf->tasksstatus_ref_id,
                            'next_contact' => null
                        ];
                    }

                }
            } else {
                $p_form = '';
            }

            $count_fact = Project_task_contact::where('project_id', '=', $project->id)
                ->whereNull('deleted_at')
                ->whereNotNull('duration')
                ->get();

            $remained = 0;
            foreach ($count_fact as $cf) {
                switch ($cf->duration) {
                    case '00:15:00':
                        $remained += 0.5;
                        break;
                    case '00:30:00':
                        $remained += 1;
                        break;
                    case '00:45:00':
                        $remained += 1;
                        break;
                    case '01:00:00':
                        $remained += 1;
                        break;
                    case '01:30:00':
                        $remained += 1.5;
                        break;
                }
            }
            //dd($remained);

            $project_tasks2 = Project_task::where('project_id', '=', $id)
                ->whereNull('deleted_at')
                ->get();

            if (count($project_tasks2) > 0) {
                foreach ($project_tasks2 as $pt) {
                    $name = Module_rule::where('id', '=', $pt->module_rule_id)
                        ->whereNull('deleted_at')
                        ->first();

                    if ($name['additional'] == null) {
                        $c_question = Clarifying_question::where('task_id', '=', $name['id'])
                            ->whereNull('deleted_at')
                            ->get();

                        if (count($c_question) > 0) {
                            foreach ($c_question as $q) {
                                $q_ar = [
                                    'id' => $q->id,
                                    'question' => $q->question
                                ];
                                $q_ar2[] = $q_ar;
                            }
                        }
                    }
                }
            }

            if (isset($q_ar2)) {
                foreach ($q_ar2 as $cq) {
                    $c_answer = Clarifying_answer::where('question_id', '=', $cq['id'])
                        ->where('project_id', '=', $id)
                        ->whereNull('deleted_at')
                        ->first();

                    $question[] = [
                        'id' => $cq['id'],
                        'question' => $cq['question'],
                        'answer' => $c_answer['answer']
                    ];

                }
            } else {
                $question = [];
            }
            //dd($q_ar2);

            if (count($question) > 0) {
                foreach ($question as $q) {
                    $c_answer = Clarifying_answer::where('question_id', '=', $q['id'])
                        ->where('project_id', '=', $id)
                        ->whereNull('deleted_at')
                        ->first();

                    //dd($c_answer['answer']);

                    if ($c_answer['answer'] != 'null') {
                        $answer[] = [
                            'q_id' => $q['id'],
                            'answer' => $c_answer['answer']
                        ];
                    } else {
                        $answer = [];
                    }
                }
            } else {
                $answer = [];
            }

            $user = User::whereNull('deleted_at')->get();
            foreach ($user as $u) {
                if ($u->id == $project->user_id) {
                    $observer[] = [
                        'default' => 1,
                        'id' => $u->id,
                        'first_name' => $u->first_name,
                        'last_name' => $u->last_name
                    ];
                } else {
                    $observer[] = [
                        'default' => 0,
                        'id' => $u->id,
                        'first_name' => $u->first_name,
                        'last_name' => $u->last_name
                    ];
                }
            }

            $doc_types = Documents::getAllDocumentsType();
            $doc_input = Documents::getProjectDocsByType(1, $id);
            $doc_output = Documents::getProjectDocsByType(2, $id);

            $forms_q = Project_print_form::where('project_id', '=', $id)
                ->whereNull('deleted_at')
                ->get();

            if (count($forms_q) > 0) {
                foreach ($forms_q as $f) {
                    $status = Printform_status_reference::find($f->printform_status_ref_id);
                    if ($f->is_print_form == 1) {
                        $type = 'print_form';
                    } elseif ($f->is_screen_form == 1) {
                        $type = 'screen_form';
                    }
                    $file = Done_printform_file::where('project_print_form_id', '=', $f->id)
                        ->whereNull('deleted_at')
                        ->first();

                    if (count($file) > 0) {
                        $url = Storage::disk('local')->url($f->file);
                        $forms[] = [
                            'id' => $f->id,
                            'name' => $f->name,
                            'status' => $status->name,
                            'date_for_client' => Carbon::Parse($f->date_implement_for_client)->format('d.m.Y'),
                            'date_status' => Carbon::Parse($f->updated_at)->format('d.m.Y'),
                            'type' => $type,
                            'file' => $file->file,
                            'img' => $url . $file->file
                        ];
                    } else {
                        $forms[] = [
                            'id' => $f->id,
                            'name' => $f->name,
                            'status' => $status->name,
                            'date_for_client' => Carbon::Parse($f->date_implement_for_client)->format('d.m.Y'),
                            'date_status' => Carbon::Parse($f->updated_at)->format('d.m.Y'),
                            'type' => $type,
                            'file' => ''
                        ];
                    }
                }
            } else {
                $forms = '';
            }

            $client = Clients_reference::find($project->client_id);

            $check_list_answer = Check_list_answer::where('project_id', '=', $id)
                ->whereNull('deleted_at')
                ->get();

            foreach ($check_list_answer as $check_answer) {
                $check_list_question = Check_list_reference::where('id', '=', $check_answer->check_list_ref_id)
                    ->whereNull('deleted_at')
                    ->first();

                $check_list[] = [
                    'id' => $check_list_question['id'],
                    'question' => strip_tags($check_list_question['question']),
                    'answer' => $check_answer->answer
                ];
            }
            //dd($p_tasks);
            $posts = Posts_reference::whereNull('deleted_at')
                ->get();

            return response()->json([
                'project' => $project,
                'project_start_date' => $project_start_date,
                'project_finish_date' => $project_finish_date,
                'forms' => $forms,
                'observer_id' => $observer_id,
                'comments' => $comments,
                'contacts' => $ar_contact,
                'modules' => $all_modules,
                'all_modules' => Modules::getModulesByProjectId($project->id),
                'project_task' => $p_tasks,
                'project_form' => $p_form,
                'group_task' => GroupTasks::getAllTasks(),
                'all_group_task' => GroupTasks::getAllTaskGroup(),
                'observer' => $observer,
                'cur_date' => $cur_date,
                'cur_date_val' => $cur_date_val,
                'tomorrow' => $tomorrow,
                'tomorrow_val' => $tomorrow_val,
                'plus_one_day' => $plus_one_day,
                'plus_one_day_val' => $plus_one_day_val,
                'plus_two_day' => $plus_two_day,
                'plus_two_day_val' => $plus_two_day_val,
                'plus_three_day' => $plus_three_day,
                'plus_three_day_val' => $plus_three_day_val,
                'client' => $client,
                'questions' => $questions_ar,
                'question' => $question,
                'answer' => $answer,
                'count_norm' => $count_norm / 2,
                'count_number' => $count_number / 2,
                'count_print_norm' => $count_print_norm,
                'count_print_number' => $count_print_number,
                'count_screen_norm' => $count_screen_norm,
                'count_screen_number' => $count_screen_number,
                'count_fact' => count($count_fact),
                'remained' => $remained,
                'doc_types' => $doc_types,
                'doc_input' => $doc_input,
                'doc_output' => $doc_output,
                'check_list' => $check_list,
                'posts' => $posts
            ]);
        }
    }
}
