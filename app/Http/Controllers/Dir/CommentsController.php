<?php

namespace App\Http\Controllers\Dir;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Comment;
use App\Printform_history;
use App\Printform_comment;
use App\Printform_status_reference;
use App\User;
use Sentinel;

class CommentsController extends Controller
{
    public static function getCommentsByProjectId($id) {
        $comments_q = Comment::where('project_id', '=', $id)
            ->orderBy('created_at', 'desc')
            ->whereNULL('deleted_at')
            ->get();

        if (count($comments_q) > 0) {
            foreach ($comments_q as $c) {
                $user = User::find($c->user_id);
                $cur_user = Sentinel::getUser();

                if ($c->for_client != NULL) {
                    if ($user->id == $cur_user->id) {
                        $comments[] = [
                            'text' => strip_tags($c->text),
                            'date' => $c->date,
                            'time' => $c->time,
                            'id' => $c->id,
                            'for_client' => 1,
                            'user_name' => $user->first_name,
                            'user_surname' => $user->last_name,
                            'user_foto' => $user->foto,
                            'autor' => true
                        ];
                    } else {
                        $comments[] = [
                            'text' => strip_tags($c->text),
                            'date' => $c->date,
                            'time' => $c->time,
                            'id' => $c->id,
                            'for_client' => 1,
                            'user_name' => $user->first_name,
                            'user_surname' => $user->last_name,
                            'user_foto' => $user->foto,
                            'autor' => false
                        ];
                    }
                } else {
                    if ($user['id'] == $cur_user->id) {
                        $comments[] = [
                            'text' => strip_tags($c->text),
                            'date' => $c->date,
                            'time' => $c->time,
                            'id' => $c->id,
                            'for_client' => 0,
                            'user_name' => $user->first_name,
                            'user_surname' => $user->last_name,
                            'user_foto' => $user->foto,
                            'autor' => true
                        ];
                    } else {
                        $comments[] = [
                            'text' => strip_tags($c->text),
                            'date' => $c->date,
                            'time' => $c->time,
                            'id' => $c->id,
                            'for_client' => 0,
                            'user_name' => $user['first_name'],
                            'user_surname' => $user['last_name'],
                            'user_foto' => $user['foto'],
                            'autor' => false
                        ];
                    }
                }
            }
        } else {
            $comments = '';
        }

        return $comments;
    }
}
