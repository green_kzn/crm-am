<?php

namespace App\Http\Controllers\Tp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImpIncomingMailAttachment extends Controller
{
    public $id;
    public $contentId;
    public $name;
    public $filePath;
    public $disposition;
}
