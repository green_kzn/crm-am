<?php

namespace App\Http\Controllers\Tp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project_task;
use App\Project_print_form;
use App\Category_printform_reference;
use App\Project_task_contact;
use App\Printform_file;
use App\Printform_history;
use App\Done_printform_file;
use App\Printform_comment;
use App\Module_rule;
use App\Comment;
use App\Printform_status_reference;
use App\Events\ImpFormChangeStatus;
use Sentinel;
use Storage;
use App\Project;
use Carbon\Carbon;
use DB;
use Event;

class PrintFormController extends Controller
{
    public function getRawForms(Request $request) {
        $curUser = Sentinel::getUser();
        if ($curUser->head == 1) {
            $forms = DB::select("select `ppf`.`id`,
                                        `ppf`.`name` as `f_name`, 
                                        `p`.`name` as `p_name`,
                                        `psr`.`name` as `status`,
                                        `cr`.`name` as `client`
                                from `project_print_forms` `ppf`
                                left join `projects` `p` on `p`.`id` = `ppf`.`project_id`
                                left join `printform_status_references` `psr` on `psr`.`id` = `ppf`.`printform_status_ref_id`
                                left join `clients_references` `cr` on `p`.`client_id` = `cr`.`id`
                                where
                                `ppf`.`raw` = 1 and
                                `ppf`.`deleted_at` is null");
        } else {
            $forms = DB::select("select `ppf`.`id`,
                                        `ppf`.`name` as `f_name`, 
                                        `p`.`name` as `p_name`,
                                        `psr`.`name` as `status`,
                                        `cr`.`name` as `client`
                                from `project_print_forms` `ppf`
                                left join `projects` `p` on `p`.`id` = `ppf`.`project_id`
                                left join `printform_status_references` `psr` on `psr`.`id` = `ppf`.`printform_status_ref_id`
                                left join `clients_references` `cr` on `p`.`client_id` = `cr`.`id`
                                where
                                `ppf`.`raw` = 1 and
                                `ppf`.`observer_id` = ".$curUser->id." and
                                `ppf`.`deleted_at` is null");
        };

        return response()->json(['raw' => $forms]);
    }

    public function getProjectsForm(Request $request) {
        $curUser = Sentinel::getUser();
        if ($curUser->head == 1) {
            $forms = DB::select("select `ppf`.`id`,
                                        `ppf`.`name` as `f_name`, 
                                        `p`.`name` as `p_name`,
                                        `psr`.`name` as `status`,
                                        `cr`.`name` as `client`,
                                        `u`.`first_name`,
                                        `u`.`last_name`
                                from `project_print_forms` `ppf`
                                left join `projects` `p` on `p`.`id` = `ppf`.`project_id`
                                left join `printform_status_references` `psr` on `psr`.`id` = `ppf`.`printform_status_ref_id`
                                left join `clients_references` `cr` on `p`.`client_id` = `cr`.`id`
                                left join `users` `u` on `u`.`id` = `ppf`.`observer_id`
                                where
                                `ppf`.`raw` IS NULL and
                                (`ppf`.`printform_status_ref_id` = 5 or
                                `ppf`.`printform_status_ref_id` = 6 or
                                `ppf`.`printform_status_ref_id` = 7 or
                                `ppf`.`printform_status_ref_id` = 9 or
                                `ppf`.`printform_status_ref_id` = 10) and
                                `ppf`.`attendant` IS NULL and
                                `ppf`.`deleted_at` is null");
        } else {
            $forms = DB::select("select `ppf`.`id`,
                                        `ppf`.`name` as `f_name`, 
                                        `p`.`name` as `p_name`,
                                        `psr`.`name` as `status`,
                                        `cr`.`name` as `client`,
                                        `u`.`first_name`,
                                        `u`.`last_name`
                                from `project_print_forms` `ppf`
                                left join `projects` `p` on `p`.`id` = `ppf`.`project_id`
                                left join `printform_status_references` `psr` on `psr`.`id` = `ppf`.`printform_status_ref_id`
                                left join `clients_references` `cr` on `p`.`client_id` = `cr`.`id`
                                left join `users` `u` on `u`.`id` = `ppf`.`observer_id`
                                where
                                `ppf`.`raw` IS NULL and
                                `ppf`.`observer_id` = ".$curUser->id." and
                                (`ppf`.`printform_status_ref_id` = 5 or
                                `ppf`.`printform_status_ref_id` = 6 or
                                `ppf`.`printform_status_ref_id` = 7 or
                                `ppf`.`printform_status_ref_id` = 9 or
                                `ppf`.`printform_status_ref_id` = 10) and
                                `ppf`.`attendant` IS NULL and
                                `ppf`.`deleted_at` is null");
        };

        return response()->json(['project' => $forms]);
    }

    public function getAttendant(Request $request) {
        $curUser = Sentinel::getUser();
        if ($curUser->head == 1) {
            $forms = DB::select("select `ppf`.`id`,
                                        `ppf`.`name` as `f_name`, 
                                        `p`.`name` as `p_name`,
                                        `psr`.`name` as `status`,
                                        `cr`.`name` as `client`,
                                        `u`.`first_name`,
                                        `u`.`last_name`
                                from `project_print_forms` `ppf`
                                left join `projects` `p` on `p`.`id` = `ppf`.`project_id`
                                left join `printform_status_references` `psr` on `psr`.`id` = `ppf`.`printform_status_ref_id`
                                left join `clients_references` `cr` on `p`.`client_id` = `cr`.`id`
                                left join `users` `u` on `u`.`id` = `ppf`.`observer_id`
                                where
                                `ppf`.`raw` IS NULL and
                                (`ppf`.`printform_status_ref_id` = 7 or
                                `ppf`.`printform_status_ref_id` = 8 or
                                `ppf`.`printform_status_ref_id` = 6 or
                                `ppf`.`printform_status_ref_id` = 10) and
                                `ppf`.`attendant` = 1 and
                                `ppf`.`deleted_at` is null");
        } else {
            $forms = DB::select("select `ppf`.`id`,
                                        `ppf`.`name` as `f_name`, 
                                        `p`.`name` as `p_name`,
                                        `psr`.`name` as `status`,
                                        `cr`.`name` as `client`,
                                        `u`.`first_name`,
                                        `u`.`last_name`
                                from `project_print_forms` `ppf`
                                left join `projects` `p` on `p`.`id` = `ppf`.`project_id`
                                left join `printform_status_references` `psr` on `psr`.`id` = `ppf`.`printform_status_ref_id`
                                left join `clients_references` `cr` on `p`.`client_id` = `cr`.`id`
                                left join `users` `u` on `u`.`id` = `ppf`.`observer_id`
                                where
                                `ppf`.`raw` IS NULL and
                                `ppf`.`observer_id` = ".$curUser->id." and
                                (`ppf`.`printform_status_ref_id` = 7 or
                                `ppf`.`printform_status_ref_id` = 8 or
                                `ppf`.`printform_status_ref_id` = 6 or
                                `ppf`.`printform_status_ref_id` = 10) and
                                `ppf`.`attendant` = 1 and
                                `ppf`.`deleted_at` is null");
        };

        return response()->json(['attendant' => $forms]);
    }
}
