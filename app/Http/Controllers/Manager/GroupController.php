<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use App\Http\Controllers\PermissionsController as Permissions;
use App\Http\Controllers\Controller;
use Sentinel;
use App\User;
use App\Role_user;
use App\Role;

class GroupController extends Controller
{
    public function groups() {
        if (session('perm')['group.view']) {
            $groups = Role::all();

            return view('Manager.groups.groups', [
                'groups' => $groups,
            ]);
        } else {
            abort(503);
        }
    }

    public function groupAdd() {
        if (session('perm')['group.create']) {
            $roles = Role::all();

            return view('Manager.groups.add', [
                'roles' => $roles,
            ]);
        } else {
            abort(503);
        }
    }

    public function groupEdit($id) {
        if (session('perm')['group.update']) {
            $roles = Role::all();

            $group = Role::find($id);

            return view('Manager.groups.edit', [
                'roles' => $roles,
                'group' => $group
            ]);
        } else {
            abort(503);
        }
    }

    public function groupUpdate(Request $request, $id) {
        if (session('perm')['group.update']) {
            /*$this->validate($request, [
                'group_name' => 'required|alpha',
                'group_slug' => 'required|alpha',
            ]);*/

            //TODO Обработать исключение
            try {
                $role = Sentinel::findRoleById($id)->update([
                    'name' => $request->group_name,
                    'slug' => $request->group_slug,
                ]);
            } catch (\QueryException $e) {
                return redirect()->back();
            }

            if ($role) {
                return redirect('/Manager/groups')->with('success', 'Группа успешно обновлена');
            }
        } else {
            abort(503);
        }
    }

    public function groupDel($id) {
        if (session('perm')['group.delete']) {
            $users = Role_user::where('role_id', '=', $id)->get();

            if (count($users) > 0) {
                return redirect()->back()->with('error', 'Вы не можете удалить группу пока в ней есть пользователи');
            } else {
                $role = Role::find($id);
                $role->delete();
                return redirect()->back()->with('success', 'Группа успешно удалена');
            }
        } else {
            abort(503);
        }
    }

    public function postGroupAdd(Request $request) {
        if (session('perm')['group.create']) {
            /*$this->validate($request, [
                'group_name' => 'required|alpha',
                'group_slug' => 'required|alpha',
            ]);*/

            //TODO Обработать исключение
            try {
                $role = Sentinel::getRoleRepository()->createModel()->create([
                    'name' => $request->group_name,
                    'slug' => $request->group_slug,
                ]);
            } catch (\QueryException $e) {
                return redirect()->back();
            }

            if ($role) {
                return redirect('/Manager/groups')->with('success', 'Группа успешно создана');
            }
        } else {
            abort(503);
        }
    }
}
