<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Manager\Projects2Controller as Projects;
use App\Http\Controllers\Manager\ClientsController as Clients;
use App\Http\Controllers\Manager\UserController as Users;
use App\Printform_status_reference;
use App\Category_printform_reference;
use App\Project_print_form;
use App\Printform_comment;
use App\Printform_file;
use Carbon\Carbon;
use DB;
use Sentinel;

class PrintFormsController extends Controller
{
    public function index() {
        return view('Manager.printforms.printforms');
    }

    public function getPay() {
        $p_forms_pay = DB::select('select * from `project_print_forms`
            where `printform_status_ref_id` = 5 and `free` = 0 and `paid` = 0');

        if (count($p_forms_pay) > 0) {
            foreach ($p_forms_pay as $fr) {
                $project = Projects::getProjectById($fr->project_id);
                $client = Clients::getClientById($project->client_id);
                $observer = Users::getUserById($fr->observer_id);
                $status = Printform_status_reference::find($fr->printform_status_ref_id);
                //dd($project);

                $form_raw[] = array(
                    'id' => $fr->id,
                    'name' => $fr->name,
                    'client' => $client->name,
                    'project' => $project->name,
                    'status' => $status->name,
                    'observer' => $observer->last_name . ' ' . $observer->first_name,
                );
            }
        } else {
            $form_raw = array();
        }

        return response()->json($form_raw);
    }

    public function getPForm($id) {
        $f_task = DB::select('select `pf`.*, `cpf`.`category`
            from `project_print_forms` `pf`
            left join `category_printform_references` `cpf` on `pf`.`category_printform_ref_id` = `cpf`.`id`
            where `pf`.`id` = '.$id);

        $statuses = Printform_status_reference::whereNull('deleted_at')
            ->get();

        $status = Printform_status_reference::find($f_task[0]->printform_status_ref_id);
        $project = Projects::getProjectById($f_task[0]->project_id);
        $client = Clients::getClientById($project->client_id);
        $implement = Users::getUserById($f_task[0]->observer_id);
        //$comments = Comments::getCommentsByProjectId($f_task->project_id);
        $category = Category_printform_reference::whereNull('deleted_at')
            ->get();
        $file = Printform_file::where('project_print_form_id', '=', $id)
            ->whereNull('deleted_at')
            ->get();
        $count_tasks = Project_print_form::whereNull('deleted_at')
            ->max('number');

        if (count($file) > 0) {
            foreach ($file as $f) {
                $url = Storage::disk('local')->url($f->file);

                $files[] = [
                    'name' => $f->file,
                    'img' => $url,
                    'public_url' => $f->public_url,
                    'download_url' => $f->download_url,
                ];

            }
        } else {
            $files = '';
        }


        $comment_g = Printform_comment::where('form_id', '=', $id)
            ->whereNull('deleted_at')
            ->orderBy('id', 'desc')
            ->get();

        $cur_user = Sentinel::getUser();

        if (count($comment_g) > 0) {
            foreach ($comment_g as $c) {
                $user = Users::getUserById($c->user_id);
                if ($cur_user->id == $user['id']) {
                    $comments[] = [
                        'id' => $c->id,
                        'text' => $c->comment,
                        'user_foto' => $user['foto'],
                        'user_name' => $user['first_name'],
                        'user_surname' => $user['last_name'],
                        'date' => Carbon::parse($c->created_at)->format('d.m.Y'),
                        'time' => Carbon::parse($c->created_at)->format('H:m'),
                        'autor' => 1
                    ];
                } else {
                    $comments[] = [
                        'id' => $c->id,
                        'text' => $c->comment,
                        'user_foto' => $user['foto'],
                        'user_name' => $user['first_name'],
                        'user_surname' => $user['last_name'],
                        'date' => Carbon::parse($c->created_at)->format('d.m.Y'),
                        'time' => Carbon::parse($c->created_at)->format('H:m'),
                        'autor' => 0
                    ];
                }
            }
        } else {
            $comments = '';
        }

        return response()->json([
            'count_task' => $count_tasks,
            'form' => $f_task[0],
            'status' => $status,
            'projects' => Projects::getClientProjects($client->id),
            'project' => $project,
            'client' => $client,
            'clients' => Clients::getAllClients(),
            'implement' => $implement,
            'comments' => $comments,
            'file' => $files,
            'statuses' => $statuses,
            'category' => $category,
            'users' => Users::getAllUsers(),
        ]);
    }

    public function addComment(Request $request) {
        $data = json_decode($request->data);
        $cur_user = Sentinel::getUser();

        $p_comment = new Printform_comment();
        $p_comment->user_id = $cur_user->id;
        $p_comment->form_id = $data->form_id;
        $p_comment->comment = $data->comment;

        if ($p_comment->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function postPay(Request $request) {
        $data = json_decode($request->data);
        $p_form = Project_print_form::find($data->id);
        if ($data->paid == 1) {
            $p_form->paid = 1;
        } else {
            $p_form->paid = 0;
        };

        if ($p_form->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }
}
