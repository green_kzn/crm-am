<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Manager\GroupTasksController as GTasks;
use App\Modules_reference;
use App\Grouptasks_reference;
use App\Further_questions_reference;
use App\Module_rule;
use App\Project_module;
use App\Clarifying_question;

class ModulesController extends Controller
{
    public function index() {
        if (session('perm')['module_ref.view']) {
            $modules = Modules_reference::whereNULL('deleted_at')
                ->get();

            return view('Manager.references.modules.modules', [
                'modules' => $modules
            ]);
        } else {
            abort(503);
        }
    }

    public function getNorm(Request $request) {
        $data = json_decode($request->data);
        $rule = Module_rule::where('module_id', '=', $data->module_id)
            ->whereNull('deleted_at')
            ->whereNull('additional')
            ->get();

        $norm_contacts = 0;
        $number_contacts = 0;
        $print_id = array();
        $screen_id = array();
        foreach ($rule as $r) {
            $gt = Grouptasks_reference::find($r->grouptasks_id);
            if ($gt->is_print_form == 1) {
                $print_id = array($gt->id);
            }
            if ($gt->is_screen_form == 1) {
                $screen_id = array($gt->id);
            }
        }
        $print_screen_id = array_merge($print_id, $screen_id);
        foreach ($rule as $r2) {
            if (count($print_screen_id) > 0) {
                foreach ($print_screen_id as $ps_id) {
                    if ($r2->grouptasks_id != $ps_id) {
                        $norm_contacts += $r2->norm_contacts;
                        $number_contacts += $r2->number_contacts;
                    }
                }
            } else {
                $norm_contacts += $r2->norm_contacts;
                $number_contacts += $r2->number_contacts;
            }
            $ps_form = count($print_screen_id);
        }
        $resp = [
            'id' => $data->module_id,
            'norm_contacts' => $norm_contacts,
            'number_contacts' => $number_contacts,
            'ps_form' => $ps_form
        ];
        return response()->json($resp);
    }

    public function getModules() {
        $modules = Modules_reference::whereNULL('deleted_at')->get();

        return response()->json($modules);
    }

    public function add() {
        if (session('perm')['module_ref.create']) {
            $tasks = Grouptasks_reference::whereNULL('deleted_at')->get();
            return view('Manager.references.modules.add', [
                'tasks' => $tasks,
            ]);
        } else {
            abort(503);
        }
    }

    public function edit($id) {
        if (session('perm')['module_ref.update']) {
            $module = Modules_reference::find($id);
            $tasks = Grouptasks_reference::whereNULL('deleted_at')->get();
            $further_questions = Further_questions_reference::all();
            $module_rule = Module_rule::where('module_id', '=', $id)
                ->whereNull('deleted_at')
                ->whereNull('additional')
                ->get();

            if (count($module_rule) > 0) {
                foreach ($module_rule as $mr) {
                    $group_task = Grouptasks_reference::find($mr->grouptasks_id);
                    $c_question = Clarifying_question::where('task_id', '=', $mr->id)
                        ->whereNull('deleted_at')
                        ->get();
                    $question = '';
                    foreach ($c_question as $cq) {
                        if ($cq->task_id == $mr->id) {
                            $question[] = $cq->question;
                        }
                    }

                    $rules[] = [
                        'id' => $mr->id,
                        'task' => $mr->task,
                        'norm_contacts' => $mr->norm_contacts,
                        'number_contacts' => $mr->number_contacts,
                        'grouptasks' => $group_task->name,
                        'questions' => $question,
                    ];
                }
            } else {
                $rules = '';
            }

            return view('Manager.references.modules.edit', [
                'module' => $module,
                'tasks' => $tasks,
                'further_questions' => $further_questions,
                'module_rule' => $rules
            ]);
        } else {
            abort(503);
        }
    }

    public function postAdd(Request $request)
    {
        if (session('perm')['module_ref.create']) {
            if (!$request->name_module) {
                return redirect()->back()->with('error', 'Введите название модуля');
            }

            if (!$request->tasks) {
                return redirect()->back()->with('error', 'Укажите задачи');
            }
            $this->success = false;
            $tasks = json_decode($request->tasks);

            DB::transaction(function () use ($request, $tasks) {
               try {
                   $modules = new Modules_reference();
                   $modules->name = $request->name_module;
                   $modules->save();

                   foreach ($tasks as $t) {
                       $g_task = GTasks::getGroupTaskByName($t->group_tasks);

                       $m_tasks = new Module_rule();
                       $m_tasks->module_id = $modules->id;
                       $m_tasks->task = $t->task;
                       $m_tasks->norm_contacts = $t->norm;
                       $m_tasks->number_contacts = $t->number;
                       $m_tasks->grouptasks_id = $g_task->id;
                       $m_tasks->save();

                       foreach ($t->clarifyingQuestions[0] as $cq) {
                           if (!is_null($cq)) {
                               $c_question = new Clarifying_question();
                               $c_question->task_id = $m_tasks->id;
                               $c_question->question = $cq;
                               $c_question->save();
                           }
                       }
                   }

                   $this->success = true;
               } catch (\Exception $e) {
                   DB::rollback();
                   $this->success = false;
               }
            });

            if ($this->success) {
                return redirect('/Manager/modules')->with('success', 'Модуль успешно добавлен');
            } else {
                return redirect('/Manager/modules')->with('error', 'Произошла ошибка при добавлении модуля');
            }
        } else {
            abort(503);
        }
    }

    public function postEdit(Request $request, $id) {
        if (session('perm')['module_ref.update']) {
            if (!$request->name_module) {
                return redirect()->back()->with('error', 'Введите название модуля');
            } else {
                $module = Modules_reference::find($id);
                $module->name = $request->name_module;

                if ($module->save()) {
                    /*$data = json_decode($request->row);

                    foreach ($data as $d) {
                        if ($d->task != '') {
                            if ($d->group_tasks) {
                                $group_task = Grouptasks_reference::where('name', '=', $d->group_tasks)->firstOrFail();

                                $mr = Module_rule::where('module_id', '=', $id)
                                    ->where('task', '=', $d->task)
                                    ->whereNull('deleted_at')
                                    ->get();

                                if (count($mr) > 0) {
                                    if (isset($d->fq)) {
                                        foreach ($d->fq as $fq) {
                                            $module_rule2 = Module_rule::where('module_id', '=', $id)->get();
                                            $module_rule2->task = $d->task;
                                            $module_rule2->norm_contacts = $d->norm;
                                            $module_rule2->number_contacts = $d->number;
                                            $module_rule2->grouptasks_id = $group_task->id;
                                            //$module_rule2->questions = $request->questions;
                                            $module_rule2->further_questions_id = $fq;
                                            $module_rule2->save();
                                        }
                                    } else {
                                        $module_rule2 = Module_rule::where('module_id', '=', $id)->firstOrFail();
                                        $module_rule2->task = $d->task;
                                        $module_rule2->norm_contacts = $d->norm;
                                        $module_rule2->number_contacts = $d->number;
                                        $module_rule2->grouptasks_id = $group_task->id;
                                        //$module_rule2->questions = $request->questions;
                                        $module_rule2->save();
                                    }
                                } else {
                                    if (isset($d->fq)) {
                                        foreach ($d->fq as $fq) {
                                            $module_rule = new Module_rule();
                                            $module_rule->module_id = $id;
                                            $module_rule->task = $d->task;
                                            $module_rule->norm_contacts = $d->norm;
                                            $module_rule->number_contacts = $d->number;
                                            $module_rule->grouptasks_id = $group_task->id;
                                            //$module_rule->questions = $request->questions;
                                            $module_rule->further_questions_id = $fq;
                                            $module_rule->save();
                                        }
                                    } else {
                                        $module_rule = new Module_rule();
                                        $module_rule->module_id = $id;
                                        $module_rule->task = $d->task;
                                        $module_rule->norm_contacts = $d->norm;
                                        $module_rule->number_contacts = $d->number;
                                        $module_rule->grouptasks_id = $group_task->id;
                                        //$module_rule->questions = $request->questions;
                                        $module_rule->save();
                                    }
                                }
                            } else {
                                return redirect()->back()->with('error', 'Укажите список задач и нормативы');
                            }
                        }
                    }*/

                    return redirect('/Manager/modules')->with('success', 'Модуль успешно обновлен');
                } else {
                    return redirect('/Manager/modules')->with('error', 'Произошла ошибка при обновлении модуля');
                }
            }
        } else {
            abort(503);
        }
    }

    public function delModuleRule($id) {
        $module_rule = Module_rule::find($id);
        $module_rule->deleted_at = date("Y-m-d H:i:s");

        if ($module_rule->save()) {
            return redirect()->back()->with('success', 'Правило успешно удалено');
        } else {
            return redirect()->back()->with('error', 'Произошла ошибка при удалении правила');
        }
    }

    public function del($id) {
        if (session('perm')['module_ref.delete']) {
            $p_module = Project_module::where('module_id', '=', $id)
                ->whereNull('deleted_at')
                ->count();
            if ($p_module > 0) {
                return redirect('/Manager/modules')->with('error', 'Данный модуль используется в проектах. Закройте сначала все проекты где он используется');
            } else {
                DB::transaction(function () use ($id) {
                    try {
                        $module = Modules_reference::find($id);
                        $module->deleted_at = date("Y-m-d H:i:s");
                        $module->save();

                        $m_rule = Module_rule::where('module_id', '=', $id)->get();
                        foreach ($m_rule as $mr) {
                            $c_question = Clarifying_question::where('task_id', '=', $mr->id)->get();
                            foreach ($c_question as $cq) {
                                $cq->deleted_at = date("Y-m-d H:i:s");
                                $cq->save();
                            }

                            $mr->deleted_at = date("Y-m-d H:i:s");
                            $mr->save();
                        }

                        $this->success = true;
                    } catch (\Exception $e) {
                        DB::rollback();
                        $this->success = false;
                    };
                });

                if ($this->success) {
                    return redirect('/Manager/modules')->with('success', 'Модуль успешно удален');
                } else {
                    return redirect('/Manager/modules')->with('error', 'Произошла ошибка при удалении модуля');
                }
            }
        } else {
            abort(503);
        }
    }

    public function getModuleRule(Request $request) {
        if ($request->ajax()) {
            $rule = Module_rule::where('module_id', '=', $request->id)
                ->whereNull('deleted_at')
                ->whereNull('additional')
                ->get();
            //dd($rule);
            $norm_contacts = 0;
            $number_contacts = 0;
            $print_id = array();
            $screen_id = array();
            foreach ($rule as $r) {
                $gt = Grouptasks_reference::find($r->grouptasks_id);
                if ($gt->is_print_form == 1) {
                    $print_id = array($gt->id);
                }
                if ($gt->is_screen_form == 1) {
                    $screen_id = array($gt->id);
                }
            }
            $print_screen_id = array_merge($print_id, $screen_id);
            foreach ($rule as $r2) {
                if (count($print_screen_id) > 0) {
                    foreach ($print_screen_id as $ps_id) {
                        if ($r2->grouptasks_id != $ps_id) {
                            $norm_contacts += $r2->norm_contacts;
                            $number_contacts += $r2->number_contacts;
                        }
                    }
                } else {
                    $norm_contacts += $r2->norm_contacts;
                    $number_contacts += $r2->number_contacts;
                }
                $ps_form = count($print_screen_id);
            }
            $resp = [
                'id' => $request->id,
                'norm_contacts' => $norm_contacts,
                'number_contacts' => $number_contacts,
                'ps_form' => $ps_form
            ];
            return response()->json($resp);
        }
    }

    public function getModuleTasks(Request $request) {
        if ($request->ajax()) {
            $tasks_q = Module_rule::where('module_id', '=', $request->id)
                ->whereNull('deleted_at')
                ->whereNull('additional')
                ->get();
            foreach ($tasks_q as $t) {
                $group_task = Grouptasks_reference::where('id', '=', $t->grouptasks_id)
                    ->whereNull('deleted_at')
                    ->firstOrFail();

                $tasks[] = [
                    'id' => $t->id,
                    'task' => $t->task,
                    'norm_contacts' => $t->norm_contacts,
                    'number_contacts' => $t->number_contacts,
                    'grouptasks' => $group_task->name,
                ];
            }

            return response()->json($tasks);
        }
    }

    static public function getAllModule() {
        $module = Modules_reference::whereNull('deleted_at')->get();
        return $module;
    }

    public function addModulleRule(Request $request) {
        if ($request->ajax()) {
            $grouptasks_id = Grouptasks_reference::where('name', '=', $request->group_tasks)
                ->firstOrFail();
            $module_rule = new Module_rule();
            $module_rule->module_id = $request->module_id;
            $module_rule->task = $request->task;
            $module_rule->norm_contacts = $request->norm;
            $module_rule->number_contacts = $request->number;
            $module_rule->grouptasks_id = $grouptasks_id->id;

            if ($module_rule->save()) {
                return redirect()->back()->with('success', 'Модуль успешно удалена');
            } else {
                return redirect()->back()->with('error', 'Произошла ошибка при удалении модуля');
            }
        }
    }

    public function updateModulleRule(Request $request) {
        if ($request->ajax()) {
            $grouptasks_id = Grouptasks_reference::where('name', '=', $request->group_tasks)
                ->firstOrFail();
            $module_rule = Module_rule::find($request->rule_id);
            $module_rule->module_id = $request->module_id;
            $module_rule->task = $request->task;
            $module_rule->norm_contacts = $request->norm;
            $module_rule->number_contacts = $request->number;
            $module_rule->grouptasks_id = $grouptasks_id->id;

            if ($module_rule->save()) {
                return redirect()->back()->with('success', 'Модуль успешно удалена');
            } else {
                return redirect()->back()->with('error', 'Произошла ошибка при удалении модуля');
            }
        }
    }

    public function delModuleQuestion($mid, $qid) {
        $questions = Clarifying_question::where('task_id', '=', $mid)
            ->whereNull('deleted_at')
            ->get();

        $cur_q = Clarifying_question::find($questions[$qid]->id);
        $cur_q->deleted_at = date("Y-m-d H:i:s");

        if ($cur_q->save()) {
            return redirect()->back()->with('success', 'Уточняющий вопрос успешно удален');
        } else {
            return redirect()->back()->with('error', 'Произошла ошибка при удалении уточняющего вопроса');
        }
    }

    public function updateModuleQuestion(Request $request) {
        if ($request->ajax()) {
            $questions = Clarifying_question::where('task_id', '=', $request->qid)
                ->whereNull('deleted_at')
                ->get();

            $cur_q = Clarifying_question::find($questions[$request->loop]->id);
            $cur_q->question = $request->new_q;

            if ($cur_q->save()) {
                return response()->json('success');
            } else {
                return response()->json('error');
            }
        }
    }

    public function addModuleQuestion(Request $request) {
        if ($request->ajax()) {
            $questions = new Clarifying_question();
            $questions->task_id = $request->qid;
            $questions->question = $request->new_q;
            $questions->deleted_at = NULL;

            if ($questions->save()) {
                return response()->json('success');
            } else {
                return response()->json('error');
            }
        }
    }

    static public function getModulesByProjectId($pid) {
        $modules = Project_module::where('project_id', '=', $pid)
            ->whereNull('deleted_at')
            ->get();

        //dd($modules);
        foreach ($modules as $m) {
            $module = Modules_reference::where('id', '=', $m->module_id)
                ->whereNull('deleted_at')
                ->get();

            $module_ar[] = [
                'id' => $module[0]->id,
                'name' => $module[0]->name,
            ];
        }


        return $module_ar;
    }
}
