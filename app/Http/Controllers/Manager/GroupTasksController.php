<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Grouptasks_reference;
use App\Tasksstatus_reference;
use App\Module_rule;

class GroupTasksController extends Controller
{
    public function index() {
        if (session('perm')['task_group_ref.view']) {
            $grouptasks = Grouptasks_reference::whereNULL('deleted_at')
                ->get();

            return view('Manager.references.grouptasks.grouptasks', [
                'tasks' => $grouptasks
            ]);
        } else {
            abort(503);
        }
    }

    public function add() {
        if (session('perm')['task_group_ref.create']) {
            return view('Manager.references.grouptasks.add');
        } else {
            abort(503);
        }
    }

    public function edit($id) {
        if (session('perm')['task_group_ref.update']) {
            $task = Grouptasks_reference::find($id);

            return view('Manager.references.grouptasks.edit', [
                'task' => $task
            ]);
        } else {
            abort(503);
        }
    }

    public function postAdd(Request $request) {
        if (session('perm')['task_group_ref.create']) {
            if (!$request->name_task) {
                return redirect()->back()->with('error', 'Введите название задачи');
            } else {
                $task = new Grouptasks_reference();
                $task->name = $request->name_task;
                if ($request->isPrintForm == 'on') {
                    $task->is_print_form = 1;
                } else {
                    $task->is_print_form = 0;
                }

                if ($task->save()) {
                    return redirect('/Manager/group-tasks')->with('success', 'Задача успешно добавлена');
                } else {
                    return redirect('/Manager/group-tasks')->with('error', 'Произошла ошибка при добавлении задачи');
                }
            }
        } else {
            abort(503);
        }
    }

    public function postEdit(Request $request, $id) {
        if (session('perm')['task_group_ref.update']) {
            if (!$request->name_task) {
                return redirect()->back()->with('error', 'Введите название задачи');
            } else {
                $task = Grouptasks_reference::find($id);
                $task->name = $request->name_task;
                if ($request->isPrintForm == 'on') {
                    $task->is_print_form = 1;
                } else {
                    $task->is_print_form = 0;
                }

                if ($task->save()) {
                    return redirect('/Manager/group-tasks')->with('success', 'Задача успешно обновлена');
                } else {
                    return redirect('/Manager/group-tasks')->with('error', 'Произошла ошибка при обновлении задачи');
                }
            }
        } else {
            abort(503);
        }
    }

    public function del($id) {
        if (session('perm')['task_group_ref.delete']) {
            $task = Grouptasks_reference::find($id);
            $task->deleted_at = date("Y-m-d H:i:s");

            if ($task->save()) {
                return redirect('/Manager/group-tasks')->with('success', 'Задача успешно удалена');
            } else {
                return redirect('/Manager/group-tasks')->with('error', 'Произошла ошибка при удалении задачи');
            }
        } else {
            abort(503);
        }
    }

    static public function getAllTasks() {
        $tasks_q = Module_rule::whereNULL('deleted_at')->get();
        foreach ($tasks_q as $t) {
            $tasks[] = [
                'id' => $t->id,
                'name' => $t->task
            ];
        }
        return $tasks;
    }

    static public function getAllProjectTasks($mid) {
        $tasks_q = Module_rule::where('module_id', '=', $mid)
            ->whereNULL('deleted_at')
            ->get();
        foreach ($tasks_q as $t) {
            $tasks[] = [
                'id' => $t->id,
                'name' => $t->task
            ];
        }
        return $tasks;
    }

    static public function getAllTasksStatus() {
        $tasks_status = Tasksstatus_reference::whereNULL('deleted_at')->get();
        return $tasks_status;
    }

    static public function getAllTaskGroup() {
        $group_tasks = Grouptasks_reference::whereNull('deleted_at')->get();
        return $group_tasks;
    }

    static public function getGroupTaskByName($name) {
        $g_task = Grouptasks_reference::where('name', '=', $name)
            ->whereNull('deleted_at')
            ->firstOrFail();

        return $g_task;
    }
}
