<?php

namespace App\Http\Controllers\Manager;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Manager\EventController as EventController;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Manager\GroupTasksController as GroupTasks;
use App\Http\Controllers\Manager\ModulesController as Modules;
use App\Http\Controllers\Manager\ContactController as Contact;
use App\Http\Controllers\Manager\ProjectsController as Projects;
use App\Http\Controllers\Manager\UserController as Users;
use App\Http\Controllers\Manager\ClientsController as Clients;
use App\Http\Controllers\Controller;
use Monolog\Handler\IFTTTHandler;
use Sentinel;
use App\Task;
use App\User;
use App\Event;
use App\Project_task;
use App\Project_task_contact;
use App\Project;
use App\Project_module;
use App\Module_rule;
use App\Comment;
use App\Tasksstatus_reference;
use App\Clients_reference;
use App\Clarifying_question;
use App\Clarifying_answer;
use App\Contact_history;
use Carbon\Carbon;

class TasksController extends Controller
{
    public function index() {
        if (session('perm')['task.view']) {
            $cur_user = Sentinel::getUser();
            $res = '';

            $tasks = Task::where('contractor_id', '=', $cur_user->id)
                ->whereNULL('deleted_at')
                ->get();

            $p_tasks = Project_task::where('user_id', '=', $cur_user->id)
                //->whereNull('status')
                ->whereNull('deleted_at')
                ->get();

            if (count($p_tasks) > 0) {
                foreach ($p_tasks as $pt) {
                    $name = Module_rule::find($pt->module_rule_id);
                    $user = User::find($pt->observer_id);
                    $res[] = [
                        'name' => $name->task,
                        'created' => $pt->created_at,
                        'observer' => $user->last_name.' '.$user->first_name
                    ];
                }
            }


            if (count($tasks) > 0) {
                foreach ($tasks as $t) {
                    $user = User::find($t->observer_id);
                    $res[] = [
                        'name' => $t->title,
                        'created' => $t->created_at,
                        'observer' => $user->last_name.' '.$user->first_name,
                        'start_date' => $t->start_date,
                        'start_time' => $t->start_time,
                        'end_date' => $t->end_date,
                        'end_time' => $t->end_time,
                    ];
                }
            }

            return view('Manager.tasks.tasks', [
                'tasks' => $res,
            ]);
        } else {
            abort(503);
        }
    }

    public function addTask() {
        if (session('perm')['task.create']) {

            return view('Manager.tasks.add', [
                'users' => Users::getAllUsers(),
                'clients' => Clients::getAllClients(),
                'projects' => Projects::getAllProjects(),
            ]);
        } else {
            abort(503);
        }
    }

    public function time_add_min($time, $min) {
        list($h, $m) = explode(':', $time);
        $h = ($h + floor($m / 60)) % 24;
        $m = ($m + $min) % 60;
        return str_pad($h, 2, "0", STR_PAD_LEFT).':'.str_pad($m, 2, '0');
    }

    public function getTasks(Request $request) {
        if ($request->ajax()) {
            $cur_user = Sentinel::getUser();

            $project_task_contacts = Project_task_contact::where('next_contact', '!=', NULL)
                ->whereNull('deleted_at')
                ->where('status', '=', 1)
                ->get();

            if (count($project_task_contacts) > 0) {
                foreach ($project_task_contacts as $ptc) {
                    $p_name = Project::find($ptc->project_id);
                    $pt = Project_task::find($ptc->project_task_id);
                    $name = Module_rule::find($pt->module_rule_id);
                    $p_tasks[] = [
                        'id' => $pt->id,
                        'name' => $name['task'],
                        'p_name' => $p_name['name'],
                        'city' => $p_name['city'],
                        'duration' => $ptc->duration,
                        'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                        'next_contact' => $ptc->next_contact,
                        'prev_contact' => $ptc->prev_contact,
                    ];
                }

                foreach ($p_tasks as $p_task) {
                    $end = explode(' ', $p_task['next_contact']);

                    $json[] = array(
                        'id' => $p_task['id'],
                        'title' => $p_task['name'],
                        'start' => $p_task['next_contact'],
                        'end' => $end[0].' '.date('H:i:s',strtotime($end[1]) + strtotime($pt['duration'])-strtotime("00:00:00")),
                        'allDay' => false,
                        'color' => '#00a65a',
//                        'url' => '/manager/calls/edit/'.$p_task['id']
                    );
                }
            } else {
                $json[] = '';
            }


            return response()->json($json);


            return response()->json($json);
        }
    }

    public function postAddEvent() {
        echo "Ok";
    }

    public function editTask($id) {
        if (session('perm')['task.update']) {
            $cur_task = Task::find($id);

            $cur_date = date("j").'.'.date("n").'.'.date("Y");
            $cur_date_val = date("Y").'-'.date("m").'-'.date("d");
            $tomorrow = date('Y-m-d', strtotime($cur_date . ' +1 day'));
            $tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +1 day'));
            $plus_one_day = date('Y-m-d', strtotime($cur_date . ' +2 day'));
            $plus_one_day_val = date('Y-m-d', strtotime($cur_date . ' +2 day'));
            $plus_two_day = date('d.m.Y', strtotime($cur_date . ' +3 day'));
            $plus_two_day_val = date('Y-m-d', strtotime($cur_date . ' +3 day'));
            $plus_three_day = date('d.m.Y', strtotime($cur_date . ' +4 day'));
            $plus_three_day_val = date('Y-m-d', strtotime($cur_date . ' +4 day'));

            return view('Manager.tasks.edit', [
                'task' => $cur_task,
                'cur_date' => $cur_date,
                'cur_date_val' => $cur_date_val,
                'tomorrow' => $tomorrow,
                'tomorrow_val' => $tomorrow_val,
                'plus_one_day' => $plus_one_day,
                'plus_one_day_val' => $plus_one_day_val,
                'plus_two_day' => $plus_two_day,
                'plus_two_day_val' => $plus_two_day_val,
                'plus_three_day' => $plus_three_day,
                'plus_three_day_val' => $plus_three_day_val,
                'task_list' => GroupTasks::getAllTasks(),
                'task_status' => GroupTasks::getAllTasksStatus(),
                'modules' => Modules::getAllModule(),
                'group_task' => GroupTasks::getAllTaskGroup(),
            ]);
        } else {
            abort(503);
        }
    }

    public function editProjectTask($id) {
        if (session('perm')['task.update']) {
            $cur_p_task = Project_task::find($id);

            $name = Module_rule::find($cur_p_task['module_rule_id']);
            $p_name = Project::find($cur_p_task['project_id']);

            if (count($cur_p_task) > 0) {
                $p_tasks[] = [
                    'id' => $cur_p_task['id'],
                    'title' => $name['task'],
                    'p_name' => $p_name['name'],
                    'city' => $p_name['city'],
                    'start_date' => $cur_p_task['next_contact'],
                    'prev_contact' => $cur_p_task['prev_contact'],
                    'main_contact' => Contact::getProjectMainContact($cur_p_task['project_id']),
                    'project_contacts' => Contact::getProjectContacts($cur_p_task['project_id']),
                ];
            } else {
                $p_tasks = '';
            }
            //dd($p_tasks[0]);

            $p_tasks = (object)$p_tasks[0];

            $cur_date = date("j").'.'.date("n").'.'.date("Y");
            $cur_date_val = date("Y").'-'.date("m").'-'.date("d");
            $tomorrow = date('Y-m-d', strtotime($cur_date . ' +1 day'));
            $tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +1 day'));
            $plus_one_day = date('Y-m-d', strtotime($cur_date . ' +2 day'));
            $plus_one_day_val = date('Y-m-d', strtotime($cur_date . ' +2 day'));
            $plus_two_day = date('d.m.Y', strtotime($cur_date . ' +3 day'));
            $plus_two_day_val = date('Y-m-d', strtotime($cur_date . ' +3 day'));
            $plus_three_day = date('d.m.Y', strtotime($cur_date . ' +4 day'));
            $plus_three_day_val = date('Y-m-d', strtotime($cur_date . ' +4 day'));

            $module_id = Project_module::where('project_id', '=', $p_name['id'])
                ->whereNull('deleted_at')
                ->firstOrFail();

            $comments_q = Comment::where('project_id', '=', $cur_p_task->project_id)
                ->whereNull('deleted_at')
                ->get();

            if (count($comments_q) > 0) {
                foreach ($comments_q as $c) {
                    $user = User::find($c->user_id);
                    $comments[] = [
                        'text' => strip_tags($c->text),
                        'date' => $c->date,
                        'time' => $c->time,
                        'id' => $c->id,
                        'user_name' => $user->first_name,
                        'user_surname' => $user->last_name,
                        'user_foto' => $user->foto,
                    ];
                }
            } else {
                $comments = '';
            }

            $project_task = Project_task::where('project_id', '=', $cur_p_task->project_id)
                ->whereNull('status')
                ->whereNull('deleted_at')
                ->get();

            $task_status = GroupTasks::getAllTasksStatus();

            if (count($project_task) > 0) {
                foreach ($project_task as $pt) {
                    $name = Module_rule::find($pt->module_rule_id);
                    $status = Tasksstatus_reference::find($pt->tasksstatus_ref_id);

                    $tasks[] = [
                        'id' => $pt->id,
                        'name' => $name->task,
                        'status_id' => $status->id,
                        'status_name' => $status->name,
                        'questions' => $name->questions
                    ];
                }
            } else {
                $tasks = '';
            }

            //dd($tasks);

            $project = Project::find($cur_p_task->project_id);
            $client = Clients_reference::find($project->client_id);

            $observer = Users::getAllUsers();
            //dd(Modules::getAllModule());

            return view('Manager.tasks.edit', [
                'task' => $p_tasks,
                'cur_date' => $cur_date,
                'cur_date_val' => $cur_date_val,
                'tomorrow' => $tomorrow,
                'tomorrow_val' => $tomorrow_val,
                'plus_one_day' => $plus_one_day,
                'plus_one_day_val' => $plus_one_day_val,
                'plus_two_day' => $plus_two_day,
                'plus_two_day_val' => $plus_two_day_val,
                'plus_three_day' => $plus_three_day,
                'plus_three_day_val' => $plus_three_day_val,
                'task_list' => GroupTasks::getAllProjectTasks($module_id->module_id),
                'task_status' => $task_status,
                'modules' => Modules::getAllModule(),
                'group_task' => GroupTasks::getAllTaskGroup(),
                'comments' => $comments,
                'tasks' => $tasks,
                'client' => $client,
                'observers' => $observer,
            ]);
        } else {
            abort(503);
        }
    }

    public function setTask(Request $request) {
        $data = json_decode($request->data);
//        print_r($data);
        $date_start = explode('T', $data->start);
        $date_end = explode('T', $data->end);
        $task = Project_task_contact::where('project_task_id', '=', $data->id)
            ->first();
        if (count($task) == 0) {
            $task2 = Event::where('id', '=', $data->id)
                ->first();
            $task2->start = $date_start[0].' '.$date_start[1];
            $task2->end = $date_end[0].' '.$date_end[1];
            if ($task2->save()) {
                return response()->json(['status' => 'Ok']);
            } else {
                return response()->json(['status' => 'No']);
            }
        } else {
            $task->next_contact = $date_start[0].' '.$date_start[1];
            $task->end = $date_end[0].' '.$date_end[1];
            if ($task->save()) {
                return response()->json(['status' => 'Ok']);
            } else {
                return response()->json(['status' => 'No']);
            }
        }
    }

    public function postEditProjectTask(Request $request) {
        switch ($request->res) {
            case 'work':
                $data = json_decode($request->data);
                $data = $data[0];

                if ($data->nextContact == 0) {
                    DB::transaction(function () use ($request, $data) {
                        $project = Projects::getProjectByTaskId($request->id);
                        $date =  explode(' ', date("Y-m-d H:i:s"));
                        $cur_user = Sentinel::getUser();
                        try {
                            $comment = new Comment();
                            $comment->project_id = $project->id;
                            $comment->user_id = $cur_user->id;
                            $comment->for_client = NULL;
                            $comment->date = $date[0];
                            $comment->time = $date[1];
                            $comment->text = $request->comment6;
                            $comment->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }

                        try {
                            $comment_client = new Comment();
                            $comment_client->project_id = $project->id;
                            $comment_client->user_id = $cur_user->id;
                            $comment_client->for_client = 1;
                            $comment_client->date = $date[0];
                            $comment_client->time = $date[1];
                            $comment_client->text = $request->comment5;
                            $comment_client->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }

                        try {
                            $cur_task = Project_task::find($request->id);
                            $cur_task->tasksstatus_ref_id = $data->curTaskStatus;
                            $cur_task->duration = $data->durationTime;
                            $cur_task->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }

                        if (isset($data->otherTask)) {
                            foreach ($data->otherTask as $ot) {
                                try {
                                    $other_task = Project_task::find($ot->id);
                                    $other_task->tasksstatus_ref_id = $ot->status;
                                    $other_task->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }

                        if (isset($data->dopTask)) {
                            foreach ($data->dopTask as $dt) {
                                try {
                                    $dopTask = new Module_rule();
                                    $dopTask->task = $dt->name;
                                    $dopTask->module_id = $dt->module;
                                    $dopTask->number_contacts = $dt->numCont;
                                    $dopTask->grouptasks_id = $dt->group;
                                    $dopTask->additional = 1;
                                    $dopTask->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }

                                try {
                                    $newDopTask = new Project_task();
                                    $newDopTask->project_id = $project->id;
                                    $newDopTask->user_id = $cur_user->id;
                                    $newDopTask->observer_id = $cur_user->id;
                                    $newDopTask->module_rule_id = $dopTask->id;
                                    $newDopTask->number_contacts = $dt->numCont;
                                    $newDopTask->module_id = $dt->module;
                                    $newDopTask->tasksstatus_ref_id = 1;
                                    $newDopTask->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }

                        foreach ($data->cq as $ca) {
                            try {
                                $answer = new Clarifying_answer();
                                $answer->question_id = $ca->id;
                                $answer->project_id = $project->id;
                                $answer->answer = $ca->answer;
                                $answer->save();

                                $this->success = true;
                            } catch (\Exception $e) {
                                $this->success = false;
                            }
                        }
                    });

                    if ($this->success) {
                        return redirect()->back()->with('success', 'Статус задачи успешно обновлен');
                    } else {
                        return redirect()->back()->with('error', 'Произошла ошибка при обновлении статуса задачи');
                    }
                } else {
                    DB::transaction(function () use ($request, $data) {
                        $project = Projects::getProjectByTaskId($request->id);
                        $date =  explode(' ', date("Y-m-d H:i:s"));
                        $cur_user = Sentinel::getUser();
                        try {
                            $comment = new Comment();
                            $comment->project_id = $project->id;
                            $comment->user_id = $cur_user->id;
                            $comment->for_client = NULL;
                            $comment->date = $date[0];
                            $comment->time = $date[1];
                            $comment->text = $request->comment6;
                            $comment->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }

                        try {
                            $comment_client = new Comment();
                            $comment_client->project_id = $project->id;
                            $comment_client->user_id = $cur_user->id;
                            $comment_client->for_client = 1;
                            $comment_client->date = $date[0];
                            $comment_client->time = $date[1];
                            $comment_client->text = $request->comment5;
                            $comment_client->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }

                        try {
                            $cur_task = Project_task::find($request->id);
                            $cur_task->tasksstatus_ref_id = $data->curTaskStatus;
                            $cur_task->duration = $data->durationTime;
                            $cur_task->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }

                        if (isset($data->otherTask)) {
                            foreach ($data->otherTask as $ot) {
                                try {
                                    $other_task = Project_task::find($ot->id);
                                    $other_task->tasksstatus_ref_id = $ot->status;
                                    $other_task->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }


                        if (isset($data->dopTask)) {
                            foreach ($data->dopTask as $dt) {
                                try {
                                    $dopTask = new Module_rule();
                                    $dopTask->task = $dt->name;
                                    $dopTask->module_id = $dt->module;
                                    $dopTask->number_contacts = $dt->numCont;
                                    $dopTask->grouptasks_id = $dt->group;
                                    $dopTask->additional = 1;
                                    $dopTask->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }

                                try {
                                    $newDopTask = new Project_task();
                                    $newDopTask->project_id = $project->id;
                                    $newDopTask->user_id = $cur_user->id;
                                    $newDopTask->observer_id = $cur_user->id;
                                    $newDopTask->module_rule_id = $dopTask->id;
                                    $newDopTask->number_contacts = $dt->numCont;
                                    $newDopTask->module_id = $dt->module;
                                    $newDopTask->tasksstatus_ref_id = 1;
                                    $newDopTask->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }

                        foreach ($data->cq as $ca) {
                            try {
                                $answer = new Clarifying_answer();
                                $answer->question_id = $ca->id;
                                $answer->project_id = $project->id;
                                $answer->answer = $ca->answer;
                                $answer->save();

                                $this->success = true;
                            } catch (\Exception $e) {
                                $this->success = false;
                            }
                        }

                        try {
                            $newContTask = Project_task::find($data->nextContTask);
                            $newContTask->next_contact = $data->nextContDateStart.' '.$data->nextContTimeStart;
                            $newContTask->user_id = $data->nextContUser;
                            $newContTask->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }
                    });

                    if ($this->success) {
                        return redirect()->back()->with('success', 'Задача успешно перенесена на другое время');
                    } else {
                        return redirect()->back()->with('error', 'Произошла ошибка при перенесении задачи на другое время');
                    }
                }
                break;
            case 'next':
                DB::transaction(function () use ($request) {
                    $project = Projects::getProjectByTaskId($request->id);
                    $date =  explode(' ', date("Y-m-d H:i:s"));
                    $cur_user = Sentinel::getUser();
                    try {
                        $comment = new Comment();
                        $comment->project_id = $project->id;
                        $comment->user_id = $cur_user->id;
                        $comment->for_client = NULL;
                        $comment->date = $date[0];
                        $comment->time = $date[1];
                        $comment->text = $request->comment6;
                        $comment->save();

                        $this->success = true;
                    } catch (\Exception $e) {
                        $this->success = false;
                    }

                    try {
                        $comment_client = new Comment();
                        $comment_client->project_id = $project->id;
                        $comment_client->user_id = $cur_user->id;
                        $comment_client->for_client = 1;
                        $comment_client->date = $date[0];
                        $comment_client->time = $date[1];
                        $comment_client->text = $request->comment5;
                        $comment_client->save();

                        $this->success = true;
                    } catch (\Exception $e) {
                        $this->success = false;
                    }

                    $data = json_decode($request->data);
                    try {
                        $project_task = Project_task::find($request->id);
                        $project_task->prev_contact = $project_task->next_contact;
                        $project_task->next_contact = $data[0]->date.' '.$data[0]->time;
                        $project_task->save();

                        $this->success = true;
                    } catch (\Exception $e) {
                        $this->success = false;
                    }
                });

                if ($this->success) {
                    return redirect()->back()->with('success', 'Задача успешно перенесена на другое время');
                } else {
                    return redirect()->back()->with('error', 'Произошла ошибка при перенесении задачи на другое время');
                }
                break;
            case 'later':
                $cur_user = Sentinel::getUser();
                $project = Projects::getProjectByTaskId($request->id);
                $date =  explode(' ', date("Y-m-d H:i:s"));
                try {
                    $p_task = Project_task::find($request->id);
                    $p_task->status = 1;
                    $p_task->prev_contact = date("Y-m-d H:i:s");
                    $p_task->next_contact = NULL;
                    $p_task->save();

                    $comment = new Comment();
                    $comment->project_id = $project->id;
                    $comment->user_id = 0;
                    $comment->for_client = NULL;
                    $comment->date = $date[0];
                    $comment->time = $date[1];
                    $comment->text = 'До клиента не дозвонились';
                    $comment->save();

                    $this->success = true;
                } catch (\Exception $e) {
                    $this->success = false;
                }

                if ($this->success) {
                    return redirect()->back()->with('success', 'Задача успешно перенесена в клиентский отделл');
                } else {
                    return redirect()->back()->with('error', 'Произошла ошибка при перенесении задачи в клиентский отделл');
                }
                break;
        }
    }

    public function getTasksForAmcalendar(Request $request) {
        if ($request->ajax()) {
            $project_task_contact = Project_task_contact::where('user_id', '=', $request->user_id)
                ->where('next_contact', 'like', $request->date.'%')
                //->where('tasksstatus_ref_id', '!=', 2)
                ->whereNull('deleted_at')
                ->whereNull('status')
                ->get();

            if (count($project_task_contact) > 0) {
                foreach ($project_task_contact as $ptc) {
                    $pt = Project_task::find($ptc->project_task_id);
                    $task_name = Module_rule::find($pt->module_rule_id);
                    $date = explode(' ', $ptc->next_contact);
                    $tasks[] = [
                        'id' => $ptc->id,
                        'time' => $date[1],
                        'task' => $task_name->task
                    ];
                }
            } else {
                $tasks[] = '';
            }

            return response()->json($tasks);
        }
    }

    static public function getTaskById($id) {
        $task = Project_task::whereNull('deleted_at')
            ->where('id', '=', $id)
            ->firstOrFail();

        return $task;
    }

    public function getClarificationQuestions(Request $request) {
        if ($request->ajax()) {
            $p_task = Project_task::find($request->tid);
            $c_question = Clarifying_question::where('task_id', '=', $p_task->module_rule_id)
                ->whereNull('deleted_at')
                ->get();
            return response()->json($c_question);
        }
    }

    public function postAddTask(Request $request) {
        switch ($request->typeTask) {
            case 'single_task':
                dd($request->all());
                break;
            case 'introduction':
                dd($request->all());
                break;
        }
    }

    static public function getHistory(Request $request) {
        if ($request->ajax()) {
            $task_history = Contact_history::where('task_id', '=', $request->id)
                ->get();

            if (count($task_history) > 0) {
                foreach ($task_history as $th) {
                    $status = Tasksstatus_reference::find($th->tasksstatus_ref_id);
                    $user = Users::getUserById($th->user_id);
                    $resp[] = [
                        'id' => $th->id,
                        'date' => Carbon::parse($th->created_at)->format('d.m.Y H:i'),
                        'status' => $status['name'],
                        'user' => $user->first_name.' '.$user->last_name
                    ];
                }
            } else {
                $resp[] = '';
            }

            return response()->json($resp);
        }
    }

    public function getTasks2($id, Request $request) {
        if ($request->ajax()) {
            $cur_user = Sentinel::getUser();

//            $project_task_contacts = Project_task_contact::where('user_id', '=', $id)
//                //->where('next_contact', '!=', NULL)
//                ->whereNull('deleted_at')
//                //->whereNull('status')
//                ->get();


            $project_task_contacts = DB::select("select `ptc`.*, ptc.id as ptc_id, 
                `pt`.`id`, `p`.`name`, `pt`.`tasksstatus_ref_id`, 
                `mr`.`task`, `u`.`first_name`, `u`.`last_name`,
                `ct`.`text` as comment, `p`.`id` as project_id, `ptc`.`contact_id`
                from `project_task_contacts` ptc
                left join `projects` `p` on `p`.`id` = `ptc`.`project_id`
                left join `project_tasks` `pt` on `pt`.`id` = `ptc`.`project_task_id`
                left join `module_rules` `mr` on `mr`.`id` = `pt`.`module_rule_id`
                left join `users` `u` on `u`.`id` = `ptc`.`observer_id`
                left join `comment_tasks` `ct` on `ct`.`task_id` = `pt`.`id`
                where 
                `ptc`.`deleted_at` is null and
                `ptc`.`user_id` = ".$id);


            if (count($project_task_contacts) > 0) {
                foreach ($project_task_contacts as $ptc) {
                    $p_tasks[] = [
                        'id' => $ptc->id,
                        'ptc_id' => $ptc->ptc_id,
                        'project_id' => $ptc->project_id,
                        'name' => $ptc->task. "\r\n".$ptc->name,
                        'p_name' => $ptc->name,
//                            'city' => $p_name['city'],
                        'duration' => $ptc->duration,
                        'tasksstatus_ref_id' => $ptc->tasksstatus_ref_id,
                        'callbackstatus_ref_id' => $ptc->callbackstatus_ref_id,
                        'next_contact' => $ptc->next_contact ?? $ptc->prev_contact,
                        'prev_contact' => $ptc->prev_contact,
                        'end' => $ptc->end,
                        'comment' => $ptc->comment,
                        'observer' => $ptc->first_name.' '.$ptc->last_name,
                        'date_production' => $ptc->date_production ? Carbon::parse($ptc->date_production)->format('d.m.Y H:i:s') : '',
                        'contact_id' => $ptc->contact_id
                    ];
                }
                foreach ($p_tasks as $p_task) {
                    $date = explode(' ', $p_task['next_contact']);

                    $color = null;
                    if (!$p_task['callbackstatus_ref_id']) {
                        switch ($p_task['tasksstatus_ref_id']) {
                            case '1':
                                $color = '#00a65a';
                                break;
                            case '2':
                                $color = '#f39c12';
                                break;
                            case '3':
                                $color = '#00c0ef';
                                break;
                        }
                    } else {
                        $color = '#dd4b39';
                    }

                    if ($color) {
                        $comment = $p_task['comment'] ? "\nКомментарий: ".$p_task['comment'] : '';
                        $json[] = array(
                            'id' => $p_task['id'],
                            'ptc_id' => $p_task['ptc_id'],
                            'project_id' => $p_task['project_id'],
                            'contact_id' => $p_task['contact_id'],
                            'title' => $p_task['name'],
                            'start' => $p_task['next_contact'],
                            //'end' => $end[0] . ' ' . date('H:i:s', strtotime($end[1]) + strtotime($pt['duration']) - strtotime("00:00:00")),
                            'end' => $p_task['end'],
                            'allDay' => false,
                            'color' => $color,
                            'extendedProps' => 'Постановщик: ' . $p_task['observer'] . ', дата: ' . $p_task['date_production'].$comment,
//                            'url' => $p_task['task'] ? '/implementer/project-task/edit/' . $p_task['id'] . '/' . $date[0] : null,
                            //'timeFormat' => 'H:mm'
                        );
                    }
                }

            } else {
                $json[] = '';
            }
        }
//         $event = Event::whereNull('deleted_at')
//             ->where('user_id', '=', $id)
//             ->get();

//         foreach($event as $e) {
//             $json[] = array(
//                 'id' => $e['id'],
//                 'title' => $e['title'],
//                 'start' => $e['start'],
// //                            'end' => $end[0].' '.date('H:i:s',strtotime($end[1]) + strtotime($p_task['duration'])-strtotime("00:00:00")),
//                 'end' => $e['end'],
//                 'allDay' => false,
//                 'color' => $e['color'],
//                 'className' => 'edit',
// //                'url' => '/implementer/project-task/edit/'.$p_task['id'] . '/' . $date[0],
//                 //'timeFormat' => 'H(:mm)'
//             );
//         };
//        print_r($json);

        $event = DB::select('select `u`.`first_name` as `first_name`, `u`.`last_name` as `last_name`, `e`.* 
            from `events` `e`
            left join `users` `u` on `u`.`id` = `e`.`observer_id`
            where 
            `e`.`deleted_at` is null and
            `e`.`user_id` = '.$id);

        // print_r($event);
        // die;

        foreach($event as $e) {
        // $user = Sentinel::getUser($e['observer_id']);

            $json[] = array(
                'id' => $e->id,
                'title' => $e->title,
                'start' => $e->start,
            //                            'end' => $end[0].' '.date('H:i:s',strtotime($end[1]) + strtotime($p_task['duration'])-strtotime("00:00:00")),
                'end' => $e->end,
                'allDay' => false,
                'color' => $e->color,
                'className' => 'edit',
                'extendedProps' => 'Постановщик: '.$e->first_name.' '.$e->last_name.', дата: '.Carbon::parse($e->date_production)->format('d.m.Y H:i:s'),
            //                'url' => '/implementer/project-task/edit/'.$p_task['id'] . '/' . $date[0],
                //'timeFormat' => 'H(:mm)'
            );
        };
        return response()->json($json);
    }

    public function getCurTask(Request $request) {
        $data = json_decode($request->data);
//        $p_task = Project_task::find($data->id);
        $p_task = DB::select('select * from `project_tasks` left join `project_task_contacts` on `project_task_contacts`.`project_task_id` = `project_tasks`.`id` left join `module_rules` on `module_rules`.`id` = `project_tasks`.`module_rule_id` where `project_tasks`.`id` = '.$data->id);

        return response()->json($p_task);
    }
}
