<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IncomingMailAttachment extends Controller
{
    public $id;
    public $contentId;
    public $name;
    public $filePath;
    public $disposition;
}
