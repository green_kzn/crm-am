<?php

namespace App\Http\Controllers\Manager;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Manager\CitysController as Citys;
use App\Http\Controllers\Manager\PostsController as Posts;
use App\Http\Controllers\Manager\ContactController as Contacts;
use Sentinel;
use App\Clients_reference;
use App\Contact;
use App\Posts_reference;
use App\Events\ManagerAddClient;

class ClientsController extends Controller
{
    public function index() {
        if (session('perm')['clients_ref.view']) {
            $clients_q = Clients_reference::whereNull('deleted_at')
                ->get();

            if (count($clients_q) > 0) {
                foreach ($clients_q as $c) {
                    $clients[] = [
                        'id' => $c->id,
                        'name' => $c->name,
                        'city' => Citys::getCityById($c->city_id),
                    ];
                }
            } else {
                $clients = '';
            }

            return view('/Manager/references/clients/clients', [
                'clients' => $clients
            ]);
        } else {
            abort(503);
        }
    }

    public function getClient(Request $request) {
        $data = json_decode($request->data);
        
        $contacts_q = Contact::where('client_id', '=', $data->client_id)
            ->whereNull('deleted_at')
            ->get();
        
            // dd($data->client_id);
        if (count((array)$contacts_q) > 0) {
            foreach ($contacts_q as $c) {
                $post = Posts_reference::find($c->post_id);
                
                $contacts[] = [
                    'id' => $c->id,
                    'first_name' => $c->first_name,
                    'last_name' => $c->last_name,
                    'patronymic' => $c->patronymic,
                    'phone' => $c->phone,
                    'email' => $c->email,
                    'main' => $c->main,
                    'mail_rep' => $c->mail_rep,
                    'post' => $post->name ?? '',
                ];
            }
        } else {
            $contacts[] = [];
        };
        
        
        return response()->json($contacts);
    }

    public function add() {
        if (session('perm')['clients_ref.create']) {

            return view('/Manager/references/clients/add', [
                'citys' => Citys::getAllCitys(),
                'posts' => Posts::getAllPosts(),
            ]);
        } else {
            abort(503);
        }
    }

    public function getClients() {
        $clients = Clients_reference::whereNull('deleted_at')->get();
        foreach ($clients as $c) {
            $clients_ar[] = [
                'code' => $c['id'],
                'value' => $c['name']
            ];
        }
        return response()->json($clients_ar); 
    }

    public function postAdd(Request $request) {
        $data = json_decode($request->data);
        
        $cur_user = Sentinel::getUser();
        $clients_ar = $data->contacts_ar;

        

        DB::transaction(function () use ($data, $clients_ar, $cur_user) {
            try {
                $client = new Clients_reference();
                $client->name = $data->project_name;
                $client->city_id = $data->city_id;
                $client->save();
                event(new ManagerAddClient($client->name, $cur_user->first_name.' '.$cur_user->last_name));

                foreach ($clients_ar as $ca) {
                    
                    try {
                        $post = Posts_reference::where('name', '=', $ca->contact_post)->firstOrFail();
                        $contact = new Contact();
                        $contact->first_name = $ca->contact_name;
                        $contact->last_name = $ca->contact_surname;
                        $contact->patronymic = $ca->contact_patronymic;
                        $contact->post_id = $post->id;
                        $contact->phone = $ca->contact_phone;
                        $contact->email = $ca->contact_email;
                        $contact->client_id = $client->id;
                        $contact->user_id = $cur_user->id;
                        $contact->save();

                        $this->success = true;
                    } catch (\Exception $e) {
                        DB::rollback();
                        $success = false;
                    }
                }
            } catch (\Exception $e) {
                DB::rollback();
                $this->success = false;
            }
        });
        if ($this->success) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'No']);
        }
        /*if (session('perm')['clients_ref.create']) {
            if (!$request->name_client) {
                return redirect()->back()->with('error', 'Укажите наименование клиента');
            }
            //$success = false;
            $cur_user = Sentinel::getUser();
            $clients_ar = json_decode($request->contacts);

            DB::transaction(function () use ($request, $clients_ar, $cur_user) {
                try {
                    $client = new Clients_reference();
                    $client->name = $request->name_client;
                    $client->city_id = $request->client_city;
                    $client->save();

                    foreach ($clients_ar as $ca) {
                        try {
                            $post = Posts_reference::where('name', '=', $ca->post)->firstOrFail();
                            $contact = new Contact();
                            $contact->first_name = $ca->name;
                            $contact->last_name = $ca->surname;
                            $contact->patronymic = $ca->patronymic;
                            $contact->post_id = $post->id;
                            $contact->phone = $ca->phone;
                            $contact->email = $ca->email;
                            $contact->client_id = $client->id;
                            $contact->user_id = $cur_user->id;
                            $contact->save();
                            $this->success = true;
                        } catch (\Exception $e) {
                            DB::rollback();
                            $success = false;
                        }
                    }
                } catch (\Exception $e) {
                    DB::rollback();
                    $this->success = false;
                }
            });

            if ($this->success) {
                return redirect('/Manager/clients')->with('success', 'Клиент успешно создан');
            } else {
                return redirect('/Manager/clients')->with('error', 'Произошла ошибка при создании клиента');
            }
        } else {
            abort(503);
        }*/
    }

    public function del($id) {
        if (session('perm')['clients_ref.delete']) {
            $client = Clients_reference::find($id);
            $client->deleted_at = date("Y-m-d H:i:s");

            if ($client->save()) {
                return redirect()->back()->with('success', 'Клиент успешно удален');
            } else {
                return redirect()->back()->with('error', 'Произошла ошибка при удалении клиента');
            }
        } else {
            abort(503);
        }
    }

    public function edit($id) {
        if (session('perm')['clients_ref.update']) {
            $client = Clients_reference::where('id', '=', $id)
                ->whereNull('deleted_at')
                ->firstOrFail();

            $contacts_q = Contacts::getClientAllContacts($client->id);
            foreach ($contacts_q as $c) {
                $post = Posts::getPostById($c->post_id);
                $contacts[] = [
                    'id' => $c->id,
                    'first_name' => $c->first_name,
                    'last_name' => $c->last_name,
                    'patronymic' => $c->patronymic,
                    'post' => $post->name,
                    'phone' => $c->phone,
                    'email' => $c->email,
                    'main' => $c->main,
                    'mail_rep' => $c->mail_rep,
                ];
            }

            return view('/Manager/references/clients/edit', [
                'citys' => Citys::getAllCitys(),
                'posts' => Posts::getAllPosts(),
                'client' => $client,
                'contacts' => $contacts
            ]);
        } else {
            abort(503);
        }
    }

    public function postEdit(Request $request, $id) {
        if (session('perm')['clients_ref.update']) {
            if (!$request->name_client) {
                return redirect()->back()->with('error', 'Укажите наименование клиента');
            }
            $cur_user = Sentinel::getUser();
            $clients_ar = json_decode($request->contacts);

            DB::transaction(function () use ($request, $clients_ar, $id, $cur_user) {
                try {
                    $client = Clients_reference::find($id);
                    $client->name = $request->name_client;
                    $client->city_id = $request->client_city;
                    $client->save();

                    foreach ($clients_ar as $ca) {
                        try {
                            $post = Posts_reference::where('name', '=', $ca->post)->firstOrFail();
                            if (isset($ca->id)) {
                                $contact = Contact::find($ca->id);
                                $contact->first_name = $ca->name;
                                $contact->last_name = $ca->surname;
                                $contact->patronymic = $ca->patronymic;
                                $contact->post_id = $post->id;
                                $contact->phone = $ca->phone;
                                $contact->email = $ca->email;
                                $contact->client_id = $client->id;
                                $contact->user_id = $cur_user->id;
                                $contact->save();
                                $this->success = true;
                            } else {
                                $contact = new Contact();
                                $contact->first_name = $ca->name;
                                $contact->last_name = $ca->surname;
                                $contact->patronymic = $ca->patronymic;
                                $contact->post_id = $post->id;
                                $contact->phone = $ca->phone;
                                $contact->email = $ca->email;
                                $contact->client_id = $client->id;
                                $contact->user_id = $cur_user->id;
                                $contact->save();
                                $this->success = true;
                            }

                        } catch (\Exception $e) {
                            DB::rollback();
                            $this->success = false;
                        }
                    }
                } catch (\Exception $e) {
                    DB::rollback();
                    $this->success = false;
                }
            });

            if ($this->success) {
                return redirect('/Manager/clients')->with('success', 'Клиент успешно создан');
            } else {
                return redirect('/Manager/clients')->with('error', 'Произошла ошибка при создании клиента');
            }
        } else {
            abort(503);
        }
    }

    static public function getAllClients() {
        $clients = Clients_reference::whereNull('deleted_at')->get();
        return $clients;
    }

    static public function getClientById($id) {
        $client = Clients_reference::where('id', '=', $id)
            ->whereNull('deleted_at')->firstOrFail();
        return $client;
    }
}
