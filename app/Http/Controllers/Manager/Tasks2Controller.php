<?php

namespace App\Http\Controllers\Manager;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Implementer\ImpEventController as EventController;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Implementer\ImpGroupTasksController as GroupTasks;
use App\Http\Controllers\Implementer\ImpModules2Controller as Modules;
use App\Http\Controllers\Implementer\ImpContact2Controller as Contact;
use App\Http\Controllers\Implementer\ImpProjectsController as Projects;
use App\Http\Controllers\Implementer\ImpUser2Controller as Users;
use App\Http\Controllers\Implementer\ImpClientsController as Clients;
use App\Http\Controllers\Controller;
use Monolog\Handler\IFTTTHandler;
use Sentinel;
use App\Task;
use App\User;
use App\Project_task;
use App\Project_contacts;
use App\Project_task_contact;
use App\Project_print_form;
use App\Printform_status_reference;
use App\Project;
use App\Project_module;
use App\Module_rule;
use App\Comment;
use App\Comment_task;
use App\Tasksstatus_reference;
use App\Clients_reference;
use App\Clarifying_question;
use App\Clarifying_answer;
use App\Event;
use Carbon\Carbon;
use App\Mail\SendComment;
use Mail;

class Tasks2Controller extends Controller
{
    public function editEvent(Request $request) {
        $data = json_decode($request->data);
        $end = explode('T', $data->end);
        $event = Event::find($data->id);
        $event->end = $end[0].' '.$end[1];
        if ($event->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function setTasks(Request $request) {
        $data = json_decode($request->data);
        // dd($data);
        $date_start = explode('T', $data->start);
        $date_end = explode('T', $data->end);

        if ($data->isEvent) {
            $task2 = Event::where('id', '=', $data->id)
                ->first();
            $task2->start = $date_start[0].' '.$date_start[1];
            $task2->end = $date_end[0].' '.$date_end[1];
            if ($task2->save()) {
                return response()->json(['status' => 'Ok']);
            } else {
                return response()->json(['status' => 'No']);
            }
        } else {
            $task = Project_task_contact::where('project_task_id', '=', $data->id)
                ->whereNull('deleted_at')
                ->first();
            $task->next_contact = $date_start[0].' '.$date_start[1];
            $task->end = $date_end[0].' '.$date_end[1];
            if ($task->save()) {
                return response()->json(['status' => 'Ok']);
            } else {
                return response()->json(['status' => 'No']);
            }
        };
    }

    public function editCont(Request $request) {
        $data = json_decode($request->data);
        $date = explode('T', $data->end);
        $task = Project_task_contact::where('project_task_id', '=', $data->id)
            ->whereNull('deleted_at')
            ->first();
        $task->end = $date[0].' '.$date[1];
        if ($task->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'No']);
        }
    }
    public function delCont(Request $request) {
        $data = json_decode($request->data);
        
        $task = Project_task::find($data->id);
        if ($task->tasksstatus_ref_id == 1) {
            $cont = Project_task_contact::where('project_task_id', '=', $data->id)
                ->whereNull('deleted_at')
                ->first();

            $cont->deleted_at = Carbon::now()->format('Y-m-d H:i:s');

            if ($cont->save()) {
                return response()->json(['status' => 'Ok']);
            } else {
                return response()->json(['status' => 'Error']);
            }
        } else {
            return response()->json(['message' => 'Удаление не возможно']);
        };
    }

    public function delEvent(Request $request) {
        $data = json_decode($request->data);
        $event = Event::find($data->id);
        $event->deleted_at = Carbon::now()->format('Y-m-d H:m:s');
        if ($event->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'No']);
        }
    }
}
