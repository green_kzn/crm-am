<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Posts_reference;

class PostsController extends Controller
{
    static public function getAllPosts() {
        $posts = Posts_reference::whereNull('deleted_at')
            ->get();

        return $posts;
    }

    static public function getPostById($id) {
        $post = Posts_reference::where('id', '=', $id)
            ->whereNull('deleted_at')
            ->firstOrFail();

        return $post;
    }

    public function getAllPostsAjax(Request $request) {
        if ($request->ajax()) {
            $posts = Posts_reference::whereNull('deleted_at')
                ->get();
            return response()->json($posts);
        }
    }

    public function getPosts() {
        $posts = Posts_reference::whereNull('deleted_at')
            ->get();
        return response()->json($posts);
    }
}
