<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact;
use App\Project_contact;
use App\Posts_reference;
use Sentinel;
use DB;

class ContactController extends Controller
{
    public function add(Request $request) {
        $data = json_decode($request->data);
        // print_r($data);
        // die;
        $cur_user = Sentinel::getUser();
        
        $contact = Contact::create([
            'first_name' => $data->newFirstName,
            'last_name' => $data->newLastName,
            'patronymic' => $data->newPatronymic,
            'post_id' => $data->newPost,
            'phone' => $data->newPhone,
            'email' => $data->newEmail,
            'client_id' => $data->client,
            'user_id' => $cur_user->id]);

        $contact = DB::select('select `c`.`id`, `c`.`first_name`,
        `c`.`last_name`, `c`.`patronymic`, `pr`.`name` as `post_name`,`pr`.`name` as `post`, `pr`.`id` as `post_id`, `c`.`phone`, `c`.`email`
        from `contacts` `c` 
        left join `posts_references` `pr` on `pr`.`id` = `c`.`post_id`
        where `c`.`id` = '.$contact->id);
        
        if (!empty($contact)) {
            return response()->json(['status' => 'Ok', 'contact' => $contact[0]]);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function getPContacts(Request $request, $id) {
        $contacts = DB::select('select `c`.`id` as `id`, `pc`.`main`, `pc`.`mail_rep`, `c`.`first_name`, 
            `c`.`last_name`, `c`.`patronymic`, `pr`.`name` as `post_name`, `pr`.`id` as `post_id`, `c`.`phone`, `c`.`email`
            from `project_contacts` `pc`
            left join `contacts` `c` on `pc`.`contact_id` = `c`.`id`
            left join `posts_references` `pr` on `pr`.`id` = `c`.`post_id`
            where
            `pc`.`deleted_at` is null and
            `c`.`deleted_at` is null and
            `pc`.`project_id` = '.$id);

        return response()->json([
            'contacts' => $contacts,
        ]);
    }

    public function addPContacts(Request $request) {
        $data = json_decode($request->data);
        $cur_user = Sentinel::getUser();

        $contact = new Contact();
        $contact->first_name = $data->newContact->firstName;
        $contact->last_name = $data->newContact->lastName;
        $contact->patronymic = $data->newContact->patronymic;
        $contact->post_id = $data->newContact->post_id;
        $contact->phone = $data->newContact->phone;
        $contact->email = $data->newContact->email;
        $contact->client_id = $data->client_id;
        $contact->user_id = $cur_user->id;
        $contact->save();

        $p_contact = new Project_contact();
        $p_contact->contact_id = $contact->id;
        $p_contact->project_id = $data->project_id;
        $p_contact->main = $data->newContact->main;
        $p_contact->mail_rep = $data->newContact->mail_rep;

        if ($p_contact->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function delPContacts(Request $request) {
        $data = json_decode($request->data);
        $p_cont = Project_contact::where('contact_id', '=', $data->client_id)
            ->where('project_id', '=', $data->p_id)
            ->first();

        $p_cont->deleted_at = date("Y-m-d H:i:s");

        if ($p_cont->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'error']);
        }
    }

    public function editPContacts(Request $request) {
        $data = json_decode($request->data);

        $cont = Contact::find($data->cont->id);
        $cont->first_name = $data->cont->first_name;
        $cont->last_name = $data->cont->last_name;
        $cont->patronymic = $data->cont->patronymic;
        $cont->email = $data->cont->email;
        $cont->post_id = $data->cont->post;
        if ($cont->save()) {
            $p_cont = Project_contact::where('contact_id', '=', $cont->id)
                ->where('project_id', '=', $data->p_id)
                ->first();

            $p_cont->main = $data->cont->main;
            $p_cont->mail_rep = $data->cont->mail_rep;
            if ($p_cont->save()) {
                return response()->json(['status' => 'Ok']);
            } else {
                return response()->json(['status' => 'Error']);
            }
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function del(Request $request) {
        $data = json_decode($request->data);
        $contact = Contact::find($data->id);
        $contact->deleted_at = date("Y-m-d H:i:s");

        if ($contact->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    static public function getProjectMainContact($pid) {
        $contact_id = Project_contact::where('project_id', '=', $pid)
            // ->where('main', '=', 1)
            ->whereNull('deleted_at')
            ->first();

        if ($contact_id) {
            $contact = Contact::find($contact_id->contact_id);

            if ($contact) {
                $res = (object)[
                    "id" => $contact['id'],
                    "first_name" => $contact['first_name'],
                    "last_name" => $contact['last_name'],
                    "patronymic" => $contact['patronymic'],
                    "post_id" => $contact['post_id'],
                    "phone" => $contact['phone'],
                    "email" => $contact['email'],
                    "project_id" => $contact['project_id'],
                    "mail_rep" => $contact['mail_rep'],
                    "user_id" => $contact['user_id'],
                    "created_at" => $contact['created_at'],
                    "updated_at" => $contact['updated_at'],
                    "deleted_at" => $contact['deleted_at']
                ];
            } else {
                $res = '';
            }
        } else {
            $res = false;
        }

        return $res;
    }

    static public function getProjectContacts($pid, $onlyMain = false) {
        $contacts = Project_contact::where('project_id', '=', $pid)
            ->whereNull('deleted_at');
        if ($onlyMain) {
            $contacts->where('main', 1);
        }
        $contacts = $contacts->get();
        $p_contact = [];
        foreach ($contacts as $c) {
            $contact = Contact::find($c->contact_id);
            $p_contact[] = [
                "id" => $contact['id'],
                "first_name" => $contact['first_name'],
                "last_name" => $contact['last_name'],
                "patronymic" => $contact['patronymic'],
                "post_id" => $contact['post_id'],
                "phone" => $contact['phone'],
                "email" => $contact['email'],
                "project_id" => $contact['project_id'],
                "mail_rep" => $contact['mail_rep'],
                "user_id" => $contact['user_id'],
                "created_at" => $contact['created_at'],
                "updated_at" => $contact['updated_at'],
                "deleted_at" => $contact['deleted_at']
            ];
        }

        return $p_contact;
    }

    public function update(Request $request, $id) {
        $contact = Contact::find($id);

        return view('Manager.contacts.edit', [
            'contact' => $contact
        ]);
    }

    static public function getClientAllContacts($id) {
        $contacts = Contact::where('client_id', '=', $id)
            ->whereNull('deleted_at')
            ->get();

        return $contacts;
    }

    public function getClientsContact(Request $request) {
        if ($request->ajax()) {
            $contacts_q = Contact::where('client_id', '=', $request->id)
                ->whereNull('deleted_at')
                ->get();
            foreach ($contacts_q as $c) {
                $post = Posts_reference::find($c->post_id);
                $contacts[] = [
                    'id' => $c->id,
                    'first_name' => $c->first_name,
                    'last_name' => $c->last_name,
                    'patronymic' => $c->patronymic,
                    'phone' => $c->phone,
                    'email' => $c->email,
                    'main' => $c->main,
                    'mail_rep' => $c->mail_rep,
                    'post' => $post->name ?? '',
                ];
            }
            return response()->json($contacts);
        }
    }

    public function saveContact(Request $request) {
        if ($request->ajax()) {
            $contact = Contact::find($request->id);
            $contact->first_name = $request->name;
            $contact->last_name = $request->surname;
            $contact->patronymic = $request->patronymic;
            $contact->post_id = $request->post;
            $contact->phone = $request->phone;
            $contact->email = $request->email;
            $contact->email = $request->email;

            if($contact->save()) {
                return response()->json(['success' => 'Ok']);
            } else {
                return response()->json(['success' => 'Error']);
            }
        }
    }


    public function edit(Request $request, $id){
        $contact = Contact::find($id);
        $data = json_decode($request->data);
        $newContact = $data->contact;

        $contact->first_name = $newContact->first_name;
        $contact->last_name = $newContact->last_name;
        $contact->patronymic = $newContact->patronymic;
        $contact->post_id = Posts_reference::where('name',$newContact->post)->first()->id;
        $contact->phone = $newContact->phone;
        $contact->email = $newContact->email;

        if ($contact->save()) {
            return response()->json(['status'=>'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }
}
