<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Settings;

class SettingsController extends Controller
{
    public function index() {
        $settings = Settings::whereNull('deleted_at')->get();

        return view('Manager.settings.settings', [
            'settings' => $settings
        ]);
    }

    static public function saveSettings($array) {
        foreach ($array as $k => $ar) {
            try {
                $value = Settings::where('slug', '=', $k)
                    ->get();

                if (count($value) > 0) {
                    $value[0]->value = $ar;

                    if ($value[0]->save()) {
                        $success = true;
                    } else {
                        $success = false;
                    }
                } else {
                    $success = false;
                }

                $success = true;
            } catch (\Exception $e) {
                $success = false;
            }
        }

        return $success;
    }

    static public function getSettings($slug) {
        $settings = Settings::where('slug', '=', $slug)
            ->get();

        return $settings[0];
    }
}
