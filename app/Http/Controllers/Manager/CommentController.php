<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Comment;
use App\User;
use Sentinel;
use DB;

class CommentController extends Controller
{
    public function getAllCommentsByProjectId($id) {
        $count = DB::select('select count(`id`) as `count`
            from `comments`
            where `deleted_at` is null and
            `project_id` = '.$id);

        if ($count[0]->count > 0) {
            $comments_q = Comment::where('project_id', '=', $id)
            ->orderBy('date', 'desc')
            ->orderBy('time', 'desc')
            ->whereNULL('deleted_at')
            ->get();

            foreach ($comments_q as $c) {
                $user = User::find($c->user_id);
                $cur_user = Sentinel::getUser();
                
                if ($c->for_client != NULL) {
                    if ($user->id == $cur_user->id) {
                        $comments[] = [
                            'text' =>  htmlspecialchars_decode($c->text),
                            'date' => $c->date,
                            'time' => $c->time,
                            'id' => $c->id,
                            'for_client' => 1,
                            'user_name' => $user->first_name,
                            'user_surname' => $user->last_name,
                            'user_foto' => $user->foto,
                            'autor' => true
                        ];
                    } else {
                        $comments[] = [
                            'text' =>  htmlspecialchars_decode($c->text),
                            'date' => $c->date,
                            'time' => $c->time,
                            'id' => $c->id,
                            'for_client' => 1,
                            'user_name' => $user->first_name,
                            'user_surname' => $user->last_name,
                            'user_foto' => $user->foto,
                            'autor' => false
                        ];
                    }
                } else {
                    if ($user['id'] == $cur_user->id) {
                        $comments[] = [
                            'text' =>  htmlspecialchars_decode($c->text),
                            'date' => $c->date,
                            'time' => $c->time,
                            'id' => $c->id,
                            'for_client' => 0,
                            'user_name' => $user->first_name,
                            'user_surname' => $user->last_name,
                            'user_foto' => $user->foto,
                            'autor' => true
                        ];
                    } else {
                        $comments[] = [
                            'text' =>  htmlspecialchars_decode($c->text),
                            'date' => $c->date,
                            'time' => $c->time,
                            'id' => $c->id,
                            'for_client' => 0,
                            'user_name' => $user['first_name'],
                            'user_surname' => $user['last_name'],
                            'user_foto' => $user['foto'],
                            'autor' => false
                        ];
                    }
                }
            }
        } else {
            $comments = "";
        };
       
        
        
        return $comments;
    }

    

    public function addComment(Request $request) {
        $data = json_decode($request->data);
        $date =  explode(' ', date("Y-m-d H:i:s"));
        $cur_user = Sentinel::getUser();
        try {
            $comment = new Comment();
            $comment->project_id = $data->project;
            $comment->user_id = $cur_user->id;
            $comment->for_client = NULL;
            $comment->close = 1;
            $comment->date = $date[0];
            $comment->time = $date[1];
            $comment->text = $data->comment;
            $comment->save();

            $this->success = true;
        } catch (\Exception $e) {
            $this->success = false;
        }

        if ($this->success) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }
    
    public static function getCommentsByProjectId($id) {
        $count = DB::select('select count(`id`) as `count`
            from `comments`
            where `deleted_at` is null and
            `project_id` = '.$id);

        if ($count[0]->count > 0) {
            $comments_q = Comment::where('project_id', '=', $id)
            ->orderBy('date', 'desc')
            ->orderBy('time', 'desc')
            ->whereNULL('deleted_at')
            ->limit(3)
            ->get();

            foreach ($comments_q as $c) {
                $user = User::find($c->user_id);
                $cur_user = Sentinel::getUser();
                $comments[] = [
                    'text' => preg_replace("/&nbsp;/", " ", strip_tags($c->text)),
                    'date' => $c->date,
                    'time' => $c->time,
                    'id' => $c->id,
                    'for_client' => intval($c->for_client != NULL),
                    'user_name' => $user->first_name,
                    'user_surname' => $user->last_name,
                    'user_foto' => $user->foto,
                    'autor' => $user->id == $cur_user->id
                ];
            }
        } else {
            $comments = [];
        };
       
        // dd($comments);
        
        return $comments;
    }

    public function add(Request $request) {
        if ($request->ajax()) {
            $cur_user = Sentinel::getUser();
            $comment = new Comment();
            $comment->project_id = $request->project_id;
            $comment->user_id = $cur_user->id;
            $comment->text = $request->text;
            $comment->date = date("Y-m-d");
            $comment->time = date("H:i:s");

            if ($comment->save()) {
                return redirect()->back()->with('success', 'Коментарий успешно добавлен');
            } else {
                return redirect()->back()->with('error', 'Произошла ошибка при добавлении коментария');
            }
        }
    }

    public function edit(Request $request) {
        $data = json_decode($request->data);
        $coment = Comment::find($data->comment->id);
        $date = $coment->created_at;
        $coment->text = $data->comment->text;
        $coment->created_at = $date;
        if ($coment->save()) {
            $comments_q = Comment::where('project_id', '=', $data->project_id)
                ->orderBy('date', 'desc')
                ->orderBy('time', 'desc')
                ->whereNULL('deleted_at')
                ->get();

            if (count($comments_q) > 0) {
                foreach ($comments_q as $c) {
                    $user = User::find($c->user_id);
                    $cur_user = Sentinel::getUser();

                    if ($c->for_client != NULL) {
                        if ($user->id == $cur_user->id) {
                            $comments[] = [
                                'text' => htmlspecialchars_decode($c->text),
                                'date' => $c->date,
                                'time' => $c->time,
                                'id' => $c->id,
                                'for_client' => 1,
                                'user_name' => $user->first_name,
                                'user_surname' => $user->last_name,
                                'user_foto' => $user->foto,
                                'autor' => true
                            ];
                        } else {
                            $comments[] = [
                                'text' => htmlspecialchars_decode($c->text),
                                'date' => $c->date,
                                'time' => $c->time,
                                'id' => $c->id,
                                'for_client' => 1,
                                'user_name' => $user->first_name,
                                'user_surname' => $user->last_name,
                                'user_foto' => $user->foto,
                                'autor' => false
                            ];
                        }
                    } else {
                        if ($user['id'] == $cur_user->id) {
                            $comments[] = [
                                'text' => htmlspecialchars_decode($c->text),
                                'date' => $c->date,
                                'time' => $c->time,
                                'id' => $c->id,
                                'for_client' => 0,
                                'user_name' => $user->first_name,
                                'user_surname' => $user->last_name,
                                'user_foto' => $user->foto,
                                'autor' => true
                            ];
                        } else {
                            $comments[] = [
                                'text' => htmlspecialchars_decode($c->text),
                                'date' => $c->date,
                                'time' => $c->time,
                                'id' => $c->id,
                                'for_client' => 0,
                                'user_name' => $user['first_name'],
                                'user_surname' => $user['last_name'],
                                'user_foto' => $user['foto'],
                                'autor' => false
                            ];
                        }
                    }
                }
            } else {
                $comments = '';
            }

            return response()->json($comments);
        } else {
            return response()->json(['status' => 'error']);
        };
    }

    public function del($id) {
        $comment = Comment::find($id);
        $comment->deleted_at = date("Y-m-d H:i:s");

        if ($comment->save()) {
            $com = self::getCommentsByProjectId($comment->project_id);
            
            return response()->json(['status' => 'Ok', 'comments' => $com, 'count' => count($com)]);
        } else {
            return response()->json(['status' =>  'Error']);
        }
    }

    static public function getProjectCloseComment($pid) {
        $comment = Comment::where('project_id', '=', $pid)
            ->where('close', '=', 1)
            ->whereNull('deleted_at')
            ->first();

        return $comment;
    }

    public function getControlComments(Request $request) {
        $data = json_decode($request->data);

        $resp = DB::select("select `question`.`id`, `question`.`question`, `answer`.`answer`, 
            DATE_FORMAT(`answer`.`created_at`,'%d %m %Y') as `date`
            from `callback_work_questions` `question`
            left join `callback_work_answers` `answer` on `answer`.`question_id` = `question`.`id`
            where
            `answer`.`project_id` =".$data->id);

        return response()->json($resp);
    }
}
