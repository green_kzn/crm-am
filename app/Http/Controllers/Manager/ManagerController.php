<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use App\Http\Controllers\Manager\UserController as Users;
use App\User;
use App\Event;
use App\Project_task_contact;
use Cache;


class ManagerController extends Controller
{
    public function admin() {
        return view('Manager.dashboard');
    }

    public function getAllImplementer() {
        $imp = User::where('roles.slug', '=', 'implementer')
            ->leftJoin('role_users', 'role_users.user_id', '=', 'users.id')
            ->leftJoin('roles', 'role_users.role_id', '=', 'roles.id')
            ->select(['users.*'])
            ->get();
        //dd($imp);
        return response()->json($imp);
    }

    public function getAllImplementerForSelect() {
        $imp = User::where('roles.slug', '=', 'implementer')
            ->leftJoin('role_users', 'role_users.user_id', '=', 'users.id')
            ->leftJoin('roles', 'role_users.role_id', '=', 'roles.id')
            ->select(['users.*'])
            ->get();

        foreach ($imp as $c) {
            $imp_ar[] = [
                'value' => $c['id'],
                'label' => $c['first_name'].' '.$c['last_name']
            ];
        }

        return response()->json($imp_ar);
    }

    public function changeImp(Request $request) {
        $data = json_decode($request->data);
        if ($data->edit == 1) {
            $event = Event::find($data->taskId);
            $event->user_id = $data->newImp->value;
            if ($event->save()) {
                return response()->json(['status' => 'Ok']);
            } else {
                return response()->json(['status' => 'Error']);
            };
        } else {
            $contact = Project_task_contact::where('project_task_id', '=', $data->taskId)
                ->first();
            $contact->user_id = $data->newImp->value;
            if ($contact->save()) {
                return response()->json(['status' => 'Ok']);
            } else {
                return response()->json(['status' => 'Error']);
            };
        };
    }

    public function changeEventName(Request $request) {
        $data = json_decode($request->data);
        if ($data->edit == 1) {
            $event = Event::find($data->taskId);
            $event->title = $data->newName;
            if ($event->save()) {
                return response()->json(['status' => 'Ok']);
            } else {
                return response()->json(['status' => 'Error']);
            };
        } else {
//            $contact = Project_task_contact::where('project_task_id', '=', $data->taskId)
//                ->first();
//            $contact->user_id = $data->newImp->value;
//            if ($contact->save()) {
//                return response()->json(['status' => 'Ok']);
//            } else {
//                return response()->json(['status' => 'Error']);
//            };
        };
    }

    public function getFirstImp() {
        $first = User::where('roles.slug', '=', 'implementer')
            ->leftJoin('role_users', 'role_users.user_id', '=', 'users.id')
            ->leftJoin('roles', 'role_users.role_id', '=', 'roles.id')
            ->select(['users.*'])
            ->first();

        return response()->json($first);
    }
}
