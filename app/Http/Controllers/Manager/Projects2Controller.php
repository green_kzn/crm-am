<?php

namespace App\Http\Controllers\Manager;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Manager\CommentController as Comments;
use App\Http\Controllers\Manager\UserController as Users2;
use App\Http\Controllers\Manager\ClientsController as Clients;
use App\Http\Controllers\Manager\ContactController as Contacts;
use App\Http\Controllers\Manager\CitysController as Citys; 
use App\Http\Controllers\Manager\PostsController as Posts;
use App\Http\Controllers\Manager\AMDateTimeController as AMDateTime;
use App\Http\Controllers\Manager\GroupTasksController as GroupTasks;
use App\Http\Controllers\Manager\ModulesController as Modules;
use App\Http\Controllers\Manager\DocumentsController as Documents;
use App\Project;
use App\Project_contact;
use App\Project_module;
use App\Project_task;
use App\Project_task_contact;
use App\Projects_doc;
use App\Contact;
use App\Comment;
use App\Posts_reference;
use App\User;
use App\Modules_reference;
use App\Module_rule;
use App\Tasksstatus_reference;
use App\Grouptasks_reference;
use App\Clients_reference;
use App\Clarifying_question;
use App\Clarifying_answer;
use App\Callback_close_question;
use App\Callback_work_question;
use App\Callback_work_answer;
use App\Callback_close_answer;
use App\Callback_other_task;
use App\Project_print_form;
use App\Check_list_reference;
use App\Check_list_answer;
use Sentinel;
use Event;
use App\Events\onAddProjectEvent;
use App\Events\ManagerCloseProject;
use App\Notifications\WorkoutAssigned;
use Notification;
use Auth;
use App\Notifications\ProjectTask;
use Carbon\Carbon;
use App\Events\ManagerCreateProject;

class Projects2Controller extends Controller
{
    public function searchProjects2(Request $request) {
        $data = json_decode($request->data);
        // dd($data);
        // $projects = DB::select("select `c`.`name` as `client_name`, `city`.`name` as `city`,
        // `u`.`first_name` as `first_name`, `u`.`last_name` as `last_name`, `p`.`*`
        // from `projects` `p`
        // left join `clients_references` `c` on `c`.`id` = `p`.`client_id`
        // left join `city_references` `city` on `city`.`id` = `c`.`city_id`
        // left join `users` `u` on `u`.`id` = `p`.`user_id`
        // where
        // `p`.`deleted_at` is null and
        // `p`.`name` like '%".$data->search."%'");
        // // dd($projects);

        // return response()->json($projects);
        $projects_q = DB::select("select *
        from `projects`
        where
        `deleted_at` is null and
        `name` like '%".$data->search."%'");

        $cur_user = Sentinel::getUser();

        if (count($projects_q) > 0) {
            foreach ($projects_q as $p) {
                $task = Project_task::where('project_id', '=', $p->id)
                    ->where('observer_id', '=', $cur_user->id)
                    ->whereNull('deleted_at')
                    ->get();

                if (count($task) > 0) {
                    foreach ($task as $t) {
                        if ($t->tasksstatus_ref_id == 2) {
                            $done2 = 0;
                        } else {
                            $done2 = 0;
                        }
                    }
                }

                $client = Clients::getClientById($p->client_id);
                $city = Citys::getCityById($client->city_id);

                if ($p->raw == 1) {
                    $projects[] = [
                        'client_name' => $client['name'],
                        'name' => $p->name,
                        'raw' => 1,
                        'id' => $p->id,
                        'prev_contact' => $p->prev_contact,
                        'next_contact' => $p->next_contact,
                        'active' => $p->active,
                        'city' => $city,
                        'imp' => Users2::getUserById($p->user_id),
                        'close' => $p->done,
                        'done' => 0
                    ];
                } else {
                    $projects[] = [
                        'client_name' => $client['name'],
                        'name' => $p->name,
                        'id' => $p->id,
                        'raw' => 0,
                        'prev_contact' => $p->prev_contact,
                        'next_contact' => $p->next_contact,
                        'start_date' => $p->start_date,
                        'finish_date' => $p->finish_date,
                        'active' => $p->active,
                        'city' => $city,
                        'imp' => Users2::getUserById($p->user_id),
                        'close' => $p->done,
                        'done' => 0

                    ];
                }
            }
        } else {
            $projects = '';
        }

        
        return response()->json([
            'projects' => $projects
        ]);
    }

    public function index() {
        if (session('perm')['project.view']) {
            $cur_user = Sentinel::getUser();
            $projects_q = Project::where('observer_id', '=', $cur_user->id)
                ->whereNULL('deleted_at')
                //->whereNULL('callback_status')
                ->get();

            if (count($projects_q) > 0) {
                foreach ($projects_q as $p) {
                    $task = Project_task::where('project_id', '=', $p['id'])
                        // ->where('observer_id', '=', $cur_user->id)
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($task) > 0) {
                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                //$done2[$p['id']][] = $t->id;
                                $done2 = 0;
                            } else {
                                $done2 = 0;
                            }
                        }
                    }

                    $client = Clients::getClientById($p['client_id']);
                    $city = Citys::getCityById($client->city_id);

                    if ($p['raw'] == 1) {
                        $projects[] = [
                            'client_name' => $client['name'],
                            'name' => $p['name'],
                            'raw' => 1,
                            'id' => $p['id'],
                            'prev_contact' => $p['prev_contact'],
                            'next_contact' => $p['next_contact'],
                            'active' => $p['active'],
                            'city' => $city,
                            'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            //'done' => 0
                        ];
                    } else {
                        $projects[] = [
                            'client_name' => $client['name'],
                            'name' => $p['name'],
                            'id' => $p['id'],
                            'raw' => 0,
                            'prev_contact' => $p['prev_contact'],
                            'next_contact' => $p['next_contact'],
                            'start_date' => $p->start_date,
                            'finish_date' => $p->finish_date,
                            'active' => $p['active'],
                            'city' => $city,
                            'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                        ];
                    }
                }
            } else {
                $projects = '';
            }
            //dd($projects);

            return view('Manager.projects.projects', [
                'projects' => $projects,
            ]);
        } else {
            abort(503);
        }
    }

    public function getPTask($id, Request $request) {
        $project_tasks = DB::select('select `pt`.`id`, `mr`.`task` as `name`, `m`.`name` as `m_name`, `gr`.`name` as `g_task`, 
            `mr`.`additional` as `additional`, `tr`.`name` as `status_name`, `pt`.`tasksstatus_ref_id` as `status`,
            `ptc`.`callbackstatus_ref_id`, `ptc`.`next_contact`, `gr`.`is_print_form`, `gr`.`is_screen_form`
            from `project_tasks` `pt`
            left join `module_rules` `mr` on `pt`.`module_rule_id` = `mr`.`id`
            left join `modules_references` `m` on `m`.`id` = `mr`.`module_id`
            left join `grouptasks_references` `gr` on `gr`.`id` = `mr`.`grouptasks_id`
            left join `tasksstatus_references` `tr` on `tr`.`id` = `pt`.`tasksstatus_ref_id`
            left join `project_task_contacts` `ptc` on `ptc`.`project_task_id` = `pt`.`id`
            where
            `pt`.`deleted_at` is null and
            `pt`.`project_id` = '.$id);

        // ------------------------ Даты для планирования задач ----------------------------
        $cur_date = date("j") . '.' . date("n") . '.' . date("Y");
        $cur_date_val = date("Y") . '-' . date("m") . '-' . date("d");
        $tomorrow = date('Y-m-d', strtotime($cur_date . ' +1 day'));
        $tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +1 day'));
        $plus_one_day = date('Y-m-d', strtotime($cur_date . ' +2 day'));
        $plus_one_day_val = date('Y-m-d', strtotime($cur_date . ' +2 day'));
        $plus_two_day = date('d.m.Y', strtotime($cur_date . ' +3 day'));
        $plus_two_day_val = date('Y-m-d', strtotime($cur_date . ' +3 day'));
        $plus_three_day = date('d.m.Y', strtotime($cur_date . ' +4 day'));
        $plus_three_day_val = date('Y-m-d', strtotime($cur_date . ' +4 day'));
        // --------------------------------------------------------------------------------

        // ---------------------------------- Нормы ---------------------------------------
        $count_norm = 0;
        $count_number = 0;
        $count_print_norm = 0;
        $count_print_number = 0;
        $count_screen_norm = 0;
        $count_screen_number = 0;

        
        // --------------------------------------------------------------------------------


        return response()->json(['project_tasks' => $project_tasks,
            'cur_date' => $cur_date,
            'cur_date_val' => $cur_date_val,
            'tomorrow' => $tomorrow,
            'tomorrow_val' => $tomorrow_val,
            'plus_one_day' => $plus_one_day,
            'plus_one_day_val' => $plus_one_day_val,
            'plus_two_day' => $plus_two_day,
            'plus_two_day_val' => $plus_two_day_val,
            'plus_three_day' => $plus_three_day,
            'plus_three_day_val' => $plus_three_day_val,
            'count_norm' => $count_norm / 2,
            'count_number' => $count_number / 2,
            'count_print_norm' => $count_print_norm,
            'count_print_number' => $count_print_number,
            'count_screen_norm' => $count_screen_norm,
            'count_screen_number' => $count_screen_number,
            // 'count_fact' => count($count_fact),
            // 'remained' => $remained,
        ]);
    }

    public function getPCard($id, Request $request) {
        $project = Project::find($id);

        // ------------------------ Дата начала и окончания проекта ----------------------
        $project_start_date = Carbon::parse($project->start_date)->format('d.m.Y');
        $project_finish_date = Carbon::parse($project->finish_date)->format('d.m.Y');
        // -------------------------------------------------------------------------------

        // ----------------------- Купленные модули --------------------------------------
        $modules = DB::select('select `pm`.`id`, `m`.`name`, `pm`.`num`, `m`.`id` as `mid`
            from `project_modules` `pm`
            left join `modules_references` `m` on `m`.`id` = `pm`.`module_id` 
            where
            `pm`.`deleted_at` is null and
            `pm`.`project_id` ='.$id);
        // -------------------------------------------------------------------------------

        // ----------------------------- Чек лист ----------------------------------------
        $check = DB::select('select `cla`.`id`, `cl`.`question`, `cla`.`answer`
            from `check_list_references` `cl`
            left join `check_list_answers` `cla` on `cla`.`check_list_ref_id` = `cl`.`id`
            where
            `cl`.`deleted_at` is null and
            `cla`.`project_id` ='.$id);
        // -------------------------------------------------------------------------------

        // ------------------------- Уточняющие вопросы ----------------------------------
        $cq = DB::select('select `mr`.`task`, `m`.`id` as `mid`, `mr`.`id` as `mrid`, 
            `cq`.`question` as `question`, `ca`.`answer` as `answer`,
            `cq`.`id` as `gid`, `ca`.`id` as `aid`
            from `project_modules` `pm`
            left join `modules_references` `m` on `m`.`id` = `pm`.`module_id`
            left join `module_rules` `mr` on `m`.`id` = `mr`.`module_id`
            left join `clarifying_questions` `cq` on `cq`.`task_id` =  `mr`.`id`
            left join `clarifying_answers` `ca` on (`ca`.`question_id` = `cq`.`id` and `ca`.`question_id` is not null and `ca`.`deleted_at` is null and `ca`.`project_id` = '.$id.')
            where
            `mr`.`additional` is null and
            `mr`.`deleted_at` is null and
            `pm`.`deleted_at` is null and
            `cq`.`id` is not null and
            `pm`.`project_id` = '.$id);
        // -------------------------------------------------------------------------------

        return response()->json([
            'project_start_date' => $project_start_date,
            'project_finish_date' => $project_finish_date,
            'modules_buy' => $modules,
            'check' => $check,
            'questions' => $cq
        ]);
    }

    public function editQuestions(Request $request) {
        $data = json_decode($request->data);
        $res = false;

        foreach($data->question as $q) {
            if ($q->aid != '') {
                $answer = Clarifying_answer::find($q->aid);
                $answer->question_id = $q->gid;
                $answer->project_id = $data->project_id;
                if ($q->answer != '') {
                    $answer->answer = $q->answer;
                } else {
                    $answer->answer = '';
                }
            } else {
                $answer = new Clarifying_answer();
                $answer->question_id = $q->gid;
                $answer->project_id = $data->project_id;
                if ($q->answer != '') {
                    $answer->answer = $q->answer;
                } else {
                    $answer->answer = '';
                }
            };

            if ($answer->save()) {
                $res = true;
            } else {
                $res = false;
            }
        }


        if ($res == true) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        };
    }

    public function changeImp(Request $request) {
        $data = json_decode($request->data);
//        print_r($data);

        DB::transaction(function () use ($data) {
            try {
                $project = Project::find($data->pid);
                $project->user_id = $data->imp;
                $project->save();

            } catch (\Exception $e) {
                DB::rollback();
            }

            try {
                $project_task = Project_task::whereProject_id($data->pid)->update(['user_id' => $data->imp]);
//                print_r($project_task);

            } catch (\Exception $e) {
                DB::rollback();
            }

            try {
                $project_task_contact = Project_task_contact::whereProject_id($data->pid)->update(['user_id' => $data->imp]);

            } catch (\Exception $e) {
                DB::rollback();
            }
        });

        return response()->json(['status' => 'Ok']);
    }

    public function edit($id) {
        if (session('perm')['project.update']) {
            $project = Project::find($id);
            $project_start_date = Carbon::parse($project->start_date)->format('d.m.Y');
            $project_finish_date = Carbon::parse($project->finish_date)->format('d.m.Y');

            if ($project) {
                $cur_date = date("j").'.'.date("n").'.'.date("Y");
                $cur_date_val = date("Y").'-'.date("m").'-'.date("d");
                $tomorrow = date('Y-m-d', strtotime($cur_date . ' +1 day'));
                $tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +1 day'));
                $plus_one_day = date('Y-m-d', strtotime($cur_date . ' +2 day'));
                $plus_one_day_val = date('Y-m-d', strtotime($cur_date . ' +2 day'));
                $plus_two_day = date('d.m.Y', strtotime($cur_date . ' +3 day'));
                $plus_two_day_val = date('Y-m-d', strtotime($cur_date . ' +3 day'));
                $plus_three_day = date('d.m.Y', strtotime($cur_date . ' +4 day'));
                $plus_three_day_val = date('Y-m-d', strtotime($cur_date . ' +4 day'));

                $comments = Comments::getCommentsByProjectId($id);

                $contacts = Project_contact::where('project_id', '=', $id)
                    ->whereNULL('deleted_at')
                    ->orderBy('id', 'asc')
                    ->get();

                foreach($contacts as $c) {
                    $cont = Contact::where('id', '=', $c->contact_id)
                        ->whereNull('deleted_at')
                        ->first();

                    //dd($cont);
                    if (!is_null($cont)) {
                        $post = Posts_reference::find($cont->post_id);

                        if ($cont->post_id == 1) {
                            $ar_contact[] = [
                                'first_name' => $cont->first_name,
                                'last_name' => $cont->last_name,
                                'patronymic' => $cont->patronymic,
                                'phone' => $cont->phone,
                                'email' => $cont->email,
                                'post' => $post->name,
                                'id' => $cont->id,
                                'director' => 1,
                                'main' => $c->main,
                            ];
                        } else {
                            $ar_contact[] = [
                                'first_name' => $cont->first_name,
                                'last_name' => $cont->last_name,
                                'patronymic' => $cont->patronymic,
                                'phone' => $cont->phone,
                                'email' => $cont->email,
                                'post' => $post['name'],
                                'id' => $cont->id,
                                'director' => 0,
                                'main' => $c->main,
                            ];
                        }
                    } else {
                        $ar_contact[] = '';
                    }

                }

                $modules = Project_module::where('project_id', '=', $id)
                    ->whereNull('deleted_at')
                    ->get();

                foreach ($modules as $m) {
                    $name = Modules_reference::find($m->module_id);

                    $all_modules[] = [
                        'id' => $name['id'],
                        'name' => $name['name'],
                        'num' => $m->num
                    ];
                }

                foreach ($all_modules as $all) {
                    $questions_q = Module_rule::where('module_id', '=', $all['id'])
                        ->whereNull('deleted_at')
                        ->get();

                    foreach ($questions_q as $q) {
                        if (count($q->questions) > 0) {
                            $questions_ar[] = [
                                'm_id' => $all['id'],
                                'id' => $q->id,
                                'questions' => $q->questions
                            ];
                        } else {
                            $questions_ar = [];
                        }
                    }
                }

                $cur_user = Sentinel::getUser();
                $count_norm = 0;
                $count_number = 0;
                $count_print_norm = 0;
                $count_print_number = 0;
                $count_screen_norm = 0;
                $count_screen_number = 0;

                $project_tasks = Project_task::where('project_id', '=', $id)
                    ->whereNull('deleted_at')
                    //->whereNull('status')
                    //->whereNull('additional_id')
                    ->get();

                if (count($project_tasks) > 0) {
                    $observer_id = $project_tasks[0]['observer_id'];
                    $print_form_id = Grouptasks_reference::select('id')
                        ->where('is_print_form', '=', 1)
                        ->get();

                    $screen_form_id = Grouptasks_reference::select('id')
                        ->where('is_screen_form', '=', 1)
                        ->get();
                    foreach ($project_tasks as $pt) {
                        $name = Module_rule::where('id', '=', $pt->module_rule_id)
                            ->whereNull('deleted_at')
                            ->first();
                        $test[] = array($name['grouptasks_id']);
                        $ptc = Project_task_contact::where('project_task_id', '=', $pt->id)
                            ->whereNull('deleted_at')
                            ->first();
                        $p_name = Project::find($pt->project_id);
                        $m_name = Modules_reference::find($pt->module_id);
                        $g_task = Grouptasks_reference::find($name['grouptasks_id']);
                        //dd($pt->module_id);

                        if ($g_task['is_print_form'] == 1) {
                            $type = 'print_form';
                        } elseif ($g_task['is_screen_form'] == 1) {
                            $type = 'screen_form';
                        } else {
                            $type = 'task';
                        }

                        if (count($ptc) > 0) {
                            $p_tasks[] = [
                                'id' => $pt->id,
                                'name' => $name['task'],
                                'p_name' => $p_name->name,
                                'observer_id' => $pt->observer_id,
                                'city' => $p_name->city,
                                'm_name' => $m_name->name,
                                'callbackstatus_ref_id' => $ptc['callbackstatus_ref_id'],
                                'g_task' => $g_task['name'],
                                'type' => $type,
                                'additional' => $name['additional'],
                                'status' => $pt->tasksstatus_ref_id,
                                'next_contact' => $ptc['next_contact']
                            ];
                        } else {
                            $p_tasks[] = [
                                'id' => $pt->id,
                                'name' => $name['task'],
                                'p_name' => $p_name->name,
                                'observer_id' => $pt->observer_id,
                                'city' => $p_name->city,
                                'm_name' => $m_name->name,
                                'callbackstatus_ref_id' => $ptc['callbackstatus_ref_id'],
                                'g_task' => $g_task['name'],
                                'type' => $type,
                                'additional' => $name['additional'],
                                'status' => $pt->tasksstatus_ref_id,
                                'next_contact' => null
                            ];
                        }

                        foreach ($print_form_id as $pfid) {
                            if($name['grouptasks_id'] != $pfid->id) {
                                $count_norm += $pt->norm_contacts;
                                $count_number += $pt->number_contacts;
                            } else {
                                $count_print_norm += $pt->norm_contacts;
                                $count_print_number += $pt->number_contacts;
                            }
                        }

                        foreach ($screen_form_id as $sfid) {
                            if($name['grouptasks_id'] != $sfid->id) {
                                $count_norm += $pt->norm_contacts;
                                $count_number += $pt->number_contacts;
                            } else {
                                $count_screen_norm += $pt->norm_contacts;
                                $count_screen_number += $pt->number_contacts;
                            }
                        }
                    }
                } else {
                    $observer_id = '';
                    $p_tasks = '';
                }

                $project_form = Project_print_form::where('project_id', '=', $id)
                    ->whereNull('deleted_at')
                    ->get();

                if (count($project_form) > 0) {
                    foreach ($project_form as $pf) {
                        $name = Module_rule::where('id', '=', $pf->module_rule_id)
                            ->whereNull('deleted_at')
                            ->first();
                        $ptc = Project_task_contact::where('project_task_id', '=', $pf->id)
                            ->whereNull('deleted_at')
                            ->first();
                        $p_name = Project::find($pf->project_id);
                        $m_name = Modules_reference::find($pf->module_id);
                        $g_task = Grouptasks_reference::find(11);

                        $q_ar[] = '';

                        if (count($ptc) > 0) {
                            $p_form[] = [
                                'id' => $pf->id,
                                'name' => $pf->name,
                                'p_name' => $p_name->name,
                                'observer_id' => $pf->observer_id,
                                'city' => $p_name->city,
                                'm_name' => $m_name->name,
                                'callbackstatus_ref_id' => $ptc['callbackstatus_ref_id'],
                                'g_task' => $g_task['name'],
                                'additional' => $name['additional'],
                                'status' => $pf->tasksstatus_ref_id,
                                'next_contact' => $ptc['next_contact']
                            ];
                        } else {
                            $p_form[] = [
                                'id' => $pf->id,
                                'name' => $pf->name,
                                'p_name' => $p_name->name,
                                'observer_id' => $pf->observer_id,
                                'city' => $p_name->city,
                                'm_name' => $m_name['name'],
                                'callbackstatus_ref_id' => $ptc['callbackstatus_ref_id'],
                                'g_task' => $g_task['name'],
                                'additional' => $name['additional'],
                                'status' => $pf->tasksstatus_ref_id,
                                'next_contact' => null
                            ];
                        }

                    }
                } else {
                    $p_form = '';
                }

                $count_fact = Project_task_contact::where('project_id', '=', $project->id)
                    ->whereNull('deleted_at')
                    ->whereNotNull('duration')
                    ->get();

                $remained = 0;
                foreach ($count_fact as $cf) {
                    switch ($cf->duration) {
                        case '00:15:00':
                            $remained += 0.5;
                            break;
                        case '00:30:00':
                            $remained += 1;
                            break;
                        case '00:45:00':
                            $remained += 1;
                            break;
                        case '01:00:00':
                            $remained += 1;
                            break;
                        case '01:30:00':
                            $remained += 1.5;
                            break;
                    }
                }
                //dd($remained);

                $project_tasks2 = Project_task::where('project_id', '=', $id)
                    ->whereNull('deleted_at')
                    ->get();

                if (count($project_tasks2) > 0) {
                    foreach ($project_tasks2 as $pt) {
                        $name = Module_rule::where('id', '=', $pt->module_rule_id)
                            ->whereNull('deleted_at')
                            ->first();

                        if ($name['additional'] == null) {
                            $c_question = Clarifying_question::where('task_id', '=', $name['id'])
                                ->whereNull('deleted_at')
                                ->first();

                            $q_ar = [
                                'id' => $c_question['id'],
                                'question' => $c_question['question']
                            ];
                            $q_ar2[] = $q_ar;
                        }
                    }
                } else {
                    $q_ar2 = '';
                }

                if($q_ar2 != '') {
                    foreach ($q_ar2 as $cq) {
                        $c_answer = Clarifying_answer::where('question_id', '=', $cq['id'])
                            ->where('project_id', '=', $id)
                            ->whereNull('deleted_at')
                            ->first();

                        $question[] = [
                            'id' => $cq['id'],
                            'question' => $cq['question'],
                            'answer' => $c_answer['answer']
                        ];

                    }
                } else {
                    $question = [];
                }

                if (count($question) > 0) {
                    foreach ($question as $q) {
                        $c_answer = Clarifying_answer::where('question_id', '=', $q['id'])
                            ->where('project_id', '=', $id)
                            ->whereNull('deleted_at')
                            ->first();

                        //dd($c_answer['answer']);

                        if ($c_answer['answer'] != 'null') {
                            $answer[] = [
                                'q_id' => $q['id'],
                                'answer' => $c_answer['answer']
                            ];
                        } else {
                            $answer = [];
                        }
                    }
                } else {
                    $answer = [];
                }

                //dd($project_tasks2);

                $user = User::whereNull('deleted_at')->get();
                foreach ($user as $u) {
                    if ($u->id == $project->user_id) {
                        $observer[] = [
                            'default' => 1,
                            'id' => $u->id,
                            'first_name' => $u->first_name,
                            'last_name' => $u->last_name
                        ];
                    } else {
                        $observer[] = [
                            'default' => 0,
                            'id' => $u->id,
                            'first_name' => $u->first_name,
                            'last_name' => $u->last_name
                        ];
                    }
                }

                $doc_types = Documents::getAllDocumentsType();
                $doc_input = Documents::getProjectDocsByType(1, $id);
                $doc_output = Documents::getProjectDocsByType(2, $id);

                //dd($p_tasks);
                $client = Clients_reference::find($project->client_id);

                $check_list_answer = Check_list_answer::where('project_id', '=', $id)
                    ->whereNull('deleted_at')
                    ->get();
                foreach ($check_list_answer as $check_answer) {
                    $check_list_question = Check_list_reference::where('id', '=',$check_answer->check_list_ref_id)
                        ->whereNull('deleted_at')
                        ->first();

                    $check_list[] = [
                        'id' => $check_list_question['id'],
                        'question' => strip_tags($check_list_question['question']),
                        'answer' => $check_answer->answer
                    ];
                }

                return view('Manager.projects.edit', [
                    'project' => $project,
                    'project_start_date' => $project_start_date,
                    'project_finish_date' => $project_finish_date,
                    'observer_id' => $observer_id,
                    'comments' => $comments,
                    'contacts' => $ar_contact,
                    'modules' => $all_modules,
                    'all_modules' => Modules::getModulesByProjectId($project->id),
                    'project_task' => $p_tasks,
                    'project_form' => $p_form,
                    'group_task' => GroupTasks::getAllTasks(),
                    'all_group_task' => GroupTasks::getAllTaskGroup(),
                    'observer' => $observer,
                    'cur_date' => $cur_date,
                    'cur_date_val' => $cur_date_val,
                    'tomorrow' => $tomorrow,
                    'tomorrow_val' => $tomorrow_val,
                    'plus_one_day' => $plus_one_day,
                    'plus_one_day_val' => $plus_one_day_val,
                    'plus_two_day' => $plus_two_day,
                    'plus_two_day_val' => $plus_two_day_val,
                    'plus_three_day' => $plus_three_day,
                    'plus_three_day_val' => $plus_three_day_val,
                    'client' => $client,
                    'questions' => $questions_ar,
                    'question' => $question,
                    'answer' => $answer,
                    'count_norm' => $count_norm/2,
                    'count_number' => $count_number/2,
                    'count_print_norm' => $count_print_norm,
                    'count_print_number' => $count_print_number,
                    'count_screen_norm' => $count_screen_norm,
                    'count_screen_number' => $count_screen_number,
                    'count_fact' => count($count_fact),
                    'remained' => $remained,
                    'doc_types' => $doc_types,
                    'doc_input' => $doc_input,
                    'doc_output' => $doc_output,
                    'check_list' => $check_list
                ]);
            } else {
                abort(404);
            }
        } else {
            abort(503);
        }
    }

    public function close($id) {
        $project = Project::find($id);
        $client = Clients_reference::find($project->client_id);

        return view('Manager.projects.close', [
            'project' => $project,
            'client' => $client
        ]);
    }

    public function postClose(Request $request, $id) {
        DB::transaction(function () use ($request, $id) {
            $date =  explode(' ', date("Y-m-d H:i:s"));
            $cur_user = Sentinel::getUser();
            try {
                $comment = new Comment();
                $comment->project_id = $id;
                $comment->user_id = $cur_user->id;
                $comment->for_client = NULL;
                $comment->close = 1;
                $comment->date = $date[0];
                $comment->time = $date[1];
                $comment->text = $request->comment;
                $comment->save();

                $this->success = true;
            } catch (\Exception $e) {
                $this->success = false;
            }

            try {
                $project = Project::find($id);
                $project->callback_status = 6;
                $project->done_datetime = date("Y-m-d H:i:s");
                $project->save();

                $this->success = true;
            } catch (\Exception $e) {
                $this->success = false;
            }
        });

        if ($this->success) {
            return redirect('/manager/projects')->with('success', 'Данные успешно сохранены');
        } else {
            return redirect()->back()->with('error', 'Произошла ошибка при сохранении данных');
        }
    }

    public function add() {
        if (session('perm')['project.create']) {
            $users = Users2::getAllUsers();
            $modules = Modules_reference::whereNULL('deleted_at')->get();
            $posts = Posts_reference::whereNull('deleted_at')->get();
            $cur_user = Sentinel::getUser();

            $documents = Documents::getDocumentsForAllProjects();

            $check_list = Check_list_reference::whereNull('deleted_at')
                ->get();
            if (count($check_list) > 0) {
                foreach ($check_list as $cl) {
                    $check_question[] = array(
                        'id' => $cl->id,
                        'question' => strip_tags($cl->question)
                    );
                }
            } else {
                $check_question[] = array();
            }

            return view('Manager.projects.add', [
                'users' => $users,
                'cur_user' => $cur_user,
                'modules' => $modules,
                'posts' => $posts,
                'documents' => $documents,
                'clients' => Clients::getAllClients(),
                'check_question' => $check_question
            ]);
        } else {
            abort(503);
        }
    }

    public function getCheckList() {
        $check_list = Check_list_reference::whereNull('deleted_at')
            ->get();
        if (count($check_list) > 0) {
            foreach ($check_list as $cl) {
                $check_question[] = array(
                    'id' => $cl->id,
                    'question' => strip_tags($cl->question),
                    'answer' => ''
                );
            }
        } else {
            $check_question[] = array();
        }

        return response()->json($check_question);
    }

    public function create(Request $request) {
        $data = json_decode($request->data);
        $cur_user = Sentinel::getUser();
        // dd($data->client);
        // DB::transaction(function () use ($data, $cur_user) {
            
        // });

        $project = new Project();
        $project->name = $data->project_name;
        $project->raw = 1;
        $project->done = 0;
        $project->active = 1;
        $project->transport_company = $data->send_docs->company;
        $project->letter_id = $data->send_docs->number;
        $project->observer_id = $cur_user->id;
        $project->client_id = $data->client;
        if ($project->save()) {
            $success = true;
        } else {
            $success = false;
        };

             
        foreach ($data->documents as $doc) {
            if ($doc->isFirst == 1) {
                $type = Documents::getDocTypeByName($doc->type);
                $doc_name = Documents::getDocumentByName($doc->name);
                // dd($type);
                $project_doc = new Projects_doc();
                $project_doc->project_id = $project->id;
                $project_doc->doc_id = $doc_name->id;
                $project_doc->type_id = $type->id;
                if ($project_doc->save()) {
                    $success = true;
                } else {
                    $success = false;
                };
            }
        }
        

        foreach ($data->checklist as $cl) {
            $answer = new Check_list_answer();
            $answer->check_list_ref_id = $cl->id;
            $answer->answer = $cl->answer;
            $answer->project_id = $project->id;
            if ($answer->save()) {
                $success = true;
            } else {
                $success = false;
            };
        }

        
        foreach ($data->project_contact as $c) {
            //$contact = Contact::find($c->id);
            $p_contact = new Project_contact();
            $p_contact->contact_id = $c->id;
            $p_contact->project_id = $project->id;
            if (isset($c->main) && $c->main == 'null') {
                $p_contact->main = 1;
            } else {
                $p_contact->main = 0;
            }
            if (isset($c->mail_rep) && $c->mail_rep == 'null') {
                $p_contact->mail_rep = 1;
            } else {
                $p_contact->mail_rep = 0;
            }
            if ($p_contact->save()) {
                $success = true;
            } else {
                $success = false;
            };
        }

        foreach ($data->modules as $m) {
            $modules = new Project_module();
            $modules->module_id = (int)$m->id;
            $modules->project_id = $project->id;
            $modules->num = (int)$m->count;
            if ($modules->save()) {
                $success = true;
            } else {
                $success = false;
            };

            $m_rule = Module_rule::where('module_id', '=', (int)$m->id)
                ->whereNull('deleted_at')
                ->whereNull('additional')
                ->get();

            foreach ($m_rule as $mr) {
                $p_task = new Project_task();
                $p_task->project_id = $project->id;
                $p_task->user_id = NULL;
                $p_task->observer_id = $cur_user->id;
                $p_task->module_rule_id = $mr->id;
                $p_task->norm_contacts = $mr->norm_contacts;
                $p_task->number_contacts = $mr->number_contacts;
                $p_task->module_id = $m->id;
                $p_task->tasksstatus_ref_id = 1;
                if ($p_task->save()) {
                    $success = true;
                } else {
                    $success = false;
                };
            }
        }
          
        event(new ManagerCreateProject($project->name, $cur_user->first_name.' '.$cur_user->last_name));
        if ($success) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function getProjects() {
        // dd('Ok');
        $cur_user = Sentinel::getUser();

        if (isset($_GET['count'])) $per_page = (int)$_GET['count']; else $per_page = 10;

        if (isset($_GET['page'])) $page = ($_GET['page']-1); else $page=0;
        // dd('Ok');
        if ($per_page == 0) {
            $count = DB::select('select count(`id`) as `count`
                    from `projects`
                    where
                    `deleted_at` is null');
    
            $projects_q = DB::select('select *
                from `projects`
                where
                `deleted_at` is null');

            $num_pages = 1;
        } else {
            $start = abs($page*$per_page);

            $count = DB::select('select count(`id`) as `count`
                from `projects`
                where
                `deleted_at` is null');
    
                    
            $projects_q = DB::select('select *
                from `projects`
                where
                `deleted_at` is null
                limit '.$start.','.$per_page);

            $num_pages = ceil($count[0]->count/$per_page);
        }

        
        if (count($projects_q) > 0) {
            foreach ($projects_q as $p) {
                $task = Project_task::where('project_id', '=', $p->id)
                    ->where('observer_id', '=', $cur_user->id)
                    ->whereNull('deleted_at')
                    ->get();

                if (count($task) > 0) {
                    foreach ($task as $t) {
                        if ($t->tasksstatus_ref_id == 2) {
                            $done2 = 0;
                        } else {
                            $done2 = 0;
                        }
                    }
                }

                $client = Clients::getClientById($p->client_id);
                $city = Citys::getCityById($client->city_id);

                if ($p->raw == 1) {
                    $projects[] = [
                        'client_name' => $client['name'],
                        'name' => $p->name,
                        'raw' => 1,
                        'id' => $p->id,
                        'prev_contact' => $p->prev_contact,
                        'next_contact' => $p->next_contact,
                        'active' => $p->active,
                        'city' => $city,
                        'imp' => Users2::getUserById($p->user_id),
                        'close' => $p->done,
                        'done' => 0
                    ];
                } else {
                    $projects[] = [
                        'client_name' => $client['name'],
                        'name' => $p->name,
                        'id' => $p->id,
                        'raw' => 0,
                        'prev_contact' => $p->prev_contact,
                        'next_contact' => $p->next_contact,
                        'start_date' => $p->start_date,
                        'finish_date' => $p->finish_date,
                        'active' => $p->active,
                        'city' => $city,
                        'imp' => Users2::getUserById($p->user_id),
                        'close' => $p->done,
                        'done' => 0

                    ];
                }
            }
        } else {
            $projects = '';
        }

        
        return response()->json([
            'projects' => $projects,
            'count' => $count[0]->count, 
            'count_pages' => $num_pages,
        ]);
    }

    public function getProject($id) {
        $project = Project::find($id);
        $cur_user = Sentinel::getUser();

        // ----------------------------- Контакты проекта ---------------------------------
        $contacts = DB::select('select `c`.`id` as `id`, `pc`.`main`, `pc`.`mail_rep`, `c`.`first_name`, 
            `c`.`last_name`, `c`.`patronymic`, `pr`.`name` as `post_name`, `pr`.`id` as `post_id`, `c`.`phone`, `c`.`email`
            from `project_contacts` `pc`
            left join `contacts` `c` on `pc`.`contact_id` = `c`.`id`
            left join `posts_references` `pr` on `pr`.`id` = `c`.`post_id`
            where
            `pc`.`deleted_at` is null and
            `c`.`deleted_at` is null and
            `pc`.`project_id` = '.$id);
        // ---------------------------------------------------------------------------------

        return response()->json([
            'head' => $cur_user->head,
            'project' => $project,
            'contacts' => $contacts,
        ]);
    }

    public function getProjectsCount() {
        $cur_user = Sentinel::getUser();
        $projects_q = Project::where('observer_id', '=', $cur_user->id)
            ->whereNULL('deleted_at')
            //->whereNULL('callback_status')
            ->count();

        return response()->json($projects_q);
    }

    public function postAdd(Request $request) {
        dd($request->all());
        if (session('perm')['project.create']) {
            if (!$request->projectName) {
                return redirect()->back()->with('error', 'Укажите название проекта');
            } else {
                $count_project = Project::where('name', '=', $request->projectName)
                    ->whereNull('deleted_at')
                    ->count();
                if ($count_project > 0) {
                    return redirect()->back()->with('error', 'Проект с данным названием уже существует');
                }
            }
            if ($request->optionsRadios != "on") {
                return redirect()->back()->with('error', 'Отсутствует информация об отправке документов');
            }
            if ($request->row_modules == '[]') {
                return redirect()->back()->with('error', 'Выберите модули');
            }
            $cur_user = Sentinel::getUser();
            $contacts = json_decode($request->contacts);
            $modules = json_decode($request->row_modules);
            $module_rule = json_decode($request->module_rule);
            $documents = json_decode($request->documents);
            $check_list = json_decode($request->check_list);
            //dd($module_rule);

            DB::transaction(function () use ($request, $contacts, $cur_user, $modules, $module_rule, $documents, $check_list) {
                try {
                    $this->project = new Project();
                    $this->project->name = $request->projectName;
                    $this->project->raw = 1;
                    $this->project->done = 0;
                    $this->project->active = 1;
                    if ($request->transComp != 'otherCompany') {
                        $this->project->transport_company = $request->transComp;
                    } else {
                        $this->project->transport_company = $request->companyName;
                    }
                    $this->project->letter_id = $request->MessageId;
                    $this->project->observer_id = $cur_user->id;
                    $this->project->client_id = $request->client;
                    $this->project->save();

                    /*$cu = User::find($request->user);
                    $cu->notify(new ProjectTask($this->project));*/
                    $this->success = true;
                } catch (\Exception $e) {
                    DB::rollback();
                    $this->success = false;
                }

                foreach ($documents as $doc) {
                    $is_first = Documents::isFirstDoc($doc->name);

                    if ($is_first) {
                        try {
                            $type = Documents::getDocTypeByName($doc->type);
                            $doc_name = Documents::getDocumentByName($doc->name);

                            $project_doc = new Projects_doc();
                            $project_doc->project_id = $this->project->id;
                            $project_doc->doc_id = $doc_name->id;
                            $project_doc->type_id = $type->id;
                            $project_doc->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            DB::rollback();
                            $this->success = false;
                        }
                    }
                }

                foreach ($check_list as $cl) {
                    try {
                        $answer = new Check_list_answer();
                        $answer->check_list_ref_id = $cl->id;
                        $answer->answer = $cl->answer;
                        $answer->project_id = $this->project->id;
                        $answer->save();

                        $this->success = true;
                    } catch (\Exception $e) {
                        DB::rollback();
                        $this->success = false;
                    }
                }

                foreach ($contacts as $c) {
                    if ($c->id != '') {
                        try {
                            $contact = Contact::find($c->id);
                            $p_contact = new Project_contact();
                            $p_contact->contact_id = $contact->id;
                            $p_contact->project_id = $this->project->id;
                            if ($c->main == 'true') {
                                $p_contact->main = 1;
                            } else {
                                $p_contact->main = 0;
                            }
                            if ($c->email_rep == 'true') {
                                $p_contact->mail_rep = 1;
                            } else {
                                $p_contact->mail_rep = 0;
                            }
                            $p_contact->save();
                            $this->success = true;
                        } catch (\Exception $e) {
                            DB::rollback();
                            $this->success = false;
                        }
                    } else {
                        try {
                            $post = Posts_reference::where('name', '=', $c->post)->firstOrFail();
                            $contact = new Contact();
                            $contact->first_name = $c->first_name;
                            $contact->last_name = $c->last_name;
                            $contact->patronymic = $c->patronymic;
                            $contact->post_id = $post->id;
                            $contact->phone = $c->phone;
                            $contact->email = $c->email;
                            $contact->client_id = $request->client;
                            $contact->user_id = $cur_user->id;
                            $contact->save();

                            $p_contact = new Project_contact();
                            $p_contact->contact_id = $contact->id;
                            $p_contact->project_id = $this->project->id;
                            if ($c->main == 'true') {
                                $p_contact->main = 1;
                            } else {
                                $p_contact->main = 0;
                            }
                            if ($c->email_rep == 'true') {
                                $p_contact->mail_rep = 1;
                            } else {
                                $p_contact->mail_rep = 0;
                            }
                            $p_contact->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            DB::rollback();
                            $this->success = false;
                        }
                    }
                }

                if (count($module_rule) > 0) {
                    //dd('yes');
                    foreach ($modules as $m) {
                        try {
                            $modules = new Project_module();
                            $modules->module_id = (int)$m->id;
                            $modules->project_id = $this->project->id;
                            $modules->num = (int)$m->num;
                            $modules->save();

                            foreach ($module_rule as $new_mr) {
                                $p_task = new Project_task();
                                $p_task->project_id = $this->project->id;
                                $p_task->user_id = NULL;
                                $p_task->observer_id = $cur_user->id;
                                $p_task->module_rule_id = $new_mr->id;
                                $p_task->norm_contacts = $new_mr->norm_contacts;
                                $p_task->number_contacts = $new_mr->number_contacts;
                                $p_task->module_id = (int)$m->id;
                                $p_task->tasksstatus_ref_id = 1;
                                $p_task->save();
                            }

                            $this->success = true;
                        } catch (\Exception $e) {
                            DB::rollback();
                            $this->success = false;
                        }
                    }
                } else {
                    //dd('no');
                    foreach ($modules as $m) {
                        try {
                            $modules = new Project_module();
                            $modules->module_id = (int)$m->id;
                            $modules->project_id = $this->project->id;
                            $modules->num = (int)$m->num;
                            $modules->save();

                            $m_rule = Module_rule::where('module_id', '=', (int)$m->id)
                                ->whereNull('deleted_at')
                                ->whereNull('additional')
                                ->get();

                            foreach ($m_rule as $mr) {
                                $p_task = new Project_task();
                                $p_task->project_id = $this->project->id;
                                $p_task->user_id = NULL;
                                $p_task->observer_id = $cur_user->id;
                                $p_task->module_rule_id = $mr->id;
                                $p_task->norm_contacts = $mr->norm_contacts;
                                $p_task->number_contacts = $mr->number_contacts;
                                $p_task->module_id = $m->id;
                                $p_task->tasksstatus_ref_id = 1;
                                $p_task->save();
                            }

                            $this->success = true;
                        } catch (\Exception $e) {
                            DB::rollback();
                            $this->success = false;
                        }
                    }
                }

                /*foreach ($modules as $m) {
                    try {
                        $modules = new Project_module();
                        $modules->module_id = (int)$m->id;
                        $modules->project_id = $this->project->id;
                        $modules->num = (int)$m->num;
                        $modules->save();

                        $m_rule = Module_rule::where('module_id', '=', (int)$m->id)
                            ->whereNull('deleted_at')
                            ->whereNull('additional')
                            ->get();

                        foreach ($m_rule as $mr) {
                            if ($mr->grouptasks_id != 11) {
                                $p_task = new Project_task();
                                $p_task->project_id = $this->project->id;
                                $p_task->user_id = NULL;
                                $p_task->observer_id = $cur_user->id;
                                $p_task->module_rule_id = $mr->id;
                                $p_task->norm_contacts = $mr->norm_contacts;
                                $p_task->number_contacts = $mr->number_contacts;
                                $p_task->module_id = $m->id;
                                $p_task->tasksstatus_ref_id = 1;
                                $p_task->save();
                            } else {
                                $number = Project_print_form::whereNull('deleted_at')
                                    ->max('number');

                                if (isset($number)) {
                                    $p_form = new Project_print_form();
                                    $p_form->project_id = $this->project->id;
                                    $p_form->number = $number + 1;
                                    $p_form->name = $mr->task;
                                    $p_form->user_id = $request->user;
                                    $p_form->observer_id = $cur_user->id;
                                    $p_form->module_rule_id = $mr->id;
                                    $p_form->norm_contacts = $mr->norm_contacts;
                                    $p_form->number_contacts = $mr->number_contacts;
                                    $p_form->module_id = $m->id;
                                    $p_form->printform_status_ref_id = 1;
                                    $p_form->save();

                                    $cu = User::find($request->user);
                                    $cu->notify(new ProjectTask($p_form));
                                } else {
                                    //dd($mr->number_contacts);
                                    $p_form_new = new Project_print_form();
                                    $p_form_new->project_id = $this->project->id;
                                    $p_form_new->number = 1;
                                    $p_form_new->name = $mr->task;
                                    $p_form_new->user_id = (int)$request->user;
                                    $p_form_new->observer_id = (int)$cur_user->id;
                                    $p_form_new->module_rule_id = (int)$mr->id;
                                    $p_form_new->norm_contacts = $mr->norm_contacts;
                                    $p_form_new->number_contacts = $mr->number_contacts;
                                    $p_form_new->module_id = (int)$m->id;
                                    $p_form_new->printform_status_ref_id = 1;
                                    $p_form_new->save();
                                }
                            }
                        }

                        foreach ($module_rule as $new_mr) {
                            dd($new_mr);
                            try {
                                $new_p_task = Project_task::where('project_id', '=', $this->project->id)
                                    ->where('module_rule_id', '=', $new_mr->id)
                                    ->first();

                                $new_p_task->norm_contacts = (float)$new_mr->norm;
                                $new_p_task->number_contacts = (int)$new_mr->number;
                                $new_p_task->save();

                                $this->success = true;
                            } catch (\Exception $e) {
                                $this->success = false;
                            }
                        }

                        //

                        $this->success = true;
                    } catch (\Exception $e) {
                        DB::rollback();
                        $this->success = false;
                    }
                }*/
            });

            if ($this->success) {
                return redirect('/manager/projects')->with('success', 'Проект успешно создан');
            } else {
                return redirect('/manager/projects')->with('error', 'Произошла ошибка при создании проекта');
            }

        } else {
            abort(503);
        }
    }

    public function sortProjectTasks(Request $request) {
        if ($request->ajax()) {
            $cur_user = Sentinel::getUser();
            unset($p_tasks);
            switch ($request->status) {
                case 'all':
                    $project_tasks_all = Project_task::where('project_id', '=', $request->pid)
                        //->whereNull('status')
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($project_tasks_all) > 0) {
                        foreach ($project_tasks_all as $pt1) {
                            $name = Module_rule::find($pt1->module_rule_id);
                            $p_name = Project::find($pt1->project_id);
                            $m_name = Modules_reference::find($pt1->module_id);
                            $g_task = Grouptasks_reference::find($name->grouptasks_id);

                            $ptc = Project_task_contact::where('project_task_id', '=', $pt1->id)
                                ->whereNull('deleted_at')
                                ->first();

                            if (count($ptc) > 0) {
                                $p_tasks[] = [
                                    'id' => $pt1->id,
                                    'name' => $name->task,
                                    'p_name' => $p_name->name,
                                    'city' => $p_name->city,
                                    'm_name' => $m_name->name,
                                    'g_task' => $g_task->name,
                                    'additional' => $name->additional,
                                    'status' => $pt1->tasksstatus_ref_id,
                                    'next_contact' => $ptc['next_contact'],
                                    'print_form' => 0
                                ];
                            } else {
                                $p_tasks[] = [
                                    'id' => $pt1->id,
                                    'name' => $name->task,
                                    'p_name' => $p_name->name,
                                    'city' => $p_name->city,
                                    'm_name' => $m_name->name,
                                    'g_task' => $g_task->name,
                                    'additional' => $name->additional,
                                    'status' => $pt1->tasksstatus_ref_id,
                                    'next_contact' => null,
                                    'print_form' => 0
                                ];
                            }
                        }
                    } else {
                        $p_tasks[] = '';
                    }

                    /*$project_form = Project_print_form::where('project_id', '=', $request->pid)
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($project_form) > 0) {
                        foreach ($project_form as $pf) {
                            $name = Module_rule::where('id', '=', $pf->module_rule_id)
                                ->whereNull('deleted_at')
                                ->first();
                            $ptc = Project_task_contact::where('project_task_id', '=', $pf->id)
                                ->whereNull('deleted_at')
                                ->first();
                            $p_name = Project::find($pf->project_id);
                            $m_name = Modules_reference::find($name->module_id);
                            $g_task = Grouptasks_reference::find(11);

                            $q_ar[] = '';

                            if (count($ptc) > 0) {
                                $p_tasks[] = [
                                    'id' => $pf->id,
                                    'name' => $pf->name,
                                    'p_name' => $p_name->name,
                                    'observer_id' => $pf->observer_id,
                                    'city' => $p_name->city,
                                    'm_name' => $m_name->name,
                                    'callbackstatus_ref_id' => $pf->callbackstatus_ref_id,
                                    'g_task' => $g_task['name'],
                                    'additional' => $name['additional'],
                                    'status' => 0,
                                    'next_contact' => $ptc['next_contact'],
                                    'print_form' => 1
                                ];
                            } else {
                                $p_tasks[] = [
                                    'id' => $pf->id,
                                    'name' => $pf->name,
                                    'p_name' => $p_name->name,
                                    'observer_id' => $pf->observer_id,
                                    'city' => $p_name->city,
                                    'm_name' => $m_name['name'],
                                    'callbackstatus_ref_id' => $pf->callbackstatus_ref_id,
                                    'g_task' => $g_task['name'],
                                    'additional' => $name['additional'],
                                    'status' => 0,
                                    'next_contact' => null,
                                    'print_form' => 1
                                ];
                            }

                        }
                    } else {
                        $p_tasks = '';
                    }*/

                    break;
                case 'main':
                    $project_tasks_main = Project_task::select('project_tasks.id',
                        'project_tasks.module_rule_id',
                        'project_tasks.module_id',
                        'project_tasks.project_id',
                        'project_tasks.tasksstatus_ref_id')
                        ->join('module_rules', 'project_tasks.module_rule_id', '=', 'module_rules.id')
                        ->where('project_tasks.project_id', '=', $request->pid)
                        ->whereNull('module_rules.additional')
                        ->whereNull('project_tasks.deleted_at')
                        //->whereNull('status')
                        ->get();

                    if (count($project_tasks_main) > 0) {
                        foreach ($project_tasks_main as $pt2) {
                            $name = Module_rule::find($pt2->module_rule_id);
                            $p_name = Project::find($pt2->project_id);
                            $m_name = Modules_reference::find($pt2->module_id);
                            $g_task = Grouptasks_reference::find($name->grouptasks_id);

                            $ptc = Project_task_contact::where('project_task_id', '=', $pt2->id)
                                ->whereNull('deleted_at')
                                ->first();

                            if (count($ptc) > 0) {
                                $p_tasks[] = [
                                    'id' => $pt2->id,
                                    'name' => $name->task,
                                    'p_name' => $p_name->name,
                                    'city' => $p_name->city,
                                    'm_name' => $m_name->name,
                                    'g_task' => $g_task->name,
                                    'additional' => $name->additional,
                                    'status' => $pt2->tasksstatus_ref_id,
                                    'next_contact' => $ptc['next_contact'],
                                    'print_form' => 0
                                ];
                            } else {
                                $p_tasks[] = [
                                    'id' => $pt2->id,
                                    'name' => $name->task,
                                    'p_name' => $p_name->name,
                                    'city' => $p_name->city,
                                    'm_name' => $m_name->name,
                                    'g_task' => $g_task->name,
                                    'additional' => $name->additional,
                                    'status' => $pt2->tasksstatus_ref_id,
                                    'next_contact' => null,
                                    'print_form' => 0
                                ];
                            }
                        }
                    } else {
                        $p_tasks[] = '';
                    }

                    /*$project_form = Project_print_form::select('project_print_forms.id',
                        'project_print_forms.module_rule_id',
                        'project_print_forms.module_id',
                        'project_print_forms.project_id',
                        'project_print_forms.name')
                        ->where('project_print_forms.project_id', '=', $request->pid)
                        ->whereNull('project_print_forms.deleted_at')
                        ->join('module_rules', 'project_print_forms.module_rule_id', '=', 'module_rules.id')
                        ->whereNull('module_rules.additional')
                        ->get();

                    if (count($project_form) > 0) {
                        foreach ($project_form as $pf) {
                            $name = Module_rule::where('id', '=', $pf->module_rule_id)
                                ->whereNull('deleted_at')
                                ->first();
                            $ptc = Project_task_contact::where('project_task_id', '=', $pf->id)
                                ->whereNull('deleted_at')
                                ->first();
                            $p_name = Project::find($pf->project_id);
                            $m_name = Modules_reference::find($pf->module_id);
                            $g_task = Grouptasks_reference::find(11);

                            $q_ar[] = '';

                            if (count($ptc) > 0) {
                                $p_tasks[] = [
                                    'id' => $pf->id,
                                    'name' => $pf->name,
                                    'p_name' => $p_name->name,
                                    'observer_id' => $pf->observer_id,
                                    'city' => $p_name->city,
                                    'm_name' => $m_name->name,
                                    'callbackstatus_ref_id' => $pf->callbackstatus_ref_id,
                                    'g_task' => $g_task['name'],
                                    'additional' => $name['additional'],
                                    'status' => 0,
                                    'next_contact' => $ptc['next_contact'],
                                    'print_form' => 1
                                ];
                            } else {
                                $p_tasks[] = [
                                    'id' => $pf->id,
                                    'name' => $pf->name,
                                    'p_name' => $p_name->name,
                                    'observer_id' => $pf->observer_id,
                                    'city' => $p_name->city,
                                    'm_name' => $m_name['name'],
                                    'callbackstatus_ref_id' => $pf->callbackstatus_ref_id,
                                    'g_task' => $g_task['name'],
                                    'additional' => $name['additional'],
                                    'status' => 0,
                                    'next_contact' => null,
                                    'print_form' => 1
                                ];
                            }

                        }
                    } else {
                        $p_tasks = '';
                    }*/

                    break;
                case 'additional':
                    $project_tasks_additional = Project_task::select('project_tasks.id',
                        'project_tasks.module_rule_id',
                        'project_tasks.module_id',
                        'project_tasks.project_id',
                        'project_tasks.tasksstatus_ref_id')
                        ->join('module_rules', 'project_tasks.module_rule_id', '=', 'module_rules.id')
                        ->where('project_tasks.project_id', '=', $request->pid)
                        ->where('module_rules.additional', '=', 1)
                        ->whereNull('project_tasks.deleted_at')
                        //->whereNull('status')
                        ->get();

                    if (count($project_tasks_additional) > 0) {
                        foreach ($project_tasks_additional as $pt3) {
                            $name = Module_rule::find($pt3->module_rule_id);
                            $p_name = Project::find($pt3->project_id);
                            $m_name = Modules_reference::find($pt3->module_id);
                            $g_task = Grouptasks_reference::find($name->grouptasks_id);

                            $ptc = Project_task_contact::where('project_task_id', '=', $pt3->id)
                                ->whereNull('deleted_at')
                                ->first();

                            if (count($ptc) > 0) {
                                $p_tasks[] = [
                                    'id' => $pt3->id,
                                    'name' => $name->task,
                                    'p_name' => $p_name->name,
                                    'city' => $p_name->city,
                                    'm_name' => $m_name->name,
                                    'g_task' => $g_task->name,
                                    'additional' => $name->additional,
                                    'status' => $pt3->tasksstatus_ref_id,
                                    'next_contact' => $ptc['next_contact'],
                                    'print_form' => 0
                                ];
                            } else {
                                $p_tasks[] = [
                                    'id' => $pt3->id,
                                    'name' => $name->task,
                                    'p_name' => $p_name->name,
                                    'city' => $p_name->city,
                                    'm_name' => $m_name->name,
                                    'g_task' => $g_task->name,
                                    'additional' => $name->additional,
                                    'status' => $pt3->tasksstatus_ref_id,
                                    'next_contact' => null,
                                    'print_form' => 0
                                ];
                            }
                        }
                    } else {
                        $p_tasks[] = '';
                    }

                    /*$project_form = Project_print_form::select('project_print_forms.id',
                        'project_print_forms.module_rule_id',
                        'project_print_forms.module_id',
                        'project_print_forms.project_id',
                        'project_print_forms.name')
                        ->where('project_print_forms.project_id', '=', $request->pid)
                        ->whereNull('project_print_forms.deleted_at')
                        ->join('module_rules', 'project_print_forms.module_rule_id', '=', 'module_rules.id')
                        ->where('module_rules.additional', '=', 1)
                        ->get();

                    if (count($project_form) > 0) {
                        foreach ($project_form as $pf) {
                            $name = Module_rule::where('id', '=', $pf->module_rule_id)
                                ->whereNull('deleted_at')
                                ->first();
                            $ptc = Project_task_contact::where('project_task_id', '=', $pf->id)
                                ->whereNull('deleted_at')
                                ->first();
                            $p_name = Project::find($pf->project_id);
                            $m_name = Modules_reference::find($name->module_id);
                            $g_task = Grouptasks_reference::find(11);

                            $q_ar[] = '';

                            if (count($ptc) > 0) {
                                $p_tasks[] = [
                                    'id' => $pf->id,
                                    'name' => $pf->name,
                                    'p_name' => $p_name->name,
                                    'observer_id' => $pf->observer_id,
                                    'city' => $p_name->city,
                                    'm_name' => $m_name->name,
                                    'callbackstatus_ref_id' => $pf->callbackstatus_ref_id,
                                    'g_task' => $g_task['name'],
                                    'additional' => $name['additional'],
                                    'status' => 0,
                                    'next_contact' => $ptc['next_contact'],
                                    'print_form' => 1
                                ];
                            } else {
                                $p_tasks[] = [
                                    'id' => $pf->id,
                                    'name' => $pf->name,
                                    'p_name' => $p_name->name,
                                    'observer_id' => $pf->observer_id,
                                    'city' => $p_name->city,
                                    'm_name' => $m_name['name'],
                                    'callbackstatus_ref_id' => $pf->callbackstatus_ref_id,
                                    'g_task' => $g_task['name'],
                                    'additional' => $name['additional'],
                                    'status' => 0,
                                    'next_contact' => null,
                                    'print_form' => 1
                                ];
                            }

                        }
                    } else {
                        $p_tasks = '';
                    }*/

                    break;
            }
            return response()->json($p_tasks);
        }
    }

    public function sortTasks(Request $request) {
        if ($request->ajax()) {
            $cur_user = Sentinel::getUser();
            switch ($request->status) {
                case 'notExecute':
                    $project_tasks = Project_task::where('user_id', '=', $cur_user->id)
                        ->whereIn('tasksstatus_ref_id', [1,3])
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($project_tasks) > 0) {
                        foreach ($project_tasks as $pt) {
                            $name = Module_rule::find($pt->module_rule_id);
                            $p_name = Project::find($pt->project_id);
                            $p_client = Clients::getClientById($p_name['client_id']);
                            $p_city = Citys::getCityById($p_client->city_id);

                            $ptc = Project_task_contact::where('project_task_id', '=', $pt->id)
                                ->whereNull('deleted_at')
                                ->first();

                            $user = User::whereNull('deleted_at')->get();
                            foreach ($user as $u) {
                                if ($u->id == $p_name->user_id) {
                                    $observer[] = [
                                        'default' => 1,
                                        'id' => $u->id,
                                        'first_name' => $u->first_name,
                                        'last_name' => $u->last_name
                                    ];
                                } else {
                                    $observer[] = [
                                        'default' => 0,
                                        'id' => $u->id,
                                        'first_name' => $u->first_name,
                                        'last_name' => $u->last_name
                                    ];
                                }
                            }

                            if (count($ptc) > 0) {
                                if ($pt['prev_contact'] != null) {
                                    $p_tasks[] = [
                                        'id' => $pt['id'],
                                        'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                        'client' => $p_client->name,
                                        'name' => $name->task,
                                        'observer' => $observer,
                                        'p_name' => $p_name['name'],
                                        'city' => $p_city,
                                        'next_contact' => $ptc['next_contact'],
                                        'prev_contact' => $pt['prev_contact'],
                                    ];
                                } else {
                                    $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                                        ->whereNull('deleted_at')
                                        //->whereNull('status')
                                        ->max('prev_contact');

                                    $p_tasks[] = [
                                        'id' => $pt['id'],
                                        'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                        'client' => $p_client->name,
                                        'name' => $name->task,
                                        'observer' => $observer,
                                        'p_name' => $p_name['name'],
                                        'city' => $p_city,
                                        'next_contact' => $ptc['next_contact'],
                                        'prev_contact' => $max_date,
                                    ];
                                }
                            } else {
                                if ($pt['prev_contact'] != null) {
                                    $p_tasks[] = [
                                        'id' => $pt['id'],
                                        'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                        'client' => $p_client->name,
                                        'name' => $name->task,
                                        'observer' => $observer,
                                        'p_name' => $p_name['name'],
                                        'city' => $p_city,
                                        'next_contact' => null,
                                        'prev_contact' => $pt['prev_contact'],
                                    ];
                                } else {
                                    $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                                        ->whereNull('deleted_at')
                                        //->whereNull('status')
                                        ->max('prev_contact');

                                    $p_tasks[] = [
                                        'id' => $pt['id'],
                                        'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                        'client' => $p_client->name,
                                        'name' => $name->task,
                                        'observer' => $observer,
                                        'p_name' => $p_name['name'],
                                        'city' => $p_city,
                                        'next_contact' => null,
                                        'prev_contact' => $max_date,
                                    ];
                                }
                            }
                        }
                    } else {
                        $p_tasks[] = '';
                    }
                    return response()->json($p_tasks);
                    break;

                case 'all':
                    $project_tasks = Project_task::where('user_id', '=', $cur_user->id)
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($project_tasks) > 0) {
                        foreach ($project_tasks as $pt) {
                            $name = Module_rule::find($pt->module_rule_id);
                            $p_name = Project::find($pt->project_id);
                            $p_client = Clients::getClientById($p_name['client_id']);
                            $p_city = Citys::getCityById($p_client->city_id);

                            $ptc = Project_task_contact::where('project_task_id', '=', $pt->id)
                                ->whereNull('deleted_at')
                                ->get();

                            $user = User::whereNull('deleted_at')->get();
                            foreach ($user as $u) {
                                if ($u->id == $p_name->user_id) {
                                    $observer[] = [
                                        'default' => 1,
                                        'id' => $u->id,
                                        'first_name' => $u->first_name,
                                        'last_name' => $u->last_name
                                    ];
                                } else {
                                    $observer[] = [
                                        'default' => 0,
                                        'id' => $u->id,
                                        'first_name' => $u->first_name,
                                        'last_name' => $u->last_name
                                    ];
                                }
                            }

                            if (count($ptc) > 0) {
                                if ($pt['prev_contact'] != null) {
                                    $p_tasks[] = [
                                        'id' => $pt['id'],
                                        'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                        'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                                        'client' => $p_client->name,
                                        'name' => $name->task,
                                        'observer' => $observer,
                                        'p_name' => $p_name['name'],
                                        'city' => $p_city,
                                        'next_contact' => $ptc[0]['next_contact'],
                                        'prev_contact' => $pt['prev_contact'],
                                    ];
                                } else {
                                    $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                                        ->whereNull('deleted_at')
                                        //->whereNull('status')
                                        ->max('prev_contact');

                                    $p_tasks[] = [
                                        'id' => $pt['id'],
                                        'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                        'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                                        'client' => $p_client->name,
                                        'name' => $name->task,
                                        'observer' => $observer,
                                        'p_name' => $p_name['name'],
                                        'city' => $p_city,
                                        'next_contact' => $ptc[0]['next_contact'],
                                        'prev_contact' => $max_date,
                                    ];
                                }
                            } else {
                                if ($pt['prev_contact'] != null) {
                                    $p_tasks[] = [
                                        'id' => $pt['id'],
                                        'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                        'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                                        'client' => $p_client->name,
                                        'name' => $name->task,
                                        'observer' => $observer,
                                        'p_name' => $p_name['name'],
                                        'city' => $p_city,
                                        'next_contact' => null,
                                        'prev_contact' => $pt['prev_contact'],
                                    ];
                                } else {
                                    $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                                        ->whereNull('deleted_at')
                                        //->whereNull('status')
                                        ->max('prev_contact');

                                    $p_tasks[] = [
                                        'id' => $pt['id'],
                                        'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                        'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                                        'client' => $p_client->name,
                                        'name' => $name->task,
                                        'observer' => $observer,
                                        'p_name' => $p_name['name'],
                                        'city' => $p_city,
                                        'next_contact' => null,
                                        'prev_contact' => $max_date,
                                    ];
                                }
                            }
                        }
                    } else {
                        $p_tasks[] = '';
                    }
                    return response()->json($p_tasks);
                    break;
            }
        }
    }

    public function calc($new_hour = 0) {
        $p_form = Project_print_form::where('printform_status_ref_id', '!=', 5)
            ->whereNull('deleted_at')
            ->get();

        $hour = 0;
        if (count($p_form) > 0) {
            foreach ($p_form as $pf) {
                $hour += $pf->max_hours;
            }
            $res = (($hour + $new_hour) / 8) / 2;

            $cur_date = date("j").'.'.date("n").'.'.date("Y");
            $date = date('Y-m-d', strtotime($cur_date . ' +'.round($res).' day'));
            //$tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +'.round($res).' day'));
        } else {
            $date = date("Y").'-'.date("m").'-'.date("d");
        }

        return $date;
    }

    public function addAdditionalTask(Request $request) {
        //dd($request->all());
        $module_rule = new Module_rule();
        $module_rule->module_id = $request->selectModule;
        $module_rule->task = $request->name_task;
        $module_rule->grouptasks_id = $request->selectGroupTask;
        $module_rule->additional = 1;
        $module_rule->number_contacts = $request->cont;


        if ($module_rule->save()) {
            $cur_user = Sentinel::getUser();

            if ($request->selectGroupTask != 11) {
                $project = Project::find($request->pid);
                try {
                    $project_task = new Project_task();
                    $project_task->project_id = $request->pid;
                    $project_task->user_id = $project['user_id'];
                    $project_task->observer_id = $request->observer_id;
                    $project_task->module_rule_id = $module_rule->id;
                    $project_task->number_contacts = $request->cont;
                    $project_task->grouptasks_id = $request->selectGroupTask;
                    $project_task->module_id = $request->selectModule;
                    $project_task->tasksstatus_ref_id = 1;
                    $project_task->save();

                    $success = true;
                } catch (\Exception $e) {
                    $success = false;
                }
            } else {
                $number = Project_print_form::whereNull('deleted_at')
                    ->max('number');
                $date = $this->calc();

                try {
                    /*$p_form = new Project_print_form();
                    $p_form->number = $number+1;
                    $p_form->project_id = $request->pid;
                    $p_form->user_id = $request->user_id;
                    $p_form->observer_id = $request->observer_id;
                    $p_form->name = $request->name_task;
                    $p_form->category_printform_ref_id = 1;
                    $p_form->module_rule_id = $module_rule->id;
                    $p_form->max_hours = 5;
                    $p_form->printform_status_ref_id = 1;
                    $p_form->date_implement = $date;
                    $p_form->date_implement_for_client = $date;
                    $p_form->is_printform = 0;
                    $p_form->is_free = 0;
                    $p_form->paid = 0;
                    $p_form->save();*/

                    $project_task = new Project_task();
                    $project_task->project_id = $request->pid;
                    $project_task->user_id = $cur_user->id;
                    $project_task->observer_id = $request->observer_id;
                    $project_task->module_rule_id = $module_rule->id;
                    $project_task->number_contacts = $request->cont;
                    $project_task->grouptasks_id = $request->selectGroupTask;
                    $project_task->module_id = $request->selectModule;
                    $project_task->tasksstatus_ref_id = 1;
                    $project_task->save();

                    $success = true;
                } catch (\Exception $e) {
                    $success = false;
                }
            }

            if ($success) {
                return redirect()->back()->with('success', 'Дополнительная задача успешно добавлена');
            }else {
                return redirect()->back()->with('error', 'Произошла ошибка при добавлении дополнительной задачи');
            }
        } else {
            return redirect()->back()->with('error', 'Произошла ошибка при добавлении дополнительной задачи');
        }
    }

    public function getCloseProjects(Request $request) {
        if ($request->ajax()) {
            $projects = Project::where('callback_status', '=', 6)
                ->get();

            $close_projects[] = '';

            if ($projects != '') {
                foreach ($projects as $p) {
                    $p_client = Clients::getClientById($p->client_id);
                    $p_city = Citys::getCityById($p_client->city_id);
                    $comment = Comments::getProjectCloseComment($p->id);

                    $close_projects[] = [
                        'id' => $p->id,
                        'p_name' => $p->name,
                        'client' => $p_client->name,
                        'city' => $p_city,
                        'comment' => $comment->text,
                        'done_datetime' => $p->done_datetime
                    ];
                }
            }

            return response()->json($close_projects);
        }
    }

    public function getWorkProjects(Request $request) {
        if ($request->ajax()) {
            $frequency_feedback = Settings::getSettings('frequency_feedback');
            $cur_date = date("d").'-'.date("n").'-'.date("Y");
            $projects = Project::whereNotIn('callback_status', [6])
                ->orWhereNull('callback_status')
                ->get();
            //dd(count($projects));
            $feedback_projects = '';
            if (count($projects) > 0) {
                foreach ($projects as $p) {
                    $frequency_feedback_date = date('d-m-Y', strtotime($p->start_date . ' +' . (int)$frequency_feedback->value . ' day'));
                    if ($frequency_feedback_date == $cur_date) {
                        $feedback_projects_ar[] = $p;
                    };
                }
            }

            if (isset($feedback_projects_ar)) {
                foreach ($feedback_projects_ar as $fp) {
                    $project_task_contact = Project_task_contact::where('project_id', '=', $fp->id)
                        ->whereNull('deleted_at')
                        ->orderBy('prev_contact', 'desc')
                        ->limit(1)
                        ->get();

                    foreach ($project_task_contact as $ptc) {
                        $project_task = Project_task::where('project_id', '=', $ptc->project_id)
                            ->where('id', '=', $ptc->project_task_id)
                            ->whereNull('deleted_at')
                            ->get();

                        foreach ($project_task as $pt) {
                            $mr = Module_rule::find($pt->module_rule_id);
                            $project_contact = Project_contact::where('project_id', '=', $fp->id)
                                ->whereNull('deleted_at')
                                ->get();
                            $contact = Contact::find($project_contact[0]->contact_id);
                            $client = Clients::getClientById($contact->client_id);
                            $city = Citys::getCityById($client->city_id);

                            $feedback_projects[] = [
                                'id' => $fp->id,
                                'prev_contact' => $ptc->prev_contact,
                                'callback_prev_contact' => $fp->callback_prev_contact,
                                'p_name' => $fp->name,
                                'task' => $mr->task,
                                'client' => $client->name,
                                'city' => $city
                            ];
                        }
                    }
                }
            } else {
                $feedback_projects = '';
            }
            return response()->json($feedback_projects);
        }
    }

    public function editCloseProject($id) {
        $project = Project::find($id);
        $p_client = Clients::getClientById($project->client_id);
        $comment = Comments::getProjectCloseComment($project->id);
        $main_contact = Contacts::getProjectMainContact($project->id);
        $user = Users2::getUserById($project->user_id);
        $comments = Comments::getCommentsByProjectId($project->id);
        $close_question = Callback_close_question::whereNull('deleted_at')
            ->get();
        //dd($close_question);

        return view('Manager.projects.editCloseProject', [
            'project' => $project,
            'client' => $p_client,
            'comment_close' => strip_tags($comment->text),
            'main_contact' => $main_contact,
            'user' => $user,
            'comments' => $comments,
            'close_question' => $close_question
        ]);
    }

    public function postEditCloseProject(Request $request, $id) {
        $data = json_decode($request->data);
        $date =  explode(' ', date("Y-m-d H:i:s"));
        $cur_user = Sentinel::getUser();

        switch ($data->optionsRadios) {
            case '1':
                try {
                    $project = Project::find($id);
                    $project->done = 0;
                    $project->callback_status = 6;
                    $project->done_datetime = date("Y-m-d H:i:s");
                    $project->callback_prev_contact = date("Y-m-d H:i:s");
                    $project->save();

                    $this->success = true;
                } catch (\Exception $e) {
                    $this->success = false;
                }

                try {
                    $comment = new Comment();
                    $comment->project_id = $id;
                    $comment->user_id = $cur_user->id;
                    $comment->for_client = NULL;
                    $comment->date = $date[0];
                    $comment->time = $date[1];
                    $comment->text = $data->comment;
                    $comment->save();

                    $this->success = true;
                } catch (\Exception $e) {
                    $this->success = false;
                }

                if ($this->success) {
                    return response()->json(['status' => 'Ok']);
                } else {
                    return response()->json(['status' => 'Error']);
                }
                break;
            case '2':
                switch ($data->closeProject) { 
                    case '1':
                        $close_q_ar = $data->close;
                        $date =  explode(' ', date("Y-m-d H:i:s"));
                        $cur_user = Sentinel::getUser();

                        try {
                            $project = Project::find($id);
                            $project->done = 1;
                            $project->callback_status = NULL;
                            $project->done_datetime = date("Y-m-d H:i:s");
                            $project->callback_prev_contact = date("Y-m-d H:i:s");
                            $project->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }

                        foreach ($close_q_ar as $cq) {
                            try {
                                $close = new Callback_close_answer();
                                $close->question_id = $cq->q_id;
                                $close->answer = $cq->answer;
                                $close->project_id = $id;
                                $close->save();

                                $this->success = true;
                            } catch (\Exception $e) {
                                $this->success = false;
                            }
                        }

                        try {
                            $comment = new Comment();
                            $comment->project_id = $id;
                            $comment->user_id = $cur_user->id;
                            $comment->for_client = NULL;
                            $comment->date = $date[0];
                            $comment->time = $date[1];
                            $comment->text = $data->comment;
                            $comment->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }

                        if ($this->success) {
                            broadcast(new ManagerCloseProject($project->name, $cur_user->first_name.' '.$cur_user->last_name));
                            return response()->json(['status' => 'Ok']);
                        } else {
                            return response()->json(['status' => 'Error']);
                        }
                        break;
                    case '2':
                        $close_q_ar = $data->close;
                        $date =  explode(' ', date("Y-m-d H:i:s"));
                        $cur_user = Sentinel::getUser();

                        try {
                            $project = Project::find($id);
                            $project->done = 0;
                            $project->callback_status = 1;
                            $project->done_datetime = date("Y-m-d H:i:s");
                            $project->callback_prev_contact = date("Y-m-d H:i:s");
                            $project->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }

                        foreach ($close_q_ar as $cq) {
                            try {
                                $close = new Callback_close_answer();
                                $close->question_id = $cq->q_id;
                                $close->answer = $cq->answer;
                                $close->project_id = $id;
                                $close->save();

                                $this->success = true;
                            } catch (\Exception $e) {
                                $this->success = false;
                            }
                        }

                        try {
                            $project_task_contact = Project_task_contact::where('project_id', '=', $id)
                                ->whereNull('deleted_at')
                                ->orderBy('prev_contact', 'desc')
                                ->limit(1)
                                ->get();
                            $project_task_contact[0]->callbackstatus_ref_id = 1;
                            $project_task_contact[0]->status = 1;
                            $project_task_contact[0]->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }

                        try {
                            $comment = new Comment();
                            $comment->project_id = $id;
                            $comment->user_id = $cur_user->id;
                            $comment->for_client = NULL;
                            $comment->date = $date[0];
                            $comment->time = $date[1];
                            $comment->text = $data->comment;
                            $comment->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }

                        if ($this->success) {
                            return response()->json(['status' => 'Ok']);
                        } else {
                            return response()->json(['status' => 'Error']);
                        }
                        break;
                }
                break;
        }
    }

    public function editCallbackProject($id) {
        $project = Project::find($id);
        $p_client = Clients::getClientById($project->client_id);
        $main_contact = Contacts::getProjectMainContact($project->id);
        $user = Users2::getUserById($project->user_id);
        $comments = Comments::getCommentsByProjectId($project->id);
        $work_question = Callback_work_question::whereNull('deleted_at')
            ->get();

        if(count($work_question) > 0) {
            foreach ($work_question as $wq) {
                $work_answer = Callback_work_answer::where('project_id', '=', $id)
                    ->where('question_id', '=', $wq->id)
                    ->get();

                if (count($work_answer) > 0) {
                    $wa[] = [
                        'id' => $work_answer[0]->id,
                        'question_id' => $work_answer[0]->question_id,
                        'answer' => $work_answer[0]->answer
                    ];
                } else {
                    $wa = '';
                }
            }
        } else {
            $work_question = '';
            $wa = '';
        }

        return view('Manager.projects.editCallsProject', [
            'project' => $project,
            'client' => $p_client,
            'main_contact' => $main_contact,
            'user' => $user,
            'comments' => $comments,
            'work_question' => $work_question,
            'work_answer' => $wa,
        ]);
    }

    public function postEditCallbackProject(Request $request, $id) {
        $date =  explode(' ', date("Y-m-d H:i:s"));
        $cur_user = Sentinel::getUser();

        switch ($request->optionsRadios) {
            case '1':
                try {
                    $project = Project::find($id);
                    $project->done = 0;
                    $project->callback_status = 1;
                    $project->done_datetime = date("Y-m-d H:i:s");
                    $project->callback_prev_contact = date("Y-m-d H:i:s");
                    $project->save();

                    $this->success = true;
                } catch (\Exception $e) {
                    $this->success = false;
                }

                /*try {
                    $project_task_contact = Project_task_contact::where('project_id', '=', $id)
                        ->whereNull('deleted_at')
                        ->orderBy('prev_contact', 'desc')
                        ->limit(1)
                        ->get();
                    $project_task_contact[0]->callbackstatus_ref_id = 5;
                    $project_task_contact[0]->save();

                    $this->success = true;
                } catch (\Exception $e) {
                    $this->success = false;
                }*/

                try {
                    $comment = new Comment();
                    $comment->project_id = $id;
                    $comment->user_id = $cur_user->id;
                    $comment->for_client = NULL;
                    $comment->date = $date[0];
                    $comment->time = $date[1];
                    $comment->text = $request->comment;
                    $comment->save();

                    $this->success = true;
                } catch (\Exception $e) {
                    $this->success = false;
                }

                if ($this->success) {
                    return redirect()->back()->with('success', 'Данные успешно сохранены');
                } else {
                    return redirect()->back()->with('error', 'Произошла ошибка при сохранении данных');
                }
                break;
            case '2':
                $work_q_ar = json_decode($request->work);
                $date =  explode(' ', date("Y-m-d H:i:s"));
                $cur_user = Sentinel::getUser();

                try {
                    $project = Project::find($id);
                    $project->callback_prev_contact = date("Y-m-d H:i:s");
                    $project->save();

                    $this->success = true;
                } catch (\Exception $e) {
                    $this->success = false;
                }

                foreach ($work_q_ar as $wq) {
                    try {
                        $work = new Callback_work_answer();
                        $work->question_id = $wq->q_id;
                        $work->answer = $wq->a_id;
                        $work->project_id = $id;
                        $work->save();

                        $this->success = true;
                    } catch (\Exception $e) {
                        $this->success = false;
                    }
                }
                try {
                    $comment = new Comment();
                    $comment->project_id = $id;
                    $comment->user_id = $cur_user->id;
                    $comment->for_client = NULL;
                    $comment->date = $date[0];
                    $comment->time = $date[1];
                    $comment->text = $request->comment;
                    $comment->save();

                    $this->success = true;
                } catch (\Exception $e) {
                    $this->success = false;
                }

                if ($this->success) {
                    return redirect()->back()->with('success', 'Данные успешно сохранены');
                } else {
                    return redirect()->back()->with('error', 'Произошла ошибка при сохранении данных');
                }
                break;
        }
    }

    public function editOtherProject(Request $request, $id) {
        $other_task = Callback_other_task::where('id', '=', $id)
            ->whereNull('deleted_at')
            ->first();
        $client = Clients::getClientById($other_task->client_id);
        $project = self::getProjectById($other_task->project_id);
        $user = Users::getUserById($project->user_id);

        return view('Manager.projects.editOtherProject', [
            'main_contact' => 1,
            'type' => $other_task->type,
            'client' => $client->name,
            'project' => $project->name,
            'user' => $user->first_name.' '.$user->last_name
        ]);
    }

    static public function getProjectById($id) {
        $project = Project::whereNull('deleted_at')
            ->where('id', '=', $id)
            ->firstOrFail();
        return $project;
    }

    static public function getProjectByTaskId($task_id) {
        $task = Project_task::find($task_id);
        $project = Project::find($task->project_id);

        return $project;
    }

    static public function getAllProjects() {
        $projects = Project::whereNull('deleted_at')->get();
        return $projects;
    }

    static public function getUserProjects($user_id) {
        $projects = Project::where('user_id', '=', $user_id)
            ->where('active', '=', 1)
            ->whereNull('deleted_at')
            ->get();

        return $projects;
    }

    static public function getUserTasks($user_id) {
        $tasks = Project_task::where('user_id', '=', $user_id)
            ->whereNull('deleted_at')
            ->get();

        if (count($tasks) > 0) {
            foreach ($tasks as $t) {
                $module_rule = Module_rule::find($t->module_rule_id);
                $grouptask = Grouptasks_reference::find($module_rule->grouptask_id);
                if ($grouptask['is_print_form'] != 1) {
                    $tasks_ar[] = [
                        'id' => $t['id'],
                        'number_contacts' => $t['number_contacts'],
                    ];
                }
            }
        } else {
            $tasks_ar = [];
        }

        return $tasks_ar;
    }

    public function searchTasks(Request $request) {
        if ($request->ajax()) {
            $cur_user = Sentinel::getUser();
            $project_tasks = Project_task::where('user_id', '=', $cur_user->id)
                ->whereNull('deleted_at')
                ->get();

            if (count($project_tasks) > 0) {
                foreach ($project_tasks as $pt) {
                    $name = Module_rule::find($pt->module_rule_id);
                    $p_name = Project::find($pt->project_id);
                    $p_client = Clients::getClientById($p_name['client_id']);
                    $p_city = Citys::getCityById($p_client->city_id);
                    $ptc = Project_task_contact::where('project_task_id', '=', $pt->id)
                        ->whereNull('deleted_at')
                        ->first();

                    $user = User::whereNull('deleted_at')->get();
                    foreach ($user as $u) {
                        if ($u->id == $p_name->user_id) {
                            $observer[] = [
                                'default' => 1,
                                'id' => $u->id,
                                'first_name' => $u->first_name,
                                'last_name' => $u->last_name
                            ];
                        } else {
                            $observer[] = [
                                'default' => 0,
                                'id' => $u->id,
                                'first_name' => $u->first_name,
                                'last_name' => $u->last_name
                            ];
                        }
                    }

                    if (count($ptc) > 0) {
                        if ($pt['prev_contact'] != null) {
                            $p_tasks[] = [
                                'id' => $pt['id'],
                                'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                                'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                'client' => $p_client->name,
                                'name' => $name->task,
                                'observer' => $observer,
                                'p_name' => $p_name['name'],
                                'city' => $p_city,
                                'next_contact' => $ptc['next_contact'],
                                'prev_contact' => $pt['prev_contact'],
                            ];
                        } else {
                            $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                                ->whereNull('deleted_at')
                                //->whereNull('status')
                                ->max('prev_contact');

                            $p_tasks[] = [
                                'id' => $pt['id'],
                                'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                                'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                'client' => $p_client->name,
                                'name' => $name->task,
                                'observer' => $observer,
                                'p_name' => $p_name['name'],
                                'city' => $p_city,
                                'next_contact' => $ptc['next_contact'],
                                'prev_contact' => $max_date,
                            ];
                        }
                    } else {
                        if ($pt['prev_contact'] != null) {
                            $p_tasks[] = [
                                'id' => $pt['id'],
                                'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                'client' => $p_client->name,
                                'name' => $name->task,
                                'observer' => $observer,
                                'p_name' => $p_name['name'],
                                'city' => $p_city,
                                'next_contact' => null,
                                'prev_contact' => $pt['prev_contact'],
                            ];
                        } else {
                            $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                                ->whereNull('deleted_at')
                                //->whereNull('status')
                                ->max('prev_contact');

                            $p_tasks[] = [
                                'id' => $pt['id'],
                                'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                'client' => $p_client->name,
                                'name' => $name->task,
                                'observer' => $observer,
                                'p_name' => $p_name['name'],
                                'city' => $p_city,
                                'next_contact' => null,
                                'prev_contact' => $max_date,
                            ];
                        }
                    }
                }
            }

            $res = '';
            foreach ($p_tasks as $p_task) {
                if (strpos($p_task['client'], $request->text) || strpos($p_task['name'], $request->text)) {
                    $res[] = $p_task;
                }
            }

            return response()->json($res);
        }
    }

    public function searchProjects(Request $request) {
        if ($request->ajax()) {
            $cur_user = Sentinel::getUser();
            $projects_q = Project::where('user_id', '=', $cur_user->id)
                ->whereNULL('deleted_at')
                ->get();

            if (count($projects_q) > 0) {
                foreach ($projects_q as $p) {
                    $task = Project_task::where('project_id', '=', $p->id)
                        ->where('user_id', '=', $cur_user->id)
                        ->whereNull('deleted_at')
                        ->get();

                    $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                        ->whereNull('deleted_at')
                        ->get();

                    $client = Clients::getClientById($p->client_id);
                    $city = Citys::getCityById($client->city_id);

                    foreach ($task as $t) {
                        if ($t->tasksstatus_ref_id == 2) {
                            $done2[$p->id][] = $t->id;
                        }
                    }

                    if (isset($done2[$p->id])) {
                        $user = Users2::getUserById($p->user_id);
                        $client = Clients::getClientById($p->client_id);
                        $projects[] = [
                            'id' => $p->id,
                            'user' => $user->last_name.' '.$user->first_name,
                            'name' => $p->name,
                            'client' => $client->name,
                            'city' => $city,
                            'prev_contact' => $ptc[0]['prev_contact'],
                            'next_contact' => $ptc[0]['next_contact'],
                            'start_date' => $p->start_date,
                            'finish_date' => $p->finish_date,
                            'created_at' => $p->created_at,
                            'active' => $p->active,
                            'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                        ];
                    } else {
                        $user = Users2::getUserById($p->user_id);
                        $client = Clients::getClientById($p->client_id);
                        $projects[] = [
                            'id' => $p->id,
                            'user' => $user->last_name.' '.$user->first_name,
                            'name' => $p->name,
                            'client' => $client->name,
                            'city' => $city,
                            'prev_contact' => $ptc[0]['prev_contact'],
                            'next_contact' => $ptc[0]['next_contact'],
                            'start_date' => $p->start_date,
                            'finish_date' => $p->finish_date,
                            'created_at' => $p->created_at,
                            'active' => $p->active,
                            //'done' => 0,
                            'done' => round(((0 / count($task)) * 100), 2),
                        ];
                    }
                }
            } else {
                $projects[] = '';
            }

            $res = '';
            foreach ($projects as $p) {
                if ($p['client'] == $request->text ||
                    $p['name'] == $request->text ||
                    $p['user'] == $request->text) {
                    $res[] = $p;
                }
            }

            return response()->json($res);
        }
    }

    public function getProjectByClientId(Request $request) {
        if ($request->ajax()) {
            $projects = Project::where('client_id', '=', $request->id)
                ->whereNull('deleted_at')
                ->get();
            return response()->json($projects);
        }
    }

    public function getProjectByClientName(Request $request) {
        if ($request->ajax()) {
            $client = Clients_reference::where('name', '=', $request->client)
                ->whereNull('deleted_at')
                ->first();
            $projects = Project::where('client_id', '=', $client['id'])
                ->whereNull('deleted_at')
                ->get();
            return response()->json($projects);
        }
    }

    public function addClients() {
        $cities = Citys::getAllCitys();
        $posts = Posts::getAllPosts();
        return view('Manager.projects.addClient', [
            'citys' => $cities,
            'posts' => $posts
        ]);
    }

    public function postAddClients(Request $request) {
        if (session('perm')['clients_ref.create']) {
            $clients_ar = json_decode($request->contacts);
            $filials_ar = json_decode($request->filials);

            if (!$request->name_client) {
                return redirect()->back()->with('error', 'Укажите наименование клиента');
            }
            if (count($clients_ar) == 0) {
                return redirect()->back()->with('error', 'Укажите контакты');
            }
            //$success = false;
            $cur_user = Sentinel::getUser();

            DB::transaction(function () use ($request, $clients_ar, $filials_ar, $cur_user) {
                //dd($filials_ar);
                $client = new Clients_reference();
                $client->name = $request->name_client;
                $client->city_id = $request->client_city;
                $client->save();

                foreach ($filials_ar as $f) {
                    try {
                        $filials = new Client_filial();
                        $filials->client_id = $client->id;
                        $filials->name = $f->name;
                        $filials->city_id = $f->city_id;
                        $filials->save();

                        $this->success = true;
                    } catch (\Exception $e) {
                        DB::rollback();
                        $this->success = false;
                    }
                }

                foreach ($clients_ar as $ca) {
                    try {
                        $post = Posts_reference::where('name', '=', $ca->post)->firstOrFail();
                        $contact = new Contact();
                        $contact->first_name = $ca->name;
                        $contact->last_name = $ca->surname;
                        $contact->patronymic = $ca->patronymic;
                        $contact->post_id = $post->id;
                        $contact->phone = $ca->phone;
                        $contact->email = $ca->email;
                        $contact->client_id = $client->id;
                        $contact->user_id = $cur_user->id;
                        $contact->save();
                        $this->success = true;
                    } catch (\Exception $e) {
                        DB::rollback();
                        $this->success = false;
                    }
                }
            });

            if ($this->success) {
                return redirect('/manager/projects/add')->with('success', 'Клиент успешно создан');
            } else {
                return redirect('/manager/projects/add')->with('error', 'Произошла ошибка при создании клиента');
            }
        } else {
            abort(503);
        }
    }

    static public function getClientProjects($cid) {
        $projects = Project::where('client_id', '=', $cid)
            ->whereNull('deleted_at')
            ->get();

        return $projects;
    }
}
