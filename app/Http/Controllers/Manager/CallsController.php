<?php

namespace App\Http\Controllers\Manager;

use App\Comment_task;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Manager\Projects2Controller as Projects;
use App\Http\Controllers\Manager\ClientsController as Clients;
use App\Http\Controllers\Manager\SettingsController as Settings;
use App\Http\Controllers\Manager\CitysController as Citys;
use App\Http\Controllers\Manager\UserController as Users;
use App\Http\Controllers\Manager\TasksController as Tasks;
use App\Http\Controllers\Manager\CommentController as Comments;
use App\Http\Controllers\Manager\ContactController as Contacts;
use Smadia\LaravelGoogleDrive\Facades\LaravelGoogleDrive as LGD;
use App\Project_task;
use App\Project;
use App\Project_contact;
use App\Contact;
use App\Project_task_contact;
use App\Module_rule;
use App\Comment;
use App\Request_comment;
use App\Request_answer;
use App\Request_file;
use phpDocumentor\Reflection\Types\Integer;
use Sentinel;
use App\Callback_other_task;
use App\Callback_close_question;
use App\Request_question;
use App\Callback_work_question;
use App\Callback_work_answer;
use Carbon\Carbon;
use DB;

class CallsController extends Controller
{
    public function index() {
        if (session('perm')['calls.view']) {
            $p_tasks = Project_task_contact::where('status', '!=', NULL)
                ->where('callbackstatus_ref_id', '=', 1)
                ->orWhere('callbackstatus_ref_id', '=', 5)
                ->get();

            if (count($p_tasks) > 0) {
                foreach ($p_tasks as $pt) {
                    $project_task = Project_task::find($pt->project_task_id);
                    $name = Module_rule::find($project_task->module_rule_id);
                    $project = Projects::getProjectById($pt->project_id);
                    $client = Clients::getClientById($project->client_id);
                    $city = Citys::getCityById($client->city_id);
                    $user = User::getUserById($pt->user_id);
                    //dd($client);

                    $tasks[] = [
                        'id' => $project_task->id,
                        'prev_contact' => substr($pt->prev_contact, 0, -3),
                        'name' => $name->task,
                        'client' => $client,
                        'project' => $project,
                        'city' => $city,
                        'user' => $user,
                        'next_contact' => substr($pt->next_contact, 0, -3)
                    ];
                }
            } else {
                $tasks = '';
            }


            $frequency_feedback = Settings::getSettings('frequency_feedback');
            $cur_date = date("d").'-'.date("m").'-'.date("Y");
            $projects = Project::whereNotIn('callback_status', [6])
                ->orWhereNull('callback_status')
                ->get();

            //$feedback_projects[] = '';
            if (count($projects) > 0) {
                foreach ($projects as $p) {
                    $frequency_feedback_date = date('d-m-Y', strtotime($p->start_date . ' +' . (int)$frequency_feedback->value . ' day'));
                    if (strtotime($frequency_feedback_date) == strtotime($cur_date)) {
                        $feedback_projects_ar[] = $p;
                    };
                }
            }

            if (isset($feedback_projects_ar)) {
                foreach ($feedback_projects_ar as $fp) {
                    $project_task_contact = Project_task_contact::where('project_id', '=', $fp->id)
                        ->whereNull('deleted_at')
                        ->orderBy('prev_contact', 'desc')
                        ->limit(1)
                        ->get();

                    if (count($project_task_contact) > 0) {
                        foreach ($project_task_contact as $ptc) {
                            $project_task = Project_task::where('project_id', '=', $ptc->project_id)
                                ->where('id', '=', $ptc->project_task_id)
                                ->whereNull('deleted_at')
                                ->get();

                            foreach ($project_task as $pt) {
                                $mr = Module_rule::find($pt->module_rule_id);
                                $project_contact = Project_contact::where('project_id', '=', $fp->id)
                                    ->whereNull('deleted_at')
                                    ->get();
                                $contact = Contact::find($project_contact[0]->contact_id);
                                $client = Clients::getClientById($contact->client_id);
                                $city = Citys::getCityById($client->city_id);

                                $feedback_projects[] = [
                                    'id' => $fp->id,
                                    'prev_contact' => substr($ptc->prev_contact, 0, -3),
                                    'callback_prev_contact' => $fp->callback_prev_contact,
                                    'p_name' => $fp->name,
                                    'task' => $mr->task,
                                    'client' => $client->name,
                                    'city' => $city
                                ];
                            }
                        }
                    } else {
                        $feedback_projects[] = '';
                    }
                }
            } else {
                $feedback_projects[] = '';
            }
            //dd($feedback_projects);

            $other_task = Callback_other_task::whereNull('deleted_at')->get();
            if (count($other_task) > 0) {
                foreach ($other_task as $ot) {
                    $client = Clients::getClientById($ot->client_id);
                    //dd($ot->project_id);
                    $project = Projects::getProjectById($ot->project_id);
                    $city = Citys::getCityById($client->city_id);
                    $user = User::getUserById($project->user_id);
                    $comment = Comment::where('project_id', '=', $project->id)
                        ->where('mail_client', '=', 1)
                        ->whereNull('deleted_at')
                        ->first();

                    $other_ar[] = [
                        'id' => $ot->id,
                        'client' => $client->name,
                        'project' => $project->name,
                        'city' => $city,
                        'type' => $ot->type,
                        'user' => $user->first_name.' '.$user->last_name,
                        'comment' => strip_tags($comment['text'])
                    ];
                }
            } else {
                $other_ar[] = '';
            }
            //dd($tasks);

            return view('/Manager/calls/calls', [
                'tasks' => $tasks,
                'feedback_projects' => $feedback_projects,
                'other_task' => $other_ar
            ]);
        } else {
            abort(503);
        }
    }

    public function postRequest(Request $request) {
        // phpinfo();
        if ($request->optionsRadios == "1") {
            dd('Ok');
            return;
        }  
        if ($request->optionsRadios == "2") {
            
            $project = Project::find($request->id);
            $project->request = 1;
            $project->save();

            $comment = new Request_comment();
            $comment->project_id = $request->id;
            $comment->comment = $request->comment;
            $comment->save();

            $question = json_decode($request->request_q);
            
            foreach ($question as $q) {
                $answer = new Request_answer();
                $answer->project_id = $request->id;
                $answer->question_id = $q->q_id;
                $answer->answer = $q->answer;
                $answer->save();
            }

            // $max_size = (int)ini_get('upload_max_filesize') * 1000;
            // $all_ext = implode(',', $this->allExtensions());

            $r = rand(100, 1000);
            $file = $request->file('file');
            $name = explode('.', $file->getClientOriginalName());
            $ext = $file->getClientOriginalExtension();
            // $type = $this->getType($ext);
            $rand = $r.'_'.$file->getClientOriginalName();
            // $rand2 = $r.'_'.$name[0];
            // dd($file->getClientOriginalName());

            $upload = LGD::put($rand, file_get_contents($file->getRealPath()));
            // dd($upload);
            // sleep(10);
            if ($upload) {
                
                // print_r(file_put_contents($upload->contents));
                $a = $upload->getAllProperties();
                
                $request_file = new Request_file();
                $request_file->project_id = $request->id;
                $request_file->file = $file->getClientOriginalName();
                $request_file->g_file = $a['basename'];
                $request_file->save();
                // dd('Ok');
                return response()->json([
                    'status' => true,
                    'file_name' => $rand,
                    'ext' => $ext,
                    'g_name' => $a['basename'],
                    'o_name' => $file->getClientOriginalName(),
                    'name' => $rand,
                ]);
            }

            return response()->json(['status' => false]);
        }
    }

    public function getClose() {
        $projects = Project::where('callback_status', '=', 6)
            ->get();

        //$close_projects[] = '';

        if ($projects != '') {
            foreach ($projects as $p) {
                $p_client = Clients::getClientById($p->client_id);
                $p_city = Citys::getCityById($p_client->city_id);
                $comment = Comments::getProjectCloseComment($p->id);

                $close_projects[] = [
                    'id' => $p->id,
                    'p_name' => $p->name,
                    'client' => $p_client->name,
                    'city' => $p_city,
                    'comment' => $comment->text,
                    'done_datetime' => $p->done_datetime
                ];
            }
        }

        return response()->json($close_projects);
    }

    public function getQuality() {
        $date = Carbon::now()->subWeeks(2)->format("Y-m-d");
        // print_r($date);
        $projects = DB::select("select distinct `p`.`id` as `p_id`, `p`.`name` as `p_name`,
            `client`.`name` as `client_name`, `city`.`name` as `city_name`, 
            DATE_FORMAT(`p`.`quality_date`, '%d.%m.%Y %H:%i') as `created_at`
            from `projects` `p`
            left join `callback_work_answers` `answer` on `answer`.`project_id` = `p`.`id`
            left join `clients_references` `client` on `client`.`id` = `p`.`client_id`
            left join `city_references` `city` on `city`.`id` = `client`.`city_id`
            left join `comments` `c` on `c`.`project_id` = `p`.`id`
            where `p`.`done` = 0 and
            `p`.`start_date` <= '".$date."' and
            `c`.`text` IS NOT NULL and
            `p`.`deleted_at` is null");

        // $date2 = Carbon::now()->format('d.m.Y');
        // $projects2 = array();
        // foreach($projects as $p) {
        //     if ($p->created_at == NULL || $p->created_at == $date2) {
        //         $projects2[] = [
        //             'p_id' => $p->p_id,
        //             'p_name' => $p->p_name,
        //             'client_name' => $p->client_name,
        //             'city_name' => $p->city_name,
        //             'created_at' => $p->created_at,
        //         ];
        //     };
        // }
        // print_r($projects2);

        return response()->json($projects);
    }

    public function getRequestFilter(Request $request) {
        $data = json_decode($request->data);
        $date = Carbon::now()->subMonths(2)->format("Y-m-d H:i:s");
        switch ($data->param) {
            case 'all': 
                $projects = DB::select("select distinct `p`.`id` as `p_id`, `p`.`name` as `p_name`, `p`.`request`,
                    `client`.`name` as `client_name`, `city`.`name` as `city_name`, 
                    DATE_FORMAT(`p`.`done_datetime`, '%d.%m.%Y %H:%i') as `done_datetime`
                    from `projects` `p`
                    left join `callback_work_answers` `answer` on `answer`.`project_id` = `p`.`id`
                    left join `clients_references` `client` on `client`.`id` = `p`.`client_id`
                    left join `city_references` `city` on `city`.`id` = `client`.`city_id`
                    where `p`.`done` = 1 and
                    `p`.`deleted_at` is null and
                    `p`.`done_datetime` <= '".$date."'");
                break;
            case 'done': 
                $projects = DB::select("select distinct `p`.`id` as `p_id`, `p`.`name` as `p_name`, `p`.`request`,
                    `client`.`name` as `client_name`, `city`.`name` as `city_name`, 
                    DATE_FORMAT(`p`.`done_datetime`, '%d.%m.%Y %H:%i') as `done_datetime`
                    from `projects` `p`
                    left join `callback_work_answers` `answer` on `answer`.`project_id` = `p`.`id`
                    left join `clients_references` `client` on `client`.`id` = `p`.`client_id`
                    left join `city_references` `city` on `city`.`id` = `client`.`city_id`
                    where `p`.`done` = 1 and
                    `p`.`request` = 1 and
                    `p`.`deleted_at` is null and
                    `p`.`done_datetime` <= '".$date."'");
                break;
            case 'other': 
                $projects = DB::select("select distinct `p`.`id` as `p_id`, `p`.`name` as `p_name`, `p`.`request`,
                    `client`.`name` as `client_name`, `city`.`name` as `city_name`, 
                    DATE_FORMAT(`p`.`done_datetime`, '%d.%m.%Y %H:%i') as `done_datetime`
                    from `projects` `p`
                    left join `callback_work_answers` `answer` on `answer`.`project_id` = `p`.`id`
                    left join `clients_references` `client` on `client`.`id` = `p`.`client_id`
                    left join `city_references` `city` on `city`.`id` = `client`.`city_id`
                    where `p`.`done` = 1 and
                    `p`.`request` IS NULL and
                    `p`.`deleted_at` is null and
                    `p`.`done_datetime` <= '".$date."'");
                break;
        }
        return response()->json($projects);
    }

    public function getRequest() {
        $date = Carbon::now()->subMonths(2)->format("Y-m-d H:i:s");
        
        $projects = DB::select("select distinct `p`.`id` as `p_id`, `p`.`name` as `p_name`, `p`.`request`,
                    `client`.`name` as `client_name`, `city`.`name` as `city_name`, 
                    DATE_FORMAT(`p`.`done_datetime`, '%d.%m.%Y %H:%i') as `done_datetime`
                    from `projects` `p`
                    left join `callback_work_answers` `answer` on `answer`.`project_id` = `p`.`id`
                    left join `clients_references` `client` on `client`.`id` = `p`.`client_id`
                    left join `city_references` `city` on `city`.`id` = `client`.`city_id`
                    where `p`.`done` = 1 and
                    `p`.`request` IS NULL and
                    `p`.`deleted_at` is null and
                    `p`.`done_datetime` <= '".$date."'");

        // $date = Carbon::now()->format('d.m.Y');
        // $projects2 = array();
        // foreach($projects as $p) {
        //     if ($p->created_at == NULL || $p->created_at == $date) {
        //         $projects2[] = [
        //             'p_id' => $p->p_id,
        //             'p_name' => $p->p_name,
        //             'client_name' => $p->client_name,
        //             'city_name' => $p->city_name,
        //             'created_at' => $p->created_at,
        //         ];
        //     };
        // }
        // print_r($projects2);

        return response()->json($projects);
    }

    public function getQualityProject($id) {
        $project = Project::find($id);
        $p_client = Clients::getClientById($project->client_id);
        $comment = Comments::getProjectCloseComment($project->id);
        $main_contact = Contacts::getProjectMainContact($project->id);
        $user = Users::getUserById($project->user_id);
        $comments = Comments::getCommentsByProjectId($project->id);
        $close_question = Callback_work_question::whereNull('deleted_at')
            ->get();
            
        if ($comment) {
            $comment2 = strip_tags($comment->text);
        } else {
            $comment2 = '';
        };

        return response()->json([
            'project' => $project,
            'client' => $p_client,
            'comment_close' => $comment2,
            'main_contact' => $main_contact,
            'user' => $user,
            'comments' => $comments,
            'close_question' => $close_question
        ]);
    }

    public function postEditQualityProject(Request $request, $id) {
        $data = json_decode($request->data);
        $date =  explode(' ', date("Y-m-d H:i:s"));
        $cur_user = Sentinel::getUser();
        $quality_date = Carbon::now()->addDay(14)->format("Y-m-d H:i:s");
        // print_r($quality_date);
        // die;
        switch ($data->optionsRadios) {
            case '1':
                try {
                    $project = Project::find($id);
                    // $project->quality_date = date("Y-m-d H:i:s");
                    $project->quality_date = $quality_date;
                    $project->quality_prev_contact = date("Y-m-d H:i:s");
                    $project->save();

                    $this->success = true;
                } catch (\Exception $e) {
                    $this->success = false;
                }

                try {
                    $comment = new Comment();
                    $comment->project_id = $id;
                    $comment->user_id = $cur_user->id;
                    $comment->for_client = NULL;
                    $comment->date = $date[0];
                    $comment->time = $date[1];
                    $comment->status = 1;
                    $comment->text = $data->comment;
                    $comment->save();

                    $this->success = true;
                } catch (\Exception $e) {
                    $this->success = false;
                }

                if ($this->success) {
                    return response()->json(['status' => 'Ok']);
                } else {
                    return response()->json(['status' => 'Error']);
                }
                break;
            case '2':
                $close_q_ar = $data->close;
                $date =  explode(' ', date("Y-m-d H:i:s"));
                $cur_user = Sentinel::getUser();

                try {
                    $project = Project::find($id);
                    // $project->quality_date = date("Y-m-d H:i:s");
                    $project->quality_date = $quality_date;
                    $project->quality_prev_contact = date("Y-m-d H:i:s");
                    $project->save();

                    $this->success = true;
                } catch (\Exception $e) {
                    $this->success = false;
                }

                foreach ($close_q_ar as $cq) {
                    try {
                        $close = new Callback_work_answer();
                        $close->question_id = $cq->q_id;
                        $close->answer = $cq->answer;
                        $close->project_id = $id;
                        $close->save();

                        $this->success = true;
                    } catch (\Exception $e) {
                        $this->success = false;
                    }
                }

                try {
                    $comment = new Comment();
                    $comment->project_id = $id;
                    $comment->user_id = $cur_user->id;
                    $comment->for_client = NULL;
                    $comment->date = $date[0];
                    $comment->time = $date[1];
                    $comment->status = 1;
                    $comment->text = $data->comment;
                    $comment->save();

                    $this->success = true;
                } catch (\Exception $e) {
                    $this->success = false;
                }

                if ($this->success) {
                    return response()->json(['status' => 'Ok']);
                } else {
                    return response()->json(['status' => 'Error']);
                }
                break;
        }
    }

    public function getCloseProject($id) {
        $project = Project::find($id);
        $p_client = Clients::getClientById($project->client_id);
        $comment = Comments::getProjectCloseComment($project->id);
        $main_contact = Contacts::getProjectMainContact($project->id);
        $user = Users::getUserById($project->user_id);
        $comments = Comments::getCommentsByProjectId($project->id);
        $close_question = Callback_close_question::whereNull('deleted_at')
            ->get();

        return response()->json([
            'project' => $project,
            'client' => $p_client,
            'comment_close' => $comment->text,
            'main_contact' => $main_contact,
            'user' => $user,
            'comments' => $comments,
            'close_question' => $close_question
        ]);
    }

    public function getRequestProject($id) {
        $project = Project::find($id);
        $p_client = Clients::getClientById($project->client_id);
        $comment = Comments::getProjectCloseComment($project->id);
        $main_contact = Contacts::getProjectMainContact($project->id);
        $user = Users::getUserById($project->user_id);
        $comments = Comments::getCommentsByProjectId($project->id);
        $request_question = Request_question::whereNull('deleted_at')->get();

        return response()->json([
            'project' => $project,
            'client' => $p_client,
            'comment_close' => strip_tags($comment->text),
            'main_contact' => $main_contact,
            'user' => $user,
            'comments' => $comments,
            'request_question' => $request_question
        ]);
    }

    public function getRequestProjectView($id) {
        $project = Project::find($id);
        $p_client = Clients::getClientById($project->client_id);
        $comment = Comments::getProjectCloseComment($project->id);
        $main_contact = Contacts::getProjectMainContact($project->id);
        $user = Users::getUserById($project->user_id);
        $comments = Comments::getCommentsByProjectId($project->id);
        $request_question = DB::select("select `rq`.`id`, `rq`.`question`, `ra`.`answer`
            from `request_questions` `rq`
            left join `request_answers` `ra` on `ra`.`question_id` = `rq`.`id`
            where
            `rq`.`deleted_at` IS NULL and
            `ra`.`project_id` = ".$id);
        $request_comment = Request_comment::where('project_id', '=', $id)
            ->get();
        $audio = Request_file::where('project_id', '=', $id)
            ->get();

        return response()->json([
            'project' => $project,
            'client' => $p_client,
            'comment_close' => strip_tags($comment->text),
            'main_contact' => $main_contact,
            'user' => $user,
            'comments' => $comments,
            'request_question' => $request_question,
            'request_comment' => $request_comment,
            'audio' => $audio
        ]);
    }

    public function getFrequency() {
        $frequency_feedback = Settings::getSettings('frequency_feedback');
        $cur_date = date("d").'-'.date("m").'-'.date("Y");
        $projects = Project::whereNotIn('callback_status', [6])
            ->orWhereNull('callback_status')
            ->get();

        //$feedback_projects[] = '';
        if (count($projects) > 0) {
            foreach ($projects as $p) {
                $frequency_feedback_date = date('d-m-Y', strtotime($p->start_date . ' +' . (int)$frequency_feedback->value . ' day'));
                if (strtotime($frequency_feedback_date) == strtotime($cur_date)) {
                    $feedback_projects_ar[] = $p;
                };
            }
        }

        if (isset($feedback_projects_ar)) {
            foreach ($feedback_projects_ar as $fp) {
                $project_task_contact = Project_task_contact::where('project_id', '=', $fp->id)
                    ->whereNull('deleted_at')
                    ->orderBy('prev_contact', 'desc')
                    ->limit(1)
                    ->get();

                if (count($project_task_contact) > 0) {
                    foreach ($project_task_contact as $ptc) {
                        $project_task = Project_task::where('project_id', '=', $ptc->project_id)
                            ->where('id', '=', $ptc->project_task_id)
                            ->whereNull('deleted_at')
                            ->get();

                        foreach ($project_task as $pt) {
                            $mr = Module_rule::find($pt->module_rule_id);
                            $project_contact = Project_contact::where('project_id', '=', $fp->id)
                                ->whereNull('deleted_at')
                                ->get();
                            $contact = Contact::find($project_contact[0]->contact_id);
                            $client = Clients::getClientById($contact->client_id);
                            $city = Citys::getCityById($client->city_id);

                            $feedback_projects[] = [
                                'id' => $fp->id,
                                'prev_contact' => substr($ptc->prev_contact, 0, -3),
                                'callback_prev_contact' => $fp->callback_prev_contact,
                                'p_name' => $fp->name,
                                'task' => $mr->task,
                                'client' => $client->name,
                                'city' => $city
                            ];
                        }
                    }
                } else {
                    $feedback_projects[] = '';
                }
            }
        } else {
            $feedback_projects[] = '';
        }

        return response()->json($feedback_projects);
    }

    public function getTask() {
//        $p_tasks = Project_task_contact::where('status', '!=', NULL)
//            ->where('callbackstatus_ref_id', '=', 1)
//            ->where('next_for_cont', 'like', Carbon::today()->format('Y-m-d').'%')
////            ->orWhere('callbackstatus_ref_id', '=', 5)
//            ->orderBy('next_for_cont', 'DESC')
//            ->get();

        $p_tasks = DB::select("SELECT * FROM `project_task_contacts` WHERE `next_for_cont` <= '".Carbon::today()->format('Y-m-d')."' AND `callbackstatus_ref_id` = '1' AND `status` != 'NULL'");

        if (count($p_tasks) > 0) {
            foreach ($p_tasks as $pt) {
                $project_task = Project_task::find($pt->project_task_id);
                $name = Module_rule::find($project_task->module_rule_id);
                $project = Projects::getProjectById($pt->project_id);
                $client = Clients::getClientById($project->client_id);
                $city = Citys::getCityById($client->city_id);
                $user = Users::getUserById($pt->user_id);


                $tasks[] = [
                    'id' => $project_task->id,
                    'prev_contact' => substr($pt->prev_contact, 0, -3),
//                    'name' => $name->task,
                    'client' => $client,
                    'project' => $project,
                    'city' => $city,
                    'user' => $user,
                    'next_contact' => substr($pt->next_for_cont, 0, -3)
                ];
            }
        } else {
            $tasks = '';
        }

        return response()->json($tasks);
    }

    public function edit($id) {
        $task = Tasks::getTaskById($id);
        $project = Projects::getProjectByTaskId($id);
        $client = Clients::getClientById($project->client_id);
        $comments = Comments::getCommentsByProjectId($project->id);
        $user = User::getUserById($project->user_id);
        $users = User::getAllUsers();
        $contacts = Contacts::getProjectContacts($project->id);
        //dd($contacts);

        $cur_date = date("j").'.'.date("n").'.'.date("Y");
        $cur_date_val = date("Y").'-'.date("m").'-'.date("d");
        $tomorrow = date('Y-m-d', strtotime($cur_date . ' +1 day'));
        $tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +1 day'));
        $plus_one_day = date('Y-m-d', strtotime($cur_date . ' +2 day'));
        $plus_one_day_val = date('Y-m-d', strtotime($cur_date . ' +2 day'));
        $plus_two_day = date('d.m.Y', strtotime($cur_date . ' +3 day'));
        $plus_two_day_val = date('Y-m-d', strtotime($cur_date . ' +3 day'));
        $plus_three_day = date('d.m.Y', strtotime($cur_date . ' +4 day'));
        $plus_three_day_val = date('Y-m-d', strtotime($cur_date . ' +4 day'));

        return view('/Manager/calls/edit', [
            'task' => $task,
            'client' => $client,
            'comments' => $comments,
            'project' => $project,
            'user' => $user,
            'users' => $users,
            'contacts' => $contacts,
            'cur_date' => $cur_date,
            'cur_date_val' => $cur_date_val,
            'tomorrow' => $tomorrow,
            'tomorrow_val' => $tomorrow_val,
            'plus_one_day' => $plus_one_day,
            'plus_one_day_val' => $plus_one_day_val,
            'plus_two_day' => $plus_two_day,
            'plus_two_day_val' => $plus_two_day_val,
            'plus_three_day' => $plus_three_day,
            'plus_three_day_val' => $plus_three_day_val,
        ]);
    }

    public function getCall($id) {
        $taskContact = Project_task_contact::find($id);
        $task = $taskContact->projectTask;
        $project = $task->project;
        $client = Clients::getClientById($project->client_id);
        $comments = Comments::getCommentsByProjectId($project->id);
        $user = Users::getUserById($project->user_id);
        $users = Users::getImplementers();
        $contacts = Contacts::getProjectContacts($project->id, true);
        //dd($contacts);

        $cur_date = date("j").'.'.date("n").'.'.date("Y");
        $cur_date_val = date("Y").'-'.date("m").'-'.date("d");
        $tomorrow = date('Y-m-d', strtotime($cur_date . ' +1 day'));
        $tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +1 day'));
        $plus_one_day = date('Y-m-d', strtotime($cur_date . ' +2 day'));
        $plus_one_day_val = date('Y-m-d', strtotime($cur_date . ' +2 day'));
        $plus_two_day = date('d.m.Y', strtotime($cur_date . ' +3 day'));
        $plus_two_day_val = date('Y-m-d', strtotime($cur_date . ' +3 day'));
        $plus_three_day = date('d.m.Y', strtotime($cur_date . ' +4 day'));
        $plus_three_day_val = date('Y-m-d', strtotime($cur_date . ' +4 day'));

        return response()->json([
            'task' => $task,
            'client' => $client,
            'comments' => $comments,
            'project' => $project,
            'user' => $user,
            'users' => $users,
            'contacts' => $contacts,
            'cur_date' => $cur_date,
            'cur_date_val' => $cur_date_val,
            'tomorrow' => $tomorrow,
            'tomorrow_val' => $tomorrow_val,
            'plus_one_day' => $plus_one_day,
            'plus_one_day_val' => $plus_one_day_val,
            'plus_two_day' => $plus_two_day,
            'plus_two_day_val' => $plus_two_day_val,
            'plus_three_day' => $plus_three_day,
            'plus_three_day_val' => $plus_three_day_val,
        ]);
    }

    public function postEdit($id, Request $request) {
        $data = json_decode($request->data);
        $date =  explode(' ', date("Y-m-d H:i:s"));
//        $project = Projects::getProjectByTaskId($id);
        $cur_user = Sentinel::getUser();

        $projectTaskContact = Project_task_contact::find($id);
        $project = $projectTaskContact->projectTask->project;

        switch ($data->optionsRadios) {
            case 1:
                try {
                    $project = Project::find($project->id);
                    $project->callback_status = 1;
                    $project->save();

                    $this->success = true;
                } catch (\Exception $e) {
                    $this->success = false;
                }

                try {
                    $p_task = Project_task_contact::where('project_task_id', '=', $id)
                        ->whereNull('deleted_at')
                        ->first();

                    $p_task->prev_contact = date("Y-m-d H:i:s");
                    $p_task->save();

                    $this->success = true;
                } catch (\Exception $e) {
                    $this->success = false;
                }

                try {
                    $comment = new Comment();
                    $comment->project_id = $project->id;
                    $comment->user_id = $cur_user->id;
                    $comment->for_client = NULL;
                    $comment->date = $date[0];
                    $comment->time = $date[1];
                    $comment->text = $data->comment;
                    $comment->save();

                    $this->success = true;
                } catch (\Exception $e) {
                    $this->success = false;
                }

                if ($this->success) {
                    return response()->json(['status' => 'Ok']);
                } else {
                    return response()->json(['status' => 'Error']);
                }
                break;
            case 2:
                //dd($request->all());

                try {
//                    $p_task_old = Project_task_contact::where('project_task_id', '=', $id)
//                        ->whereNull('deleted_at')
//                        ->first()->toArray();

                    $nextDate = new Carbon($data->next_date2 . ' ' . $data->next_time2);

                    $newProjectTask = Project_task::create([
                        'project_id' => $project->id,
                        'user_id' => $data->user_id,
                        'observer_id' => $cur_user->id,
                        'tasksstatus_ref_id' => 1,
                    ]);

                    $newPtc = Project_task_contact::create([
                        'project_id' => $projectTaskContact->project_id,
                        'project_task_id' => $newProjectTask->id,
                        'observer_id' =>  $cur_user->id,
                        'prev_contact' => date("Y-m-d H:i:s"),
                        'next_contact' => $nextDate->format("Y-m-d H:i:s"),
                        'end' => $nextDate->add((new Carbon('00:00'))->diff(new Carbon($data->duration))),
                        'user_id' => $data->user_id,
                        'duration' => $data->duration,
                        'date_production' => date("Y-m-d H:i:s"),
                        'status' => NULL,
                        'callbackstatus_ref_id' => NULL,
                        'contact_id' => $data->contact_id,
                    ]);


                    $projectTaskContact->update(['callbackstatus_ref_id' => null]);
//                    $projectTaskContact->projectTask->update(['tasksstatus_ref_id' => 3]);

//                    $p_task->prev_contact = date("Y-m-d H:i:s");
//                    $p_task->next_contact = $data->next_date2.' '.$data->next_time2;
//                    $p_task->user_id = $data->user_id;
//                    $p_task->duration = $data->duration;
//                    $p_task->status = NULL;
//                    $p_task->callbackstatus_ref_id = NULL;
//                    $p_task->save();

                    Comment_task::create([
                        'project_id' => $project->id,
                        'user_id' => $cur_user->id,
                        'task_id' => $newProjectTask->id,
                        'date' => $date[0],
                        'time' => $date[1],
                        'text' => $data->comment,
                    ]);

                    $this->success = true;
                } catch (\Exception $e) {
                    $this->success = false;
                }

                try {

//                    $comment = new Comment();
//                    $comment->project_id = $project->id;
//                    $comment->user_id = $cur_user->id;
//                    $comment->for_client = NULL;
//                    $comment->date = $date[0];
//                    $comment->time = $date[1];
//                    $comment->text = $data->comment;
//                    $comment->save();

                    $this->success = true;
                } catch (\Exception $e) {
                    $this->success = false;
                }

                if ($this->success) {
                    return response()->json(['status' => 'Ok']);
                } else {
                    return response()->json(['status' => 'Error']);
                }
                break;
            case 3:
                try {
                    $p_task = Project_task_contact::where('project_task_id', '=', $id)
                        ->whereNull('deleted_at')
                        ->first();

                    $p_task->prev_contact = date("Y-m-d H:i:s");
                    $p_task->next_for_cont = $data->next_date.' '.$data->next_time;
                    $p_task->save();

                    $this->success = true;
                } catch (\Exception $e) {
                    $this->success = false;
                }

                try {
                    $comment = new Comment();
                    $comment->project_id = $project->id;
                    $comment->user_id = $cur_user->id;
                    $comment->for_client = NULL;
                    $comment->date = $date[0];
                    $comment->time = $date[1];
                    $comment->text = $data->comment;
                    $comment->save();

                    $this->success = true;
                } catch (\Exception $e) {
                    $this->success = false;
                }

                if ($this->success) {
                    return response()->json(['status' => 'Ok']);
                } else {
                    return response()->json(['status' => 'Error']);
                }

                break;
        }
    }

    public function createClientFromMail(Request $request) {
        dd($request->all());
    }
}
