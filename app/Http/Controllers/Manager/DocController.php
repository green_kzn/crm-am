<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Manager\DocumentsController as Documents;
use App\Http\Controllers\Manager\ClientsController as Clients;
use App\Http\Controllers\Manager\CitysController as Citys;
use App\Projects_doc;
use App\Project;
use App\Doc_types_reference;
use App\Documents_reference;

class DocController extends Controller
{
    public function index() {
        if (session('perm')['doc.view']) {
            $doc_types = Documents::getDocName();
            $projects_doc = Projects_doc::whereNull('deleted_at')
                ->get();

            if (count($projects_doc) > 0) {
                foreach ($projects_doc as $pd) {
                    $project_name = Project::find($pd->project_id);
                    $client = Clients::getClientById($project_name->client_id);
                    $city = Citys::getCityById($client->city_id);
                    $doc_type = Doc_types_reference::find($pd->type_id);
                    $doc = Documents_reference::find($pd->doc_id);

                    $finish_date = date('Y-m-d', strtotime($project_name->start_date . ' +'.round($doc->days_for_execute).' day'));

                    $docs[] = [
                        'id' => $pd->id,
                        'project_name' => $project_name->name,
                        'client' => $client->name,
                        'city' => $city,
                        'doc_type' => $doc_type->name,
                        'doc' => $doc->name,
                        'start_date' => $project_name->start_date,
                        'finish_date' => $finish_date
                    ];
                }
            } else {
                $docs = '';
            }
            //dd($docs);

            return view('Manager.doc.doc', [
                'doc_type' =>  $doc_types,
                'docs' => $docs
            ]);
        } else {
            abort(503);
        }
    }

    public function edit($id) {
        $projects_doc = Projects_doc::find($id);
        $project_name = Project::find($projects_doc->project_id);
        $client = Clients::getClientById($project_name->client_id);
        $doc_type = Doc_types_reference::find($projects_doc->type_id);
        $doc = Documents_reference::find($projects_doc->doc_id);


        return view('Manager.doc.edit', [
            'project_name' => $project_name->name,
            'client' => $client->name,
            'doc_type' => $doc_type->name,
            'doc' => $doc->name,
            'doc_type_id' => $projects_doc->type_id
        ]);
    }

    public function sort(Request $request) {
        if ($request->ajax()) {
            switch ($request->type) {
                case 'input':
                    $projects_doc = Projects_doc::where('type_id', '=', 1)
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($projects_doc) > 0) {
                        foreach ($projects_doc as $pd) {
                            $project_name = Project::find($pd->project_id);
                            $client = Clients::getClientById($project_name->client_id);
                            $city = Citys::getCityById($client->city_id);
                            $doc_type = Doc_types_reference::find($pd->type_id);
                            $doc = Documents_reference::find($pd->doc_id);

                            $finish_date = date('Y-m-d', strtotime($project_name->start_date . ' +'.round($doc->days_for_execute).' day'));

                            $docs[] = [
                                'id' => $pd->id,
                                'project_name' => $project_name->name,
                                'client' => $client->name,
                                'city' => $city,
                                'doc_type' => $doc_type->name,
                                'doc' => $doc->name,
                                'start_date' => $project_name->start_date,
                                'finish_date' => $finish_date
                            ];
                        }
                    } else {
                        $docs = '';
                    }

                    break;
                case 'output':
                    $projects_doc = Projects_doc::where('type_id', '=', 2)
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($projects_doc) > 0) {
                        foreach ($projects_doc as $pd) {
                            $project_name = Project::find($pd->project_id);
                            $client = Clients::getClientById($project_name->client_id);
                            $city = Citys::getCityById($client->city_id);
                            $doc_type = Doc_types_reference::find($pd->type_id);
                            $doc = Documents_reference::find($pd->doc_id);

                            $finish_date = date('Y-m-d', strtotime($project_name->start_date . ' +'.round($doc->days_for_execute).' day'));

                            $docs[] = [
                                'id' => $pd->id,
                                'project_name' => $project_name->name,
                                'client' => $client->name,
                                'city' => $city,
                                'doc_type' => $doc_type->name,
                                'doc' => $doc->name,
                                'start_date' => $project_name->start_date,
                                'finish_date' => $finish_date
                            ];
                        }
                    } else {
                        $docs = '';
                    }

                    break;
                case 'all':
                    $projects_doc = Projects_doc::whereNull('deleted_at')
                        ->get();

                    if (count($projects_doc) > 0) {
                        foreach ($projects_doc as $pd) {
                            $project_name = Project::find($pd->project_id);
                            $client = Clients::getClientById($project_name->client_id);
                            $city = Citys::getCityById($client->city_id);
                            $doc_type = Doc_types_reference::find($pd->type_id);
                            $doc = Documents_reference::find($pd->doc_id);

                            $finish_date = date('Y-m-d', strtotime($project_name->start_date . ' +'.round($doc->days_for_execute).' day'));

                            $docs[] = [
                                'id' => $pd->id,
                                'project_name' => $project_name->name,
                                'client' => $client->name,
                                'city' => $city,
                                'doc_type' => $doc_type->name,
                                'doc' => $doc->name,
                                'start_date' => $project_name->start_date,
                                'finish_date' => $finish_date
                            ];
                        }
                    } else {
                        $docs = '';
                    }

                    break;
            }

            return response()->json($docs);
        }
    }

    public function sortType(Request $request) {
        if ($request->ajax()) {
            $doc_id = Documents_reference::where('name', 'like', $request->type)
                ->whereNull('deleted_at')
                ->first();

            $projects_doc = Projects_doc::where('doc_id', '=', $doc_id->id)
                ->whereNull('deleted_at')
                ->get();

            if (count($projects_doc) > 0) {
                foreach ($projects_doc as $pd) {
                    $project_name = Project::find($pd->project_id);
                    $client = Clients::getClientById($project_name->client_id);
                    $city = Citys::getCityById($client->city_id);
                    $doc_type = Doc_types_reference::find($pd->type_id);
                    $doc = Documents_reference::find($pd->doc_id);

                    $finish_date = date('Y-m-d', strtotime($project_name->start_date . ' +'.round($doc->days_for_execute).' day'));

                    $docs[] = [
                        'id' => $pd->id,
                        'project_name' => $project_name->name,
                        'client' => $client->name,
                        'city' => $city,
                        'doc_type' => $doc_type->name,
                        'doc' => $doc->name,
                        'start_date' => $project_name->start_date,
                        'finish_date' => $finish_date
                    ];
                }
            } else {
                $docs = '';
            }

            return response()->json($docs);
        }
    }

    public function getQueueDoc(Request $request) {
        if ($request->ajax()) {
            $doc = Documents_reference::whereNull('deleted_at')
                ->where('sort', '!=', 0)
                ->orderBy('sort')
                ->get();

            foreach ($doc as $d) {
                $type = Doc_types_reference::find($d->type_id);
                $doc_ar[] = [
                    'type' => $type->name,
                    'doc' => $d->name
                ];
            }

            return response()->json($doc_ar);
        }
    }
}
