<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use Sentinel;
use Activation;
use Mail;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\PermissionsController as Permissions;
use App\Http\Controllers\Manager\Projects2Controller as Projects;
use App\Http\Controllers\Controller;
use App\User;
use App\Role_user;
use App\Role;

class UserController extends Controller
{
    public function users() {
        if (session('perm')['user.view']) {
            $users = User::all();
            foreach ($users as $u) {
                $u['role'] = Role_user::getByUserId($u->id);
                if ($u->is_ban == 1)
                    $u['status'] = 1;
                else
                    $u['status'] = 0;
            }

            foreach ($users as $u) {
                if (Activation::completed($u))
                    $u['active'] = 1;
                else
                    $u['active'] = 0;
            }

            return view('Manager.users.users', [
                'users' => $users,
            ]);
        } else {
            abort(503);
        }
    }

    public function switchStatus(Request $request) {
        if ($request->ajax()) {
            $user = User::where('id', '=', $request->id)->firstOrFail();
            if ($request->status == 1)
                $user->is_ban = 0;
            else
                $user->is_ban = 1;

            if ($user->save()) {
                $resp = [
                    'status' => 1
                ];
            } else {
                $resp = [
                    'status' => 0
                ];
            }

            return $resp;
        }
    }

    public function userDel($id) {
        if (session('perm')['user.delete']) {
            $user = Sentinel::findById($id);
            if ($user->delete()) {
                return redirect()->back()->with('success', 'Пользователь успешно удален');
            } else {
                return redirect()->back()->with('error', 'Произошла ошибка при удалении пользователя');
            }
        } else {
            abort(503);
        }
    }

    public function add() {
        if (session('perm')['user.create']) {
            $roles = Role::all();

            return view('Manager.users.add', [
                'roles' => $roles,
            ]);
        } else {
            abort(503);
        }
    }

    public function userEdit($id) {
        if (session('perm')['user.update']) {
            $roles = Role::all();

            $u = User::find($id);

            return view('Manager.users.edit', [
                'roles' => $roles,
                'u' => $u
            ]);
        } else {
            abort(503);
        }
    }

    //TODO Доделать загрузку фото
    public function upload($img) {
        $photoName = time().'.'.$img->getClientOriginalExtension();
        $foto = $img->move(public_path('avatars'), $photoName);

        return $photoName;
    }

    public function store(Request $request) {
        $this->validate($request, [
            'user_name' => 'required|alpha',
            'user_surname' => 'required|alpha',
            'user_email' => 'required|email',
            'user_pass' => 'required',
        ]);

        $credentials = [
            'email'    => $request->user_email,
            'password' => $request->user_pass,
            'last_name' => $request->user_surname,
            'first_name' => $request->user_name
        ];
        $user = Sentinel::register($credentials);
        if ($request->img) {
            $upload = $this->upload($request->file('img'));
            $new_user = User::where('id', '=', $user->id)->firstOrFail();
            $new_user->foto = $upload;
            $new_user->save();
        } else {
            $new_user = User::where('id', '=', $user->id)->firstOrFail();
            $new_user->foto = 'camera_200.png';
            $new_user->save();
        }

        $activation = Activation::create($user);
        $role = Sentinel::findRoleById($request->user_role);
        $role->users()->attach($user);

        return redirect('/Manager/users')->with('success', 'Пользователь успешно добавлен');
    }

    public function userUpdate($id, Request $request) {
        dd($request->all());
    }

    public function getUserInfo($id) {
        dd(User::find($id));
    }

    static public function getAllUsers() {
        $users = User::whereNull('deleted_at')
            ->where('id', '!=', 0)
            ->get();
        return $users;
    }

    static public function getUserById ($id) {
        $user = User::where('id', '=', $id)
            //->whereNull('deleted_at')
            ->first();

        if (!$user){
            $user = (object)[
                'last_name' => '',
                'first_name' => ''
            ];
        }
        return $user;
    }

    public function getUserProjects(Request $request) {
        if ($request->ajax()) {
            $projects = Projects::getUserProjects($request->id);
            $tasks = Projects::getUserTasks($request->id);
            $allTimeTask = 0;
            foreach ($tasks as $t) {
                $allTimeTask += $t['number_contacts'];
            }

            return response()->json(['projects' => count($projects), 'tasks' => $allTimeTask]);
        }

    }

    static public function getImplementers() {
        $users = User::whereNull('users.deleted_at')
            ->select('users.*')
            ->leftJoin('role_users', 'role_users.user_id', '=',  'users.id')
            ->where('users.id', '!=', 0)
            ->where('role_users.role_id', 4)
            ->get();
        return $users;
    }
}
