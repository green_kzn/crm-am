<?php

namespace App\Http\Controllers\Defaultcab;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DocController extends Controller
{
    public function index() {
        if (session('perm')['doc.view']) {
            return view('Defaultcab.doc.doc');
        } else {
            abort(503);
        }
    }
}
