<?php

namespace App\Http\Controllers\Defaultcab;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use App\Http\Controllers\PermissionsController as Permissions;
use App\User;

class DefaultcabController extends Controller
{
    public function admin() {
        if (session('perm')['dashboard.view']) {
            return view('Defaultcab.dashboard');
        } else {
            abort(503);
        }
    }
}
