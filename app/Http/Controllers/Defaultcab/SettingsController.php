<?php

namespace App\Http\Controllers\Defaultcab;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Settings;

class SettingsController extends Controller
{
    public function index() {
        $settings = Settings::whereNull('deleted_at')->get();

        return view('Defaultcab.settings.settings', [
            'settings' => $settings
        ]);
    }
}
