<?php

namespace App\Http\Controllers\Defaultcab;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PermissionsController as Permissions;
use Sentinel;
use App\User;
use App\Event;

class EventController extends Controller
{
    public static function getUserEvents($id) {
        $events = Event::where('user_id', '=', $id)->whereNULL('deleted_at')->get();

        return $events;
    }

    public function addEvent() {
        return view('Defaultcab.events.add');
    }

    public function postAddEvent(Request $request) {
        if (!$request->inputTitleEvent) {
            return redirect()->back()->with('error', 'Вы не указали название события');
        }
        if (!$request->dateEvent) {
            return redirect()->back()->with('error', 'Вы не указали дату события');
        }
        dd($request->all());
    }
}
