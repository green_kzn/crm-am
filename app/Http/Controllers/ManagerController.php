<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;

class ManagerController extends Controller
{
    public function manager() {
        $user = Sentinel::getUser()->first();
        $role = Sentinel::getUser()->roles()->first()->name;

        return view('manager.dashboard', [
            'user' => $user,
            'role' => $role
        ]);
    }
}
