<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use App\Http\Controllers\PermissionsController as Permissions;
use App\Review;
use App\Client;

class ReviewController extends Controller
{
    public function review() {
        $cur_user = Sentinel::getUser();
        $role = Sentinel::getUser()->roles()->first()->name;

        $perm = new Permissions();

        $reviews = Review::whereNull('deleted_at')->get();
        foreach ($reviews as $r) {
            $c_name = Client::where('cid', '=', $r->cid)->whereNull('deleted_at')->first();
            $r['client_name'] = $c_name['name'];
        }

        return view('admin.reviews.reviews', [
            'user' => $cur_user,
            'role' => $role,
            'user_perm' => $perm->getAdminPermissions()[0],
            'group_perm' => $perm->getAdminPermissions()[1],
            'event_perm' => $perm->getAdminPermissions()[2],
            'reviews' => $reviews,
        ]);
    }

    public function reviewView($id) {
        $cur_user = Sentinel::getUser();
        $role = Sentinel::getUser()->roles()->first()->name;

        $perm = new Permissions();

        $review = Review::find($id);

        $client = Client::where('cid', '=', $review->cid)->whereNull('deleted_at')->first();

        return view('admin.reviews.review', [
            'user' => $cur_user,
            'role' => $role,
            'user_perm' => $perm->getAdminPermissions()[0],
            'group_perm' => $perm->getAdminPermissions()[1],
            'event_perm' => $perm->getAdminPermissions()[2],
            'review' => $review,
            'client' => $client,
        ]);
    }

    public function reviewDel($id) {
        $review = Review::find($id);
        $review->deleted_at = date('Y-m-d H:i:s');

        if ($review->save()) {
            return redirect()->back()->with('success', 'Отзыв успешно удален');
        } else {
            return redirect()->back()->with('error', 'Ошибка при удалении отзыва');
        }
    }
}
