<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use App\Http\Controllers\PermissionsController as Permissions;
use App\Event;

class EventController extends Controller
{
    public function event() {
        $user = Sentinel::getUser()->first();
        $role = Sentinel::getUser()->roles()->first()->name;

        $perm = new Permissions();

        $events = Event::all();

        return view('admin.events.events', [
            'user' => $user,
            'role' => $role,
            'user_perm' => $perm->getAdminPermissions()[0],
            'group_perm' => $perm->getAdminPermissions()[1],
            'event_perm' => $perm->getAdminPermissions()[2],
            'events' => $events,
        ]);
    }

    public function addEvents() {
        $user = Sentinel::getUser()->first();
        $role = Sentinel::getUser()->roles()->first()->name;

        $perm = new Permissions();

        return view('admin.events.add', [
            'user' => $user,
            'role' => $role,
            'user_perm' => $perm->getAdminPermissions()[0],
            'group_perm' => $perm->getAdminPermissions()[1],
            'event_perm' => $perm->getAdminPermissions()[2],
        ]);
    }

    public function postAddEvent(Request $request) {
        dd($request);
    }
}
