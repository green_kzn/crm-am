<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;

class PaginatorController extends Controller
{
    static public function getCountRecord()
    {
        $clients = Client::whereNull('deleted_at')->get();

        return count($clients);
    }
}
