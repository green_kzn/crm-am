<?php

namespace App\Http\Controllers\Implementer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Callback_work_question;

class QualityControlController extends Controller
{
    public function postAdd(Request $request) {
        $data = json_decode($request->data);
        
        $quality_list = new Callback_work_question(); 
        $quality_list->question = $data->name;
        
        if ($quality_list->save()) {
            return response()->json(array('status' => 'Ok'));
        } else {
            return response()->json(array('status' => 'Error'));
        }
    }

    public function getQualityList() {
        $quality_list = Callback_work_question::whereNull('deleted_at')
            ->get();

        return response()->json($quality_list);
    }

    public function delQualityList(Request $request) {
        $data = json_decode($request->data);
        
        $quality_list = Callback_work_question::find($data->id);
        $quality_list->deleted_at = date("Y-m-d H:i:s");
        if ($quality_list->save()) {
            $list = Callback_work_question::whereNull('deleted_at')
            ->get();

            return response()->json(array('status' => 'Ok', 'list' => $list));
        } else {
            return response()->json(array('status' => 'Error'));
        }
    }

    public function getQuestion($id) {
        $question = Callback_work_question::find($id);

        return response()->json($question);
    }

    public function postEdit(Request $request) {
        $data = json_decode($request->data);
        $question = Callback_work_question::find($data->id);
        $question->question = $data->name;
        if ($question->save()) {
            return response()->json(array('status' => 'Ok'));
        } else {
            return response()->json(array('status' => 'Error'));
        }
    }
}
