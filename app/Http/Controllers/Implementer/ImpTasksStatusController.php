<?php

namespace App\Http\Controllers\Implementer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tasksstatus_reference;

class ImpTasksStatusController extends Controller
{
    public function index() {
        $tstatus = Tasksstatus_reference::whereNULL('deleted_at')
            ->get();

        return response()->json($tstatus);
    }

    public function add() {
        if (session('perm')['status_task_ref.create']) {
            return view('Implementer.references.tasks_status.add');
        } else {
            abort(503);
        }
    }

    public function edit($id) {
        $tstatus = Tasksstatus_reference::find($id);

        return response()->json($tstatus);
    }

    public function postAdd(Request $request) {
        $data = json_decode($request->data);
        $tstatus = new Tasksstatus_reference();
        $tstatus->name = $data->name_tstatus;

        if ($tstatus->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function postEdit(Request $request, $id) {
        $tstatus = Tasksstatus_reference::find($id);
        $data = json_decode($request->data);
        $tstatus->name = $data->name_tstatus;

        if ($tstatus->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function del($id) {
        $tstatus = Tasksstatus_reference::find($id);
        $tstatus->deleted_at = date("Y-m-d H:i:s");

        if ($tstatus->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }
}
