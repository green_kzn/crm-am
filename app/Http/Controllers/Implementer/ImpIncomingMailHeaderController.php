<?php

namespace App\Http\Controllers\Implementer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImpIncomingMailHeaderController extends Controller
{
    /** @var int|string $id The IMAP message ID - not the "Message-ID:"-header of the email */
    public $id;
    public $date;
    public $headersRaw;
    public $headers;
    public $subject;

    public $fromName;
    public $fromAddress;

    public $to = array();
    public $toString;
    public $cc = array();
    public $bcc = array();
    public $replyTo = array();

    public $messageId;
}
