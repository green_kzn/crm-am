<?php

namespace App\Http\Controllers\Implementer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Settings;

class ImpSettingsController extends Controller
{
    public function index() {
        $settings = Settings::whereNull('deleted_at')->get();

        return view('Implementer.settings.settings', [
            'settings' => $settings
        ]);
    }
}
