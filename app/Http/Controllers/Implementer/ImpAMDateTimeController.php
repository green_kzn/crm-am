<?php

namespace App\Http\Controllers\Implementer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImpAMDateTimeController extends Controller
{
    static public function startProjectDate($startDate) {
        $nowDate = date("Y-m-d");
        if ($nowDate > $startDate) {
            return false;
        } else {
            return true;
        }
    }

    static public function finishProjectDate($finishDate) {
        $nowDate = date("Y-m-d");
        if ($nowDate > $finishDate) {
            return false;
        } else {
            return true;
        }
    }
}
