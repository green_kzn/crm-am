<?php

namespace App\Http\Controllers\Defaultcab;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use App\Http\Controllers\PermissionsController as Permissions;
use App\User;

class ImpDefaultcabController extends Controller
{
    public function admin() {
        if (session('perm')['dashboard.view']) {
            return view('default.dashboard');
        } else {
            abort(503);
        }
    }
}
