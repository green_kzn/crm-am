<?php

namespace App\Http\Controllers\Implementer;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Implementer\ImpEventController as EventController;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Implementer\ImpGroupTasksController as GroupTasks;
use App\Http\Controllers\Implementer\ImpModulesController as Modules;
use App\Http\Controllers\Implementer\ImpContactController as Contact;
use App\Http\Controllers\Implementer\ImpProjectsController as Projects;
use App\Http\Controllers\Implementer\ImpUserController as Users;
use App\Http\Controllers\Implementer\ImpClientsController as Clients;
use App\Http\Controllers\Controller;
use Monolog\Handler\IFTTTHandler;
use Sentinel;
use App\Task;
use App\User;
use App\Project_task;
use App\Project;
use App\Project_module;
use App\Module_rule;
use App\Comment;
use App\Tasksstatus_reference;
use App\Clients_reference;
use App\Clarifying_question;
use App\Clarifying_answer;

class ImpTasksController extends Controller
{
    public function index() {
        if (session('perm')['task.view']) {
            $cur_user = Sentinel::getUser();
            $res = '';

            $tasks = Task::where('contractor_id', '=', $cur_user->id)
                ->whereNULL('deleted_at')
                ->get();

            $p_tasks = Project_task::where('user_id', '=', $cur_user->id)
                ->whereNull('status')
                ->whereNull('deleted_at')
                ->get();

            if (count($p_tasks) > 0) {
                foreach ($p_tasks as $pt) {
                    $name = Module_rule::find($pt->module_rule_id);
                    $user = User::find($pt->observer_id);
                    $res[] = [
                        'name' => $name->task,
                        'created' => $pt->created_at,
                        'observer' => $user->last_name.' '.$user->first_name
                    ];
                }
            }


            if (count($tasks) > 0) {
                foreach ($tasks as $t) {
                    $user = User::find($t->observer_id);
                    $res[] = [
                        'name' => $t->title,
                        'created' => $t->created_at,
                        'observer' => $user->last_name.' '.$user->first_name,
                        'start_date' => $t->start_date,
                        'start_time' => $t->start_time,
                        'end_date' => $t->end_date,
                        'end_time' => $t->end_time,
                    ];
                }
            }

            return view('Implementer.tasks.tasks', [
                'tasks' => $res,
            ]);
        } else {
            abort(503);
        }
    }

    public function addTask() {
        if (session('perm')['task.create']) {

            return view('Implementer.tasks.add', [
                'users' => Users::getAllUsers(),
                'clients' => Clients::getAllClients(),
                'projects' => Projects::getAllProjects(),
            ]);
        } else {
            abort(503);
        }
    }

    public function time_add_min($time, $min) {
        list($h, $m) = explode(':', $time);
        $h = ($h + floor($m / 60)) % 24;
        $m = ($m + $min) % 60;
        return str_pad($h, 2, "0", STR_PAD_LEFT).':'.str_pad($m, 2, '0');
    }

    public function getTasks(Request $request) {
        if ($request->ajax()) {
            $cur_user = Sentinel::getUser();
            $tasks = Task::where('contractor_id', '=', $cur_user->id)
                ->where('start_date', '!=', NULL)
                ->whereNULL('deleted_at')
                ->get();
            $project_tasks = Project_task::where('user_id', '=', $cur_user->id)
                ->where('next_contact', '!=', NULL)
                ->whereNull('deleted_at')
                ->whereNull('status')
                ->get();

            if (count($tasks) > 0) {
                foreach ($tasks as $task) {
                    $json[] = array(
                        'id' => $task->id,
                        'title' => $task->title,
                        'start' => $task->start_date.' '.$task->start_time,
                        'end' => $task->end_date,
                        'allDay' => false,
                        'color' => $task->color,
                        'url' => '/implementer/task/edit/'.$task->id
                    );
                }
            }

            if (count($project_tasks) > 0) {
                foreach ($project_tasks as $pt) {
                    $name = Module_rule::find($pt->module_rule_id);
                    $p_name = Project::find($pt->project_id);
                    $p_tasks[] = [
                        'id' => $pt['id'],
                        'name' => $name->task,
                        'p_name' => $p_name['name'],
                        'city' => $p_name['city'],
                        'duration' => $pt['duration'],
                        'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                        'next_contact' => $pt['next_contact'],
                        'prev_contact' => $pt['prev_contact'],
                    ];
                }

                foreach ($p_tasks as $pt) {
                    switch ($pt['tasksstatus_ref_id']) {
                        case 1:
                            $end = explode(' ', $pt['next_contact']);
                            $json[] = array(
                                'id' => $pt['id'],
                                'title' => $pt['name'],
                                'start' => $pt['next_contact'],
                                'end' => $end[0].' '.date('H:i:s',strtotime($end[1]) + strtotime($pt['duration'])-strtotime("00:00:00")),
                                'allDay' => false,
                                'color' => '#3a87ad',
                                'url' => '/implementer/project-task/edit/'.$pt['id']
                            );
                            break;
                        case 2:
                            $end = explode(' ', $pt['next_contact']);
                            $json[] = array(
                                'id' => $pt['id'],
                                'title' => $pt['name'],
                                'start' => $pt['next_contact'],
                                'end' => $end[0].' '.date('H:i:s',strtotime($end[1]) + strtotime($pt['duration'])-strtotime("00:00:00")),
                                'allDay' => false,
                                'color' => 'green',
                                'url' => '/implementer/project-task/edit/'.$pt['id']
                            );
                            break;
                        case 3:
                            $end = explode(' ', $pt['next_contact']);
                            $json[] = array(
                                'id' => $pt['id'],
                                'title' => $pt['name'],
                                'start' => $pt['next_contact'],
                                'end' => $end[0].' '.date('H:i:s',strtotime($end[1]) + strtotime($pt['duration'])-strtotime("00:00:00")),
                                'allDay' => false,
                                'color' => 'yellow',
                                'url' => '/implementer/project-task/edit/'.$pt['id']
                            );
                            break;
                    }
                }
            }


            return response()->json($json);
        }
    }

    public function postAddEvent() {
        echo "Ok";
    }

    public function editTask($id) {
        if (session('perm')['task.update']) {
            $cur_task = Task::find($id);

            $cur_date = date("j").'.'.date("n").'.'.date("Y");
            $cur_date_val = date("Y").'-'.date("m").'-'.date("d");
            $tomorrow = date('Y-m-d', strtotime($cur_date . ' +1 day'));
            $tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +1 day'));
            $plus_one_day = date('Y-m-d', strtotime($cur_date . ' +2 day'));
            $plus_one_day_val = date('Y-m-d', strtotime($cur_date . ' +2 day'));
            $plus_two_day = date('d.m.Y', strtotime($cur_date . ' +3 day'));
            $plus_two_day_val = date('Y-m-d', strtotime($cur_date . ' +3 day'));
            $plus_three_day = date('d.m.Y', strtotime($cur_date . ' +4 day'));
            $plus_three_day_val = date('Y-m-d', strtotime($cur_date . ' +4 day'));

            return view('Implementer.tasks.edit', [
                'task' => $cur_task,
                'cur_date' => $cur_date,
                'cur_date_val' => $cur_date_val,
                'tomorrow' => $tomorrow,
                'tomorrow_val' => $tomorrow_val,
                'plus_one_day' => $plus_one_day,
                'plus_one_day_val' => $plus_one_day_val,
                'plus_two_day' => $plus_two_day,
                'plus_two_day_val' => $plus_two_day_val,
                'plus_three_day' => $plus_three_day,
                'plus_three_day_val' => $plus_three_day_val,
                'task_list' => GroupTasks::getAllTasks(),
                'task_status' => GroupTasks::getAllTasksStatus(),
                'modules' => Modules::getAllModule(),
                'group_task' => GroupTasks::getAllTaskGroup(),
            ]);
        } else {
            abort(503);
        }
    }

    public function editProjectTask($id) {
        if (session('perm')['task.update']) {
            $cur_p_task = Project_task::find($id);

            $name = Module_rule::find($cur_p_task['module_rule_id']);
            $p_name = Project::find($cur_p_task['project_id']);

            if (count($cur_p_task) > 0) {
                $p_tasks[] = [
                    'id' => $cur_p_task['id'],
                    'title' => $name['task'],
                    'p_name' => $p_name['name'],
                    'city' => $p_name['city'],
                    'start_date' => $cur_p_task['next_contact'],
                    'prev_contact' => $cur_p_task['prev_contact'],
                    'main_contact' => Contact::getProjectMainContact($cur_p_task['project_id']),
                    'project_contacts' => Contact::getProjectContacts($cur_p_task['project_id']),
                ];
            } else {
                $p_tasks = '';
            }
            //dd($p_tasks[0]);

            $p_tasks = (object)$p_tasks[0];

            $cur_date = date("j").'.'.date("n").'.'.date("Y");
            $cur_date_val = date("Y").'-'.date("m").'-'.date("d");
            $tomorrow = date('Y-m-d', strtotime($cur_date . ' +1 day'));
            $tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +1 day'));
            $plus_one_day = date('Y-m-d', strtotime($cur_date . ' +2 day'));
            $plus_one_day_val = date('Y-m-d', strtotime($cur_date . ' +2 day'));
            $plus_two_day = date('d.m.Y', strtotime($cur_date . ' +3 day'));
            $plus_two_day_val = date('Y-m-d', strtotime($cur_date . ' +3 day'));
            $plus_three_day = date('d.m.Y', strtotime($cur_date . ' +4 day'));
            $plus_three_day_val = date('Y-m-d', strtotime($cur_date . ' +4 day'));

            $module_id = Project_module::where('project_id', '=', $p_name['id'])
                ->whereNull('deleted_at')
                ->firstOrFail();

            $comments_q = Comment::where('project_id', '=', $cur_p_task->project_id)
                ->whereNull('deleted_at')
                ->get();

            if (count($comments_q) > 0) {
                foreach ($comments_q as $c) {
                    $user = User::find($c->user_id);
                    $comments[] = [
                        'text' => strip_tags($c->text),
                        'date' => $c->date,
                        'time' => $c->time,
                        'id' => $c->id,
                        'user_name' => $user->first_name,
                        'user_surname' => $user->last_name,
                        'user_foto' => $user->foto,
                    ];
                }
            } else {
                $comments = '';
            }

            $project_task = Project_task::where('project_id', '=', $cur_p_task->project_id)
                ->whereNull('status')
                ->whereNull('deleted_at')
                ->get();

            $task_status = GroupTasks::getAllTasksStatus();

            if (count($project_task) > 0) {
                foreach ($project_task as $pt) {
                    $name = Module_rule::find($pt->module_rule_id);
                    $status = Tasksstatus_reference::find($pt->tasksstatus_ref_id);

                    $tasks[] = [
                        'id' => $pt->id,
                        'name' => $name->task,
                        'status_id' => $status->id,
                        'status_name' => $status->name,
                        'questions' => $name->questions
                    ];
                }
            } else {
                $tasks = '';
            }

            //dd($tasks);

            $project = Project::find($cur_p_task->project_id);
            $client = Clients_reference::find($project->client_id);

            $observer = Users::getAllUsers();
            //dd(Modules::getAllModule());

            return view('Implementer.tasks.edit', [
                'task' => $p_tasks,
                'cur_date' => $cur_date,
                'cur_date_val' => $cur_date_val,
                'tomorrow' => $tomorrow,
                'tomorrow_val' => $tomorrow_val,
                'plus_one_day' => $plus_one_day,
                'plus_one_day_val' => $plus_one_day_val,
                'plus_two_day' => $plus_two_day,
                'plus_two_day_val' => $plus_two_day_val,
                'plus_three_day' => $plus_three_day,
                'plus_three_day_val' => $plus_three_day_val,
                'task_list' => GroupTasks::getAllProjectTasks($module_id->module_id),
                'task_status' => $task_status,
                'modules' => Modules::getAllModule(),
                'group_task' => GroupTasks::getAllTaskGroup(),
                'comments' => $comments,
                'tasks' => $tasks,
                'client' => $client,
                'observers' => $observer,
            ]);
        } else {
            abort(503);
        }
    }

    public function postEditProjectTask(Request $request) {
        switch ($request->res) {
            case 'work':
                $data = json_decode($request->data);
                $data = $data[0];

                if ($data->nextContact == 0) {
                    DB::transaction(function () use ($request, $data) {
                        $project = Projects::getProjectByTaskId($request->id);
                        $date =  explode(' ', date("Y-m-d H:i:s"));
                        $cur_user = Sentinel::getUser();
                        try {
                            $comment = new Comment();
                            $comment->project_id = $project->id;
                            $comment->user_id = $cur_user->id;
                            $comment->for_client = NULL;
                            $comment->date = $date[0];
                            $comment->time = $date[1];
                            $comment->text = $request->comment6;
                            $comment->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }

                        try {
                            $comment_client = new Comment();
                            $comment_client->project_id = $project->id;
                            $comment_client->user_id = $cur_user->id;
                            $comment_client->for_client = 1;
                            $comment_client->date = $date[0];
                            $comment_client->time = $date[1];
                            $comment_client->text = $request->comment5;
                            $comment_client->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }

                        try {
                            $cur_task = Project_task::find($request->id);
                            $cur_task->tasksstatus_ref_id = $data->curTaskStatus;
                            $cur_task->duration = $data->durationTime;
                            $cur_task->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }

                        if (isset($data->otherTask)) {
                            foreach ($data->otherTask as $ot) {
                                try {
                                    $other_task = Project_task::find($ot->id);
                                    $other_task->tasksstatus_ref_id = $ot->status;
                                    $other_task->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }

                        if (isset($data->dopTask)) {
                            foreach ($data->dopTask as $dt) {
                                try {
                                    $dopTask = new Module_rule();
                                    $dopTask->task = $dt->name;
                                    $dopTask->module_id = $dt->module;
                                    $dopTask->number_contacts = $dt->numCont;
                                    $dopTask->grouptasks_id = $dt->group;
                                    $dopTask->additional = 1;
                                    $dopTask->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }

                                try {
                                    $newDopTask = new Project_task();
                                    $newDopTask->project_id = $project->id;
                                    $newDopTask->user_id = $cur_user->id;
                                    $newDopTask->observer_id = $cur_user->id;
                                    $newDopTask->module_rule_id = $dopTask->id;
                                    $newDopTask->number_contacts = $dt->numCont;
                                    $newDopTask->module_id = $dt->module;
                                    $newDopTask->tasksstatus_ref_id = 1;
                                    $newDopTask->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }

                        foreach ($data->cq as $ca) {
                            try {
                                $answer = new Clarifying_answer();
                                $answer->question_id = $ca->id;
                                $answer->project_id = $project->id;
                                $answer->answer = $ca->answer;
                                $answer->save();

                                $this->success = true;
                            } catch (\Exception $e) {
                                $this->success = false;
                            }
                        }
                    });

                    if ($this->success) {
                        return redirect()->back()->with('success', 'Статус задачи успешно обновлен');
                    } else {
                        return redirect()->back()->with('error', 'Произошла ошибка при обновлении статуса задачи');
                    }
                } else {
                    DB::transaction(function () use ($request, $data) {
                        $project = Projects::getProjectByTaskId($request->id);
                        $date =  explode(' ', date("Y-m-d H:i:s"));
                        $cur_user = Sentinel::getUser();
                        try {
                            $comment = new Comment();
                            $comment->project_id = $project->id;
                            $comment->user_id = $cur_user->id;
                            $comment->for_client = NULL;
                            $comment->date = $date[0];
                            $comment->time = $date[1];
                            $comment->text = $request->comment6;
                            $comment->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }

                        try {
                            $comment_client = new Comment();
                            $comment_client->project_id = $project->id;
                            $comment_client->user_id = $cur_user->id;
                            $comment_client->for_client = 1;
                            $comment_client->date = $date[0];
                            $comment_client->time = $date[1];
                            $comment_client->text = $request->comment5;
                            $comment_client->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }

                        try {
                            $cur_task = Project_task::find($request->id);
                            $cur_task->tasksstatus_ref_id = $data->curTaskStatus;
                            $cur_task->duration = $data->durationTime;
                            $cur_task->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }

                        if (isset($data->otherTask)) {
                            foreach ($data->otherTask as $ot) {
                                try {
                                    $other_task = Project_task::find($ot->id);
                                    $other_task->tasksstatus_ref_id = $ot->status;
                                    $other_task->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }


                        if (isset($data->dopTask)) {
                            foreach ($data->dopTask as $dt) {
                                try {
                                    $dopTask = new Module_rule();
                                    $dopTask->task = $dt->name;
                                    $dopTask->module_id = $dt->module;
                                    $dopTask->number_contacts = $dt->numCont;
                                    $dopTask->grouptasks_id = $dt->group;
                                    $dopTask->additional = 1;
                                    $dopTask->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }

                                try {
                                    $newDopTask = new Project_task();
                                    $newDopTask->project_id = $project->id;
                                    $newDopTask->user_id = $cur_user->id;
                                    $newDopTask->observer_id = $cur_user->id;
                                    $newDopTask->module_rule_id = $dopTask->id;
                                    $newDopTask->number_contacts = $dt->numCont;
                                    $newDopTask->module_id = $dt->module;
                                    $newDopTask->tasksstatus_ref_id = 1;
                                    $newDopTask->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }

                        foreach ($data->cq as $ca) {
                            try {
                                $answer = new Clarifying_answer();
                                $answer->question_id = $ca->id;
                                $answer->project_id = $project->id;
                                $answer->answer = $ca->answer;
                                $answer->save();

                                $this->success = true;
                            } catch (\Exception $e) {
                                $this->success = false;
                            }
                        }

                        try {
                            $newContTask = Project_task::find($data->nextContTask);
                            $newContTask->next_contact = $data->nextContDateStart.' '.$data->nextContTimeStart;
                            $newContTask->user_id = $data->nextContUser;
                            $newContTask->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }
                    });

                    if ($this->success) {
                        return redirect()->back()->with('success', 'Задача успешно перенесена на другое время');
                    } else {
                        return redirect()->back()->with('error', 'Произошла ошибка при перенесении задачи на другое время');
                    }
                }
                break;
            case 'next':
                DB::transaction(function () use ($request) {
                    $project = Projects::getProjectByTaskId($request->id);
                    $date =  explode(' ', date("Y-m-d H:i:s"));
                    $cur_user = Sentinel::getUser();
                    try {
                        $comment = new Comment();
                        $comment->project_id = $project->id;
                        $comment->user_id = $cur_user->id;
                        $comment->for_client = NULL;
                        $comment->date = $date[0];
                        $comment->time = $date[1];
                        $comment->text = $request->comment6;
                        $comment->save();

                        $this->success = true;
                    } catch (\Exception $e) {
                        $this->success = false;
                    }

                    try {
                        $comment_client = new Comment();
                        $comment_client->project_id = $project->id;
                        $comment_client->user_id = $cur_user->id;
                        $comment_client->for_client = 1;
                        $comment_client->date = $date[0];
                        $comment_client->time = $date[1];
                        $comment_client->text = $request->comment5;
                        $comment_client->save();

                        $this->success = true;
                    } catch (\Exception $e) {
                        $this->success = false;
                    }

                    $data = json_decode($request->data);
                    try {
                        $project_task = Project_task::find($request->id);
                        $project_task->prev_contact = $project_task->next_contact;
                        $project_task->next_contact = $data[0]->date.' '.$data[0]->time;
                        $project_task->save();

                        $this->success = true;
                    } catch (\Exception $e) {
                        $this->success = false;
                    }
                });

                if ($this->success) {
                    return redirect()->back()->with('success', 'Задача успешно перенесена на другое время');
                } else {
                    return redirect()->back()->with('error', 'Произошла ошибка при перенесении задачи на другое время');
                }
                break;
            case 'later':
                $cur_user = Sentinel::getUser();
                $project = Projects::getProjectByTaskId($request->id);
                $date =  explode(' ', date("Y-m-d H:i:s"));
                try {
                    $p_task = Project_task::find($request->id);
                    $p_task->status = 1;
                    $p_task->prev_contact = date("Y-m-d H:i:s");
                    $p_task->next_contact = NULL;
                    $p_task->save();

                    $comment = new Comment();
                    $comment->project_id = $project->id;
                    $comment->user_id = 0;
                    $comment->for_client = NULL;
                    $comment->date = $date[0];
                    $comment->time = $date[1];
                    $comment->text = 'До клиента не дозвонились';
                    $comment->save();

                    $this->success = true;
                } catch (\Exception $e) {
                    $this->success = false;
                }

                if ($this->success) {
                    return redirect()->back()->with('success', 'Задача успешно перенесена в клиентский отделл');
                } else {
                    return redirect()->back()->with('error', 'Произошла ошибка при перенесении задачи в клиентский отделл');
                }
                break;
        }
    }

    public function getTasksForAmcalendar(Request $request) {
        if ($request->ajax()) {
            $project_task = Project_task::where('user_id', '=', $request->user_id)
                ->where('next_contact', 'like', $request->date.'%')
                ->where('tasksstatus_ref_id', '=', 1)
                ->whereNull('deleted_at')
                ->get();

            foreach ($project_task as $pt) {
                $task_name = Module_rule::find($pt->module_rule_id);
                $date = explode(' ', $pt->next_contact);
                $tasks[] = [
                    'id' => $pt->id,
                    'time' => $date[1],
                    'task' => $task_name->task
                ];
            }

            return response()->json($tasks);
        }
    }

    static public function getTaskById($id) {
        $task = Project_task::whereNull('deleted_at')
            ->where('id', '=', $id)
            ->firstOrFail();

        return $task;
    }

    public function getClarificationQuestions(Request $request) {
        if ($request->ajax()) {
            $p_task = Project_task::find($request->tid);
            $c_question = Clarifying_question::where('task_id', '=', $p_task->module_rule_id)
                ->whereNull('deleted_at')
                ->get();
            return response()->json($c_question);
        }
    }

    public function postAddTask(Request $request) {
        switch ($request->typeTask) {
            case 'single_task':
                dd($request->all());
                break;
            case 'introduction':
                dd($request->all());
                break;
        }
    }
}
