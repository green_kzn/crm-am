<?php

namespace App\Http\Controllers\Implementer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IncomingMailAttachment extends Controller
{
    public $id;
    public $contentId;
    public $name;
    public $filePath;
    public $disposition;
}
