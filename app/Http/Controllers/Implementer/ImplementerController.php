<?php

namespace App\Http\Controllers\Implementer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use App\Http\Controllers\PermissionsController as Permissions;
use App\Http\Controllers\Implementer\ImpUserController as Users;
use App\Events\MessageSent;
use App\User;
use Cache;

use App\Events\addPrintFormFromProject;
use Event;

class ImplementerController extends Controller
{
    public function implementer(Request $request) {
        // event(new MessageSent('test'));
        return view('Implementer.dashboard');
    }

    public function test() {
        $cur_user = Sentinel::getUser();
        broadcast(new addPrintFormFromProject($cur_user->first_name.' '.$cur_user->last_name));
        print_r('Ok');
    }

    public function getAllImplementer() {
        $imp = User::where('roles.slug', '=', 'implementer')
            ->whereNULL('users.deleted_at')
            ->leftJoin('role_users', 'role_users.user_id', '=', 'users.id')
            ->leftJoin('roles', 'role_users.role_id', '=', 'roles.id')
            ->select(['users.*'])
            ->get();
        //dd($imp);
        return response()->json($imp);
    }

    static public function allImplementer() {
        $imp = User::where('roles.slug', '=', 'implementer')
            ->whereNULL('users.deleted_at')
            ->leftJoin('role_users', 'role_users.user_id', '=', 'users.id')
            ->leftJoin('roles', 'role_users.role_id', '=', 'roles.id')
            ->select(['users.*'])
            ->get();
        //dd($imp);
        return $imp;
    }
}
