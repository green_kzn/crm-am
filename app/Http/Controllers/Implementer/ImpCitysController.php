<?php

namespace App\Http\Controllers\Implementer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\City_reference;

class ImpCitysController extends Controller
{
    public function index() {
        if (session('perm')['citys_ref.view']) {
            $citys = City_reference::whereNull('deleted_at')->paginate(10);

            return view('Implementer.references.citys.citys', [
                'citys' => $citys
            ]);
        } else {
            abort(503);
        }
    }

    public function delete($id) {
        if (session('perm')['citys_ref.delete']) {
            $city = City_reference::find($id);
            $city->deleted_at = date("Y-m-d H:i:s");

            if ($city->save()) {
                return redirect()->back()->with('success', 'Город успешно удален');
            } else {
                return redirect()->back()->with('error', 'Произошла ошибка при удалении города');
            }
        } else {
            abort(503);
        }
    }

    public function edit($id) {
        if (session('perm')['citys_ref.update']) {
            $city = City_reference::find($id);
            return view('Implementer.references.citys.edit', [
                'city' => $city
            ]);
        } else {
            abort(503);
        }
    }

    public function postEdit(Request $request, $id) {
        if (session('perm')['citys_ref.update']) {
            if ($request->name_city) {
                $new_city = City_reference::find($id);
                $new_city->name = $request->name_city;
                $new_city->difference_msk = $request->name_dif;

                if ($new_city->save()) {
                    return redirect('/implementer/citys')->with('success', 'Город успешно изменен');
                } else {
                    return redirect('/implementer/citys')->with('error', 'Произошла ошибка при изменении города');
                }
            } else {
                return redirect()->back()->with('error', 'Укажите название города');
            }
        } else {
            abort(503);
        }
    }

    public function add() {
        if (session('perm')['citys_ref.create']) {
            return view('Implementer.references.citys.add');
        } else {
            abort(503);
        }
    }

    public function postAdd(Request $request) {
        if (session('perm')['citys_ref.create']) {
            if ($request->name_city) {
                $city = City_reference::where('name', 'like', '%'.$request->name_city.'%')
                    ->whereNull('deleted_at')
                    ->get();

                if (count($city) == 0) {
                    $city = new City_reference();
                    $city->name = $request->name_city;
                    $city->difference_msk = $request->name_dif;

                    if ($city->save()) {
                        return redirect('/implementer/citys')->with('success', 'Город успешно добавлен');
                    } else {
                        return redirect('/implementer/citys')->with('error', 'Произошла ошибка при добавлении города');
                    }
                } else {
                    return redirect()->back()->with('error', 'Город с таким названием уже существует');
                }
            } else {
                return redirect()->back()->with('error', 'Укажите название города');
            }
        } else {
            abort(503);
        }
    }

    static public function getAllCitys() {
        $citys = City_reference::whereNull('deleted_at')
            ->orderBy('name', 'asc')
            ->get();

        return $citys;
    }

    static public function getCityById($id) {
        $city = City_reference::where('id', '=', $id)
            ->whereNull('deleted_at')
            ->firstOrFail();

        return $city->name;
    }
}
