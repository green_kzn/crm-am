<?php

namespace App\Http\Controllers\Implementer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Implementer\ImpProjectsController as Projects;
use App\Http\Controllers\Implementer\ImpClientsController as Clients;
use App\Http\Controllers\Implementer\ImpCitysController as Citys;
use App\Http\Controllers\Implementer\ImpUserController as User;
use App\Http\Controllers\Implementer\ImpTasksController as Tasks;
use App\Http\Controllers\Implementer\ImpCommentController as Comments;
use App\Project_task;
use App\Module_rule;
use Sentinel;

class ImpCallsController extends Controller
{
    public function index() {
        if (session('perm')['calls.view']) {
            $p_tasks = Project_task::where('status', '!=', NULL)
                ->get();

            if (count($p_tasks) > 0) {
                foreach ($p_tasks as $pt) {
                    $name = Module_rule::find($pt->module_rule_id);
                    $project = Projects::getProjectById($pt->project_id);
                    $client = Clients::getClientById($project->client_id);
                    $city = Citys::getCityById($client->city_id);
                    $user = User::getUserById($pt->user_id);
                    //dd($client);

                    $tasks[] = [
                        'id' => $pt->id,
                        'prev_contact' => $pt->prev_contact,
                        'name' => $name->task,
                        'client' => $client,
                        'project' => $project,
                        'city' => $city,
                        'user' => $user,
                        'next_contact' => $pt->next_contact
                    ];
                }
            } else {
                $tasks = '';
            }

            //dd($tasks);

            return view('Implementer.calls.calls', [
                'tasks' => $tasks
            ]);
        } else {
            abort(503);
        }
    }

    public function edit($id) {
        $task = Tasks::getTaskById($id);
        $project = Projects::getProjectByTaskId($id);
        $client = Clients::getClientById($project->client_id);
        $comments = Comments::getCommentsByProjectId($project->id);

        $cur_date = date("j").'.'.date("n").'.'.date("Y");
        $cur_date_val = date("Y").'-'.date("m").'-'.date("d");
        $tomorrow = (date("j")+1).'.'.date("n").'.'.date("Y");
        $tomorrow_val = date("Y").'-'.date("m").'-'.(date("d")+1);
        $plus_one_day = (date("j")+2).'.'.date("n").'.'.date("Y");
        $plus_one_day_val = date("Y").'-'.date("m").'-'.(date("d")+2);
        $plus_two_day = (date("j")+3).'.'.date("n").'.'.date("Y");
        $plus_two_day_val = date("Y").'-'.date("m").'-'.(date("d")+3);
        $plus_three_day = (date("j")+4).'.'.date("n").'.'.date("Y");
        $plus_three_day_val = date("Y").'-'.date("m").'-'.(date("d")+4);

        return view('Implementer.calls.edit', [
            'task' => $task,
            'client' => $client,
            'comments' => $comments,

            'cur_date' => $cur_date,
            'cur_date_val' => $cur_date_val,
            'tomorrow' => $tomorrow,
            'tomorrow_val' => $tomorrow_val,
            'plus_one_day' => $plus_one_day,
            'plus_one_day_val' => $plus_one_day_val,
            'plus_two_day' => $plus_two_day,
            'plus_two_day_val' => $plus_two_day_val,
            'plus_three_day' => $plus_three_day,
            'plus_three_day_val' => $plus_three_day_val,
        ]);
    }
}
