<?php

namespace App\Http\Controllers\Implementer;

use Validator;
use Illuminate\Http\Request;
use Sentinel;
use Activation;
use Mail;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\PermissionsController as Permissions;
use App\Http\Controllers\Implementer\ImpProjectsController as Projects;
use App\Http\Controllers\Controller;
use App\User;
use App\Role_user;
use App\Role;
use App\Mail\ActivationAccount;

class ImpUser2Controller extends Controller
{
    public function users() {
        if (session('perm')['user.view']) {
            $users = User::whereNull('users.deleted_at')
                ->join('role_users', 'users.id', '=', 'role_users.user_id')
                ->join('roles', 'role_users.role_id', '=', 'roles.id')
                ->select('users.id', 'roles.name as role', 'users.email', 'users.is_ban', 'users.foto', 'users.first_name', 'users.last_name')
                ->get();
            foreach ($users as $u) {
                //$u['role'] = Role_user::getByUserId($u->id);
                if ($u->is_ban == 1)
                    $u['status'] = 1;
                else
                    $u['status'] = 0;
            }

            foreach ($users as $u) {
                if (Activation::completed($u))
                    $u['active'] = 1;
                else
                    $u['active'] = 0;
            }

            return view('Implementer.users.users', [
                'users' => $users,
            ]);
        } else {
            abort(503);
        }
    }

    public function getFields() {
        $cur_user = Sentinel::getUser();
        $perm = $cur_user->permissions;
        // dd($perm);
        return response()->json($perm);
    }

    public function changePerm(Request $request) {
        $data = json_decode($request->data);
        $cur_user = Sentinel::getUser();
        $perm = $cur_user->permissions;
        $cur_user->updatePermission(
            'forms.'.$data->name, $data->perm
        );

        if ($cur_user->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'No']);
        };
    }

    public function changePermProj(Request $request) {
        $data = json_decode($request->data);
        $cur_user = Sentinel::getUser();
        $perm = $cur_user->permissions;
        $cur_user->updatePermission(
            'project.'.$data->name, $data->perm
        );

        if ($cur_user->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'No']);
        };
    }

    public function getUsers() {
        $users = User::whereNull('users.deleted_at')
            ->join('role_users', 'users.id', '=', 'role_users.user_id')
            ->join('roles', 'role_users.role_id', '=', 'roles.id')
            ->select('users.id', 'roles.name as role', 'users.email', 'users.is_ban', 'users.foto', 'users.first_name', 'users.last_name')
            ->get();
        foreach ($users as $u) {
            //$u['role'] = Role_user::getByUserId($u->id);
            if ($u->is_ban == 1)
                $u['status'] = 1;
            else
                $u['status'] = 0;
        }

        foreach ($users as $u) {
            if (Activation::completed($u))
                $u['active'] = 1;
            else
                $u['active'] = 0;
        }

        return response()->json($users);
    }

    public function getUserId() {
        $curUser = Sentinel::getUser();
        $curDate = date("d-m-Y");
        return response()->json(['curUser' => $curUser, 'curDate' => $curDate]);
    }

    public function getLoadUser(Request $request) {
        $data = json_decode($request->data);
        $projects = Projects::getUserProjects($data->user_id);
        $tasks = Projects::getUserTasks($data->user_id);
        $allTimeTask = 0;
        foreach ($tasks as $t) {
            $allTimeTask += $t['number_contacts'];
        }

        return response()->json(['projects' => count($projects), 'tasks' => $allTimeTask]);
    }

    public function switchStatus(Request $request) {
        $data = json_decode($request->data);
        $user = User::where('id', '=', $data->id)->first();
        if ($data->status == 1)
            $user->is_ban = 0;
        else
            $user->is_ban = 1;

        if ($user->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function userDel(Request $request) {
        $data = json_decode($request->data);
        $user = Sentinel::findById($data->id);
        if ($user->delete()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function getGroups() {
        $roles = Role::all();

        return response()->json($roles);
    }

    public function userEdit($id) {
        $u = User::find($id);
        $user_role = Role_user::where('user_id', '=', $u->id)->first();
        $u_role = Sentinel::findRoleById($user_role->role_id);
        return response()->json([
            'u' => $u,
            'u_role' => $u_role
        ]);
    }

    //TODO Доделать загрузку фото
    public function upload($img) {
        $photoName = time().'.'.$img->getClientOriginalExtension();
        $foto = $img->move(public_path('avatars'), $photoName);

        return $photoName;
    }

    public function store(Request $request) {
        $data = json_decode($request->data);

        $credentials = [
            'email'    => $data->user_email,
            'password' => $data->user_pass,
            'last_name' => $data->user_surname,
            'first_name' => $data->user_name
        ];

        $validator = Validator::make($credentials, [
            'email' => 'required|unique:users|max:255'
        ]);

        if ($validator->fails()) {
            return ['status' => 'Error', 'message' => 'Email уже используется'];
        }

        try {
            $user = Sentinel::register($credentials);
        } catch (QueryException $ex) {
            return response()->json(['status' => 'Error']);
        }

        if (isset($data->img)) {
            $upload = $this->upload($data->file('img'));
            $new_user = User::where('id', '=', $user->id)->firstOrFail();
            $new_user->foto = $upload;
            $new_user->save();
        } else {
            $new_user = User::where('id', '=', $user->id)->firstOrFail();
            $new_user->foto = 'camera_200.png';
            $new_user->save();
        }

        $activation = Activation::create($user);
        Mail::to($user)->send(new ActivationAccount($new_user, $activation['code']));
        $role = Sentinel::findRoleById($data->user_role);
        $role->users()->attach($user);

        return response()->json(['status' => 'Ok']);
    }

    public function userUpdate(Request $request) {
        $data = json_decode($request->data);
        $user = Sentinel::findById($data->user->id);
        if (isset($data->user->pass)) {
            $credentials = [
                'email' => $data->user->email,
                'first_name' => $data->user->first_name,
                'last_name' => $data->user->last_name,
                'password' => $data->user->pass,
            ];
        } else {
            $credentials = [
                'email' => $data->user->email,
                'first_name' => $data->user->first_name,
                'last_name' => $data->user->last_name
            ];

        }


        if (Sentinel::validForUpdate($user, $credentials)) {
            $user = Sentinel::update($user, $credentials);
            $role = Role_user::where('user_id', '=', $user->id)->first();
            $role->role_id = $data->user->group;
            $role->save();

            if ($data->user->head == true) {
                $user->head = 1;
            } else {
                $user->head = NULL;
            }
            $user->save();
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function getUserInfo($id) {
        dd(User::find($id));
    }

    static public function getAllUsers() {
        $users = User::whereNull('deleted_at')
            ->where('id', '!=', 0)
            ->get();
        return $users;
    }

    static public function getImplementers() {
        $users = User::whereNull('users.deleted_at')
            ->select('users.*')
            ->leftJoin('role_users', 'role_users.user_id', '=',  'users.id')
            ->where('users.id', '!=', 0)
            ->where('role_users.role_id', 4)
            ->get();
        return $users;
    }

    static public function getUserById ($id) {
        $user = User::where('id', '=', $id)
            //->whereNull('deleted_at')
            ->first();
        if (!$user){
            $user = (object)[
                'last_name' => '',
                'first_name' => ''
            ];
        }
        return $user;
    }

    public function getUserProjects(Request $request) {
        if ($request->ajax()) {
            $projects = Projects::getUserProjects($request->id);
            $tasks = Projects::getUserTasks($request->id);
            $allTimeTask = 0;
            foreach ($tasks as $t) {
                $allTimeTask += $t['number_contacts'];
            }

            return response()->json(['projects' => count($projects), 'tasks' => $allTimeTask]);
        }

    }

    public function getUser(Request $request) {
        if ($request->ajax()) {
            $users = User::whereNull('deleted_at')
                ->where('id', '!=', 0)
                ->get();
            return response()->json($users);
        }
    }
}
