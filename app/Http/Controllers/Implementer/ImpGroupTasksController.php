<?php

namespace App\Http\Controllers\Implementer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Grouptasks_reference;
use App\Tasksstatus_reference;
use App\Module_rule;

class ImpGroupTasksController extends Controller
{
    public function getGroupTasks() {
        $grouptasks = Grouptasks_reference::whereNULL('deleted_at')
            ->get();

        return response()->json($grouptasks);
    }

    public function add() {
        if (session('perm')['task_group_ref.create']) {
            return view('Implementer.references.grouptasks.add');
        } else {
            abort(503);
        }
    }

    public function edit($id) {
        $task = Grouptasks_reference::find($id);

        return response()->json($task);
    }

    public function postAdd(Request $request) {
        $data = json_decode($request->data);
        $task = new Grouptasks_reference();
        $task->name = $data->TaskName;
        if ($data->isPrintForm == 'true') {
            $task->is_print_form = 1;
        } else {
            $task->is_print_form = 0;
        }
        if ($data->TaskNorm == 'true') {
            $task->is_norm = 1;
        } else {
            $task->is_norm = 0;
        }

        if ($task->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function postEdit(Request $request, $id) {
        $task = Grouptasks_reference::find($id);
        $data = json_decode($request->data);
        //dd($data);
        $task->name = $data->TaskName;
        if ($data->isPrintForm == 'true') {
            $task->is_print_form = 1;
        } else {
            $task->is_print_form = 0;
        }
        if ($data->TaskNorm == 'true') {
            $task->is_norm = 1;
        } else {
            $task->is_norm = 0;
        }

        if ($task->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function del($id) {
        $task = Grouptasks_reference::find($id);
        $task->deleted_at = date("Y-m-d H:i:s");

        if ($task->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    static public function getAllTasks() {
        $tasks_q = Module_rule::whereNULL('deleted_at')->get();
        foreach ($tasks_q as $t) {
            $tasks[] = [
                'id' => $t->id,
                'name' => $t->task
            ];
        }
        return $tasks;
    }

    static public function getAllProjectTasks($mid) {
        $tasks_q = Module_rule::where('module_id', '=', $mid)
            ->whereNULL('deleted_at')
            ->get();
        foreach ($tasks_q as $t) {
            $tasks[] = [
                'id' => $t->id,
                'name' => $t->task
            ];
        }
        return $tasks;
    }

    static public function getAllTasksStatus() {
        $tasks_status = Tasksstatus_reference::whereNULL('deleted_at')->get();
        return $tasks_status;
    }

    static public function getAllTaskGroup() {
        $group_tasks = Grouptasks_reference::whereNull('deleted_at')->get();
        return $group_tasks;
    }

    static public function getGroupTaskByName($name) {
        $g_task = Grouptasks_reference::where('name', '=', $name)
            ->whereNull('deleted_at')
            ->firstOrFail();

        return $g_task;
    }
}
