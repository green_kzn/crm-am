<?php

namespace App\Http\Controllers\Implementer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Waiting_list_reference;

class WaitingListController extends Controller
{
    public function postAdd(Request $request) {
        $data = json_decode($request->data);
        $wating_list = new Waiting_list_reference();
        $wating_list->name = $data->name;
        
        if ($wating_list->save()) {
            return response()->json(array('status' => 'Ok'));
        } else {
            return response()->json(array('status' => 'Error'));
        }
    }

    public function getWaitingList(Request $request) {
        $wating_list = Waiting_list_reference::whereNull('deleted_at')
            ->get();

        return response()->json($wating_list);
    } 

    public function del(Request $request) {
        $data = json_decode($request->data);
        
        $wating_list = Waiting_list_reference::find($data->id);
        $wating_list->deleted_at = date("Y-m-d H:i:s");
        if ($wating_list->save()) {
            $list = Waiting_list_reference::whereNull('deleted_at')
            ->get();

            return response()->json(array('status' => 'Ok', 'list' => $list));
        } else {
            return response()->json(array('status' => 'Error'));
        }
    }

    public function getList(Request $request, $id) {
        $wating_list = Waiting_list_reference::find($id);
        return response()->json($wating_list);
    }

    public function postEdit(Request $request) {
        $data = json_decode($request->data);
        $wating_list = Waiting_list_reference::find($data->id);
        $wating_list->name = $data->name;
        if ($wating_list->save()) {
            return response()->json(array('status' => 'Ok'));
        } else {
            return response()->json(array('status' => 'Error'));
        }
    }

    static public function getWListById($id) {
        $list = Waiting_list_reference::find($id);
        return $list;
    }
}
