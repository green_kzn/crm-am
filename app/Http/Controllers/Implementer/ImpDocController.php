<?php

namespace App\Http\Controllers\Implementer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImpDocController extends Controller
{
    public function index() {
        if (session('perm')['doc.view']) {
            return view('Implementer.doc.doc');
        } else {
            abort(503);
        }
    }
}
