<?php

namespace App\Http\Controllers\Implementer;

use App\Client;
use App\Http\Controllers\Common\ProjectCategoriesController as Categories;
use App\Http\Controllers\YandexFileAdapter;
use App\Printform_status_reference;
use App\ProjectCategory;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Implementer\ImpComment2Controller as Comments;
use App\Http\Controllers\Implementer\ImpUser2Controller as Users2;
use App\Http\Controllers\Implementer\ImpClients2Controller as Clients;
use App\Http\Controllers\Implementer\ImpCitysController as Citys;
use App\Http\Controllers\Implementer\ImpAMDateTimeController as AMDateTime;
use App\Http\Controllers\Implementer\ImpGroupTasksController as GroupTasks;
use App\Http\Controllers\Implementer\ImpModules2Controller as Modules;
use App\Http\Controllers\Implementer\ImpDocuments2Controller as Documents;
use App\Http\Controllers\Implementer\WaitingListController as WList;
use App\Http\Controllers\Implementer\ImplementerController as Imp;
use App\Project;
use App\Comment;
use App\Project_contact;
use App\Project_module;
use App\Project_task;
use App\Project_print_form;
use App\Project_task_contact;
use App\Project_additional_task;
use App\Projects_doc;
use App\Done_printform_file;
// use App\Project_print_form;
use App\Contact;
use App\Posts_reference;
use App\User;
use App\Modules_reference;
use App\Module_rule;
use App\Tasksstatus_reference;
use App\Grouptasks_reference;
use App\Clients_reference;
use App\Clarifying_question;
use App\Clarifying_answer;
use App\Check_list_reference;
use App\Check_list_answer;
use App\Close_check_list;
use Sentinel;
use Event;
use App\Events\onAddProjectEvent;
use App\Events\ImpCloseProject;
use App\Notifications\WorkoutAssigned;
use Notification;
use Auth;
use Storage;
use App\Notifications\ProjectTask;
use App\Events\saveRaw;
use App\Events\changeImp;
use Smadia\LaravelGoogleDrive\Facades\LaravelGoogleDrive as LGD;
use DB;

class ImpProjects2Controller extends Controller
{
    private $image_ext = ['jpg', 'jpeg', 'png', 'gif'];
    private $audio_ext = ['mp3', 'ogg', 'mpga'];
    private $video_ext = ['mp4', 'mpeg'];
    private $document_ext = ['doc', 'docx', 'pdf', 'odt', 'xls', 'xlsx'];
    
    public function searchProjects2(Request $request) {
        $data = json_decode($request->data);
        // dd($data);
        // $projects = DB::select("select `c`.`name` as `client_name`, `city`.`name` as `city`,
        // `u`.`first_name` as `first_name`, `u`.`last_name` as `last_name`, `p`.`*`
        // from `projects` `p`
        // left join `clients_references` `c` on `c`.`id` = `p`.`client_id`
        // left join `city_references` `city` on `city`.`id` = `c`.`city_id`
        // left join `users` `u` on `u`.`id` = `p`.`user_id`
        // where
        // `p`.`deleted_at` is null and
        // `p`.`name` like '%".$data->search."%'");
        // // dd($projects);

        // return response()->json($projects);
        // $projects_q = DB::select("select *
        // from `projects`
        // where
        // `deleted_at` is null and
        // `name` like '%".$data->search."%'");

        $cur_user = Sentinel::getUser();
        // dd('Ok');
        if ($cur_user->head != 1) {
            $count = DB::select("select count(`id`) as `count`
                from `projects`
                where
                `done` = 0 and
                `raw` is null and
                `deleted_at` is null and
                `name` like '%".$data->search."%' and
                `user_id` = ".$cur_user->id);

            $projects_q = DB::select("select *
                from `projects`
                where
                `done` = 0 and
                `raw` is null and
                `deleted_at` is null and
                `name` like '%".$data->search."%' and
                `user_id` = ".$cur_user->id);
        } else {
            $count = DB::select("select count(`id`) as `count`
                from `projects`
                where
                `done` = 0 and
                `raw` is null and
                `name` like '%".$data->search."%' and
                `deleted_at` is null");

            $projects_q = DB::select("select *
                from `projects`
                where
                `done` = 0 and
                `raw` is null and
                `name` like '%".$data->search."%' and
                `deleted_at` is null");
        }
        $num_pages = 1;
        // dd('Ok');
        $cur_date = date("j").'.'.date("n").'.'.date("Y");
        $cur_date_val = date("Y").'-'.date("m").'-'.date("d");
        $tomorrow = date('Y-m-d', strtotime($cur_date . ' +1 day'));
        $tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +1 day'));
        $plus_one_day = date('Y-m-d', strtotime($cur_date . ' +2 day'));
        $plus_one_day_val = date('Y-m-d', strtotime($cur_date . ' +2 day'));
        $plus_two_day = date('d.m.Y', strtotime($cur_date . ' +3 day'));
        $plus_two_day_val = date('Y-m-d', strtotime($cur_date . ' +3 day'));
        $plus_three_day = date('d.m.Y', strtotime($cur_date . ' +4 day'));
        $plus_three_day_val = date('Y-m-d', strtotime($cur_date . ' +4 day'));
        
        if (count($projects_q) > 0) {
            foreach ($projects_q as $p) {
                if ($cur_user->head != 1) {
                    $task = Project_task::where('project_id', '=', $p->id)
                        ->where('user_id', '=', $cur_user->id)
                        ->whereNull('deleted_at')
                        ->get();
                } else {
                    $task = Project_task::where('project_id', '=', $p->id)
                        ->whereNull('deleted_at')
                        ->get();
                }

                if (count($task) > 0) {
                    $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($ptc) > 0) {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);

                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                // 'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'prev_contact' => Carbon::parse($ptc[0]['prev_contact'])->format('d.m.Y'),
                                // 'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'next_contact' => Carbon::parse($ptc[0]['next_contact'])->format('d.m.Y'),
                                'start_date' => $p->start_date,
                                'finish_date' => Carbon::parse($p->finish_date)->format('d.m.Y'),
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            // print_r($p->id.'|');
                            $user = Users2::getUserById($p->user_id);
                            // print_r($user->last_name.'|');
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                //'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                // 'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'prev_contact' => Carbon::parse($ptc[0]['prev_contact'])->format('d.m.Y'),
                                // 'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'next_contact' => Carbon::parse($ptc[0]['next_contact'])->format('d.m.Y'),
                                'start_date' => $p->start_date,
                                'finish_date' => Carbon::parse($p->finish_date)->format('d.m.Y'),
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    } else {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);
                        //dd($client);
                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => Carbon::parse($p->finish_date)->format('d.m.Y'),
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => Carbon::parse($p->finish_date)->format('d.m.Y'),
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    }
                } else {
                    $task = Project_print_form::where('project_id', '=', $p->id)
                        ->where('user_id', '=', $cur_user->id)
                        ->whereNull('deleted_at')
                        ->get();

                    $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($ptc) > 0) {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);

                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                // 'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'prev_contact' => Carbon::parse($ptc[0]['prev_contact'])->format('d.m.Y'),
                                // 'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'next_contact' => Carbon::parse($ptc[0]['next_contact'])->format('d.m.Y'),
                                'start_date' => $p->start_date,
                                'finish_date' => Carbon::parse($p->finish_date)->format('d.m.Y'),
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                // 'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'prev_contact' => Carbon::parse($ptc[0]['prev_contact'])->format('d.m.Y'),
                                // 'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'next_contact' => Carbon::parse($ptc[0]['next_contact'])->format('d.m.Y'),
                                'start_date' => $p->start_date,
                                'finish_date' => Carbon::parse($p->finish_date)->format('d.m.Y'),
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    } else {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);
                        //dd($client);
                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => Carbon::parse($p->finish_date)->format('d.m.Y'),
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => Carbon::parse($p->finish_date)->format('d.m.Y'),
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    }
                }
            }
        } else {
            $projects = '';
        }
        

        if ($cur_user->head != 1) {
            $p_id = Project::where('user_id', '=', $cur_user->id)
                ->whereNull('deleted_at')
                ->get();
        } else {
            $p_id = Project::whereNull('deleted_at')
                ->get();
        }
        
        foreach ($p_id as $pid) {
            $user2 = User::where('id', '!=', 0)
                ->get();
            foreach ($user2 as $u2) {
                if ($u2->id == $pid->user_id) {
                    $observer2[] = [
                        'default' => 1,
                        'id' => $u2->id,
                        'first_name' => $u2->first_name,
                        'last_name' => $u2->last_name
                    ];
                } else {
                    $observer2[] = [
                        'default' => 0,
                        'id' => $u2->id,
                        'first_name' => $u2->first_name,
                        'last_name' => $u2->last_name
                    ];
                }
            }
            break;
        }
        // dd($projects);

        return response()->json(['projects' => $projects,
            // 'count' => $count[0]->count, 
            // 'count_pages' => $num_pages,
//            'project_task' => $p_tasks,
            // 'observer' => $observer2,
            // 'imp' => Imp::allImplementer(),
            // 'raw' => $raw,
            // 'cur_date' => $cur_date,
            // 'cur_date_val' => $cur_date_val,
            // 'tomorrow' => $tomorrow,
            // 'tomorrow_val' => $tomorrow_val,
            // 'plus_one_day' => $plus_one_day,
            // 'plus_one_day_val' => $plus_one_day_val,
            // 'plus_two_day' => $plus_two_day,
            // 'plus_two_day_val' => $plus_two_day_val,
            // 'plus_three_day' => $plus_three_day,
            // 'plus_three_day_val' => $plus_three_day_val
            ]);
    }

    public function index() {
        if (session('perm')['project.view']) {
            $cur_user = Sentinel::getUser();
            if ($cur_user->id == 8) {
                $raw_q = Project::where('raw' , '=', 1)
                    ->whereNULL('deleted_at')
                    ->get();
                if (count($raw_q) > 0) {
                    foreach ($raw_q as $r) {
                        $client = Clients::getClientById($r->client_id);
                        $city = Citys::getCityById($client->city_id);
                        $raw[] = [
                            'id' => $r->id,
                            'client' => $client->name,
                            'name' => $r->name,
                            'city' => $city,
                            'created_date' => Carbon::parse($r['created_at'])->format('d.m.Y H:i')
                        ];
                    }
                } else {
                    $raw = '';
                }

                $projects_q = Project::where('user_id', '!=', $cur_user->id)
                    ->whereNULL('deleted_at')
                    ->whereNULL('raw')
                    ->get();

                $cur_date = date("j").'.'.date("n").'.'.date("Y");
                $cur_date_val = date("Y").'-'.date("m").'-'.date("d");
                $tomorrow = date('Y-m-d', strtotime($cur_date . ' +1 day'));
                $tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +1 day'));
                $plus_one_day = date('Y-m-d', strtotime($cur_date . ' +2 day'));
                $plus_one_day_val = date('Y-m-d', strtotime($cur_date . ' +2 day'));
                $plus_two_day = date('d.m.Y', strtotime($cur_date . ' +3 day'));
                $plus_two_day_val = date('Y-m-d', strtotime($cur_date . ' +3 day'));
                $plus_three_day = date('d.m.Y', strtotime($cur_date . ' +4 day'));
                $plus_three_day_val = date('Y-m-d', strtotime($cur_date . ' +4 day'));

                if (count($projects_q) > 0) {
                    foreach ($projects_q as $p) {
                        $task = Project_task::where('project_id', '=', $p->id)
                            ->where('user_id', '!=', $cur_user->id)
                            ->whereNull('deleted_at')
                            ->get();

                        if (count($task) > 0) {
                            $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                                ->whereNull('deleted_at')
                                ->get();

                            if (count($ptc) > 0) {
                                $client = Clients::getClientById($p->client_id);
                                $city = Citys::getCityById($client->city_id);

                                foreach ($task as $t) {
                                    if ($t->tasksstatus_ref_id == 2) {
                                        $done2[$p->id][] = $t->id;
                                    }
                                }

                                if (isset($done2[$p->id])) {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        'name' => $p->name,
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                        'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => $p->created_at,
                                        'active' => $p->active,
                                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                    ];
                                } else {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        'name' => $p->name,
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                        'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => $p->created_at,
                                        'active' => $p->active,
                                        //'done' => 0,
                                        'done' => round(((0 / count($task)) * 100), 2),
                                    ];
                                }
                            } else {
                                $client = Clients::getClientById($p->client_id);
                                $city = Citys::getCityById($client->city_id);
                                //dd($client);
                                foreach ($task as $t) {
                                    if ($t->tasksstatus_ref_id == 2) {
                                        $done2[$p->id][] = $t->id;
                                    }
                                }

                                if (isset($done2[$p->id])) {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        'name' => $p->name,
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => NULL,
                                        'next_contact' => NULL,
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => $p->created_at,
                                        'active' => $p->active,
                                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                    ];
                                } else {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        'name' => $p->name,
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => NULL,
                                        'next_contact' => NULL,
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => $p->created_at,
                                        'active' => $p->active,
                                        //'done' => 0,
                                        'done' => round(((0 / count($task)) * 100), 2),
                                    ];
                                }
                            }
                        } else {
                            $task = Project_print_form::where('project_id', '=', $p->id)
                                ->where('user_id', '!=', $cur_user->id)
                                ->whereNull('deleted_at')
                                ->get();

                            $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                                ->whereNull('deleted_at')
                                ->get();

                            if (count($ptc) > 0) {
                                $client = Clients::getClientById($p->client_id);
                                $city = Citys::getCityById($client->city_id);

                                foreach ($task as $t) {
                                    if ($t->tasksstatus_ref_id == 2) {
                                        $done2[$p->id][] = $t->id;
                                    }
                                }

                                if (isset($done2[$p->id])) {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        'name' => $p->name,
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                        'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => $p->created_at,
                                        'active' => $p->active,
                                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                    ];
                                } else {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        'name' => $p->name,
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                        'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => $p->created_at,
                                        'active' => $p->active,
                                        //'done' => 0,
                                        'done' => round(((0 / count($task)) * 100), 2),
                                    ];
                                }
                            } else {
                                $client = Clients::getClientById($p->client_id);
                                $city = Citys::getCityById($client->city_id);
                                //dd($client);
                                foreach ($task as $t) {
                                    if ($t->tasksstatus_ref_id == 2) {
                                        $done2[$p->id][] = $t->id;
                                    }
                                }

                                if (isset($done2[$p->id])) {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        'name' => $p->name,
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => NULL,
                                        'next_contact' => NULL,
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => $p->created_at,
                                        'active' => $p->active,
                                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                    ];
                                } else {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        'name' => $p->name,
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => NULL,
                                        'next_contact' => NULL,
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => $p->created_at,
                                        'active' => $p->active,
                                        //'done' => 0,
                                        'done' => round(((0 / count($task)) * 100), 2),
                                    ];
                                }
                            }
                        }
                    }
                } else {
                    $projects = '';
                }


                $project_tasks = Project_task::where('user_id', '!=', $cur_user->id)
                    ->whereNull('deleted_at')
                    ->whereIn('tasksstatus_ref_id', [1,3])
                    ->get();

                if (count($project_tasks) > 0) {
                    foreach ($project_tasks as $pt) {
                        $name = Module_rule::find($pt->module_rule_id);
                        $p_name = Project::find($pt->project_id);
                        $p_client = Clients::getClientById($p_name['client_id']);
                        $p_city = Citys::getCityById($p_client->city_id);
                        $ptc = Project_task_contact::where('project_task_id', '=', $pt->id)
                            ->whereNull('deleted_at')
                            ->first();
                        $user = User::whereNull('deleted_at')->get();
                        foreach ($user as $u) {
                            if ($u->id == $p_name->user_id) {
                                $observer[] = [
                                    'default' => 1,
                                    'id' => $u->id,
                                    'first_name' => $u->first_name,
                                    'last_name' => $u->last_name
                                ];
                            } else {
                                $observer[] = [
                                    'default' => 0,
                                    'id' => $u->id,
                                    'first_name' => $u->first_name,
                                    'last_name' => $u->last_name
                                ];
                            }
                        }

                        if (count($ptc) > 0) {
                            if ($pt['prev_contact'] != null) {
                                $p_tasks[] = [
                                    'id' => $pt['id'],
                                    'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                    'client' => $p_client->name,
                                    'name' => $name->task,
                                    'observer' => $observer,
                                    'p_name' => $p_name['name'],
                                    'city' => $p_city,
                                    'next_contact' => substr($ptc['next_contact'], 0, 16),
                                    'prev_contact' => substr($pt['prev_contact'], 0, 16),
                                ];
                            } else {
                                $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                                    ->whereNull('deleted_at')
                                    //->whereNull('status')
                                    ->max('prev_contact');

                                $p_tasks[] = [
                                    'id' => $pt['id'],
                                    'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                    'client' => $p_client->name,
                                    'name' => $name->task,
                                    'observer' => $observer,
                                    'p_name' => $p_name['name'],
                                    'city' => $p_city,
                                    'next_contact' => substr($ptc['next_contact'], 0, 16),
                                    'prev_contact' => substr($max_date, 0, 16),
                                ];
                            }
                        } else {
                            if ($pt['prev_contact'] != null) {
                                $p_tasks[] = [
                                    'id' => $pt['id'],
                                    'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                    'client' => $p_client->name,
                                    'name' => $name->task,
                                    'observer' => $observer,
                                    'p_name' => $p_name['name'],
                                    'city' => $p_city,
                                    'next_contact' => null,
                                    'prev_contact' => substr($pt['prev_contact'], 0, 16),
                                ];
                            } else {
                                $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                                    ->whereNull('deleted_at')
                                    //->whereNull('status')
                                    ->max('prev_contact');

                                $p_tasks[] = [
                                    'id' => $pt['id'],
                                    'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                    'client' => $p_client->name,
                                    'name' => $name->task,
                                    'observer' => $observer,
                                    'p_name' => $p_name['name'],
                                    'city' => $p_city,
                                    'next_contact' => null,
                                    'prev_contact' => substr($max_date, 0, 16),
                                ];
                            }
                        }
                    }
                } else {
                    $p_tasks[] = '';
                    $observer2[] = '';
                }

                //dd($p_tasks);


                $p_id = Project::where('user_id', '!=', $cur_user->id)
                    ->whereNull('deleted_at')
                    ->get();
                foreach ($p_id as $pid) {
                    $user2 = User::where('id', '!=', 0)
                        ->get();
                    foreach ($user2 as $u2) {
                        if ($u2->id == $pid->user_id) {
                            $observer2[] = [
                                'default' => 1,
                                'id' => $u2->id,
                                'first_name' => $u2->first_name,
                                'last_name' => $u2->last_name
                            ];
                        } else {
                            $observer2[] = [
                                'default' => 0,
                                'id' => $u2->id,
                                'first_name' => $u2->first_name,
                                'last_name' => $u2->last_name
                            ];
                        }
                    }
                    break;
                }
                //dd($projects);
            } else {
                $raw_q = Project::where('raw' , '=', 1)
                    ->whereNULL('deleted_at')
                    ->get();
                if (count($raw_q) > 0) {
                    foreach ($raw_q as $r) {
                        $client = Clients::getClientById($r->client_id);
                        $city = Citys::getCityById($client->city_id);
                        $raw[] = [
                            'id' => $r->id,
                            'client' => $client->name,
                            'name' => $r->name,
                            'city' => $city,
                            'created_date' => Carbon::parse($r['created_at'])->format('d.m.Y H:i')
                        ];
                    }
                } else {
                    $raw = '';
                }

                $projects_q = Project::where('user_id', '=', $cur_user->id)
                    ->whereNULL('deleted_at')
                    ->whereNULL('raw')
                    ->get();

                $cur_date = date("j").'.'.date("n").'.'.date("Y");
                $cur_date_val = date("Y").'-'.date("m").'-'.date("d");
                $tomorrow = date('Y-m-d', strtotime($cur_date . ' +1 day'));
                $tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +1 day'));
                $plus_one_day = date('Y-m-d', strtotime($cur_date . ' +2 day'));
                $plus_one_day_val = date('Y-m-d', strtotime($cur_date . ' +2 day'));
                $plus_two_day = date('d.m.Y', strtotime($cur_date . ' +3 day'));
                $plus_two_day_val = date('Y-m-d', strtotime($cur_date . ' +3 day'));
                $plus_three_day = date('d.m.Y', strtotime($cur_date . ' +4 day'));
                $plus_three_day_val = date('Y-m-d', strtotime($cur_date . ' +4 day'));

                if (count($projects_q) > 0) {
                    foreach ($projects_q as $p) {
                        $task = Project_task::where('project_id', '=', $p->id)
                            ->where('user_id', '=', $cur_user->id)
                            ->whereNull('deleted_at')
                            ->get();

                        if (count($task) > 0) {
                            $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                                ->whereNull('deleted_at')
                                ->get();

                            if (count($ptc) > 0) {
                                $client = Clients::getClientById($p->client_id);
                                $city = Citys::getCityById($client->city_id);

                                foreach ($task as $t) {
                                    if ($t->tasksstatus_ref_id == 2) {
                                        $done2[$p->id][] = $t->id;
                                    }
                                }

                                if (isset($done2[$p->id])) {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        'name' => $p->name,
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                        'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => $p->created_at,
                                        'active' => $p->active,
                                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                    ];
                                } else {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        'name' => $p->name,
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                        'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => $p->created_at,
                                        'active' => $p->active,
                                        //'done' => 0,
                                        'done' => round(((0 / count($task)) * 100), 2),
                                    ];
                                }
                            } else {
                                $client = Clients::getClientById($p->client_id);
                                $city = Citys::getCityById($client->city_id);
                                //dd($client);
                                foreach ($task as $t) {
                                    if ($t->tasksstatus_ref_id == 2) {
                                        $done2[$p->id][] = $t->id;
                                    }
                                }

                                if (isset($done2[$p->id])) {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        'name' => $p->name,
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => NULL,
                                        'next_contact' => NULL,
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => $p->created_at,
                                        'active' => $p->active,
                                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                    ];
                                } else {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        'name' => $p->name,
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => NULL,
                                        'next_contact' => NULL,
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => $p->created_at,
                                        'active' => $p->active,
                                        //'done' => 0,
                                        'done' => round(((0 / count($task)) * 100), 2),
                                    ];
                                }
                            }
                        } else {
                            $task = Project_print_form::where('project_id', '=', $p->id)
                                ->where('user_id', '=', $cur_user->id)
                                ->whereNull('deleted_at')
                                ->get();

                            $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                                ->whereNull('deleted_at')
                                ->get();

                            if (count($ptc) > 0) {
                                $client = Clients::getClientById($p->client_id);
                                $city = Citys::getCityById($client->city_id);

                                foreach ($task as $t) {
                                    if ($t->tasksstatus_ref_id == 2) {
                                        $done2[$p->id][] = $t->id;
                                    }
                                }

                                if (isset($done2[$p->id])) {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        'name' => $p->name,
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                        'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => $p->created_at,
                                        'active' => $p->active,
                                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                    ];
                                } else {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        'name' => $p->name,
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                        'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => $p->created_at,
                                        'active' => $p->active,
                                        //'done' => 0,
                                        'done' => round(((0 / count($task)) * 100), 2),
                                    ];
                                }
                            } else {
                                $client = Clients::getClientById($p->client_id);
                                $city = Citys::getCityById($client->city_id);
                                //dd($client);
                                foreach ($task as $t) {
                                    if ($t->tasksstatus_ref_id == 2) {
                                        $done2[$p->id][] = $t->id;
                                    }
                                }

                                if (isset($done2[$p->id])) {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        'name' => $p->name,
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => NULL,
                                        'next_contact' => NULL,
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => $p->created_at,
                                        'active' => $p->active,
                                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                    ];
                                } else {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        'name' => $p->name,
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => NULL,
                                        'next_contact' => NULL,
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => $p->created_at,
                                        'active' => $p->active,
                                        //'done' => 0,
                                        'done' => round(((0 / count($task)) * 100), 2),
                                    ];
                                }
                            }
                        }
                    }
                } else {
                    $projects = '';
                }


                $project_tasks = Project_task::where('user_id', '=', $cur_user->id)
                    ->whereNull('deleted_at')
                    ->whereIn('tasksstatus_ref_id', [1,3])
                    ->get();

                if (count($project_tasks) > 0) {
                    foreach ($project_tasks as $pt) {
                        $name = Module_rule::find($pt->module_rule_id);
                        $p_name = Project::find($pt->project_id);
                        $p_client = Clients::getClientById($p_name['client_id']);
                        $p_city = Citys::getCityById($p_client->city_id);
                        $ptc = Project_task_contact::where('project_task_id', '=', $pt->id)
                            ->whereNull('deleted_at')
                            ->first();
                        $user = User::whereNull('deleted_at')->get();
                        foreach ($user as $u) {
                            if ($u->id == $p_name->user_id) {
                                $observer[] = [
                                    'default' => 1,
                                    'id' => $u->id,
                                    'first_name' => $u->first_name,
                                    'last_name' => $u->last_name
                                ];
                            } else {
                                $observer[] = [
                                    'default' => 0,
                                    'id' => $u->id,
                                    'first_name' => $u->first_name,
                                    'last_name' => $u->last_name
                                ];
                            }
                        }

                        if (count($ptc) > 0) {
                            if ($pt['prev_contact'] != null) {
                                $p_tasks[] = [
                                    'id' => $pt['id'],
                                    'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                    'client' => $p_client->name,
                                    'name' => $name->task,
                                    'observer' => $observer,
                                    'p_name' => $p_name['name'],
                                    'city' => $p_city,
                                    'next_contact' => substr($ptc['next_contact'], 0, 16),
                                    'prev_contact' => substr($pt['prev_contact'], 0, 16),
                                ];
                            } else {
                                $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                                    ->whereNull('deleted_at')
                                    //->whereNull('status')
                                    ->max('prev_contact');

                                $p_tasks[] = [
                                    'id' => $pt['id'],
                                    'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                    'client' => $p_client->name,
                                    'name' => $name->task,
                                    'observer' => $observer,
                                    'p_name' => $p_name['name'],
                                    'city' => $p_city,
                                    'next_contact' => substr($ptc['next_contact'], 0, 16),
                                    'prev_contact' => substr($max_date, 0, 16),
                                ];
                            }
                        } else {
                            if ($pt['prev_contact'] != null) {
                                $p_tasks[] = [
                                    'id' => $pt['id'],
                                    'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                    'client' => $p_client->name,
                                    'name' => $name->task,
                                    'observer' => $observer,
                                    'p_name' => $p_name['name'],
                                    'city' => $p_city,
                                    'next_contact' => null,
                                    'prev_contact' => substr($pt['prev_contact'], 0, 16),
                                ];
                            } else {
                                $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                                    ->whereNull('deleted_at')
                                    //->whereNull('status')
                                    ->max('prev_contact');

                                $p_tasks[] = [
                                    'id' => $pt['id'],
                                    'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                    'client' => $p_client->name,
                                    'name' => $name->task,
                                    'observer' => $observer,
                                    'p_name' => $p_name['name'],
                                    'city' => $p_city,
                                    'next_contact' => null,
                                    'prev_contact' => substr($max_date, 0, 16),
                                ];
                            }
                        }
                    }
                } else {
                    $p_tasks[] = '';
                    $observer2[] = '';
                }

                //dd($p_tasks);


                $p_id = Project::where('user_id', '=', $cur_user->id)
                    ->whereNull('deleted_at')
                    ->get();
                foreach ($p_id as $pid) {
                    $user2 = User::where('id', '!=', 0)
                        ->get();
                    foreach ($user2 as $u2) {
                        if ($u2->id == $pid->user_id) {
                            $observer2[] = [
                                'default' => 1,
                                'id' => $u2->id,
                                'first_name' => $u2->first_name,
                                'last_name' => $u2->last_name
                            ];
                        } else {
                            $observer2[] = [
                                'default' => 0,
                                'id' => $u2->id,
                                'first_name' => $u2->first_name,
                                'last_name' => $u2->last_name
                            ];
                        }
                    }
                    break;
                }
                //dd($projects);
            }

            return view('Implementer.projects.projects', [
                'projects' => $projects,
                'project_task' => $p_tasks,
                'observer' => $observer2,
                'raw' => $raw,
                'cur_date' => $cur_date,
                'cur_date_val' => $cur_date_val,
                'tomorrow' => $tomorrow,
                'tomorrow_val' => $tomorrow_val,
                'plus_one_day' => $plus_one_day,
                'plus_one_day_val' => $plus_one_day_val,
                'plus_two_day' => $plus_two_day,
                'plus_two_day_val' => $plus_two_day_val,
                'plus_three_day' => $plus_three_day,
                'plus_three_day_val' => $plus_three_day_val
            ]);
        } else {
            abort(503);
        }
    }

    /**
     * Get all extensions
     * @return array Extensions of all file types
     */
    private function allExtensions()
    {
        return array_merge($this->image_ext, $this->audio_ext, $this->video_ext, $this->document_ext);
    }

    /**
     * Get type by extension
     * @param string $ext Specific extension
     * @return string   Type
     */
    private function getType($ext)
    {
        if (in_array($ext, $this->image_ext)) {
            return 'image';
        }

        if (in_array($ext, $this->audio_ext)) {
            return 'audio';
        }

        if (in_array($ext, $this->video_ext)) {
            return 'video';
        }

        if (in_array($ext, $this->document_ext)) {
            return 'document';
        }

        return false;
    }

    public function getChekList(Request $request) {
        $data = json_decode($request->data);

        $file = Close_check_list::where('project_id', '=', $data->id)
            ->whereNull('deleted_at')
            ->first();

        if ($file) {
            return response()->json(['status' => 'Ok', 'file' => $file]);
        } else {
            return response()->json(['status' => 'Ok', 'file' => null]);
        }


    }

    public function getPTask($id, Request $request) {
        $project = Project::find($id);
        $cur_user = Sentinel::getUser();

        if ($cur_user->head != 1) {
            $project_tasks = DB::select('select `pt`.`id`, `mr`.`task` as `name`, `m`.`name` as `m_name`, `gr`.`name` as `g_task`, 
                `mr`.`additional` as `additional`, `tr`.`name` as `status_name`, `pt`.`tasksstatus_ref_id` as `status`,
                `ptc`.`callbackstatus_ref_id`, `ptc`.`next_contact`, `gr`.`is_print_form`, `gr`.`is_screen_form`,
                `pt`.`user_id`
                from `project_tasks` `pt`
                left join `module_rules` `mr` on `pt`.`module_rule_id` = `mr`.`id`
                left join `modules_references` `m` on `m`.`id` = `mr`.`module_id`
                left join `grouptasks_references` `gr` on `gr`.`id` = `mr`.`grouptasks_id`
                left join `tasksstatus_references` `tr` on `tr`.`id` = `pt`.`tasksstatus_ref_id`
                left join `project_task_contacts` `ptc` on `ptc`.`project_task_id` = `pt`.`id`
                where
                `pt`.`deleted_at` is null and
                `ptc`.`deleted_at` is null and
                `pt`.`user_id` = '.$cur_user->id.' and
                `pt`.`project_id` = '.$id);
        } else {
            $project_tasks = DB::select('select `pt`.`id`, `mr`.`task` as `name`, `m`.`name` as `m_name`, `gr`.`name` as `g_task`, 
                `mr`.`additional` as `additional`, `tr`.`name` as `status_name`, `pt`.`tasksstatus_ref_id` as `status`,
                `ptc`.`callbackstatus_ref_id`, `ptc`.`next_contact`, `gr`.`is_print_form`, `gr`.`is_screen_form`,
                `pt`.`user_id`
                from `project_tasks` `pt`
                left join `module_rules` `mr` on `pt`.`module_rule_id` = `mr`.`id`
                left join `modules_references` `m` on `m`.`id` = `mr`.`module_id`
                left join `grouptasks_references` `gr` on `gr`.`id` = `mr`.`grouptasks_id`
                left join `tasksstatus_references` `tr` on `tr`.`id` = `pt`.`tasksstatus_ref_id`
                left join `project_task_contacts` `ptc` on `ptc`.`project_task_id` = `pt`.`id`
                where
                `pt`.`deleted_at` is null and
                `ptc`.`deleted_at` is null and
                `pt`.`project_id` = '.$id);
        }

        foreach ($project_tasks as &$project_task) {
            $project_task->is_print_form = $project_task->is_print_form ?? 0;
            $project_task->is_screen_form = $project_task->is_screen_form ?? 0;
        }

        // ------------------------ Даты для планирования задач ----------------------------
        $cur_date = date("j") . '.' . date("n") . '.' . date("Y");
        $cur_date_val = date("Y") . '-' . date("m") . '-' . date("d");
        $tomorrow = date('Y-m-d', strtotime($cur_date . ' +1 day'));
        $tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +1 day'));
        $plus_one_day = date('Y-m-d', strtotime($cur_date . ' +2 day'));
        $plus_one_day_val = date('Y-m-d', strtotime($cur_date . ' +2 day'));
        $plus_two_day = date('d.m.Y', strtotime($cur_date . ' +3 day'));
        $plus_two_day_val = date('Y-m-d', strtotime($cur_date . ' +3 day'));
        $plus_three_day = date('d.m.Y', strtotime($cur_date . ' +4 day'));
        $plus_three_day_val = date('Y-m-d', strtotime($cur_date . ' +4 day'));
        // --------------------------------------------------------------------------------

        // ---------------------------------- Нормы ---------------------------------------
        $count_norm = 0;
        $count_number = 0;
        $count_print_norm = 0;
        $count_print_number = 0;
        $count_screen_norm = 0;
        $count_screen_number = 0;


        // --------------------------------------------------------------------------------


        return response()->json(['project_tasks' => $project_tasks,
            'cur_date' => $cur_date,
            'cur_date_val' => $cur_date_val,
            'tomorrow' => $tomorrow,
            'tomorrow_val' => $tomorrow_val,
            'plus_one_day' => $plus_one_day,
            'plus_one_day_val' => $plus_one_day_val,
            'plus_two_day' => $plus_two_day,
            'plus_two_day_val' => $plus_two_day_val,
            'plus_three_day' => $plus_three_day,
            'plus_three_day_val' => $plus_three_day_val,
            'count_norm' => $count_norm / 2,
            'count_number' => $count_number / 2,
            'count_print_norm' => $count_print_norm,
            'count_print_number' => $count_print_number,
            'count_screen_norm' => $count_screen_norm,
            'count_screen_number' => $count_screen_number,
            // 'count_fact' => count($count_fact),
            // 'remained' => $remained,
        ]);
    }

    public function getPCard($id, Request $request) {
        $project = Project::find($id);
        $cur_user = Sentinel::getUser();

        // ------------------------ Дата начала и окончания проекта ----------------------
        $project_start_date = Carbon::parse($project->start_date)->format('d.m.Y');
        $project_finish_date = Carbon::parse($project->finish_date)->format('d.m.Y');
        // -------------------------------------------------------------------------------

        // ----------------------- Купленные модули --------------------------------------
        $modules = DB::select('select `pm`.`id`, `m`.`name`, `pm`.`num`, `m`.`id` as `mid`
            from `project_modules` `pm`
            left join `modules_references` `m` on `m`.`id` = `pm`.`module_id`
            where
            `pm`.`deleted_at` is null and
            `pm`.`project_id` ='.$id);
        // -------------------------------------------------------------------------------

        // ----------------------------- Чек лист ----------------------------------------
        $check = DB::select('select `cla`.`id`, `cl`.`question`, `cla`.`answer`
            from `check_list_references` `cl`
            left join `check_list_answers` `cla` on `cla`.`check_list_ref_id` = `cl`.`id`
            where
            `cl`.`deleted_at` is null and
            `cla`.`project_id` ='.$id);
        // -------------------------------------------------------------------------------

        // ------------------------- Уточняющие вопросы ----------------------------------
        $cq = DB::select('select `mr`.`task`, `m`.`id` as `mid`, `mr`.`id` as `mrid`, 
            `cq`.`question` as `question`, `ca`.`answer` as `answer`,
            `cq`.`id` as `gid`, `ca`.`id` as `aid`
            from `project_modules` `pm`
            left join `modules_references` `m` on `m`.`id` = `pm`.`module_id`
            left join `module_rules` `mr` on `m`.`id` = `mr`.`module_id`
            left join `clarifying_questions` `cq` on `cq`.`task_id` =  `mr`.`id`
            left join `clarifying_answers` `ca` on (`ca`.`question_id` = `cq`.`id` and `ca`.`question_id` is not null and `ca`.`deleted_at` is null and `ca`.`project_id` = '.$id.')
            where
            `mr`.`additional` is null and
            `mr`.`deleted_at` is null and
            `pm`.`deleted_at` is null and
            `cq`.`id` is not null and
            `pm`.`project_id` = '.$id);
        // -------------------------------------------------------------------------------

        return response()->json([
            'project_start_date' => $project_start_date,
            'project_finish_date' => $project_finish_date,
            'modules_buy' => $modules,
            'check' => $check,
            'questions' => $cq
        ]);
    }

    public function getModules() {
        $modules = DB::select("select `mr`.`id`, `mr`.`name`
            from `modules_references` `mr`
            where 
            `mr`.`deleted_at` is null");

        return response()->json($modules);
    }

    public function setModules(Request $request) {
        $data = json_decode($request->data);

        $cur_user = Sentinel::getUser();

        $pm = Project_module::where('project_id', '=', $data->id)
            ->whereNull('deleted_at')
            ->get();

        $success = false;

        if (count((array)$data->new_Module) != 0) {
            // dd($data);
            $modules = new Project_module();
            $modules->module_id = (int)$data->new_Module->id;
            $modules->project_id = $data->id;
            $modules->num = (int)$data->new_Module->num;
            $modules->save();

            $m_rule = Module_rule::where('module_id', '=', (int)$data->new_Module->id)
                ->whereNull('deleted_at')
                ->whereNull('additional')
                ->get();

            foreach ($m_rule as $mr) {
                $p_task = new Project_task();
                $p_task->project_id = $data->id;
                $p_task->user_id = NULL;
                $p_task->observer_id = $cur_user->id;
                $p_task->module_rule_id = $mr->id;
                $p_task->norm_contacts = $mr->norm_contacts;
                $p_task->number_contacts = $mr->number_contacts;
                $p_task->module_id = $data->new_Module->id;
                $p_task->tasksstatus_ref_id = 1;
                if ($p_task->save()) {
                    $success = true;
                } else {
                    $success = false;
                };
            }
        };

        foreach ($data->modules as $m) {
            $module = Project_module::find($m->id);
            // $module->module_id = (int)$m->mid;
            // $module->project_id = $data->id;
            $module->num = (int)$m->num;
            if ($module->save()) {
                $success = true;
            } else {
                $success = false;
            };
        };

        if ($success == true) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function getProject($id, Request $request) {
        $project = Project::with('projectCategory:id,name')->find($id);
        $cur_user = Sentinel::getUser();

        // ----------------------------- Контакты проекта ---------------------------------
        $contacts = DB::select('select `c`.`id` as `id`, `pc`.`main`, `pc`.`mail_rep`, `c`.`first_name`, 
            `c`.`last_name`, `c`.`patronymic`, `pr`.`name` as `post_name`, `pr`.`id` as `post_id`, `c`.`phone`, `c`.`email`
            from `project_contacts` `pc`
            left join `contacts` `c` on `pc`.`contact_id` = `c`.`id`
            left join `posts_references` `pr` on `pr`.`id` = `c`.`post_id`
            where
            `pc`.`deleted_at` is null and
            `c`.`deleted_at` is null and
            `pc`.`project_id` = '.$id);
        // ---------------------------------------------------------------------------------

        return response()->json([
            'head' => $cur_user->head,
            'project' => $project,
            'contacts' => $contacts,
        ]);
    }

    public function getProject2($id, Request $request)
    {
        $project = Project::find($id);
        $project_start_date = Carbon::parse($project->start_date)->format('d.m.Y');
        $project_finish_date = Carbon::parse($project->finish_date)->format('d.m.Y');

        if ($project) {
            $cur_date = date("j") . '.' . date("n") . '.' . date("Y");
            $cur_date_val = date("Y") . '-' . date("m") . '-' . date("d");
            $tomorrow = date('Y-m-d', strtotime($cur_date . ' +1 day'));
            $tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +1 day'));
            $plus_one_day = date('Y-m-d', strtotime($cur_date . ' +2 day'));
            $plus_one_day_val = date('Y-m-d', strtotime($cur_date . ' +2 day'));
            $plus_two_day = date('d.m.Y', strtotime($cur_date . ' +3 day'));
            $plus_two_day_val = date('Y-m-d', strtotime($cur_date . ' +3 day'));
            $plus_three_day = date('d.m.Y', strtotime($cur_date . ' +4 day'));
            $plus_three_day_val = date('Y-m-d', strtotime($cur_date . ' +4 day'));

            // $comments = Comments::getCommentsByProjectId($id);

            $contacts = Project_contact::where('project_id', '=', $id)
                ->whereNULL('deleted_at')
                ->orderBy('id', 'asc')
                ->get();

            foreach ($contacts as $c) {
                $cont = Contact::where('id', '=', $c->contact_id)
                    ->whereNull('deleted_at')
                    ->first();

                //dd($cont);
                if (!is_null($cont)) {
                    $post = Posts_reference::find($cont->post_id);

                    if ($cont->post_id == 1) {
                        $ar_contact[] = [
                            'first_name' => $cont->first_name,
                            'last_name' => $cont->last_name,
                            'patronymic' => $cont->patronymic,
                            'phone' => $cont->phone,
                            'email' => $cont->email,
                            'post' => $post->name ?? '',
                            'id' => $cont->id,
                            'director' => 1,
                            'main' => $c->main,
                        ];
                    } else {
                        $ar_contact[] = [
                            'first_name' => $cont->first_name,
                            'last_name' => $cont->last_name,
                            'patronymic' => $cont->patronymic,
                            'phone' => $cont->phone,
                            'email' => $cont->email,
                            'post' => $post['name'] ?? '',
                            'id' => $cont->id,
                            'director' => 0,
                            'main' => $c->main,
                        ];
                    }
                } else {
                    $ar_contact[] = '';
                }

            }

            $modules = Project_module::where('project_id', '=', $id)
                ->whereNull('deleted_at')
                ->get();

            foreach ($modules as $m) {
                $name = Modules_reference::find($m->module_id);

                $all_modules[] = [
                    'id' => $name['id'],
                    'name' => $name['name'],
                    'num' => $m->num
                ];
            }

            // dd($all_modules);
            // foreach ($all_modules as $all) {
            //     $task_q = Module_rule::where('module_id', '=', $all['id'])
            //         ->whereNull('deleted_at')
            //         ->get();

            //     foreach ($task_q as $q) {
            //         print_r($q->questions);
            //         if (count($q->questions) > 0) {
            //             $questions_ar[] = [
            //                 'm_id' => $all['id'],
            //                 'id' => $q->id,
            //                 'questions' => $q->questions
            //             ];
            //         } else {
            //             $questions_ar = [];
            //         }
            //     }
            // }
            // die;
            $questions_ar = [];


            $cur_user = Sentinel::getUser();
            $count_norm = 0;
            $count_number = 0;
            $count_print_norm = 0;
            $count_print_number = 0;
            $count_screen_norm = 0;
            $count_screen_number = 0;

            if ($cur_user->head != 1) {
                $project_tasks = Project_task::where('user_id', '=', $cur_user->id)
                    ->where('project_id', '=', $id)
                    ->whereNull('deleted_at')
                    //->whereNull('status')
                    //->whereNull('additional_id')
                    ->get();
            } else {
                $project_tasks = Project_task::where('project_id', '=', $id)
                    ->whereNull('deleted_at')
                    //->whereNull('status')
                    //->whereNull('additional_id')
                    ->get();
            }

            if (count($project_tasks) > 0) {
                $observer_id = $project_tasks[0]['observer_id'];
                $print_form_id = Grouptasks_reference::select('id')
                    ->where('is_print_form', '=', 1)
                    ->get();


                $screen_form_id = Grouptasks_reference::select('id')
                    ->where('is_screen_form', '=', 1)
                    ->get();


                foreach ($project_tasks as $pt) {
                    if ($pt->module_rule_id || $pt->module_id) {
                        $name = Module_rule::where('id', '=', $pt->module_rule_id)
                            ->whereNull('deleted_at')
                            ->first();
                    } else {
                        $name = Module_rule::where('project_tasks_id', '=', $pt->id)
                            ->whereNull('deleted_at')
                            ->first();
                    }

                    $ptc = Project_task_contact::where('project_task_id', '=', $pt->id)
                        ->whereNull('deleted_at')
                        ->first();
                    $p_name = Project::find($pt->project_id);
                    if ($pt->module_id) {
                        $m_name = Modules_reference::find($pt->module_id);
                    } else {
                        $m_name = [];
                    }

                    if (count($name) != 0) {
                        $g_task = Grouptasks_reference::find($name['grouptasks_id']);
                    } else {
                        $g_task = [];
                    }

                    if (count($g_task) != 0) {
                        if ($g_task['is_print_form'] == 1) {
                            $type = 'print_form';
                        } elseif ($g_task['is_screen_form'] == 1) {
                            $type = 'screen_form';
                        } else {
                            $type = 'task';
                        }
                    } else {
                        $type = '';
                    }

                    $date = explode(' ', $ptc['next_contact']);
                    if (count($name) != 0 /*|| count($m_name) != 0 || count($g_task) != 0*/) {

                        if (count($ptc) > 0) {

                            $p_tasks[] = [
                                'id' => $pt->id,
                                'name' => $name['task'],
                                'p_name' => $p_name->name,
                                'observer_id' => $pt->observer_id,
                                'city' => $p_name->city,
                                'm_name' => $m_name->name,
                                'callbackstatus_ref_id' => $ptc['callbackstatus_ref_id'],
                                'g_task' => $g_task['name'],
                                'type' => $type,
                                'additional' => $name['additional'],
                                'status' => $pt->tasksstatus_ref_id,
                                'next_contact' => $date[0]
                            ];

                        } else {
                            $p_tasks[] = [
                                'id' => $pt->id,
                                'name' => $name['task'],
                                'p_name' => $p_name->name,
                                'observer_id' => $pt->observer_id,
                                'city' => $p_name->city,
                                'm_name' => $m_name->name,
                                'callbackstatus_ref_id' => $ptc['callbackstatus_ref_id'],
                                'g_task' => $g_task['name'],
                                'type' => $type,
                                'additional' => $name['additional'],
                                'status' => $pt->tasksstatus_ref_id,
                                'next_contact' => null
                            ];

                        }

                        foreach ($print_form_id as $pfid) {
                            if ($name['grouptasks_id'] != $pfid->id) {
                                $count_norm += $pt->norm_contacts;
                                $count_number += $pt->number_contacts;
                            } else {
                                $count_print_norm += $pt->norm_contacts;
                                $count_print_number += $pt->number_contacts;
                            }
                        }

                        foreach ($screen_form_id as $sfid) {
                            if ($name['grouptasks_id'] != $sfid->id) {
                                $count_norm += $pt->norm_contacts;
                                $count_number += $pt->number_contacts;
                            } else {
                                $count_screen_norm += $pt->norm_contacts;
                                $count_screen_number += $pt->number_contacts;
                            }
                        }

                    } else {

                        if (count($ptc) > 0) {
                            $p_tasks[] = [
                                'id' => $pt->id,
                                'name' => 'name',
                                'p_name' => $p_name->name,
                                'observer_id' => $pt->observer_id,
                                'city' => $p_name->city,
                                'm_name' => 'm_name',
                                'callbackstatus_ref_id' => $ptc['callbackstatus_ref_id'],
                                'g_task' => 'g_task',
                                'type' => $type,
                                'additional' => 'additional',
                                'status' => $pt->tasksstatus_ref_id,
                                'next_contact' => $date[0]
                            ];
                        } else {
                            $p_tasks[] = [
                                'id' => $pt->id,
                                'name' => 'name',
                                'p_name' => $p_name->name,
                                'observer_id' => $pt->observer_id,
                                'city' => $p_name->city,
                                'm_name' => 'm_name',
                                'callbackstatus_ref_id' => $ptc['callbackstatus_ref_id'],
                                'g_task' => 'g_task',
                                'type' => $type,
                                'additional' => 'additional',
                                'status' => $pt->tasksstatus_ref_id,
                                'next_contact' => null
                            ];

                        }

                       foreach ($print_form_id as $pfid) {
                           if ($name['grouptasks_id'] != $pfid->id) {
                               $count_norm += $pt->norm_contacts;
                               $count_number += $pt->number_contacts;
                           } else {
                               $count_print_norm += $pt->norm_contacts;
                               $count_print_number += $pt->number_contacts;
                           }
                       }

                       foreach ($screen_form_id as $sfid) {
                           if ($name['grouptasks_id'] != $sfid->id) {
                               $count_norm += $pt->norm_contacts;
                               $count_number += $pt->number_contacts;
                           } else {
                               $count_screen_norm += $pt->norm_contacts;
                               $count_screen_number += $pt->number_contacts;
                           }
                       }
                    }
                }
                die;
            } else {
                $observer_id = '';
                $p_tasks = '';
            }

            $count_fact = Project_task_contact::where('project_id', '=', $project->id)
                ->whereNull('deleted_at')
                ->whereNotNull('duration')
                ->get();

            $remained = 0;
            foreach ($count_fact as $cf) {
                switch ($cf->duration) {
                    case '00:15:00':
                        $remained += 0.5;
                        break;
                    case '00:30:00':
                        $remained += 1;
                        break;
                    case '00:45:00':
                        $remained += 1;
                        break;
                    case '01:00:00':
                        $remained += 1;
                        break;
                    case '01:30:00':
                        $remained += 1.5;
                        break;
                }
            }
            //dd($remained);

            $project_tasks2 = Project_task::where('user_id', '=', $cur_user->id)
                ->where('project_id', '=', $id)
                ->whereNull('deleted_at')
                ->get();

            if (count($project_tasks2) > 0) {
                foreach ($project_tasks2 as $pt) {
                    if ($pt->module_rule_id && $pt->module_id) {
                        $name = Module_rule::where('id', '=', $pt->module_rule_id)
                            ->whereNull('deleted_at')
                            ->first();

                        if ($name['additional'] == null) {
                            $c_question = Clarifying_question::where('task_id', '=', $name['id'])
                                ->whereNull('deleted_at')
                                ->get();

                            if (count($c_question) > 0) {
                                foreach ($c_question as $q) {
                                    $q_ar = [
                                        'id' => $q->id,
                                        'question' => $q->question
                                    ];
                                    $q_ar2[] = $q_ar;
                                }
                            }
                        }
                    }


                }
            }

            if (isset($q_ar2)) {
                foreach ($q_ar2 as $cq) {
                    $c_answer = Clarifying_answer::where('question_id', '=', $cq['id'])
                        ->where('project_id', '=', $id)
                        ->whereNull('deleted_at')
                        ->first();

                    $question[] = [
                        'id' => $cq['id'],
                        'question' => $cq['question'],
                        'answer' => $c_answer['answer']
                    ];

                }
            } else {
                $question = [];
            }
            //dd($q_ar2);

            if (count($question) > 0) {
                foreach ($question as $q) {
                    $c_answer = Clarifying_answer::where('question_id', '=', $q['id'])
                        ->where('project_id', '=', $id)
                        ->whereNull('deleted_at')
                        ->first();

                    //dd($c_answer['answer']);

                    if ($c_answer['answer'] != 'null') {
                        $answer[] = [
                            'q_id' => $q['id'],
                            'answer' => $c_answer['answer']
                        ];
                    } else {
                        $answer = [];
                    }
                }
            } else {
                $answer = [];
            }

            //dd($project_tasks2);

            $user = User::whereNull('deleted_at')->get();
            foreach ($user as $u) {
                if ($u->id == $project->user_id) {
                    $observer[] = [
                        'default' => 1,
                        'id' => $u->id,
                        'first_name' => $u->first_name,
                        'last_name' => $u->last_name
                    ];
                } else {
                    $observer[] = [
                        'default' => 0,
                        'id' => $u->id,
                        'first_name' => $u->first_name,
                        'last_name' => $u->last_name
                    ];
                }
            }

            // $doc_types = Documents::getAllDocumentsType();
            // $doc_input = Documents::getProjectDocsByType(1, $id);
            // $doc_output = Documents::getProjectDocsByType(2, $id);




            $client = Clients_reference::find($project->client_id);

            $check_list_answer = Check_list_answer::where('project_id', '=', $id)
                ->whereNull('deleted_at')
                ->get();
            foreach ($check_list_answer as $check_answer) {
                $check_list_question = Check_list_reference::where('id', '=', $check_answer->check_list_ref_id)
                    ->whereNull('deleted_at')
                    ->first();

                $check_list[] = [
                    'id' => $check_list_question['id'],
                    'question' => strip_tags($check_list_question['question']),
                    'answer' => $check_answer->answer
                ];
            }

            $posts = Posts_reference::whereNull('deleted_at')
                ->get();

            return response()->json([
                'head' => $cur_user->head,
                'project' => $project,
                'project_start_date' => $project_start_date,
                'project_finish_date' => $project_finish_date,
                // 'forms' => $forms,
                'observer_id' => $observer_id,
                // 'comments' => $comments,
                'contacts' => $ar_contact,
                'modules' => $all_modules,
                'all_modules' => Modules::getModulesByProjectId($project->id),
                'project_task' => $p_tasks,
                // 'project_form' => $p_form,
                // 'group_task' => GroupTasks::getAllTasks(),
                'all_group_task' => GroupTasks::getAllTaskGroup(),
                'observer' => $observer,
                'cur_date' => $cur_date,
                'cur_date_val' => $cur_date_val,
                'tomorrow' => $tomorrow,
                'tomorrow_val' => $tomorrow_val,
                'plus_one_day' => $plus_one_day,
                'plus_one_day_val' => $plus_one_day_val,
                'plus_two_day' => $plus_two_day,
                'plus_two_day_val' => $plus_two_day_val,
                'plus_three_day' => $plus_three_day,
                'plus_three_day_val' => $plus_three_day_val,
                'client' => $client,
                'questions' => $questions_ar,
                'question' => $question,
                'answer' => $answer,
                'count_norm' => $count_norm / 2,
                'count_number' => $count_number / 2,
                'count_print_norm' => $count_print_norm,
                'count_print_number' => $count_print_number,
                'count_screen_norm' => $count_screen_norm,
                'count_screen_number' => $count_screen_number,
                'count_fact' => count($count_fact),
                'remained' => $remained,
                // 'doc_types' => $doc_types,
                // 'doc_input' => $doc_input,
                // 'doc_output' => $doc_output,
                'check_list' => $check_list,
                'posts' => $posts
            ]);
        }
    }

    public function getProjects() {
        $cur_user = Sentinel::getUser();

        $raw_q = Project::where('raw' , '=', 1)
            ->whereNULL('deleted_at')
            ->get();

        if (count($raw_q) > 0) {
            foreach ($raw_q as $r) {

                $client = Clients::getClientById($r->client_id);

                $city = Citys::getCityById($client->city_id);
                $raw[] = [
                    'id' => $r->id,
                    'client' => $client->name,
                    'name' => $r->name,
                    'city' => $city,
                    'created_date' => Carbon::parse($r['created_at'])->format('d.m.Y H:i')
                ];
            }
        } else {
            $raw = '';
        }

        // print_r('Ok');
        //     die;


        if (isset($_GET['count'])) $per_page = (int)$_GET['count']; else $per_page = 10;
        // $per_page = 10;
        // $per_page = (int)$per_page;

        if (isset($_GET['page'])) $page = ($_GET['page']-1); else $page=0;


        if ($per_page == 0) {

            if ($cur_user->head != 1) {
                $count = DB::select('select count(`id`) as `count`
                    from `projects`
                    where
                    `done` = 0 and
                    `raw` is null and
                    `deleted_at` is null and
                    `user_id` = '.$cur_user->id);

                $projects_q = DB::select('select *
                    from `projects`
                    where
                    `done` = 0 and
                    `raw` is null and
                    `deleted_at` is null and
                    `user_id` = '.$cur_user->id);
            } else {
                $count = DB::select('select count(`id`) as `count`
                    from `projects`
                    where
                    `done` = 0 and
                    `raw` is null and
                    `deleted_at` is null');

                $projects_q = DB::select('select *
                    from `projects`
                    where
                    `done` = 0 and
                    `raw` is null and
                    `deleted_at` is null');
            }
            $num_pages = 1;
        } else {

            $start = abs($page*$per_page);
            if ($cur_user->head != 1) {
                $count = DB::select('select count(`id`) as `count`
                    from `projects`
                    where
                    `done` = 0 and
                    `raw` is null and
                    `deleted_at` is null and
                    `user_id` = '.$cur_user->id);

                $projects_q = DB::select('select *
                    from `projects`
                    where
                    `done` = 0 and
                    `raw` is null and
                    `deleted_at` is null and
                    `user_id` = '.$cur_user->id.'
                    limit '.$start.','.$per_page);
            } else {
                // print_r($start);
                // die;
                $count = DB::select('select count(`id`) as `count`
                    from `projects`
                    where
                    `done` = 0 and
                    `raw` is null and
                    `deleted_at` is null');


                $projects_q = DB::select('select *
                    from `projects`
                    where
                    `done` = 0 and
                    `raw` is null and
                    `deleted_at` is null
                    limit '.$start.','.$per_page);
            }
            $num_pages = ceil($count[0]->count/$per_page);
        }




        $cur_date = date("j").'.'.date("n").'.'.date("Y");
        $cur_date_val = date("Y").'-'.date("m").'-'.date("d");
        $tomorrow = date('Y-m-d', strtotime($cur_date . ' +1 day'));
        $tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +1 day'));
        $plus_one_day = date('Y-m-d', strtotime($cur_date . ' +2 day'));
        $plus_one_day_val = date('Y-m-d', strtotime($cur_date . ' +2 day'));
        $plus_two_day = date('d.m.Y', strtotime($cur_date . ' +3 day'));
        $plus_two_day_val = date('Y-m-d', strtotime($cur_date . ' +3 day'));
        $plus_three_day = date('d.m.Y', strtotime($cur_date . ' +4 day'));
        $plus_three_day_val = date('Y-m-d', strtotime($cur_date . ' +4 day'));

        if (count($projects_q) > 0) {
            foreach ($projects_q as $p) {
                if ($cur_user->head != 1) {
                    $task = Project_task::where('project_id', '=', $p->id)
                        ->where('user_id', '=', $cur_user->id)
                        ->whereNull('deleted_at')
                        ->get();
                } else {
                    $task = Project_task::where('project_id', '=', $p->id)
                        ->whereNull('deleted_at')
                        ->get();
                }

                if (count($task) > 0) {
                    $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($ptc) > 0) {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);

                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                // 'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'prev_contact' => Carbon::parse($ptc[0]['prev_contact'])->format('d.m.Y'),
                                // 'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'next_contact' => Carbon::parse($ptc[0]['next_contact'])->format('d.m.Y'),
                                'start_date' => $p->start_date,
                                'finish_date' => Carbon::parse($p->finish_date)->format('d.m.Y'),
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            // print_r($p->id.'|');
                            $user = Users2::getUserById($p->user_id);
                            // print_r($user->last_name.'|');
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                //'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                // 'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'prev_contact' => Carbon::parse($ptc[0]['prev_contact'])->format('d.m.Y'),
                                // 'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'next_contact' => Carbon::parse($ptc[0]['next_contact'])->format('d.m.Y'),
                                'start_date' => $p->start_date,
                                'finish_date' => Carbon::parse($p->finish_date)->format('d.m.Y'),
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    } else {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);
                        //dd($client);
                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => Carbon::parse($p->finish_date)->format('d.m.Y'),
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => Carbon::parse($p->finish_date)->format('d.m.Y'),
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    }
                } else {
                    $task = Project_print_form::where('project_id', '=', $p->id)
                        ->where('user_id', '=', $cur_user->id)
                        ->whereNull('deleted_at')
                        ->get();

                    $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($ptc) > 0) {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);

                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                // 'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'prev_contact' => Carbon::parse($ptc[0]['prev_contact'])->format('d.m.Y'),
                                // 'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'next_contact' => Carbon::parse($ptc[0]['next_contact'])->format('d.m.Y'),
                                'start_date' => $p->start_date,
                                'finish_date' => Carbon::parse($p->finish_date)->format('d.m.Y'),
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                // 'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'prev_contact' => Carbon::parse($ptc[0]['prev_contact'])->format('d.m.Y'),
                                // 'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'next_contact' => Carbon::parse($ptc[0]['next_contact'])->format('d.m.Y'),
                                'start_date' => $p->start_date,
                                'finish_date' => Carbon::parse($p->finish_date)->format('d.m.Y'),
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    } else {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);
                        //dd($client);
                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => Carbon::parse($p->finish_date)->format('d.m.Y'),
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => Carbon::parse($p->finish_date)->format('d.m.Y'),
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    }
                }
            }
        } else {
            $projects = '';
        }


        if ($cur_user->head != 1) {
            $p_id = Project::where('user_id', '=', $cur_user->id)
                ->whereNull('deleted_at')
                ->get();
        } else {
            $p_id = Project::whereNull('deleted_at')
                ->get();
        }

        foreach ($p_id as $pid) {
            $user2 = User::where('id', '!=', 0)
                ->get();
            foreach ($user2 as $u2) {
                if ($u2->id == $pid->user_id) {
                    $observer2[] = [
                        'default' => 1,
                        'id' => $u2->id,
                        'first_name' => $u2->first_name,
                        'last_name' => $u2->last_name
                    ];
                } else {
                    $observer2[] = [
                        'default' => 0,
                        'id' => $u2->id,
                        'first_name' => $u2->first_name,
                        'last_name' => $u2->last_name
                    ];
                }
            }
            break;
        }
        //dd($projects);

        return response()->json(['projects' => $projects,
            'count' => $count[0]->count,
            'count_pages' => $num_pages,
//            'project_task' => $p_tasks,
            'observer' => $observer2,
            'imp' => Imp::allImplementer(),
            'raw' => $raw,
            'cur_date' => $cur_date,
            'cur_date_val' => $cur_date_val,
            'tomorrow' => $tomorrow,
            'tomorrow_val' => $tomorrow_val,
            'plus_one_day' => $plus_one_day,
            'plus_one_day_val' => $plus_one_day_val,
            'plus_two_day' => $plus_two_day,
            'plus_two_day_val' => $plus_two_day_val,
            'plus_three_day' => $plus_three_day,
            'plus_three_day_val' => $plus_three_day_val]);
    }

    public function getStatusProjects(Request $request) {
        $data = json_decode($request->data);
        switch ($data->statusFilter) {
            case 1:
                $date = Carbon::today()->subDays(14)->format('Y-m-d');

                if (isset($data->imp)) {
                    $projects_q = DB::select("SELECT  `p`.*, max(`c`.`date`)
                        FROM `comments` `c`
                        left join `projects` `p` on `p`.`id` = `c`.`project_id`
                        where `p`.`deleted_at` IS NULL and
                        `c`.`deleted_at` IS NULL and
                        `p`.`done` = 0 and
                        `p`.`user_id` = ".$data->imp." and
                        `p`.`raw` IS NULL
                        GROUP BY `p`.`id`
                        HAVING max(`c`.`date`) < '".$date."'");
                } else {
                    $projects_q = DB::select("SELECT  `p`.*, max(`c`.`date`)
                        FROM `comments` `c`
                        left join `projects` `p` on `p`.`id` = `c`.`project_id`
                        where `p`.`deleted_at` IS NULL and
                        `c`.`deleted_at` IS NULL and
                        `p`.`done` = 0 and
                        `p`.`raw` IS NULL
                        GROUP BY `p`.`id`
                        HAVING max(`c`.`date`) < '".$date."'");
                };

                if (count($projects_q) > 0) {
                    foreach ($projects_q as $p) {
                        $task = Project_task::where('project_id', '=', $p->id)
                                ->whereNull('deleted_at')
                                ->get();

                        if (count($task) > 0) {
                            $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                                ->whereNull('deleted_at')
                                ->get();

                            if (count($ptc) > 0) {
                                $client = Clients::getClientById($p->client_id);
                                $city = Citys::getCityById($client->city_id);

                                foreach ($task as $t) {
                                    if ($t->tasksstatus_ref_id == 2) {
                                        $done2[$p->id][] = $t->id;
                                    }
                                }

                                if (isset($done2[$p->id])) {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                        'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                    ];
                                } else {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        //'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                        'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        //'done' => 0,
                                        'done' => round(((0 / count($task)) * 100), 2),
                                    ];
                                }
                            } else {
                                $client = Clients::getClientById($p->client_id);
                                $city = Citys::getCityById($client->city_id);
                                //dd($client);
                                foreach ($task as $t) {
                                    if ($t->tasksstatus_ref_id == 2) {
                                        $done2[$p->id][] = $t->id;
                                    }
                                }

                                if (isset($done2[$p->id])) {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => NULL,
                                        'next_contact' => NULL,
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                    ];
                                } else {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => NULL,
                                        'next_contact' => NULL,
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        //'done' => 0,
                                        'done' => round(((0 / count($task)) * 100), 2),
                                    ];
                                }
                            }
                        } else {
                            $task = Project_print_form::where('project_id', '=', $p->id)
                                ->where('user_id', '=', $cur_user->id)
                                ->whereNull('deleted_at')
                                ->get();

                            $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                                ->whereNull('deleted_at')
                                ->get();

                            if (count($ptc) > 0) {
                                $client = Clients::getClientById($p->client_id);
                                $city = Citys::getCityById($client->city_id);

                                foreach ($task as $t) {
                                    if ($t->tasksstatus_ref_id == 2) {
                                        $done2[$p->id][] = $t->id;
                                    }
                                }

                                if (isset($done2[$p->id])) {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                        'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                    ];
                                } else {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                        'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        //'done' => 0,
                                        'done' => round(((0 / count($task)) * 100), 2),
                                    ];
                                }
                            } else {
                                $client = Clients::getClientById($p->client_id);
                                $city = Citys::getCityById($client->city_id);
                                //dd($client);
                                foreach ($task as $t) {
                                    if ($t->tasksstatus_ref_id == 2) {
                                        $done2[$p->id][] = $t->id;
                                    }
                                }

                                if (isset($done2[$p->id])) {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => NULL,
                                        'next_contact' => NULL,
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                    ];
                                } else {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => NULL,
                                        'next_contact' => NULL,
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        //'done' => 0,
                                        'done' => round(((0 / count($task)) * 100), 2),
                                    ];
                                }
                            }
                        }
                    }
                } else {
                    $projects = '';
                }

                return response()->json(['projects' => $projects]);
                break;
            case 2:
                $date = Carbon::today()->addDays(10)->format('Y-m-d');
                if (isset($data->imp)) {
                    $projects_q = Project::whereNULL('deleted_at')
                    ->where('done', '=', 0)
                    ->where('finish_date', '>=', Carbon::today()->format('Y-m-d'))
                    ->where('finish_date', '<=', $date)
                    ->where('user_id', '=', $data->imp)
                    ->whereNULL('raw')
                    ->get();
                } else {
                    $projects_q = Project::whereNULL('deleted_at')
                    ->where('done', '=', 0)
                    ->where('finish_date', '>=', Carbon::today()->format('Y-m-d'))
                    ->where('finish_date', '<=', $date)
                    ->whereNULL('raw')
                    ->get();
                };
                if (count($projects_q) > 0) {
                    foreach ($projects_q as $p) {
                        $task = Project_task::where('project_id', '=', $p->id)
                                ->whereNull('deleted_at')
                                ->get();

                        if (count($task) > 0) {
                            $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                                ->whereNull('deleted_at')
                                ->get();

                            if (count($ptc) > 0) {
                                $client = Clients::getClientById($p->client_id);
                                $city = Citys::getCityById($client->city_id);

                                foreach ($task as $t) {
                                    if ($t->tasksstatus_ref_id == 2) {
                                        $done2[$p->id][] = $t->id;
                                    }
                                }

                                if (isset($done2[$p->id])) {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                        'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                    ];
                                } else {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        //'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                        'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        //'done' => 0,
                                        'done' => round(((0 / count($task)) * 100), 2),
                                    ];
                                }
                            } else {
                                $client = Clients::getClientById($p->client_id);
                                $city = Citys::getCityById($client->city_id);
                                //dd($client);
                                foreach ($task as $t) {
                                    if ($t->tasksstatus_ref_id == 2) {
                                        $done2[$p->id][] = $t->id;
                                    }
                                }

                                if (isset($done2[$p->id])) {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => NULL,
                                        'next_contact' => NULL,
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                    ];
                                } else {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => NULL,
                                        'next_contact' => NULL,
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        //'done' => 0,
                                        'done' => round(((0 / count($task)) * 100), 2),
                                    ];
                                }
                            }
                        } else {
                            $task = Project_print_form::where('project_id', '=', $p->id)
                                ->where('user_id', '=', $cur_user->id)
                                ->whereNull('deleted_at')
                                ->get();

                            $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                                ->whereNull('deleted_at')
                                ->get();

                            if (count($ptc) > 0) {
                                $client = Clients::getClientById($p->client_id);
                                $city = Citys::getCityById($client->city_id);

                                foreach ($task as $t) {
                                    if ($t->tasksstatus_ref_id == 2) {
                                        $done2[$p->id][] = $t->id;
                                    }
                                }

                                if (isset($done2[$p->id])) {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                        'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                    ];
                                } else {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                        'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        //'done' => 0,
                                        'done' => round(((0 / count($task)) * 100), 2),
                                    ];
                                }
                            } else {
                                $client = Clients::getClientById($p->client_id);
                                $city = Citys::getCityById($client->city_id);
                                //dd($client);
                                foreach ($task as $t) {
                                    if ($t->tasksstatus_ref_id == 2) {
                                        $done2[$p->id][] = $t->id;
                                    }
                                }

                                if (isset($done2[$p->id])) {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => NULL,
                                        'next_contact' => NULL,
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                    ];
                                } else {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => NULL,
                                        'next_contact' => NULL,
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        //'done' => 0,
                                        'done' => round(((0 / count($task)) * 100), 2),
                                    ];
                                }
                            }
                        }
                    }
                } else {
                    $projects = '';
                }

                return response()->json(['projects' => $projects]);
                break;
            case 3:
                if (isset($data->imp)) {
                    $projects_q = Project::whereNULL('deleted_at')
                    ->where('done', '=', 1)
                    ->where('user_id', '=', $data->imp)
                    ->whereNULL('raw')
                    ->get();
                } else {
                    $projects_q = Project::whereNULL('deleted_at')
                    ->where('done', '=', 1)
                    ->whereNULL('raw')
                    ->get();
                };
                if (count($projects_q) > 0) {
                    foreach ($projects_q as $p) {
                        $task = Project_task::where('project_id', '=', $p->id)
                                ->whereNull('deleted_at')
                                ->get();

                        if (count($task) > 0) {
                            $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                                ->whereNull('deleted_at')
                                ->get();

                            if (count($ptc) > 0) {
                                $client = Clients::getClientById($p->client_id);
                                $city = Citys::getCityById($client->city_id);

                                foreach ($task as $t) {
                                    if ($t->tasksstatus_ref_id == 2) {
                                        $done2[$p->id][] = $t->id;
                                    }
                                }

                                if (isset($done2[$p->id])) {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                        'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                    ];
                                } else {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        //'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                        'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        //'done' => 0,
                                        'done' => round(((0 / count($task)) * 100), 2),
                                    ];
                                }
                            } else {
                                $client = Clients::getClientById($p->client_id);
                                $city = Citys::getCityById($client->city_id);
                                //dd($client);
                                foreach ($task as $t) {
                                    if ($t->tasksstatus_ref_id == 2) {
                                        $done2[$p->id][] = $t->id;
                                    }
                                }

                                if (isset($done2[$p->id])) {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => NULL,
                                        'next_contact' => NULL,
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                    ];
                                } else {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => NULL,
                                        'next_contact' => NULL,
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        //'done' => 0,
                                        'done' => round(((0 / count($task)) * 100), 2),
                                    ];
                                }
                            }
                        } else {
                            $task = Project_print_form::where('project_id', '=', $p->id)
                                ->where('user_id', '=', $cur_user->id)
                                ->whereNull('deleted_at')
                                ->get();

                            $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                                ->whereNull('deleted_at')
                                ->get();

                            if (count($ptc) > 0) {
                                $client = Clients::getClientById($p->client_id);
                                $city = Citys::getCityById($client->city_id);

                                foreach ($task as $t) {
                                    if ($t->tasksstatus_ref_id == 2) {
                                        $done2[$p->id][] = $t->id;
                                    }
                                }

                                if (isset($done2[$p->id])) {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                        'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                    ];
                                } else {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                        'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        //'done' => 0,
                                        'done' => round(((0 / count($task)) * 100), 2),
                                    ];
                                }
                            } else {
                                $client = Clients::getClientById($p->client_id);
                                $city = Citys::getCityById($client->city_id);
                                //dd($client);
                                foreach ($task as $t) {
                                    if ($t->tasksstatus_ref_id == 2) {
                                        $done2[$p->id][] = $t->id;
                                    }
                                }

                                if (isset($done2[$p->id])) {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => NULL,
                                        'next_contact' => NULL,
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                    ];
                                } else {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => NULL,
                                        'next_contact' => NULL,
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        //'done' => 0,
                                        'done' => round(((0 / count($task)) * 100), 2),
                                    ];
                                }
                            }
                        }
                    }
                } else {
                    $projects = '';
                }

                return response()->json(['projects' => $projects]);
                break;
            case 4:
                if (isset($data->imp)) {
                    $projects_q = DB::select("select DISTINCT (`ptc`.`project_id`), `p`.*
                        from `project_task_contacts` `ptc`
                        left join `projects` `p` on `ptc`.`project_id` = `p`.`id`
                        where 
                        `ptc`.`next_contact` >= '2019-05-13 00:00:00'
                        and `ptc`.`next_contact` <= '2019-05-13 23:59:59'
                        and `p`.`raw` IS NULL
                        and `p`.`deleted_at` IS NULL 
                        and `p`.`user_id` = ".$data->imp."
                        and `p`.`done` = 0");
                } else {
                    $projects_q = DB::select("select DISTINCT (`ptc`.`project_id`), `p`.*
                        from `project_task_contacts` `ptc`
                        left join `projects` `p` on `ptc`.`project_id` = `p`.`id`
                        where 
                        `ptc`.`next_contact` >= '2019-05-13 00:00:00'
                        and `ptc`.`next_contact` <= '2019-05-13 23:59:59'
                        and `p`.`raw` IS NULL
                        and `p`.`deleted_at` IS NULL 
                        and `p`.`done` = 0");
                };

                if (count($projects_q) > 0) {
                    foreach ($projects_q as $p) {
                        $task = Project_task::where('project_id', '=', $p->id)
                                ->whereNull('deleted_at')
                                ->get();

                        if (count($task) > 0) {
                            $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                                ->whereNull('deleted_at')
                                ->get();

                            if (count($ptc) > 0) {
                                $client = Clients::getClientById($p->client_id);
                                $city = Citys::getCityById($client->city_id);

                                foreach ($task as $t) {
                                    if ($t->tasksstatus_ref_id == 2) {
                                        $done2[$p->id][] = $t->id;
                                    }
                                }

                                if (isset($done2[$p->id])) {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                        'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                    ];
                                } else {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        //'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                        'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        //'done' => 0,
                                        'done' => round(((0 / count($task)) * 100), 2),
                                    ];
                                }
                            } else {
                                $client = Clients::getClientById($p->client_id);
                                $city = Citys::getCityById($client->city_id);
                                //dd($client);
                                foreach ($task as $t) {
                                    if ($t->tasksstatus_ref_id == 2) {
                                        $done2[$p->id][] = $t->id;
                                    }
                                }

                                if (isset($done2[$p->id])) {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => NULL,
                                        'next_contact' => NULL,
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                    ];
                                } else {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => NULL,
                                        'next_contact' => NULL,
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        //'done' => 0,
                                        'done' => round(((0 / count($task)) * 100), 2),
                                    ];
                                }
                            }
                        } else {
                            $task = Project_print_form::where('project_id', '=', $p->id)
                                ->where('user_id', '=', $cur_user->id)
                                ->whereNull('deleted_at')
                                ->get();

                            $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                                ->whereNull('deleted_at')
                                ->get();

                            if (count($ptc) > 0) {
                                $client = Clients::getClientById($p->client_id);
                                $city = Citys::getCityById($client->city_id);

                                foreach ($task as $t) {
                                    if ($t->tasksstatus_ref_id == 2) {
                                        $done2[$p->id][] = $t->id;
                                    }
                                }

                                if (isset($done2[$p->id])) {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                        'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                    ];
                                } else {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                        'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        //'done' => 0,
                                        'done' => round(((0 / count($task)) * 100), 2),
                                    ];
                                }
                            } else {
                                $client = Clients::getClientById($p->client_id);
                                $city = Citys::getCityById($client->city_id);
                                //dd($client);
                                foreach ($task as $t) {
                                    if ($t->tasksstatus_ref_id == 2) {
                                        $done2[$p->id][] = $t->id;
                                    }
                                }

                                if (isset($done2[$p->id])) {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => NULL,
                                        'next_contact' => NULL,
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                                    ];
                                } else {
                                    $user = Users2::getUserById($p->user_id);
                                    $client = Clients::getClientById($p->client_id);
                                    $projects[] = [
                                        'id' => $p->id,
                                        'close' => $p->done,
                                        'user' => $user->last_name . ' ' . $user->first_name,
                                        // 'user' => '',
                                        'name' => $p->name,
                                        // 'wlist' => $p->waiting_list_ref_id,
                                        'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                        'client' => $client->name,
                                        'city' => $city,
                                        'prev_contact' => NULL,
                                        'next_contact' => NULL,
                                        'start_date' => $p->start_date,
                                        'finish_date' => $p->finish_date,
                                        'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                        'active' => $p->active,
                                        //'done' => 0,
                                        'done' => round(((0 / count($task)) * 100), 2),
                                    ];
                                }
                            }
                        }
                    }
                } else {
                    $projects = '';
                }

                return response()->json(['projects' => $projects]);
                break;
        }
    }

    public function searchProjects(Request $request){
        $data = json_decode($request->data);
        $todayStr = Carbon::today()->format('Y-m-d');
//        print_r(Carbon::today()->format('Y-m-d'));die();
        $projects_q = Project::whereNULL('projects.deleted_at')
            ->whereNULL('raw')
            ->select('projects.*')
            ->orderBy('waiting_list_ref_id')
            ->orderBy('done')
            ->orderBy('start_date', 'desc');

        if (isset($data->search) && $data->search != ''){
            $projects_q->where('projects.name', 'like', "%$data->search%");
        }
        if (isset($data->imp)){
            $projects_q->where('projects.user_id', $data->imp);
        }

        if (isset($data->filterIndex)) {
            if ($data->filterIndex == 1) {
                if ($data->statusFilter == 1) {
                    $date = Carbon::today()->subDays(14)->format('Y-m-d');
                    $projects_q->leftJoin('comments', 'projects.id', '=', 'comments.project_id')
                        ->groupBy('projects.id')
                        ->selectRaw('max(`comments`.`date`)')
                        ->havingRaw('max(`comments`.`date`) <= ?', [$date]);
                } elseif ($data->statusFilter == 2) {
                    $date = Carbon::today()->addDays(10)->format('Y-m-d');
                    $projects_q->where('projects.finish_date', '>=', Carbon::today()->format('Y-m-d'))
                        ->where('projects.finish_date', '<=', $date)
                        ->where('projects.done', 0);
                } elseif ($data->statusFilter == 3) {
                    $projects_q->where('projects.done', 1);
                } elseif ($data->statusFilter == 4) {
                    $projects_q->where('projects.done', 0)
                        ->leftJoin('project_task_contacts', 'projects.id', '=', 'project_task_contacts.project_id')
                        ->where('project_task_contacts.next_contact', '>=', $todayStr . ' 00:00:00')
                        ->where('project_task_contacts.next_contact', '<=', $todayStr . ' 23:59:59')
                        ->whereNULL('project_task_contacts.deleted_at')
                        ->distinct();
                }

            }
            if ($data->filterIndex == 2) {
                $projects_q->where('projects.done', 0)
                    ->where('finish_date', '>=', $data->startClose)
                    ->where('finish_date', '<=', $data->endClose);
            }
        }

        $count = $projects_q->count();

        if (isset($data->count) && $data->count > 0) {
            if (isset($data->page)) {
                $projects_q->offset(($data->page - 1) * $data->count);
            }
            $projects_q->limit($data->count);
        }


//        $sql = str_replace_array('?', $projects_q->getBindings(), $projects_q->toSql());
//        print_r($sql);die;

        $projects_q = $projects_q->get();

        $projects = [];
        foreach ($projects_q as $p) {
            $task = Project_task::where('project_id', '=', $p->id)
                ->whereNull('deleted_at')
                ->get();

            $ptc = [];
            if (count($task) > 0){
                $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                    ->whereNull('deleted_at')
                    ->get();
            }


            $client = Clients::getClientById($p->client_id);
            $city = Citys::getCityById($client->city_id);

            if (count($ptc) > 0) {
                $prev_contact = substr($ptc[0]['prev_contact'], 0, -3);
                $next_contact = substr($ptc[0]['next_contact'], 0, -3);
            } else {
                $prev_contact = null;
                $next_contact = null;
            }

            $doneTasks = [];
            foreach ($task as $t) {
                if ($t->tasksstatus_ref_id == 2) {
                    $doneTasks[] = $t->id;
                }
            }

            $user = Users2::getUserById($p->user_id);
            $client = Clients::getClientById($p->client_id);
            $projects[] = [
                'id' => $p->id,
                'close' => $p->done,
                'user' => $user->last_name . ' ' . $user->first_name,
                'name' => $p->name,
                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                'client' => $client->name,
                'city' => $city,
                'prev_contact' => $prev_contact,
                'next_contact' => $next_contact,
                'start_date' => $p->start_date,
                'finish_date' => $p->finish_date,
                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                'active' => $p->active,
                'done' => round(((count($doneTasks) / count($task)) * 100), 2),
            ];
        }

        return response()->json(['projects' => $projects, 'count' => $count]);

    }


    public function getCloseDateProjects(Request $request) {
        $data = json_decode($request->data);

        if (isset($data->imp)) {
            $projects_q = Project::whereNULL('deleted_at')
            ->where('done', '=', 0)
            ->where('finish_date', '>=', $data->startClose)
            ->where('finish_date', '<=', $data->endClose)
            ->where('user_id', '=', $data->imp)
            ->whereNULL('raw')
            ->get();
        } else {
            $projects_q = Project::whereNULL('deleted_at')
            ->where('done', '=', 0)
            ->where('finish_date', '>=', $data->startClose)
            ->where('finish_date', '<=', $data->endClose)
            ->whereNULL('raw')
            ->get();
        };

        if (count($projects_q) > 0) {
            foreach ($projects_q as $p) {
                $task = Project_task::where('project_id', '=', $p->id)
                        ->whereNull('deleted_at')
                        ->get();

                if (count($task) > 0) {
                    $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($ptc) > 0) {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);

                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                //'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    } else {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);
                        //dd($client);
                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    }
                } else {
                    $task = Project_print_form::where('project_id', '=', $p->id)
                        ->where('user_id', '=', $cur_user->id)
                        ->whereNull('deleted_at')
                        ->get();

                    $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($ptc) > 0) {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);

                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    } else {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);
                        //dd($client);
                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    }
                }
            }
        } else {
            $projects = '';
        }

        return response()->json(['projects' => $projects]);
    }

    public function getProjectsByImpId($id) {
        $projects_q = Project::whereNULL('deleted_at')
            ->where('done', '=', 0)
            ->where('user_id', '=', $id)
            ->whereNULL('raw')
            ->get();

        if (count($projects_q) > 0) {
            foreach ($projects_q as $p) {
                $task = Project_task::where('project_id', '=', $p->id)
                        ->whereNull('deleted_at')
                        ->get();

                if (count($task) > 0) {
                    $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($ptc) > 0) {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);

                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            // print_r($p->id.'|');
                            $user = Users2::getUserById($p->user_id);
                            // print_r($user->last_name.'|');
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                //'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    } else {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);
                        //dd($client);
                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    }
                } else {
                    $task = Project_print_form::where('project_id', '=', $p->id)
                        ->where('user_id', '=', $cur_user->id)
                        ->whereNull('deleted_at')
                        ->get();

                    $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($ptc) > 0) {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);

                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    } else {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);
                        //dd($client);
                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                // 'user' => '',
                                'name' => $p->name,
                                // 'wlist' => $p->waiting_list_ref_id,
                                'wlist' => WList::getWListById($p->waiting_list_ref_id),
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    }
                }
            }
        } else {
            $projects = '';
        }

        return response()->json(['projects' => $projects]);
    }

    public function changeImp(Request $request) {
        $data = json_decode($request->data);

        DB::transaction(function () use ($data) {
            try {
                $project = Project::find($data->pid);
                $project->user_id = $data->imp;
                $project->save();

            } catch (\Exception $e) {
                DB::rollback();
            }

            try {
                $project_task = Project_task::whereProject_id($data->pid)->update(['user_id' => $data->imp]);
            } catch (\Exception $e) {
                DB::rollback();
            }

            try {
                $project_task_contact = Project_task_contact::whereProject_id($data->pid)->update(['user_id' => $data->imp]);

            } catch (\Exception $e) {
                DB::rollback();
            }

            try {
                $project_print_forms = Project_print_form::whereProject_id($data->pid)->update(['observer_id' => $data->imp]);

            } catch (\Exception $e) {
                DB::rollback();
            }
        });
        $user = User::find($data->imp);
        $project = Project::find($data->pid);
        event(new changeImp($project->name, $user->first_name.' '.$user->last_name));
        return response()->json(['status' => 'Ok']);
    }

    public function changeFinishDate(Request $request) {
        $data = json_decode($request->data);
        $curDate = Carbon::now()->format('Y-m-d H:i:s');
        $curDate = explode(' ', $curDate);
        $curUser = Sentinel::getUser();

        $comment = new Comment();
        $comment->project_id = $data->pid;
        $comment->user_id = $curUser->id;
        $comment->time = $curDate[1];
        $comment->date = $curDate[0];
        $comment->text = $data->comment;
        $comment->save();

        $project = Project::find($data->pid);
        $project->finish_date = $data->date;
        $project->save();

        return response()->json(['status' => 'Ok']);
    }

    public function edit($id) {
        if (session('perm')['project.update']) {
            $project = Project::find($id);
            $project_start_date = Carbon::parse($project->start_date)->format('d.m.Y');
            $project_finish_date = Carbon::parse($project->finish_date)->format('d.m.Y');

            if ($project) {
                $cur_date = date("j").'.'.date("n").'.'.date("Y");
                $cur_date_val = date("Y").'-'.date("m").'-'.date("d");
                $tomorrow = date('Y-m-d', strtotime($cur_date . ' +1 day'));
                $tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +1 day'));
                $plus_one_day = date('Y-m-d', strtotime($cur_date . ' +2 day'));
                $plus_one_day_val = date('Y-m-d', strtotime($cur_date . ' +2 day'));
                $plus_two_day = date('d.m.Y', strtotime($cur_date . ' +3 day'));
                $plus_two_day_val = date('Y-m-d', strtotime($cur_date . ' +3 day'));
                $plus_three_day = date('d.m.Y', strtotime($cur_date . ' +4 day'));
                $plus_three_day_val = date('Y-m-d', strtotime($cur_date . ' +4 day'));

                $comments = Comments::getCommentsByProjectId($id);

                $contacts = Project_contact::where('project_id', '=', $id)
                    ->whereNULL('deleted_at')
                    ->orderBy('id', 'asc')
                    ->get();

                foreach($contacts as $c) {
                    $cont = Contact::where('id', '=', $c->contact_id)
                        ->whereNull('deleted_at')
                        ->first();

                    //dd($cont);
                    if (!is_null($cont)) {
                        $post = Posts_reference::find($cont->post_id);

                        if ($cont->post_id == 1) {
                            $ar_contact[] = [
                                'first_name' => $cont->first_name,
                                'last_name' => $cont->last_name,
                                'patronymic' => $cont->patronymic,
                                'phone' => $cont->phone,
                                'email' => $cont->email,
                                'post' => $post->name ?? '',
                                'id' => $cont->id,
                                'director' => 1,
                                'main' => $c->main,
                            ];
                        } else {
                            $ar_contact[] = [
                                'first_name' => $cont->first_name,
                                'last_name' => $cont->last_name,
                                'patronymic' => $cont->patronymic,
                                'phone' => $cont->phone,
                                'email' => $cont->email,
                                'post' => $post['name'] ?? '',
                                'id' => $cont->id,
                                'director' => 0,
                                'main' => $c->main,
                            ];
                        }
                    } else {
                        $ar_contact[] = '';
                    }

                }

                $modules = Project_module::where('project_id', '=', $id)
                    ->whereNull('deleted_at')
                    ->get();

                foreach ($modules as $m) {
                    $name = Modules_reference::find($m->module_id);

                    $all_modules[] = [
                        'id' => $name['id'],
                        'name' => $name['name'],
                        'num' => $m->num
                    ];
                }

                foreach ($all_modules as $all) {
                    $task_q = Module_rule::where('module_id', '=', $all['id'])
                        ->whereNull('deleted_at')
                        ->get();

                    foreach ($task_q as $q) {
                        if (count($q->questions) > 0) {
                            $questions_ar[] = [
                                'm_id' => $all['id'],
                                'id' => $q->id,
                                'questions' => $q->questions
                            ];
                        } else {
                            $questions_ar = [];
                        }
                    }
                }

                $cur_user = Sentinel::getUser();
                $count_norm = 0;
                $count_number = 0;
                $count_print_norm = 0;
                $count_print_number = 0;
                $count_screen_norm = 0;
                $count_screen_number = 0;

                $project_tasks = Project_task::where('user_id', '=', $cur_user->id)
                    ->where('project_id', '=', $id)
                    ->whereNull('deleted_at')
                    //->whereNull('status')
                    //->whereNull('additional_id')
                    ->get();

                if (count($project_tasks) > 0) {
                    $observer_id = $project_tasks[0]['observer_id'];
                    $print_form_id = Grouptasks_reference::select('id')
                        ->where('is_print_form', '=', 1)
                        ->get();

                    $screen_form_id = Grouptasks_reference::select('id')
                        ->where('is_screen_form', '=', 1)
                        ->get();

                    foreach ($project_tasks as $pt) {
                        $name = Module_rule::where('id', '=', $pt->module_rule_id)
                            ->whereNull('deleted_at')
                            ->first();

                        $test[] = array($name['grouptasks_id']);
                        $ptc = Project_task_contact::where('project_task_id', '=', $pt->id)
                            ->whereNull('deleted_at')
                            ->first();
                        $p_name = Project::find($pt->project_id);
                        $m_name = Modules_reference::find($pt->module_id);
                        $g_task = Grouptasks_reference::find($name['grouptasks_id']);
                        //dd($pt->module_id);

                        if ($g_task['is_print_form'] == 1) {
                            $type = 'print_form';
                        } elseif ($g_task['is_screen_form'] == 1) {
                            $type = 'screen_form';
                        } else {
                            $type = 'task';
                        }

                        if (count($ptc) > 0) {
                            $p_tasks[] = [
                                'id' => $pt->id,
                                'name' => $name['task'],
                                'p_name' => $p_name->name,
                                'observer_id' => $pt->observer_id,
                                'city' => $p_name->city,
                                'm_name' => $m_name->name,
                                'callbackstatus_ref_id' => $ptc['callbackstatus_ref_id'],
                                'g_task' => $g_task['name'],
                                'type' => $type,
                                'additional' => $name['additional'],
                                'status' => $pt->tasksstatus_ref_id,
                                'next_contact' => $ptc['next_contact']
                            ];
                        } else {
                            $p_tasks[] = [
                                'id' => $pt->id,
                                'name' => $name['task'],
                                'p_name' => $p_name->name,
                                'observer_id' => $pt->observer_id,
                                'city' => $p_name->city,
                                'm_name' => $m_name->name,
                                'callbackstatus_ref_id' => $ptc['callbackstatus_ref_id'],
                                'g_task' => $g_task['name'],
                                'type' => $type,
                                'additional' => $name['additional'],
                                'status' => $pt->tasksstatus_ref_id,
                                'next_contact' => null
                            ];
                        }
                        //dd($p_tasks);

                        foreach ($print_form_id as $pfid) {
                            if($name['grouptasks_id'] != $pfid->id) {
                                $count_norm += $pt->norm_contacts;
                                $count_number += $pt->number_contacts;
                            } else {
                                $count_print_norm += $pt->norm_contacts;
                                $count_print_number += $pt->number_contacts;
                            }
                        }

                        foreach ($screen_form_id as $sfid) {
                            if($name['grouptasks_id'] != $sfid->id) {
                                $count_norm += $pt->norm_contacts;
                                $count_number += $pt->number_contacts;
                            } else {
                                $count_screen_norm += $pt->norm_contacts;
                                $count_screen_number += $pt->number_contacts;
                            }
                        }
                    }
                } else {
                    $observer_id = '';
                    $p_tasks = '';
                }

                //foreach ()
                //dd($screen_form_id);

                $project_form = Project_print_form::where('project_id', '=', $id)
                    ->whereNull('deleted_at')
                    ->get();

                if (count($project_form) > 0) {
                    foreach ($project_form as $pf) {
                        $name = Module_rule::where('id', '=', $pf->module_rule_id)
                            ->whereNull('deleted_at')
                            ->first();
                        $ptc = Project_task_contact::where('project_task_id', '=', $pf->id)
                            ->whereNull('deleted_at')
                            ->first();
                        $p_name = Project::find($pf->project_id);
                        $m_name = Modules_reference::find($pf->module_id);
                        $g_task = Grouptasks_reference::find(11);

                        $q_ar[] = '';

                        if (count($ptc) > 0) {
                            $p_form[] = [
                                'id' => $pf->id,
                                'name' => $pf->name,
                                'p_name' => $p_name->name,
                                'observer_id' => $pf->observer_id,
                                'city' => $p_name->city,
                                'm_name' => $m_name->name,
                                'callbackstatus_ref_id' => $ptc['callbackstatus_ref_id'],
                                'g_task' => $g_task['name'],
                                'additional' => $name['additional'],
                                'status' => $pf->tasksstatus_ref_id,
                                'next_contact' => $ptc['next_contact']
                            ];
                        } else {
                            $p_form[] = [
                                'id' => $pf->id,
                                'name' => $pf->name,
                                'p_name' => $p_name->name,
                                'observer_id' => $pf->observer_id,
                                'city' => $p_name->city,
                                'm_name' => $m_name['name'],
                                'callbackstatus_ref_id' => $ptc['callbackstatus_ref_id'],
                                'g_task' => $g_task['name'],
                                'additional' => $name['additional'],
                                'status' => $pf->tasksstatus_ref_id,
                                'next_contact' => null
                            ];
                        }

                    }
                } else {
                    $p_form = '';
                }

                $count_fact = Project_task_contact::where('project_id', '=', $project->id)
                    ->whereNull('deleted_at')
                    ->whereNotNull('duration')
                    ->get();

                $remained = 0;
                foreach ($count_fact as $cf) {
                    switch ($cf->duration) {
                        case '00:15:00':
                            $remained += 0.5;
                            break;
                        case '00:30:00':
                            $remained += 1;
                            break;
                        case '00:45:00':
                            $remained += 1;
                            break;
                        case '01:00:00':
                            $remained += 1;
                            break;
                        case '01:30:00':
                            $remained += 1.5;
                            break;
                    }
                }
                //dd($remained);

                $project_tasks2 = Project_task::where('user_id', '=', $cur_user->id)
                    ->where('project_id', '=', $id)
                    ->whereNull('deleted_at')
                    ->get();

                if (count($project_tasks2) > 0) {
                    foreach ($project_tasks2 as $pt) {
                        $name = Module_rule::where('id', '=', $pt->module_rule_id)
                            ->whereNull('deleted_at')
                            ->first();

                        if ($name['additional'] == null) {
                            $c_question = Clarifying_question::where('task_id', '=', $name['id'])
                                ->whereNull('deleted_at')
                                ->get();

                            if (count($c_question) > 0) {
                                foreach ($c_question as $q) {
                                    $q_ar = [
                                        'id' => $q->id,
                                        'question' => $q->question
                                    ];
                                    $q_ar2[] = $q_ar;
                                }
                            }
                        }
                    }
                }

                if(isset($q_ar2)) {
                    foreach ($q_ar2 as $cq) {
                        $c_answer = Clarifying_answer::where('question_id', '=', $cq['id'])
                            ->where('project_id', '=', $id)
                            ->whereNull('deleted_at')
                            ->first();

                        $question[] = [
                            'id' => $cq['id'],
                            'question' => $cq['question'],
                            'answer' => $c_answer['answer']
                        ];

                    }
                } else {
                    $question = [];
                }
                //dd($q_ar2);

                if (count($question) > 0) {
                    foreach ($question as $q) {
                        $c_answer = Clarifying_answer::where('question_id', '=', $q['id'])
                            ->where('project_id', '=', $id)
                            ->whereNull('deleted_at')
                            ->first();

                        //dd($c_answer['answer']);

                        if ($c_answer['answer'] != 'null') {
                            $answer[] = [
                                'q_id' => $q['id'],
                                'answer' => $c_answer['answer']
                            ];
                        } else {
                            $answer = [];
                        }
                    }
                } else {
                    $answer = [];
                }

                //dd($project_tasks2);

                $user = User::whereNull('deleted_at')->get();
                foreach ($user as $u) {
                    if ($u->id == $project->user_id) {
                        $observer[] = [
                            'default' => 1,
                            'id' => $u->id,
                            'first_name' => $u->first_name,
                            'last_name' => $u->last_name
                        ];
                    } else {
                        $observer[] = [
                            'default' => 0,
                            'id' => $u->id,
                            'first_name' => $u->first_name,
                            'last_name' => $u->last_name
                        ];
                    }
                }

                $doc_types = Documents::getAllDocumentsType();
                $doc_input = Documents::getProjectDocsByType(1, $id);
                $doc_output = Documents::getProjectDocsByType(2, $id);

                $forms_q = Project_print_form::where('project_id', '=', $id)
                    ->whereNull('deleted_at')
                    ->get();
                if (count($forms_q) > 0)  {
                    foreach ($forms_q as $f) {
                        $status = Printform_status_reference::find($f->printform_status_ref_id);
                        if ($f->is_print_form == 1) {
                            $type = 'print_form';
                        } elseif ($f->is_screen_form == 1) {
                            $type = 'screen_form';
                        }
                        $file = Done_printform_file::where('project_print_form_id', '=', $f->id)
                            ->whereNull('deleted_at')
                            ->first();

                        if (count($file) > 0) {
                            $url = Storage::disk('local')->url($f->file);
                            $forms[] = [
                                'id' => $f->id,
                                'name' => $f->name,
                                'status' => $status->name,
                                'date_for_client' => Carbon::Parse($f->date_implement_for_client)->format('d.m.Y'),
                                'date_status' => Carbon::Parse($f->updated_at)->format('d.m.Y'),
                                'type' => $type,
                                'file' => $file->file,
                                'img' => $url.$file->file
                            ];
                        } else {
                            $forms[] = [
                                'id' => $f->id,
                                'name' => $f->name,
                                'status' => $status->name,
                                'date_for_client' => Carbon::Parse($f->date_implement_for_client)->format('d.m.Y'),
                                'date_status' => Carbon::Parse($f->updated_at)->format('d.m.Y'),
                                'type' => $type,
                                'file' => ''
                            ];
                        }
                    }
                } else {
                    $forms = '';
                }

                $client = Clients_reference::find($project->client_id);

                $check_list_answer = Check_list_answer::where('project_id', '=', $id)
                    ->whereNull('deleted_at')
                    ->get();
                foreach ($check_list_answer as $check_answer) {
                    $check_list_question = Check_list_reference::where('id', '=',$check_answer->check_list_ref_id)
                        ->whereNull('deleted_at')
                        ->first();

                    $check_list[] = [
                        'id' => $check_list_question['id'],
                        'question' => strip_tags($check_list_question['question']),
                        'answer' => $check_answer->answer
                    ];
                }
                //dd($p_tasks);

                return view('Implementer.projects.edit', [
                    'project' => $project,
                    'project_start_date' => $project_start_date,
                    'project_finish_date' => $project_finish_date,
                    'forms' => $forms,
                    'observer_id' => $observer_id,
                    'comments' => $comments,
                    'contacts' => $ar_contact,
                    'modules' => $all_modules,
                    'all_modules' => Modules::getModulesByProjectId($project->id),
                    'project_task' => $p_tasks,
                    'project_form' => $p_form,
                    'group_task' => GroupTasks::getAllTasks(),
                    'all_group_task' => GroupTasks::getAllTaskGroup(),
                    'observer' => $observer,
                    'cur_date' => $cur_date,
                    'cur_date_val' => $cur_date_val,
                    'tomorrow' => $tomorrow,
                    'tomorrow_val' => $tomorrow_val,
                    'plus_one_day' => $plus_one_day,
                    'plus_one_day_val' => $plus_one_day_val,
                    'plus_two_day' => $plus_two_day,
                    'plus_two_day_val' => $plus_two_day_val,
                    'plus_three_day' => $plus_three_day,
                    'plus_three_day_val' => $plus_three_day_val,
                    'client' => $client,
                    'questions' => $questions_ar,
                    'question' => $question,
                    'answer' => $answer,
                    'count_norm' => $count_norm/2,
                    'count_number' => $count_number/2,
                    'count_print_norm' => $count_print_norm,
                    'count_print_number' => $count_print_number,
                    'count_screen_norm' => $count_screen_norm,
                    'count_screen_number' => $count_screen_number,
                    'count_fact' => count($count_fact),
                    'remained' => $remained,
                    'doc_types' => $doc_types,
                    'doc_input' => $doc_input,
                    'doc_output' => $doc_output,
                    'check_list' => $check_list
                ]);
            } else {
                abort(404);
            }
        } else {
            abort(503);
        }
    }

    public function add() {
        if (session('perm')['project.create']) {
            $users = Users2::getAllUsers();
            $modules = Modules_reference::whereNULL('deleted_at')->get();
            $posts = Posts_reference::whereNull('deleted_at')->get();
            $cur_user = Sentinel::getUser();

            $documents = Documents::getDocumentsForAllProjects();

            return view('Implementer.projects.add', [
                'users' => $users,
                'cur_user' => $cur_user,
                'modules' => $modules,
                'posts' => $posts,
                'documents' => $documents,
                'clients' => Clients::getAllClients()
            ]);
        } else {
            abort(503);
        }
    }

    public function postAdd(Request $request) {
        if (session('perm')['project.create']) {
            if (!$request->projectName) {
                return redirect()->back()->with('error', 'Укажите название проекта');
            } else {
                $count_project = Project::where('name', '=', $request->projectName)
                    ->whereNull('deleted_at')
                    ->count();
                if ($count_project > 0) {
                    return redirect()->back()->with('error', 'Проект с данным названием уже существует');
                }
            }
            if (!$request->startDate) {
                return redirect()->back()->with('error', 'Укажите дату начала');
            } else {
                $startDate = AMDateTime::startProjectDate($request->startDate);
                if (!$startDate) {
                    return redirect()->back()->with('error', 'Дата создания проекта меньше текущей даты');
                }
            }
            if (!$request->finishDate) {
                return redirect()->back()->with('error', 'Укажите дату окончания');
            } else {
                $finishDate = AMDateTime::finishProjectDate($request->finishDate);
                if (!$finishDate) {
                    return redirect()->back()->with('error', 'Дата окончания проекта меньше текущей даты');
                }
            }
            if ($request->startDate >= $request->finishDate) {
                return redirect()->back()->with('error', 'Дата окончания проекта меньше даты начала');
            }
            if ($request->optionsRadios != "on") {
                return redirect()->back()->with('error', 'Отсутствует информация об отправке документов');
            }
            if ($request->row_modules == '[]') {
                return redirect()->back()->with('error', 'Выберите модули');
            }
            $cur_user = Sentinel::getUser();
            $contacts = json_decode($request->contacts);
            $modules = json_decode($request->row_modules);
            $module_rule = json_decode($request->module_rule);
            $documents = json_decode($request->documents);

            //dd($documents);

            DB::transaction(function () use ($request, $contacts, $cur_user, $modules, $module_rule, $documents) {
                try {
                    $this->project = new Project();
                    $this->project->name = $request->projectName;
                    $this->project->done = 0;
                    $this->project->active = 1;
                    $this->project->transport_company = $request->transComp;
                    $this->project->letter_id = $request->MessageId;
                    $this->project->start_date = $request->startDate;
                    $this->project->finish_date = $request->finishDate;
                    $this->project->user_id = $request->user;
                    $this->project->client_id = $request->client;
                    $this->project->observer_id = $cur_user->id;
                    $this->project->save();

                    $cu = User::find($request->user);
                    $cu->notify(new ProjectTask($this->project));
                    $this->success = true;
                } catch (\Exception $e) {
                    DB::rollback();
                    $this->success = false;
                }

                foreach ($documents as $doc) {
                    $is_first = Documents::isFirstDoc($doc->name);

                    if ($is_first) {
                        try {
                            $type = Documents::getDocTypeByName($doc->type);
                            $doc_name = Documents::getDocumentByName($doc->name);

                            $project_doc = new Projects_doc();
                            $project_doc->project_id = $this->project->id;
                            $project_doc->doc_id = $doc_name->id;
                            $project_doc->type_id = $type->id;
                            $project_doc->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            DB::rollback();
                            $this->success = false;
                        }
                    }
                }

                foreach ($contacts as $c) {
                    if ($c->id != '') {
                        try {
                            $contact = Contact::find($c->id);
                            $p_contact = new Project_contact();
                            $p_contact->contact_id = $contact->id;
                            $p_contact->project_id = $this->project->id;
                            if ($c->main == 'true') {
                                $p_contact->main = 1;
                            } else {
                                $p_contact->main = 0;
                            }
                            if ($c->email_rep == 'true') {
                                $p_contact->mail_rep = 1;
                            } else {
                                $p_contact->mail_rep = 0;
                            }
                            $p_contact->save();
                            $this->success = true;
                        } catch (\Exception $e) {
                            DB::rollback();
                            $this->success = false;
                        }
                    } else {
                        try {
                            $post = Posts_reference::where('name', '=', $c->post)->firstOrFail();
                            $contact = new Contact();
                            $contact->first_name = $c->first_name;
                            $contact->last_name = $c->last_name;
                            $contact->patronymic = $c->patronymic;
                            $contact->post_id = $post->id;
                            $contact->phone = $c->phone;
                            $contact->email = $c->email;
                            $contact->client_id = $request->client;
                            $contact->user_id = $cur_user->id;
                            $contact->save();

                            $p_contact = new Project_contact();
                            $p_contact->contact_id = $contact->id;
                            $p_contact->project_id = $this->project->id;
                            if ($c->main == 'true') {
                                $p_contact->main = 1;
                            } else {
                                $p_contact->main = 0;
                            }
                            if ($c->email_rep == 'true') {
                                $p_contact->mail_rep = 1;
                            } else {
                                $p_contact->mail_rep = 0;
                            }
                            $p_contact->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            DB::rollback();
                            $this->success = false;
                        }
                    }
                }

                foreach ($modules as $m) {
                    try {
                        $modules = new Project_module();
                        $modules->module_id = (int)$m->id;
                        $modules->project_id = $this->project->id;
                        $modules->num = (int)$m->num;
                        $modules->save();

                        $m_rule = Module_rule::where('module_id', '=', (int)$m->id)
                            ->whereNull('deleted_at')
                            ->whereNull('additional')
                            ->get();

                        foreach ($m_rule as $mr) {
                            //if ($mr->grouptasks_id != 11) {
                                $p_task = new Project_task();
                                $p_task->project_id = $this->project->id;
                                $p_task->user_id = $request->user;
                                $p_task->observer_id = $cur_user->id;
                                $p_task->module_rule_id = $mr->id;
                                $p_task->norm_contacts = $mr->norm_contacts;
                                $p_task->number_contacts = $mr->number_contacts;
                                $p_task->module_id = $m->id;
                                $p_task->tasksstatus_ref_id = 1;
                                $p_task->save();
                            /*} else {
                                $number = Project_print_form::whereNull('deleted_at')
                                    ->max('number');

                                if (isset($number)) {
                                    $p_form = new Project_print_form();
                                    $p_form->project_id = $this->project->id;
                                    $p_form->number = $number + 1;
                                    $p_form->name = $mr->task;
                                    $p_form->user_id = $request->user;
                                    $p_form->observer_id = $cur_user->id;
                                    $p_form->module_rule_id = $mr->id;
                                    $p_form->norm_contacts = $mr->norm_contacts;
                                    $p_form->number_contacts = $mr->number_contacts;
                                    $p_form->module_id = $m->id;
                                    $p_form->printform_status_ref_id = 1;
                                    $p_form->save();

                                    $cu = User::find($request->user);
                                    $cu->notify(new ProjectTask($p_form));
                                } else {
                                    //dd($mr->number_contacts);
                                    $p_form_new = new Project_print_form();
                                    $p_form_new->project_id = $this->project->id;
                                    $p_form_new->number = 1;
                                    $p_form_new->name = $mr->task;
                                    $p_form_new->user_id = (int)$request->user;
                                    $p_form_new->observer_id = (int)$cur_user->id;
                                    $p_form_new->module_rule_id = (int)$mr->id;
                                    $p_form_new->norm_contacts = $mr->norm_contacts;
                                    $p_form_new->number_contacts = $mr->number_contacts;
                                    $p_form_new->module_id = (int)$m->id;
                                    $p_form_new->printform_status_ref_id = 1;
                                    $p_form_new->save();
                                }
                            }*/
                        }

                        foreach ($module_rule as $new_mr) {
                            try {
                                $new_p_task = Project_task::where('project_id', '=', $project->id)
                                    ->where('module_rule_id', '=', $new_mr->id)
                                    ->firstOrFail();

                                $new_p_task->norm_contacts = (float)$new_mr->norm;
                                $new_p_task->number_contacts = (int)$new_mr->number;
                                $new_p_task->save();

                                $this->success = true;
                            } catch (\Exception $e) {
                                $this->success = false;
                            }
                        }

                        //

                        $this->success = true;
                    } catch (\Exception $e) {
                        DB::rollback();
                        $this->success = false;
                    }
                }
            });

            if ($this->success) {
                return redirect('/implementer/projects')->with('success', 'Проект успешно создан');
            } else {
                return redirect('/implementer/projects')->with('error', 'Произошла ошибка при создании проекта');
            }

        } else {
            abort(503);
        }
    }

    public function close($id) {
        $project = Project::find($id);
        $client = Clients_reference::find($project->client_id);

        return view('Implementer.projects.close', [
            'project' => $project,
            'client' => $client
        ]);
    }

    public function postClose(Request $request, $id) {
        $data = json_decode($request->data);

        DB::transaction(function () use ($request, $data, $id) {
            $date =  explode(' ', date("Y-m-d H:i:s"));
            $cur_user = Sentinel::getUser();
            try {
                $comment = new Comment();
                $comment->project_id = $id;
                $comment->user_id = $cur_user->id;
                $comment->for_client = NULL;
                $comment->close = 1;
                $comment->date = $date[0];
                $comment->time = $date[1];
                $comment->text = $data->comment;
                $comment->save();

                $this->success = true;
            } catch (\Exception $e) {
                $this->success = false;
            }

            try {
                $project = Project::find($id);
                $project->callback_status = 6;
                $project->done_datetime = $data->date_close.' '.$data->time_close.':00';
                $project->save();

                $this->success = true;
            } catch (\Exception $e) {
                $this->success = false;
            }
            broadcast(new ImpCloseProject($project->name, $cur_user->first_name.' '.$cur_user->last_name));
        });


        if ($this->success) {
            $project = DB::select("select `p`.`name` as `project`, `city`.`name` as `city`
                from `projects` `p`
                left join `clients_references` `cr` on `cr`.`id` = `p`.`client_id`
                left join `city_references` `city` on `cr`.`city_id` = `city`.`id`
                where
                `p`.`id` = ".$id);

            $contact = DB::select("select `c`.`first_name`, `c`.`last_name`, `c`.`patronymic`, `c`.`phone`, `c`.`email`
                from `contacts` `c`
                left join `project_contacts` `pc` on `pc`.`contact_id` = `c`.`id`
                where
                `pc`.`deleted_at` IS NULL and
                `pc`.`project_id` =".$id);

            $modules = DB::select("select `m`.`name` as `name`, `pm`.`num`
                from `project_modules` `pm`
                left join `modules_references` `m` on `m`.`id` = `pm`.`module_id`
                where 
                `pm`.`project_id` = ".$id);

            $close_comment = DB::select("select `c`.`text`, DATE_FORMAT(`c`.`time`, '%H:%i') as `time`, 
                DATE_FORMAT(`c`.`date`, '%d.%m.%Y') as `date`, `u`.`last_name`, `u`.`first_name`
                from `comments` `c`
                left join `users` `u` on `u`.`id` = `c`.`user_id`
                where 
                `c`.`close` = 1 and
                `c`.`deleted_at` IS NULL and
                `c`.`project_id` = ".$id);

            $comments = DB::select("select `c`.`text`, DATE_FORMAT(`c`.`time`, '%H:%i') as `time`, DATE_FORMAT(`c`.`date`, '%d.%m.%Y') as `date`,
                `u`.`last_name`, `u`.`first_name`, `c`.`for_client`
                from `comments` `c`
                left join `users` `u` on `u`.`id` = `c`.`user_id`
                where 
                `c`.`close` IS NULL and
                `c`.`deleted_at` IS NULL and
                `c`.`project_id` = ".$id);

            $question = DB::select("select `cq`.`question`, `ca`.`answer`
                from `clarifying_questions` `cq`
                left join `clarifying_answers` `ca` on `ca`.`question_id` = `cq`.`id`
                where
                `cq`.`deleted_at` IS NULL and
                `ca`.`project_id` = ".$id);


            // print_r($close_comment);
            // die;

            $phpWord = new \PhpOffice\PhpWord\PhpWord();

            $section = $phpWord->addSection();

            $fontStyle = array('size'=>18, 'bold'=>true);
            $paragraphStyle = array('align'=>'center');

            $myTextElement = $section->addText("Чек лист на первичное внедрение");
            $myTextElement->setFontStyle($fontStyle);
            $myTextElement->setParagraphStyle($paragraphStyle);

            $fontStyle = array('size'=>14, 'bold'=>false);
            $paragraphStyle = array('align'=>'left');

            $myTextElement = $section->addText("Клиент");
            $myTextElement->setFontStyle($fontStyle);
            $myTextElement->setParagraphStyle($paragraphStyle);

            $styleTable = array('borderSize' => 6, 'borderColor' => '999999');
            $cellRowSpan = array('vMerge' => 'restart', 'valign' => 'center');
            $cellRowContinue = array('vMerge' => 'continue');
            $cellColSpan2 = array('gridSpan' => 2, 'valign' => 'center');
            $cellColSpan3 = array('gridSpan' => 3, 'valign' => 'both');

            $cellHCentered = array('align' => 'center');
            $cellVCentered = array('valign' => 'center');

            $phpWord->addTableStyle('Colspan Rowspan', $styleTable);
            $table = $section->addTable('Colspan Rowspan');
            $table->addRow(null, array('tblHeader' => true));
            $table->addCell(5000, $cellVCentered)->addText('Название', array('bold' => true), $cellHCentered);
            $table->addCell(5000, $cellVCentered)->addText('Адрес', array('bold' => true), $cellHCentered);

            $table->addRow();
            $table->addCell(2000, $cellRowSpan)->addText($project[0]->project, null, $cellHCentered);
            $table->addCell(2000, $cellRowSpan)->addText($project[0]->city, null, $cellHCentered);

            $myTextElement = $section->addText("Контакты");
            $myTextElement->setFontStyle($fontStyle);
            $myTextElement->setParagraphStyle($paragraphStyle);

            $styleTable = array('borderSize' => 6, 'borderColor' => '999999');
            $cellRowSpan = array('vMerge' => 'restart', 'valign' => 'center');
            $cellRowContinue = array('vMerge' => 'continue');
            $cellColSpan2 = array('gridSpan' => 2, 'valign' => 'center');
            $cellColSpan3 = array('gridSpan' => 3, 'valign' => 'both');

            $cellHCentered = array('align' => 'center');
            $cellVCentered = array('valign' => 'center');

            $phpWord->addTableStyle('Colspan Rowspan', $styleTable);
            $table = $section->addTable('Colspan Rowspan');
            $table->addRow(null, array('tblHeader' => true));
            $table->addCell(3333, $cellVCentered)->addText('ФИО', array('bold' => true), $cellHCentered);
            $table->addCell(3333, $cellVCentered)->addText('Телефон', array('bold' => true), $cellHCentered);
            $table->addCell(3333, $cellVCentered)->addText('email', array('bold' => true), $cellHCentered);

            foreach($contact as $c) {
                $table->addRow();
                $table->addCell(2000, $cellRowSpan)->addText($c->last_name.' '.$c->first_name.' '.$c->patronymic, null, $cellHCentered);
                $table->addCell(2000, $cellRowSpan)->addText($c->phone, null, $cellHCentered);
                $table->addCell(2000, $cellRowSpan)->addText($c->email, null, $cellHCentered);
            }

            $myTextElement = $section->addText("Модули");
            $myTextElement->setFontStyle($fontStyle);
            $myTextElement->setParagraphStyle($paragraphStyle);

            $styleTable = array('borderSize' => 6, 'borderColor' => '999999');
            $cellRowSpan = array('vMerge' => 'restart', 'valign' => 'center');
            $cellRowContinue = array('vMerge' => 'continue');
            $cellColSpan2 = array('gridSpan' => 2, 'valign' => 'center');
            $cellColSpan3 = array('gridSpan' => 3, 'valign' => 'both');

            $cellHCentered = array('align' => 'center');
            $cellVCentered = array('valign' => 'center');

            $phpWord->addTableStyle('Colspan Rowspan', $styleTable);
            $table = $section->addTable('Colspan Rowspan');
            $table->addRow(null, array('tblHeader' => true));
            $table->addCell(5000, $cellVCentered)->addText('Название', array('bold' => true), $cellHCentered);
            $table->addCell(5000, $cellVCentered)->addText('Количество', array('bold' => true), $cellHCentered);

            foreach($modules as $m) {
                $table->addRow();
                $table->addCell(2000, $cellRowSpan)->addText($m->name, null, $cellHCentered);
                $table->addCell(2000, $cellRowSpan)->addText($m->num, null, $cellHCentered);
            }

            $fontStyle = array('size'=>18, 'bold'=>true);
            $paragraphStyle = array('align'=>'center');

            $myTextElement = $section->addText("Закрытие проекта");
            $myTextElement->setFontStyle($fontStyle);
            $myTextElement->setParagraphStyle($paragraphStyle);

            $styleTable = array('borderSize' => 6, 'borderColor' => '999999');
            $cellRowSpan = array('vMerge' => 'restart', 'valign' => 'center');
            $cellRowContinue = array('vMerge' => 'continue');
            $cellColSpan2 = array('gridSpan' => 2, 'valign' => 'center');
            $cellColSpan3 = array('gridSpan' => 3, 'valign' => 'both');

            $cellHCentered = array('align' => 'both', 'size'=>16);
            $cellVCentered = array('valign' => 'center');

            $cont = $contact[0]->last_name.' '.$contact[0]->first_name.' '.$contact[0]->patronymic.' / '.$contact[0]->phone;
            $date = $close_comment[0]->date.' '.$close_comment[0]->time;
            $com = strip_tags($close_comment[0]->text);
            $user = $close_comment[0]->last_name.' '.$close_comment[0]->first_name;

            // var_dump($com);
            // die;

            $phpWord->addTableStyle('Colspan Rowspan', $styleTable);
            $table = $section->addTable('Colspan Rowspan');
            $table->addRow(null, array('tblHeader' => true));
            $table->addCell(5000, $cellVCentered)->addText('Обратная связь от директора / ответственного лица', array('bold' => true), $cellHCentered);
            $table->addCell(5000, $cellVCentered)->addText($cont, array('bold' => false), $cellHCentered);
            $table->addRow(null, array('tblHeader' => true));
            $table->addCell(5000, $cellVCentered)->addText('Дата и время созвона', array('bold' => true), $cellHCentered);
            $table->addCell(5000, $cellVCentered)->addText($date, array('bold' => false), $cellHCentered);
            $table->addRow(null, array('tblHeader' => true));
            $table->addCell(5000, $cellVCentered)->addText('Коментарий', array('bold' => true), $cellHCentered);
            $table->addCell(5000, $cellVCentered)->addText($com, array('bold' => false), $cellHCentered);
            $table->addRow(null, array('tblHeader' => true));
            $table->addCell(5000, $cellVCentered)->addText('Закрыл', array('bold' => true), $cellHCentered);
            $table->addCell(5000, $cellVCentered)->addText($user, array('bold' => false), $cellHCentered);

            $myTextElement = $section->addText("Уточняющие вопросы");
            $myTextElement->setFontStyle($fontStyle);
            $myTextElement->setParagraphStyle($paragraphStyle);

            $styleTable = array('borderSize' => 6, 'borderColor' => '999999');
            $cellRowSpan = array('vMerge' => 'restart', 'valign' => 'center');
            $cellRowContinue = array('vMerge' => 'continue');
            $cellColSpan2 = array('gridSpan' => 2, 'valign' => 'center');
            $cellColSpan3 = array('gridSpan' => 3, 'valign' => 'both');

            $cellHCentered = array('align' => 'center');
            $cellVCentered = array('valign' => 'center');

            $phpWord->addTableStyle('Colspan Rowspan', $styleTable);
            $table = $section->addTable('Colspan Rowspan');
            $table->addRow(null, array('tblHeader' => true));
            $table->addCell(5000, $cellVCentered)->addText('Вопрос', array('bold' => true), $cellHCentered);
            $table->addCell(5000, $cellVCentered)->addText('Ответ', array('bold' => true), $cellHCentered);

            foreach($question as $q) {
                $table->addRow();
                $table->addCell(2000, $cellRowSpan)->addText($q->question, null, $cellHCentered);
                $table->addCell(2000, $cellRowSpan)->addText($q->answer, null, $cellHCentered);
            }

            $section->addPageBreak();

            $myTextElement = $section->addText("Дневник внедрения");
            $myTextElement->setFontStyle($fontStyle);
            $myTextElement->setParagraphStyle($paragraphStyle);

            $phpWord->addTableStyle('Colspan Rowspan', $styleTable);
            $table = $section->addTable('Colspan Rowspan');
            // $table->addRow(null, array('tblHeader' => true));

            foreach($comments as $c) {
                $com2 = strip_tags($c->text);
                $com2 = preg_replace("/&nbsp;/", ' ', $com2);
                $u = $c->last_name.' '.$c->first_name;
                $table->addRow();
                $table->addCell(3333, $cellRowSpan)->addText($u, null, $cellHCentered);
                $table->addCell(3333, $cellRowSpan)->addText($c->date.' '.$c->time, null, $cellHCentered);
                if ($c->for_client == 1) {
                    $table->addCell(3333, $cellRowSpan)->addText('(Для клиента)', null, $cellHCentered);
                } else {
                    $table->addCell(3333, $cellRowSpan)->addText('', null, $cellHCentered);
                }
                $table->addRow();
                $table->addCell(3333, $cellColSpan3)->addText($com2, null, $cellHCentered);
            }


            $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
            try {
                $objWriter->save(storage_path('helloWorld.docx'));

                $cur_user = Sentinel::getUser();
                $max_size = (int)ini_get('upload_max_filesize') * 1000;
                $all_ext = implode(',', $this->allExtensions());

                $r = rand(100, 1000);
                $project = Project::find($id);
                $file = $project->name.'.docx';
                $name = explode('.', $file);

                $type = $this->getType($name[1]);
                $rand = $r.'_'.$request['name'];
                $rand2 = $r.'_'.$file;

//                $upload = LGD::put($rand2, file_get_contents(storage_path('helloWorld.doc')));
                $fileAdapter =  new YandexFileAdapter();
                $upload = $fileAdapter->put($rand2, file_get_contents(storage_path('helloWorld.docx')));
                // sleep(10);
                if ($upload) {
//                    $a = $upload->getAllProperties();

                    $check = Close_check_list::where('project_id', $id)->first();
                    if (!$check) $check = new Close_check_list();
                    $check->project_id = $id;
                    $check->g_name = '';
                    $check->public_url = $upload['public_url'];
                    $check->download_url = $upload['download_url'];
                    $check->name = $r.'_'.$file;

                    if ($check->save()) {
                        return response()->json(['status' => 'Ok']);
                    } else {
                        return response()->json(['status' => 'Error']);
                    }
                }
            } catch (Exception $e) {
                print_r($e);
            }

            return response()->json(['status' => 'Error']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function sortProjectTasks(Request $request) {
        if ($request->ajax()) {
            $cur_user = Sentinel::getUser();

            unset($p_tasks);
            switch ($request->status) {
                case 'all':
                    $project_tasks_all = Project_task::where('user_id', '=', $cur_user->id)
                        ->where('project_id', '=', $request->pid)
                        //->whereNull('status')
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($project_tasks_all) > 0) {
                        foreach ($project_tasks_all as $pt1) {
                            $name = Module_rule::find($pt1->module_rule_id);
                            $p_name = Project::find($pt1->project_id);
                            $m_name = Modules_reference::find($pt1->module_id);
                            $g_task = Grouptasks_reference::find($name->grouptasks_id);

                            $ptc = Project_task_contact::where('project_task_id', '=', $pt1->id)
                                ->whereNull('deleted_at')
                                ->first();

                            if ($g_task['is_print_form'] == 1) {
                                $type = 'print_form';
                            } elseif ($g_task['is_screen_form'] == 1) {
                                $type = 'screen_form';
                            } else {
                                $type = 'task';
                            }

                            if (count($ptc) > 0) {
                                $p_tasks[] = [
                                    'id' => $pt1->id,
                                    'name' => $name->task,
                                    'p_name' => $p_name->name,
                                    'p_id' => $pt1->project_id,
                                    'city' => $p_name->city,
                                    'm_name' => $m_name->name,
                                    'g_task' => $g_task->name,
                                    'type' => $type,
                                    'additional' => $name->additional,
                                    'status' => $pt1->tasksstatus_ref_id,
                                    'next_contact' => $ptc['next_contact'],
                                    'print_form' => 0
                                ];
                            } else {
                                $p_tasks[] = [
                                    'id' => $pt1->id,
                                    'name' => $name->task,
                                    'p_name' => $p_name->name,
                                    'p_id' => $pt1->project_id,
                                    'city' => $p_name->city,
                                    'm_name' => $m_name->name,
                                    'g_task' => $g_task->name,
                                    'type' => $type,
                                    'additional' => $name->additional,
                                    'status' => $pt1->tasksstatus_ref_id,
                                    'next_contact' => null,
                                    'print_form' => 0
                                ];
                            }
                        }
                    } else {
                        $p_tasks[] = '';
                    }

                    $data = [
                        'p_task' => $p_tasks
                    ];

                    break;
                case 'main':
                    $project_tasks_main = Project_task::select('project_tasks.id',
                        'project_tasks.module_rule_id',
                        'project_tasks.module_id',
                        'project_tasks.project_id',
                        'project_tasks.tasksstatus_ref_id')
                        ->where('project_tasks.user_id', '=', $cur_user->id)
                        ->join('module_rules', 'project_tasks.module_rule_id', '=', 'module_rules.id')
                        ->where('project_tasks.project_id', '=', $request->pid)
                        ->whereNull('module_rules.additional')
                        ->whereNull('project_tasks.deleted_at')
                        //->whereNull('status')
                        ->get();

                    if (count($project_tasks_main) > 0) {
                        foreach ($project_tasks_main as $pt2) {
                            $name = Module_rule::find($pt2->module_rule_id);
                            $p_name = Project::find($pt2->project_id);
                            $m_name = Modules_reference::find($pt2->module_id);
                            $g_task = Grouptasks_reference::find($name->grouptasks_id);

                            $ptc = Project_task_contact::where('project_task_id', '=', $pt2->id)
                                ->whereNull('deleted_at')
                                ->first();

                            if ($g_task['is_print_form'] == 1) {
                                $type = 'print_form';
                            } elseif ($g_task['is_screen_form'] == 1) {
                                $type = 'screen_form';
                            } else {
                                $type = 'task';
                            }

                            if (count($ptc) > 0) {
                                $p_tasks[] = [
                                    'id' => $pt2->id,
                                    'name' => $name->task,
                                    'p_name' => $p_name->name,
                                    'p_id' => $pt2->project_id,
                                    'city' => $p_name->city,
                                    'm_name' => $m_name->name,
                                    'g_task' => $g_task->name,
                                    'type' => $type,
                                    'additional' => $name->additional,
                                    'status' => $pt2->tasksstatus_ref_id,
                                    'next_contact' => $ptc['next_contact'],
                                    'print_form' => 0
                                ];
                            } else {
                                $p_tasks[] = [
                                    'id' => $pt2->id,
                                    'name' => $name->task,
                                    'p_name' => $p_name->name,
                                    'p_id' => $pt2->project_id,
                                    'city' => $p_name->city,
                                    'm_name' => $m_name->name,
                                    'g_task' => $g_task->name,
                                    'type' => $type,
                                    'additional' => $name->additional,
                                    'status' => $pt2->tasksstatus_ref_id,
                                    'next_contact' => null,
                                    'print_form' => 0
                                ];
                            }
                        }
                    } else {
                        $p_tasks[] = '';
                    }

                    $data = [
                        'p_task' => $p_tasks
                    ];

                    break;
                case 'additional':
                    $count_norm = 0;
                    $count_number = 0;
                    $count_print_norm = 0;
                    $count_print_number = 0;
                    $count_screen_norm = 0;
                    $count_screen_number = 0;
                    $pf_ar = [];
                    $sf_ar = [];

                    $print_form_id = Grouptasks_reference::select('id')
                        ->where('is_print_form', '=', 1)
                        ->get();

                    $screen_form_id = Grouptasks_reference::select('id')
                        ->where('is_screen_form', '=', 1)
                        ->get();

                    $project_tasks_additional = Project_task::select('project_tasks.id',
                        'project_tasks.module_rule_id',
                        'project_tasks.module_id',
                        'project_tasks.project_id',
                        'project_tasks.tasksstatus_ref_id',
                        'project_tasks.norm_contacts',
                        'project_tasks.number_contacts')
                        ->where('project_tasks.user_id', '=', $cur_user->id)
                        ->join('module_rules', 'project_tasks.module_rule_id', '=', 'module_rules.id')
                        ->where('project_tasks.project_id', '=', $request->pid)
                        ->where('module_rules.additional', '=', 1)
                        ->whereNull('project_tasks.deleted_at')
                        //->whereNull('status')
                        ->get();

                    if (count($project_tasks_additional) > 0) {
                        foreach ($project_tasks_additional as $pt3) {
                            $name = Module_rule::find($pt3->module_rule_id);
                            $p_name = Project::find($pt3->project_id);
                            $m_name = Modules_reference::find($pt3->module_id);
                            $g_task = Grouptasks_reference::find($name->grouptasks_id);

                            $ptc = Project_task_contact::where('project_task_id', '=', $pt3->id)
                                ->whereNull('deleted_at')
                                ->first();

                            if ($g_task['is_print_form'] == 1) {
                                $type = 'print_form';
                            } elseif ($g_task['is_screen_form'] == 1) {
                                $type = 'screen_form';
                            } else {
                                $type = 'task';
                            }

                            foreach ($print_form_id as $pfid) {
                                if($name->grouptasks_id != $pfid->id) {
                                    /*$count_norm += $pt3->norm_contacts;
                                    $count_number += $pt3->number_contacts;*/
                                } else {
                                    $count_print_norm += $pt3->norm_contacts;
                                    $count_print_number += $pt3->number_contacts;
                                    $pf_ar[] = $pfid->id;
                                }
                            }

                            foreach ($screen_form_id as $sfid) {
                                if($name->grouptasks_id != $sfid->id) {
                                    /*$count_norm += $pt3->norm_contacts;
                                    $count_number += $pt3->number_contacts;*/
                                } else {
                                    $count_screen_norm += $pt3->norm_contacts;
                                    $count_screen_number += $pt3->number_contacts;
                                    $sf_ar[] = $sfid->id;
                                }
                            }

                            if (count($ptc) > 0) {
                                $p_tasks[] = [
                                    'id' => $pt3->id,
                                    'name' => $name->task,
                                    'p_name' => $p_name->name,
                                    'p_id' => $pt3->project_id,
                                    'city' => $p_name->city,
                                    'm_name' => $m_name->name,
                                    'g_task' => $g_task->name,
                                    'g_task_id' => $name->grouptasks_id,
                                    'type' => $type,
                                    'additional' => $name->additional,
                                    'status' => $pt3->tasksstatus_ref_id,
                                    'next_contact' => $ptc['next_contact'],
                                    'print_form' => 0,
                                    'norm_contacts' => $pt3->norm_contacts,
                                    'number_contacts' => $pt3->number_contacts
                                ];
                            } else {
                                $p_tasks[] = [
                                    'id' => $pt3->id,
                                    'name' => $name->task,
                                    'p_name' => $p_name->name,
                                    'p_id' => $pt3->project_id,
                                    'city' => $p_name->city,
                                    'm_name' => $m_name->name,
                                    'g_task' => $g_task->name,
                                    'g_task_id' => $name->grouptasks_id,
                                    'type' => $type,
                                    'additional' => $name->additional,
                                    'status' => $pt3->tasksstatus_ref_id,
                                    'next_contact' => null,
                                    'print_form' => 0,
                                    'norm_contacts' => $pt3->norm_contacts,
                                    'number_contacts' => $pt3->number_contacts
                                ];
                            }
                        }
                    } else {
                        $p_tasks[] = '';
                    }

                    $form_ar = array_merge($pf_ar, $sf_ar);
                    foreach ($p_tasks as $tp) {
                        if(!in_array($tp['g_task_id'], $form_ar)) {
                            $count_norm += $tp['norm_contacts'];
                            $count_number += $tp['number_contacts'];
                        }
                    }

                    $count_fact = Project_task_contact::where('project_id', '=', $request->pid)
                        ->whereNull('deleted_at')
                        ->whereNotNull('duration')
                        ->get();

                    $remained = 0;
                    foreach ($count_fact as $cf) {
                        switch ($cf->duration) {
                            case '00:15:00':
                                $remained += 0.5;
                                break;
                            case '00:30:00':
                                $remained += 1;
                                break;
                            case '00:45:00':
                                $remained += 1;
                                break;
                            case '01:00:00':
                                $remained += 1;
                                break;
                            case '01:30:00':
                                $remained += 1.5;
                                break;
                        }
                    }

                    $data = [
                        'p_task' => $p_tasks,
                        'count_norm' => $count_norm,
                        'count_number' => $count_number,
                        'count_print_norm' => $count_print_norm,
                        'count_print_number' => $count_print_number,
                        'count_screen_norm' => $count_screen_norm,
                        'count_screen_number' => $count_screen_number
                    ];

                    break;
            }
            return response()->json($data);
        }
    }

    public function sortTasks(Request $request) {
        if ($request->ajax()) {
            $cur_user = Sentinel::getUser();
            switch ($request->status) {
                case 'notExecute':
                    $project_tasks = Project_task::where('user_id', '=', $cur_user->id)
                        ->whereIn('tasksstatus_ref_id', [1,3])
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($project_tasks) > 0) {
                        foreach ($project_tasks as $pt) {
                            $name = Module_rule::find($pt->module_rule_id);
                            $p_name = Project::find($pt->project_id);
                            $p_client = Clients::getClientById($p_name['client_id']);
                            $p_city = Citys::getCityById($p_client->city_id);

                            $ptc = Project_task_contact::where('project_task_id', '=', $pt->id)
                                ->whereNull('deleted_at')
                                ->first();

                            $user = User::whereNull('deleted_at')->get();
                            foreach ($user as $u) {
                                if ($u->id == $p_name->user_id) {
                                    $observer[] = [
                                        'default' => 1,
                                        'id' => $u->id,
                                        'first_name' => $u->first_name,
                                        'last_name' => $u->last_name
                                    ];
                                } else {
                                    $observer[] = [
                                        'default' => 0,
                                        'id' => $u->id,
                                        'first_name' => $u->first_name,
                                        'last_name' => $u->last_name
                                    ];
                                }
                            }

                            if (count($ptc) > 0) {
                                if ($pt['prev_contact'] != null) {
                                    $p_tasks[] = [
                                        'id' => $pt['id'],
                                        'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                        'client' => $p_client->name,
                                        'name' => $name->task,
                                        'observer' => $observer,
                                        'p_name' => $p_name['name'],
                                        'city' => $p_city,
                                        'next_contact' => $ptc['next_contact'],
                                        'prev_contact' => $pt['prev_contact'],
                                    ];
                                } else {
                                    $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                                        ->whereNull('deleted_at')
                                        //->whereNull('status')
                                        ->max('prev_contact');

                                    $p_tasks[] = [
                                        'id' => $pt['id'],
                                        'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                        'client' => $p_client->name,
                                        'name' => $name->task,
                                        'observer' => $observer,
                                        'p_name' => $p_name['name'],
                                        'city' => $p_city,
                                        'next_contact' => $ptc['next_contact'],
                                        'prev_contact' => $max_date,
                                    ];
                                }
                            } else {
                                if ($pt['prev_contact'] != null) {
                                    $p_tasks[] = [
                                        'id' => $pt['id'],
                                        'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                        'client' => $p_client->name,
                                        'name' => $name->task,
                                        'observer' => $observer,
                                        'p_name' => $p_name['name'],
                                        'city' => $p_city,
                                        'next_contact' => null,
                                        'prev_contact' => $pt['prev_contact'],
                                    ];
                                } else {
                                    $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                                        ->whereNull('deleted_at')
                                        //->whereNull('status')
                                        ->max('prev_contact');

                                    $p_tasks[] = [
                                        'id' => $pt['id'],
                                        'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                        'client' => $p_client->name,
                                        'name' => $name->task,
                                        'observer' => $observer,
                                        'p_name' => $p_name['name'],
                                        'city' => $p_city,
                                        'next_contact' => null,
                                        'prev_contact' => $max_date,
                                    ];
                                }
                            }
                        }
                    } else {
                        $p_tasks[] = '';
                    }
                    return response()->json($p_tasks);
                    break;

                case 'all':
                    $project_tasks = Project_task::where('user_id', '=', $cur_user->id)
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($project_tasks) > 0) {
                        foreach ($project_tasks as $pt) {
                            $name = Module_rule::find($pt->module_rule_id);
                            $p_name = Project::find($pt->project_id);
                            $p_client = Clients::getClientById($p_name['client_id']);
                            $p_city = Citys::getCityById($p_client->city_id);

                            $ptc = Project_task_contact::where('project_task_id', '=', $pt->id)
                                ->whereNull('deleted_at')
                                ->get();

                            $user = User::whereNull('deleted_at')->get();
                            foreach ($user as $u) {
                                if ($u->id == $p_name->user_id) {
                                    $observer[] = [
                                        'default' => 1,
                                        'id' => $u->id,
                                        'first_name' => $u->first_name,
                                        'last_name' => $u->last_name
                                    ];
                                } else {
                                    $observer[] = [
                                        'default' => 0,
                                        'id' => $u->id,
                                        'first_name' => $u->first_name,
                                        'last_name' => $u->last_name
                                    ];
                                }
                            }

                            if (count($ptc) > 0) {
                                if ($pt['prev_contact'] != null) {
                                    $p_tasks[] = [
                                        'id' => $pt['id'],
                                        'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                        'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                                        'client' => $p_client->name,
                                        'name' => $name->task,
                                        'observer' => $observer,
                                        'p_name' => $p_name['name'],
                                        'city' => $p_city,
                                        'next_contact' => $ptc[0]['next_contact'],
                                        'prev_contact' => $pt['prev_contact'],
                                    ];
                                } else {
                                    $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                                        ->whereNull('deleted_at')
                                        //->whereNull('status')
                                        ->max('prev_contact');

                                    $p_tasks[] = [
                                        'id' => $pt['id'],
                                        'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                        'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                                        'client' => $p_client->name,
                                        'name' => $name->task,
                                        'observer' => $observer,
                                        'p_name' => $p_name['name'],
                                        'city' => $p_city,
                                        'next_contact' => $ptc[0]['next_contact'],
                                        'prev_contact' => $max_date,
                                    ];
                                }
                            } else {
                                if ($pt['prev_contact'] != null) {
                                    $p_tasks[] = [
                                        'id' => $pt['id'],
                                        'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                        'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                                        'client' => $p_client->name,
                                        'name' => $name->task,
                                        'observer' => $observer,
                                        'p_name' => $p_name['name'],
                                        'city' => $p_city,
                                        'next_contact' => null,
                                        'prev_contact' => $pt['prev_contact'],
                                    ];
                                } else {
                                    $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                                        ->whereNull('deleted_at')
                                        //->whereNull('status')
                                        ->max('prev_contact');

                                    $p_tasks[] = [
                                        'id' => $pt['id'],
                                        'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                        'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                                        'client' => $p_client->name,
                                        'name' => $name->task,
                                        'observer' => $observer,
                                        'p_name' => $p_name['name'],
                                        'city' => $p_city,
                                        'next_contact' => null,
                                        'prev_contact' => $max_date,
                                    ];
                                }
                            }
                        }
                    } else {
                        $p_tasks[] = '';
                    }
                    return response()->json($p_tasks);
                    break;
            }
        }
    }

    public function calc($new_hour = 0) {
        $p_form = Project_print_form::where('printform_status_ref_id', '!=', 5)
            ->whereNull('deleted_at')
            ->get();

        $hour = 0;
        if (count($p_form) > 0) {
            foreach ($p_form as $pf) {
                $hour += $pf->max_hours;
            }
            $res = (($hour + $new_hour) / 8) / 2;

            $cur_date = date("j").'.'.date("n").'.'.date("Y");
            $date = date('Y-m-d', strtotime($cur_date . ' +'.round($res).' day'));
            //$tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +'.round($res).' day'));
        } else {
            $date = date("Y").'-'.date("m").'-'.date("d");
        }

        return $date;
    }

    public function addAdditionalTask(Request $request) {
        $data = json_decode($request->data);
        $module_rule = new Module_rule();
        $module_rule->module_id = $data->module->id;
        $module_rule->task = $data->task->value;
        $module_rule->grouptasks_id = $data->groupTask->id;
        $module_rule->additional = 1;
        $module_rule->number_contacts = $data->numCont;


        if ($module_rule->save()) {
            $cur_user = Sentinel::getUser();
            try {
                $project_task = new Project_task();
                $project_task->project_id = $data->project;
                $project_task->user_id = $cur_user->id;
                $project_task->observer_id = $cur_user->id;
                $project_task->module_rule_id = $module_rule->id;
                $project_task->number_contacts = $data->numCont;
                $project_task->grouptasks_id = $data->groupTask->id;
                $project_task->module_id = $data->module->id;
                $project_task->tasksstatus_ref_id = 1;
                $project_task->save();

                $success = true;
            } catch (\Exception $e) {
                $success = false;
            }

            if ($success) {
                return response()->json(['status' => 'Ok']);
            } else {
                return response()->json(['status' => 'Error']);
            }
        }
    }

    public function searchTasks(Request $request) {
        if ($request->ajax()) {
            $cur_user = Sentinel::getUser();
            $project_tasks = Project_task::where('user_id', '=', $cur_user->id)
                ->whereNull('deleted_at')
                ->get();

            if (count($project_tasks) > 0) {
                foreach ($project_tasks as $pt) {
                    $name = Module_rule::find($pt->module_rule_id);
                    $p_name = Project::find($pt->project_id);
                    $p_client = Clients::getClientById($p_name['client_id']);
                    $p_city = Citys::getCityById($p_client->city_id);
                    $ptc = Project_task_contact::where('project_task_id', '=', $pt->id)
                        ->whereNull('deleted_at')
                        ->first();

                    $user = User::whereNull('deleted_at')->get();
                    foreach ($user as $u) {
                        if ($u->id == $p_name->user_id) {
                            $observer[] = [
                                'default' => 1,
                                'id' => $u->id,
                                'first_name' => $u->first_name,
                                'last_name' => $u->last_name
                            ];
                        } else {
                            $observer[] = [
                                'default' => 0,
                                'id' => $u->id,
                                'first_name' => $u->first_name,
                                'last_name' => $u->last_name
                            ];
                        }
                    }

                    if (count($ptc) > 0) {
                        if ($pt['prev_contact'] != null) {
                            $p_tasks[] = [
                                'id' => $pt['id'],
                                'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                                'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                'client' => $p_client->name,
                                'name' => $name->task,
                                'observer' => $observer,
                                'p_name' => $p_name['name'],
                                'city' => $p_city,
                                'next_contact' => $ptc['next_contact'],
                                'prev_contact' => $pt['prev_contact'],
                            ];
                        } else {
                            $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                                ->whereNull('deleted_at')
                                //->whereNull('status')
                                ->max('prev_contact');

                            $p_tasks[] = [
                                'id' => $pt['id'],
                                'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                                'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                'client' => $p_client->name,
                                'name' => $name->task,
                                'observer' => $observer,
                                'p_name' => $p_name['name'],
                                'city' => $p_city,
                                'next_contact' => $ptc['next_contact'],
                                'prev_contact' => $max_date,
                            ];
                        }
                    } else {
                        if ($pt['prev_contact'] != null) {
                            $p_tasks[] = [
                                'id' => $pt['id'],
                                'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                'client' => $p_client->name,
                                'name' => $name->task,
                                'observer' => $observer,
                                'p_name' => $p_name['name'],
                                'city' => $p_city,
                                'next_contact' => null,
                                'prev_contact' => $pt['prev_contact'],
                            ];
                        } else {
                            $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                                ->whereNull('deleted_at')
                                //->whereNull('status')
                                ->max('prev_contact');

                            $p_tasks[] = [
                                'id' => $pt['id'],
                                'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                                'client' => $p_client->name,
                                'name' => $name->task,
                                'observer' => $observer,
                                'p_name' => $p_name['name'],
                                'city' => $p_city,
                                'next_contact' => null,
                                'prev_contact' => $max_date,
                            ];
                        }
                    }
                }
            }

            $res = '';
            foreach ($p_tasks as $p_task) {
                if (strpos($p_task['client'], $request->text) || strpos($p_task['name'], $request->text)) {
                    $res[] = $p_task;
                }
            }

            return response()->json($res);
        }
    }

//    public function searchProjects(Request $request) {
//        if ($request->ajax()) {
//            $cur_user = Sentinel::getUser();
//            $projects_q = Project::where('user_id', '=', $cur_user->id)
//                ->whereNULL('deleted_at')
//                ->get();
//
//            if (count($projects_q) > 0) {
//                foreach ($projects_q as $p) {
//                    $task = Project_task::where('project_id', '=', $p->id)
//                        ->where('user_id', '=', $cur_user->id)
//                        ->whereNull('deleted_at')
//                        ->get();
//
//                    $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
//                        ->whereNull('deleted_at')
//                        ->get();
//
//                    $client = Clients::getClientById($p->client_id);
//                    $city = Citys::getCityById($client->city_id);
//
//                    foreach ($task as $t) {
//                        if ($t->tasksstatus_ref_id == 2) {
//                            $done2[$p->id][] = $t->id;
//                        }
//                    }
//
//                    if (isset($done2[$p->id])) {
//                        $user = Users2::getUserById($p->user_id);
//                        $client = Clients::getClientById($p->client_id);
//                        $projects[] = [
//                            'id' => $p->id,
//                            'user' => $user->last_name.' '.$user->first_name,
//                            'name' => $p->name,
//                            'client' => $client->name,
//                            'city' => $city,
//                            'prev_contact' => $ptc[0]['prev_contact'],
//                            'next_contact' => $ptc[0]['next_contact'],
//                            'start_date' => $p->start_date,
//                            'finish_date' => $p->finish_date,
//                            'created_at' => $p->created_at,
//                            'active' => $p->active,
//                            'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
//                        ];
//                    } else {
//                        $user = Users2::getUserById($p->user_id);
//                        $client = Clients::getClientById($p->client_id);
//                        $projects[] = [
//                            'id' => $p->id,
//                            'user' => $user->last_name.' '.$user->first_name,
//                            'name' => $p->name,
//                            'client' => $client->name,
//                            'city' => $city,
//                            'prev_contact' => $ptc[0]['prev_contact'],
//                            'next_contact' => $ptc[0]['next_contact'],
//                            'start_date' => $p->start_date,
//                            'finish_date' => $p->finish_date,
//                            'created_at' => $p->created_at,
//                            'active' => $p->active,
//                            //'done' => 0,
//                            'done' => round(((0 / count($task)) * 100), 2),
//                        ];
//                    }
//                }
//            } else {
//                $projects[] = '';
//            }
//
//            $res = '';
//            foreach ($projects as $p) {
//                if ($p['client'] == $request->text ||
//                    $p['name'] == $request->text ||
//                    $p['user'] == $request->text) {
//                    $res[] = $p;
//                }
//            }
//
//            return response()->json($res);
//        }
//    }

    public function getProjectByClientId(Request $request) {
        if ($request->ajax()) {
            $projects = Project::where('client_id', '=', $request->id)
                ->whereNull('deleted_at')
                ->get();
            return response()->json($projects);
        }
    }

    public function getCloseProject() {
        $cur_user = Sentinel::getUser();
        if ($cur_user->head != 1) {
            $projects_q = Project::where('user_id', '=', $cur_user->id)
                ->where('done', '=', 1)
                ->whereNULL('deleted_at')
                ->whereNULL('raw')
                ->get();
        } else {
            $projects_q = Project::whereNULL('deleted_at')
                ->where('done', '=', 1)
                ->whereNULL('raw')
                ->get();
        }

        if (count($projects_q) > 0) {
            foreach ($projects_q as $p) {
                if ($cur_user->head != 1) {
                    $task = Project_task::where('project_id', '=', $p->id)
                        ->where('user_id', '=', $cur_user->id)
                        ->whereNull('deleted_at')
                        ->get();
                } else {
                    $task = Project_task::where('project_id', '=', $p->id)
                        ->whereNull('deleted_at')
                        ->get();
                }

                if (count($task) > 0) {
                    $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($ptc) > 0) {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);

                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    } else {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);
                        //dd($client);
                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    }
                } else {
                    $task = Project_print_form::where('project_id', '=', $p->id)
                        ->where('user_id', '=', $cur_user->id)
                        ->whereNull('deleted_at')
                        ->get();

                    $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($ptc) > 0) {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);

                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    } else {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);
                        //dd($client);
                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    }
                }
            }
        } else {
            $projects = '';
        }

        return response()->json($projects);
    }

    public function getSlipProject() {
        $cur_user = Sentinel::getUser();
        if ($cur_user->head != 1) {
            $projects_q = DB::select("select `projects`.* from `projects` left join `project_task_contacts` on `projects`.`id` = `project_task_contacts`.`project_id` where `project_task_contacts`.`prev_contact` <= '".Carbon::now()->subDays(7)."' and `projects`.`user_id` = ".$cur_user->id." and `projects`.`done` != 1 and `projects`.`deleted_at` IS NULL GROUP BY `projects`.`id`");
        } else {
            $projects_q = DB::select("select `projects`.* from `projects` left join `project_task_contacts` on `projects`.`id` = `project_task_contacts`.`project_id` where `project_task_contacts`.`prev_contact` <= '".Carbon::now()->subDays(7)."' and `projects`.`done` != 1 and `projects`.`deleted_at` IS NULL GROUP BY `projects`.`id`");
        }

        if (count($projects_q) > 0) {
            foreach ($projects_q as $p) {
                if ($cur_user->head != 1) {
                    $task = Project_task::where('project_id', '=', $p->id)
                        ->where('user_id', '=', $cur_user->id)
                        ->whereNull('deleted_at')
                        ->get();
                } else {
                    $task = Project_task::where('project_id', '=', $p->id)
                        ->whereNull('deleted_at')
                        ->get();
                }

                if (count($task) > 0) {
                    $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($ptc) > 0) {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);

                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    } else {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);
                        //dd($client);
                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    }
                } else {
                    $task = Project_print_form::where('project_id', '=', $p->id)
                        ->where('user_id', '=', $cur_user->id)
                        ->whereNull('deleted_at')
                        ->get();

                    $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($ptc) > 0) {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);

                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    } else {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);
                        //dd($client);
                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    }
                }
            }
        } else {
            $projects = '';
        }

        return response()->json($projects);
    }

    public function getTodayProject() {
        $cur_user = Sentinel::getUser();
        if ($cur_user->head != 1) {
            $projects_q = DB::select("select `projects`.* from `projects` left join `project_task_contacts` on `projects`.`id` = `project_task_contacts`.`project_id` where `project_task_contacts`.`next_contact` = '".Carbon::today()->format('Y-m-d')."' and `projects`.`user_id` = ".$cur_user->id." and `projects`.`done` != 1 and `projects`.`deleted_at` IS NULL GROUP BY `projects`.`id`");
        } else {
            $projects_q = DB::select("select `projects`.* from `projects` left join `project_task_contacts` on `projects`.`id` = `project_task_contacts`.`project_id` where `project_task_contacts`.`next_contact` = '".Carbon::today()->format('Y-m-d')."' and `projects`.`done` != 1 and `projects`.`deleted_at` IS NULL GROUP BY `projects`.`id`");
        }

        if (count($projects_q) > 0) {
            foreach ($projects_q as $p) {
                if ($cur_user->head != 1) {
                    $task = Project_task::where('project_id', '=', $p->id)
                        ->where('user_id', '=', $cur_user->id)
                        ->whereNull('deleted_at')
                        ->get();
                } else {
                    $task = Project_task::where('project_id', '=', $p->id)
                        ->whereNull('deleted_at')
                        ->get();
                }

                if (count($task) > 0) {
                    $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($ptc) > 0) {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);

                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    } else {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);
                        //dd($client);
                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    }
                } else {
                    $task = Project_print_form::where('project_id', '=', $p->id)
                        ->where('user_id', '=', $cur_user->id)
                        ->whereNull('deleted_at')
                        ->get();

                    $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($ptc) > 0) {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);

                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    } else {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);
                        //dd($client);
                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    }
                }
            }
        } else {
            $projects = '';
        }

        return response()->json($projects);
    }

    public function getFireProject() {
        $cur_user = Sentinel::getUser();
        if ($cur_user->head != 1) {
            $projects_q = DB::select("select * from `projects` where `projects`.`finish_date` <= '".Carbon::today()->format('Y-m-d')."' and `projects`.`user_id` = ".$cur_user->id." and `projects`.`done` != 1 and `projects`.`deleted_at` IS NULL ");
        } else {
            $projects_q = DB::select("select * from `projects` where `projects`.`finish_date` <= '".Carbon::today()->format('Y-m-d')."' and `projects`.`done` != 1 and `projects`.`deleted_at` IS NULL ");
        }

        if (count($projects_q) > 0) {
            foreach ($projects_q as $p) {
                if ($cur_user->head != 1) {
                    $task = Project_task::where('project_id', '=', $p->id)
                        ->where('user_id', '=', $cur_user->id)
                        ->whereNull('deleted_at')
                        ->get();
                } else {
                    $task = Project_task::where('project_id', '=', $p->id)
                        ->whereNull('deleted_at')
                        ->get();
                }

                if (count($task) > 0) {
                    $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($ptc) > 0) {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);

                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    } else {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);
                        //dd($client);
                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    }
                } else {
                    $task = Project_print_form::where('project_id', '=', $p->id)
                        ->where('user_id', '=', $cur_user->id)
                        ->whereNull('deleted_at')
                        ->get();

                    $ptc = Project_task_contact::where('project_task_id', '=', $task[0]->id)
                        ->whereNull('deleted_at')
                        ->get();

                    if (count($ptc) > 0) {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);

                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => substr($ptc[0]['prev_contact'], 0, -3),
                                'next_contact' => substr($ptc[0]['next_contact'], 0, -3),
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    } else {
                        $client = Clients::getClientById($p->client_id);
                        $city = Citys::getCityById($client->city_id);
                        //dd($client);
                        foreach ($task as $t) {
                            if ($t->tasksstatus_ref_id == 2) {
                                $done2[$p->id][] = $t->id;
                            }
                        }

                        if (isset($done2[$p->id])) {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                'done' => round(((count($done2[$p->id]) / count($task)) * 100), 2),
                            ];
                        } else {
                            $user = Users2::getUserById($p->user_id);
                            $client = Clients::getClientById($p->client_id);
                            $projects[] = [
                                'id' => $p->id,
                                'close' => $p->done,
                                'user' => $user->last_name . ' ' . $user->first_name,
                                'name' => $p->name,
                                'client' => $client->name,
                                'city' => $city,
                                'prev_contact' => NULL,
                                'next_contact' => NULL,
                                'start_date' => $p->start_date,
                                'finish_date' => $p->finish_date,
                                'created_at' => Carbon::parse($p->created_at)->format('d.m.Y H:i'),
                                'active' => $p->active,
                                //'done' => 0,
                                'done' => round(((0 / count($task)) * 100), 2),
                            ];
                        }
                    }
                }
            }
        } else {
            $projects = '';
        }

        return response()->json($projects);
    }

    public function getProjectByClientName(Request $request) {
        if ($request->ajax()) {
            $client = Clients_reference::where('name', '=', $request->client)
                ->whereNull('deleted_at')
                ->first();
            $projects = Project::where('client_id', '=', $client['id'])
                ->whereNull('deleted_at')
                ->get();
            return response()->json($projects);
        }
    }

    public function saveRaw(Request $request) {
        $data = json_decode($request->data);
        $project = Project::find($data->project_id);
        $project->raw = Null;
        $project->start_date = $data->start_date;
        $project->finish_date = $data->end_date;
        $project->user_id = $data->user_id;
        $project->category_id = $data->project_category;

        $p_task = Project_task::where('project_id', '=', $data->project_id)
            ->whereNull('deleted_at')
            ->get();
        foreach ($p_task as $pt) {
            $pt->user_id = $data->user_id;
            $pt->save();
        }

        if ($project->save()) {
            $user = User::find($data->user_id);
            event(new saveRaw($project->name, $user->first_name.' '.$user->last_name));
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function getRaw($id) {
        $project = Project::find($id);
        $client = Clients::getClientById($project->client_id);
        $city = Citys::getCityById($client->city_id);
        $project_categories = ProjectCategory::select('id', 'name')->get();
        $users = Users2::getImplementers();
        $project_modules = Project_module::where('project_id', '=', $id)
            ->whereNull('deleted_at')
            ->get();
        if (count($project_modules) > 0) {
            foreach ($project_modules as $m) {
                $m_name = Modules_reference::find($m->module_id);
                $modules[] = [
                    'name' => $m_name->name,
                    'count' => $m->num
                ];
            }
        } else {
            $modules = '';
        }

        $contacts_q = Contact::where('client_id', '=', $client->id)
            ->whereNull('deleted_at')
            ->get();
        // dd($client->id);
        if (count($contacts_q) > 0) {
            foreach ($contacts_q as $c) {
                $post = Posts_reference::find($c->post_id);
                $contacts[] = [
                    'last_name' => $c->last_name,
                    'first_name' => $c->first_name,
                    'patronymic' => $c->patronymic,
                    'phone' => $c->phone,
                    'email' => $c->email,
                    'post' => $post->name ?? ''
                ];
            }
        } else {
            $contacts = '';
        }
        // print_r('Ok');
        //     die;
        $check_list_q = Check_list_answer::where('project_id', '=', $id)
            ->whereNull('deleted_at')
            ->get();
        if (count($check_list_q) > 0) {
            foreach ($check_list_q as $cl) {
                $question = Check_list_reference::find($cl->check_list_ref_id);
                $check_list[] = array(
                    'id' => $cl->id,
                    'questuon' => strip_tags($question->question),
                    'answer' => $cl->answer
                );
            }
        } else {
            $check_list = '';
        }


        return response()->json([
            'client' => $client,
            'project' => $project,
            'created_date' => Carbon::parse($project->created_at)->format('d.m.Y H:i'),
            'city' => $city,
            'users' => $users,
            'modules' => $modules,
            'contacts' => $contacts,
            'check_list' => $check_list,
            'project_categories' => $project_categories
        ]);
    }

    public function raw($id) {
        $project = Project::find($id);
        $client = Clients::getClientById($project->client_id);
        $city = Citys::getCityById($client->city_id);
        $users = Users2::getAllUsers();
        $project_modules = Project_module::where('project_id', '=', $id)
            ->whereNull('deleted_at')
            ->get();
        if (count($project_modules) > 0) {
            foreach ($project_modules as $m) {
                $m_name = Modules_reference::find($m->module_id);
                $modules[] = [
                    'name' => $m_name->name,
                    'count' => $m->num
                ];
            }
        } else {
            $modules = '';
        }
        $contacts_q = Contact::where('client_id', '=', $client->id)
            ->whereNull('deleted_at')
            ->get();
        if (count($contacts_q) > 0) {
            foreach ($contacts_q as $c) {
                $post = Posts_reference::find($c->post_id);
                $contacts[] = [
                    'last_name' => $c->last_name,
                    'first_name' => $c->first_name,
                    'patronymic' => $c->patronymic,
                    'phone' => $c->phone,
                    'email' => $c->email,
                    'post' => $post->name ?? ''
                ];
            }
        } else {
            $contacts = '';
        }

        $check_list_q = Check_list_answer::where('project_id', '=', $id)
            ->whereNull('deleted_at')
            ->get();
        if (count($check_list_q) > 0) {
            foreach ($check_list_q as $cl) {
                $question = Check_list_reference::find($cl->check_list_ref_id);
                $check_list[] = array(
                    'id' => $cl->id,
                    'questuon' => strip_tags($question->question),
                    'answer' => $cl->answer
                );
            }
        } else {
            $check_list = '';
        }


        return view('Implementer.projects.raw', [
            'client' => $client,
            'project' => $project,
            'created_date' => Carbon::parse($project->created_at)->format('d.m.Y H:i'),
            'city' => $city,
            'users' => $users,
            'modules' => $modules,
            'contacts' => $contacts,
            'check_list' => $check_list
        ]);
    }

    public function postRaw(Request $request, $id) {
        $project = Project::find($id);
        $project->raw = Null;
        $project->start_date = $request->startDate;
        $project->finish_date = $request->finishDate;
        $project->user_id = $request->user;

        $p_task = Project_task::where('project_id', '=', $id)
            ->whereNull('deleted_at')
            ->get();
        foreach ($p_task as $pt) {
            $pt->user_id = $request->user;
            $pt->save();
        }
        /*Project_task_contact::create([
            'project_id' => $p_task[0]->project_id,
            'project_task_id' => $p_task[0]->id,
            'user_id' => $request->user,
            'callbackstatus_ref_id' => 1,
            'status' => 1
        ]);*/

        //dd($p_task_contact);

        if ($project->save()) {
            return redirect('/implementer/projects')->with('success', 'Данные успешно обновлены');
        } else {
            return redirect()->back()->with('error', 'Произошла обибка при обновлении данных');
        }
    }

    static public function getProjectById($id) {
        $project = Project::whereNull('deleted_at')
            ->where('id', '=', $id)
            ->firstOrFail();
        return $project;
    }

    public function getProjectTask($id) {
        $project_tasks = Project_task::where('project_id', '=', $id)
            ->whereNull('deleted_at')
            ->get();

        if (count($project_tasks) > 0) {
            foreach ($project_tasks as $pt) {
                $name = Module_rule::find($pt->module_rule_id);
                $p_name = Project::find($pt->project_id);
                $p_client = Clients::getClientById($p_name['client_id']);
                $p_city = Citys::getCityById($p_client->city_id);
                $ptc = Project_task_contact::where('project_task_id', '=', $pt->id)
                    ->whereNull('deleted_at')
                    ->first();

                $user = User::whereNull('deleted_at')->get();
                foreach ($user as $u) {
                    if ($u->id == $p_name->user_id) {
                        $observer[] = [
                            'default' => 1,
                            'id' => $u->id,
                            'first_name' => $u->first_name,
                            'last_name' => $u->last_name
                        ];
                    } else {
                        $observer[] = [
                            'default' => 0,
                            'id' => $u->id,
                            'first_name' => $u->first_name,
                            'last_name' => $u->last_name
                        ];
                    }
                }

                if (count($ptc) > 0) {
                    if ($pt['prev_contact'] != null) {
                        $p_tasks[] = [
                            'id' => $pt['id'],
                            'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                            'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                            'client' => $p_client->name,
                            'name' => $name->task,
                            'observer' => $observer,
                            'p_name' => $p_name['name'],
                            'city' => $p_city,
                            'next_contact' => $ptc['next_contact'],
                            'prev_contact' => $pt['prev_contact'],
                        ];
                    } else {
                        $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                            ->whereNull('deleted_at')
                            //->whereNull('status')
                            ->max('prev_contact');

                        $p_tasks[] = [
                            'id' => $pt['id'],
                            'tasksstatus_ref_id' => $pt['tasksstatus_ref_id'],
                            'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                            'client' => $p_client->name,
                            'name' => $name->task,
                            'observer' => $observer,
                            'p_name' => $p_name['name'],
                            'city' => $p_city,
                            'next_contact' => $ptc['next_contact'],
                            'prev_contact' => $max_date,
                        ];
                    }
                } else {
                    if ($pt['prev_contact'] != null) {
                        $p_tasks[] = [
                            'id' => $pt['id'],
                            'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                            'client' => $p_client->name,
                            'name' => $name->task,
                            'observer' => $observer,
                            'p_name' => $p_name['name'],
                            'city' => $p_city,
                            'next_contact' => null,
                            'prev_contact' => $pt['prev_contact'],
                        ];
                    } else {
                        $max_date = Project_task_contact::where('project_id', '=', $pt->project_id)
                            ->whereNull('deleted_at')
                            //->whereNull('status')
                            ->max('prev_contact');

                        $p_tasks[] = [
                            'id' => $pt['id'],
                            'callbackstatus_ref_id' => $pt['callbackstatus_ref_id'],
                            'client' => $p_client->name,
                            'name' => $name->task,
                            'observer' => $observer,
                            'p_name' => $p_name['name'],
                            'city' => $p_city,
                            'next_contact' => null,
                            'prev_contact' => $max_date,
                        ];
                    }
                }
            }
        }

        return response()->json($p_tasks);
    }

    static public function getProjectByTaskId($task_id) {
        $task = Project_task::find($task_id);
        $project = Project::find($task->project_id);

        return $project;
    }

    static public function getAllProjects() {
        $projects = Project::whereNull('deleted_at')->get();
        return $projects;
    }

    static public function getUserProjects($user_id) {
        $projects = Project::where('user_id', '=', $user_id)
            ->where('active', '=', 1)
            ->whereNull('deleted_at')
            ->get();

        return $projects;
    }

    static public function getUserTasks($user_id) {
        $tasks = Project_task::where('user_id', '=', $user_id)
            ->whereNull('deleted_at')
            ->get();

        if (count($tasks) > 0) {
            foreach ($tasks as $t) {
                $module_rule = Module_rule::find($t->module_rule_id);
                $grouptask = Grouptasks_reference::find($module_rule->grouptask_id);
                if ($grouptask['is_print_form'] != 1) {
                    $tasks_ar[] = [
                        'id' => $t['id'],
                        'number_contacts' => $t['number_contacts'],
                    ];
                }
            }
        } else {
            $tasks_ar = [];
        }

        return $tasks_ar;
    }

    static public function getClientProjects($cid) {
        $projects = Project::where('client_id', '=', $cid)
            ->whereNull('deleted_at')
            ->get();

        return $projects;
    }

    public function editQuestions(Request $request) {
        $data = json_decode($request->data);
        $res = false;

        foreach($data->question as $q) {
            if ($q->aid != '') {
                $answer = Clarifying_answer::find($q->aid);
                $answer->question_id = $q->gid;
                $answer->project_id = $data->project_id;
                if ($q->answer != '') {
                    $answer->answer = $q->answer;
                } else {
                    $answer->answer = '';
                }
            } else {
                $answer = new Clarifying_answer();
                $answer->question_id = $q->gid;
                $answer->project_id = $data->project_id;
                if ($q->answer != '') {
                    $answer->answer = $q->answer;
                } else {
                    $answer->answer = '';
                }
            };

            if ($answer->save()) {
                $res = true;
            } else {
                $res = false;
            }
        }


        if ($res == true) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        };
    }

    public function addComment(Request $request) {
        $data = json_decode($request->data);
        $date =  explode(' ', date("Y-m-d H:i:s"));
        $cur_user = Sentinel::getUser();
        try {
            $comment = new Comment();
            $comment->project_id = $data->project;
            $comment->user_id = $cur_user->id;
            $comment->for_client = NULL;
            $comment->close = 1;
            $comment->date = $date[0];
            $comment->time = $date[1];
            $comment->text = $data->comment;
            $comment->save();

            $this->success = true;
        } catch (\Exception $e) {
            $this->success = false;
        }

        if ($this->success) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function editCont(Request $request) {
        $data = json_decode($request->data);
        // dd($data);
        $cont = Contact::find($data->cont->id);
        $cont->first_name = $data->cont->first_name;
        $cont->last_name = $data->cont->last_name;
        $cont->patronymic = $data->cont->patronymic;
        $cont->email = $data->cont->email;
        $cont->post_id = $data->cont->post;
        if ($cont->save()) {
            $p_cont = Project_contact::where('contact_id', '=', $cont->id)
                ->where('project_id', '=', $data->p_id)
                ->first();

            $p_cont->main = $data->cont->main;
            $p_cont->mail_rep = $data->cont->mail_rep;
            if ($p_cont->save()) {
                return response()->json(['status' => 'Ok']);
            } else {
                return response()->json(['status' => 'Error']);
            }
        } else {
            return response()->json(['status' => 'Error']);
        }
    }
}
