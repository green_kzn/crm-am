<?php

namespace App\Http\Controllers\Implementer;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Implementer\ImpGroupTasksController as GTasks;
use App\Modules_reference;
use App\Grouptasks_reference;
use App\Further_questions_reference;
use App\Module_rule;
use App\Project_module;
use App\Clarifying_question;

class ImpModules2Controller extends Controller
{
    public function index() {
        $modules = Modules_reference::whereNULL('deleted_at')
            ->get();

        return response()->json($modules);
    }

    public function add() {
        if (session('perm')['module_ref.create']) {
            $tasks = Grouptasks_reference::whereNULL('deleted_at')->get();
            return view('Implementer.references.modules.add', [
                'tasks' => $tasks,
            ]);
        } else {
            abort(503);
        }
    }

    public function edit($id) {
        if (session('perm')['module_ref.update']) {
            $module = Modules_reference::find($id);
            $tasks = Grouptasks_reference::whereNULL('deleted_at')->get();
            $further_questions = Further_questions_reference::all();
            $module_rule = Module_rule::where('module_id', '=', $id)
                ->whereNull('deleted_at')
                ->whereNull('additional')
                ->get();

            if (count($module_rule) > 0) {
                foreach ($module_rule as $mr) {
                    $group_task = Grouptasks_reference::find($mr->grouptasks_id);
                    $c_question = Clarifying_question::where('task_id', '=', $mr->id)
                        ->whereNull('deleted_at')
                        ->get();
                    $question = array();
                    foreach ($c_question as $cq) {
                        if ($cq->task_id == $mr->id) {
                            $question[] = $cq->question;
                        }
                    }

                    $rules[] = [
                        'id' => $mr->id,
                        'task' => $mr->task,
                        'norm_contacts' => $mr->norm_contacts,
                        'number_contacts' => $mr->number_contacts,
                        'grouptasks' => $group_task->name,
                        'questions' => $question,
                    ];
                }
            } else {
                $rules = '';
            }

            return view('Implementer.references.modules.edit', [
                'module' => $module,
                'tasks' => $tasks,
                'further_questions' => $further_questions,
                'module_rule' => $rules
            ]);
        } else {
            abort(503);
        }
    }

    public function postAdd(Request $request)
    {
        $data = json_decode($request->data);
        $this->success = false;

        DB::transaction(function () use ($request, $data) {
            try {
                $modules = new Modules_reference();
                $modules->name = $data->module_name;
                $modules->save();

                foreach ($data->Module_ar as $t) {
                    //$g_task = GTasks::getGroupTaskByName($t->group_tasks);

                    $m_tasks = new Module_rule();
                    $m_tasks->module_id = $modules->id;
                    $m_tasks->task = $t->Task;
                    $m_tasks->norm_contacts = $t->Norm;
                    $m_tasks->number_contacts = $t->Count;
                    $m_tasks->grouptasks_id = $t->Group->id;
                    $m_tasks->save();

                    foreach ($t->Questions as $cq) {
                        if (!is_null($cq->name)) {
                            $c_question = new Clarifying_question();
                            $c_question->task_id = $m_tasks->id;
                            $c_question->question = $cq->name;
                            $c_question->save();
                        }
                    }
                }

                $this->success = true;
            } catch (\Exception $e) {
                DB::rollback();
                $this->success = false;
            }
        });

        if ($this->success) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function getModule($id) {
        $module = Modules_reference::find($id);
        $further_questions = Further_questions_reference::all();
        $module_rule = Module_rule::where('module_id', '=', $id)
            ->whereNull('deleted_at')
            ->whereNull('additional')
            ->get();

        if (count($module_rule) > 0) {
            foreach ($module_rule as $mr) {
                $group_task = Grouptasks_reference::find($mr->grouptasks_id);
                $c_question = Clarifying_question::where('task_id', '=', $mr->id)
                    ->whereNull('deleted_at')
                    ->get();
                $question = array();
                foreach ($c_question as $cq) {
                    if ($cq->task_id == $mr->id) {
                        $question[] = [
                            'name' => $cq->question,
                            'id' => $cq->id
                        ];
                    }
                }

                $rules[] = [
                    'id' => $mr->id,
                    'Task' => $mr->task,
                    'Norm' => $mr->norm_contacts,
                    'Count' => $mr->number_contacts,
                    'Group' => [
                        'name' => $group_task->name,
                        'id' => $group_task->id,
                    ],
                    'Questions' => $question,
                ];
            }
        } else {
            $rules = '';
        }

        return response()->json([
            'module' => $module,
            'rules' => $rules
        ]);
    }

    public function postEdit(Request $request, $id) {
        $data = json_decode($request->data);
        $this->success = false;

        DB::transaction(function () use ($data, $id) {
            try {
                $modules = Modules_reference::find($id);
                $modules->name = $data->module_name;
                $modules->save();

                foreach ($data->Module_ar as $t) {
                    if (isset($t->id)) {
                        //$g_task = GTasks::getGroupTaskByName($t->group_tasks);
                        $m_tasks = Module_rule::find($t->id);
                        $m_tasks->module_id = $modules->id;
                        $m_tasks->task = $t->Task;
                        $m_tasks->norm_contacts = $t->Norm;
                        $m_tasks->number_contacts = $t->Count;
                        $m_tasks->grouptasks_id = $t->Group->id;
                        $m_tasks->save();

                        foreach ($t->Questions as $cq) {
                            if (isset($cq->id)) {
                                $c_question = Clarifying_question::find($cq->id);
                                $c_question->task_id = $m_tasks->id;
                                $c_question->question = $cq->name;
                                $c_question->save();
                            } else {
                                $c_question = new Clarifying_question();
                                $c_question->task_id = $m_tasks->id;
                                $c_question->question = $cq->name;
                                $c_question->save();
                            }
                        }
                    } else {
                        $m_tasks = new Module_rule();
                        $m_tasks->module_id = $modules->id;
                        $m_tasks->task = $t->Task;
                        $m_tasks->norm_contacts = $t->Norm;
                        $m_tasks->number_contacts = $t->Count;
                        $m_tasks->grouptasks_id = $t->Group->id;
                        $m_tasks->save();

                        foreach ($t->Questions as $cq) {
                            if (!is_null($cq->name)) {
                                $c_question = new Clarifying_question();
                                $c_question->task_id = $m_tasks->id;
                                $c_question->question = $cq->name;
                                $c_question->save();
                            }
                        }
                    }
                }

                $this->success = true;
            } catch (\Exception $e) {
                DB::rollback();
                $this->success = false;
            }
        });

        if ($this->success) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function delModuleRule(Request $request) {
        $data = json_decode($request->data);
        $module_rule = Module_rule::find($data->rule_id->id);

        $module_rule->deleted_at = date("Y-m-d H:i:s");

        if ($module_rule->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function del(Request $request) {
        $data = json_decode($request->data);
        $p_module = Project_module::where('module_id', '=', $data->id)
            ->whereNull('deleted_at')
            ->count();

        if ($p_module > 0) {
            return response()->json(['status' => 'Error']);
        } else {
            DB::transaction(function () use ($data) {
                try {
                    $module = Modules_reference::find($data->id);
                    $module->deleted_at = date("Y-m-d H:i:s");
                    $module->save();

                    $m_rule = Module_rule::where('module_id', '=', $data->id)->get();
                    foreach ($m_rule as $mr) {
                        $c_question = Clarifying_question::where('task_id', '=', $mr->id)->get();
                        foreach ($c_question as $cq) {
                            $cq->deleted_at = date("Y-m-d H:i:s");
                            $cq->save();
                        }

                        $mr->deleted_at = date("Y-m-d H:i:s");
                        $mr->save();
                    }

                    $this->success = true;
                } catch (\Exception $e) {
                    DB::rollback();
                    $this->success = false;
                };
            });

            if ($this->success) {
                return response()->json(['status' => 'Ok']);
            } else {
                return response()->json(['status' => 'Error']);
            }
        }
    }

    public function getModuleRule(Request $request) {
        if ($request->ajax()) {
            $rule = Module_rule::where('module_id', '=', $request->id)
                ->whereNull('deleted_at')
                ->whereNull('additional')
                ->get();
            //dd($rule);
            $norm_contacts = 0;
            $number_contacts = 0;
            $num_printform = 0;
            foreach ($rule as $r) {
                $norm_contacts += $r->norm_contacts;
                $number_contacts += $r->number_contacts;
                if ($r['grouptasks_id'] == 11) {
                    $num_printform += 1;
                }
            }
            $resp = [
                'norm_contacts' => $norm_contacts,
                'number_contacts' => $number_contacts,
                'num_printform' => $num_printform,
            ];
            return response()->json($resp);
        }
    }

    public function getModuleTasks(Request $request) {
        if ($request->ajax()) {
            $tasks_q = Module_rule::where('module_id', '=', $request->id)
                ->whereNull('deleted_at')
                ->whereNull('additional')
                ->get();
            foreach ($tasks_q as $t) {
                $group_task = Grouptasks_reference::where('id', '=', $t->grouptasks_id)
                    ->whereNull('deleted_at')
                    ->firstOrFail();

                $tasks[] = [
                    'id' => $t->id,
                    'task' => $t->task,
                    'norm_contacts' => $t->norm_contacts,
                    'number_contacts' => $t->number_contacts,
                    'grouptasks' => $group_task->name,
                ];
            }

            return response()->json($tasks);
        }
    }

    static public function getAllModule() {
        $module = Modules_reference::whereNull('deleted_at')->get();
        return $module;
    }

    public function addModulleRule(Request $request) {
        if ($request->ajax()) {
            $grouptasks_id = Grouptasks_reference::where('name', '=', $request->group_tasks)
                ->firstOrFail();
            $module_rule = new Module_rule();
            $module_rule->module_id = $request->module_id;
            $module_rule->task = $request->task;
            $module_rule->norm_contacts = $request->norm;
            $module_rule->number_contacts = $request->number;
            $module_rule->grouptasks_id = $grouptasks_id->id;

            if ($module_rule->save()) {
                return redirect()->back()->with('success', 'Модуль успешно удалена');
            } else {
                return redirect()->back()->with('error', 'Произошла ошибка при удалении модуля');
            }
        }
    }

    public function updateModulleRule(Request $request) {
        if ($request->ajax()) {
            $grouptasks_id = Grouptasks_reference::where('name', '=', $request->group_tasks)
                ->firstOrFail();
            $module_rule = Module_rule::find($request->rule_id);
            $module_rule->module_id = $request->module_id;
            $module_rule->task = $request->task;
            $module_rule->norm_contacts = $request->norm;
            $module_rule->number_contacts = $request->number;
            $module_rule->grouptasks_id = $grouptasks_id->id;

            if ($module_rule->save()) {
                return redirect()->back()->with('success', 'Модуль успешно удалена');
            } else {
                return redirect()->back()->with('error', 'Произошла ошибка при удалении модуля');
            }
        }
    }

    public function delModuleQuestion($mid, $qid) {
        $questions = Clarifying_question::where('task_id', '=', $mid)
            ->whereNull('deleted_at')
            ->get();

        $cur_q = Clarifying_question::find($questions[$qid]->id);
        $cur_q->deleted_at = date("Y-m-d H:i:s");

        if ($cur_q->save()) {
            return redirect()->back()->with('success', 'Уточняющий вопрос успешно удален');
        } else {
            return redirect()->back()->with('error', 'Произошла ошибка при удалении уточняющего вопроса');
        }
    }

    public function updateModuleQuestion(Request $request) {
        if ($request->ajax()) {
            $questions = Clarifying_question::where('task_id', '=', $request->qid)
                ->whereNull('deleted_at')
                ->get();

            $cur_q = Clarifying_question::find($questions[$request->loop]->id);
            $cur_q->question = $request->new_q;

            if ($cur_q->save()) {
                return response()->json('success');
            } else {
                return response()->json('error');
            }
        }
    }

    public function addModuleQuestion(Request $request) {
        if ($request->ajax()) {
            $questions = new Clarifying_question();
            $questions->task_id = $request->qid;
            $questions->question = $request->new_q;
            $questions->deleted_at = NULL;

            if ($questions->save()) {
                return response()->json('success');
            } else {
                return response()->json('error');
            }
        }
    }

    static public function getModulesByProjectId($pid) {
        $modules = Project_module::where('project_id', '=', $pid)
            ->whereNull('deleted_at')
            ->get();

        //dd($modules);
        foreach ($modules as $m) {
            $module = Modules_reference::where('id', '=', $m->module_id)
                ->whereNull('deleted_at')
                ->get();

            $module_ar[] = [
                'id' => $module[0]->id,
                'name' => $module[0]->name,
            ];
        }


        return $module_ar;
    }
}
