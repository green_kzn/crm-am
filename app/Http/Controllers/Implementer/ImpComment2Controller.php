<?php

namespace App\Http\Controllers\Implementer;

use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Comment;
use App\Callback_work_question;
use App\Callback_work_answer;
use App\Printform_history;
use App\Printform_comment;
use App\Printform_status_reference;
use App\User;
use Sentinel;
use DB;
use Carbon\Carbon;

class ImpComment2Controller extends Controller
{
    public function saveEditComment(Request $request) {
        $data = json_decode($request->data);

        $comment = Comment::find($data->comment->id);
        $comment->text = $data->comment->text;

        if ($comment->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function getAllCommentsByProjectId($id) {
        $count = DB::select('select count(`id`) as `count`
            from `comments`
            where `deleted_at` is null and
            `project_id` = '.$id);

        // dd($count[0]->count);
        if ($count[0]->count > 0) {
            $comments_q = DB::select("select `c`.`id`, DATE_FORMAT(`c`.`date`, '%d.%m.%Y') as 'date', `c`.`time`, `c`.`text`, 
                `u`.`first_name`, `u`.`last_name`, `mr`.`task`, `c`.`for_client`, `u`.`foto`, `u`.`id` as 'user_id'
                from `comments` `c`
                left join `project_tasks` `pt` on `pt`.`id` = `c`.`task_id`
                left join `module_rules` `mr` on `pt`.`module_rule_id` = `mr`.`id`
                left join `users` `u` on `u`.`id` = `c`.`user_id`
                where
                `c`.`deleted_at` is null and
                `c`.`project_id` = ".$id."
                order by `c`.`date` desc");

            foreach ($comments_q as $c) {
                // $user = User::find($c->user_id);
                $cur_user = Sentinel::getUser();
                $comments[] = [
                    'text' =>  htmlspecialchars_decode($c->text),
                    'date' => $c->date,
                    'time' => $c->time,
                    'id' => $c->id,
                    'for_client' => $c->for_client != NULL?1:0,
                    'task' => $c->task,
                    'user_name' => $c->first_name,
                    'user_surname' => $c->last_name,
                    'user_foto' => $c->foto,
                    'autor' => $c->user_id == $cur_user->id
                ];
            }
        } else {
            $comments = [];
        }

        // dd($comments);

        $p_comments = DB::select("select `ppf`.`id`, `ppf`.`project_task_id`, `pc`.`comment`, `u`.`first_name`,
            `u`.`last_name`, `u`.`foto`, `pc`.`created_at`
            from `project_print_forms` `ppf`
            left join `printform_comments` `pc` on `pc`.`form_id` = `ppf`.`id`
            left join `users` `u` on `u`.`id` = `pc`.`user_id`
            where
            `ppf`.`deleted_at` is null and
            `pc`.`deleted_at` is null and
            `ppf`.`project_id` =".$id." and `pc`.`id` is not null
            order by `pc`.`created_at` desc");

        foreach($p_comments as $pc) {
            $date = explode(' ', $pc->created_at);
            // dd($pc);
            if ($c->user_id == $cur_user->id) {
                $comments[] = [
                    'text' =>  htmlspecialchars_decode($pc->comment),
                    'date' => Carbon::parse($date[0])->format('d.m.Y'),
                    'time' => Carbon::parse($date[1])->format('H:i'),
                    'id' => $pc->id,
                    'pf' => 1,
                    'task' => '',
                    'user_name' => $pc->first_name,
                    'user_surname' => $pc->last_name,
                    'user_foto' => $pc->foto,
                    'autor' => true
                ];
            } else {
                $comments[] = [
                    'text' =>  htmlspecialchars_decode($pc->comment),
                    'date' => Carbon::parse($date[0])->format('d.m.Y'),
                    'time' => Carbon::parse($date[1])->format('H:i'),
                    'id' => $pc->id,
                    'pf' => 1,
                    'task' => '',
                    'user_name' => $pc->first_name,
                    'user_surname' => $pc->last_name,
                    'user_foto' => $pc->foto,
                    'autor' => false
                ];
            }
        };
        usort($comments, function ($i1, $i2){
            return Carbon::parse($i1['date'].' '.$i1['time']) < Carbon::parse($i2['date'].' '.$i2['time']);
        });
        return $comments;
    }

    public static function getCommentsByProjectId($id) {
        $count = DB::select('select count(`id`) as `count`
            from `comments`
            where `deleted_at` is null and
            `project_id` = '.$id);

        // dd($count[0]->count);
        if ($count[0]->count > 0) {
            $comments_q = DB::select("select `c`.`id`, DATE_FORMAT(`c`.`date`, '%d.%m.%Y') as 'date', `c`.`time`, `c`.`text`, 
                `u`.`first_name`, `u`.`last_name`, `mr`.`task`, `c`.`for_client`, `u`.`foto`, `u`.`id` as 'user_id'
                from `comments` `c`
                left join `project_tasks` `pt` on `pt`.`id` = `c`.`task_id`
                left join `module_rules` `mr` on `pt`.`module_rule_id` = `mr`.`id`
                left join `users` `u` on `u`.`id` = `c`.`user_id`
                where
                `c`.`deleted_at` is null and
                `c`.`project_id` = ".$id."
                order by `c`.`id` desc
                limit 3");

            foreach ($comments_q as $c) {
                // $user = User::find($c->user_id);
                $cur_user = Sentinel::getUser();
                $comments[] = [
                    'text' =>  htmlspecialchars_decode($c->text),
                    'date' => $c->date,
                    'time' => $c->time,
                    'id' => $c->id,
                    'for_client' => $c->for_client != NULL? 1:0,
                    'task' => $c->task,
                    'user_name' => $c->first_name,
                    'user_surname' => $c->last_name,
                    'user_foto' => $c->foto,
                    'autor' => $c->user_id == $cur_user->id
                ];
            }
        } else {
            $comments = [];
        };

        // dd($comments);

        $p_comments = DB::select("select `ppf`.`id`, `ppf`.`project_task_id`, `pc`.`comment`, `u`.`first_name`,
            `u`.`last_name`, `u`.`foto`, `pc`.`created_at`
            from `project_print_forms` `ppf`
            left join `printform_comments` `pc` on `pc`.`form_id` = `ppf`.`id`
            left join `users` `u` on `u`.`id` = `pc`.`user_id`
            where
            `ppf`.`deleted_at` is null and
            `pc`.`deleted_at` is null and
            `ppf`.`project_id` =".$id." and `pc`.`id` is not null
            order by `pc`.`created_at` desc
            limit 3");

        foreach($p_comments as $pc) {
            $date = explode(' ', $pc->created_at);

            $comments[] = [
                'text' =>  htmlspecialchars_decode($pc->comment),
                'date' => Carbon::parse($date[0])->format('d.m.Y'),
                'time' => Carbon::parse($date[1])->format('H:i:s'),
                'id' => $pc->id,
                'pf' => 1,
                'task' => '',
                'user_name' => $pc->first_name,
                'user_surname' => $pc->last_name,
                'user_foto' => $pc->foto,
                'autor' => $c->user_id == $cur_user->id
            ];
        };

        usort($comments, function ($i1, $i2){
            return Carbon::parse($i1['date'].' '.$i1['time']) < Carbon::parse($i2['date'].' '.$i2['time']);
        });

        return array_slice($comments, 0, 3);
    }

    public function getControlComments(Request $request) {
        $data = json_decode($request->data);

        $resp = DB::select("select `question`.`id`, `question`.`question`, `answer`.`answer`, 
            DATE_FORMAT(`answer`.`created_at`,'%d %m %Y') as `date`
            from `callback_work_questions` `question`
            left join `callback_work_answers` `answer` on `answer`.`question_id` = `question`.`id`
            where
            `answer`.`project_id` =".$data->id);

        return response()->json($resp);
    }

    public function getCloseComments(Request $request) {
        $data = json_decode($request->data);

        $resp = DB::select("select `question`.`id`, `question`.`question`, `answer`.`answer`, 
            DATE_FORMAT(`answer`.`created_at`,'%d %m %Y') as `date`
            from `callback_close_questions` `question`
            left join `callback_close_answers` `answer` on `answer`.`question_id` = `question`.`id`
            where
            `answer`.`project_id` =".$data->id);

        return response()->json($resp);
    }

    public function add(Request $request) {
        if ($request->ajax()) {
            $cur_user = Sentinel::getUser();
            $comment = new Comment();
            $comment->project_id = $request->project_id;
            $comment->user_id = $cur_user->id;
            $comment->text = $request->text;
            $comment->date = date("Y-m-d");
            $comment->time = date("H:i:s");

            if ($comment->save()) {
                return redirect()->back()->with('success', 'Коментарий успешно добавлен');
            } else {
                return redirect()->back()->with('error', 'Произошла ошибка при добавлении коментария');
            }
        }
    }

    public function add2(Request $request) {
        if ($request->ajax()) {
            $cur_user = Sentinel::getUser();
            try {
                $comment = new Printform_comment();
                $comment->user_id = $cur_user->id;
                $comment->comment = $request->text;
                $comment->form_id = $request->form_id;
                $comment->save();

                $success = true;
            } catch (\Exception $e) {
                $success = false;
            }

            try {
                $p_history = new Printform_history();
                $p_history->printform_id = $request->form_id;
                $p_history->user_id = $cur_user->id;
                $p_history->printform_status_ref_id = $request->status;
                $p_history->comment_id = $comment->id;
                $p_history->save();

                $success = true;
            } catch (\Exception $e) {
                $success = false;
            }

            if ($success) {
                return redirect()->back()->with('success', 'Коментарий успешно добавлен');
            } else {
                return redirect()->back()->with('error', 'Произошла ошибка при добавлении коментария');
            }
        }
    }

    public function del($id) {
        $comment = Comment::find($id);
        $comment->deleted_at = date("Y-m-d H:i:s");

        if ($comment->save()) {
            // $com = $this::getCommentsByProjectId($comment->project_id);

            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' =>  'Error']);
        }
    }

    static public function getProjectCloseComment($pid) {
        $comment = Comment::where('project_id', '=', $pid)
            ->where('close', '=', 1)
            ->whereNull('deleted_at')
            ->get();

        return $comment;
    }

    // public function saveEditComment(Request $request) {
    //     $data = json_decode($request->data);
    //     $coment = Comment::find($data->comment->id);
    //     $date = $coment->created_at;
    //     $coment->text = $data->comment->text;
    //     $coment->created_at = $date;
    //     if ($coment->save()) {
    //         $comments_q = Comment::where('project_id', '=', $data->project_id)
    //             ->orderBy('date', 'desc')
    //             ->orderBy('time', 'desc')
    //             ->whereNULL('deleted_at')
    //             ->get();

    //         if (count($comments_q) > 0) {
    //             foreach ($comments_q as $c) {
    //                 $user = User::find($c->user_id);
    //                 $cur_user = Sentinel::getUser();

    //                 if ($c->for_client != NULL) {
    //                     if ($user->id == $cur_user->id) {
    //                         $comments[] = [
    //                             'text' => htmlspecialchars_decode($c->text),
    //                             'date' => $c->date,
    //                             'time' => $c->time,
    //                             'id' => $c->id,
    //                             'for_client' => 1,
    //                             'user_name' => $user->first_name,
    //                             'user_surname' => $user->last_name,
    //                             'user_foto' => $user->foto,
    //                             'autor' => true
    //                         ];
    //                     } else {
    //                         $comments[] = [
    //                             'text' => htmlspecialchars_decode($c->text),
    //                             'date' => $c->date,
    //                             'time' => $c->time,
    //                             'id' => $c->id,
    //                             'for_client' => 1,
    //                             'user_name' => $user->first_name,
    //                             'user_surname' => $user->last_name,
    //                             'user_foto' => $user->foto,
    //                             'autor' => false
    //                         ];
    //                     }
    //                 } else {
    //                     if ($user['id'] == $cur_user->id) {
    //                         $comments[] = [
    //                             'text' => htmlspecialchars_decode($c->text),
    //                             'date' => $c->date,
    //                             'time' => $c->time,
    //                             'id' => $c->id,
    //                             'for_client' => 0,
    //                             'user_name' => $user->first_name,
    //                             'user_surname' => $user->last_name,
    //                             'user_foto' => $user->foto,
    //                             'autor' => true
    //                         ];
    //                     } else {
    //                         $comments[] = [
    //                             'text' => htmlspecialchars_decode($c->text),
    //                             'date' => $c->date,
    //                             'time' => $c->time,
    //                             'id' => $c->id,
    //                             'for_client' => 0,
    //                             'user_name' => $user['first_name'],
    //                             'user_surname' => $user['last_name'],
    //                             'user_foto' => $user['foto'],
    //                             'autor' => false
    //                         ];
    //                     }
    //                 }
    //             }
    //         } else {
    //             $comments = '';
    //         }

    //         return response()->json($comments);
    //     } else {
    //         return response()->json(['status' => 'error']);
    //     };
    // }
}
