<?php

namespace App\Http\Controllers\Implementer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Check_list_reference;

class ImpCheckListController extends Controller
{
    public function index() {
        $questions_q = Check_list_reference::whereNull('deleted_at')
            ->get();
        if (count($questions_q) > 0) {
            foreach ($questions_q as $q) {
                $questions[] = [
                    'id' => $q->id,
                    'question' => strip_tags($q->question)
                ];
            }
        } else {
            $questions[] = array();
        }
        //dd($questions);

        return view('Implementer.references.check_list.check_list', [
            'questions' => $questions
        ]);
    }

    public function add() {

        return view('Implementer.references.check_list.add');
    }

    public function postAdd(Request $request) {
        if ($request->question == '') {
            return redirect()->back()->with(['error' => 'Поле вопрос не заполнено']);
        }

        try {
            $check_list = new Check_list_reference();
            $check_list->question = $request->question;
            $check_list->save();

            $success = true;
        } catch (\Exception $e) {
            $success = false;
        }

        if ($success) {
            return redirect('/implementer/check-list')->with('success', 'Данные успешно обновлены');
        } else {
            return redirect('/implementer/check-list')->with('error', 'Произошла ошибка при обновлении данных');
        }
    }

    public function del($id) {
        try {
            $question = Check_list_reference::find($id);
            $question->deleted_at = date("Y-m-d H:i:s");
            $question->save();

            $success = true;
        } catch (\Exception $e) {
            $success = false;
        }

        if ($success) {
            return redirect('/implementer/check-list')->with('success', 'Данные успешно обновлены');
        } else {
            return redirect('/implementer/check-list')->with('error', 'Произошла ошибка при обновлении данных');
        }
    }

    public function edit($id) {
        $question = Check_list_reference::where('id', '=', $id)
            ->whereNull('deleted_at')
            ->first();

        return view('Implementer.references.check_list.edit', [
            'question' => $question
        ]);
    }

    public function postEdit(Request $request, $id) {
        if ($request->question == '') {
            return redirect()->back()->with(['error' => 'Поле вопрос не заполнено']);
        }

        try {
            $check_list = Check_list_reference::find($id);
            $check_list->question = $request->question;
            $check_list->save();

            $success = true;
        } catch (\Exception $e) {
            $success = false;
        }

        if ($success) {
            return redirect('/implementer/check-list')->with('success', 'Данные успешно обновлены');
        } else {
            return redirect('/implementer/check-list')->with('error', 'Произошла ошибка при обновлении данных');
        }
    }
}
