<?php

namespace App\Http\Controllers\Implementer;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use App\Http\Controllers\PermissionsController as Permissions;
use App\Http\Controllers\Implementer\ImpProjectsController as Projects;
use App\Http\Controllers\Controller;
use Sentinel;
use App\Project_task;
use App\Project;
use App\Clients_reference;
use App\Comment;
use App\Printform_comment;
use App\Module_rule;
use App\Project_task_contact;
use App\Project_additional_task;
use App\Project_print_form;
use App\Printform_file;
use Storage;
use Carbon\Carbon;

class ImpCalendar2Controller extends Controller
{
    public function index($id) {
        $perm = session('perm');

        if ($perm['calendar.view']) {
            $cur_user = Sentinel::getUser();
            $project = Project::where('user_id', '=', $id)
                ->whereNull('deleted_at')
                ->get();

            if (count($project) > 0) {
                foreach ($project as $p) {
                    $clients[] = Clients_reference::find($p->client_id);

                    $project_task = Project_task::where('project_id', '=', $p->id)
                        ->whereNull('deleted_at')
                        ->where('tasksstatus_ref_id', '!=', 2)
                        ->get();
                    if (count($project_task) > 0) {
                        foreach ($project_task as $pt) {
                            $module_rule = Module_rule::find($pt['module_rule_id']);
                            $task[] = [
                                'id' => $pt['id'],
                                'name' => $module_rule['task']
                            ];
                        }
                    } else {
                        $task = '';
                    }
                }
            } else {
                $project = '';
                $task = '';
                $clients = '';
            }

            /*$event = Event::where('user_id', '=', $cur_user->id)
                ->whereNULL('deleted_at')
                ->get();*/
            //dd($task);
            return view('Implementer.calendars.calendar', [
                //'events' => $event,
                'perm' => $perm,
                'project' => $project,
                'task' => $task,
                'clients' => $clients
            ]);
        } else {
            abort(503);
        }
    }

    public function addEvent(Request $request) {
        if (!$request->nameEvent) {
            return redirect()->back()->with('error', 'Не указано название события');
        }
        if (!$request->color) {
            return redirect()->back()->with('error', 'Не выбран цвет');
        }

        $cur_user = Sentinel::getUser();

        $event = new Event();
        $event->name = $request->nameEvent;
        $event->color = $request->color;
        $event->user_id = $cur_user->id;

        if ($event->save()) {
            return redirect()->back();
        } else {
            return redirect()->back()->with('error', 'Событие не сохранено');
        }
    }

    public function delEvent(Request $request, $id) {
        $event = Event::find($id);
        $event->deleted_at = date("Y-m-d H:i:s");
        if ($event->save()) {
            return redirect()->back();
        } else {
            return redirect()->back()->with('error', 'Ошибка при попытке удаления события');
        }
    }

    public function addProjectTask(Request $request) {
        $data = json_decode($request->data);
        $endTime = strtotime($data->time) + strtotime('00:59');
        $cur_user = Sentinel::getUser();
        switch ($data->type) {
            case 'task':
                $p_task = Project_task::find($data->task_id);

                $m_rule = Module_rule::find($p_task->module_rule_id);
                $pid = Projects::getProjectByTaskId($data->task_id);

                $res = strtotime(date('H:i:s',$endTime)) -strtotime("00:00:00");
                $time = date('H:i:s',$res);
                //dd(date('H:i:s',strtotime(date('H:i:s',$endTime)) -strtotime("00:00:00")));

                $project_task_contact = new Project_task_contact();
                $project_task_contact->next_contact = $data->date.' '.$data->time;
                $project_task_contact->user_id = $data->user_id;
                $project_task_contact->observer_id = $cur_user->id;
                $project_task_contact->date_production = Carbon::now()->format('Y-m-d H:i:s');
                $project_task_contact->project_id = $pid->id;
                $project_task_contact->project_task_id = $data->task_id;
                $project_task_contact->end = $data->date.' '. $time;

                if ($project_task_contact->save()) {
                    return response()->json(['status' => 'Ok']);
                } else {
                    return response()->json(['status' => 'Error']);
                }
                break;
            case 'print_form':
                // $cur_user = Sentinel::getUser();

                try {
                    $p_form = new Project_print_form();
                    $p_form->name = $request->formName;
                    $p_form->project_id = $request->pid;
                    $p_form->raw = 1;
                    $p_form->observer_id = $cur_user->id;
                    $p_form->is_printform = 1;
                    $p_form->printform_status_ref_id = 6;
                    $p_form->save();

                    $success = true;
                } catch (\Exception $e) {
                    $success = false;
                }

                $comment2 = 'comment_form_'.$request->tid;
                try {
                    $comment = new Printform_comment();
                    $comment->user_id = $cur_user->id;
                    $comment->comment = $request->$comment2;
                    $comment->form_id = $p_form->id;
                    $comment->save();

                    $success = true;
                } catch (\Exception $e) {
                    $success = false;
                }

                if ($request->hasFile('img')) {
                    $file = $request->file('img');
                    foreach ($file as $f) {
                        if ($request->hasFile('img')) {
                            //$f->move($destinationPath, $filename);
                            $content = file_get_contents($f);
                            $t = Storage::disk('local')->put($f->getClientOriginalName(), $content);

                            $pf_file = new Printform_file();
                            $pf_file->file = $f->getClientOriginalName();
                            $pf_file->project_print_form_id = $p_form->id;
                            $pf_file->save();
                        }
                    }
                }

                if ($success) {
                    return redirect()->back()->with('success', 'Данные успешно обновлены');
                }else {
                    return redirect()->back()->with('error', 'Произошла ошибка при обновлении данных');
                }

                break;
            case 'screen_form':
                $cur_user = Sentinel::getUser();

                try {
                    $p_form = new Project_print_form();
                    $p_form->name = $request->formName;
                    $p_form->project_id = $request->pid;
                    $p_form->raw = 1;
                    $p_form->observer_id = $cur_user->id;
                    $p_form->is_screenform = 1;
                    $p_form->printform_status_ref_id = 6;
                    $p_form->save();

                    $success = true;
                } catch (\Exception $e) {
                    $success = false;
                }

                $comment3 = 'screen_comment_form_'.$request->tid;
                try {
                    $comment = new Printform_comment();
                    $comment->user_id = $cur_user->id;
                    $comment->comment = $request->$comment3;
                    $comment->form_id = $p_form->id;
                    $comment->save();

                    $success = true;
                } catch (\Exception $e) {
                    $success = false;
                }

                if ($request->hasFile('img2')) {
                    $file = $request->file('img2');
                    foreach ($file as $f) {
                        if ($request->hasFile('img2')) {
                            //$f->move($destinationPath, $filename);
                            $content = file_get_contents($f);
                            $t = Storage::disk('local')->put($f->getClientOriginalName(), $content);

                            $pf_file = new Printform_file();
                            $pf_file->file = $f->getClientOriginalName();
                            $pf_file->project_print_form_id = $p_form->id;
                            $pf_file->save();
                        }
                    }
                }

                if ($success) {
                    return redirect()->back()->with('success', 'Данные успешно обновлены');
                }else {
                    return redirect()->back()->with('error', 'Произошла ошибка при обновлении данных');
                }
                break;
        }
    }

    public function addProjectAdditionalTask(Request $request) {
        if ($request->ajax()) {
            $endTime = strtotime($request->time) + strtotime('01:00');
            $count = Project_additional_task::where('end', '>', $request->date.' '.$request->time)
                ->where('tasksstatus_ref_id', '!=', 2)
                ->whereNull('deleted_at')
                ->count();

            if ($count == 0) {
                $project_task = Project_additional_task::find($request->tid);
                $project_task->next_contact = $request->date.' '.$request->time;
                $project_task->end = $request->date.' '.date('H:i:s',$endTime);

                if ($project_task->save()) {
                    return response()->json(['status' => 'Ok']);
                } else {
                    return redirect()->back()->with('error', 'Ошибка при попытке удаления события');
                }
            } else {
                return response()->json(['status' => 'error', 'error' => 'Ошибка! На данную дату/время уже запланирована задача']);
            }
        }
    }

    public function addTask(Request $request) {
        dd('Ok');
    }

    public function addNewEvent($date) {
        return view('Implementer.calendars.addEvent');
    }
}
