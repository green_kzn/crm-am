<?php

namespace App\Http\Controllers\Implementer;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Contact;
use App\Project_contact;
use App\Project;
use App\Posts_reference;
use Sentinel;

class ImpContact2Controller extends Controller
{
    public function add(Request $request) {
        DB::transaction(function () use ($request) {
            $cur_user = Sentinel::getUser();
            $post_id = Posts_reference::where('name', '=', $request->contact_post)
                ->whereNull('deleted_at')
                ->first();
            $project_id = Project::find($request->p_id);

            try {
                $contact = new Contact();
                $contact->first_name = $request->contact_name;
                $contact->last_name = $request->contact_surname;
                $contact->patronymic = $request->contact_patronymic;
                $contact->phone = $request->contact_phone;
                $contact->email = $request->contact_email;
                $contact->post_id = $post_id->id;
                $contact->user_id = $cur_user->id;
                $contact->client_id = $project_id->client_id;
                $contact->save();

                $this->success = true;
            } catch (\Exception $e) {
                DB::rollback();
                $this->success = false;
            }

            try {
                $p_contact = new Project_contact();
                $p_contact->contact_id = $contact->id;
                $p_contact->project_id = $request->p_id;
                $p_contact->main = 0;
                $p_contact->mail_rep = 0;
                $p_contact->save();

                $this->success = true;
            } catch (\Exception $e) {
                DB::rollback();
                $this->success = false;
            }
        });

        if ($this->success) {
            return redirect()->back()->with('success', 'Данные успешно обновлены');
        } else {
            return redirect()-back()->with('error', 'Произошла ошибка при обновлении данных');
        }
    }

    public function delCont(Request $request) {
        $data = json_decode($request->data);
        
        $p_cont = Project_contact::where('contact_id', '=', $data->client_id)
            ->where('project_id', '=', $data->p_id)
            ->first();

        $p_cont->deleted_at = date("Y-m-d H:i:s");

        if ($p_cont->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'error']);
        }
    }

    public function del($id) {
        try {
            $p_contact = Project_contact::where('contact_id', '=', $id)
                ->whereNull('deleted_at')
                ->first();
            $p_contact->deleted_at = date("Y-m-d H:i:s");
            $p_contact->save();

            $this->success = true;
        } catch (\Exception $e) {
            $this->success = false;
        }


        if ($this->success) {
            return redirect()->back()->with('success', 'Контакт успешно удален');
        } else {
            return redirect()->back()->with('error', 'Произошла ошибка при удалении контакта');
        }
    }

    static public function getProjectMainContact($pid) {
        $contact_id = Project_contact::where('project_id', '=', $pid)
            ->where('main', '=', 1)
            ->whereNull('deleted_at')
            ->first();

        if ($contact_id) {
            $contact = Contact::find($contact_id->contact_id);
            // dd($contact);
            if (count((array)$contact) > 0) {
                $res = (object)[
                    "id" => $contact['id'],
                    "first_name" => $contact['first_name'],
                    "last_name" => $contact['last_name'],
                    "patronymic" => $contact['patronymic'],
                    "post_id" => $contact['post_id'],
                    "phone" => $contact['phone'],
                    "email" => $contact['email'],
                    "project_id" => $contact['project_id'],
                    "mail_rep" => $contact['mail_rep'],
                    "user_id" => $contact['user_id'],
                    "created_at" => $contact['created_at'],
                    "updated_at" => $contact['updated_at'],
                    "deleted_at" => $contact['deleted_at']
                ];
            } else {
                $res = '';
            }
        } else {
            $res = false;
        }

        return $res;
    }

    static public function getProjectContacts($pid) {
        $contacts = Project_contact::where('project_id', '=', $pid)
            ->whereNull('deleted_at')
            ->get();

        foreach ($contacts as $c) {
            $contact = Contact::find($c->contact_id);
            $p_contact[] = [
                "id" => $contact['id'],
                "first_name" => $contact['first_name'],
                "last_name" => $contact['last_name'],
                "patronymic" => $contact['patronymic'],
                "post_id" => $contact['post_id'],
                "phone" => $contact['phone'],
                "email" => $contact['email'],
                "project_id" => $contact['project_id'],
                "mail_rep" => $contact['mail_rep'],
                "user_id" => $contact['user_id'],
                "created_at" => $contact['created_at'],
                "updated_at" => $contact['updated_at'],
                "deleted_at" => $contact['deleted_at']
            ];
        }

        return $p_contact;
    }

    public function update(Request $request, $id) {
        $contact = Contact::find($id);

        return view('Implementer.contacts.edit', [
            'contact' => $contact
        ]);
    }

    static public function getClientAllContacts($id) {
        $contacts = Contact::where('client_id', '=', $id)
            ->whereNull('deleted_at')
            ->get();

        return $contacts;
    }

    public function getClientsContact(Request $request) {
        if ($request->ajax()) {
            $contacts_q = Contact::where('client_id', '=', $request->id)
                ->whereNull('deleted_at')
                ->get();
            foreach ($contacts_q as $c) {
                $post = Posts_reference::find($c->post_id);
                $contacts[] = [
                    'id' => $c->id,
                    'first_name' => $c->first_name,
                    'last_name' => $c->last_name,
                    'patronymic' => $c->patronymic,
                    'phone' => $c->phone,
                    'email' => $c->email,
                    'main' => $c->main,
                    'mail_rep' => $c->mail_rep,
                    'post' => $post->name,
                ];
            }
            return response()->json($contacts);
        }
    }

    public function saveContact(Request $request) {
        if ($request->ajax()) {
            $contact = Contact::find($request->id);
            $contact->first_name = $request->name;
            $contact->last_name = $request->surname;
            $contact->patronymic = $request->patronymic;
            $contact->post_id = $request->post;
            $contact->phone = $request->phone;
            $contact->email = $request->email;
            $contact->email = $request->email;

            if($contact->save()) {
                return response()->json(['success' => 'Ok']);
            } else {
                return response()->json(['success' => 'Error']);
            }
        }
    }

    static public function getContactByEmail($email) {
        $person = Contact::where('email', '=', $email)
            ->whereNull('deleted_at')
            ->first();

        return $person;
    }

    public function addContact(Request $request) {
        $data = json_decode($request->data);
        $cur_user = Sentinel::getUser();

        $contact = new Contact();
        $contact->first_name = $data->newContact->firstName;
        $contact->last_name = $data->newContact->lastName;
        $contact->patronymic = $data->newContact->patronymic;
        $contact->post_id = $data->newContact->post_id;
        $contact->phone = $data->newContact->phone;
        $contact->email = $data->newContact->email;
        $contact->client_id = $data->client_id;
        $contact->user_id = $cur_user->id;
        $contact->save();

        $p_contact = new Project_contact();
        $p_contact->contact_id = $contact->id;
        $p_contact->project_id = $data->project_id;
        $p_contact->main = $data->newContact->main;
        $p_contact->mail_rep = $data->newContact->mail_rep;

        if ($p_contact->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }
}
