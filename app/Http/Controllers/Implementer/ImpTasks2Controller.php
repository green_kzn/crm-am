<?php

namespace App\Http\Controllers\Implementer;

use App\Client;
use App\ProjectWaitingList;
use Illuminate\Http\Request;
use App\Http\Controllers\Implementer\ImpEventController as EventController;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Implementer\ImpGroupTasksController as GroupTasks;
use App\Http\Controllers\Implementer\ImpModules2Controller as Modules;
use App\Http\Controllers\Implementer\ImpContact2Controller as Contact;
use App\Http\Controllers\Implementer\ImpProjectsController as Projects;
use App\Http\Controllers\Implementer\ImpUser2Controller as Users;
use App\Http\Controllers\Implementer\ImpClientsController as Clients;
use App\Http\Controllers\Controller;
use Monolog\Handler\IFTTTHandler;
use Sentinel;
use App\Task;
use App\User;
use App\Project_task;
use App\Project_contacts;
use App\Project_task_contact;
use App\Project_print_form;
use App\Printform_status_reference;
use App\Project;
use App\Project_module;
use App\Module_rule;
use App\Comment;
use App\Comment_task;
use App\Tasksstatus_reference;
use App\Clients_reference;
use App\Clarifying_question;
use App\Clarifying_answer;
use App\Event;
use Carbon\Carbon;
use App\Mail\SendComment;
use Mail;

class ImpTasks2Controller extends Controller 
{
    public function index() {
        return view('Implementer.tasks.tasks', array(
            'tasks' => array()
        ));
    }

    public function getTasksList() {
        $cur_user = Sentinel::getUser();
        $res = array();

        $tasks = Task::where('contractor_id', '=', $cur_user->id)
            ->whereNULL('deleted_at')
            ->get();

        $p_tasks = Project_task::where('user_id', '=', $cur_user->id)
            //->whereNull('status')
            ->whereNull('deleted_at')
            ->get();

        if (count($p_tasks) > 0) {
            foreach ($p_tasks as $pt) {
                $name = Module_rule::find($pt->module_rule_id);
                $user = User::find($pt->observer_id);
                $res[] = array(
                    'name' => $name->task,
                    'created' => $pt->created_at,
                    'observer' => $user->last_name.' '.$user->first_name
                );
            }
        }


        if (count($tasks) > 0) {
            foreach ($tasks as $t) {
                $user = User::find($t->observer_id);
                $res[] = array(
                    'name' => $t->title,
                    'created' => $t->created_at,
                    'observer' => $user->last_name.' '.$user->first_name,
                    'start_date' => $t->start_date,
                    'start_time' => $t->start_time,
                    'end_date' => $t->end_date,
                    'end_time' => $t->end_time,
                );
            }
        }

        return response()->json($res);
    }

    public function addTask() {
        if (session('perm')['task.create']) {

            return view('Implementer.tasks.add', [
                'users' => Users::getAllUsers(),
                'clients' => Clients::getAllClients(),
                'projects' => Projects::getAllProjects(),
            ]);
        } else {
            abort(503);
        }
    }

    public function time_add_min($time, $min) {
        list($h, $m) = explode(':', $time);
        $h = ($h + floor($m / 60)) % 24;
        $m = ($m + $min) % 60;
        return str_pad($h, 2, "0", STR_PAD_LEFT).':'.str_pad($m, 2, '0');
    }

    public function getTasks($id, Request $request) {
        if ($request->ajax()) {
            $cur_user = Sentinel::getUser();

//            $project_task_contacts = Project_task_contact::where('user_id', '=', $id)
//                //->where('next_contact', '!=', NULL)
//                ->whereNull('deleted_at')
//                //->whereNull('status')
//                ->get();

            $project_task_contacts = DB::select("select `ptc`.*, `pt`.`id`, ptc.id as ptc_id, 
                `p`.`name`, `pt`.`tasksstatus_ref_id`, 
                `mr`.`task`, `u`.`first_name`, `u`.`last_name`, `ptc`.`date_production`,
                `ct`.`text` as comment, `p`.`id` as project_id, `ptc`.`contact_id`
                from `project_task_contacts` ptc
                left join `projects` `p` on `p`.`id` = `ptc`.`project_id`
                left join `project_tasks` `pt` on `pt`.`id` = `ptc`.`project_task_id`
                left join `module_rules` `mr` on `mr`.`id` = `pt`.`module_rule_id`
                left join `users` `u` on `u`.`id` = `ptc`.`observer_id`
                left join `comment_tasks` `ct` on `ct`.`task_id` = `pt`.`id`
                where 
                `p`.`done` = 0 and 
                ptc.next_contact > ? and ptc.next_contact < ? and 
                `ptc`.`deleted_at` is null and
                `ptc`.`user_id` =  ? ", [$request->start, $request->end, $id]);

            // print_r($project_task_contacts);

            if (count($project_task_contacts) > 0) {
                foreach ($project_task_contacts as $ptc) {
                    $p_tasks[] = [
                        'id' => $ptc->id,
                        'ptc_id' => $ptc->ptc_id,
                        'contact_id' => $ptc->contact_id,
                        'project_id' => $ptc->project_id,
                        'name' => $ptc->task. "\r\n".$ptc->name,
                        'task' => $ptc->task,
                        'p_name' => $ptc->name,
//                            'city' => $p_name['city'],
                        'duration' => $ptc->duration,
                        'tasksstatus_ref_id' => $ptc->tasksstatus_ref_id,
                        'callbackstatus_ref_id' => $ptc->callbackstatus_ref_id,
                        'next_contact' => $ptc->next_contact ? $ptc->next_contact : $ptc->prev_contact,
                        'prev_contact' => $ptc->prev_contact,
                        'end' => $ptc->end,
                        'comment' => $ptc->comment,
                        'observer' => $ptc->first_name.' '.$ptc->last_name,
                        'date_production' => $ptc->date_production ? Carbon::parse($ptc->date_production)->format('d.m.Y H:i:s') : ''
                    ];
                }
            //    print_r($p_tasks);
                foreach ($p_tasks as $p_task) {
                    $date = explode(' ', $p_task['next_contact']);

                    $color = null;
                    if (!$p_task['callbackstatus_ref_id']) {
                        switch ($p_task['tasksstatus_ref_id']) {
                            case '1':
                                $color = '#00a65a';
                                break;
                            case '2':
                                $color = '#f39c12';
                                break;
                            case '3':
                                $color = '#00c0ef';
                                break;
                        }
                    } else {
                        $color = '#dd4b39';
                    }

                    if ($color) {
                        $comment = $p_task['comment'] ? "\nКомментарий: ".$p_task['comment'] : '';
                        $json[] = array(
                            'id' => $p_task['id'],
                            'ptc_id' => $p_task['ptc_id'],
                            'contact_id' => $p_task['contact_id'],
                            'project_id' => $p_task['project_id'],
                            'title' => $p_task['name'],
                            'start' => $p_task['next_contact'],
                            //'end' => $end[0] . ' ' . date('H:i:s', strtotime($end[1]) + strtotime($pt['duration']) - strtotime("00:00:00")),
                            'end' => $p_task['end'],
                            'allDay' => false,
                            'color' => $color,
                            'extendedProps' => 'Постановщик: ' . $p_task['observer'] . ', дата: ' . $p_task['date_production'].$comment,
                            'url' => $p_task['task'] ? '/implementer/project-task/edit/' . $p_task['id'] . '/' . $date[0] : null,
                            //'timeFormat' => 'H:mm'
                        );
                    }

//                    for($i = 1; $i <= date("t"); $i++) {
//                        $weekend = date("w",strtotime($i.'.01.'.date("Y")));
//                        if($weekend==0 || $weekend==6) {
//                            if ($i<10) {
//                                $json[] = [
//                                    'start' => date("Y-01-").'0'.$i,
//                                    'rendering' => 'background',
//                                    'backgroundColor' => '#F00',
//                                    'textColor' => '#000'
//                                ];
//                            } else {
//                                $json[] = array(
//                                    'start' => date("Y-01-").$i,
//                                    'rendering' => 'background',
//                                    'backgroundColor' => '#F00',
//                                    'textColor' => '#000'
//                                );
//                            }
//                        };
//                    };
//
//                    for($i = 1; $i <= date("t"); $i++) {
//                        $weekend = date("w",strtotime($i.'.02.'.date("Y")));
//                        if($weekend==0 || $weekend==6) {
//                            if ($i<10) {
//                                $json[] = [
//                                    'start' => date("Y-02-").'0'.$i,
//                                    'rendering' => 'background',
//                                    'backgroundColor' => '#F00',
//                                    'textColor' => '#000'
//                                ];
//                            } else {
//                                $json[] = array(
//                                    'start' => date("Y-02-").$i,
//                                    'rendering' => 'background',
//                                    'backgroundColor' => '#F00',
//                                    'textColor' => '#000'
//                                );
//                            }
//                        };
//                    };
//
//                    for($i = 1; $i <= date("t"); $i++) {
//                        $weekend = date("w",strtotime($i.'.03.'.date("Y")));
//                        if($weekend==0 || $weekend==6) {
//                            if ($i<10) {
//                                $json[] = [
//                                    'start' => date("Y-03-").'0'.$i,
//                                    'rendering' => 'background',
//                                    'backgroundColor' => '#F00',
//                                    'textColor' => '#000'
//                                ];
//                            } else {
//                                $json[] = array(
//                                    'start' => date("Y-03-").$i,
//                                    'rendering' => 'background',
//                                    'backgroundColor' => '#F00',
//                                    'textColor' => '#000'
//                                );
//                            }
//                        };
//                    };
                }
            } else {
                $json[] = '';

//                for($i = 1; $i <= date("t"); $i++) {
//                    $weekend = date("w",strtotime($i.'.01.'.date("Y")));
//                    if($weekend==0 || $weekend==6) {
//                        if ($i<10) {
//                            $json[] = [
//                                'start' => date("Y-01-").'0'.$i,
//                                'rendering' => 'background',
//                                'backgroundColor' => '#F00',
//                                'textColor' => '#000'
//                            ];
//                        } else {
//                            $json[] = array(
//                                'start' => date("Y-01-").$i,
//                                'rendering' => 'background',
//                                'backgroundColor' => '#F00',
//                                'textColor' => '#000'
//                            );
//                        }
//                    };
//                };
//
//                for($i = 1; $i <= date("t"); $i++) {
//                    $weekend = date("w",strtotime($i.'.02.'.date("Y")));
//                    if($weekend==0 || $weekend==6) {
//                        if ($i<10) {
//                            $json[] = [
//                                'start' => date("Y-02-").'0'.$i,
//                                'rendering' => 'background',
//                                'backgroundColor' => '#F00',
//                                'textColor' => '#000'
//                            ];
//                        } else {
//                            $json[] = array(
//                                'start' => date("Y-02-").$i,
//                                'rendering' => 'background',
//                                'backgroundColor' => '#F00',
//                                'textColor' => '#000'
//                            );
//                        }
//                    };
//                };
//
//                for($i = 1; $i <= date("t"); $i++) {
//                    $weekend = date("w",strtotime($i.'.03.'.date("Y")));
//                    if($weekend==0 || $weekend==6) {
//                        if ($i<10) {
//                            $json[] = [
//                                'start' => date("Y-03-").'0'.$i,
//                                'rendering' => 'background',
//                                'backgroundColor' => '#F00',
//                                'textColor' => '#000'
//                            ];
//                        } else {
//                            $json[] = array(
//                                'start' => date("Y-03-").$i,
//                                'rendering' => 'background',
//                                'backgroundColor' => '#F00',
//                                'textColor' => '#000'
//                            );
//                        }
//                    };
//                };
            }

            // $event = Event::whereNull('deleted_at')
            //     ->where('user_id', '=', $id)
            //     ->get();

            $event = DB::select('select `u`.`first_name` as `first_name`, `u`.`last_name` as `last_name`, `e`.* 
                from `events` `e`
                left join `users` `u` on `u`.`id` = `e`.`observer_id`
                where 
                `e`.`deleted_at` is null and
                `e`.`start` > ? and `e`.`start`< ? and
                `e`.`user_id` = ?', [$request->start, $request->end, $id]);

            // print_r($event);
            // die;

            foreach($event as $e) {
                // $user = Sentinel::getUser($e['observer_id']);
                
                $json[] = array(
                    'id' => $e->id,
                    'title' => $e->title,
                    'start' => $e->start,
//                            'end' => $end[0].' '.date('H:i:s',strtotime($end[1]) + strtotime($p_task['duration'])-strtotime("00:00:00")),
                    'end' => $e->end,
                    'allDay' => false,
                    'color' => $e->color,
                    'className' => 'edit',
                    'extendedProps' => 'Постановщик: '.$e->first_name.' '.$e->last_name.', дата: '.Carbon::parse($e->date_production)->format('d.m.Y H:i:s'),
//                'url' => '/implementer/project-task/edit/'.$p_task['id'] . '/' . $date[0],
                    //'timeFormat' => 'H(:mm)'
                );
            };
        }

        return response()->json($json);
    }

    public function editEvent(Request $request) {
        $data = json_decode($request->data);
        $end = explode('T', $data->end);
        $event = Event::find($data->id);
        $event->end = $end[0].' '.$end[1];
        if ($event->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'Error']);
        }
    }

    public function editTask($id) {
        if (session('perm')['task.update']) {
            $cur_task = Task::find($id);

            $cur_date = date("j").'.'.date("n").'.'.date("Y");
            $cur_date_val = date("Y").'-'.date("m").'-'.date("d");
            $tomorrow = date('Y-m-d', strtotime($cur_date . ' +1 day'));
            $tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +1 day'));
            $plus_one_day = date('Y-m-d', strtotime($cur_date . ' +2 day'));
            $plus_one_day_val = date('Y-m-d', strtotime($cur_date . ' +2 day'));
            $plus_two_day = date('d.m.Y', strtotime($cur_date . ' +3 day'));
            $plus_two_day_val = date('Y-m-d', strtotime($cur_date . ' +3 day'));
            $plus_three_day = date('d.m.Y', strtotime($cur_date . ' +4 day'));
            $plus_three_day_val = date('Y-m-d', strtotime($cur_date . ' +4 day'));

            return view('Implementer.tasks.edit', [
                'task' => $cur_task,
                'cur_date' => $cur_date,
                'cur_date_val' => $cur_date_val,
                'tomorrow' => $tomorrow,
                'tomorrow_val' => $tomorrow_val,
                'plus_one_day' => $plus_one_day,
                'plus_one_day_val' => $plus_one_day_val,
                'plus_two_day' => $plus_two_day,
                'plus_two_day_val' => $plus_two_day_val,
                'plus_three_day' => $plus_three_day,
                'plus_three_day_val' => $plus_three_day_val,
                'task_list' => GroupTasks::getAllTasks(),
                'task_status' => GroupTasks::getAllTasksStatus(),
                'modules' => Modules::getAllModule(),
                'group_task' => GroupTasks::getAllTaskGroup(),
            ]);
        } else {
            abort(503);
        }
    }

    public function getTask($id)
    {
        $cur_p_task = Project_task::find($id);

        if ($cur_p_task) {
            $name = Module_rule::find($cur_p_task['module_rule_id']);
            $p_name = Project::find($cur_p_task['project_id']);
            $ptc = Project_task_contact::where('project_task_id', '=', $cur_p_task['id'])
                ->whereNull('deleted_at')
                ->first();

            // var_dump($cur_p_task);
            if (count((array)$cur_p_task) > 0) {
                // dd(Contact::getProjectMainContact($cur_p_task['project_id']));
                $p_tasks[] = [
                    'id' => $cur_p_task['id'],
                    'title' => $name['task'],
                    'p_name' => $p_name['name'],
                    'city' => $p_name['city'],
                    'start_date' => Carbon::parse($ptc['next_contact'])->format('d.m.Y H:i'),
                    'prev_contact' => $cur_p_task['prev_contact'],
                    'main_contact' => Contact::getProjectMainContact($cur_p_task['project_id']),
                    'project_contacts' => Contact::getProjectContacts($cur_p_task['project_id']),
                ];
            } else {
                $p_tasks = '';
            }
            

            $p_tasks = (object)$p_tasks[0];

            $cur_date = date("j") . '.' . date("n") . '.' . date("Y");
            $cur_date_val = date("Y") . '-' . date("m") . '-' . date("d");
            $tomorrow = date('Y-m-d', strtotime($cur_date . ' +1 day'));
            $tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +1 day'));
            $plus_one_day = date('Y-m-d', strtotime($cur_date . ' +2 day'));
            $plus_one_day_val = date('Y-m-d', strtotime($cur_date . ' +2 day'));
            $plus_two_day = date('d.m.Y', strtotime($cur_date . ' +3 day'));
            $plus_two_day_val = date('Y-m-d', strtotime($cur_date . ' +3 day'));
            $plus_three_day = date('d.m.Y', strtotime($cur_date . ' +4 day'));
            $plus_three_day_val = date('Y-m-d', strtotime($cur_date . ' +4 day'));
            $plus_seven_day = date('d.m.Y', strtotime($cur_date . ' +7 day'));
            $plus_seven_day_val = date('Y-m-d', strtotime($cur_date . ' +7 day'));

            $module_id = Project_module::where('project_id', '=', $p_name['id'])
                ->whereNull('deleted_at')
                ->firstOrFail();

            $comments = DB::select("select `c`.`id`, DATE_FORMAT(`c`.`date`, '%d.%m.%Y') as 'date', `c`.`time`, `c`.`text`, 
                `u`.`first_name`, `u`.`last_name`, `mr`.`task`, `u`.`foto`
                from `comments` `c`
                left join `project_tasks` `pt` on `pt`.`id` = `c`.`task_id`
                left join `module_rules` `mr` on `pt`.`module_rule_id` = `mr`.`id`
                left join `users` `u` on `u`.`id` = `c`.`user_id`
                where
                `c`.`deleted_at` is null and
                `c`.`project_id` = ".$cur_p_task->project_id."
                order by `c`.`date` desc, `c`.`time` desc");

                

                

            // if (count($comments_q) > 0) {
            //     foreach ($comments_q as $c) {
            //         $user = User::find($c->user_id);
            //         $comments[] = [
            //             'text' => strip_tags($c->text),
            //             'date' => $c->date,
            //             'time' => $c->time,
            //             'id' => $c->id,
            //             'user_name' => $user->first_name,
            //             'user_surname' => $user->last_name,
            //             'user_foto' => $user->foto,
            //         ];
            //     }
            // } else {
            //     $comments = '';
            // }

            //dd($comments);

            $project_task = Project_task::query()->where('project_tasks.project_id', '=', $cur_p_task->project_id)
                ->join('modules_references', 'modules_references.id', '=', 'project_tasks.module_id')
                ->leftJoin(DB::raw('project_task_contacts ptc'), 'ptc.project_task_id', '=', DB::raw('project_tasks.id and ptc.deleted_at is null'))
                ->where('tasksstatus_ref_id', '!=', 2)
                ->select(['project_tasks.*', 'modules_references.name as module'])
                ->whereNull('project_tasks.deleted_at')
                ->whereNull('ptc.id')
                ->get();

            $task_status = GroupTasks::getAllTasksStatus();

            if (count($project_task) > 0) {
                foreach ($project_task as $pt) {
                    $name = Module_rule::find($pt->module_rule_id);
                    $status = Tasksstatus_reference::find($pt->tasksstatus_ref_id);

                    $tasks[] = [
                        'id' => $pt->id,
                        'name' => $name->task ?? '',
                        'status_id' => $status['id'],
                        'status_name' => $status['name'],
                        'questions' => $name->questions ?? '',
                        'form' => 0,
                        'module' => $pt->module,
                    ];
                }
            } else {
                $tasks = '';
            }

        /*$project_form = Project_print_form::where('project_id', '=', $cur_p_task->project_id)
            ->whereNull('deleted_at')
            ->get();

        if (count($project_form) > 0) {
            foreach ($project_form as $pf) {
                $name = Module_rule::find($pf->module_rule_id);
                $status = Printform_status_reference::find($pf->printform_status_ref_id);

                $tasks[] = [
                    'id' => $pf->id,
                    'name' => $pf->name,
                    'status_id' => $status->id,
                    'status_name' => $status->name,
                    //'questions' => $name->questions,
                    'form' => 1
                ];
            }
        } else {
            $tasks = '';
        }
        //dd($tasks);*/

            $project = Project::find($cur_p_task->project_id);
            $client = Clients_reference::find($project->client_id);

            $observer = Users::getImplementers();
            foreach ($observer as $o) {
                if ($o->id == $project->user_id) {
                    $observer_delault[] = [
                        'id' => $o->id,
                        'email' => $o->email,
                        'foto' => $o->foto,
                        'first_name' => $o->first_name,
                        'last_name' => $o->last_name,
                        'patronymic' => $o->last_name,
                        'default' => 1
                    ];
                } else {
                    $observer_delault[] = [
                        'id' => $o->id,
                        'email' => $o->email,
                        'foto' => $o->foto,
                        'first_name' => $o->first_name,
                        'last_name' => $o->last_name,
                        'default' => 0
                    ];
                }


            }
            $cur_user = Sentinel::getUser();

            $t_comment = Comment_task::where('task_id', '=', $p_tasks->id)
                ->whereNull('deleted_at')
                ->first();

            if (isset($t_comment->text) == true) {
                $task_com = $t_comment->text;
            } else {
                $task_com = '';
            }

            // dd($task_com);

            return response()->json([
                'project_id' => $cur_p_task->project_id,
                'task' => $p_tasks,
                'cur_date' => $cur_date,
                'cur_date_val' => $cur_date_val,
                'tomorrow' => $tomorrow,
                'tomorrow_val' => $tomorrow_val,
                'plus_one_day' => $plus_one_day,
                'plus_one_day_val' => $plus_one_day_val,
                'plus_two_day' => $plus_two_day,
                'plus_two_day_val' => $plus_two_day_val,
                'plus_three_day' => $plus_three_day,
                'plus_three_day_val' => $plus_three_day_val,
                'plus_seven_day' => $plus_seven_day,
                'plus_seven_day_val' => $plus_seven_day_val,
                'task_list' => GroupTasks::getAllProjectTasks($module_id->module_id),
                'task_status' => $task_status,
                'modules' => Modules::getModulesByProjectId($cur_p_task->project_id),
                'group_task' => GroupTasks::getAllTaskGroup(),
                'comments' => $comments,
                'tasks' => $tasks,
                'client' => $client,
                'observers' => $observer_delault,
                'cur_user_id' => $cur_user->id,
                'task_com' => $task_com,
                'contact' => $ptc->contact()->select('id', 'first_name', 'last_name', 'phone')->first() ?? null
            ]);
        }
    }

    public function editProjectTask($id) {
        if (session('perm')['task.update']) {
            $cur_p_task = Project_task::find($id);

            if ($cur_p_task) {
                $name = Module_rule::find($cur_p_task['module_rule_id']);
                $p_name = Project::find($cur_p_task['project_id']);
                $start_date = Project_task_contact::where('project_task_id', '=', $cur_p_task['id'])
                    ->whereNull('deleted_at')
                    ->first();

                if (count($cur_p_task) > 0) {
                    $p_tasks[] = [
                        'id' => $cur_p_task['id'],
                        'title' => $name['task'],
                        'p_name' => $p_name['name'],
                        'city' => $p_name['city'],
                        'start_date' => Carbon::parse($start_date['next_contact'])->format('d.m.Y H:i'),
                        'prev_contact' => $cur_p_task['prev_contact'],
                        'main_contact' => Contact::getProjectMainContact($cur_p_task['project_id']),
                        'project_contacts' => Contact::getProjectContacts($cur_p_task['project_id']),
                    ];
                } else {
                    $p_tasks = '';
                }
                //dd($p_tasks[0]);

                $p_tasks = (object)$p_tasks[0];

                $cur_date = date("j").'.'.date("n").'.'.date("Y");
                $cur_date_val = date("Y").'-'.date("m").'-'.date("d");
                $tomorrow = date('Y-m-d', strtotime($cur_date . ' +1 day'));
                $tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +1 day'));
                $plus_one_day = date('Y-m-d', strtotime($cur_date . ' +2 day'));
                $plus_one_day_val = date('Y-m-d', strtotime($cur_date . ' +2 day'));
                $plus_two_day = date('d.m.Y', strtotime($cur_date . ' +3 day'));
                $plus_two_day_val = date('Y-m-d', strtotime($cur_date . ' +3 day'));
                $plus_three_day = date('d.m.Y', strtotime($cur_date . ' +4 day'));
                $plus_three_day_val = date('Y-m-d', strtotime($cur_date . ' +4 day'));

                $module_id = Project_module::where('project_id', '=', $p_name['id'])
                    ->whereNull('deleted_at')
                    ->firstOrFail();

                $comments_q = Comment::where('project_id', '=', $cur_p_task->project_id)
                    ->whereNull('deleted_at')
                    ->orderBy('created_at', 'desc')
                    ->get();

                if (count($comments_q) > 0) {
                    foreach ($comments_q as $c) {
                        $user = User::find($c->user_id);
                        $comments[] = [
                            'text' => strip_tags($c->text),
                            'date' => $c->date,
                            'time' => $c->time,
                            'id' => $c->id,
                            'user_name' => $user->first_name,
                            'user_surname' => $user->last_name,
                            'user_foto' => $user->foto,
                        ];
                    }
                } else {
                    $comments = '';
                }

                //dd($comments);

                $project_task = Project_task::where('project_tasks.project_id', '=', $cur_p_task->project_id)
                    ->join('modules_references', 'modules_references.id', '=', 'project_tasks.module_id')
                    //->where('tasksstatus_ref_id', '<>', 2)
                    ->select(['project_tasks.*', 'modules_references.name as module'])
                    ->whereNull('project_tasks.deleted_at')
                    ->get();

                $task_status = GroupTasks::getAllTasksStatus();

                if (count($project_task) > 0) {
                    foreach ($project_task as $pt) {
                        $name = Module_rule::find($pt->module_rule_id);
                        $status = Tasksstatus_reference::find($pt->tasksstatus_ref_id);


                        $tasks[] = [
                            'id' => $pt->id,
                            'name' => $name->task,
                            'status_id' => $status['id'],
                            'status_name' => $status['name'],
                            'questions' => $name->questions,
                            'form' => 0,
                            'module' => $pt->module,
                        ];
                    }
                } else {
                    $tasks = '';
                }

                /*$project_form = Project_print_form::where('project_id', '=', $cur_p_task->project_id)
                    ->whereNull('deleted_at')
                    ->get();

                if (count($project_form) > 0) {
                    foreach ($project_form as $pf) {
                        $name = Module_rule::find($pf->module_rule_id);
                        $status = Printform_status_reference::find($pf->printform_status_ref_id);

                        $tasks[] = [
                            'id' => $pf->id,
                            'name' => $pf->name,
                            'status_id' => $status->id,
                            'status_name' => $status->name,
                            //'questions' => $name->questions,
                            'form' => 1
                        ];
                    }
                } else {
                    $tasks = '';
                }
                //dd($tasks);*/

                $project = Project::find($cur_p_task->project_id);
                $client = Clients_reference::find($project->client_id);

                $observer = Users::getAllUsers();
                foreach ($observer as $o) {
                    if ($o->id == $project->user_id) {
                        $observer_delault[] = [
                            'id' => $o->id,
                            'email' => $o->email,
                            'foto' => $o->foto,
                            'first_name' => $o->first_name,
                            'last_name' => $o->last_name,
                            'patronymic' => $o->last_name,
                            'default' => 1
                        ];
                    } else {
                        $observer_delault[] = [
                            'id' => $o->id,
                            'email' => $o->email,
                            'foto' => $o->foto,
                            'first_name' => $o->first_name,
                            'last_name' => $o->last_name,
                            'default' => 0
                        ];
                    }
                }
                //dd($tasks);

                return view('Implementer.tasks.edit', [
                    'task' => $p_tasks,
                    'cur_date' => $cur_date,
                    'cur_date_val' => $cur_date_val,
                    'tomorrow' => $tomorrow,
                    'tomorrow_val' => $tomorrow_val,
                    'plus_one_day' => $plus_one_day,
                    'plus_one_day_val' => $plus_one_day_val,
                    'plus_two_day' => $plus_two_day,
                    'plus_two_day_val' => $plus_two_day_val,
                    'plus_three_day' => $plus_three_day,
                    'plus_three_day_val' => $plus_three_day_val,
                    'task_list' => GroupTasks::getAllProjectTasks($module_id->module_id),
                    'task_status' => $task_status,
                    'modules' => Modules::getModulesByProjectId($cur_p_task->project_id),
                    'group_task' => GroupTasks::getAllTaskGroup(),
                    'comments' => $comments,
                    'tasks' => $tasks,
                    'client' => $client,
                    'observers' => $observer_delault,
                ]);
            } else {
                abort(404);
            }
        } else {
            abort(503);
        }
    }

    public function getClientProject(Request $request) {
        if ($request->ajax()) {
            $project = Project::where('client_id', '=', $request->cid)
                ->get();

            if (count($project) > 0) {
                return response()->json($project);
            }
        }
    }

    public function postEditProjectTask($id, Request $request) {
        $data = json_decode($request->data);
        // dd($id);
        switch ($data->res) {
            case 'work':
                if ($data->nextContact == 2) {
                    DB::transaction(function () use ($request, $data, $id) {
                        $project = Projects::getProjectByTaskId($id);

                        $date =  explode(' ', date("Y-m-d H:i:s"));
                        $cur_user = Sentinel::getUser();
                        if ($data->comment != 'null') {
                            try {
                                $comment = new Comment();
                                $comment->project_id = $project->id;
                                $comment->user_id = $cur_user->id;
                                $comment->for_client = NULL;
                                $comment->date = $date[0];
                                $comment->time = $date[1];
                                $comment->task_id = $id;
                                $comment->text = $data->comment;
                                $comment->save();

                                $this->success = true;
                            } catch (\Exception $e) {
                                $this->success = false;
                            }
                        }

                        if ($data->commentForClient != 'null') {
                            try {
                                $comment_client = new Comment();
                                $comment_client->project_id = $project->id;
                                $comment_client->user_id = $cur_user->id;
                                $comment_client->for_client = 1;
                                $comment_client->date = $date[0];
                                $comment_client->time = $date[1];
                                $comment_client->text = $data->commentForClient;
                                $comment_client->save();

                                $this->sendMessages($project->id, $data->commentForClient);

                                $this->success = true;
                            } catch (\Exception $e) {
                                $this->success = false;
                            }
                        }
                        //dd($data);

                        try {
                            $cur_task = Project_task::find($id);
                            $cur_task->tasksstatus_ref_id = $data->curTaskStatus;
                            $cur_task->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }

                        try {
                            $cur_task_contact = Project_task_contact::where('project_task_id', '=', $id)
                                ->whereNull('deleted_at')
                                ->get();
                            $cur_task_contact[0]->duration = $data->durationTime;
                            $cur_task_contact[0]->status = 1;
                            $cur_task_contact[0]->callbackstatus_ref_id = 1;
//                            $cur_task_contact[0]->prev_contact = date("Y-m-d H:i:s");
//                            $cur_task_contact[0]->next_contact = Carbon::now()->addDays(3);
//                            $cur_task_contact[0]->end = Carbon::now()->addDays(3)->addHours(1);
                            $cur_task_contact[0]->next_for_cont = $data->nextDate;
                            $cur_task_contact[0]->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }


                        if (isset($data->otherTask)) {
                            foreach ($data->otherTask as $ot) {
                                //dd($ot->taskId);
                                try {
                                    $other_task = Project_task::find($ot->t_id);
                                    $other_task->tasksstatus_ref_id = $ot->status;
                                    //$other_task->prev_contact = date("Y-m-d H:i:s");
                                    $other_task->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }

                        if (isset($data->dopTask)) {
                            foreach ($data->dopTask as $dt) {
                                try {
                                    $dopTask = new Module_rule();
                                    $dopTask->task = $dt->name;
                                    $dopTask->module_id = $dt->module;
                                    $dopTask->number_contacts = $dt->count;
                                    $dopTask->grouptasks_id = $dt->group_task;
                                    $dopTask->additional = 1;
                                    $dopTask->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }

                                try {
                                    $newDopTask = new Project_task();
                                    $newDopTask->project_id = $project->id;
                                    $newDopTask->user_id = $cur_user->id;
                                    $newDopTask->observer_id = $cur_user->id;
                                    $newDopTask->module_rule_id = $dopTask->id;
                                    $newDopTask->number_contacts = $dt->count;
                                    $newDopTask->module_id = $dt->module;
                                    $newDopTask->tasksstatus_ref_id = 1;
                                    $newDopTask->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }

                        if (count($data->cq) > 0) {
                            foreach ($data->cq as $ca) {
                                try {
                                    $answer = new Clarifying_answer();
                                    $answer->question_id = $ca->id;
                                    $answer->project_id = $project->id;
                                    $answer->answer = $ca->answer;
                                    $answer->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }

                        if (isset($data->cqOtherTask)) {
                            foreach ($data->cqOtherTask as $ca2) {
                                try {
                                    $answer = new Clarifying_answer();
                                    $answer->question_id = $ca2->id;
                                    $answer->project_id = $project->id;
                                    $answer->answer = $ca2->answer;
                                    $answer->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }
                    });

                    if ($this->success) {
                        return response()->json(['status' => 'Ok']);
                    } else {
                        return response()->json(['status' => 'Error']);
                    }
                } elseif ($data->nextContact == 3) {

                    /**  Лист ожидания  **/

                    DB::transaction(function () use ($request, $data, $id) {
                        $project = Projects::getProjectByTaskId($id);
//                        $project->waiting_list_ref_id = $data->WList;
//                        $project->save();
                        $wList = $data->wList;

                        if (isset($wList)) {
                            $projectWaitingList = new ProjectWaitingList((array)$wList);
                            $projectWaitingList->waiting_list_id = $wList->waiting_list->id;
                            $projectWaitingList->project_id = $project->id;
                            $projectWaitingList->save();
                        }

                        $date =  explode(' ', date("Y-m-d H:i:s"));
                        $cur_user = Sentinel::getUser();
                        //dd($project);

                        if ($data->comment != 'null') {
                            try {
                                $comment = new Comment();
                                $comment->project_id = $project->id;
                                $comment->user_id = $cur_user->id;
                                $comment->for_client = NULL;
                                $comment->date = $date[0];
                                $comment->time = $date[1];
                                $comment->task_id = $id;
                                $comment->text = $data->comment;
                                $comment->save();

                                $this->success = true;
                            } catch (\Exception $e) {
                                $this->success = false;
                            }
                        }

                        if ($data->commentForClient != 'null') {
                            try {
                                $comment_client = new Comment();
                                $comment_client->project_id = $project->id;
                                $comment_client->user_id = $cur_user->id;
                                $comment_client->for_client = 1;
                                $comment_client->date = $date[0];
                                $comment_client->time = $date[1];
                                $comment_client->text = $data->commentForClient;
                                $comment_client->save();

                                $this->sendMessages($project->id, $data->commentForClient);

                                $this->success = true;
                            } catch (\Exception $e) {
                                $this->success = false;
                            }
                        }

                        if (isset($data->otherTask)) {
                            foreach ($data->otherTask as $ot) {
                                //dd($ot->taskId);
                                try {
                                    $other_task = Project_task::find($ot->t_id);
                                    $other_task->tasksstatus_ref_id = $ot->status;
                                    //$other_task->prev_contact = date("Y-m-d H:i:s");
                                    $other_task->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }

                        if (isset($data->dopTask)) {
                            foreach ($data->dopTask as $dt) {
                                try {
                                    $dopTask = new Module_rule();
                                    $dopTask->task = $dt->name;
                                    $dopTask->module_id = $dt->module;
                                    $dopTask->number_contacts = $dt->count;
                                    $dopTask->grouptasks_id = $dt->group_task;
                                    $dopTask->additional = 1;
                                    $dopTask->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }

                                try {
                                    $newDopTask = new Project_task();
                                    $newDopTask->project_id = $project->id;
                                    $newDopTask->user_id = $cur_user->id;
                                    $newDopTask->observer_id = $cur_user->id;
                                    $newDopTask->module_rule_id = $dopTask->id;
                                    $newDopTask->number_contacts = $dt->count;
                                    $newDopTask->module_id = $dt->module;
                                    $newDopTask->tasksstatus_ref_id = 1;
                                    $newDopTask->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }

                        if (count($data->cq) > 0) {
                            foreach ($data->cq as $ca) {
                                try {
                                    $answer = new Clarifying_answer();
                                    $answer->question_id = $ca->id;
                                    $answer->project_id = $project->id;
                                    $answer->answer = $ca->answer;
                                    $answer->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }

                        if (isset($data->cqOtherTask)) {
                            foreach ($data->cqOtherTask as $ca2) {
                                try {
                                    $answer = new Clarifying_answer();
                                    $answer->question_id = $ca2->id;
                                    $answer->project_id = $project->id;
                                    $answer->answer = $ca2->answer;
                                    $answer->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }
                    });

                    try {
                        $cur_task = Project_task::find($id);
                        $cur_task->tasksstatus_ref_id = $data->curTaskStatus;
                        $cur_task->save();

                        $this->success = true;
                    } catch (\Exception $e) {
                        $this->success = false;
                    }

                    try {
                        $cur_task_contact = Project_task_contact::where('project_task_id', '=', $id)
                            ->whereNull('deleted_at')
                            ->get();
                        $cur_task_contact[0]->duration = $data->durationTime;
                        $cur_task_contact[0]->prev_contact = date("Y-m-d H:i:s");
                        $cur_task_contact[0]->callbackstatus_ref_id = NULL;
                        $cur_task_contact[0]->next_for_cont = NULL;
                        $cur_task_contact[0]->save();

                        $this->success = true;
                    } catch (\Exception $e) {
                        $this->success = false;
                    }

                    if ($this->success) {
                        return response()->json(['status' => 'Ok']);
                    } else {
                        return response()->json(['status' => 'Error']);
                    }
                } else {
                    //dd($data);

                    /**  Назначен следующий контакт  **/


//                    DB::transaction(function () use ($request, $data, $id) {
                        $project = Projects::getProjectByTaskId($id);
                        $date =  explode(' ', date("Y-m-d H:i:s"));
                        $cur_user = Sentinel::getUser();

                        //dd($project);

                        if ($data->comment != 'null') {
                            try {
                                $comment = new Comment();
                                $comment->project_id = $project->id;
                                $comment->user_id = $cur_user->id;
                                $comment->for_client = NULL;
                                $comment->date = $date[0];
                                $comment->time = $date[1];
                                $comment->task_id = $id;
                                $comment->text = $data->comment;
                                $comment->save();

                                $this->success = true;
                            } catch (\Exception $e) {
                                $this->success = false;
                            }
                        }

                        if ($data->commentForClient != 'null') {
                            try {
                                $comment_client = new Comment();
                                $comment_client->project_id = $project->id;
                                $comment_client->user_id = $cur_user->id;
                                $comment_client->for_client = 1;
                                $comment_client->date = $date[0];
                                $comment_client->time = $date[1];
                                $comment_client->text = $data->commentForClient;
                                $comment_client->save();

                                $this->sendMessages($project->id, $data->commentForClient);
                                $this->success = true;
                            } catch (\Exception $e) {
                                $this->success = false;
                            }
                        }

                        if (isset($data->otherTask)) {
                            foreach ($data->otherTask as $ot) {
                                //dd($ot->taskId);
                                try {
                                    $other_task = Project_task::find($ot->t_id);
                                    $other_task->tasksstatus_ref_id = $ot->status;
                                    //$other_task->prev_contact = date("Y-m-d H:i:s");
                                    $other_task->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }

                        if (isset($data->dopTask)) {
                            foreach ($data->dopTask as $dt) {
                                try {
                                    $dopTask = new Module_rule();
                                    $dopTask->task = $dt->name;
                                    $dopTask->module_id = $dt->module;
                                    $dopTask->number_contacts = $dt->count;
                                    $dopTask->grouptasks_id = $dt->group_task;
                                    $dopTask->additional = 1;
                                    $dopTask->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }

                                try {
                                    $newDopTask = new Project_task();
                                    $newDopTask->project_id = $project->id;
                                    $newDopTask->user_id = $cur_user->id;
                                    $newDopTask->observer_id = $cur_user->id;
                                    $newDopTask->module_rule_id = $dopTask->id;
                                    $newDopTask->number_contacts = $dt->count;
                                    $newDopTask->module_id = $dt->module;
                                    $newDopTask->tasksstatus_ref_id = 1;
                                    $newDopTask->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }

                        if (count($data->cq) > 0) {
                            foreach ($data->cq as $ca) {
                                try {
                                    $answer = new Clarifying_answer();
                                    $answer->question_id = $ca->id;
                                    $answer->project_id = $project->id;
                                    $answer->answer = $ca->answer;
                                    $answer->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }

                        if (isset($data->cqOtherTask)) {
                            foreach ($data->cqOtherTask as $ca2) {
                                try {
                                    $answer = new Clarifying_answer();
                                    $answer->question_id = $ca2->id;
                                    $answer->project_id = $project->id;
                                    $answer->answer = $ca2->answer;
                                    $answer->save();

                                    $this->success = true;
                                } catch (\Exception $e) {
                                    $this->success = false;
                                }
                            }
                        }

                        //dd($data);
//                        print_r($data->nextContTask);
//                        die;
//                        try {
//                            $newContTask = Project_task_contact::where('project_task_id', '=', $data->nextContTask)
//                                ->whereNull('deleted_at')
//                                ->get();
//
//
//                            $t = Carbon::createFromTimeString($data->nextContTimeEnd)->diffInSeconds(Carbon::createFromTimeString($data->nextContTimeStart));
//                            if (count($newContTask) > 0) {
//                                $newContTask[0]->next_contact = $data->nextContDateStart.' '.$data->nextContTimeStart.':00';
//                                $newContTask[0]->end = $data->nextContDateStart.' '.$data->nextContTimeEnd.':00';
//                                $newContTask[0]->user_id = $data->nextContUser;
//                                $newContTask[0]->duration = gmdate('H:i:s', $t);
//                                $newContTask[0]->prev_contact = date("Y-m-d H:i:s");
//                                $newContTask[0]->save();
//                            } else {
//                                //dd($data->nextContUser);
//                                $newContTask2 = new Project_task_contact();
//                                $newContTask2->project_id = $project->id;
//                                $newContTask2->user_id = $data->nextContUser;
//                                $newContTask2->project_task_id = $data->nextContTask;
//                                $newContTask2->duration = gmdate('H:i:s', $t);
//                                $newContTask2->next_contact = $data->nextContDateStart.' '.$data->nextContTimeStart;
//                                $newContTask2->end = $data->nextContDateStart.' '.$data->nextContTimeEnd.':00';
//                                $newContTask2->prev_contact = date("Y-m-d H:i:s");
//
//                                $newContTask2->save();
//                            }
//
//                            $this->success = true;
//                        } catch (\Exception $e) {
//                            $this->success = false;
//                        }
                        if ($data->nextContTask > 0) {
                            try {
                                $newContTask = Project_task_contact::where('project_task_id', '=', $data->nextContTask)
                                    ->whereNull('deleted_at')
                                    ->get();


                                $t = Carbon::createFromTimeString($data->nextContTimeEnd)->diffInSeconds(Carbon::createFromTimeString($data->nextContTimeStart));
                                if (count($newContTask) > 0) {
                                    $newContTask[0]->next_contact = $data->nextContDateStart.' '.$data->nextContTimeStart.':00';
                                    $newContTask[0]->end = $data->nextContDateStart.' '.$data->nextContTimeEnd.':00';
                                    $newContTask[0]->user_id = $data->nextContUser;
                                    $newContTask[0]->callbackstatus_ref_id = NULL;
                                    $newContTask[0]->next_for_cont = NULL;
                                    $newContTask[0]->duration = gmdate('H:i:s', $t);
                                    $newContTask[0]->prev_contact = date("Y-m-d H:i:s");
                                    $newContTask[0]->date_production = date("Y-m-d H:i:s");
                                    $newContTask[0]->observer_id = $cur_user->id;
                                    $newContTask[0]->contact_id = $data->projectContact;
                                    $newContTask[0]->save();
                                } else {
                                    //dd($data->nextContUser);
                                    $newContTask2 = new Project_task_contact();
                                    $newContTask2->project_id = $project->id;
                                    $newContTask2->user_id = $data->nextContUser;
                                    $newContTask2->project_task_id = $data->nextContTask;
                                    $newContTask2->duration = gmdate('H:i:s', $t);
                                    $newContTask2->next_contact = $data->nextContDateStart.' '.$data->nextContTimeStart;
                                    $newContTask2->end = $data->nextContDateStart.' '.$data->nextContTimeEnd.':00';
                                    $newContTask2->prev_contact = date("Y-m-d H:i:s");
                                    $newContTask2->date_production = date("Y-m-d H:i:s");
                                    $newContTask2->observer_id = $cur_user->id;
                                    $newContTask2->contact_id = $data->projectContact;
                                    $newContTask2->save();
                                }

                                $this->success = true;
                            } catch (\Exception $e) {
                                $this->success = false;
                            }
                        } else {
                            try {
                                $newContTask = Project_task_contact::where('project_task_id', '=', $newDopTask->id)
                                    ->whereNull('deleted_at')
                                    ->get();


                                $t = Carbon::createFromTimeString($data->nextContTimeEnd)->diffInSeconds(Carbon::createFromTimeString($data->nextContTimeStart));
                                if (count($newContTask) > 0) {
                                    $newContTask[0]->next_contact = $data->nextContDateStart.' '.$data->nextContTimeStart.':00';
                                    $newContTask[0]->end = $data->nextContDateStart.' '.$data->nextContTimeEnd.':00';
                                    $newContTask[0]->user_id = $data->nextContUser;
                                    $newContTask[0]->callbackstatus_ref_id = NULL;
                                    $newContTask[0]->next_for_cont = NULL;
                                    $newContTask[0]->duration = gmdate('H:i:s', $t);
                                    $newContTask[0]->prev_contact = date("Y-m-d H:i:s");
                                    $newContTask[0]->date_production = date("Y-m-d H:i:s");
                                    $newContTask[0]->observer_id = $cur_user->id;
                                    $newContTask[0]->contact_id = $data->projectContact;
                                    $newContTask[0]->save();
                                } else {
                                    //dd($data->nextContUser);
                                    $newContTask2 = new Project_task_contact();
                                    $newContTask2->project_id = $project->id;
                                    $newContTask2->user_id = $data->nextContUser;
                                    $newContTask2->project_task_id = $newDopTask->id;
                                    $newContTask2->duration = gmdate('H:i:s', $t);
                                    $newContTask2->next_contact = $data->nextContDateStart.' '.$data->nextContTimeStart;
                                    $newContTask2->end = $data->nextContDateStart.' '.$data->nextContTimeEnd.':00';
                                    $newContTask2->prev_contact = date("Y-m-d H:i:s");
                                    $newContTask2->date_production = date("Y-m-d H:i:s");
                                    $newContTask2->observer_id = $cur_user->id;
                                    $newContTask2->contact_id = $data->projectContact;
                                    $newContTask2->save();
//                                    print_r($newContTask2->id);
//                                    die;
                                }

                                $this->success = true;
                            } catch (\Exception $e) {
                                $this->success = false;
                            }
                        }

//                    });


                    try {
                        $cur_task = Project_task::find($id);
                        $cur_task->tasksstatus_ref_id = $data->curTaskStatus;
                        $cur_task->save();

                        $this->success = true;
                    } catch (\Exception $e) {
                        $this->success = false;
                    }

                    try {
                        $cur_task_contact = Project_task_contact::where('project_task_id', '=', $id)
                            ->whereNull('deleted_at')
                            ->get();
                            
                        $dt = explode(" ", $cur_task_contact[0]->next_contact);
                        $date = strtotime($dt[1]) + strtotime($data->durationTime) - strtotime("00:00:00");
                        
                        $cur_task_contact[0]->duration = $data->durationTime;
                        $cur_task_contact[0]->prev_contact = date("Y-m-d H:i:s");
                        $cur_task_contact[0]->callbackstatus_ref_id = NULL;
                        $cur_task_contact[0]->next_for_cont = NULL;
                        $cur_task_contact[0]->end = $dt[0].' '.date('H:i:s',$date);
                        $cur_task_contact[0]->save();

                        $this->success = true;
                    } catch (\Exception $e) {
                        $this->success = false;
                    }

                    if ($this->success) {
                        return response()->json(['status' => 'Ok']);
                    } else {
                        return response()->json(['status' => 'Error']);
                    }
                }
                break;
            case 'next':
//                print_r($data);
//                die;
                DB::transaction(function () use ($request, $data, $id) {
                    $project = Projects::getProjectByTaskId($id);
                    $date =  explode(' ', date("Y-m-d H:i:s"));
                    $cur_user = Sentinel::getUser();
                    if ($data->comment != 'null') {
                        try {
                            $comment = new Comment();
                            $comment->project_id = $project->id;
                            $comment->user_id = $cur_user->id;
                            $comment->for_client = NULL;
                            $comment->date = $date[0];
                            $comment->time = $date[1];
                            $comment->task_id = $id;
                            $comment->text = $data->comment;
                            $comment->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }
                    }

                    if ($data->commentForClient != 'null') {
                        try {
                            $comment_client = new Comment();
                            $comment_client->project_id = $project->id;
                            $comment_client->user_id = $cur_user->id;
                            $comment_client->for_client = 1;
                            $comment_client->date = $date[0];
                            $comment_client->time = $date[1];
                            $comment_client->text = $data->commentForClient;
                            $comment_client->save();

                            $this->sendMessages($project->id, $data->commentForClient);
                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }
                    }

                    //$data = json_decode($request->data);
                    try {
                        $project_task = Project_task_contact::where('project_task_id', '=', $id)
                            ->whereNull('deleted_at')
                            ->get();
                        $project_task[0]->prev_contact = $project_task[0]->next_contact;
//                        $project_task[0]->next_contact = Carbon::now()->addDays(3);
                        $project_task[0]->next_contact = $data->nextContDateStart.' '.$data->nextContTimeStart;
                        $project_task[0]->end = $data->nextContDateStart.' '.$data->nextContTimeEnd;
                        $project_task[0]->callbackstatus_ref_id = NULL;
                        $project_task[0]->next_for_cont = NULL;
                        $project_task[0]->save();

                        $this->success = true;
                    } catch (\Exception $e) {
                        $this->success = false;
                    }
                });

                if ($this->success) {
                    return response()->json(['status' => 'Ok']);
                } else {
                    return response()->json(['status' => 'Error']);
                }
                break;
            case 'later':
                //dd($request->all());
                $project = Projects::getProjectByTaskId($id);
                $date =  explode(' ', date("Y-m-d H:i:s"));
                $cur_user = Sentinel::getUser();
                try {
                    $p_task = Project_task_contact::where('project_task_id', '=', $id)
                        ->whereNull('deleted_at')
                        ->get();
                    //dd($p_task[0]);
                    $p_task[0]->status = 1;
                    $p_task[0]->callbackstatus_ref_id = 1;
//                    $p_task[0]->prev_contact = date("Y-m-d H:i:s");
//                    $p_task[0]->next_contact = Carbon::now()->addDays(3);
                    $p_task[0]->next_for_cont = Carbon::now()->addDays(3)->addHours(1);
                    $p_task[0]->save();

                    $comment = new Comment();
                    $comment->project_id = $project->id;
                    $comment->user_id = 0;
                    $comment->for_client = NULL;
                    $comment->date = $date[0];
                    $comment->time = $date[1];
                    $comment->task_id = $id;
                    $comment->text = 'До клиента не дозвонились';
                    $comment->save();

                    if ($data->comment != 'null') {
                        try {
                            $comment = new Comment();
                            $comment->project_id = $project->id;
                            $comment->user_id = $cur_user->id;
                            $comment->for_client = NULL;
                            $comment->date = $date[0];
                            $comment->time = $date[1];
                            $comment->task_id = $id;
                            $comment->text = $data->comment;
                            $comment->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }
                    }

                    if ($data->commentForClient != 'null') {
                        try {
                            $comment_client = new Comment();
                            $comment_client->project_id = $project->id;
                            $comment_client->user_id = $cur_user->id;
                            $comment_client->for_client = 1;
                            $comment_client->date = $date[0];
                            $comment_client->time = $date[1];
                            $comment_client->text = $data->commentForClient;
                            $comment_client->save();

                            $this->sendMessages($project->id, $data->commentForClient);

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }
                    }

                    $this->success = true;
                } catch (\Exception $e) {
                    $this->success = false;
                }

                if ($this->success) {
                    return response()->json(['status' => 'Ok']);
                } else {
                    return response()->json(['status' => 'Error']);
                }
                break;
        }
    }

    public function getTasksForAmcalendar(Request $request) {
        if ($request->ajax()) {
            $project_task_contact = Project_task_contact::where('user_id', '=', $request->user_id)
                ->where('next_contact', 'like', $request->date.'%')
                //->where('tasksstatus_ref_id', '!=', 2)
                ->whereNull('deleted_at')
                ->get();

            if (count($project_task_contact) > 0) {
                foreach ($project_task_contact as $ptc) {
                    $pt = Project_task::find($ptc->project_task_id);
                    $task_name = Module_rule::find($pt->module_rule_id);
                    $date = explode(' ', $ptc->next_contact);
                    $tasks[] = [
                        'id' => $ptc->id,
                        'time' => $date[1],
                        'task' => $task_name->task
                    ];
                }
            } else {
                $tasks[] = '';
            }

            return response()->json($tasks);
        }
    }

    static public function getTaskById($id) {
        $task = Project_task::whereNull('deleted_at')
            ->where('id', '=', $id)
            ->firstOrFail();

        return $task;
    }

    public function getClarificationQuestions(Request $request) {
        $data = json_decode($request->data);

        $p_task = Project_task::find($data->tid);
        $c_question = Clarifying_question::where('task_id', '=', $p_task->module_rule_id)
            ->whereNull('deleted_at')
            ->get();

        if (count($c_question) > 0) {
            foreach ($c_question as $q) {
                $answer = Clarifying_answer::where('question_id', '=', $q->id)
                    ->where('project_id', '=', $p_task->project_id)
                    ->whereNull('deleted_at')
                    ->first();
                if ($answer) {
                    $question = [
                        'id' => $q->id,
                        'question' => $q->question,
                        'task_id' => $q->task_id,
                        'answer' => $answer->answer
                    ];
                } else {
                    $question = [
                        'id' => $q->id,
                        'question' => $q->question,
                        'task_id' => $q->task_id,
                        'answer' => ''
                    ];
                }
                $question_ar[] = $question;
            }
            return response()->json($question_ar);
        } else {
            //$question[] = '';
            return response()->json();
        }
    }

    public function postAddTask(Request $request) {
        switch ($request->typeTask) {
            case 'single_task':
                dd($request->all());
                break;
            case 'introduction':
                dd($request->all());
                break;
        }
    }

    public function getTasksByDateTime(Request $request) {
        if ($request->ajax()) {
            $count = Project_task_contact::where('end', '>', $request->date.' '.$request->time)
                //->where('tasksstatus_ref_id', '!=', 2)
                ->whereNull('deleted_at')
                ->count();

            return response()->json($count);
        }
    }

    public function getStatus(Request $request) {
        if ($request->ajax()) {
            $status = Tasksstatus_reference::whereNull('deleted_at')->get();
            return response()->json($status);
        }
    }

    public function getWeekends(Request $request) {
        if ($request->ajax()) {
            for($i = 1; $i <= date("t"); $i++) {
                $weekend = date("w",strtotime($i.date(".m.Y")));
                if($weekend==0 || $weekend==6) {
                    if ($i<10) {
                        $weekends[] = [
                            'date' => date("Y-m-").'0'.$i
                        ];
                    } else {
                        $weekends[] = [
                            'date' => date("Y-m-").$i
                        ];
                    }
                }
            }

            return response()->json($weekends);
        }
    }

    public function getProjectTask(Request $request) {
        if ($request->ajax()) {
            $project_task = Project_task::where('project_id', '=', $request->pid)
                ->whereNull('deleted_at')
                ->where('tasksstatus_ref_id', '!=', 2)
                ->get();
            foreach ($project_task as $pt) {
                $module_rule = Module_rule::find($pt['module_rule_id']);
                $task[] = [
                    'id' => $pt['id'],
                    'name' => $module_rule['task']
                ];
            }

            return response()->json($task);
        }
    }

    public function addTaskFromCalendar(Request $request) {
        $data = json_decode($request->data);
        dd($data);
        $res = strtotime($request->time) + strtotime('00:59')-strtotime("00:00:00");
        $cur_user = Sentinel::getUser();
        $project_task_contact = new Project_task_contact();
        $project_task_contact->project_id = $request->project;
        $project_task_contact->user_id = $cur_user->id;
        $project_task_contact->project_task_id = $request->task;
        $project_task_contact->next_contact = $request->date.' '.$request->time;
        $project_task_contact->end = $request->date.' '.$request->timeEnd;
        if ($project_task_contact->save()) {
            return redirect()->back();
        } else {
            return redirect()->back()->with('error', 'Произошла ошибка при обновлении данных');
        }
    }

    public function setTasks(Request $request) {
        $data = json_decode($request->data);
        // dd($data);
        $date_start = explode('T', $data->start);
        $date_end = explode('T', $data->end);

        if ($data->isEvent) {
            $task2 = Event::where('id', '=', $data->id)
                ->first();
            $task2->start = $date_start[0].' '.$date_start[1];
            $task2->end = $date_end[0].' '.$date_end[1];
            if ($task2->save()) {
                return response()->json(['status' => 'Ok']);
            } else {
                return response()->json(['status' => 'No']);
            }
        } else {
            $task = Project_task_contact::where('project_task_id', '=', $data->id)
                ->whereNull('deleted_at')
                ->first();
            $task->next_contact = $date_start[0].' '.$date_start[1];
            $task->end = $date_end[0].' '.$date_end[1];
            if ($task->save()) {
                return response()->json(['status' => 'Ok']);
            } else {
                return response()->json(['status' => 'No']);
            }
        };
    }

    public function edit(Request $request) {
        $data = json_decode($request->data);
        $date = explode('T', $data->end);
        $task = Project_task_contact::where('project_task_id', '=', $data->id)
            ->whereNull('deleted_at')
            ->first();
        $task->end = $date[0].' '.$date[1];
        if ($task->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'No']);
        }
    }

    public function delCont(Request $request) {
        $data = json_decode($request->data);
        
        $task = Project_task::find($data->id);
        if ($task->tasksstatus_ref_id == 1) {
            $cont = Project_task_contact::where('project_task_id', '=', $data->id)
                ->whereNull('deleted_at')
                ->first();

            $cont->deleted_at = Carbon::now()->format('Y-m-d H:i:s');

            if ($cont->save()) {
                return response()->json(['status' => 'Ok']);
            } else {
                return response()->json(['status' => 'Error']);
            }
        } else {
            return response()->json(['message' => 'Удаление не возможно']);
        };
    }

    public function delEvent(Request $request) {
        $data = json_decode($request->data);
        $event = Event::find($data->id);
        $event->deleted_at = Carbon::now()->format('Y-m-d H:m:s');
        if ($event->save()) {
            return response()->json(['status' => 'Ok']);
        } else {
            return response()->json(['status' => 'No']);
        }
    }

    function sendMessages($projectId, $message){
        $cur_user = Sentinel::getUser();
        $p_contact = DB::select(
            "select `c`.`first_name`, `c`.`last_name`, `c`.`patronymic`, `c`.`email` 
                   from `contacts` `c`
                   left join `project_contacts` `pc` on `pc`.`contact_id` = `c`.`id`
                   where `pc`.`project_id` = ".$projectId." 
                   and `pc`.`deleted_at` is null
                   and pc.main = 1 
                   and pc.mail_rep = 1");

        foreach ($p_contact as $contact) {
            Mail::send(new SendComment($cur_user->email,
                $cur_user->last_name,
                $cur_user->first_name,
                $contact->last_name,
                $contact->first_name,
                $contact->patronymic,
                $contact->email,
                $message));
        }
    }
}
