<?php

namespace App\Http\Controllers\Implementer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Helpers\Contracts\AMImap;

class ImpMailController extends Controller
{
    private $listBox;

    public function __construct(AMImap $imap) {
        $this->mailbox = $imap->connect();
        if ($this->mailbox) {
            $this->listBox = $imap->getMailboxes();
        }
    }

    public function mail(AMImap $imap, $folder = 'INBOX') {
        $flag = 0;
        if ($folder == 'inbox') {
            $folder = strtoupper($folder);
        } else {
            $folder = ucfirst($folder);
        }

        foreach ($this->listBox as $lb) {
            if ($folder == $lb['shortpath']) {
                $flag = 1;
                $mailbox = $lb;
            }
        }

        if ($flag == 1) {
            dd($imap->getMailInBox($folder));
        } else {
            abort(404);
        }

    }

    public function inboxMail() {
        /*$mailsIds = $this->mailbox->searchMailbox('ALL');

        foreach ($mailsIds as $m_id) {
            $m = $this->mailbox->getMail($m_id);
            $date = Carbon::createFromTimestamp(strtotime($m->headers->Date))
                ->timezone('Europe/Moscow')
                ->toDateTimeString();
            $mail[] = [
                'id' => $m->id,
                'personal' => $this->mailbox->decodeMimeStr($m->headers->sender[0]->personal, 'utf-8'),
                'subject' => $this->mailbox->decodeMimeStr($m->headers->Subject, 'utf-8'),
                'sender' => $m->headers->sender[0]->mailbox.'@'.$m->headers->sender[0]->host,
                'date' => $date,
                'short_text' => strip_tags($m->textHtml)
            ];
        }
        //dd($mail);

        return view('admin/mail/mail', [
            'header' => 'Входящие',
            'folders' => $this->folder,
            'mail' => $mail
        ]);*/
    }

    public function getMessage($id) {
        /*$message = $this->mailbox->getMail($id);
        //dd($message);
        $date = Carbon::createFromTimestamp(strtotime($message->headers->Date))
            ->timezone('Europe/Moscow')
            ->toDateTimeString();
        $test = htmlspecialchars_decode($message->textHtml);

        return view('admin/mail/message', [
            'folders' => $this->folder,
            'subject' => $message->subject,
            'fromaddress' => $message->headers->fromaddress,
            'date' => $date,
            'text' => $test
        ]);*/
    }

    public function archiveMail() {
        /*$mailbox = new Mailbox('{37.61.179.77:143/imap/tls/novalidate-cert}Archive', 'yamaletdinov@archimed-soft.ru', 'yMCKECh345', __DIR__);
        $mailsIds = $mailbox->searchMailbox('ALL');

        if ($mailsIds) {
            foreach ($mailsIds as $m_id) {
                $m = $this->mailbox->getMail($m_id);
                $date = Carbon::createFromTimestamp(strtotime($m->headers->Date))
                    ->timezone('Europe/Moscow')
                    ->toDateTimeString();
                $mail[] = [
                    'id' => $m->id,
                    'personal' => $this->mailbox->decodeMimeStr($m->headers->sender[0]->personal, 'utf-8'),
                    'subject' => $this->mailbox->decodeMimeStr($m->headers->Subject, 'utf-8'),
                    'sender' => $m->headers->sender[0]->mailbox.'@'.$m->headers->sender[0]->host,
                    'date' => $date,
                    'short_text' => strip_tags($m->textHtml)
                ];
            }
        } else {
            $mail[] = '';
        }

        //dd($mail);

        return view('admin/mail/mail', [
            'header' => 'Архив',
            'folders' => $this->folder,
            'mail' => $mail
        ]);*/
    }

    public function trashMail() {
        /*$mailbox = new Mailbox('{37.61.179.77:143/imap/tls/novalidate-cert}Trash', 'yamaletdinov@archimed-soft.ru', 'yMCKECh345', __DIR__);
        $mailsIds = $mailbox->searchMailbox('ALL');

        if ($mailsIds) {
            foreach ($mailsIds as $m_id) {
                $m = $this->mailbox->getMail($m_id);
                $date = Carbon::createFromTimestamp(strtotime($m->headers->Date))
                    ->timezone('Europe/Moscow')
                    ->toDateTimeString();
                $mail[] = [
                    'id' => $m->id,
                    'personal' => $this->mailbox->decodeMimeStr($m->headers->sender[0]->personal, 'utf-8'),
                    'subject' => $this->mailbox->decodeMimeStr($m->headers->Subject, 'utf-8'),
                    'sender' => $m->headers->sender[0]->mailbox.'@'.$m->headers->sender[0]->host,
                    'date' => $date,
                    'short_text' => strip_tags($m->textHtml)
                ];
            }
        } else {
            $mail[] = '';
        }

        //dd($mail);

        return view('admin/mail/mail', [
            'header' => 'Корзина',
            'folders' => $this->folder,
            'mail' => $mail
        ]);*/
    }

    public function sentMail() {
        /*$mailbox = new Mailbox('{37.61.179.77:143/imap/tls/novalidate-cert}Sent', 'yamaletdinov@archimed-soft.ru', 'yMCKECh345', __DIR__);
        $mailsIds = $mailbox->searchMailbox('ALL');

        if ($mailsIds) {
            foreach ($mailsIds as $m_id) {
                $m = $this->mailbox->getMail($m_id);
                $date = Carbon::createFromTimestamp(strtotime($m->headers->Date))
                    ->timezone('Europe/Moscow')
                    ->toDateTimeString();
                $mail[] = [
                    'id' => $m->id,
                    'personal' => $this->mailbox->decodeMimeStr($m->headers->sender[0]->personal, 'utf-8'),
                    'subject' => $this->mailbox->decodeMimeStr($m->headers->Subject, 'utf-8'),
                    'sender' => $m->headers->sender[0]->mailbox.'@'.$m->headers->sender[0]->host,
                    'date' => $date,
                    'short_text' => strip_tags($m->textHtml)
                ];
            }
        } else {
            $mail[] = '';
        }

        //dd($mail);

        return view('admin/mail/mail', [
            'header' => 'Отправленные',
            'folders' => $this->folder,
            'mail' => $mail
        ]);*/
    }

    public function junkMail() {
        /*$mailbox = new Mailbox('{37.61.179.77:143/imap/tls/novalidate-cert}Junk', 'yamaletdinov@archimed-soft.ru', 'yMCKECh345', __DIR__);
        $mailsIds = $mailbox->searchMailbox('ALL');

        if ($mailsIds) {
            foreach ($mailsIds as $m_id) {
                $m = $this->mailbox->getMail($m_id);
                $date = Carbon::createFromTimestamp(strtotime($m->headers->Date))
                    ->timezone('Europe/Moscow')
                    ->toDateTimeString();
                $mail[] = [
                    'id' => $m->id,
                    'personal' => $this->mailbox->decodeMimeStr($m->headers->sender[0]->personal, 'utf-8'),
                    'subject' => $this->mailbox->decodeMimeStr($m->headers->Subject, 'utf-8'),
                    'sender' => $m->headers->sender[0]->mailbox.'@'.$m->headers->sender[0]->host,
                    'date' => $date,
                    'short_text' => strip_tags($m->textHtml)
                ];
            }
        } else {
            $mail[] = '';
        }

        //dd($mail);

        return view('admin/mail/mail', [
            'header' => 'Спам',
            'folders' => $this->folder,
            'mail' => $mail
        ]);*/
    }

    public function draftsMail() {
        /*$mailbox = new Mailbox('{37.61.179.77:143/imap/tls/novalidate-cert}Drafts', 'yamaletdinov@archimed-soft.ru', 'yMCKECh345', __DIR__);
        $mailsIds = $mailbox->searchMailbox('ALL');

        if ($mailsIds) {
            foreach ($mailsIds as $m_id) {
                $m = $this->mailbox->getMail($m_id);
                $date = Carbon::createFromTimestamp(strtotime($m->headers->Date))
                    ->timezone('Europe/Moscow')
                    ->toDateTimeString();
                $mail[] = [
                    'id' => $m->id,
                    'personal' => $this->mailbox->decodeMimeStr($m->headers->sender[0]->personal, 'utf-8'),
                    'subject' => $this->mailbox->decodeMimeStr($m->headers->Subject, 'utf-8'),
                    'sender' => $m->headers->sender[0]->mailbox.'@'.$m->headers->sender[0]->host,
                    'date' => $date,
                    'short_text' => strip_tags($m->textHtml)
                ];
            }
        } else {
            $mail[] = '';
        }

        //dd($mail);

        return view('admin/mail/mail', [
            'header' => 'Ченовики',
            'folders' => $this->folder,
            'mail' => $mail
        ]);*/
    }

    public function getMailInFolder(Request $request) {
        /*if ($request->ajax()) {
            switch ($request->slug) {
                case 'inbox':

                    break;
                case 'archive':
                    break;
                case 'trash':
                    break;
                case 'sent':
                    break;
                case 'junk':
                    break;
                case 'drafts':
                    break;
            }
            return response()->json($request->slug);
        }*/
    }
}
