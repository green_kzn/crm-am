<?php

namespace App\Http\Controllers\Implementer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImpPrintFormsController extends Controller
{
    public function index() {
        return view('Implementer.printforms.printforms');
    }
}
