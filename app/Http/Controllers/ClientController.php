<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use App\Http\Controllers\PermissionsController as Permissions;
use App\Client;

class ClientController extends Controller
{
    public function client() {
        $cur_user = Sentinel::getUser();
        $role = Sentinel::getUser()->roles()->first()->name;

        $perm = new Permissions();

        $clients = Client::whereNull('deleted_at')->paginate(10);

        return view('admin.clients.clients', [
            'user' => $cur_user,
            'role' => $role,
            'user_perm' => $perm->getAdminPermissions()[0],
            'group_perm' => $perm->getAdminPermissions()[1],
            'event_perm' => $perm->getAdminPermissions()[2],
            'clients' => $clients,
        ]);
    }

    public function clientView($id) {
        $cur_user = Sentinel::getUser();
        $role = Sentinel::getUser()->roles()->first()->name;

        $perm = new Permissions();

        $client = Client::find($id);

        return view('admin.clients.client', [
            'user' => $cur_user,
            'role' => $role,
            'user_perm' => $perm->getAdminPermissions()[0],
            'group_perm' => $perm->getAdminPermissions()[1],
            'event_perm' => $perm->getAdminPermissions()[2],
            'client' => $client,
        ]);
    }

    public function clientDel($id) {
        $client = Client::find($id);
        $client->deleted_at = date('Y-m-d H:i:s');

        if ($client->save()) {
            return redirect()->back()->with('success', 'Клиент успешно удален');
        } else {
            return redirect()->back()->with('error', 'Ошибка при удалении клиента');
        }
    }

    public function clientSearch(Request $request) {
        if ($request->ajax()) {
            $client = Client::where('name', 'like', '%'.$request->search.'%')
                ->orWhere('phone', 'like', '%'.$request->search.'%')
                ->orWhere('email', 'like', '%'.$request->search.'%')
                ->get();
            return $client;
        }
    }
}
