<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Events\addPrintFormFromProject;
use App;
use Sentinel;
use App\Printform_file;
use App\Project_print_form;
use App\Done_printform_file;
use App\Printform_comment;
use App\Http\Controllers\Controller;
use DB;
use Event;

class FileController extends Controller
{
    private $image_ext = ['jpg', 'jpeg', 'png', 'gif'];
    private $audio_ext = ['mp3', 'ogg', 'mpga'];
    private $video_ext = ['mp4', 'mpeg'];
    private $document_ext = ['doc', 'docx', 'pdf', 'odt', 'xls', 'xlsx'];

    /**
     * Constructor
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    public function test(Request $request) {
        dd('Ok');
    }

    /**
     * Fetch files by Type or Id
     * @param string $type File type
     * @param integer $id File Id
     * @return object    Files list, JSON
     */
    public function index($type, $id = null)
    {
        $model = new File();

        if (!is_null($id)) {
            $response = $model::findOrFail($id);
        } else {
            $records_per_page = ($type == 'video') ? 6 : 15;

            $files = $model::where('type', $type)
                ->where('user_id', Auth::id())
                ->orderBy('id', 'desc')->paginate($records_per_page);

            $response = [
                'pagination' => [
                    'total' => $files->total(),
                    'per_page' => $files->perPage(),
                    'current_page' => $files->currentPage(),
                    'last_page' => $files->lastPage(),
                    'from' => $files->firstItem(),
                    'to' => $files->lastItem()
                ],
                'data' => $files
            ];
        }

        return response()->json($response);
    }

    public function storeAttendant(Request $request) {
        // if(version_compare(PHP_VERSION, '7.2.0', '>=')) {
        //     error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
        // }

        $cur_user = Sentinel::getUser();
        // dd($cur_user);
        $max_size = (int)ini_get('upload_max_filesize') * 1000000;
        $all_ext = implode(',', $this->allExtensions());

        $this->validate($request, [
            'name' => 'required|unique:files',
            'file' => 'required|file|mimes:' . $all_ext . '|max:' . $max_size
        ]);

        $r = rand(100, 1000);
        $file = $request->file('file');
        $name = explode('.', $file->getClientOriginalName());
        $ext = $file->getClientOriginalExtension();
        $type = $this->getType($ext);
        $rand = $r.'_'.$request['name'];
        $rand2 = $r.'_'.$name[0];
        // dd('Ok');
        // $upload = LGD::put($rand, file_get_contents($file->getRealPath()));
//        $upload = Storage::disk('yandexcloud')->put('15975333', file_get_contents($file->getRealPath()));
        $fileAdapter =  new YandexFileAdapter();
        $upload = $fileAdapter->put($rand, file_get_contents($file->getRealPath()));
        // sleep(10);
        if ($upload) {
            // print_r(file_put_contents($upload->contents));
            $a = $upload->getAllProperties();
            
            return response()->json([
                'status' => true,
                'file_name' => $rand,
                'ext' => $ext,
                'g_name' => $a['basename'],
                'o_name' => $name[0],
                'name' => $rand2,
                'public_url' => $upload['public_url'],
                'download_url' => $upload['download_url'],
            ]);
        }

        return response()->json(['status' => false]);
    }

    /**
     * Upload new file and store it
     * @param Request $request Request with form data: filename and file info
     * @return boolean     True if success, otherwise - false
     */
    public function store(Request $request)
    {
        // dd('Ok');
        if(version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
        }
        $cur_user = Sentinel::getUser();
        // dd($cur_user);
        $max_size = (int)ini_get('upload_max_filesize') * 1000000;
        $all_ext = implode(',', $this->allExtensions());
        $files = array();
        
        // try {
        //     foreach($request->file as $f) {
        //         $p_form = new Project_print_form();
        //         $p_form->name = $f->getClientOriginalName();
        //         $p_form->project_id = $request->project_id;
        //         $p_form->project_task_id = $request->task_id;
        //         $p_form->raw = 1;
        //         $p_form->observer_id = $cur_user->id;
        //         if ($request->isPrintForm == 1) {
        //             $p_form->is_printform = 1;
        //         } elseif ($request->isPrintForm == 2) {
        //             $p_form->is_screenform = 1;
        //         }
        //         if ($request->forPaid == 'true') {
        //             $p_form->free = 0;
        //         } else {
        //             $p_form->free = 1;
        //         }
        //         if ($request->paided == 'true') {
        //             $p_form->paid = 1;
        //         } else {
        //             $p_form->paid = 0;
        //         }
        //         $p_form->printform_status_ref_id = 5;
        //         $p_form->save();

        //         $comment = new Printform_comment();
        //         $comment->user_id = $cur_user->id;
        //         $comment->comment = $request->comment;
        //         $comment->form_id = $p_form->id;
        //         $comment->save();
                
                
        //         $file = $f;
        //         $name = explode('.', $file->getClientOriginalName());
        //         $ext = $file->getClientOriginalExtension();
        //         $type = $this->getType($ext);
        //         $rand = rand(100, 1000).'_'.$f->getClientOriginalName();
                
        //         // $upload = LGD::put($rand, file_get_contents($file->getRealPath()));
        //         $upload = Storage::disk('yandexcloud')->put($rand, file_get_contents($file->getRealPath()));

        //         if ($upload) {
        //             // $a = Storage::disk('yandexcloud')->get($rand);
        //             // dd($a);

        //             // $model::create([
        //             //     'file' => $rand,
        //             //     'project_print_form_id' => $p_form->id,
        //             //     'g_file' => ''
        //             // ]);
        //             $model = new Printform_file();
        //             $model->file = $rand;
        //             $model->project_print_form_id = $p_form->id;
        //             $model->g_file = '';
        //             $model->save();

        //             $files[] = [
        //                 'file' => $rand,
        //                 'project_print_form_id' => $p_form->id,
        //                 'g_file' => ''
        //             ];

        //             $success = true;
        //         }
        //     };

            
        // } catch (\Exception $e) {
        //     $success = false;
        // }

        foreach($request->file as $f) {
            $p_form = new Project_print_form();
            $p_form->name = $f->getClientOriginalName();
            $p_form->project_id = $request->project_id;
            $p_form->project_task_id = $request->task_id;
            $p_form->raw = 1;
            $p_form->observer_id = $cur_user->id;
            if ($request->isPrintForm == 1) {
                $p_form->is_printform = 1;
            } elseif ($request->isPrintForm == 2) {
                $p_form->is_screenform = 1;
            }
            if ($request->forPaid == 'true') {
                $p_form->free = 0;
            } else {
                $p_form->free = 1;
            }
            if ($request->paided == 'true') {
                $p_form->paid = 1;
            } else {
                $p_form->paid = 0;
            }
            $p_form->printform_status_ref_id = 5;
            $p_form->save();
            // dd($f);
            $comment = new Printform_comment();
            $comment->user_id = $cur_user->id;
            $comment->comment = $request->comment;
            $comment->form_id = $p_form->id;
            $comment->save();
            
            
            $file = $f;
            $name = explode('.', $file->getClientOriginalName());
            
            $ext = $file->getClientOriginalExtension();
            
            $type = $this->getType($ext);
            $rand = rand(100, 1000).'_'.$f->getClientOriginalName();
            // dd(file_get_contents($file->getRealPath()));
            // $upload = LGD::put($rand, file_get_contents($file->getRealPath()));
//            $upload = Storage::disk('yandexcloud')->put($rand, file_get_contents($file->getRealPath()));
            $fileAdapter =  new YandexFileAdapter();
            $upload = $fileAdapter->put($rand, file_get_contents($file->getRealPath()));
            // dd('Ok');
            if ($upload) {
                // $a = Storage::disk('yandexcloud')->get($rand);
                // dd($a);

                // $model::create([
                //     'file' => $rand,
                //     'project_print_form_id' => $p_form->id,
                //     'g_file' => ''
                // ]);
                $model = new Printform_file();
                $model->file = $rand;
                $model->project_print_form_id = $p_form->id;
                $model->g_file = '';
                $model->public_url = $upload['public_url'];
                $model->download_url = $upload['download_url'];
                $model->save();

                $files[] = [
                    'file' => $rand,
                    'project_print_form_id' => $p_form->id,
                    'g_file' => '',
                    'public_url' => $upload['public_url'],
                    'download_url' => $upload['download_url'],
                ];

                $success = true;
            }
        };


        if ($success == true) {
            broadcast(new addPrintFormFromProject($cur_user->first_name.' '.$cur_user->last_name));
            return response()->json([
                'status' => 'Ok',
                'files' => $files
            ]);
        }

        return response()->json(false);
    } 

    public function getForms(Request $request, $id) {
        $form = DB::select('select `pf`.`file`, `pf`.`g_file`
        from `project_print_forms` `ppf`
        left join `printform_files` `pf` on `pf`.`project_print_form_id` = `ppf`.`id`
        where `ppf`.`deleted_at` is null and
        `ppf`.`project_task_id` ='.$id);

        return response()->json($form);
    }

    public function storePrintForm(Request $request) {
        // print_r($request->all());
        // die;
        $cur_user = Sentinel::getUser();
        $max_size = (int)ini_get('upload_max_filesize') * 1000;
        $all_ext = implode(',', $this->allExtensions());

        $model = new Done_printform_file();
    
        $file = $request->file;
        $name = explode('.', $file->getClientOriginalName());
        $ext = $file->getClientOriginalExtension();
        $type = $this->getType($ext);
        $rand = rand(100, 1000).'_'.$request['name'];

//        $upload = LGD::put($rand, file_get_contents($file->getRealPath()));
//        $upload = LGD::put($rand, file_get_contents($file->getRealPath()));
        $fileAdapter =  new YandexFileAdapter();
        $upload = $fileAdapter->put($rand, file_get_contents($file->getRealPath()));
        if ($upload) {
//            return json_encode($upload['public_url']);
//            $a = $upload->getAllProperties();
            return $model::create([
                'file' => $rand,
                'project_print_form_id' => $request['form_id'],
                'g_file' => '',
                'public_url' => $upload['public_url'],
                'download_url' => $upload['download_url'],
            ]);
        }
    }

    /**
     * Edit specific file
     * @param integer $id   File Id
     * @param Request $request Request with form data: filename
     * @return boolean     True if success, otherwise - false
     */
    public function edit($id, Request $request)
    {
        $file = File::where('id', $id)->where('user_id', Auth::id())->first();

        if ($file->name == $request['name']) {
            return response()->json(false);
        }

        $this->validate($request, [
            'name' => 'required|unique:files'
        ]);

        $old_filename = '/public/' . $this->getUserDir() . '/' . $file->type . '/' . $file->name . '.' . $file->extension;
        $new_filename = '/public/' . $this->getUserDir() . '/' . $request['type'] . '/' . $request['name'] . '.' . $request['extension'];

        if (Storage::disk('local')->exists($old_filename)) {
            if (Storage::disk('local')->move($old_filename, $new_filename)) {
                $file->name = $request['name'];
                return response()->json($file->save());
            }
        }

        return response()->json(false);
    }


    /**
     * Delete file from disk and database
     * @param integer $id File Id
     * @return boolean   True if success, otherwise - false
     */
    public function destroy(Request $request, $id){
        $modelName = 'App\\' . $request->modelName;
        $file = (new $modelName())->find($id);
        if (!$file) {
            return ['message'=>'Файл не найден', 'status'=>'Error'];
        }
        $fileAdapter = new YandexFileAdapter();
        $fileAdapter->delete($file->file);
        $file->delete();
        return ['status'=>'Ok'];
    }

    /**
     * Get type by extension
     * @param string $ext Specific extension
     * @return string   Type
     */
    private function getType($ext)
    {
        if (in_array($ext, $this->image_ext)) {
            return 'image';
        }

        if (in_array($ext, $this->audio_ext)) {
            return 'audio';
        }

        if (in_array($ext, $this->video_ext)) {
            return 'video';
        }

        if (in_array($ext, $this->document_ext)) {
            return 'document';
        }

        return false;
    }

    /**
     * Get all extensions
     * @return array Extensions of all file types
     */
    private function allExtensions()
    {
        return array_merge($this->image_ext, $this->audio_ext, $this->video_ext, $this->document_ext);
    }

    /**
     * Get directory for the specific user
     * @return string Specific user directory
     */
    private function getUserDir()
    {
        return Auth::user()->name . '_' . Auth::id();
    }
}
