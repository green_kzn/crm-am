<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use App\Http\Controllers\PermissionsController as Permissions;
use Sentinel;
use App\User;
use App\Role_user;
use App\Role;

class GroupController extends Controller
{
    public function groups() {
        $cur_user = Sentinel::getUser();
        $role = Sentinel::getUser()->roles()->first()->name;

        $perm = new Permissions();

        $groups = Role::all();

        return view('admin.groups.groups', [
            'user' => $cur_user,
            'role' => $role,
            'groups' => $groups,
            'user_perm' => $perm->getAdminPermissions()[0],
            'group_perm' => $perm->getAdminPermissions()[1],
            'event_perm' => $perm->getAdminPermissions()[2],
        ]);
    }

    public function groupAdd() {
        $cur_user = Sentinel::getUser();
        $role = Sentinel::getUser()->roles()->first()->name;

        $perm = new Permissions();
        $roles = Role::all();

        return view('admin.groups.add', [
            'user' => $cur_user,
            'role' => $role,
            'user_perm' => $perm->getAdminPermissions()[0],
            'group_perm' => $perm->getAdminPermissions()[1],
            'event_perm' => $perm->getAdminPermissions()[2],
            'roles' => $roles,
        ]);
    }

    public function groupEdit($id) {
        $cur_user = Sentinel::getUser();
        $role = Sentinel::getUser()->roles()->first()->name;

        $perm = new Permissions();
        $roles = Role::all();

        $group = Role::find($id);

        return view('admin.groups.edit', [
            'user' => $cur_user,
            'role' => $role,
            'user_perm' => $perm->getAdminPermissions()[0],
            'group_perm' => $perm->getAdminPermissions()[1],
            'event_perm' => $perm->getAdminPermissions()[2],
            'roles' => $roles,
            'group' => $group
        ]);
    }

    public function groupUpdate(Request $request, $id) {
        $this->validate($request, [
            'group_name' => 'required|alpha',
            'group_slug' => 'required|alpha',
        ]);

        //TODO Обработать исключение
        try {
            $role = Sentinel::findRoleById($id)->update([
                'name' => $request->group_name,
                'slug' => $request->group_slug,
            ]);
        } catch (\QueryException $e) {
            return redirect()->back();
        }

        if ($role) {
            return redirect('/admin/groups')->with('success', 'Группа успешно обновлена');
        }
    }

    public function groupDel($id) {
        $users = Role_user::where('role_id', '=', $id)->get();

        if (count($users) > 0) {
            return redirect()->back()->with('error', 'Вы не можете удалить группу пока в ней есть пользователи');
        } else {
            $role = Role::find($id);
            $role->delete();
            return redirect()->back()->with('success', 'Группа успешно удалена');
        }
    }

    public function postGroupAdd(Request $request) {
        $this->validate($request, [
            'group_name' => 'required|alpha',
            'group_slug' => 'required|alpha',
        ]);

        //TODO Обработать исключение
        try {
            $role = Sentinel::getRoleRepository()->createModel()->create([
                'name' => $request->group_name,
                'slug' => $request->group_slug,
            ]);
        } catch (\QueryException $e) {
            return redirect()->back();
        }

        if ($role) {
            return redirect('/admin/groups')->with('success', 'Группа успешно создана');
        }

    }
}
