<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project_task;
use App\Project_print_form;

class AdmAMDateTimeController extends Controller
{
    static public function startProjectDate($startDate) {
        $nowDate = date("Y-m-d");
        if ($nowDate > $startDate) {
            return false;
        } else {
            return true;
        }
    }

    static public function finishProjectDate($finishDate) {
        $nowDate = date("Y-m-d");
        if ($nowDate > $finishDate) {
            return false;
        } else {
            return true;
        }
    }

    public function calculate(Request $request) {
        if ($request->ajax()) {
            $p_form = Project_print_form::where('printform_status_ref_id', '!=', 5)
                ->whereNull('deleted_at')
                ->get();

            $hour = 0;
            if (count($p_form) > 0) {
                foreach ($p_form as $pf) {
                    $hour += $pf->max_hours;
                }
                $res = ($hour / 8) / 2;

                $cur_date = date("j").'.'.date("n").'.'.date("Y");
                $date = date('Y-m-d', strtotime($cur_date . ' +'.round($res).' day'));
                //$tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +'.round($res).' day'));
            } else {
                $date = date("Y").'-'.date("m").'-'.date("d");
            }

            return response()->json($date);
        }
    }

    static public function calc($new_hour = 0) {
        $p_form = Project_print_form::where('printform_status_ref_id', '!=', 5)
            ->whereNull('deleted_at')
            ->get();

        $hour = 0;
        if (count($p_form) > 0) {
            foreach ($p_form as $pf) {
                $hour += $pf->max_hours;
            }
            $res = (($hour + $new_hour) / 8) / 2;

            $cur_date = date("j").'.'.date("n").'.'.date("Y");
            $date = date('Y-m-d', strtotime($cur_date . ' +'.round($res).' day'));
            //$tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +'.round($res).' day'));
        } else {
            $date = date("Y").'-'.date("m").'-'.date("d");
        }

        return $date;
    }
}
