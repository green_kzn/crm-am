<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\PermissionsController as Permissions;
use App\Http\Controllers\Admin\AdmEventController as EventController;
use App\Http\Controllers\Controller;
use Sentinel;
use App\Task;
use App\User;
use App\Event;

class AdmTasksController extends Controller
{
    public function index() {
        $cur_user = Sentinel::getUser();
        $role = Sentinel::getUser()->roles()->first()->name;

        $perm = new Permissions();

        $tasks = Task::where('contractor_id', '=', $cur_user->id)
            ->whereNULL('deleted_at')
            ->get();

        return view('admin.tasks.tasks', [
            'user' => $cur_user,
            'role' => $role,
            'user_perm' => $perm->getAdminPermissions()[0],
            'group_perm' => $perm->getAdminPermissions()[1],
            'event_perm' => $perm->getAdminPermissions()[2],
            'tasks' => $tasks,
        ]);
    }

    public function addTask() {
        $cur_user = Sentinel::getUser();
        $role = Sentinel::getUser()->roles()->first()->name;

        $perm = new Permissions();

        $users = User::all();

        return view('admin.tasks.add', [
            'user' => $cur_user,
            'role' => $role,
            'user_perm' => $perm->getAdminPermissions()[0],
            'group_perm' => $perm->getAdminPermissions()[1],
            'event_perm' => $perm->getAdminPermissions()[2],
            'users' => $users
        ]);
    }

    public function getTasks(Request $request) {
        if ($request->ajax()) {
            $cur_user = Sentinel::getUser();
            $tasks = Task::where('contractor_id', '=', $cur_user->id)->whereNULL('deleted_at')->get();
            $events = EventController::getUserEvents($cur_user->id);
            foreach ($tasks as $task) {
                $json[] = array(
                    'id' => $task->id,
                    'title' => $task->title,
                    'start' => $task->start_date.' '.$task->start_time,
                    'end' => $task->end_date,
                    'allDay' => false,
                    'color' => $task->color,
                    'url' => '/admin/task/edit/'.$task->id
                );
            }
            foreach ($events as $event) {
                $json[] = array(
                    'id' => $event->id,
                    'title' => $event->title,
                    'start' => $event->start_date.' '.$event->start_time,
                    'allDay' => false,
                    'color' => $event->color,
                    'url' => '/admin/event/edit/'.$task->id
                );
            }

            return response()->json($json);
        }
    }

    public function postAddEvent() {
        echo "Ok";
    }
}
