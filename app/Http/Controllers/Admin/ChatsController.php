<?php

namespace App\Http\Controllers\Admin;

use App\Message;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\MessageSent;
use Sentinel;
use Pusher;

class ChatsController extends Controller
{
    /**
     * Show chats
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pusher');
    }

    /**
     * Fetch all messages
     *
     * @return Message
     */
    public function fetchMessages()
    {
        $messages = Message::whereNull('deleted_at')
            ->get();
        foreach ($messages as $m) {
            $user = User::where('id', '=', $m->user_id)
                ->whereNull('deleted_at')
                ->first();
            $mes[] = [
                'message' => $m->message,
                'user' => $user
            ];
        }
        return $mes;
    }

    /**
     * Persist message to database
     *
     * @param  Request $request
     * @return Response
     */
    public function sendMessage(Request $request)
    {
        //dd($request->message);
        $cur_user = Sentinel::getUser();
        $user = User::find($cur_user->id);

        /*$message = $user->messages()->create([
            'message' => '456'
        ]);*/

        $message = new Message();
        $message->user_id = $cur_user->id;
        $message->message = $request->message;
        $message->save();

        $options = array(
            'cluster' => 'ap1',
            'encrypted' => true
        );
        $pusher = new Pusher(
            '41add183dc4e4f40251b',
            '0ec099d5be276067a0fd',
            '448390',
            $options
        );

        broadcast(new MessageSent($user, $message));
        $data['message'] = $request->message;
        $data['user'] = $user;
        $pusher->trigger('chat', 'MessageSent', $data);

        return ['status' => 'Message Sent!'];
    }
}
