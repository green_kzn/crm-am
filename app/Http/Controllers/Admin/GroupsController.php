<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use Role;

class GroupsController extends Controller
{
    public function getGroups(Request $request) {
        $groups = Role::all();
        return response()->json($groups);
    }
}
