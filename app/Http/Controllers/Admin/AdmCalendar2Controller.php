<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use App\Http\Controllers\AdmPermissionsController as Permissions;
use App\Http\Controllers\Admin\AdmProjectsController as Projects;
use App\Http\Controllers\Controller;
use Sentinel;
use App\Project_task;
use App\Module_rule;
use App\Project_task_contact;
use App\Project_additional_task;
use App\Project_print_form;
use App\Printform_file;
use Storage;

class AdmCalendar2Controller extends Controller
{
    public function index() {
        $perm = session('perm');

        if ($perm['calendar.view']) {
            $cur_user = Sentinel::getUser();

            /*$event = Event::where('user_id', '=', $cur_user->id)
                ->whereNULL('deleted_at')
                ->get();*/
            
            return view('admin.calendars.calendar', [
                //'events' => $event,
                'perm' => $perm
            ]);
        } else {
            abort(503);
        }
    }

    public function addEvent(Request $request) {
        if (!$request->nameEvent) {
            return redirect()->back()->with('error', 'Не указано название события');
        }
        if (!$request->color) {
            return redirect()->back()->with('error', 'Не выбран цвет');
        }

        $cur_user = Sentinel::getUser();

        $event = new Event();
        $event->name = $request->nameEvent;
        $event->color = $request->color;
        $event->user_id = $cur_user->id;

        if ($event->save()) {
            return redirect()->back();
        } else {
            return redirect()->back()->with('error', 'Событие не сохранено');
        }
    }

    public function delEvent(Request $request, $id) {
        $event = Event::find($id);
        $event->deleted_at = date("Y-m-d H:i:s");
        if ($event->save()) {
            return redirect()->back();
        } else {
            return redirect()->back()->with('error', 'Ошибка при попытке удаления события');
        }
    }

    public function addProjectTask(Request $request) {
        $input_date = strtotime($request->date_cont.' '.$request->time_cont);
        $curdate = strtotime(date("Y-m-d H:i"));

        if ($input_date >= $curdate) {
            $endTime = strtotime($request->time) + strtotime('00:59');

            $count = Project_task_contact::where('end', '>=', $request->date_cont.' '.$request->time_cont)
                ->where('user_id', '=', $request->user)
                ->where('next_contact', 'like', '%'.$request->date_cont.'%')
                //->where('tasksstatus_ref_id', '!=', 2)
                ->whereNull('deleted_at')
                ->get();
            //dd($request->date_cont.' '.$request->time_cont);

            if (count($count) == 0) {
                switch ($request->type) {
                    case 'task':
                        //$project_task_contact = Project_task_contact::find($request->tid);
                        $p_task = Project_task::find($request->tid);
                        $m_rule = Module_rule::find($p_task->module_rule_id);
                        $pid = Projects::getProjectByTaskId($request->tid);

                        $res = strtotime($request->time_cont) + strtotime(date('H:i:s',$endTime)) -strtotime("00:00:00");
                        $time = date('H:i:s',$res);

                        $project_task_contact = new Project_task_contact();
                        $project_task_contact->next_contact = $request->date_cont.' '.$request->time_cont;
                        $project_task_contact->user_id = $request->user;
                        $project_task_contact->project_id = $pid->id;
                        $project_task_contact->project_task_id = $request->tid;
                        $project_task_contact->end = $request->date_cont.' '. $time;

                        if ($project_task_contact->save()) {
                            return redirect()->back()->with('success', 'Данные успешно обновлены');
                        } else {
                            return redirect()->back()->with('error', 'Произошла ошибка при обновлении данных');
                        }

                        /*if ($m_rule->grouptasks_id != 11) {
                            $pid = Projects::getProjectByTaskId($request->tid);
                            $project_task_contact = new Project_task_contact();
                            $project_task_contact->next_contact = $request->date.' '.$request->time;
                            $project_task_contact->user_id = $request->user_id;
                            $project_task_contact->project_id = $pid->id;
                            $project_task_contact->project_task_id = $request->tid;
                            $project_task_contact->end = $request->date.' '.date('H:i:s',$endTime);

                            if ($project_task_contact->save()) {
                                return response()->json(['status' => 'Ok']);
                            } else {
                                return redirect()->back()->with('error', 'Ошибка при попытке удаления события');
                            }
                        /*} else {
                            $pid = Projects::getProjectByTaskId($request->tid);
                            $project_task_contact = new Project_task_contact();
                            $project_task_contact->next_contact = $request->date.' '.$request->time;
                            $project_task_contact->user_id = $request->user_id;
                            $project_task_contact->status = 2;
                            $project_task_contact->project_id = $pid->id;
                            $project_task_contact->project_task_id = $request->tid;
                            $project_task_contact->end = $request->date.' '.date('H:i:s',$endTime);

                            if ($project_task_contact->save()) {
                                return response()->json(['status' => 'Ok']);
                            } else {
                                return redirect()->back()->with('error', 'Ошибка при попытке удаления события');
                            }
                        }*/
                        break;
                    case 'print_form':
                        //dd($request->file('img'));
                        $cur_user = Sentinel::getUser();

                        try {
                            $p_form = new Project_print_form();
                            $p_form->name = $request->formName;
                            $p_form->project_id = $request->pid;
                            $p_form->raw = 1;
                            $p_form->observer_id = $cur_user->id;
                            $p_form->is_printform = 1;
                            $p_form->printform_status_ref_id = 6;
                            $p_form->save();

                            $success = true;
                        } catch (\Exception $e) {
                            $success = false;
                        }

                        if ($request->hasFile('img')) {
                            $file = $request->file('img');
                            foreach ($file as $f) {
                                if ($request->hasFile('img')) {
                                    //$f->move($destinationPath, $filename);
                                    $content = file_get_contents($f);
                                    $t = Storage::disk('local')->put($f->getClientOriginalName(), $content);

                                    $pf_file = new Printform_file();
                                    $pf_file->file = $f->getClientOriginalName();
                                    $pf_file->project_print_form_id = $p_form->id;
                                    $pf_file->save();
                                }
                            }
                        }

                        if ($success) {
                            return redirect()->back()->with('success', 'Данные успешно обновлены');
                        }else {
                            return redirect()->back()->with('error', 'Произошла ошибка при обновлении данных');
                        }

                        break;
                    case 'screen_form':
                        $cur_user = Sentinel::getUser();

                        try {
                            $p_form = new Project_print_form();
                            $p_form->name = $request->formName;
                            $p_form->project_id = $request->pid;
                            $p_form->raw = 1;
                            $p_form->observer_id = $cur_user->id;
                            $p_form->is_screenform = 1;
                            $p_form->printform_status_ref_id = 6;
                            $p_form->save();

                            $success = true;
                        } catch (\Exception $e) {
                            $success = false;
                        }

                        if ($request->hasFile('img2')) {
                            $file = $request->file('img2');
                            foreach ($file as $f) {
                                if ($request->hasFile('img2')) {
                                    //$f->move($destinationPath, $filename);
                                    $content = file_get_contents($f);
                                    $t = Storage::disk('local')->put($f->getClientOriginalName(), $content);

                                    $pf_file = new Printform_file();
                                    $pf_file->file = $f->getClientOriginalName();
                                    $pf_file->project_print_form_id = $p_form->id;
                                    $pf_file->save();
                                }
                            }
                        }

                        if ($success) {
                            return redirect()->back()->with('success', 'Данные успешно обновлены');
                        }else {
                            return redirect()->back()->with('error', 'Произошла ошибка при обновлении данных');
                        }
                        break;
                }
            } else {
                return redirect()->back()->with('error', 'Ошибка! на данную дату/время уже назначен контакт');
            }
        } else {
            return redirect()->back()->with('error', 'Ошибка! введенная дата/время меньше текущей');
        }
    }

    public function addProjectAdditionalTask(Request $request) {
        if ($request->ajax()) {
            $endTime = strtotime($request->time) + strtotime('01:00');
            $count = Project_additional_task::where('end', '>', $request->date.' '.$request->time)
                ->where('tasksstatus_ref_id', '!=', 2)
                ->whereNull('deleted_at')
                ->count();

            if ($count == 0) {
                $project_task = Project_additional_task::find($request->tid);
                $project_task->next_contact = $request->date.' '.$request->time;
                $project_task->end = $request->date.' '.date('H:i:s',$endTime);

                if ($project_task->save()) {
                    return response()->json(['status' => 'Ok']);
                } else {
                    return redirect()->back()->with('error', 'Ошибка при попытке удаления события');
                }
            } else {
                return response()->json(['status' => 'error', 'error' => 'Ошибка! На данную дату/время уже запланирована задача']);
            }
        }
    }
}
