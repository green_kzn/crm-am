<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use App\Role;

class AdmRightController extends Controller
{
    public function rights() {
        if (session('perm')['right.view']) {
            $groups = Role::all();
            foreach ($groups as $g) {
                $role[] = Sentinel::findRoleBySlug($g->slug);
            }

            foreach ($role as $r) {
                $perm[$r->slug] = $r->permissions;
            }

            foreach ($perm as $k => $p) {
                foreach ($p as $k2 => $p2) {
                    $ar[] = explode('.', $k2);
                    foreach ($ar as $i) {
                        $right[$k][$i[0]][$i[1]] = '';
                    }
                }
            }

            return view('admin.rights.rights', [
                'groups' => $groups,
                'right' => $perm
            ]);
        } else {
            abort(503);
        }
    }


    public function rightsSave(Request $request) {
        foreach (session('perm') as $k => $perm) {
            if ($request->has(str_replace('.', '_', $k))) {
                $res[$k] = true;
            } else {
                $res[$k] = false;
            }
        }

        $role = Role::where('slug', '=', $request->slug)
            ->firstOrFail();

        $role->permissions = json_encode($res);

        if ($role->save()) {
            return redirect()->back()->with('success', 'Права успешно обновлены');
        } else {
            return redirect()->back()->with('error', 'Произошла ошибка при обновлении прав');
        }
    }
}
