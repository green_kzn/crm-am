<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Sentinel;
use Activation;
use Mail;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\PermissionsController as Permissions;
use App\Http\Controllers\Admin\AdmProjectsController as Projects;
use App\Http\Controllers\Controller;
use App\User;
use App\Role_user;
use App\Events\onAddNewUser;
use App\Role;
use App\Mail\ActivationAccount;
use Illuminate\Database\QueryException;

class AdmUser2Controller extends Controller
{
    public function users() {
        if (session('perm')['user.view']) {
            $users = User::whereNull('users.deleted_at')
                ->join('role_users', 'users.id', '=', 'role_users.user_id')
                ->join('roles', 'role_users.role_id', '=', 'roles.id')
                ->select('users.id', 'roles.name as role', 'users.email', 'users.is_ban', 'users.foto', 'users.first_name', 'users.last_name')
                ->get();
            foreach ($users as $u) {
                //$u['role'] = Role_user::getByUserId($u->id);
                if ($u->is_ban == 1)
                    $u['status'] = 1;
                else
                    $u['status'] = 0;
            }

            foreach ($users as $u) {
                if (Activation::completed($u))
                    $u['active'] = 1;
                else
                    $u['active'] = 0;
            }

            return view('admin.users.users', [
                'users' => $users,
            ]);
        } else {
            abort(503);
        }
    }

    public function switchStatus(Request $request) {
        if ($request->ajax()) {
            $user = User::where('id', '=', $request->id)->firstOrFail();
            if ($request->status == 1)
                $user->is_ban = 0;
            else
                $user->is_ban = 1;

            if ($user->save()) {
                $resp = [
                    'status' => 1
                ];
            } else {
                $resp = [
                    'status' => 0
                ];
            }

            return $resp;
        }
    }

    public function userDel($id) {
        if (session('perm')['user.delete']) {
            $user = Sentinel::findById($id);
            if ($user->delete()) {
                return redirect()->back()->with('success', 'Пользователь успешно удален');
            } else {
                return redirect()->back()->with('error', 'Произошла ошибка при удалении пользователя');
            }
        } else {
            abort(503);
        }
    }

    public function add() {
        if (session('perm')['user.create']) {
            $roles = Role::all();

            return view('admin.users.add', [
                'roles' => $roles,
            ]);
        } else {
            abort(503);
        }
    }

    public function userEdit($id) {
        if (session('perm')['user.update']) {
            $roles = Role::all();

            $u = User::find($id);
            $user_role = Role_user::where('user_id', '=', $u->id)->first();
            $u_role = Sentinel::findRoleById($user_role->role_id);
            return view('admin.users.edit', [
                'roles' => $roles,
                'u' => $u,
                'u_role' => $u_role
            ]);
        } else {
            abort(503);
        }
    }

    //TODO Доделать загрузку фото
    public function upload($img) {
        $photoName = time().'.'.$img->getClientOriginalExtension();
        $foto = $img->move(public_path('avatars'), $photoName);

        return $photoName;
    }

    public function store(Request $request) {
        $credentials = [
            'email'    => $request->user_email,
            'password' => $request->user_pass,
            'last_name' => $request->user_surname,
            'first_name' => $request->user_name
        ];
        try {
            $user = Sentinel::register($credentials);
        } catch (QueryException $ex) {
            return redirect()->back()->with(['error' => 'Ошибка при добавлении пользователя']);
        }

        if ($request->img) {
            $upload = $this->upload($request->file('img'));
            $new_user = User::where('id', '=', $user->id)->firstOrFail();
            $new_user->foto = $upload;
            $new_user->save();
        } else {
            $new_user = User::where('id', '=', $user->id)->firstOrFail();
            $new_user->foto = 'camera_200.png';
            $new_user->save();
        }

        $activation = Activation::create($user);
        $role = Sentinel::findRoleById($request->user_role);
        $role->users()->attach($user);

        $cur_user = Sentinel::getUser();

        Mail::to($user)->send(new ActivationAccount($new_user, $activation['code']));
        event(new onAddNewUser($cur_user, $user));

        return redirect('/admin/users')->with('success', 'Пользователь успешно добавлен');
    }

    public function userUpdate($id, Request $request) {
        $user = Sentinel::findById($id);
        $credentials = [
            'email' => $request->inputEmail,
            'first_name' => $request->inputName,
            'last_name' => $request->inputLastName,
            'password' => $request->inputPassword
        ];

        if (Sentinel::validForUpdate($user, $credentials)) {
            $user = Sentinel::update($user, $credentials);
            $role = Role_user::where('user_id', '=', $user->id)->first();
            $role->role_id = $request->user_role;
            $role->save();
            return redirect('/admin/users')->with(['success' => 'Данные о пользователе успешно обновлены']);
        } else {
            return redirect()->back()->with(['error' => 'Ошибка при обновлении данных пользователя']);
        }
    }

    public function getUserInfo($id) {
        $user = User::whereNull('users.deleted_at')
            ->join('role_users', 'users.id', '=', 'role_users.user_id')
            ->join('roles', 'role_users.role_id', '=', 'roles.id')
            ->where('users.id', '=', $id)
            ->select('users.id', 'roles.name as role', 'users.email', 'users.is_ban', 'users.foto', 'users.first_name', 'users.last_name')
            ->first();

        //dd($user);
        return view('admin.users.user', [
            'user' => $user
        ]);
    }

    static public function getAllUsers() {
        $users = User::whereNull('deleted_at')
            ->where('id', '!=', 0)
            ->get();
        return $users;
    }

    static public function getUserById ($id) {
        $user = User::where('id', '=', $id)
            ->whereNull('deleted_at')
            ->get();
        return $user[0];
    }

    public function getUserProjects(Request $request) {
        if ($request->ajax()) {
            $projects = Projects::getUserProjects($request->id);
            $tasks = Projects::getUserTasks($request->id);
            $allTimeTask = 0;
            foreach ($tasks as $t) {
                $allTimeTask += $t['number_contacts'];
            }

            return response()->json(['projects' => count($projects), 'tasks' => $allTimeTask]);
        }

    }

    public function getUser(Request $request) {
        if ($request->ajax()) {
            $users = User::whereNull('deleted_at')
                ->where('id', '!=', 0)
                ->get();
            return response()->json($users);
        }
    }
}
