<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Sentinel;
use Activation;
use Mail;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\PermissionsController as Permissions;
use App\Http\Controllers\Controller;
use App\User;
use App\Role_user;
use App\Role;

class AdmUserController extends Controller
{
    public function users() {
        $cur_user = Sentinel::getUser();
        $role = Sentinel::getUser()->roles()->first()->name;

        $perm = new Permissions();

        $users = User::all();
        foreach ($users as $u) {
            $u['role'] = Role_user::getByUserId($u->id);
            if ($u->is_ban == 1)
                $u['status'] = 1;
            else
                $u['status'] = 0;
        }

        foreach ($users as $u) {
            if (Activation::completed($u))
                $u['active'] = 1;
            else
                $u['active'] = 0;
        }

        return view('admin.users.users', [
            'user' => $cur_user,
            'role' => $role,
            'user_perm' => $perm->getAdminPermissions()[0],
            'group_perm' => $perm->getAdminPermissions()[1],
            'event_perm' => $perm->getAdminPermissions()[2],
            'users' => $users,
        ]);
    }

    public function switchStatus(Request $request) {
        if ($request->ajax()) {
            $user = User::where('id', '=', $request->id)->firstOrFail();
            if ($request->status == 1)
                $user->is_ban = 0;
            else
                $user->is_ban = 1;

            if ($user->save()) {
                $resp = [
                    'status' => 1
                ];
            } else {
                $resp = [
                    'status' => 0
                ];
            }

            return $resp;
        }
    }

    public function userDel($id) {
        $user = Sentinel::findById($id);
        if ($user->delete()) {
            return redirect()->back()->with('success', 'Пользователь успешно удален');
        } else {
            return redirect()->back()->with('error', 'Произошла ошибка при удалении пользователя');
        }
    }

    public function add() {
        $cur_user = Sentinel::getUser();
        $role = Sentinel::getUser()->roles()->first()->name;

        $perm = new Permissions();
        $roles = Role::all();

        return view('admin.users.add', [
            'user' => $cur_user,
            'role' => $role,
            'user_perm' => $perm->getAdminPermissions()[0],
            'group_perm' => $perm->getAdminPermissions()[1],
            'event_perm' => $perm->getAdminPermissions()[2],
            'roles' => $roles,
        ]);
    }

    public function userEdit($id) {
        $cur_user = Sentinel::getUser();
        $role = Sentinel::getUser()->roles()->first()->name;

        $perm = new Permissions();
        $roles = Role::all();

        $u = User::find($id);

        return view('admin.users.edit', [
            'user' => $cur_user,
            'role' => $role,
            'user_perm' => $perm->getAdminPermissions()[0],
            'group_perm' => $perm->getAdminPermissions()[1],
            'event_perm' => $perm->getAdminPermissions()[2],
            'roles' => $roles,
            'u' => $u
        ]);
    }

    //TODO Доделать загрузку фото
    public function upload($img) {
        $photoName = time().'.'.$img->getClientOriginalExtension();
        $foto = $img->move(public_path('avatars'), $photoName);

        return $photoName;
    }

    public function store(Request $request) {
        $this->validate($request, [
            'user_name' => 'required|alpha',
            'user_surname' => 'required|alpha',
            'user_email' => 'required|email',
            'user_pass' => 'required',
        ]);

        $credentials = [
            'email'    => $request->user_email,
            'password' => $request->user_pass,
            'last_name' => $request->user_surname,
            'first_name' => $request->user_name
        ];
        $user = Sentinel::register($credentials);
        if ($request->img) {
            $upload = $this->upload($request->file('img'));
            $new_user = User::where('id', '=', $user->id)->firstOrFail();
            $new_user->foto = $upload;
            $new_user->save();
        } else {
            $new_user = User::where('id', '=', $user->id)->firstOrFail();
            $new_user->foto = 'camera_200.png';
            $new_user->save();
        }

        $activation = Activation::create($user);
        $role = Sentinel::findRoleById($request->user_role);
        $role->users()->attach($user);

        return redirect('/admin/users')->with('success', 'Пользователь успешно добавлен');
    }

    public function userUpdate($id, Request $request) {
        dd($request->all());
    }

    static public function getAllUsers() {
        $users = User::whereNull('deleted_at')
            ->where('id', '!=', 0)
            ->get();
        return $users;
    }
}
