<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\PermissionsController as Permissions;
use App\Http\Controllers\Controller;
use Sentinel;

class AdmCalendarController extends Controller
{
    public function __construct() {
        $perm = new Permissions();
        $cur_user = Sentinel::getUser();
        foreach ($perm->getAdminPermissions() as $k => $p) {
            if ($k == 'calendar') {
                if ($p['calendar_view'] != 1) {
                    return abort(503);
                }
            }
        }
    }

    public function index() {
        $cur_user = Sentinel::getUser();
        $role = Sentinel::getUser()->roles()->first()->name;

        $perm = new Permissions();

        //$event = Event::where('user_id', '=', $cur_user->id)->whereNULL('deleted_at')->get();

        return view('admin.calendars.calendar', [
            'user' => $cur_user,
            'role' => $role,
            'user_perm' => $perm->getAdminPermissions()['user'],
            'group_perm' => $perm->getAdminPermissions()['group'],
            'event_perm' => $perm->getAdminPermissions()['event'],
            'calendar_perm' => $perm->getAdminPermissions()['calendar'],
            //'events' => $event
        ]);
    }

    public function addEvent(Request $request) {
        if (!$request->nameEvent) {
            return redirect()->back()->with('error', 'Не указано название события');
        }
        if (!$request->color) {
            return redirect()->back()->with('error', 'Не выбран цвет');
        }

        $cur_user = Sentinel::getUser();

        /*$event = new Event();
        $event->name = $request->nameEvent;
        $event->color = $request->color;
        $event->user_id = $cur_user->id;

        if ($event->save()) {
            return redirect()->back();
        } else {
            return redirect()->back()->with('error', 'Событие не сохранено');
        }*/
    }

    public function delEvent(Request $request, $id) {
        /*$event = Event::find($id);
        $event->deleted_at = date("Y-m-d H:i:s");
        if ($event->save()) {
            return redirect()->back();
        } else {
            return redirect()->back()->with('error', 'Ошибка при попытке удаления события');
        }*/
    }
}
