<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use App\Http\Controllers\PermissionsController as Permissions;
use App\Http\Controllers\Admin\AdmUser2Controller as Users;
use App\User;
use Cache;
use Mail;
use App\Mail\ActivationAccount;
use App\History;

class AdminController extends Controller
{
    public function admin(Request $request) {
        return view('admin.dashboard');
    }

    public function mail() {
        $to      = 'yamaletdinov.rusl@yandex.ru';
        $user = User::find(7);
        Mail::to($user)->send(new ActivationAccount());
    }
}
