<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdmDocController extends Controller
{
    public function index() {
        if (session('perm')['doc.view']) {
            return view('admin.doc.doc');
        } else {
            abort(503);
        }
    }
}
