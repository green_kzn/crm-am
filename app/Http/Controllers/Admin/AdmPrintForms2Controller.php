<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\AdmProjects2Controller as Projects;
use App\Http\Controllers\Admin\AdmComment2Controller as Comments;
use App\Http\Controllers\Admin\AdmUser2Controller as Users;
use App\Http\Controllers\Admin\AdmClients2Controller as Clients;
use App\Http\Controllers\Admin\AdmCitys2Controller as Citys;
use App\Http\Controllers\Admin\AdmAMDateTimeController as AMDateTime;
use App\Project_task;
use App\Printform_history;
use App\Project_print_form;
use App\Category_printform_reference;
use App\Project_task_contact;
use App\Printform_file;
use App\Module_rule;
use App\Comment;
use App\Printform_status_reference;
use Sentinel;
use Storage;
use App\Project;
use Carbon\Carbon;

class AdmPrintForms2Controller extends Controller
{
    public function index() {
        $cur_user = Sentinel::getUser();

        $p_forms_raw = Project_print_form::whereNull('deleted_at')
            ->where('observer_id', '=', $cur_user->id)
            ->where('printform_status_ref_id', '=', 8)
            ->get();

        if (count($p_forms_raw) > 0) {
            foreach ($p_forms_raw as $fr) {
                $project = Projects::getProjectById($fr->project_id);
                $client = Clients::getClientById($project->client_id);
                $observer = Users::getUserById($fr->observer_id);
                $status = Printform_status_reference::find($fr->printform_status_ref_id);
                //dd($project);

                $form_raw[] = [
                    'id' => $fr->id,
                    'name' => $fr->name,
                    'client' => $client->name,
                    'project' => $project->name,
                    'status' => $status->name,
                    'observer' => $observer->last_name . ' ' . $observer->first_name,
                ];
            }
        } else {
            $form_raw[] = '';
        }

        $p_forms_date = Project_print_form::whereNull('deleted_at')
            ->whereNull('raw')
            ->orderByDesc('date_implement')
            ->get();

        //dd($p_forms_date);

        if (count($p_forms_date) > 0) {
            foreach ($p_forms_date as $pf) {
                $project = Projects::getProjectById($pf->project_id);
                $client = Clients::getClientById($project->client_id);
                $city = Citys::getCityById($client->city_id);
                if ($pf->user_id) {
                    $user = Users::getUserById($pf->user_id);
                } else {
                    $user = '';
                }
                $observer = Users::getUserById($pf->observer_id);
                $status = Printform_status_reference::find($pf->printform_status_ref_id);
                //dd($project);

                if ($pf->user_id) {
                    $form_date[] = [
                        'id' => $pf->id,
                        'max_hours' => $pf->max_hours,
                        'prev_contact' => $pf->prev_contact,
                        'name' => $pf->name,
                        'client' => $client->name,
                        'project' => $project->name,
                        'city' => $city,
                        'user' => $user->last_name . ' ' . $user->first_name,
                        'is_free' => $pf->is_free,
                        'next_contact' => $pf->next_contact,
                        'date' => $pf->created_at,
                        'date_implement' => $pf->date_implement,
                        'date_implement_for_client' => $pf->date_implement_for_client,
                        'status' => $status->name,
                        'observer' => $observer->last_name . ' ' . $observer->first_name,
                        'number' => $pf->number,
                    ];
                } else {
                    $form_date[] = [
                        'id' => $pf->id,
                        'max_hours' => $pf->max_hours,
                        'prev_contact' => $pf->prev_contact,
                        'name' => $pf->name,
                        'client' => $client->name,
                        'project' => $project->name,
                        'city' => $city,
                        'user' => '',
                        'is_free' => $pf->is_free,
                        'next_contact' => $pf->next_contact,
                        'date' => $pf->created_at,
                        'date_implement' => $pf->date_implement,
                        'date_implement_for_client' => $pf->date_implement_for_client,
                        'status' => $status->name,
                        'observer' => $observer->last_name . ' ' . $observer->first_name,
                        'number' => $pf->number,
                    ];
                }
            }
        } else {
            $form_date[] = '';
        }
        //dd($form);

        $p_forms_number = Project_print_form::whereNull('deleted_at')
            ->whereNull('raw')
            ->orderBy('number', 'asc')
            ->get();

        //dd($p_forms);

        if (count($p_forms_number) > 0) {
            foreach ($p_forms_number as $pf) {
                $project = Projects::getProjectById($pf->project_id);
                $client = Clients::getClientById($project->client_id);
                $city = Citys::getCityById($client->city_id);
                if ($pf->user_id) {
                    $user = Users::getUserById($pf->user_id);
                } else {
                    $user = '';
                }
                $observer = Users::getUserById($pf->observer_id);
                $status = Printform_status_reference::find($pf->printform_status_ref_id);
                //dd($client);

                if ($pf->user_id) {
                    $form_number[] = [
                        'id' => $pf->id,
                        'max_hours' => $pf->max_hours,
                        'prev_contact' => $pf->prev_contact,
                        'name' => $pf->name,
                        'client' => $client->name,
                        'project' => $project->name,
                        'city' => $city,
                        'user' => $user->last_name . ' ' . $user->first_name,
                        'is_free' => $pf->is_free,
                        'next_contact' => $pf->next_contact,
                        'date' => $pf->created_at,
                        'date_implement' => $pf->date_implement,
                        'date_implement_for_client' => $pf->date_implement_for_client,
                        'status' => $status->name,
                        'observer' => $observer->last_name . ' ' . $observer->first_name,
                        'number' => $pf->number,
                    ];
                } else {
                    $form_number[] = [
                        'id' => $pf->id,
                        'max_hours' => $pf->max_hours,
                        'prev_contact' => $pf->prev_contact,
                        'name' => $pf->name,
                        'client' => $client->name,
                        'project' => $project->name,
                        'city' => $city,
                        'user' => '',
                        'is_free' => $pf->is_free,
                        'next_contact' => $pf->next_contact,
                        'date' => $pf->created_at,
                        'date_implement' => $pf->date_implement,
                        'date_implement_for_client' => $pf->date_implement_for_client,
                        'status' => $status->name,
                        'observer' => $observer->last_name . ' ' . $observer->first_name,
                        'number' => $pf->number,
                    ];
                }
            }
        } else {
            $form_number[] = '';
        }

        //dd($form_date);
        return view('admin.printforms.printforms', [
            'tasks_date' => $form_date,
            'tasks_number' => $form_number,
            'form_raw' => $form_raw
        ]);
    }

    public function add() {
        $count_tasks = Project_print_form::whereNull('deleted_at')
            ->max('number');

        $category = Category_printform_reference::whereNull('deleted_at')
            ->get();

        $status = Printform_status_reference::whereNull('deleted_at')
            ->get();

        return view('admin.printforms.add', [
            'count_task' => $count_tasks,
            'users' => Users::getAllUsers(),
            'clients' => Clients::getAllClients(),
            'projects' => Projects::getAllProjects(),
            'category' => $category,
            'status' => $status
        ]);
    }

    public function calc($new_hour = 0) {
        $p_form = Project_print_form::where('printform_status_ref_id', '!=', 5)
            ->whereNull('deleted_at')
            ->get();

        $hour = 0;
        if (count($p_form) > 0) {
            foreach ($p_form as $pf) {
                $hour += $pf->max_hours;
            }
            $res = (($hour + $new_hour) / 8) / 2;

            $cur_date = date("j").'.'.date("n").'.'.date("Y");
            $date = date('Y-m-d', strtotime($cur_date . ' +'.round($res).' day'));
            //$tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +'.round($res).' day'));
        } else {
            $date = date("Y").'-'.date("m").'-'.date("d");
        }

        return $date;
    }

    public function postAdd(Request $request) {
        switch ($request->typeTask) {
            case 'introduction':
                if (!$request->taskName_introduction) {
                    return redirect()->back()->with('error', 'Укажите название задачи');
                }
                if (!$request->dateFinish_introduction) {
                    return redirect()->back()->with('error', 'Укажите дату реализации');
                }
                if (!$request->dateFinishForCustomers_introduction) {
                    return redirect()->back()->with('error', 'Укажите дату реализации для заказчика');
                }

                $count = Project_print_form::where('number', '=', $request->queue_introduction)
                    ->whereNull('deleted_at')
                    ->count();
                //dd($count);

                if ($count > 0) {
                    $number = Project_print_form::where('number', '>=', $request->queue_introduction)
                        ->whereNull('deleted_at')
                        ->get();

                    $new_date = $this->calc((int)$request->queue_introduction);

                    foreach ($number as $n) {
                        $new_number = Project_print_form::find($n->id);
                        $new_number->number += 1;

                        /*$res = ($request->maxCountHours_introduction / 8) / 2;
                        $cur_date = date("j").'.'.date("n").'.'.date("Y");
                        $date = date('Y-m-d', strtotime($request->dateFinish_introduction . ' +'.round($res).' day'));

                        dd($date);*/

                        $new_number->save();
                    }

                    if ($request->comment) {
                        $date =  explode(' ', date("Y-m-d H:i:s"));
                        $cur_user = Sentinel::getUser();
                        try {
                            $comment = new Comment();
                            $comment->project_id = $request->projectName_introduction;
                            $comment->user_id = $cur_user->id;
                            $comment->for_client = NULL;
                            $comment->close = 0;
                            $comment->date = $new_date;
                            $comment->time = $date[1];
                            $comment->text = $request->comment;
                            $comment->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }
                    }

                    try {
                        $p_form = new Project_print_form();
                        $p_form->number = $request->queue_introduction;
                        $p_form->project_id = $request->projectName_introduction;
                        $p_form->user_id = $request->user_introduction;
                        $p_form->observer_id = $request->implementer_introduction;
                        $p_form->name = $request->taskName_introduction;
                        $p_form->category_printform_ref_id = $request->category_introduction;
                        $p_form->max_hours = $request->maxCountHours_introduction;
                        $p_form->printform_status_ref_id = $request->status_introduction;
                        $p_form->date_implement = $request->dateFinish_introduction;
                        $p_form->date_implement_for_client = $request->dateFinishForCustomers_introduction;
                        if ($request->isPrintForm_introduction == 'on') {
                            $p_form->is_printform = 1;
                        } else {
                            $p_form->is_printform = 0;
                        }
                        if (isset($request->forPaid_introduction)) {
                            $p_form->is_free = 0;
                        } else {
                            $p_form->is_free = 1;
                        }
                        if (isset($request->paided_introduction)) {
                            $p_form->paid = 1;
                        } else {
                            $p_form->paid = 0;
                        }
                        $p_form->save();


                        $success = true;
                    } catch (\Exception $e) {
                        $success = false;
                    }

                    if ($request->hasFile('img')) {
                        $file = $request->file('img');
                        foreach ($file as $f) {
                            if ($request->hasFile('img')) {
                                //$f->move($destinationPath, $filename);
                                $content = file_get_contents($f);
                                $t = Storage::disk('local')->put($f->getClientOriginalName(), $content);

                                $pf_file = new Printform_file();
                                $pf_file->file = $f->getClientOriginalName();
                                $pf_file->project_print_form_id = $p_form->id;
                                $pf_file->save();
                            }
                        }
                    }
                } else {
                    if ($request->comment) {
                        $date =  explode(' ', date("Y-m-d H:i:s"));
                        $cur_user = Sentinel::getUser();
                        try {
                            $comment = new Comment();
                            $comment->project_id = $request->projectName_introduction;
                            $comment->user_id = $cur_user->id;
                            $comment->for_client = NULL;
                            $comment->close = 0;
                            $comment->date = $date[0];
                            $comment->time = $date[1];
                            $comment->text = $request->comment;
                            $comment->save();

                            $this->success = true;
                        } catch (\Exception $e) {
                            $this->success = false;
                        }
                    }
                    
                    try {
                        $p_form = new Project_print_form();
                        $p_form->number = $request->queue_introduction;
                        $p_form->project_id = $request->projectName_introduction;
                        $p_form->user_id = $request->user_introduction;
                        $p_form->observer_id = $request->implementer_introduction;
                        $p_form->name = $request->taskName_introduction;
                        $p_form->category_printform_ref_id = $request->category_introduction;
                        $p_form->max_hours = $request->maxCountHours_introduction;
                        $p_form->printform_status_ref_id = $request->status_introduction;
                        $p_form->date_implement = $request->dateFinish_introduction;
                        $p_form->date_implement_for_client = $request->dateFinishForCustomers_introduction;
                        if ($request->isPrintForm_introduction == 'on') {
                            $p_form->is_printform = 1;
                        } else {
                            $p_form->is_printform = 0;
                        }
                        if (isset($request->forPaid_introduction)) {
                            $p_form->is_free = 1;
                        } else {
                            $p_form->is_free = 0;
                        }
                        if (isset($request->paided_introduction)) {
                            $p_form->paid = 1;
                        } else {
                            $p_form->paid = 0;
                        }
                        $p_form->save();

                        $success = true;
                    } catch (\Exception $e) {
                        $success = false;
                    }

                    if ($request->hasFile('img')) {
                        $file = $request->file('img');
                        foreach ($file as $f) {
                            if ($request->hasFile('img')) {
                                //$f->move($destinationPath, $filename);
                                $content = file_get_contents($f);
                                Storage::disk('local')->put($f->getClientOriginalName(), $content);

                                $pf_file = new Printform_file();
                                $pf_file->file = $f->getClientOriginalName();
                                $pf_file->project_print_form_id = $p_form->id;
                                $pf_file->save();
                            }
                        }
                    }
                }

                if ($success) {
                    return redirect('/admin/print-form')->with('success', 'Данные успешно сохранены');
                } else {
                    return redirect('/admin/print-form')->with('error', 'Произошла ошибка при сохранении данных');
                }

                break;
        }
    }

    public function upload(Request $request){
        dd($request->all());
        $page= new Page($request->except('img'));
        if ($request->hasFile('img')) {
            $file = $request->file('img');
            $destinationPath =  public_path().'/house/uploads/';
            $filename = str_random(20) .'.' . $file->getClientOriginalExtension() ?: 'png';
            $page->img = $filename;
            if ($request->hasFile('img')) {
                $request->file('img')->move($destinationPath, $filename);
            }
        }
        $page->save();

        return redirect()->route('page.index');
    }

    public function categoryRef() {
        $category = Category_printform_reference::whereNull('deleted_at')
            ->get();

        return view('admin.references.print_form.print_form', [
            'category' => $category
        ]);
    }

    public function addCategoryRef() {
        return view('admin.references.print_form.add');
    }

    public function postAddCategoryRef(Request $request) {
        if (!$request->name_category) {
            return redirect()->back()->with('error', 'Укажите категорию');
        }

        if (!$request->norm_category) {
            return redirect()->back()->with('error', 'Укажите норму для категории');
        }

        try {
            $category = new Category_printform_reference();
            $category->category = $request->name_category;
            $category->norm = $request->norm_category;
            $category->save();

            $success = true;
        } catch (\Exception $e) {
            $success = false;
        }

        if ($success) {
            return redirect('/admin/printform-category')->with('success', 'Данные успешно сохранены');
        } else {
            return redirect('/admin/printform-category')->with('error', 'Произошла ошибка при сохранении данных');
        }
    }

    public function delCategoryRef($id) {
        if (!$id) {
            return redirect()->back()->with('error', 'Произошла ошибка при сохранении данных');
        }

        try {
            $category = Category_printform_reference::find($id);
            $category->deleted_at = date("Y-m-d H:i:s");
            $category->save();

            $success = true;
        } catch (\Exception $e) {
            $success = false;
        }

        if ($success) {
            return redirect()->back()->with('success', 'Данные успешно сохранены');
        } else {
            return redirect()->back()->with('error', 'Произошла ошибка при сохранении данных');
        }
    }

    public function editCategoryRef($id) {
        $category = Category_printform_reference::find($id);

        return view('admin.references.print_form.edit', [
            'category' => $category
        ]);
    }

    public function postEdittCategoryRef(Request $request, $id) {
        if (!$id) {
            return redirect()->back()->with('error', 'Произошла ошибка при сохранении данных');
        }

        if (!$request->name_category) {
            return redirect()->back()->with('error', 'Укажите категорию');
        }

        if (!$request->norm_category) {
            return redirect()->back()->with('error', 'Укажите норму для категории');
        }

        try {
            $category = Category_printform_reference::find($id);
            $category->category = $request->name_category;
            $category->norm = $request->norm_category;
            $category->save();

            $success = true;
        } catch (\Exception $e) {
            $success = false;
        }

        if ($success) {
            return redirect('/admin/printform-category')->with('success', 'Данные успешно сохранены');
        } else {
            return redirect('/admin/printform-category')->with('error', 'Произошла ошибка при сохранении данных');
        }
    }

    public function getCategoryNorm(Request $request) {
        if ($request->ajax()) {
            $norm = Category_printform_reference::find($request->category);
            return response()->json($norm->norm);
        }
    }

    public function edit(Request $request, $id) {
        $f_task = Project_print_form::where('id', '=', $id)
            ->whereNull('deleted_at')
            ->first();
        $status = Printform_status_reference::find($f_task->printform_status_ref_id);
        $project = Projects::getProjectById($f_task->project_id);
        $client = Clients::getClientById($project->client_id);
        $category = Category_printform_reference::where('id', '=', $f_task->category_printform_ref_id)
            ->whereNull('deleted_at')
            ->first();
        $user = Users::getUserById($f_task->user_id);
        $implement = Users::getUserById($f_task->observer_id);
        $comments = Comments::getCommentsByProjectId($f_task->project_id);
        $file = Printform_file::where('project_print_form_id', '=', $id)
            ->whereNull('deleted_at')
            ->get();
        /*foreach ($file as $f) {
            $filePath[] = Storage::disk('google')->getIds($f->file);
        }*/
        if (count($file) > 0) {
            foreach ($file as $f) {
                $url = Storage::disk('local')->url($f->file);
                $size = Storage::size($f->file);
                $files[] = [
                    'name' => $f->file,
                    'img' => $url,
                    'size' => $size
                ];
            }
        } else {
            $files = '';
        }
        //dd($files);

        return view('admin.printforms.edit', [
            'form' => $f_task,
            'status' => $status->name,
            'project' => $project,
            'client' => $client,
            'category' => $category,
            'user' => $user,
            'implement' => $implement,
            'comments' => $comments,
            'file' => $files
        ]);
    }

    public function delete(Request $request, $id) {
        $f_task = Project_print_form::find($id);

        $f_task->deleted_at = Carbon::now();

        if ($f_task->save()) {
            return redirect()->back()->with('success', 'Данные успешно обновлены');
        } else {
            return redirect()->back()->with('error', 'Произошла ошибка при удалении данных');
        }
    }

    public function saveField(Request $request) {
        if ($request->ajax()) {
            $form = Project_print_form::find($request->fid);
            $form->number = $request->new_number;
            $form->printform_status_ref_id = $request->new_status;
            $form->project_id = $request->new_project_id;
            $form->name = $request->new_form_name;
            if ($request->new_is_form == 'off') {
                $form->is_printform = 0;
            } else {
                $form->is_printform = 1;
            }
            $form->category_printform_ref_id = $request->new_category;
            $form->max_hours = $request->new_max_hour;
            $form->user_id = $request->new_user;
            $form->date_implement = $request->new_date_implement.' 00:00:00';
            $form->date_implement_for_client = $request->new_date_implement_for_client.' 00:00:00';
            $form->observer_id = $request->new_implement;
            if ($request->new_is_paid == 'off') {
                $form->is_free = 0;
            } else {
                $form->is_free = 1;
            }
            if ($request->new_ia_paided == 'off') {
                $form->paid = 0;
            } else {
                $form->paid = 1;
            }

            if ($form->save()) {
                return response()->json("Ok");
            } else {
                return response()->json("No");
            }
        }
    }

    public function getStatus(Request $request) {
        if ($request->ajax()) {
            $status = Printform_status_reference::whereNull('deleted_at')->get();
            return response()->json($status);
        }
    }

    public function getCategory(Request $request) {
        if ($request->ajax()) {
            $category = Category_printform_reference::whereNull('deleted_at')->get();
            return response()->json($category);
        }
    }

    public function calculate(Request $request) {
        if ($request->ajax()) {
            $p_form = Project_print_form::where('printform_status_ref_id', '!=', 5)
                ->whereNull('deleted_at')
                ->get();

            $hour = 0;
            if (count($p_form) > 0) {
                foreach ($p_form as $pf) {
                    $hour += $pf->max_hours;
                }
                $res = ($hour / 8) / 2;

                $cur_date = date("j").'.'.date("n").'.'.date("Y");
                $date = date('Y-m-d', strtotime($cur_date . ' +'.round($res).' day'));
                //$tomorrow_val = date('Y-m-d', strtotime($cur_date . ' +'.round($res).' day'));
            } else {
                $date = date("Y").'-'.date("m").'-'.date("d");
            }

            for($i = 1; $i <= date("t"); $i++) {
                $weekend = date("w",strtotime($i.'.01.'.date("Y")));
                if($weekend==0 || $weekend==6) {
                    if ($i<10) {
                        $w_date[] = date("Y-01-").'0'.$i;
                    } else {
                        $w_date[] = date("Y-01-").$i;
                    }
                };
            };

            foreach ($w_date as $wd) {
                for ($j=0; $j<=28; $j++) {
                    $d = date('Y-m-d', strtotime($date . ' +'.$j.' day'));
                    if ($d == $wd) {
                        continue;
                    } else {
                        $res = $d;
                        break;
                    }
                }
            }

            return response()->json($res);
        }
    }

    public function raw($id) {
        $f_task = Project_print_form::where('id', '=', $id)
            ->whereNull('deleted_at')
            ->first();
        //dd($f_task);
        $statuses = Printform_status_reference::whereNull('deleted_at')
            ->get();
        $status = Printform_status_reference::find($f_task->printform_status_ref_id);
        $project = Projects::getProjectById($f_task->project_id);
        $client = Clients::getClientById($project->client_id);
        $implement = Users::getUserById($f_task->observer_id);
        $comments = Comments::getCommentsByProjectId($f_task->project_id);
        $category = Category_printform_reference::whereNull('deleted_at')
            ->get();
        $file = Printform_file::where('project_print_form_id', '=', $id)
            ->whereNull('deleted_at')
            ->get();
        $count_tasks = Project_print_form::whereNull('deleted_at')
            ->max('number');
        /*foreach ($file as $f) {
            $filePath[] = Storage::disk('local')->getIds($f->file);
        }*/
        if (count($file) > 0) {
            foreach ($file as $f) {
                $url = Storage::disk('local')->url($f->file);
                $size = Storage::size($f->file);
                $files[] = [
                    'name' => $f->file,
                    'img' => $url,
                    'size' => $size
                ];
            }
        } else {
            $files = '';
        }

        return view('admin.printforms.raw',[
            'count_task' => $count_tasks,
            'form' => $f_task,
            'date_implement' => substr($f_task->date_implement, 0, -9),
            'date_implement_for_client' => substr($f_task->date_implement_for_client, 0, -9),
            'status' => $status->id,
            'projects' => Projects::getClientProjects($client->id),
            'project' => $project,
            'client' => $client,
            'clients' => Clients::getAllClients(),
            'implement' => $implement,
            'comments' => $comments,
            'file' => $files,
            'statuses' => $statuses,
            'category' => $category,
            'users' => Users::getAllUsers(),
        ]);
    }

    public function postRaw(Request $request, $id) {
        try {
            $f_task = Project_print_form::where('id', '=', $id)
                ->whereNull('deleted_at')
                ->first();
            $f_task->raw = NULL;
            $f_task->number = $request->queue_introduction;
            $f_task->project_id = $request->projectName_introduction;
            $f_task->user_id = $request->user_introduction;
            $f_task->observer_id = $request->implementer_introduction;
            $f_task->name = $request->taskName_introduction;
            $f_task->category_printform_ref_id = $request->category_introduction;
            $f_task->max_hours = $request->maxCountHours_introduction;
            $f_task->printform_status_ref_id = $request->status_introduction;
            $f_task->date_implement = $request->dateFinish_introduction;
            $f_task->date_implement_for_client = $request->dateFinishForCustomers_introduction;
            if ($request->isPrintForm_introduction == 'on') {
                $f_task->is_printform = 1;
            } else {
                $f_task->is_printform = 0;
            }
            if (isset($request->forPaid_introduction)) {
                $f_task->is_free = 0;
            } else {
                $f_task->is_free = 1;
            }
            if (isset($request->paided_introduction)) {
                $f_task->paid = 1;
            } else {
                $f_task->paid = 0;
            }
            $f_task->save();

            $success = true;
        } catch (\Exception $e) {
            $success = false;
        }

        if ($success) {
            return redirect('/admin/print-form')->with('success', 'Данные успешно сохранены');
        } else {
            return redirect('/admin/print-form')->with('error', 'Произошла ошибка при сохранении данных');
        }
    }

    public function getFormHistory(Request $request) {
        if ($request->ajax()) {
            $f_history = Printform_history::where('printform_id', '=', $request->id)
                ->whereNull('deleted_at')
                ->get();

            if (count($f_history) > 0) {
                foreach ($f_history as $h) {
                    $user = Users::getUserById($h->user_id);
                    $status = Printform_status_reference::find($h->printform_status_ref_id);
                    $comment = Comment::find($h->comment_id);
                    $history[] = [
                        'date' => Carbon::parse($h->created_at)->format('d.m.Y H:m'),
                        'user' => $user,
                        'status' => $status->name,
                        'comment' => $comment
                    ];
                }
            } else {
                $history = '';
            }

            return response()->json($history);
        }
    }
}
