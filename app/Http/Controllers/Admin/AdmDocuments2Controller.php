<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Doc_types_reference;
use App\Documents_reference;
use App\Projects_doc;

class AdmDocuments2Controller extends Controller
{
    public function index() {
        if (session('perm')['documents_ref.view']) {
            $types_q = Doc_types_reference::whereNull('deleted_at')
                ->get();
            foreach ($types_q as $t) {
                $types[] = [
                    'id' => $t->id,
                    'slug' => $t->slug,
                    'name' => $t->name,
                ];
            }

            $documents_q_input = Documents_reference::whereNull('deleted_at')
                ->where('type_id', '=', 1)
                ->get();

            if (count($documents_q_input) > 0) {
                foreach ($documents_q_input as $dq) {
                    $documents_input[] = [
                        'id' => $dq->id,
                        'name' => $dq->name,
                        'type_id' => $dq->type_id,
                        'is_first' => $dq->is_first,
                        'for_all_projects' => $dq->for_all_projects,
                        'days_for_execute' => $dq->days_for_execute,
                        'days_for_execute' => $dq->days_for_execute,
                    ];
                }
            } else {
                $documents_input = '';
            }

            $documents_q_output = Documents_reference::whereNull('deleted_at')
                ->where('type_id', '=', 2)
                ->get();

            if (count($documents_q_output) > 0) {
                foreach ($documents_q_output as $dq) {
                    $documents_output[] = [
                        'id' => $dq->id,
                        'name' => $dq->name,
                        'type_id' => $dq->type_id,
                        'is_first' => $dq->is_first,
                        'for_all_projects' => $dq->for_all_projects,
                        'days_for_execute' => $dq->days_for_execute,
                        'days_for_execute' => $dq->days_for_execute,
                    ];
                }
            } else {
                $documents_output = '';
            }

            return view('admin.references.documents.documents', [
                'types' => $types,
                'documents_input' => $documents_input,
                'documents_output' => $documents_output,
            ]);
        } else {
            abort(503);
        }
    }

    public function add() {
        if (session('perm')['documents_ref.create']) {
            $type = Doc_types_reference::whereNull('deleted_at')
                ->get();

            return view('admin.references.documents.add', [
                'type' => $type
            ]);
        } else {
            abort(503);
        }
    }

    public function postAdd(Request $request) {
        if (session('perm')['documents_ref.create']) {
            if ($request->name_doc == '') {
                return redirect()->back()->with(['error' => 'Укажите название документа']);
            }

            $doc_type_id = Doc_types_reference::where('slug', '=', $request->typeDoc)
                ->whereNull('deleted_at')
                ->firstOrFail();

            if ($request->isFirstDoc == 'on') {
                $first = Documents_reference::where('type_id', '=', $doc_type_id->id)
                    ->whereNull('deleted_at')
                    ->where('is_first', '=', 1)
                    ->first();

                if ($first) {
                    return redirect()->back()->with(['error' => 'Самый первый документ уже существует']);
                }
            }

            $doc = Documents_reference::where('name', '=', $request->name_doc)
                ->where('type_id', '=', $doc_type_id->id)
                ->whereNull('deleted_at')
                ->get();

            //dd(count($doc));
            if (count($doc) > 0) {
                return redirect()->back()->with(['error' => 'Документ с таким названием уже существует']);
            } else {
                $new_doc = new Documents_reference();
                $new_doc->name = $request->name_doc;
                $new_doc->type_id = $doc_type_id->id;
                if ($request->isFirstDoc == 'on') {
                    $new_doc->is_first = 1;
                    $new_doc->sort = 1;
                } else {
                    $new_doc->is_first = 0;
                    $new_doc->sort = 0;
                }
                if ($request->forAllProjects == 'on') {
                    $new_doc->for_all_projects = 1;
                } else {
                    $new_doc->for_all_projects = 0;
                }
                $new_doc->days_for_execute = $request->days_doc;

                if ($new_doc->save()) {
                    return redirect('/admin/documents')->with(['success' => 'Документ успешно сохранен']);
                } else {
                    return redirect('/admin/documents')->with(['error' => 'Ошибка при сохранении документа']);
                }
            }
        } else {
            abort(503);
        }
    }

    public function del(Request $request, $id) {
        if (session('perm')['documents_ref.delete']) {
            $doc = Documents_reference::find($id);
            $doc->deleted_at = date("Y-m-d H:i:s");

            if ($doc->save()) {
                return redirect()->back()->with(['success' => 'Документ успешно удален']);
            } else {
                return redirect()->back()->with(['error' => 'Ошибка при удалении документа']);
            }
        } else {
            abort(503);
        }
    }

    public function update($id) {
        if (session('perm')['documents_ref.update']) {
            $type = Doc_types_reference::whereNull('deleted_at')
                ->get();

            $doc = Documents_reference::find($id);
            $cur_type = Doc_types_reference::find($doc->type_id);

            return view('admin.references.documents.edit', [
                'type' => $type,
                'doc' => $doc,
                'cur_type' => $cur_type
            ]);
        } else {
            abort(503);
        }
    }

    public function postUpdate(Request $request, $id) {
        if (session('perm')['documents_ref.update']) {
            if ($request->name_doc == '') {
                return redirect()->back()->with(['error' => 'Укажите название документа']);
            }

            $doc_type_id = Doc_types_reference::where('slug', '=', $request->typeDoc)
                ->whereNull('deleted_at')
                ->firstOrFail();

            if ($request->isFirstDoc == 'on') {
                $first = Documents_reference::where('type_id', '=', $doc_type_id->id)
                    ->whereNull('deleted_at')
                    ->where('is_first', '=', 1)
                    ->first();

                if ($first->id != $request->doc_id) {
                    return redirect()->back()->with(['error' => 'Самый первый документ уже существует']);
                }
            }

            $doc_ar = json_decode($request->doc_ar);
            //dd($doc_ar);
            foreach ($doc_ar as $d) {
                if ($d->id != 1) {
                    //dd($d);
                    $doc = Documents_reference::find($d->doc);
                    $doc->sort = $d->id;
                    $doc->save();
                };
            }

            /*$doc = Documents_reference::where('name', '=', $request->name_doc)
                ->where('type_id', '=', $doc_type_id->id)
                ->whereNull('deleted_at')
                ->get();*/


            $new_doc = Documents_reference::find($id);
            $new_doc->name = $request->name_doc;
            $new_doc->type_id = $doc_type_id->id;
            if ($request->isFirstDoc == 'on') {
                $new_doc->is_first = 1;
                $new_doc->sort = 1;
            } else {
                $new_doc->is_first = 0;
                $new_doc->sort = 0;
            }
            if ($request->forAllProjects == 'on') {
                $new_doc->for_all_projects = 1;
            } else {
                $new_doc->for_all_projects = 0;
            }

            if ($new_doc->save()) {
                return redirect('/admin/documents')->with(['success' => 'Документ успешно обновлен']);
            } else {
                return redirect('/admin/documents')->with(['error' => 'Ошибка при обновлении документа']);
            }
        } else {
            abort(503);
        }
    }

    public function getTypeDoc(Request $request) {
        if ($request->ajax()) {
            $cur_type_id = Documents_reference::find($request->doc_id);

            $type_id = Doc_types_reference::where('slug', '=', $request->next_type)
                ->whereNull('deleted_at')
                ->firstOrFail();

            if ($cur_type_id['type_id'] == $type_id->id) {
                $doc = Documents_reference::where('type_id', '=', $type_id->id)
                    ->whereNull('deleted_at')
                    ->where('is_first', '!=', 1)
                    ->get();
            } else {
                $doc = Documents_reference::where('type_id', '=', $type_id->id)
                    ->whereNull('deleted_at')
                    ->get();
            }

            return response()->json($doc);
        }
    }

    static public function getDocumentsForAllProjects() {
        $doc = Documents_reference::where('for_all_projects', '=', 1)
            ->whereNull('deleted_at')
            ->get();

        foreach ($doc as $d) {
            $doc_type = Doc_types_reference::find($d->type_id);
            $resp[] = [
                'id' => $d->id,
                'name' => $d->name,
                'type' => $doc_type->name,
                'isFirst' => $d->is_first
            ];
        }

        return $resp;
    }

    static public function getDocTypeByName($name) {
        $type = Doc_types_reference::where('name', '=', $name)
            ->whereNull('deleted_at')
            ->first();

        return $type;
    }

    static public function getDocumentByName($name) {
        $doc = Documents_reference::where('name', '=', $name)
            ->whereNull('deleted_at')
            ->first();

        return $doc;
    }

    static public function getAllDocumentsType() {
        $type = Doc_types_reference::whereNull('deleted_at')
            ->get();

        return $type;
    }

    static public function getProjectDocsByType($type, $project_id) {
        $p_docs = Projects_doc::where('type_id', '=', $type)
            ->where('project_id', '=', $project_id)
            ->whereNull('deleted_at')
            ->get();

        if (count($p_docs) > 0) {
            foreach ($p_docs as $pd) {
                $doc = Documents_reference::find($pd->doc_id);
                $docs[] = [
                    'id' => $pd->id,
                    'name' => $doc->name,
                ];
            }
        } else {
            $docs = [];
        }

        return $docs;
    }

    static public function isFirstDoc($name) {
        $isFirst = Documents_reference::where('name', '=', $name)
            ->where('is_first', '=', 1)
            ->get();

        if (count($isFirst) > 0) {
            return true;
        } else {
            return false;
        }
    }
}
