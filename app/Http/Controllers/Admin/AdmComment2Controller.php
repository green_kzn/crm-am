<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Comment;
use App\User;
use Sentinel;

class AdmComment2Controller extends Controller
{
    public static function getCommentsByProjectId($id) {
        $comments_q = Comment::where('project_id', '=', $id)
            ->orderBy('created_at', 'desc')
            ->whereNULL('deleted_at')
            ->get();

        if (count($comments_q) > 0) {
            foreach ($comments_q as $c) {
                $user = User::find($c->user_id);
                $cur_user = Sentinel::getUser();
                
                if ($c->for_client != NULL) {
                    if ($user->id == $cur_user->id) {
                        $comments[] = [
                            'text' => strip_tags($c->text),
                            'date' => $c->date,
                            'time' => $c->time,
                            'id' => $c->id,
                            'for_client' => 1,
                            'user_name' => $user->first_name,
                            'user_surname' => $user->last_name,
                            'user_foto' => $user->foto,
                            'autor' => true
                        ];
                    } else {
                        $comments[] = [
                            'text' => strip_tags($c->text),
                            'date' => $c->date,
                            'time' => $c->time,
                            'id' => $c->id,
                            'for_client' => 1,
                            'user_name' => $user->first_name,
                            'user_surname' => $user->last_name,
                            'user_foto' => $user->foto,
                            'autor' => false
                        ];
                    }
                } else {
                    if ($user['id'] == $cur_user->id) {
                        $comments[] = [
                            'text' => strip_tags($c->text),
                            'date' => $c->date,
                            'time' => $c->time,
                            'id' => $c->id,
                            'for_client' => 0,
                            'user_name' => $user->first_name,
                            'user_surname' => $user->last_name,
                            'user_foto' => $user->foto,
                            'autor' => true
                        ];
                    } else {
                        $comments[] = [
                            'text' => strip_tags($c->text),
                            'date' => $c->date,
                            'time' => $c->time,
                            'id' => $c->id,
                            'for_client' => 0,
                            'user_name' => $user['first_name'],
                            'user_surname' => $user['last_name'],
                            'user_foto' => $user['foto'],
                            'autor' => false
                        ];
                    }
                }
            }
        } else {
            $comments = '';
        }
        
        return $comments;
    }

    public function add(Request $request) {
        if ($request->ajax()) {
            $cur_user = Sentinel::getUser();
            $comment = new Comment();
            $comment->project_id = $request->project_id;
            $comment->user_id = $cur_user->id;
            $comment->text = $request->text;
            $comment->date = date("Y-m-d");
            $comment->time = date("H:i:s");

            if ($comment->save()) {
                return redirect()->back()->with('success', 'Коментарий успешно добавлен');
            } else {
                return redirect()->back()->with('error', 'Произошла ошибка при добавлении коментария');
            }
        }
    }

    public function del($id) {
        $comment = Comment::find($id);
        $comment->deleted_at = date("Y-m-d H:i:s");

        if ($comment->save()) {
            return redirect()->back()->with('success', 'Коментарий успешно удален');
        } else {
            return redirect()->back()->with('error', 'Произошла ошибка при удалении коментария');
        }
    }

    static public function getProjectCloseComment($pid) {
        $comment = Comment::where('project_id', '=', $pid)
            ->where('close', '=', 1)
            ->whereNull('deleted_at')
            ->get();

        return $comment;
    }
}
