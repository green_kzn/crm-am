<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tasksstatus_reference;

class AdmTasksStatusController extends Controller
{
    public function index() {
        if (session('perm')['status_task_ref.view']) {
            $tstatus = Tasksstatus_reference::whereNULL('deleted_at')
                ->get();

            return view('admin.references.tasks_status.tasks_status', [
                'tstatus' => $tstatus
            ]);
        } else {
            abort(503);
        }
    }

    public function add() {
        if (session('perm')['status_task_ref.create']) {
            return view('admin.references.tasks_status.add');
        } else {
            abort(503);
        }
    }

    public function edit($id) {
        if (session('perm')['status_task_ref.update']) {
            $tstatus = Tasksstatus_reference::find($id);

            return view('admin.references.tasks_status.edit', [
                'tstatus' => $tstatus
            ]);
        } else {
            abort(503);
        }
    }

    public function postAdd(Request $request) {
        if (session('perm')['status_task_ref.create']) {
            if (!$request->name_tstatus) {
                return redirect()->back()->with('error', 'Введите название модуля');
            } else {
                $tstatus = new Tasksstatus_reference();
                $tstatus->name = $request->name_tstatus;

                if ($tstatus->save()) {
                    return redirect('/admin/tasks-status')->with('success', 'Модуль успешно добавлен');
                } else {
                    return redirect('/admin/tasks-status')->with('error', 'Произошла ошибка при добавлении модуля');
                }
            }
        } else {
            abort(503);
        }
    }

    public function postEdit(Request $request, $id) {
        if (session('perm')['status_task_ref.update']) {
            if (!$request->name_tstatus) {
                return redirect()->back()->with('error', 'Введите название модуля');
            } else {
                $tstatus = Tasksstatus_reference::find($id);
                $tstatus->name = $request->name_tstatus;

                if ($tstatus->save()) {
                    return redirect('/admin/tasks-status')->with('success', 'Модуль успешно обновлена');
                } else {
                    return redirect('/admin/tasks-status')->with('error', 'Произошла ошибка при обновлении модуля');
                }
            }
        } else {
            abort(503);
        }
    }

    public function del($id) {
        if (session('perm')['status_task_ref.delete']) {
            $tstatus = Tasksstatus_reference::find($id);
            $tstatus->deleted_at = date("Y-m-d H:i:s");

            if ($tstatus->save()) {
                return redirect('/admin/tasks-status')->with('success', 'Модуль успешно удалена');
            } else {
                return redirect('/admin/tasks-status')->with('error', 'Произошла ошибка при удалении модуля');
            }
        } else {
            abort(503);
        }
    }
}
