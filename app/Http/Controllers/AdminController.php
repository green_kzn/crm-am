<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use App\Http\Controllers\PermissionsController as Permissions;
use App\User;
use App\Review;

class AdminController extends Controller
{
    private function getMinReviewDate() {
        $reviews = Review::all('created_at');
        foreach ($reviews as $r) {
            $date[] = $r->created_at;
        }
        $min = min($date);
        $date = date_parse_from_format('Y-m-d H:i:s', $min);
        
        switch ($date['month']) {
            case '01': $m = 'Январь'; break;
            case '02': $m = 'Февраль'; break;
            case '03': $m = 'Март'; break;
            case '04': $m = 'Апрель'; break;
            case '05': $m = 'Май'; break;
            case '06': $m = 'Июнь'; break;
            case '07': $m = 'Июль'; break;
            case '08': $m = 'Август'; break;
            case '09': $m = 'Сентябрь'; break;
            case '10': $m = 'Октябрь'; break;
            case '11': $m = 'Ноябрь'; break;
            case '12': $m = 'Декабрь'; break;
        }

        return $date['day'].' '.$m.' '.$date['year'];
    }

    private function getCurrentDate() {
        $d = date("d");
        $month = date("m");
        switch ($month) {
            case '01': $m = 'Январь'; break;
            case '02': $m = 'Февраль'; break;
            case '03': $m = 'Март'; break;
            case '04': $m = 'Апрель'; break;
            case '05': $m = 'Май'; break;
            case '06': $m = 'Июнь'; break;
            case '07': $m = 'Июль'; break;
            case '08': $m = 'Август'; break;
            case '09': $m = 'Сентябрь'; break;
            case '10': $m = 'Октябрь'; break;
            case '11': $m = 'Ноябрь'; break;
            case '12': $m = 'Декабрь'; break;
        }
        $y = date("Y");
        $date = $d.' '.$m.' '.$y;
        return $date;
    }

    public function admin() {
        $user = Sentinel::getUser();
        $role = Sentinel::getUser()->roles()->first()->name;

        $perm = new Permissions();

        $bad = 0;
        $soso = 0;
        $normally = 0;
        $well = 0;
        $famously = 0;

        $reviews = Review::all();
        foreach ($reviews as $r) {
            switch ($r->mark) {
                case '1':
                    $bad ++; break;
                case '2':
                    $soso ++; break;
                case '3':
                    $normally ++; break;
                case '4':
                    $well ++; break;
                case '5':
                    $famously ++; break;
            }
        }

        return view('admin.dashboard', [
            'user' => $user,
            'role' => $role,
            'user_perm' => $perm->getAdminPermissions()[0],
            'group_perm' => $perm->getAdminPermissions()[1],
            'event_perm' => $perm->getAdminPermissions()[2],
            'reviews' => $reviews,
            'bad' => $bad,
            'soso' => $soso,
            'normally' => $normally,
            'well' => $well,
            'famously' => $famously,
            'cur_date' => $this->getCurrentDate(),
            'min_date' => $this->getMinReviewDate()
        ]);
    }
}
