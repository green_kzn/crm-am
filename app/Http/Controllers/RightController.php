<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\PermissionsController as Permissions;
use Sentinel;
use App\User;
use App\Role_user;
use App\Role;

class RightController extends Controller
{
    public function rights() {
        $cur_user = Sentinel::getUser();
        $role = Sentinel::getUser()->roles()->first()->name;

        $perm = new Permissions();

        $groups = Role::all();

        return view('admin.rights.rights', [
            'user' => $cur_user,
            'role' => $role,
            'groups' => $groups,
            'user_perm' => $perm->getAdminPermissions()[0],
            'group_perm' => $perm->getAdminPermissions()[1],
            'event_perm' => $perm->getAdminPermissions()[2],
        ]);
    }
}
