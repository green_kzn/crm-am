<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendImpRepMail extends Mailable
{
    use Queueable, SerializesModels;

    public $comments;
    public $date;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($comments, $date)
    {
        $this->comments = $comments;
        $this->date = $date;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to('svetlana@archimed-soft.ru')
            ->with([
                'comments' => $this->comments,
                'date' => $this->date
            ])
            ->from('noreplay@stage.88003010373.ru', 'ArchiMed +')
            ->replyTo('noreplay@stage.88003010373.ru', 'ArchiMed +')
            ->cc('yamaletdinov.rusl@yandex.ru', 'Test')
            ->subject('Отчет по внедренцам')
            ->view('emails.imprep');
    }
}
