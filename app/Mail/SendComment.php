<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendComment extends Mailable
{
    use Queueable, SerializesModels;

    public $email;
    public $first_name;
    public $last_name;
    public $c_first_name;
    public $c_last_name;
    public $c_patronymic;
    public $c_email;
    public $comment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $first_name, $last_name, $c_first_name, $c_last_name, $c_patronymic, $c_email, $comment)
    {
        $this->email = $email;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->c_first_name = $c_first_name;
        $this->c_last_name = $c_last_name;
        $this->c_patronymic = $c_patronymic;
        $this->c_email = $c_email;
        $this->comment = $comment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() 
    {

        return $this->to($this->c_email)
            ->with([
                'imp' => $this->last_name.' '.$this->first_name,
                'client' => $this->c_last_name.' '.$this->c_patronymic,
                'comment' => $this->comment
            ])
            ->from('noreplay@archimed.pro', 'ArchiMed +')
            ->replyTo('noreplay@archimed.pro', 'ArchiMed +')
            // ->cc('ya.ruslan2013@mail.ru', 'Test')
            ->subject('Информационное письмо')
            ->view('emails.comment');
    }
}
