<?php

namespace App\Helpers\Contracts;

Interface AMImap {
    public function connect();

    public function getMailboxes();
}

?>