<?php

namespace App\Helpers;

use App\Helpers\Contracts\AMImap;

class AMImapConnection implements AMImap {
    public function __construct() {
        $config = Config('imap.accounts.archimed-soft');
        $this->host = $config['host'];
        $this->port = $config['port'];
        $this->encryption = $config['encryption'];
        $this->validate_cert = $config['validate_cert'];
        $this->username = $config['username'];
        $this->password = $config['password'];
    }

    public function connect($folder = 'INBOX') {
        try {
            $mbox = imap_open('{'. $this->host.':'.$this->port.'/imap/'.$this->encryption.'/'.$this->validate_cert.'}'.$folder, $this->username, $this->password);
            return $mbox;
        } catch (\Exception $e) {
            return "Ошибка подключения к серверу imap: ".$e->getMessage();
        }
    }

    public function getMailboxes($search = "*") {
        $arr = [];
        if($t = imap_getmailboxes($this->connect(), '{'. $this->host.':'.$this->port.'/imap/'.$this->encryption.'/'.$this->validate_cert.'}', $search)) {
            foreach($t as $item) {
                switch(substr($item->name, strpos($item->name, '}') + 1)) {
                    case 'INBOX':
                        $arr[] = [
                            "fullpath" => $item->name,
                            "attributes" => $item->attributes,
                            "delimiter" => $item->delimiter,
                            "shortpath" => substr($item->name, strpos($item->name, '}') + 1),
                            'name' => 'Входящие',
                        ];
                        break;
                    case 'Archive':
                        $arr[] = [
                            "fullpath" => $item->name,
                            "attributes" => $item->attributes,
                            "delimiter" => $item->delimiter,
                            "shortpath" => substr($item->name, strpos($item->name, '}') + 1),
                            'name' => 'Архив',
                        ];
                        break;
                    case 'Drafts':
                        $arr[] = [
                            "fullpath" => $item->name,
                            "attributes" => $item->attributes,
                            "delimiter" => $item->delimiter,
                            "shortpath" => substr($item->name, strpos($item->name, '}') + 1),
                            'name' => 'Черновики',
                        ];
                        break;
                    case 'Junk':
                        $arr[] = [
                            "fullpath" => $item->name,
                            "attributes" => $item->attributes,
                            "delimiter" => $item->delimiter,
                            "shortpath" => substr($item->name, strpos($item->name, '}') + 1),
                            'name' => 'Спам',
                        ];
                        break;
                    case 'Sent':
                        $arr[] = [
                            "fullpath" => $item->name,
                            "attributes" => $item->attributes,
                            "delimiter" => $item->delimiter,
                            "shortpath" => substr($item->name, strpos($item->name, '}') + 1),
                            'name' => 'Отправленные',
                        ];
                        break;
                    case 'Trash':
                        $arr[] = [
                            "fullpath" => $item->name,
                            "attributes" => $item->attributes,
                            "delimiter" => $item->delimiter,
                            "shortpath" => substr($item->name, strpos($item->name, '}') + 1),
                            'name' => 'Корзина',
                        ];
                        break;
                    }

                krsort($arr);
            }
        }
        return $arr;
    }

    /**
     * Call IMAP extension function call wrapped with utf7 args conversion & errors handling
     *
     * @param $methodShortName
     * @param array|string $args
     * @param bool $prependConnectionAsFirstArg
     * @param string|null $throwExceptionClass
     * @return mixed
     * @throws Exception
     */
    public function imap($methodShortName, $args = [], $prependConnectionAsFirstArg = true, $throwExceptionClass = Exception::class) {
        if(!is_array($args)) {
            $args = [$args];
        }
        foreach($args as &$arg) {
            if(is_string($arg)) {
                $arg = imap_utf7_encode($arg);
            }
        }
        if($prependConnectionAsFirstArg) {
            array_unshift($args, $this->connect());
        }

        imap_errors(); // flush errors
        $result = @call_user_func_array("imap_$methodShortName", $args);

        if(!$result) {
            $errors = imap_errors();
            if($errors) {
                if($throwExceptionClass) {
                    throw new $throwExceptionClass("IMAP method imap_$methodShortName() failed with error: " . implode('. ', $errors));
                }
                else {
                    return false;
                }
            }
        }

        return $result;
    }

    public function searchMailbox($criteria = 'ALL') {
        return $this->imap('search', [$criteria, SE_UID, $this->encryption]) ?: [];
    }

    /**
     * Switch mailbox without opening a new connection
     *
     * @param string $imapPath
     * @throws Exception
     */
    public function switchMailbox($imapPath) {
        $this->imapPath = $imapPath;
        dd($this->imapPath);
        $this->imap('reopen', $this->imapPath);
    }

    public function getMailInBox($folder) {
        $mbox = '{'. $this->host.':'.$this->port.'/imap/'.$this->encryption.'/'.$this->validate_cert.'}'.$folder;
        $this->switchMailbox($mbox);
        $mailsIds = $this->imap('search', ['ALL', SE_UID, $this->encryption]) ?: [];
        dd($mailsIds);
        if ($mailsIds) {
            foreach ($mailsIds as $m_id) {
                $m = $this->mailbox->getMail($m_id);
                $date = Carbon::createFromTimestamp(strtotime($m->headers->Date))
                    ->timezone('Europe/Moscow')
                    ->toDateTimeString();
                $mail[] = [
                    'id' => $m->id,
                    'personal' => $this->mailbox->decodeMimeStr($m->headers->sender[0]->personal, 'utf-8'),
                    'subject' => $this->mailbox->decodeMimeStr($m->headers->Subject, 'utf-8'),
                    'sender' => $m->headers->sender[0]->mailbox.'@'.$m->headers->sender[0]->host,
                    'date' => $date,
                    'short_text' => strip_tags($m->textHtml)
                ];
            }
        } else {
            $mail[] = '';
        }

        dd($mail);
        return $mail;
    }
}

class Exception extends \Exception {

}

class ConnectionException extends Exception {

}

?>