<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\AMImapConnection;

class AMImapServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Helpers\Contracts\AMImap', function () {
            return new AMImapConnection();
        });
    }
}
