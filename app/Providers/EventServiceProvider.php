<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Log;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\onAddProjectEvent' => [
            'App\Listeners\AddProjectListener',
        ],
        'App\Events\HelloPusherEvent' => [
            'App\Listeners\HelloPusherEventListener',
        ],
        'App\Events\onAddNewUser' => [
            'App\Listeners\AddUserListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Event::listen('project', function ($project) {
            //Log::info('Project save in database', ['project' => '123']);
            return true;
        });
        Event::listen('user', function ($project) {
            Log::info('Project save in database', ['project' => '123']);
            return true;
        });
    }
}
