<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Printform_file extends Model
{
    protected $fillable = [
        'file', 'project_print_form_id', 'g_file'
    ];
}
