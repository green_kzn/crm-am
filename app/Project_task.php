<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project_task extends Model {

    use SoftDeletes;

    protected $fillable = [
        'tasksstatus_ref_id',
        'project_id',
        'user_id',
        'observer_id',
        'module_rule_id',
        'number_contacts',
        'grouptasks_id',
        'module_id',
    ];

    public function project() {
        return $this->belongsTo(Project::class);
    }

    public function moduleRule() {
        return $this->belongsTo(Module_rule::class);
    }

    public function commentTask() {
        return $this->belongsTo(Comment_task::class, 'id', 'task_id');
    }

    public function projectTaskContacts() {
        return $this->hasMany(Project_task_contact::class);
    }
}
