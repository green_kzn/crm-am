<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Sentinel;
use App\Comment;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendImpRepMail as SendImpRepMail;

class SendImpRep extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ImpRep:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending implementors reporps on email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = Carbon::now()->format('Y-m-d');
        $date2 = Carbon::now()->format('d.m.Y');
        $comment = DB::select("select `p`.`name` as `p_name`, `u`.`first_name`, `u`.`last_name`, `c`.`text`, `c`.`for_client`
            from `comments` `c`
            left join `projects` `p` on `c`.`project_id` = `p`.`id`
            left join `users` `u` on `c`.`user_id` = `u`.`id`
            left join `role_users` `r` on `r`.`user_id` = `u`.`id`
            where `c`.`deleted_at` is null and
            `r`.`role_id` = 4 and
            `c`.`created_at` like '".$date."%'
            order by `u`.`first_name`");

        // dd($comment);
        if (count($comment) > 0) {
            Mail::send(new SendImpRepMail($comment, $date2));
        }
    }
}
