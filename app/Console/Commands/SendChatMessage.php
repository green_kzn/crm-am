<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Redis;
use App\User;
use App\Events\ChatMessageWasReceived;
use App\Events\ManagerAddClient;
use App\Events\saveRaw;
use App\Events\addPrintFormFromProject;
use App\Message;
use App\Mail\SendComment;
use Event;
use Mail;
use Sentinel;

class SendChatMessage extends Command
{
    protected $signature = 'chat:message {message}';

    protected $description = 'Send chat message.';

    public function handle()
    {
        // Log:info(Sentinel::getUser());
        broadcast(new addPrintFormFromProject($cur_user->first_name.' '.$cur_user->last_name));
    }
}
