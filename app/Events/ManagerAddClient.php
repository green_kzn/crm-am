<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class ManagerAddClient implements ShouldBroadcastNow
{
    use SerializesModels;

    public $client;
    public $user;

    public function __construct($client, $user)
    {
        $this->client = $client;
        $this->user = $user;
    }


    public function broadcastOn()
    {
        return ['client'];
    }

    public function broadcastAs()
    {
        return 'add';
    }
}
