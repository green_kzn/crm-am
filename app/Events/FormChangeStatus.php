<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class FormChangeStatus implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $status;
    public $form;

    public function __construct($status, $form) {
        $this->form = $form;
        $this->status = $status;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('formChangeStatus');
    }
 
    public function broadcastAs()
    {
        return 'formChangeStatus';
    }
}
