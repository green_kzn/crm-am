<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Log;

class ChatMessageWasReceived implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $msg;
    public $user;
    public $userid;

    public function __construct($msg, $user) {
        $this->msg = $msg;
        $this->user = $user;
        $this->userid = 7;
    }

    public function broadcastOn()
    {
        // print_r('Ok');
        return new PrivateChannel('message123');
        //Log::info(new PresenceChannel('message123.'.$this->userid));
        // return ['message123'];
    }
 
    public function broadcastAs()
    {
        return 'message123';
    }
}
