<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use App\Task;
use App\Project_task;
use App\Module_rule;
use App\User;

class addImpEvent implements ShouldBroadcastNow
{
    use SerializesModels;

    public $events;

    public function __construct($events)
    {
        $this->events = $events;
    }

    public function broadcastOn()
    {
//        return response()->json($res);
        return ['impEvent'];
    }

    public function broadcastAs()
    {
        return 'add';
    }
}
