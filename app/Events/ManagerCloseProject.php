<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class ManagerCloseProject implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $project;
    public $user;

    public function __construct($project, $user) {
        $this->project = $project;
        $this->user = $user;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('ManagerCloseProject');
    }
 
    public function broadcastAs()
    {
        return 'ManagerCloseProject';
    }
}
