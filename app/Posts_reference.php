<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Posts_reference extends Model
{
    use SoftDeletes;

    protected $fillable = ['name'];
}
