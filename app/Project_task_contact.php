<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project_task_contact extends Model
{
    protected $fillable = ['project_id', 'project_task_id', 'user_id', 'callbackstatus_ref_id', 'status',
        'observer_id',
        'date_production',
        'duration',
        'end',
        'next_for_cont',
        'prev_contact',
        'next_contact',
        'contact_id',
    ];

    public function projectTask(){
        return $this->belongsTo(Project_task::class);
    }

    public function contact(){
        return $this->belongsTo(Contact::class);
    }

}
