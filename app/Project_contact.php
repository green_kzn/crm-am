<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project_contact extends Model
{

    use SoftDeletes;
    protected $fillable = [
        'contact_id',
        'project_id',
        'main',
        'mail_rep',
    ];
}
