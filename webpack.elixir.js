let Elixir = require('laravel-webpacker');


Elixir.mix(function (mix) {
    mix.js('./src/app.js', './dist/app.js')
        .sass('./src/app.scss', './dist/app.css');
});


module.exports = Elixir;