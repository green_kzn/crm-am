/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import Project_categories from "./components/implementer_view/Project_categories";


require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import Vue from 'vue'
import VueRouter from 'vue-router'
import FullCalendar from 'vue-full-calendar'
// import Vuetify from 'vuetify'
// import colors from 'vuetify/es5/util/colors'
import * as VeeValidate from 'vee-validate'
// import VueFroala from 'vue-froala-wysiwyg'
import 'froala-editor/js/froala_editor.pkgd.min'
import Notifications2 from 'vue-notification'
// import VueUploadMultipleImage from 'vue-upload-multiple-image'
// import VueUploadMultipleImage from 'vue-upload-multiple-image'
// import Multiselect from 'vue-multiselect'
import CKEditor from '@ckeditor/ckeditor5-vue';
import Select2 from 'v-select2-component';
import VueSelect from 'vue-select';
import 'vue-select/dist/vue-select.css'
import VueConfirmDialog from 'vue-confirm-dialog'


Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component('v-select', VueSelect)
// Vue.use( SimpleUploadAdapter );
Vue.use( CKEditor);
// Vue.component('multiselect', Multiselect)
Vue.component('Select2', Select2);
Vue.use(Notifications2)
// Vue.use(VueFroala)
Vue.use(VeeValidate)
Vue.use(FullCalendar)
Vue.use(VueRouter)
// Vue.use(Vuetify, {
//     theme: {
//         primary: '#1976D2',
//         secondary: '#424242',
//         accent: '#82B1FF',
//         error: '#FF5252',
//         info: '#2196F3',
//         success: '#4CAF50',
//         warning: '#FFC107'
//     }
// })
Vue.use(VueConfirmDialog)
Vue.component('vue-confirm-dialog', VueConfirmDialog.default)

//Vue.component('example-component', require('./components/ExampleComponent.vue'));
//Vue.component('Login', require('./components/Login.vue'));
Vue.component('Notification', require('./components/Notification.vue'))
Vue.component('ContextMenuItem', require('./components/ContextMenuItem.vue'));
Vue.component('ContextMenu', require('./components/ContextMenu.vue'));

// Меню
import App from './components/App.vue'
import Manager_menu from './components/manager_view/Menu.vue'
import Implementer_menu from './components/implementer_view/Menu.vue'
import Print_form_menu from './components/print_form_view/Menu.vue'
import Dir_menu from './components/dir_view/Menu.vue'
import Tp_menu from './components/tp_view/Menu.vue'
import Admin_menu from './components/admin_view/Menu.vue'

// Администраторы
import AdmUsersGroup from './components/admin_view/UsersGroup.vue'

// Внедренцы
import Hello from './components/implementer_view/ExampleComponent.vue'
import Fullcalendar2 from './components/implementer_view/Fullcalendar2.vue'
import Calendar from './components/implementer_view/Calendar.vue'
import Tasks from './components/implementer_view/Tasks.vue'
import Projects from './components/implementer_view/ProjectsNew.vue'
import Imp_Print_form from './components/implementer_view/Imp_Print_form.vue'
import Imp_Print_raw_edit from './components/implementer_view/Imp_Print_raw_edit.vue'
import Print_attendant_add from './components/implementer_view/Print_attendant_add.vue'
import Print_attendant_edit from './components/implementer_view/Print_attendant_edit.vue'
import Print_form_project_edit from './components/implementer_view/Print_form_project_edit.vue'
import Projects_edit from './components/implementer_view/Projects_edit.vue'
import Projects_raw from './components/implementer_view/Projects_raw.vue'
import Projects_close from './components/implementer_view/Projects_close.vue'
import Edit_task from './components/implementer_view/Edit_task.vue'
import Login from './components/Auth/Login.vue'

// Менеджеры
import Manager_projects from './components/manager_view/Projects.vue'
import Manager_projects_add from './components/manager_view/Project_add.vue'
import Manager_clients_add from './components/manager_view/Clients_add.vue'
import Manager_projects_edit from './components/manager_view/Project_edit.vue'
import Manager_calls from './components/manager_view/CallsNew.vue'
import Manager_quality_edit from './components/manager_view/Quality_edit.vue'
import Manager_calls_edit from './components/manager_view/Calls_edit.vue'
import Manager_request_edit from './components/manager_view/Request_edit.vue'
import Manager_request_view from './components/manager_view/Request_view.vue'
import Manager_callback_edit from './components/manager_view/Callback_edit.vue'
// import Manager_calendar from './components/manager_view/Manager_calendar2.vue'
import Manager_forms from './components/manager_view/Manager_forms.vue'
import Manager_mail from './components/manager_view/Mail.vue'
import Manager_pay from './components/manager_view/Manager_pay.vue'
// import Manager_form_add from './components/manager_view/Manager_form_add.vue'

// Отдел разработки форм
import Print_form from './components/print_form_view/Print_formNew.vue'
import Print_form_tasks from './components/print_form_view/Print_form_tasks.vue'
import Print_form_raw from './components/print_form_view/Print_form_raw.vue'
import Print_form_pay from './components/print_form_view/Print_form_pay.vue' 
import Print_form_edit from './components/print_form_view/Print_form_edit.vue'
import Print_form_attendant_edit from './components/print_form_view/Print_form_attendant_edit.vue'
import Print_form_mail from './components/print_form_view/Mail.vue'

// Руководство
import Dir_projects from './components/dir_view/Dir_projects.vue'
import Dir_project_edit from './components/dir_view/Dir_project_edit.vue'

//Техническая поддержка
import Tp_forms from './components/tp_view/Tp_forms.vue'
import Tp_mail from './components/tp_view/Mail.vue'

// Общие
import Settings from './components/Settings.vue'
import Callback from './components/Callback.vue'
import Quality_control from './components/Quality_control.vue'
import Quality_control_add from './components/Quality_control_add.vue'
import Quality_control_edit from './components/Quality_control_edit.vue'
import Users from './components/Users.vue'
import Users_add from './components/Users_add.vue'
import Users_edit from './components/Users_edit.vue'
import Dashboard from './components/Dashboard.vue'
import Calls from './components/Calls.vue'
import Group_task from './components/Group_task.vue'
import Group_task_add from './components/Group_task_add.vue'
import Group_task_edit from './components/Group_task_edit.vue'
import Tasks_status from './components/Tasks_status.vue'
import Tasks_status_add from './components/Tasks_status_add.vue'
import Tasks_status_edit from './components/Tasks_status_edit.vue'
import Modules from './components/Modules.vue'
import Modules_add from './components/Modules_add.vue'
import Modules_edit from './components/Modules_edit.vue'
import Waiting_list from './components/references/WaitingList'
import Waiting_list_add from './components/Waiting_list_add.vue'
import Waiting_list_edit from './components/Waiting_list_edit.vue'
import Clients from './components/Clients.vue'
import Clients_add from './components/Clients_add.vue'
import Clients_edit from './components/Clients_edit.vue'
import ContactPosts from "./components/ContactPosts.vue";
import Modal from './components/Modal.vue';
import Modal2 from './components/Modal2.vue';
import UploadForm from './components/UploadForm.vue';
import Imp_uploadForm_attendant from './components/Imp_uploadForm_attendant.vue';
import PF_UploadForm from './components/PF_UploadForm.vue';
// import Notification from './components/Notification.vue'

import Calendar_common from './components/calendar_common/Calendar_common.vue'

import VuePagination from './components/vue-pagination.vue'; 

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/admin/users-group',
            name: 'AdmUsersGroup',
            component: AdmUsersGroup,
        },
        {
            path: '/login',
            name: 'Login',
            component: Login,
        },
        {
            path: '/implementer/calendar/:id/:date',
            name: 'Calendar',
            component: Calendar_common,
        },
        {
            path: '/implementer',
            name: 'dashboard',
            component: Dashboard,
        },
        {
            path: '/implementer/calls-result',
            name: 'Calls_result',
            component: Calls,
        },
        {
            path: '/implementer/tasks',
            name: 'Tasks',
            component: Tasks,
        },
        {
            path: '/implementer/projects',
            name: 'Projects',
            component: Projects,
        },
        {
            path: '/implementer/project/raw/:id',
            name: 'Projects_raw',
            component: Projects_raw,
        },
        {
            path: '/implementer/project/edit/:id',
            name: 'Projects_edit',
            component: Projects_edit,
        },
        {
            path: '/implementer/print-form',
            name: 'Imp_Print_form',
            component: Imp_Print_form,
        },
        {
            path: '/implementer/print-form/raw/:id',
            name: 'Imp_Print_raw_edit',
            component: Imp_Print_raw_edit,
        },
        {
            path: '/implementer/print-form/attendant/add',
            name: 'Print_attendant_add',
            component: Print_attendant_add,
        },
        {
            path: '/implementer/print-form/attendant/:id',
            name: 'Print_attendant_edit',
            component: Print_attendant_edit,
        },
        {
            path: '/implementer/print-form/project/:id',
            name: 'Print_form_project_edit',
            component: Print_form_project_edit,
        },
        {
            path: '/implementer/project-task/edit/:id/:date',
            name: 'Edit_task',
            component: Edit_task,
        },
        {
            path: '/implementer/project/close/:id',
            name: 'Projects_close',
            component: Projects_close,
        },
        {
            path: '/implementer/settings',
            name: 'Settings',
            component: Settings,
        },
        {
            path: '/implementer/users',
            name: 'Users',
            component: Users,
        },
        {
            path: '/implementer/users/add',
            name: 'Users_add',
            component: Users_add,
        },
        {
            path: '/implementer/users/edit/:id',
            name: 'Users_edit',
            component: Users_edit,
        },
        {
            path: '/implementer/callback',
            name: 'Callback',
            component: Callback,
        },
        {
            path: '/implementer/quality-control',
            name: 'Quality_control',
            component: Quality_control,
        },
        {
            path: '/implementer/quality-control/add',
            name: 'Quality_control_add',
            component: Quality_control_add,
        },
        {
            path: '/implementer/quality-control/edit/:id',
            name: 'Quality_control_edit',
            component: Quality_control_edit,
        },
        {
            path: '/implementer/group-tasks',
            name: 'Group_task',
            component: Group_task,
        },
        {
            path: '/implementer/group-tasks/add',
            name: 'Group_task_add',
            component: Group_task_add,
        },
        {
            path: '/implementer/group-tasks/edit/:id',
            name: 'Group_task_edit',
            component: Group_task_edit,
        },
        {
            path: '/implementer/tasks-status',
            name: 'Tasks_status',
            component: Tasks_status,
        },
        {
            path: '/implementer/tasks-status/add',
            name: 'Tasks_status_add',
            component: Tasks_status_add,
        },
        {
            path: '/implementer/tasks-status/edit/:id',
            name: 'Tasks_status_edit',
            component: Tasks_status_edit,
        },
        {
            path: '/implementer/modules',
            name: 'Modules',
            component: Modules,
        },
        {
            path: '/implementer/modules/add',
            name: 'Modules_add',
            component: Modules_add,
        },
        {
            path: '/implementer/modules/edit/:id',
            name: 'Modules_edit',
            component: Modules_edit,
        },
        {
            path: '/implementer/waiting-list',
            name: 'Waiting_list',
            component: Waiting_list,
        },
        // {
        //     path: '/implementer/waiting-list/add',
        //     name: 'Waiting_list_add',
        //     component: Waiting_list_add,
        // },
        // {
        //     path: '/implementer/waiting-list/edit/:id',
        //     name: 'Waiting_list_edit',
        //     component: Waiting_list_edit,
        // },
        {
            path: '/implementer/clients',
            name: 'Clients',
            component: Clients,
        },
        {
            path: '/implementer/clients/add',
            name: 'Clients_add',
            component: Clients_add,
        },
        {
            path: '/implementer/clients/edit/:id',
            name: 'Clients_edit',
            component: Clients_edit,
        },
        {
            path: '/implementer/contact-posts',
            name: 'ContactPosts',
            component: ContactPosts,
        },
        {
            path: '/implementer/project-categories',
            name: 'ProjectCategories',
            component: Project_categories,
        },
        {
            path: '/manager',
            name: 'dashboard_manager',
            component: Dashboard,
        },
        {
            path: '/manager/calls-result',
            name: 'manager_calls_result',
            component: Calls,
        },
        {
            path: '/manager/projects',
            name: 'manager_projects',
            component: Manager_projects,
        },
        {
            path: '/manager/projects/add',
            name: 'manager_projects_add',
            component: Manager_projects_add,
        },
        {
            path: '/manager/projects/clients/add',
            name: 'manager_clients_add',
            component: Manager_clients_add,
        },
        {
            path: '/manager/project/edit/:id',
            name: 'manager_projects_edit',
            component: Manager_projects_edit,
        },
        {
            path: '/manager/calls',
            name: 'manager_calls',
            component: Manager_calls,
        },
        {
            path: '/manager/callback/quality-edit/:id',
            name: 'Manager_quality_edit',
            component: Manager_quality_edit,
        },
        {
            path: '/manager/calls/edit/:id',
            name: 'manager_calls_edit',
            component: Manager_calls_edit,
        },
        {
            path: '/manager/callback/edit/:id',
            name: 'manager_callback_edit',
            component: Manager_callback_edit,
        },
        {
            path: '/manager/request/edit/:id',
            name: 'manager_request_edit',
            component: Manager_request_edit,
        },
        {
            path: '/manager/request/view/:id',
            name: 'manager_request_view',
            component: Manager_request_view,
        },
        {
            path: '/manager/calendar/:id/:date',
            name: 'Manager_calendar',
            component: Calendar_common,
        },
        {
            path: '/manager/forms',
            name: 'Manager_forms',
            component: Manager_forms,
        },
        {
            path: '/manager/mail',
            name: 'Manager_mail',
            component: Manager_mail,
        },
        {
            path: '/manager/forms/pay/:id',
            name: 'Manager_pay',
            component: Manager_pay,
        },
        // {
        //     path: '/manager/add-form',
        //     name: 'Manager_form_add',
        //     component: Manager_form_add,
        // },
        {
            path: '/print_form',
            name: 'dashboard_print_form',
            component: Dashboard,
        },
        {
            path: '/print_form/calls-result',
            name: 'Calls',
            component: Calls,
        }, 
        {
            path: '/print_form/forms',
            name: 'print_form',
            component: Print_form,
        },
        {
            path: '/print_form/forms/raw/:id',
            name: 'print_form_raw',
            component: Print_form_raw,
        },
        {
            path: '/print_form/forms/attendant/edit/:id',
            name: 'Print_form_attendant_edit',
            component: Print_form_attendant_edit,
        },
        {
            path: '/print_form/forms/pay/:id',
            name: 'print_form_pay',
            component: Print_form_pay,
        },
        {
            path: '/print_form/forms/edit/:id',
            name: 'print_form_edit',
            component: Print_form_edit,
        },
        {
            path: '/print_form/tasks',
            name: 'print_form_tasks',
            component: Print_form_tasks,
        },
        {
            path: '/print_form/mail',
            name: 'print_form_mail',
            component: Print_form_mail,
        },
        {
            path: '/dir',
            name: 'dashboard_dir',
            component: Dashboard,
        },
        {
            path: '/dir/projects',
            name: 'dir_projects',
            component: Dir_projects,
        },
        {
            path: '/dir/project/edit/:id',
            name: 'dir_project_edit',
            component: Dir_project_edit,
        },
        {
            path: '/tp',
            name: 'dashboard_tp',
            component: Dashboard,
        },
        {
            path: '/tp/forms',
            name: 'Tp_forms',
            component: Tp_forms,
        },
        {
            path: '/tp/mail',
            name: 'Tp_mail',
            component: Tp_mail,
        },
    ],
});

Vue.notify({
    group: 'foo',
    title: 'Important message',
    text: 'Hello user! This is a notification!'
})

Vue.component('pagination', require('laravel-vue-pagination'));
import SortedTablePlugin from "vue-sorted-table";
import VueSocketio from 'vue-socket.io';
import VueSocketIOExt from 'vue-socket.io-extended';
import Echo from "laravel-echo";
window.io = require('socket.io-client');
// import io from 'socket.io-client';
 
// const socket = io('//ruslan.stage.88003010373.ru:6000/');
 
// Vue.use(VueSocketIOExt, socket);

Vue.use(SortedTablePlugin);
// Vue.use(VueSocketio); 

const options = { path: '/my-app/' }; //Options object to pass into SocketIO

// Vue.use(new VueSocketio({
//     debug: true,
//     connection: '//stage.88003010373.ru:4023/',
//     vuex: {
//         actionPrefix: 'SOCKET_',
//         mutationPrefix: 'SOCKET_'
//     }
// }))

import {request} from './api-plugin.js'
Vue.prototype.$request = request;

const app = new Vue({
    el: '#app',
    components: {
        // VueUploadMultipleImage,
        App,
        Admin_menu,
        Manager_menu,
        Implementer_menu,
        Modal,
        Print_form_menu,
        Dir_menu,
        Tp_menu,
        // VueUploadMultipleImage,
        UploadForm,
        PF_UploadForm,
        Imp_uploadForm_attendant,
        // Multiselect,
        VuePagination
        //Notification
    },
    router,
    data() {
        return {
            count: 0,
            raw_count: 0,
            date2: null,
            time2: null,
            time3: null,
            implementer: null,
            project_id: null,
            caldate: null,
            perm: '',
            // events: null,
            newevents: null,
            pagination: null,
            currentPage: 1,
            // socket: io("//ruslan.stage.88003010373.ru:6000/")
        }
    },
    sockets:{
        connect() {
            console.log('socket connected')
        },
        customEmit(val) {
            console.log('this method was fired by the socket server. eg: io.emit("customEmit", data)')
        }
    },
    create: {
        // listenForBroadcast(survey_id) {
        //     console.log(survey_id);
        //     Echo.join('private-message123')
        //       .here((users) => {
        //         console.log('Ok2');
        //         this.users_viewing = users;
        //         this.$forceUpdate();
        //       })
        //       .joining((user) => {
        //         console.log('Ok3');
        //         if (this.checkIfUserAlreadyViewingSurvey(user)) {
        //           this.users_viewing.push(user);
        //           this.$forceUpdate();
        //         }
        //       })
        //       .leaving((user) => {
        //         this.removeViewingUser(user);
        //         this.$forceUpdate();
        //       });
        // }
    },
    mounted: function() {
        // Notification.requestPermission(function(permission){
        //     this.perm = permission;
        //     console.log('Результат запроса прав:', permission);
        // }.bind(this));
        // // this.$socket.$subscribe('chat:message', payload => {
        // //     console.log(payload)
        // // });
        // this.$socket.on('chat:message', function (data) {
        //     console.log(data);
        //     this.$notify('text')
        //     this.count = this.count+1
        // }.bind(this));
        // this.$socket.on('client:add', function (data) {
        //     console.log(data);
        //     this.$notify('text')
        //     this.count = this.count+1
        // }.bind(this));
        // // this.$socket.client.emit('chat:message', val);
    },
    methods: {
        clickButton: function(val){
            // $socket is socket.io-client instance
            // this.$socket.emit('emit_method', val);
        },
        
    }
});
