export const request = function (r) {
    var req = {
        url: '',
        method: 'get',
        data: {},
        onError: () => {},
        onSuccess: () => {},
        onResponse: null,
        successMessage: null,
    }
    $.extend(req, r);

    axios
        [req.method](req.url, req.data)
        .then(response => {
            if (req.onResponse) {
                req.onResponse(response.data)
            } else {
                if (response.data.status == 'Ok') {
                    if (req.successMessage)
                        $.toast({
                            text: req.successMessage,
                            showHideTransition: 'fade',
                            hideAfter: 1000,
                            position: 'bottom-right'

                        });
                    req.onSuccess(response.data)
                } else {
                    $.toast({
                        text: response.data.message,
                        showHideTransition: 'fade',
                        hideAfter: 1000,
                        position: 'bottom-right'

                    });
                    req.onError();
                    console.log(response.data.message)
                }
            }
        })
        .catch(function (error) {
            console.log(error);
            $.toast({
                text: 'Ошибка сервера',
                showHideTransition: 'fade',
                hideAfter: 1000,
                position: 'bottom-right'

            });
        })
}