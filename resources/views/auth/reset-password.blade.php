@extends('layouts.admin-panel')

@section('title')
    Восстановление пароля
@endsection

@section('content')
    <form action="" method="POST">
        {{ csrf_field() }}

        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="form-group">
            <div class="input-group">
            <span class="input-group-addon">
                <i class="fa fa-lock"></i>
            </span>

                <input type="password" name="password" class="form-control" placeholder="Новый пароль" required>
            </div>
        </div>

        <div class="form-group">
            <div class="input-group">
            <span class="input-group-addon">
                <i class="fa fa-lock"></i>
            </span>

                <input type="password" name="password_confirmation" class="form-control" placeholder="Подтверждение пароля" required>
            </div>
        </div>

        <div class="form-group">
            <div class="input-group">
                <input type="submit" value="Сохранить" class="btn btn-success">
            </div>
        </div>
    </form>
@endsection