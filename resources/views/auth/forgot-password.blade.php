@extends('layouts.admin-panel')

@section('title')
    Восстановление пароля
@endsection

@section('content')
    <form action="/forgot-password" method="POST">
        {{ csrf_field() }}

        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif

        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-envelope"></i>
                </span>

                <input type="email" name="email" class="form-control" placeholder="name@site.ru" required>
            </div>
        </div>

        <div class="form-group">
            <div class="input-group">
                <input type="submit" value="Восстановить" class="btn btn-success">
            </div>
        </div>
        <a href="/login">Войти</a>
    </form>
@endsection