<div class="header clearfix">
    <nav>
        <ul class="nav nav-pills pull-right">
            @if(Sentinel::check())
                <li role="presentation">
                    <form action="logout" method="POST" id="logout-form">
                        {{ csrf_field() }}

                        <a href="#" onclick="document.getElementById('logout-form').submit()">Выход</a>
                    </form>
                </li>
            @else
                <li role="presentation"><a href="/login">Вход</a></li>
            @endif
        </ul>
    </nav>
    <h3 class="text-muted">
        @if(Sentinel::check())
            {{ Sentinel::getUser()->first_name }}
        @else
            <a href="/">{{ config('app.name') }}</a>
        @endif
    </h3>
</div>