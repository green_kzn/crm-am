@extends('layouts.cabinet')

@section('title')
    Ошибка 404
@endsection

@section('content')
    Упс! Страница не найдена
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection