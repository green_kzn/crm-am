@extends('layouts.cabinet')

@section('title')
    Ошибка 503
@endsection

@section('content')
    Упс! Доступ запрещен
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection