@extends('layouts.cabinet')

@section('title')
    Мои проекты
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#projects" data-toggle="tab" aria-expanded="true">Проекты</a></li>
            <li class=""><a href="#project_tasks" data-toggle="tab" aria-expanded="false">Задачи по проектам</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="projects">
                <div class="box">
                    <div class="box-header">
                        <a href="" style="padding-right: 15px;">Спящие</a>
                        <a href="" style="padding-right: 15px;">Активные</a>
                        <a href="" style="padding-right: 15px;">Сегодня</a>
                        <a href="" style="padding-right: 15px;">Горящие</a>

                        <div class="box-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="project_search" class="form-control pull-right" placeholder="Поиск">

                                <div class="input-group-btn">
                                    <button onclick="projects_search();" type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                        @if(session('perm')['project.create'])
                            <a href="/admin/projects/add" class="btn btn-primary">Добавить проект</a>
                        @endif
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover" id="projects">
                            <thead>
                                <tr><th style="vertical-align: middle;">Название клиента</th>
                                    <th style="vertical-align: middle;">Название проекта</th>
                                    <th style="vertical-align: middle;">Внедренец</th>
                                    <th style="vertical-align: middle;">Предыдущий контакт</th>
                                    <th style="vertical-align: middle;">Следующий контакт</th>
                                    <th style="vertical-align: middle;">Город</th>
                                    <th style="vertical-align: middle;">Готовность</th>
                                    <th style="vertical-align: middle;">Активность</th>
                                    <th style="vertical-align: middle;">Дата начала</th>
                                    <th style="vertical-align: middle;">Планируемая дата окончания</th>
                                    <th style="vertical-align: middle;">Действия</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($projects)
                                    @foreach($projects as $p)
                                        @if($p['close'] != 1)
                                            <tr>
                                                <td style="vertical-align: middle;">{{ $p['client'] }}</td>
                                                <td style="vertical-align: middle;">{{ $p['name'] }}</td>
                                                <td style="vertical-align: middle;">{{ $p['user'] }}</td>
                                                @if($p['prev_contact'])
                                                    <td style="vertical-align: middle;">{{ $p['prev_contact'] }}</td>
                                                @else
                                                    <td style="vertical-align: middle;">История контактов отсутствует</td>
                                                @endif
                                                @if($p['next_contact'])
                                                    <td style="vertical-align: middle;">{{ $p['next_contact'] }}</td>
                                                @else
                                                    <td style="vertical-align: middle;">История контактов отсутствует</td>
                                                @endif
                                                <td style="vertical-align: middle;">{{ $p['city'] }}</td>
                                                <td style="vertical-align: middle;">{{ $p['done'] }} %</td>
                                                <td style="vertical-align: middle;">{{ $p['active'] }} %</td>
                                                <td style="vertical-align: middle;">{{ $p['start_date'] }}</td>
                                                <td style="vertical-align: middle;">{{ $p['finish_date'] }}</td>
                                                <td style="vertical-align: middle;">
                                                    @if(session('perm')['project.update'])
                                                        <a href="project/edit/{{ $p['id'] }}" class="btn btn-primary">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                    @else
                                                        <a href="javascript: void(0)" class="btn btn-primary" disabled="true">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @else
                                            <tr style="text-decoration: line-through;">
                                                <td style="vertical-align: middle;">{{ $p['client'] }}</td>
                                                <td style="vertical-align: middle;">{{ $p['name'] }}</td>
                                                <td style="vertical-align: middle;">{{ $p['user'] }}</td>
                                                @if($p['prev_contact'])
                                                    <td style="vertical-align: middle;">{{ $p['prev_contact'] }}</td>
                                                @else
                                                    <td style="vertical-align: middle;">История контактов отсутствует</td>
                                                @endif
                                                @if($p['next_contact'])
                                                    <td style="vertical-align: middle;">{{ $p['next_contact'] }}</td>
                                                @else
                                                    <td style="vertical-align: middle;">История контактов отсутствует</td>
                                                @endif
                                                <td style="vertical-align: middle;">{{ $p['city'] }}</td>
                                                <td style="vertical-align: middle;">{{ $p['done'] }} %</td>
                                                <td style="vertical-align: middle;">{{ $p['active'] }} %</td>
                                                <td style="vertical-align: middle;">{{ $p['start_date'] }}</td>
                                                <td style="vertical-align: middle;">{{ $p['finish_date'] }}</td>
                                                <td style="vertical-align: middle;">
                                                    @if(session('perm')['project.update'])
                                                        <a href="project/edit/{{ $p['id'] }}" class="btn btn-primary">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                    @else
                                                        <a href="javascript: void(0)" class="btn btn-primary" disabled="true">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="11" style="text-align: center;">Проекты не найдены</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            <!-- /.tab-pane -->
            </div>
            <div class="tab-pane" id="project_tasks">
                <div class="box">
                    <div class="box-header">
                        <a id="notExecute" onclick="sort_task('notExecute');" class="btn btn-default active">Невыполненные</a>
                        <a id="all" onclick="sort_task('all');" class="btn btn-default">Все</a>

                        <div class="box-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="task_search" class="form-control pull-right" placeholder="Поиск">

                                <div class="input-group-btn">
                                    <button type="submit" onclick="task_search();" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th style="vertical-align: middle;">Клиент</th>
                                <th style="vertical-align: middle;">Проект</th>
                                <th style="vertical-align: middle;">Город</th>
                                <th style="vertical-align: middle;">Название задачи</th>
                                <th style="vertical-align: middle;">Предыдущий контакт</th>
                                <th style="vertical-align: middle;">Запланированный контакт</th>
                                <th style="vertical-align: middle;">Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($project_task[0])
                                @foreach($project_task as $pt)
                                    <tr>
                                        <td style="vertical-align: middle;">{{ $pt['client'] }}</td>
                                        <td style="vertical-align: middle;">{{ $pt['p_name'] }}</td>
                                        <td style="vertical-align: middle;">{{ $pt['city'] }}</td>
                                        <td style="vertical-align: middle;">{{ $pt['name'] }}</td>
                                        @if($pt['prev_contact'])
                                            <td style="vertical-align: middle;">{{ $pt['prev_contact'] }}</td>
                                        @else
                                            <td style="vertical-align: middle;">История контактов отсутствует</td>
                                        @endif
                                        @if($pt['next_contact'])
                                            <td style="vertical-align: middle;">{{ $pt['next_contact'] }}</td>
                                        @else
                                            <td style="vertical-align: middle;">Не запланировано</td>
                                        @endif
                                        @if($pt['callbackstatus_ref_id'] == null)
                                            @if ($pt['next_contact'])
                                                <td style="vertical-align: middle;">
                                                    <a href="/admin/project-task/edit/{{ $pt['id'] }}" class="btn btn-primary"><i class="fa fa-search"></i></a>
                                                </td>
                                            @else
                                                <td style="vertical-align: middle;">
                                                    <a onclick="showCalendar({{ $loop->index+1 }}, {{ $pt['id'] }});" class="btn btn-success"><i class="fa fa-calendar-plus-o"></i></a>
                                                </td>
                                            @endif
                                        @else
                                            <td style="vertical-align: middle;">

                                            </td>
                                        @endif
                                    </tr>
                                    <tr style="display: none; background-color: #fff;" id="tr_{{ $loop->index+1 }}">
                                        <td colspan="6">
                                            <div class="box-body">
                                                <form action="/admin/calendar/add-project-task" method="get" id="add_project_task">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="tid" value="{{ $pt['id'] }}">
                                                    <div style="padding-bottom: 20px;">
                                                        <a id="date1" onclick="setDateCont('date1', {{ $loop->index+1 }});" data-content="{{ $tomorrow_val }}" class="btn btn-primary">Завтра</a>
                                                        <a id="date2" onclick="setDateCont('date2', {{ $loop->index+1 }});" data-content="{{ $plus_one_day_val }}" class="btn btn-primary">Послезавтра</a>
                                                        <a id="date3" onclick="setDateCont('date3', {{ $loop->index+1 }});" data-content="{{ $plus_two_day_val }}" class="btn btn-primary">{{ $plus_two_day }}</a>
                                                        <a id="date4" onclick="setDateCont('date4', {{ $loop->index+1 }});" data-content="{{ $plus_three_day_val }}" class="btn btn-primary">{{ $plus_three_day }}</a>
                                                    </div>
                                                    <div style="padding-bottom: 20px;">
                                                        <table class="table">
                                                            <tr>
                                                                <td style="vertical-align: middle;" width="25%">
                                                                    Внедренец
                                                                </td>
                                                                <td style="vertical-align: middle;" width="25%">
                                                                    <select name="user" id="user_{{ $loop->index+1 }}" class="form-control">
                                                                        @if ($observer != '')
                                                                            @foreach($observer as $o)
                                                                                @if($o['default'] == 1)
                                                                                    <option value="{{ $o['id'] }}" selected>{{ $o['first_name'] }} {{ $o['last_name'] }}</option>
                                                                                @else
                                                                                    <option value="{{ $o['id'] }}">{{ $o['first_name'] }} {{ $o['last_name'] }}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                </td>
                                                                <td rowspan="3" width="50%">
                                                                    <div id="am-calendar-{{ $loop->index+1 }}"></div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="vertical-align: middle;">Дата</td>
                                                                <td style="vertical-align: middle;"><input type="date" class="form-control" value="{{ $cur_date_val }}" id="date_cont_{{ $loop->index+1 }}" name="date_cont" required></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="vertical-align: middle;">Время</td>
                                                                <td style="vertical-align: middle;"><input type="time" class="form-control" id="time_cont_{{ $loop->index+1 }}" name="time_cont" required></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="vertical-align: middle;"><input type="submit" class="btn btn-success" value="Запланировать"></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" style="text-align: center;">Задачи не найдены</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
    </div>
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection

@push('scripts')
    <script>
        var url = document.location.toString();
        if (url.match('#')) {
            $('.nav-tabs li').removeClass('active');
            $('.nav-tabs li a').attr("aria-expanded","false");
            $('.nav-tabs a[href="#'+url.split('#')[1]+'"]').parent('li').addClass('active');
            $('.nav-tabs a[href="#'+url.split('#')[1]+'"]').attr("aria-expanded","true");
            $('.tab-pane').removeClass('active');
            $('#'+url.split('#')[1]).addClass('active');
        }

        $('.nav-tabs li a').on('shown.bs.tab', function (e) {
            window.location.hash = e.target.hash;
            scroll(0,0);
        });

        $(document).ready(function(){
            $("#add_project_task").validate({
                messages: {
                    time_cont: {
                        required: "Это поле обязательно для заполнения"
                    },
                    date_cont: {
                        required: "Это поле обязательно для заполнения"
                    }
                }
            });
        });

        function projects_search() {
            text = $("input[name~='project_search']").val();

            content = '';
            $.ajax({
                url: '/admin/projects/project_search',
                data: {'text': text},
                success: function (resp) {
                    $("#projects table tbody tr").remove();
                    console.log(resp);
                    if (resp != '') {
                        for(i=0;i<resp.length;i++) {
                            content += '<tr>' +
                                '<td style="vertical-align: middle;">'+resp[i].client+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].name+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].user+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].prev_contact+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].next_contact+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].city+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].done+'%</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].active+'%</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].start_date+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].finish_date+'</td>' +
                                '<td style="vertical-align: middle;"><a href="project/edit/'+resp[i].id+'" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>' +
                                '</tr>';
                        }
                    } else {
                        content += '<tr>' +
                            '<td colspan="11" style="vertical-align: middle; text-align: center;"><i>По вашему запросу ни чего не найдено</i></td>' +
                            '</tr>';
                    }

                    $("#projects table tbody").append(content);
                }
            });
        }

        function task_search() {
            text = $("input[name~='task_search']").val();

            content = '';
            $.ajax({
                url: '/admin/projects/task_search',
                data: {'text': text},
                success: function (resp) {
                    console.log(resp);
                    $("#project_tasks table tbody tr").remove();
                    if (resp != '') {
                        for (i=0;i<resp.length;i++) {
                            if (resp[i].tasksstatus_ref_id != 2) {
                                content += '<tr>' +
                                    '<td style="vertical-align: middle;">'+resp[i].client+'</td>' +
                                    '<td style="vertical-align: middle;">'+resp[i].p_name+'</td>' +
                                    '<td style="vertical-align: middle;">'+resp[i].city+'</td>' +
                                    '<td style="vertical-align: middle;">'+resp[i].name+'</td>';
                                if(resp[i].prev_contact != null) {
                                    content += '<td style="vertical-align: middle;">'+resp[i].prev_contact+'</td>';
                                } else {
                                    content += '<td style="vertical-align: middle;">История контактов отсутствует</td>';
                                }
                                if(resp[i].next_contact != null) {
                                    content += '<td style="vertical-align: middle;">'+resp[i].next_contact+'</td>';
                                } else {
                                    content += '<td style="vertical-align: middle;">Не запланировано</td>';
                                }
                                if(resp[i].callbackstatus_ref_id == null) {
                                    if (resp[i].next_contact != null) {
                                        content += "<td style=\"vertical-align: middle;\"><a href='/admin/project-task/edit/"+resp[i].id+"' class='btn btn-primary'><i class='fa fa-search'></i></a></td>";
                                    } else {
                                        content += "<td style=\"vertical-align: middle;\"><a onclick='showCalendar("+(i+1)+", "+resp[i].id+");' class='btn btn-success'><i class='fa fa-calendar-plus-o'></i></a></td>";
                                    }
                                }
                                content += '</tr>';
                                content += '<tr style="display: none; background-color: #fff;" id="tr_'+(i+1)+'">' +
                                    '<td colspan="6">' +
                                    '<div class="box-body">'+
                                    '<div style="padding-bottom: 20px;">'+
                                    '<a id="date1" onclick="setDateCont(\'date1\', '+(i+1)+');" data-content="{{ $tomorrow_val }}" class="btn btn-primary">Завтра</a>'+
                                    '<a id="date2" onclick="setDateCont(\'date2\', '+(i+1)+');" data-content="{{ $plus_one_day_val }}" class="btn btn-primary">Послезавтра</a>'+
                                    '<a id="date3" onclick="setDateCont(\'date3\', '+(i+1)+');" data-content="{{ $plus_two_day_val }}" class="btn btn-primary">{{ $plus_two_day }}</a>'+
                                    '<a id="date4" onclick="setDateCont(\'date4\', '+(i+1)+');" data-content="{{ $plus_three_day_val }}" class="btn btn-primary">{{ $plus_three_day }}</a>'+
                                    '</div>'+
                                    '<div style="padding-bottom: 20px;">'+
                                    '<table class="table">'+
                                    '<tr>'+
                                    '<td style="vertical-align: middle;" width="25%">'+
                                    'Внедренец ' +
                                    '</td>'+
                                    '<td style="vertical-align: middle;" width="25%">'+
                                    '<select name="" id="user_'+(i+1)+'" class="form-control">'+
                                    '@if($observer[0] != "")' +
                                    '@foreach($observer as $o)'+
                                    '@if($o['default'] == 1)'+
                                    '<option value="{{ $o['id'] }}" selected>{{ $o['first_name'] }} {{ $o['last_name'] }}</option>'+
                                    '@else'+
                                    '<option value="{{ $o['id'] }}">{{ $o['first_name'] }} {{ $o['last_name'] }}</option>'+
                                    '@endif'+
                                    '@endforeach'+
                                    '@endif' +
                                    '</select>'+
                                    '</td>'+
                                    '<td rowspan="3" width="50%">'+
                                    '<div id="am-calendar-'+(i+1)+'"></div>'+
                                    '</td>'+
                                    '</tr>'+
                                    '<tr>'+
                                    '<td style="vertical-align: middle;">Дата</td>'+
                                    '<td style="vertical-align: middle;"><input type="date" class="form-control" value="{{ $cur_date_val }}" id="date_cont_'+(i+1)+'"></td>'+
                                    '<td></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                    '<td style="vertical-align: middle;">Время</td>'+
                                    '<td style="vertical-align: middle;"><input type="time" class="form-control" id="time_cont_'+(i+1)+'"></td>'+
                                    '<td></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                    '<td style="vertical-align: middle;"><a onclick="saveToCalendar('+resp[i].id+', '+(i+1)+');" class="btn btn-success">Запланировать</a></td>'+
                                    '</tr>'+
                                    '</table>'+
                                    '</div>'+
                                    '</div>' +
                                    '</td>' +
                                    '</tr>';
                            } else {
                                content += '<tr style="text-decoration: line-through;">' +
                                    '<td style="vertical-align: middle;">'+resp[i].client+'</td>' +
                                    '<td style="vertical-align: middle;">'+resp[i].p_name+'</td>' +
                                    '<td style="vertical-align: middle;">'+resp[i].city+'</td>' +
                                    '<td style="vertical-align: middle;">'+resp[i].name+'</td>';
                                if(resp[i].prev_contact != null) {
                                    content += '<td style="vertical-align: middle;">'+resp[i].prev_contact+'</td>';
                                } else {
                                    content += '<td style="vertical-align: middle;">История контактов отсутствует</td>';
                                }
                                if(resp[i].next_contact != null) {
                                    content += '<td style="vertical-align: middle;">'+resp[i].next_contact+'</td>';
                                } else {
                                    content += '<td style="vertical-align: middle;">Не запланировано</td>';
                                }
                                if(resp[i].callbackstatus_ref_id == null) {
                                    if (resp[i].next_contact != null) {
                                        content += "<td style=\"vertical-align: middle;\"><a href='/admin/project-task/edit/"+resp[i].id+"' class='btn btn-primary'><i class='fa fa-search'></i></a></td>";
                                    } else {
                                        content += "<td style=\"vertical-align: middle;\"><a onclick='showCalendar("+i+", "+resp[i].id+");' class='btn btn-success'><i class='fa fa-calendar-plus-o'></i></a></td>";
                                    }
                                }
                                content += '</tr>';
                                content += '<tr style="display: none; background-color: #fff;" id="tr_'+(i+1)+'">' +
                                    '<td colspan="6">' +
                                    '<div class="box-body">'+
                                    '<div style="padding-bottom: 20px;">'+
                                    '<a id="date1" onclick="setDateCont(\'date1\', '+(i+1)+');" data-content="{{ $tomorrow_val }}" class="btn btn-primary">Завтра</a>'+
                                    '<a id="date2" onclick="setDateCont(\'date2\', '+(i+1)+');" data-content="{{ $plus_one_day_val }}" class="btn btn-primary">Послезавтра</a>'+
                                    '<a id="date3" onclick="setDateCont(\'date3\', '+(i+1)+');" data-content="{{ $plus_two_day_val }}" class="btn btn-primary">{{ $plus_two_day }}</a>'+
                                    '<a id="date4" onclick="setDateCont(\'date4\', '+(i+1)+');" data-content="{{ $plus_three_day_val }}" class="btn btn-primary">{{ $plus_three_day }}</a>'+
                                    '</div>'+
                                    '<div style="padding-bottom: 20px;">'+
                                    '<table class="table">'+
                                    '<tr>'+
                                    '<td style="vertical-align: middle;" width="25%">'+
                                    'Внедренец ' +
                                    '</td>'+
                                    '<td style="vertical-align: middle;" width="25%">'+
                                    '<select name="" id="user_'+(i+1)+'" class="form-control">'+
                                    '@if($observer[0] != "")' +
                                    '@foreach($observer as $o)'+
                                    '@if($o['default'] == 1)'+
                                    '<option value="{{ $o['id'] }}" selected>{{ $o['first_name'] }} {{ $o['last_name'] }}</option>'+
                                    '@else'+
                                    '<option value="{{ $o['id'] }}">{{ $o['first_name'] }} {{ $o['last_name'] }}</option>'+
                                    '@endif'+
                                    '@endforeach'+
                                    '@endif' +
                                    '</select>'+
                                    '</td>'+
                                    '<td rowspan="3" width="50%">'+
                                    '<div id="am-calendar-'+(i+1)+'"></div>'+
                                    '</td>'+
                                    '</tr>'+
                                    '<tr>'+
                                    '<td style="vertical-align: middle;">Дата</td>'+
                                    '<td style="vertical-align: middle;"><input type="date" class="form-control" value="{{ $cur_date_val }}" id="date_cont_'+(i+1)+'"></td>'+
                                    '<td></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                    '<td style="vertical-align: middle;">Время</td>'+
                                    '<td style="vertical-align: middle;"><input type="time" class="form-control" id="time_cont_'+(i+1)+'"></td>'+
                                    '<td></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                    '<td style="vertical-align: middle;"><a onclick="saveToCalendar('+resp[i].id+', '+(i+1)+');" class="btn btn-success">Запланировать</a></td>'+
                                    '</tr>'+
                                    '</table>'+
                                    '</div>'+
                                    '</div>' +
                                    '</td>' +
                                    '</tr>';
                            }
                        }
                    } else {
                        content += '<tr>' +
                            '<td colspan="7" style="vertical-align: middle; text-align: center;"><i>По вашему запросу ни чего не найдено</i></td>' +
                            '</tr>';
                    }

                    $("#project_tasks table tbody").append(content);
                }
            });
        }

        function showCalendar(i) {
            $("#tr_"+i).slideToggle("fast", function () {
                date = $("#date_cont_"+i).val();
                tasks = getObserverCalendar(i);
                user = $("#user_"+i+" option:selected").html();
                tasks = getObserverCalendar(i);

                if (tasks[0] != '') {
                    date = $("#date_cont_"+i).val();
                    console.log(tasks[0]['id']);
                    user = $("#user_"+i+" option:selected").html();
                    content = '<div class="am-calendar-header">' +
                        '<div id="date" style="float: left; width: 50%;">'+date+'</div>' +
                        '<div id="user">'+user+'</div>' +
                        '<div class="clearfix"></div>';
                    content += '<div><table>';
                    for (k=0; k<tasks.length; k++) {
                        content += '<tr>';
                        content += '<td style="padding-right: 10px;">'+tasks[k]['time']+'</td>';
                        content += '<td>'+tasks[k]['task']+'</td>';
                        content += '</tr>';
                    }
                    content += '</table></div>';
                } else {
                    content = '<div class="am-calendar-header">' +
                        '<div id="date" style="float: left; width: 50%;">'+date+'</div>' +
                        '<div id="user">'+user+'</div>' +
                        '<div class="clearfix"></div>';
                    content += '<div><table>';
                    content += '<tr><td colspan="2">Задачи отсутствуют</td></tr>';
                    content += '</table></div>';
                }


                $("#am-calendar-"+i).html(content);

                $("#date_cont_"+i).change(function () {
                    date = $(this).val();
                    if (difDate(date)) {
                        tasks = getObserverCalendar(i);

                        if (tasks[0] != '') {
                            date = $("#date_cont_"+i).val();
                            console.log(tasks[0]['id']);
                            user = $("#user_"+i+" option:selected").html();
                            content = '<div class="am-calendar-header">' +
                                '<div id="date" style="float: left; width: 50%;">'+date+'</div>' +
                                '<div id="user">'+user+'</div>' +
                                '<div class="clearfix"></div>';
                            content += '<div><table>';
                            for (k=0; k<tasks.length; k++) {
                                content += '<tr>';
                                content += '<td style="padding-right: 10px;">'+tasks[k]['time']+'</td>';
                                content += '<td>'+tasks[k]['task']+'</td>';
                                content += '</tr>';
                            }
                            content += '</table></div>';
                        } else {
                            content = '<div class="am-calendar-header">' +
                                '<div id="date" style="float: left; width: 50%;">'+date+'</div>' +
                                '<div id="user">'+user+'</div>' +
                                '<div class="clearfix"></div>';
                            content += '<div><table>';
                            content += '<tr><td colspan="2">Задачи отсутствуют</td></tr>';
                            content += '</table></div>';
                        }


                        $("#am-calendar-"+i).html(content);
                    } else {
                        content = '<div class="am-calendar-header">' +
                            '<div id="date" style="float: left; width: 50%;"></div>' +
                            '<div id="user">'+user+'</div>' +
                            '<div class="clearfix"></div>';
                        content += '<div><table>';
                        content += '<tr><td colspan="2">Ошибка! Выбранная дата меньше текущей</td></tr>';
                        content += '</table></div>';

                        $("#am-calendar-"+i).html(content);
                    };
                });

                $("#date_cont_"+i).change(function () {
                    date = $(this).val();
                    if (difDate(date)) {
                        tasks = getObserverCalendar(i);

                        if (tasks[0] != '') {
                            date = $("#date_cont_"+i).val();
                            console.log(tasks[0]['id']);
                            user = $("#user_"+i+" option:selected").html();
                            content = '<div class="am-calendar-header">' +
                                '<div id="date" style="float: left; width: 50%;">'+date+'</div>' +
                                '<div id="user">'+user+'</div>' +
                                '<div class="clearfix"></div>';
                            content += '<div><table>';
                            for (k=0; k<tasks.length; k++) {
                                content += '<tr>';
                                content += '<td style="padding-right: 10px;">'+tasks[k]['time']+'</td>';
                                content += '<td>'+tasks[k]['task']+'</td>';
                                content += '</tr>';
                            }
                            content += '</table></div>';
                        } else {
                            content = '<div class="am-calendar-header">' +
                                '<div id="date" style="float: left; width: 50%;">'+date+'</div>' +
                                '<div id="user">'+user+'</div>' +
                                '<div class="clearfix"></div>';
                            content += '<div><table>';
                            content += '<tr><td colspan="2">Задачи отсутствуют</td></tr>';
                            content += '</table></div>';
                        }


                        $("#am-calendar-"+i).html(content);
                    } else {
                        content = '<div class="am-calendar-header">' +
                            '<div id="date" style="float: left; width: 50%;"></div>' +
                            '<div id="user">'+user+'</div>' +
                            '<div class="clearfix"></div>';
                        content += '<div><table>';
                        content += '<tr><td colspan="2">Ошибка! Выбранная дата меньше текущей</td></tr>';
                        content += '</table></div>';

                        $("#am-calendar-"+i).html(content);
                    };
                });

                $("#user_"+i).change(function () {
                    tasks = getObserverCalendar(i);

                    if (tasks[0] != '') {
                        date = $("#date_cont_"+i).val();
                        console.log(tasks[0]['id']);
                        user = $("#user_"+i+" option:selected").html();
                        content = '<div class="am-calendar-header">' +
                            '<div id="date" style="float: left; width: 50%;">'+date+'</div>' +
                            '<div id="user">'+user+'</div>' +
                            '<div class="clearfix"></div>';
                        content += '<div><table>';
                        for (k=0; k<tasks.length; k++) {
                            content += '<tr>';
                            content += '<td style="padding-right: 10px;">'+tasks[k]['time']+'</td>';
                            content += '<td>'+tasks[k]['task']+'</td>';
                            content += '</tr>';
                        }
                        content += '</table></div>';
                    } else {
                        content = '<div class="am-calendar-header">' +
                            '<div id="date" style="float: left; width: 50%;">'+date+'</div>' +
                            '<div id="user">'+user+'</div>' +
                            '<div class="clearfix"></div>';
                        content += '<div><table>';
                        content += '<tr><td colspan="2">Задачи отсутствуют</td></tr>';
                        content += '</table></div>';
                    }


                    $("#am-calendar-"+i).html(content);
                });
            });

        }

        function getObserverCalendar(i) {
            user = $("#user_"+i+" option:selected").html();
            user_id = $("#user_"+i+" option:selected").val();
            date = $("#date_cont_"+i).val();
            result = '';

            $.ajax({
                url: '/admin/tasks/get-taks-am-calendar',
                async:false,
                data: {'date': date, 'user_id': user_id, 'user': user},
                success: function (resp) {
                    result = resp;
                }
            });

            return result;
        }

        function setDateCont(id, i) {
            date = $('#'+id).attr('data-content');
            $('#date_cont_'+i).val(date);
            tasks = getObserverCalendar(i);

            if (tasks[0] != '') {
                date = $("#date_cont_"+i).val();
                console.log(tasks[0]['id']);
                user = $("#user_"+i+" option:selected").html();
                content = '<div class="am-calendar-header">' +
                    '<div id="date" style="float: left; width: 50%;">'+date+'</div>' +
                    '<div id="user">'+user+'</div>' +
                    '<div class="clearfix"></div>';
                content += '<div><table>';
                for (k=0; k<tasks.length; k++) {
                    content += '<tr>';
                    content += '<td style="padding-right: 10px;">'+tasks[k]['time']+'</td>';
                    content += '<td>'+tasks[k]['task']+'</td>';
                    content += '</tr>';
                }
                content += '</table></div>';
            } else {
                content = '<div class="am-calendar-header">' +
                    '<div id="date" style="float: left; width: 50%;">'+date+'</div>' +
                    '<div id="user">'+user+'</div>' +
                    '<div class="clearfix"></div>';
                content += '<div><table>';
                content += '<tr><td colspan="2">Задачи отсутствуют</td></tr>';
                content += '</table></div>';
            }


            $("#am-calendar-"+i).html(content);
        }
        
        function sort_task(status) {
            switch(status) {
                case 'notExecute':
                    content = '';
                    $("#notExecute").removeClass('active');
                    $("#all").removeClass('active');

                    $("#notExecute").addClass('active');
                    $.ajax({
                        url: '/admin/projects/sort_task',
                        data: {'status': status},
                        success: function (resp) {
                            console.log(resp);
                            $("#project_tasks table tbody tr").remove();
                            for (i=0;i<resp.length;i++) {
                                content += '<tr>' +
                                        '<td style="vertical-align: middle;">'+resp[i].client+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].p_name+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].city+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].name+'</td>';
                                if(resp[i].prev_contact != null) {
                                    content += '<td style="vertical-align: middle;">'+resp[i].prev_contact+'</td>';
                                } else {
                                    content += '<td style="vertical-align: middle;">История контактов отсутствует</td>';
                                }
                                if(resp[i].next_contact != null) {
                                    content += '<td style="vertical-align: middle;">'+resp[i].next_contact+'</td>';
                                } else {
                                    content += '<td style="vertical-align: middle;">Не запланировано</td>';
                                }

                                if(resp[i].callbackstatus_ref_id == null) {
                                    if (resp[i].next_contact != null) {
                                        content += "<td style=\"vertical-align: middle;\"><a href='/admin/project-task/edit/"+resp[i].id+"' class='btn btn-primary'><i class='fa fa-search'></i></a></td>";
                                    } else {
                                        content += "<td style=\"vertical-align: middle;\"><a onclick='showCalendar("+(i+1)+", "+resp[i].id+");' class='btn btn-success'><i class='fa fa-calendar-plus-o'></i></a></td>";
                                    }
                                }
                                content += '</tr>';
                                content += '<tr style="display: none; background-color: #fff;" id="tr_'+(i+1)+'">' +
                                    '<td colspan="6">' +
                                    '<div class="box-body">'+
                                    '<div style="padding-bottom: 20px;">'+
                                    '<a id="date1" onclick="setDateCont(\'date1\', '+(i+1)+');" data-content="{{ $tomorrow_val }}" class="btn btn-primary">Завтра</a>'+
                                    '<a id="date2" onclick="setDateCont(\'date2\', '+(i+1)+');" data-content="{{ $plus_one_day_val }}" class="btn btn-primary">Послезавтра</a>'+
                                    '<a id="date3" onclick="setDateCont(\'date3\', '+(i+1)+');" data-content="{{ $plus_two_day_val }}" class="btn btn-primary">{{ $plus_two_day }}</a>'+
                                    '<a id="date4" onclick="setDateCont(\'date4\', '+(i+1)+');" data-content="{{ $plus_three_day_val }}" class="btn btn-primary">{{ $plus_three_day }}</a>'+
                                    '</div>'+
                                    '<div style="padding-bottom: 20px;">'+
                                    '<table class="table">'+
                                    '<tr>'+
                                    '<td style="vertical-align: middle;" width="25%">'+
                                    'Внедренец ' +
                                    '</td>'+
                                    '<td style="vertical-align: middle;" width="25%">'+
                                    '<select name="" id="user_'+(i+1)+'" class="form-control">'+
                                    '@if($observer[0] != "")' +
                                    '@foreach($observer as $o)'+
                                    '@if($o['default'] == 1)'+
                                    '<option value="{{ $o['id'] }}" selected>{{ $o['first_name'] }} {{ $o['last_name'] }}</option>'+
                                    '@else'+
                                    '<option value="{{ $o['id'] }}">{{ $o['first_name'] }} {{ $o['last_name'] }}</option>'+
                                    '@endif'+
                                    '@endforeach'+
                                    '@endif' +
                                    '</select>'+
                                    '</td>'+
                                    '<td rowspan="3" width="50%">'+
                                    '<div id="am-calendar-'+(i+1)+'"></div>'+
                                    '</td>'+
                                    '</tr>'+
                                    '<tr>'+
                                    '<td style="vertical-align: middle;">Дата</td>'+
                                    '<td style="vertical-align: middle;"><input type="date" class="form-control" value="{{ $cur_date_val }}" id="date_cont_'+(i+1)+'"></td>'+
                                    '<td></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                    '<td style="vertical-align: middle;">Время</td>'+
                                    '<td style="vertical-align: middle;"><input type="time" class="form-control" id="time_cont_'+(i+1)+'"></td>'+
                                    '<td></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                    '<td style="vertical-align: middle;"><a onclick="saveToCalendar('+resp[i].id+', '+(i+1)+');" class="btn btn-success">Запланировать</a></td>'+
                                    '</tr>'+
                                    '</table>'+
                                    '</div>'+
                                    '</div>' +
                                    '</td>' +
                                    '</tr>';
                            }
                            $("#project_tasks table tbody").append(content);
                        }
                    });
                    break;
                case 'all':
                    content = '';
                    $("#notExecute").removeClass('active');
                    $("#all").removeClass('active');

                    $("#all").addClass('active');
                    $.ajax({
                        url: '/admin/projects/sort_task',
                        data: {'status': status},
                        success: function (resp) {
                            $("#project_tasks table tbody tr").remove();
                            for (i=0;i<resp.length;i++) {
                                if (resp[i].tasksstatus_ref_id != 2) {
                                    content += '<tr>' +
                                        '<td style="vertical-align: middle;">'+resp[i].client+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].p_name+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].city+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].name+'</td>';
                                    if(resp[i].prev_contact != null) {
                                        content += '<td style="vertical-align: middle;">'+resp[i].prev_contact+'</td>';
                                    } else {
                                        content += '<td style="vertical-align: middle;">История контактов отсутствует</td>';
                                    }
                                    if(resp[i].next_contact != null) {
                                        content += '<td style="vertical-align: middle;">'+resp[i].next_contact+'</td>';
                                    } else {
                                        content += '<td style="vertical-align: middle;">Не запланировано</td>';
                                    }
                                    if(resp[i].callbackstatus_ref_id == null) {
                                        if (resp[i].next_contact != null) {
                                            content += "<td style=\"vertical-align: middle;\"><a href='/admin/project-task/edit/"+resp[i].id+"' class='btn btn-primary'><i class='fa fa-search'></i></a></td>";
                                        } else {
                                            content += "<td style=\"vertical-align: middle;\"><a onclick='showCalendar("+(i+1)+", "+resp[i].id+");' class='btn btn-success'><i class='fa fa-calendar-plus-o'></i></a></td>";
                                        }
                                    }
                                    content += '</tr>';
                                    content += '<tr style="display: none; background-color: #fff;" id="tr_'+(i+1)+'">' +
                                        '<td colspan="6">' +
                                        '<div class="box-body">'+
                                        '<div style="padding-bottom: 20px;">'+
                                        '<a id="date1" onclick="setDateCont(\'date1\', '+(i+1)+');" data-content="{{ $tomorrow_val }}" class="btn btn-primary">Завтра</a>'+
                                        '<a id="date2" onclick="setDateCont(\'date2\', '+(i+1)+');" data-content="{{ $plus_one_day_val }}" class="btn btn-primary">Послезавтра</a>'+
                                        '<a id="date3" onclick="setDateCont(\'date3\', '+(i+1)+');" data-content="{{ $plus_two_day_val }}" class="btn btn-primary">{{ $plus_two_day }}</a>'+
                                        '<a id="date4" onclick="setDateCont(\'date4\', '+(i+1)+');" data-content="{{ $plus_three_day_val }}" class="btn btn-primary">{{ $plus_three_day }}</a>'+
                                        '</div>'+
                                        '<div style="padding-bottom: 20px;">'+
                                        '<table class="table">'+
                                        '<tr>'+
                                        '<td style="vertical-align: middle;" width="25%">'+
                                        'Внедренец ' +
                                        '</td>'+
                                        '<td style="vertical-align: middle;" width="25%">'+
                                        '<select name="" id="user_'+(i+1)+'" class="form-control">'+
                                        '@if($observer[0] != "")' +
                                        '@foreach($observer as $o)'+
                                        '@if($o['default'] == 1)'+
                                        '<option value="{{ $o['id'] }}" selected>{{ $o['first_name'] }} {{ $o['last_name'] }}</option>'+
                                        '@else'+
                                        '<option value="{{ $o['id'] }}">{{ $o['first_name'] }} {{ $o['last_name'] }}</option>'+
                                        '@endif'+
                                        '@endforeach'+
                                        '@endif' +
                                        '</select>'+
                                        '</td>'+
                                        '<td rowspan="3" width="50%">'+
                                        '<div id="am-calendar-'+(i+1)+'"></div>'+
                                        '</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                        '<td style="vertical-align: middle;">Дата</td>'+
                                        '<td style="vertical-align: middle;"><input type="date" class="form-control" value="{{ $cur_date_val }}" id="date_cont_'+(i+1)+'"></td>'+
                                        '<td></td>'+
                                        '</tr>'+
                                        '<tr>'+
                                        '<td style="vertical-align: middle;">Время</td>'+
                                        '<td style="vertical-align: middle;"><input type="time" class="form-control" id="time_cont_'+(i+1)+'"></td>'+
                                        '<td></td>'+
                                        '</tr>'+
                                        '<tr>'+
                                        '<td style="vertical-align: middle;"><a onclick="saveToCalendar('+resp[i].id+', '+(i+1)+');" class="btn btn-success">Запланировать</a></td>'+
                                        '</tr>'+
                                        '</table>'+
                                        '</div>'+
                                        '</div>' +
                                        '</td>' +
                                        '</tr>';
                                } else {
                                    content += '<tr style="text-decoration: line-through;">' +
                                        '<td style="vertical-align: middle;">'+resp[i].client+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].p_name+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].city+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].name+'</td>';
                                    if(resp[i].prev_contact != null) {
                                        content += '<td style="vertical-align: middle;">'+resp[i].prev_contact+'</td>';
                                    } else {
                                        content += '<td style="vertical-align: middle;">История контактов отсутствует</td>';
                                    }
                                    if(resp[i].next_contact != null) {
                                        content += '<td style="vertical-align: middle;">'+resp[i].next_contact+'</td>';
                                    } else {
                                        content += '<td style="vertical-align: middle;">Не запланировано</td>';
                                    }
                                    if(resp[i].callbackstatus_ref_id == null) {
                                        if (resp[i].next_contact != null) {
                                            content += "<td style=\"vertical-align: middle;\"><a href='/admin/project-task/edit/"+resp[i].id+"' class='btn btn-primary'><i class='fa fa-search'></i></a></td>";
                                        } else {
                                            content += "<td style=\"vertical-align: middle;\"><a onclick='showCalendar("+i+", "+resp[i].id+");' class='btn btn-success'><i class='fa fa-calendar-plus-o'></i></a></td>";
                                        }
                                    }
                                    content += '</tr>';
                                    content += '<tr style="display: none; background-color: #fff;" id="tr_'+(i+1)+'">' +
                                        '<td colspan="6">' +
                                        '<div class="box-body">'+
                                        '<div style="padding-bottom: 20px;">'+
                                        '<a id="date1" onclick="setDateCont(\'date1\', '+(i+1)+');" data-content="{{ $tomorrow_val }}" class="btn btn-primary">Завтра</a>'+
                                        '<a id="date2" onclick="setDateCont(\'date2\', '+(i+1)+');" data-content="{{ $plus_one_day_val }}" class="btn btn-primary">Послезавтра</a>'+
                                        '<a id="date3" onclick="setDateCont(\'date3\', '+(i+1)+');" data-content="{{ $plus_two_day_val }}" class="btn btn-primary">{{ $plus_two_day }}</a>'+
                                        '<a id="date4" onclick="setDateCont(\'date4\', '+(i+1)+');" data-content="{{ $plus_three_day_val }}" class="btn btn-primary">{{ $plus_three_day }}</a>'+
                                        '</div>'+
                                        '<div style="padding-bottom: 20px;">'+
                                        '<table class="table">'+
                                        '<tr>'+
                                        '<td style="vertical-align: middle;" width="25%">'+
                                        'Внедренец ' +
                                        '</td>'+
                                        '<td style="vertical-align: middle;" width="25%">'+
                                        '<select name="" id="user_'+(i+1)+'" class="form-control">'+
                                        '@if($observer[0] != "")' +
                                        '@foreach($observer as $o)'+
                                        '@if($o['default'] == 1)'+
                                        '<option value="{{ $o['id'] }}" selected>{{ $o['first_name'] }} {{ $o['last_name'] }}</option>'+
                                        '@else'+
                                        '<option value="{{ $o['id'] }}">{{ $o['first_name'] }} {{ $o['last_name'] }}</option>'+
                                        '@endif'+
                                        '@endforeach'+
                                        '@endif' +
                                        '</select>'+
                                        '</td>'+
                                        '<td rowspan="3" width="50%">'+
                                        '<div id="am-calendar-'+(i+1)+'"></div>'+
                                        '</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                        '<td style="vertical-align: middle;">Дата</td>'+
                                        '<td style="vertical-align: middle;"><input type="date" class="form-control" value="{{ $cur_date_val }}" id="date_cont_'+(i+1)+'"></td>'+
                                        '<td></td>'+
                                        '</tr>'+
                                        '<tr>'+
                                        '<td style="vertical-align: middle;">Время</td>'+
                                        '<td style="vertical-align: middle;"><input type="time" class="form-control" id="time_cont_'+(i+1)+'"></td>'+
                                        '<td></td>'+
                                        '</tr>'+
                                        '<tr>'+
                                        '<td style="vertical-align: middle;"><a onclick="saveToCalendar('+resp[i].id+', '+(i+1)+');" class="btn btn-success">Запланировать</a></td>'+
                                        '</tr>'+
                                        '</table>'+
                                        '</div>'+
                                        '</div>' +
                                        '</td>' +
                                        '</tr>';
                                }
                            }
                            $("#project_tasks table tbody").append(content);
                        }
                    });
                    break;
            }
        }

        function saveToCalendar(tid, i) {
            date = $('#date_cont_'+i).val();
            time = $('#time_cont_'+i).val();
            user_id = $("#user_"+i+" option:selected").val();

            if (!date) {
                alert('Укажите дату');
                return false;
            }

            if (!time) {
                alert('Укажите время');
                return false;
            }

            $.ajax({
                url: '/admin/calendar/add-project-task',
                data: {'tid': tid, 'date': date, 'time': time, 'user_id': user_id},
                success: function (resp) {
                    if (resp.status == 'Ok') {
                        location.reload();
                    } else {
                        console.log(resp.error);
                        alert(resp.error);
                    }
                }
            });
        }
    </script>
@endpush