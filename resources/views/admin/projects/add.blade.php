@extends('layouts.cabinet')

@section('title')
    Мои проекты: Добавление нового проекта
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="box" style="padding: 10px;">
        <form action="/admin/projects/add" method="post" id="formAddProject">
            {{ csrf_field() }}
            <input type="hidden" name="contacts" id="contacts">
            <input type="hidden" name="row_modules" id="row_modules">
            <input type="hidden" name="module_rule" id="module_rule">
            <input type="hidden" name="data" id="data">
            <input type="hidden" name="documents" id="documents">
            <div class="form-group">
                <label for="projectName">Наименование клиента</label>
                <select name="client" id="clientList" class="form-control">
                    @foreach($clients as $c)
                    <option value="{{ $c->id }}">{{ $c->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="projectName">Название проекта</label>
                <input type="text" class="form-control" name="projectName" required>
            </div>
            <table class="table">
                <tbody>
                    <tr>
                        <td style="vertical-align: middle;"><label>Дата начала</label></td>
                        <td style="vertical-align: middle;"><input type="date" name="startDate" class="form-control" required></td>
                        <td style="vertical-align: middle;"><label>Дата окончания</label></td>
                        <td style="vertical-align: middle;"><input type="date" name="finishDate" class="form-control" required></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div class="form-group" style="margin-top: 10px;">
                                <input type="checkbox" name="optionsRadios" id="optionsRadios1">
                                <label for="optionsRadios1">
                                    <h4 style="color: #3c8dbc;">Клиент выслал комплект наших документов нам обратно?</h4>
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <div class="form-group" style="margin-top: 10px;">
                                <input type="radio" name="transComp" id="PostOfRussia" value="PostOfRussia" disabled>
                                <label for="PostOfRussia">
                                    <h4 style="color: #3c8dbc;">Почта России</h4>
                                </label>
                            </div>
                        </td>
                        <td >
                            <div class="form-group" style="margin-top: 10px;">
                                <input type="radio" name="transComp" id="otherCompany" value="otherCompany" disabled>
                                <label for="otherCompany">
                                    <h4 style="color: #3c8dbc;">Другая компания</h4>
                                </label>
                            </div>
                        </td>
                        <td style="vertical-align: middle;">
                            <label>Название компании</label>
                        </td>
                        <td style="vertical-align: middle;">
                            <div class="form-group">
                                <input type="text" class="form-control" name="companyName" disabled>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <label>Идентификатор письма</label>
                        </td>
                        <td>
                            <div class="form-group">
                                <input type="text" class="form-control" name="MessageId" disabled>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;"><label>Ответственный внедренец</label></td>
                        <td style="vertical-align: middle;">
                            <select name="user" class="form-control">
                                @foreach($users as $u)
                                    @if ($u->id == $cur_user->id)
                                        <option value="{{ $u->id }}" selected>{{ $u->last_name }} {{ $u->first_name }}</option>
                                    @else
                                        <option value="{{ $u->id }}">{{ $u->last_name }} {{ $u->first_name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </td>
                        <td style="vertical-align: middle;">
                            <a onclick="showWorkload();" style="cursor: pointer;">Посмотреть загрузку внедренцев</a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div id="workload" style="display: none;"></div>
                        </td>
                    </tr>
                </tbody>
            </table>

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Контакты</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table table-striped" id="tableContacts">
                        <tbody><tr>
                            <th>Фамилия</th>
                            <th>Имя</th>
                            <th>Отчество</th>
                            <th>Должность</th>
                            <th>Телефон</th>
                            <th>E-mail</th>
                            <th>Ответственный за внедрение</th>
                            <th>Отправлять отчет на почту</th>
                            <th colspan="2">Действия</th>
                        </tr>
                        <tr id="contact_row"></tr>
                        <tr>
                            <td><input id="contact_surname" type="text" class="form-control"></td>
                            <td><input id="contact_name" type="text" class="form-control"></td>
                            <td><input id="contact_patronymic" type="text" class="form-control"></td>
                            <td>
                                <select class="form-control" id="contact_post">
                                    @foreach($posts as $p)
                                        <option id="{{ $p->id }}" value="{{ $p->name }}">{{ $p->name }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td><input id="contact_phone" type="text" class="form-control"></td>
                            <td><input id="contact_email" type="text" class="form-control"></td>
                            <td></td>
                            <td><input type="checkbox" id="mail_rep"></td>
                            <td>
                                <a class="btn btn-primary" id="saveContact">
                                    <i class="fa fa-check"></i>
                                </a>
                            </td>
                        </tr></tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>

            <div class="box">
                <table>
                    <tr>
                        <td>
                            <div class="box-header">
                                <h3 class="box-title">
                                    Модули
                                </h3>
                            </div>
                        </td>
                        <td>
                            <select class="form-control" id="modules">
                                @foreach($modules as $m)
                                    <option id="{{ $m->id }}" value="{{ $m->name }}">{{ $m->name }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td style="padding-left: 10px;">
                            <input type="number" min="0" name="modules_num" id="modules_num" placeholder="Кол-во" class="form-control" required>
                        </td>
                        <td style="padding-left: 10px;">
                            <a id="addModule" class="btn btn-primary">Добавить</a>
                        </td>
                    </tr>
                </table>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table table-bordered table-striped" style="width: 50%" id="tableModule">
                        <thead>
                        <tr>
                            <th>Название</th>
                            <th>Кол-во модулей</th>
                            <th>Кол-во контактов</th>
                            <th>с учетом запаса</th>
                            <th>Кол-во форм/отчетов</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr id="modules_list"></tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div id="update-block" class="box" style="margin-top: 30px; display: none;">
                    <div class="box-body no-padding">
                        <table class="table table-striped" id="tableModuleTasks">
                            <thead>
                            <tr>
                                <th>Список задач</th>
                                <th>Норма кол-ва контактов по договору 1 контакт - 1 час</th>
                                <th>Кол - во разрешенных контактов</th>
                                <th>Группа задач</th>
                                <th>Категория формы (только для группы задач "Печатные формы/отчеты")</th>
                                <th colspan="2">Действия</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyTasks">
                            <tr id="task_list"></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        Документы по проекту
                    </h3>
                </div>

                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="alert alert-danger alert-dismissible" id="error" style="display: none;">
                        <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>-->
                        <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                        <span id="error-text"></span>
                    </div>
                    <table class="table table-bordered table-striped" style="width: 50%" id="tableModule">
                        <thead>
                        <tr>
                            <th>Тип документа</th>
                            <th>Документ</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($documents as $d)
                        <tr id="doc_{{ $loop->index+1 }}" class="documents">
                            <td class="doc_isFirst" style="display: none;">{{ $d['isFirst'] }}</td>
                            <td class="doc_type">{{ $d['type'] }}</td>
                            <td class="doc_name">{{ $d['name'] }}</td>
                            <td>
                                <a class="btn btn-danger" onclick="delDocuments({{ $loop->index+1 }});"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>

            <div class="form-group">
                <input type="submit" value="Сохранить" class="btn btn-success">
                <a href="/admin/projects" class="btn btn-default">Отмена</a>
            </div>
        </form>
    </div>
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection

@push('scripts')
    <script>
        $("#formAddProject").validate({
            messages: {
                projectName: {
                    required: "Это поле обязательно к заполнению",
                    minlength: "Введите не менее 2-х символов"
                },
                startDate: {
                    required: "Это поле обязательно к заполнению"
                },
                finishDate: {
                    required: "Это поле обязательно к заполнению"
                },
                modules_num: {
                    required: "Это поле обязательно к заполнению"
                },
                email: {
                    required: "Поле 'Email' обязательно к заполнению",
                    email: "Необходим формат адреса email"
                },
                url: "Поле 'Сайт' обязательно к заполнению"
            }
        });
        var res = [];
        var res2 = {};
        var data = [];
        var obj = {};
        var doc = {};
        var doc_ar = [];

        $(document).ready(function () {
            doc_length = $(".documents").length;
            for(k=1;k<=doc_length;k++) {
                type = $("tr#doc_"+k+" .doc_type").html();
                doc_name = $("tr#doc_"+k+" .doc_name").html();
                is_first = $("tr#doc_"+k+" .doc_isFirst").html();

                doc = {};
                doc = {
                    'id': k,
                    'name': doc_name,
                    'type': type,
                    'isFirst': is_first
                };
                doc_ar.push(doc);
            }

            $("select[name~='user']").change(function () {
                user_id = $("select[name~='user'] option:selected").val();
                user_name = $("select[name~='user'] option:selected").html();

                $.ajax({
                    url: "/admin/user/getUserProjects",
                    data: {'id': user_id},
                    method: "POST",
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function () {
                        $("#workload").html('Загрузка ...');
                    },
                    success: function (resp) {
                        contentWorkload = "<table>" +
                            "<tr>" +
                            "<td>Внедренец:&nbsp;&nbsp;<td>" +
                            "<td>"+user_name+"<td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Активных проектов:&nbsp;&nbsp;<td>" +
                            "<td>"+resp.projects+"<td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Общее время задач:&nbsp;&nbsp;<td>" +
                            "<td>"+resp.tasks+"<td>" +
                            "</tr>" +
                            "</table>";
                        $("#workload").html(contentWorkload);
                    }
                });
            });

            $("#saveContact").click(function () {
                newContact();
            });

            $("#optionsRadios1").click(function () {
                if ($(this).prop('checked')) {
                    $("#PostOfRussia").removeAttr('disabled');
                    $("#otherCompany").removeAttr('disabled');
                    $("input[name~='MessageId']").removeAttr('disabled');
                    $("input[name~='companyName']").removeAttr('disabled');
                } else {
                    $("#PostOfRussia").attr('disabled', 'true');
                    $("#otherCompany").attr('disabled', 'true');
                    $("input[name~='MessageId']").attr('disabled', 'true');
                    $("input[name~='companyName']").attr('disabled', 'true');
                }
            });

            $("input[name~='transComp']").on('change', function() {
                company = $("input[name~='transComp']:checked").val();

                if (company == 'otherCompany') {
                    $("input[name~='companyName']").removeAttr('disabled');
                } else {
                    $("input[name~='companyName']").attr('disabled', 'true');
                    res2 = {};
                    res2.companyName = company;
                }
            });

            getClients();

            $('#clientList').change(function () {
                $(".contacts").remove();
                getClients();
            });

            $("#addModule").click(function () {
                num = $("#modules_num").val();
                id = $('#modules option:selected').attr('id');
                i = 0;

                if (!num) {
                    num = 1;
                }

                $.ajax({
                    url: '/admin/module-rule/getRule',
                    method: 'POST',
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {'id': id},
                    success: function (resp) {
                        console.log(resp);
                        i = $(".row_index").length;
                        id = $("#modules option:checked").attr('id');
                        $("#modules_list").before('<tr class="row_index" id='+(i+1)+'>' +
                            '<td style="display: none;"><span class="module_id">'+id+'</span></td>' +
                            '<td>'+$("#modules").val()+'</td>' +
                            '<td><span class="module_num">'+num+'</span></td>' +
                            '<td>'+resp.norm_contacts+'</td>' +
                            '<td>'+resp.number_contacts+'</td>' +
                            '<td>'+resp.num_printform+'</td>' +
                            '<td><a class="btn btn-primary" onclick="update_norm('+id+');">Уточнить нормы</a></td>' +
                            '</tr>');
                        i++;
                    },
                    error: function () {
                        alert("Ошибка заполнения справочника");
                    }
                });
            });
        });

        function getClients() {
            client_id = $('#clientList option:selected').attr('value');
            $.ajax({
                url: '/admin/contact/get-contact',
                async: false,
                data: {'id': client_id},
                success: function (resp) {
                    for(i=0; i<resp.length; i++) {
                        content = '';
                        content = '<tr id="'+(i+1)+'" class="contacts">' +
                            '<td style="display: none;"><span name="contacts_id">'+resp[i].id+'</span></td>' +
                            '<td><span name="contacts_surname">'+resp[i].last_name+'</span></td>' +
                            '<td><span name="contacts_name">'+resp[i].first_name+'</span></td>' +
                            '<td><span name="contacts_patronymic">'+resp[i].patronymic+'</span></td>' +
                            '<td><span name="contacts_post">'+resp[i].post+'</span></td>' +
                            '<td><span name="contacts_phone">'+resp[i].phone+'</span></td>' +
                            '<td><span name="contacts_email">'+resp[i].email+'</span></td>' +
                            '<td><input type="radio" name="main"></td>' +
                            '<td><input type="checkbox" name="mail_rep"></td>' +
                            '<td><a class="btn btn-danger" onclick="delContact('+(i+1)+')"><i class="fa fa-trash"></i></a></td>' +
                            '</tr>';
                        $("#contact_row").before(content);
                    }
                }
            });
        }

        function delContact(id) {
            $('#'+id).remove();
        }

        function update_norm(id) {
            $("#tbodyTasks").text('');
            $("#tbodyTasks").append('<tr id="task_list"></tr>');
            $.ajax({
                'url': '/admin/module-rule/getTasks',
                'data': {'id': id},
                success: function (resp) {
                    for (i=0; i<resp.length; i++) {
                        content = '<tr class="row_index" id="'+(i+1)+'">' +
                            '<td style="display: none;" id="task_id"><span>'+resp[i].id+'</span></td>' +
                            '<td id="task_'+(i+1)+'"><span>'+resp[i].task+'</span></td>' +
                            '<td id="norm_'+(i+1)+'"><span>'+resp[i].norm_contacts+'</span></td>' +
                            '<td id="number_'+(i+1)+'"><span>'+resp[i].number_contacts+'</span></td>' +
                            '<td id="group_tasks_'+(i+1)+'"><span>'+resp[i].grouptasks+'</span></td>' +
                            '<td id="categoryPrintForm_'+(i+1)+'"></td>' +
                            '<td id="edit_rule_'+(i+1)+'"><a class="btn btn-primary" onclick="editNorm('+resp[i].id+', '+(i+1)+');"><i class="fa fa-pencil"></i></a></td>' +
                            '<td><a class="btn btn-danger" onclick="delNorm('+(i+1)+');"><i class="fa fa-trash"></i></a></td>' +
                            '</tr>';
                        $("#task_list").before(content);
                    }

                }
            });
            $.ajax({
                'url': '/admin/group-tasks/get-task-groups',
                success: function (resp) {
                    console.log(resp);
                    content2 = '<tr id="addTaskRow">' +
                            '<td><input id="taskName" type="text" class="form-control"></td>' +
                            '<td><input id="taskNorm" type="number" class="form-control"></td>' +
                            '<td><input id="taskCol" type="number" class="form-control"></td>' +
                            '<td><select id="taskGroup" onchange="getCategoryForm()" class="form-control">';
                    for (l=0; l<resp.length; l++) {
                        content2 += '<option value="'+resp[l].id+'">'+resp[l].name+'</option>';
                    }
                    content2 += '</select></td>' +
                            '<td id="categoryPrintForm"></td>' +
                            '<td><a onclick="addRowNorm();" class="btn btn-primary"><i class="fa fa-check"></a></td>' +
                        '</tr>';
                    $("#task_list").after(content2);
                }
            });
            $( "#update-block" ).slideToggle( "fast");
        }

        function delNorm(id) {
            $("#tableModuleTasks #"+id).remove();
        }

        function getCategoryForm() {
            task = $("#taskGroup option:selected").val();
            content = '';
            if (task == 11) {
                $.ajax({
                    url: '/admin/group-tasks/get-category-printform',
                    success: function (resp) {
                        console.log(resp);
                        content = '<select class="form-control" id="category">';
                        for (i=0;i<resp.length;i++) {
                            content += '<option id="'+resp[i].id+'" value="'+resp[i].id+'">'+resp[i].category+'</option>';
                        }
                        content += '</select>';
                        $("#categoryPrintForm").html(content);
                    }
                });
            } else {
                $("#categoryPrintForm").html('');
            }
        }

        function getCategoryForm2(id) {
            task = $("#inputGroupTask option:selected").val();
            content = '';
            if (task == 11) {
                $.ajax({
                    url: '/admin/group-tasks/get-category-printform',
                    success: function (resp) {
                        console.log(resp);
                        content = '<select class="form-control" id="category">';
                        for (i=0;i<resp.length;i++) {
                            content += '<option id="'+resp[i].id+'" value="'+resp[i].id+'">'+resp[i].category+'</option>';
                        }
                        content += '</select>';
                        $("#categoryPrintForm_"+id).html(content);
                    }
                });
            } else {
                $("#categoryPrintForm_"+id).html('');
            }
        }

        function addRowNorm() {
            id = $(".row_index").length;
            name = $("#addTaskRow #taskName").val();
            norm = $("#addTaskRow #taskNorm").val();
            col = $("#addTaskRow #taskCol").val();
            norm_form = $("#addTaskRow #category option:selected").text();
            g_task = $("#addTaskRow #taskGroup option:selected").text();
            content = '<tr class="row_index" id="'+id+'">' +
                    '<td style="display: none;" id="task_id"><span>0</span></td>' +
                    '<td id="task_'+id+'"><span>'+name+'</span></td>' +
                    '<td id="norm_'+id+'"><span>'+norm+'</span></td>' +
                    '<td id="number_'+id+'"><span>'+col+'</span></td>' +
                    '<td id="group_tasks_'+id+'"><span>'+g_task+'</span></td>' +
                    '<td id="categoryPrintForm_'+id+'"><span>'+norm_form+'</span></td>' +
                    '<td id="edit_rule_'+id+'"><a class="btn btn-primary" onclick="editNorm(0, '+id+');"><i class="fa fa-pencil"></i></a></td><td><a onclick="delNorm('+id+')" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>' +
                '</tr>';
            $('#task_list').before(content);
        }

        function editNorm(id, loop) {
            active_btn = $('.btn-success').length;
            if ((active_btn-1) >= 1) {
                alert('Имеются не сохраненные данные. Сохраните сначала изменения');
            } else {
                $.ajax({
                    'url': '/admin/group-tasks/get-task-groups',
                    success: function (resp) {
                        console.log(resp);
                        task = $("#task_"+loop+' > span').text();
                        norm = $("#norm_"+loop+' > span').text();
                        number = $("#number_"+loop+' > span').text();
                        group_tasks = $("#group_tasks_"+loop+' > span').text();

                        group_tasks_content2 = '<select id="inputGroupTask" onchange="getCategoryForm2('+loop+')" class="form-control">';
                        for (m=0; m<resp.length; m++) {
                            group_tasks_content2 += '<option value="'+resp[m].id+'">'+resp[m].name+'</option>';
                        }
                        group_tasks_content2 += '</select>';

                        $("#task_"+loop).html('<input id="inputTask" type="text" class="form-control" value="'+task+'">');
                        $("#norm_"+loop).html('<input id="inputNorm" type="text" class="form-control" value="'+norm+'">');
                        $("#number_"+loop).html('<input id="inputNumber" type="text" class="form-control" value="'+number+'">');
                        $("#group_tasks_"+loop).html(group_tasks_content2);
                        $("#group_tasks_"+loop).html();
                        $("#edit_rule_"+loop).html('<a onclick="saveRule('+id+', '+loop+');" class="btn btn-success"><i class="fa fa-check"></i></a>');
                    }
                });
            }
        }

        function saveRule(id, loop) {
            task = $("#inputTask").val();
            norm = $("#inputNorm").val();
            number = $("#inputNumber").val();
            group_tasks = $("#inputGroupTask option:selected").text();
            category_norm = $("#category option:selected").text();
            module_id = $("#module_id").val();

            obj = {
                'id': id,
                'task': task,
                'norm': norm,
                'number': number,
                'group_tasks': group_tasks
            };
            res.push(obj);
            console.log(obj);
            console.log(res);

            $("#task_"+loop).html('<span name="task_'+loop+'">'+task+'</span>');
            $("#norm_"+loop).html('<span name="norm_'+loop+'">'+norm+'</span>');
            $("#number_"+loop).html('<span name="number_'+loop+'">'+number+'</span>');
            $("#group_tasks_"+loop).html('<span name="group_tasks_'+loop+'">'+group_tasks+'</span>');
            $("#categoryPrintForm_"+loop).html('<span name="categoryPrintForm_'+loop+'">'+category_norm+'</span>');
            $("#edit_rule_"+loop).html('<a onclick="editNorm('+id+', '+loop+');" class="btn btn-primary"><i class="fa fa-pencil"></i></a>');
        }

        function newContact() {
            name = $("#contact_name").val();
            surname = $("#contact_surname").val();
            patronymic = $("#contact_patronymic").val();
            post = $("#contact_post").val();
            phone = $("#contact_phone").val();
            email = $("#contact_email").val();
            mail_rep = $("#mail_rep").prop('checked');
            er = 0;
            i = $("#tableContacts tr").length-2;
            var contact = [];

            if (!surname) {
                surname = '';
            }

            if (!patronymic) {
                patronymic = '';
            }

            if (!email) {
                email = '';
            }

            if (!name) {
                er++;
                alert('Введите имя контакта');
            }

            if (!post) {
                er++;
                alert('Введите должность контакта');
            }

            if (!phone) {
                er++;
                alert('Введите номер телефона контакта');
            }

            if (er == 0) {
                if(mail_rep == true) {
                    $("#contact_row").before('<tr id="'+i+'" class="contacts">' +
                        '<td><span name="contacts_surname">'+surname+'</span></td>' +
                        '<td><span name="contacts_name">'+name+'</span></td>' +
                        '<td><span name="contacts_patronymic">'+patronymic+'</span></td>' +
                        '<td><span name="contacts_post">'+post+'</span></td>' +
                        '<td><span name="contacts_phone">'+phone+'</span></td>' +
                        '<td><span name="contacts_email">'+email+'</span></td>' +
                        '<td><input type="radio" name="main"></td>' +
                        '<td><input type="checkbox" name="mail_rep" checked></td>' +
                        '<td><a class="btn btn-danger" onclick="delContact('+i+')"><i class="fa fa-trash"></i></a></td>' +
                        '</tr>');
                } else {
                    $("#contact_row").before('<tr id="'+i+'" class="contacts">' +
                        '<td><span name="contacts_surname">'+surname+'</span></td>' +
                        '<td><span name="contacts_name">'+name+'</span></td>' +
                        '<td><span name="contacts_patronymic">'+patronymic+'</span></td>' +
                        '<td><span name="contacts_post">'+post+'</span></td>' +
                        '<td><span name="contacts_phone">'+phone+'</span></td>' +
                        '<td><span name="contacts_email">'+email+'</span></td>' +
                        '<td><input type="radio" name="main"></td>' +
                        '<td><input type="checkbox" name="mail_rep"></td>' +
                        '<td><a class="btn btn-danger" onclick="delContact('+i+')"><i class="fa fa-trash"></i></a></td>' +
                        '</tr>');
                }

                i++;



                $("#contact_name").val('');
                $("#contact_surname").val('');
                $("#contact_patronymic").val('');
                $("#contact_post").val('');
                $("#contact_phone").val('');
                $("#contact_email").val('');
            }
        }

        function bouncer(arr) {
            return arr.filter( function(v){return !(v !== v);});
        }

        $("#formAddProject").submit(function () {
            rowContacts = $("#tableContacts tr").length-3;
            rowModules = $("#tableModule .row_index").length;
            check = $("#optionsRadios1").prop('checked');
            company = $("input[name~='transComp']:checked").val();
            var contact = [];
            var modules = [];

            for(i = 1; i <= rowContacts; i++) {
                cont_id = $('#'+i+' > td > span[name~="contacts_id"]').text();
                first_name = $('#'+i+' > td > span[name~="contacts_name"]').text();
                last_name = $('#'+i+' > td > span[name~="contacts_surname"]').text();
                patronymic = $('#'+i+' > td > span[name~="contacts_patronymic"]').text();
                post = $('#'+i+' > td > span[name~="contacts_post"]').text();
                phone = $('#'+i+' > td > span[name~="contacts_phone"]').text();
                email = $('#'+i+' > td > span[name~="contacts_email"]').text();
                main = $('#'+i+' > td > input[name~="main"] ').prop("checked");
                email_rep = $('#'+i+' > td > input[name~="mail_rep"] ').prop("checked");

                contact.push({
                    'id': cont_id,
                    'first_name': first_name,
                    'last_name': last_name,
                    'patronymic': patronymic,
                    'post': post,
                    'phone': phone,
                    'email': email,
                    'main': main,
                    'email_rep': email_rep
                });
            }

            for (j = 1; j <= rowModules; j++) {
                mod_id = $('#tableModule #'+j+' .module_id').text();
                num = $('#tableModule #'+j+' .module_num').text();

                modules.push({
                    'id': mod_id,
                    'num': num
                });
            }

            res2 = {};
            res2.check = check;

            if (check) {
                if (company == 'otherCompany') {
                    companyName = $("input[name~='companyName']").val();
                    res2.companyName = companyName;
                } else {
                    res2.companyName = company;
                }
                messageId = $("input[name~='MessageId']").val();
                res2.messageId = messageId;
            }

            data = [];
            data.push(res);

            $("#contacts").val(JSON.stringify(contact));
            $("#row_modules").val(JSON.stringify(modules));
            $("#module_rule").val(JSON.stringify(res));
            $("#data").val(JSON.stringify(data));
            $("#documents").val(JSON.stringify(doc_ar));

            return true;
        });

        function delDocuments(doc_id) {
            $("#error").css('display', 'none');
            for (i=0;i<doc_ar.length;i++) {
                if (doc_ar[i].id == doc_id) {
                    if (doc_ar[i].isFirst != 1) {
                        doc_ar.splice(i, 1);
                        $("tr#doc_"+doc_id).remove();
                    } else {
                        $("#error-text").html('Этот документ является первым. Его нельзя удалять');
                        $("#error").css('display', 'block');
                        break;
                    }
                }
            }
        }

        function showWorkload() {
            $("#workload").slideToggle("fast", function () {
                if ($("#workload").css('display') != 'none') {
                    user_id = $("select[name~='user'] option:selected").val();
                    user_name = $("select[name~='user'] option:selected").html();

                    $.ajax({
                        url: "/admin/user/getUserProjects",
                        data: {'id': user_id},
                        method: "POST",
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        },
                        beforeSend: function () {
                            $("#workload").html('Загрузка ...');
                        },
                        success: function (resp) {
                            contentWorkload = "<table>" +
                                "<tr>" +
                                "<td>Внедренец:&nbsp;&nbsp;<td>" +
                                "<td>"+user_name+"<td>" +
                                "</tr>" +
                                "<tr>" +
                                "<td>Активных проектов:&nbsp;&nbsp;<td>" +
                                "<td>"+resp.projects+"<td>" +
                                "</tr>" +
                                "<tr>" +
                                "<td>Общее время задач:&nbsp;&nbsp;<td>" +
                                "<td>"+resp.tasks+"<td>" +
                                "</tr>" +
                                "</table>";
                            $("#workload").html(contentWorkload);
                        }
                    });
                }
            });
        }
    </script>
@endpush