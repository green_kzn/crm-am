@extends('layouts.cabinet')

@section('title')
    Клиент: {{ $client->name }}<br>
    Проект: {{ $project->name }}
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div id="contactOk" class="alert alert-success alert-dismissible" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
        Данные о контакте успешно обновлены
    </div>
    <div id="contactError" class="alert alert-danger alert-dismissible" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
        Произошла ошибка во время обновления контакта
    </div>
    <input type="hidden" value="{{ $project->id }}" id="project_id">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#history" data-toggle="tab" aria-expanded="true">История внедрения</a></li>
            <li class=""><a href="#contacts" data-toggle="tab" aria-expanded="false">Контакты</a></li>
            <li class=""><a href="#tasks" data-toggle="tab" aria-expanded="false">Список задач</a></li>
            <li class=""><a href="#card" data-toggle="tab" aria-expanded="false">Карточка проекта</a></li>
            <li class=""><a href="#documents" data-toggle="tab" aria-expanded="false">Документы</a></li>
            <li class=""><a href="#forms" data-toggle="tab" aria-expanded="false">Формы</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="history">
                <div style="padding-bottom: 20px; display: block;" id="add">
                    <a href="" class="btn btn-primary" id="click_mes_form">Добавить новый коментарий</a>
                </div>
                <div style="padding-bottom: 20px; display: none;" id="addComment">
                    <a class="btn btn-primary" id="addCommentButton" style="display: none; float: left; margin-right: 10px;">Добавить</a>&nbsp;
                    <a href="" class="btn btn-primary" style="display: none; float: left;">Отмена</a>
                </div>
                <div class="clear"></div>
                <div class="wall_form" id="popup_message_form" style="display:none; margin-bottom: 20px;">
                    <label id="editor-error" class="error" for="editor" style="display: none;">Это поле обязательно к заполнению</label>
                    <div id="editor" name="editor" required></div>
                </div>
                @if($comments)
                    @foreach($comments as $c)
                        <div class="post">
                            <div class="user-block">
                                <img class="img-circle img-bordered-sm" src="{{ asset('public/avatars/'.$c['user_foto']) }}" alt="user image">
                                <span class="username">
                                    <a href="#">{{ $c['user_name'] }} {{ $c['user_surname'] }}</a>
                                    @if($c['autor'])
                                        <a onclick="editComment({{ $c['id'] }}, '{{ $c['text'] }}')" class="pull-right btn-box-tool"><i class="fa fa-pencil"></i></a>
                                        <a href="/admin/comment/del/{{ $c['id'] }}" class="pull-right btn-box-tool"><i class="fa fa-trash"></i></a>
                                    @endif
                                </span>
                                @if($c['for_client'] == 1)
                                    <span class="description">Опубликовано - {{ $c['time'] }} {{ $c['date'] }} (Для клиента)</span>
                                @else
                                    <span class="description">Опубликовано - {{ $c['time'] }} {{ $c['date'] }}</span>
                                @endif
                            </div>
                            <!-- /.user-block -->
                            <div id="comment_{{ $c['id'] }}">
                                {{ $c['text'] }}
                            </div>
                            <!--<ul class="list-inline">
                                <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Share</a></li>
                            </ul>

                            <input class="form-control input-sm" type="text" placeholder="Type a comment">-->
                        </div>
                    @endforeach
                @else
                    <p style="text-align: center">Коментарии отсутствуют</p>
                @endif
            </div>
            <div class="tab-pane" id="contacts">
                <div style="padding-bottom: 20px; display: block;" id="addCont">
                    <a href="" class="btn btn-primary" id="click_contact_add">Добавить контакт</a>
                </div>
                <form action="/admin/contact/add" method="get" id="addContact" style="padding-bottom: 20px; display: none;">
                    {{ csrf_field() }}
                    <input type="hidden" name="p_id" value="{{ $project->id }}">
                    <div id="form_button">
                        <input type="submit" class="btn btn-primary" value="Добавить" style="display: none; float: left; margin-right: 10px;">&nbsp
                        <a onclick="closeContactForm()" class="btn btn-primary" style="display: none; float: left;">Отмена</a>
                    </div>
                    <div class="clear"></div>
                    <div class="wall_form" id="popup_contact_form" style="display:none; margin-top: 20px; margin-bottom: 20px;">
                        <table>
                            <tr>
                                <td>
                                    <div class="form-group" id="Name" style="margin-right: 20px;">
                                        <label class="control-label" for="inputName" style="display: none;"></label>
                                        <input type="text" class="form-control" name="contact_name" id="inputName" placeholder="Имя" required>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group" id="LastName" style="margin-right: 20px;">
                                        <label class="control-label" for="inputLastName" style="display: none;"></label>
                                        <input type="text" class="form-control" name="contact_surname" id="inputLastName" placeholder="Фамилия" required>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group" id="Patronymic" style="margin-right: 20px;">
                                        <label class="control-label" for="inputPatronymic" style="display: none;"></label>
                                        <input type="text" class="form-control" name="contact_patronymic" id="inputPatronymic" placeholder="Отчество" required>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group" id="Post" style="margin-right: 20px;">
                                        <label class="control-label" for="inputPost" style="display: none;"></label>
                                        <select name="contact_post" id="inputPost" class="form-control"></select>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group" id="Phone" style="margin-right: 20px;">
                                        <label class="control-label" for="inputPhone" style="display: none;"></label>
                                        <input type="tel" class="form-control" name="contact_phone" id="inputPhone" placeholder="Телефон" required>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group" id="Email" style="margin-right: 20px;">
                                        <label class="control-label" for="inputEmail" style="display: none;"></label>
                                        <input type="email" class="form-control" name="contact_email" id="inputEmail" placeholder="E-mail" required>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </form>
                @if(count($contacts) > 0)
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody><tr>
                                <th>№</th>
                                <th>Фамилия</th>
                                <th>Имя</th>
                                <th>Отчество</th>
                                <th>Должность</th>
                                <th>Телефон</th>
                                <th>Email</th>
                                <th>Действия</th>
                            </tr>
                            @if($contacts[0] != '')
                            @foreach($contacts as $c)
                                <tr>
                                    <td style="vertical-align: middle;">{{ $loop->index+1 }}</td>
                                    <td style="vertical-align: middle;"><span id="contactSurname">{{ $c['last_name'] }}</span></td>
                                    <td style="vertical-align: middle;"><span id="contactName">{{ $c['first_name'] }}</span></td>
                                    <td style="vertical-align: middle;"><span id="contactPatronymic">{{ $c['patronymic'] }}</span></td>
                                    <td style="vertical-align: middle;"><span id="contactPost">{{ $c['post'] }}</span></td>
                                    <td style="vertical-align: middle;"><span id="contactPhone"><a href="tel:{{ $c['phone'] }}">{{ $c['phone'] }}</a></span></td>
                                    <td style="vertical-align: middle;"><span id="contactEmail"><a href="mailto:{{ $c['email'] }}">{{ $c['email'] }}</a></span></td>
                                    <td style="vertical-align: middle;">
                                        <a href="/admin/contact/del/{{ $c['id'] }}" class="btn btn-danger">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                        <a id="edit_{{ $c['id'] }}" onclick="contactEdit({{ $c['id'] }});" class="btn btn-primary">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>

                @else
                    <p style="text-align: center">Контакты отсутствуют</p>
                @endif
            </div>
            <div class="tab-pane" id="tasks">
                <input type="hidden" id="pid" value="{{ $project->id }}">

                <a onclick="sort('all');" class="btn btn-success">Все</a>
                <a onclick="sort('main');" class="btn btn-success">Основные работы</a>
                <a onclick="sort('additional');" class="btn btn-success">Дополнительные работы</a>

                <div class="pull-right">
                    <a onclick="addAdditionalTask();" class="btn btn-primary">Добавить дополнительную задачу</a>
                    <a href="/admin/project/close/{{ $project->id }}"  class="btn btn-primary">Закрыть проект</a>
                </div>
                <div id="formAdditionalTask" style="margin-top: 20px; display: none;">
                    <div class="box">
                        <form action="/admin/projects/add-additional-task" method="post" id="formAdditional">
                            {{ csrf_field() }}
                            <input type="hidden" name="pid" value="{{ $project->id }}">
                            <input type="hidden" name="user_id" value="{{ $project->user_id }}">
                            <input type="hidden" name="observer_id" value="{{ $observer_id }}">
                            <table class="table">
                                <tr>
                                    <td>
                                        <div class="form-group" id="NameTask">
                                            <label class="control-label" for="inputTask">Наименование задачи</label>
                                            <input type="text" class="form-control" name="name_task" id="inputTask" required>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group" id="NameTask">
                                            <label class="control-label" for="selectModule">Модуль</label>
                                            <select name="selectModule" id="selectModule" class="form-control">
                                                @foreach($all_modules as $am)
                                                <option value="{{ $am['id'] }}">{{ $am['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group" id="TaskCont">
                                            <label class="control-label" for="inputTaskCont">Количество контактов</label>
                                            <input type="text" class="form-control" name="cont" id="inputTaskCont" required>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group" id="NameTask">
                                            <label class="control-label" for="selectGroupTask">Группа задач</label>
                                            <select name="selectGroupTask" id="selectGroupTask" class="form-control">
                                                @foreach($all_group_task as $gt)
                                                <option value="{{ $gt->id }}">{{ $gt->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <div class="form-group">
                                            <input type="submit" value="Сохранить" class="btn btn-primary">
                                            <a onclick="addAdditionalTask();" class="btn btn-default">Отмена</a>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                </div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Наименование задачи</th>
                                <th>Модуль</th>
                                <th>Группа работ</th>
                                <th>Дополнительная работа</th>
                                <th>Выполнено</th>
                                <th>Действия</th>
                            </tr>
                        </thead>
                        <tbody id="tableTasks">
                            @if ($project_task)
                                @foreach($project_task as $pt)
                                    <tr>
                                        <td style="vertical-align: middle; display: none;" id="pt_id">{{ $pt['id'] }}</td>
                                        <td style="vertical-align: middle;">{{ $pt['name'] }}</td>
                                        <td style="vertical-align: middle;">{{ $pt['m_name'] }}</td>
                                        <td style="vertical-align: middle;">{{ $pt['g_task'] }}</td>
                                        @if($pt['additional'])
                                            <td style="vertical-align: middle;">Да</td>
                                        @else
                                            <td style="vertical-align: middle;"></td>
                                        @endif
                                        @if($pt['status'] == 1)
                                            <td style="vertical-align: middle;"><i class="fa fa-square-o"></i></td>
                                        @endif
                                        @if($pt['status'] == 2)
                                            <td style="vertical-align: middle;"><i class="fa fa-check-square-o"></i></td>
                                        @endif
                                        @if($pt['status'] == 3)
                                            <td style="vertical-align: middle;"><i class="fa fa-square"></i></td>
                                        @endif
                                        @if(!$pt['callbackstatus_ref_id'])
                                            @if ($pt['next_contact'])
                                                <td style="vertical-align: middle;">
                                                    <a href="/admin/project-task/edit/{{ $pt['id'] }}" class="btn btn-primary"><i class="fa fa-search"></i></a>
                                                </td>
                                            @else
                                                @if ($pt['type'] == 'task')
                                                <td style="vertical-align: middle;">
                                                    <a onclick="showCalendarTask({{ $loop->index+1 }}, {{ $pt['id'] }});" class="btn btn-success"><i class="fa fa-calendar-plus-o"></i></a>
                                                </td>
                                                @elseif ($pt['type'] == 'print_form')
                                                <td style="vertical-align: middle;">
                                                    <a onclick="showCalendarPrintForm({{ $loop->index+1 }}, {{ $pt['id'] }});" class="btn btn-success"><i class="fa fa-calendar-plus-o"></i></a>
                                                </td>
                                                @elseif ($pt['type'] == 'screen_form')
                                                <td style="vertical-align: middle;">
                                                    <a onclick="showCalendarScreenForm({{ $loop->index+1 }}, {{ $pt['id'] }});" class="btn btn-success"><i class="fa fa-calendar-plus-o"></i></a>
                                                </td>
                                                @endif
                                            @endif
                                        @else
                                            <td style="vertical-align: middle;">
                                                <a href="/admin/project-task/edit/{{ $pt['id'] }}" class="btn btn-primary"><i class="fa fa-search"></i></a>
                                            </td>
                                        @endif
                                    </tr>
                                    <tr style="display: none; background-color: #fff;" id="tr_{{ $loop->index+1 }}">
                                        <td colspan="6">
                                            <div class="box-body">
                                                <form action="/admin/calendar/add-project-task" method="post" id="add_project_task">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="tid" value="{{ $pt['id'] }}">
                                                    <input type="hidden" name="type" value="{{ $pt['type'] }}">
                                                    <input type="hidden" name="pid" value="{{ $project->id }}">
                                                    <div style="padding-bottom: 20px;">
                                                        <a id="date1" onclick="setDateCont('date1', {{ $loop->index+1 }});" data-content="{{ $tomorrow_val }}" class="btn btn-primary">Завтра</a>
                                                        <a id="date2" onclick="setDateCont('date2', {{ $loop->index+1 }});" data-content="{{ $plus_one_day_val }}" class="btn btn-primary">Послезавтра</a>
                                                        <a id="date3" onclick="setDateCont('date3', {{ $loop->index+1 }});" data-content="{{ $plus_two_day_val }}" class="btn btn-primary">{{ $plus_two_day }}</a>
                                                        <a id="date4" onclick="setDateCont('date4', {{ $loop->index+1 }});" data-content="{{ $plus_three_day_val }}" class="btn btn-primary">{{ $plus_three_day }}</a>
                                                    </div>
                                                    <div style="padding-bottom: 20px;">
                                                        <table class="table">
                                                            <tr>
                                                                <td style="vertical-align: middle;" width="25%">
                                                                    Внедренец
                                                                </td>
                                                                <td style="vertical-align: middle;" width="25%">
                                                                    <select name="user" id="user_{{ $loop->index+1 }}" class="form-control">
                                                                        @foreach($observer as $o)
                                                                            @if($o['default'] == 1)
                                                                                <option value="{{ $o['id'] }}" selected>{{ $o['first_name'] }} {{ $o['last_name'] }}</option>
                                                                            @else
                                                                                <option value="{{ $o['id'] }}">{{ $o['first_name'] }} {{ $o['last_name'] }}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </td>
                                                                <td rowspan="3" width="50%">
                                                                    <div id="am-calendar-{{ $loop->index+1 }}"></div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="vertical-align: middle;">Дата</td>
                                                                <td style="vertical-align: middle;"><input type="date" class="form-control" value="{{ $cur_date_val }}" id="date_cont_{{ $loop->index+1 }}" name="date_cont" required></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="vertical-align: middle;">Время</td>
                                                                <td style="vertical-align: middle;"><input type="time" class="form-control" id="time_cont_{{ $loop->index+1 }}" name="time_cont" required></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="vertical-align: middle;">
                                                                    <!--<a onclick="saveToCalendar({{ $pt['id'] }}, {{ $loop->index+1 }});" class="btn btn-success">Запланировать</a>-->
                                                                    <input type="submit" class="btn btn-success" value="Запланировать">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr style="display: none; background-color: #fff;" id="tr_print_form_{{ $loop->index+1 }}">
                                        <td colspan="6">
                                            <div class="box-body">
                                                <form action="/admin/calendar/add-project-task" method="post" id="add_print_form_task" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="tid" value="{{ $pt['id'] }}">
                                                    <input type="hidden" name="type" value="{{ $pt['type'] }}">
                                                    <input type="hidden" name="pid" value="{{ $project->id }}">
                                                    <div class="form-group">
                                                        <label for="printFormName_{{ $loop->index+1 }}">Название</label>
                                                        <input type="text" name="formName" id="printFormName_{{ $loop->index+1 }}" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="file" name="img[]" multiple="true">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="editor1">Коментарий</label><br>
                                                        <div id="editor1" name="editor1"></div>
                                                    </div>

                                                    <input type="submit" class="btn btn-success" value="Запланировать">
                                                </form>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr style="display: none; background-color: #fff;" id="tr_screen_form_{{ $loop->index+1 }}">
                                        <td colspan="6">
                                            <div class="box-body">
                                                <form action="/admin/calendar/add-project-task" method="post" id="add_screen_form_task" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="tid" value="{{ $pt['id'] }}">
                                                    <input type="hidden" name="type" value="{{ $pt['type'] }}">
                                                    <input type="hidden" name="pid" value="{{ $project->id }}">
                                                    <div class="form-group">
                                                        <label for="screenFormName_{{ $loop->index+1 }}">Название</label>
                                                        <input type="text" name="formName" id="screenFormName_{{ $loop->index+1 }}" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="file" name="img2[]" multiple="true">
                                                    </div>
                                                    <div class="wall_form">
                                                        <label for="editor2">Коментарий</label><br>
                                                        <div id="editor2" name="editor2"></div>
                                                    </div>

                                                    <input type="submit" class="btn btn-success" value="Запланировать">
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @else

                            @endif

                            @if($project_form == '' && $project_task == '')
                                <tr>
                                    <td colspan="6" style="text-align: center; font-style: italic;">Задачи не найдены</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>

                <h4>ИТОГО контактов/форм по группам</h4>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover" id="normTable">
                        <thead>
                        <tr>
                            <th></th>
                            <th>ФАКТ</th>
                            <th>По договору</th>
                            <th>Осталось</th>
                            <th>Запас</th>
                            <th>Осталось с учетом запаса</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Контакты</td>
                                <td id="cont_count_fact">{{ $count_fact }}</td>
                                <td id="cont_count_norm">{{ $count_norm }}</td>
                                <td id="cont_remained">{{ $count_norm-$remained }}</td>
                                <td id="cont_count_number">{{ $count_number }}</td>
                                <td id="cont_stock">{{ $count_number-$remained }}</td>
                            </tr>
                            <tr>
                                <td>Печатные формы/отчеты</td>
                                <td id="print_count_fact"></td>
                                <td id="print_count_norm">{{ $count_print_norm }}</td>
                                <td id="print_remained"></td>
                                <td id="print_count_number">{{ $count_print_number }}</td>
                                <td id="print_stock"></td>
                            </tr>
                            <tr>
                                <td>Экранные формы</td>
                                <td id="screen_count_fact"></td>
                                <td id="screen_count_norm">{{ $count_screen_norm }}</td>
                                <td id="screen_remained"></td>
                                <td id="screen_count_number">{{ $count_screen_number }}</td>
                                <td id="screen_stock"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane" id="card">
                    <div class="box-body table-responsive no-padding">
                        <table>
                            @if($contacts[0] != '')
                                @foreach($contacts as $c)
                                    @if($c['director'])
                                    <tr>
                                        <td style="padding-right: 10px;"><span class="bold">{{ $c['post'] }}: </span></td>
                                        <td>{{ $c['last_name'] }} {{ $c['first_name'] }} {{ $c['patronymic'] }} Тел. {{ $c['phone'] }}</td>
                                    </tr>
                                    @endif
                                    @if($c['main'])
                                    <tr>
                                        <td style="padding-right: 10px;"><span class="bold">Ответственный за внедрение: </span></td>
                                        <td>{{ $c['last_name'] }} {{ $c['first_name'] }} {{ $c['patronymic'] }} Тел. {{ $c['phone'] }}</td>
                                    </tr>
                                    @endif
                                @endforeach
                            @endif
                            @if(count($modules) > 0)
                            <tr>
                                <td colspan="2"><span class="bold">Купленные модули</span></td>
                            </tr>
                            @foreach($modules as $m)
                                <tr>
                                    <td>{{ $m['name'] }}</td>
                                    <td>{{ $m['num'] }}</td>
                                </tr>
                            @endforeach
                            @endif

                        </table>
                        <table>
                            <tr>
                                <td style="padding-right: 10px;"><span class="bold">Дата начала внедрения</span></td>
                                <td style="padding-right: 20px;">{{ $project->start_date }}</td>
                                <td style="padding-right: 10px;"><span class="bold">Запланированная дата окончания</span></td>
                                <td>{{ $project->finish_date }}</td>
                            </tr>
                            @if($question)
                                @foreach($question as $q)
                                    @if($q['question'] != null)
                                    <tr>
                                        <td><span class="bold">{{ $q['question'] }}</span></td>
                                        <td>{{ $q['answer'] }}</td>
                                    </tr>
                                    @endif
                                @endforeach
                            @endif
                        </table>
                    </div>

            </div>
            <div class="tab-pane" id="documents">
                <table class="table">
                    <tr>
                        @foreach($doc_types as $dt)
                            <td>{{ $dt->name }}</td>
                        @endforeach
                    </tr>
                </table>
                <table class="table" style="width: 48%; float: left;">
                    @foreach($doc_input as $di)
                        <tr>
                            <td>{{ $di['name'] }}</td>
                        </tr>
                    @endforeach
                </table>
                <table class="table" style="width: 52%;">
                    @foreach($doc_output as $do)
                        <tr>
                            <td>{{ $do['name'] }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="tab-pane" id="forms">
                <table class="table">
                    <thead>
                    <th>Наименование</th>
                    <th>Текущий статус</th>
                    <th>Дата статуса</th>
                    <th>ТИП</th>
                    <th>История изменений</th>
                    </thead>
                    <tbody>
                    @if ($forms != '')
                        @foreach ($forms as $f)
                            <tr>
                                <td>{{ $f['name'] }}</td>
                                <td>{{ $f['status'] }}</td>
                                <td>{{ $f['date_status'] }}</td>
                                @if($f['type'] == 'print_form')
                                    <td>Печатная форма</td>
                                @else
                                    <td>Экранная форма</td>
                                @endif
                                <td>
                                    <a style="cursor: pointer" onclick="showDetail({{ $f['id'] }});">Смотреть</a>
                                </td>
                            </tr>
                            <tr id="detail_{{ $f['id'] }}" style="display: none;">
                                <td colspan="5">
                                    <table class="table" id="history_{{ $f['id'] }}">
                                        <thead>
                                        <tr>
                                            <td><b>Дата</b></td>
                                            <td><b>Сотрудник</b></td>
                                            <td><b>Статус</b></td>
                                            <td><b>Коментарий</b></td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5" style="text-align: center"><i>Формы отсутствуют</i></td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <!-- /.tab-pane -->
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
    </div>
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">
        var url = document.location.toString();
        if (url.match('#')) {
            $('.nav-tabs li').removeClass('active');
            $('.nav-tabs li a').attr("aria-expanded","false");
            $('.nav-tabs a[href="#'+url.split('#')[1]+'"]').parent('li').addClass('active');
            $('.nav-tabs a[href="#'+url.split('#')[1]+'"]').attr("aria-expanded","true");
            $('.tab-pane').removeClass('active');
            $('#'+url.split('#')[1]).addClass('active');
            scroll(0,0);
        }

        $('.nav-tabs li a').on('shown.bs.tab', function (e) {
            window.location.hash = e.target.hash;
            scroll(0,0);
        });

        function showDetail(id) {
            $('#detail_'+id).slideToggle('fast', function () {
                $("#history_"+id+" > tbody").html('');
                context = '';
                $.ajax({
                    url: '/admin/print-form/get-form-hisstory',
                    data: {'id': id},
                    success: function (resp) {
                        console.log(resp);
                        if (resp) {
                            for(i=0;i<resp.length;i++) {
                                context += '<tr>' +
                                    '<td>'+resp[i].date+'</td>' +
                                    '<td>'+resp[i].user.last_name+' '+resp[i].user.first_name+'</td>' +
                                    '<td>'+resp[i].status+'</td>' +
                                    '<td>'+resp[i].comment.text+'</td>' +
                                    '</tr>';
                            }
                        } else {
                            context = '<tr>' +
                                '<td colspan="4" style="text-align: center"><i>История отсутствует</i></td>' +
                                '</tr>';
                        }

                        $("#history_"+id+" > tbody").html(context);
                    }
                });
            });
        }

        $(document).ready(function(){
            $("#formAdditional").validate({
                messages: {
                    name_task: {
                        required: "Это поле обязательно для заполнения"
                    },
                    cont: {
                        required: "Это поле обязательно для заполнения"
                    }
                }
            });
            $("#add_project_task").validate({
                messages: {
                    time_cont: {
                        required: "Это поле обязательно для заполнения"
                    },
                    date_cont: {
                        required: "Это поле обязательно для заполнения"
                    }
                }
            });
            $("#click_mes_form").click(function(){
                $("#popup_message_form").slideToggle("slow");
                $(this).toggleClass("active");
                $('#add').css('display', 'none');
                $('#addComment').css('display', 'block');
                $('#addComment > a').css('display', 'block');
                return false;
            });

            $("#click_contact_add").click(function(){
                content = "";
                $.ajax({
                    url: '/admin/posts/get-all-post',
                    success: function (resp) {
                        console.log(resp);
                        for (i=0;i<resp.length;i++) {
                            content += '<option id="'+resp[i].id+'">'+resp[i].name+'</option>';
                        }
                        $("#inputPost").html(content);
                    }
                });
                $("#popup_contact_form").slideToggle("slow");
                $(this).toggleClass("active");
                $('#addCont').css('display', 'none');
                $('#form_button > input').css('display', 'block');
                $('#addContact').css('display', 'block');
                $('#form_button > a').css('display', 'block');
                return false;
            });

            $('#addCommentButton').click(function () {
                comment = CKEDITOR.instances.editor.getData();
                p_id = $('#project_id').val();
                if (comment) {
                    $.ajax({
                        'url': '/admin/comments/add',
                        'data': {'text': comment, 'project_id': p_id},
                        success: function (resp) {
                            location.reload();
                        }
                    });
                } else {
                    $("#editor-error").css('display', 'block');
                }
            });

            $("#addContact").validate({
                messages: {
                    contact_name: {
                        required: "Это поле обязательно для заполнения"
                    },
                    contact_surname: {
                        required: "Это поле обязательно для заполнения"
                    },
                    contact_patronymic: {
                        required: "Это поле обязательно для заполнения"
                    },
                    contact_phone: {
                        required: "Это поле обязательно для заполнения"
                    },
                    contact_email: {
                        required: "Это поле обязательно для заполнения"
                    }
                }
            });

            CKEDITOR.replace('editor');
            CKEDITOR.replace('editor1');
            CKEDITOR.replace('editor2');
        });

        function editComment(id, comment) {
            $('#comment_'+id).html('<textarea id="textComment" cols="172" rows="10" style="resize: vertical;">'+comment+'</textarea>');
            $('#textComment').setFocus();
        }

        function closeContactForm() {
            $("#popup_contact_form").slideToggle("slow");
            $(this).toggleClass("active");
            $('#addCont').css('display', 'block');
            $('#addContact').css('display', 'none');
            $('#addContact > a').css('display', 'none');
            return false;
        }

        function contactEdit(id) {
            surname = $("#contactSurname").text();
            name = $("#contactName").text();
            patronymic = $("#contactPatronymic").text();
            post = $("#contactPost").text();
            phone = $("#contactPhone").text();
            email = $("#contactEmail").text();

            $("#contactSurname").closest('td').html('<input type="text" class="form-control" id="inputContSurname" value="'+surname+'">');
            $("#contactName").closest('td').html('<input type="text" class="form-control" id="inputContName" value="'+name+'">');
            $("#contactPatronymic").closest('td').html('<input type="text" class="form-control" id="inputContPatronymic" value="'+patronymic+'">');
            $("#contactPhone").closest('td').html('<input type="text" class="form-control" id="inputContPhone" value="'+phone+'">');
            $("#contactEmail").closest('td').html('<input type="email" class="form-control" id="inputContEmail" value="'+email+'">');
            $.ajax({
                url: '/admin/posts/get-all-post',
                success: function (resp) {
                    content = '';
                    content = '<select class="form-control" id="inputContPost">';
                    for (i=0;i<resp.length;i++) {
                        content += '<option value="'+resp[i]['id']+'">'+resp[i]['name']+'</option>';
                    }
                    content += '</select>';
                    $("#contactPost").closest('td').html(content);
                }
            });
            $("a#edit_"+id).attr('onclick', 'saveContact('+id+')');
            $("a#edit_"+id).html('<i class="fa fa-check"></i>');
        }

        function saveContact(id) {
            surname = $("#inputContSurname").val();
            name = $("#inputContName").val();
            patronymic = $("#inputContPatronymic").val();
            post = $("#inputContPost option:selected").val();
            post_name = $("#inputContPost option:selected").html();
            phone = $("#inputContPhone").val();
            email = $("#inputContEmail").val();

            $.ajax({
                url: '/admin/contact/save',
                data: {'id': id, 'surname': surname, 'name': name, 'patronymic': patronymic, 'post': post, 'phone': phone, 'email': email},
                success: function (resp) {
                    if (resp.success == 'Ok') {
                        $("#contactOk").css('display', 'block');
                        $("#inputContSurname").closest('td').html('<span id="contactSurname">'+surname+'</span>');
                        $("#inputContName").closest('td').html('<span id="contactName">'+name+'</span>');
                        $("#inputContPatronymic").closest('td').html('<span id="contactPatronymic">'+patronymic+'</span>');
                        $("#inputContPhone").closest('td').html('<span id="contactPhone">'+phone+'</span>');
                        $("#inputContPost").closest('td').html('<span id="contactPost">'+post_name+'</span>');
                        $("#inputContEmail").closest('td').html('<span id="contactEmail">'+email+'</span>');
                    } else {
                        $("#contactError").css('display', 'block');
                    }
                }
            });
        }

        function sort(status) {
            pid = $("#project_id").val();
            pt_id = $("#pt_id").val();

            $.ajax({
                url: '/admin/projects/sort',
                data: {'status': status, 'pid': pid},
                success: function (resp) {
                    console.log(resp);
                    if (resp != '') {
                        $('#tableTasks > tr').remove();
                        for (i=0; i<resp.p_task.length; i++) {
                            content = '<tr>';
                            content += '<td style="vertical-align: middle;">'+resp.p_task[i].name+'</td>';

                            content += '<td style="vertical-align: middle;">'+resp.p_task[i].m_name+'</td>';
                            content += '<td style="vertical-align: middle;">'+resp.p_task[i].g_task+'</td>';
                            if (resp.p_task[i].additional == 1) {
                                content += '<td style="vertical-align: middle;">Да</td>';
                            } else {
                                content += '<td style="vertical-align: middle;"></td>';
                            }
                            if (resp.p_task[i].status == 0) {
                                content += '<td style="vertical-align: middle;"></td>';
                            }
                            if (resp.p_task[i].status == 1) {
                                content += '<td style="vertical-align: middle;"><i class="fa fa-square-o"></i></td>';
                            }
                            if (resp.p_task[i].status == 2) {
                                content += '<td style="vertical-align: middle;"><i class="fa fa-check-square-o"></i></td>';
                            }
                            if (resp.p_task[i].status == 3) {
                                content += '<td style="vertical-align: middle;"><i class="fa fa-square"></i></td>';
                            }
                            if (resp.p_task[i].print_form == 0) {
                                if (resp.p_task[i].next_contact) {
                                    content += '<td style="vertical-align: middle;">\n' +
                                        '<a href="/admin/project-task/edit/' + resp.p_task[i].id + '" class="btn btn-primary"><i class="fa fa-search"></i></a>\n' +
                                        '</td>';
                                } else {
                                    if (resp.p_task[i].type == 'task') {
                                        content += '<td style="vertical-align: middle;"><a onclick="showCalendarTask(' + (i + 1) + ',' + resp.p_task[i].id + ');" class="btn btn-success"><i class="fa fa-calendar-plus-o"></i></a></td>';
                                        content += '</tr>';
                                        content += '<tr style="display: none; background-color: #fff;" id="tr_' + (i + 1) + '">' +
                                            '<td colspan="6">' +
                                            '<div class="box-body">' +
                                            '<form action="/admin/calendar/add-project-task" method="post" id="add_project_task">' +
                                            '{{ csrf_field() }}' +
                                            '<input type="hidden" name="tid" value="'+resp.p_task[i].id+'">' +
                                            '<input type="hidden" name="type" value="'+resp.p_task[i].type+'">' +
                                            '<input type="hidden" name="pid" value="'+resp.p_task[i].p_id+'">' +
                                            '<div style="padding-bottom: 20px;">' +
                                            '<a id="date1" onclick="setDateCont(\'date1\', ' + (i + 1) + ');" data-content="{{ $tomorrow_val }}" class="btn btn-primary">Завтра</a>' +
                                            '<a id="date2" onclick="setDateCont(\'date2\', ' + (i + 1) + ');" data-content="{{ $plus_one_day_val }}" class="btn btn-primary">Послезавтра</a>' +
                                            '<a id="date3" onclick="setDateCont(\'date3\', ' + (i + 1) + ');" data-content="{{ $plus_two_day_val }}" class="btn btn-primary">{{ $plus_two_day }}</a>' +
                                            '<a id="date4" onclick="setDateCont(\'date4\', ' + (i + 1) + ');" data-content="{{ $plus_three_day_val }}" class="btn btn-primary">{{ $plus_three_day }}</a>' +
                                            '</div>' +
                                            '<div style="padding-bottom: 20px;">' +
                                            '<table class="table">' +
                                            '<tr>' +
                                            '<td style="vertical-align: middle;" width="25%">' +
                                            'Внедренец ' +
                                            '</td>' +
                                            '<td style="vertical-align: middle;" width="25%">' +
                                            '<select name="user" id="user_' + (i + 1) + '" class="form-control">' +
                                            '@foreach($observer as $o)' +
                                            '@if($o['default'] == 1)' +
                                            '<option value="{{ $o['id'] }}" selected>{{ $o['first_name'] }} {{ $o['last_name'] }}</option>' +
                                            '@else' +
                                            '<option value="{{ $o['id'] }}">{{ $o['first_name'] }} {{ $o['last_name'] }}</option>' +
                                            '@endif' +
                                            '@endforeach' +
                                            '</select>' +
                                            '</td>' +
                                            '<td rowspan="3" width="50%">' +
                                            '<div id="am-calendar-' + (i + 1) + '"></div>' +
                                            '</td>' +
                                            '</tr>' +
                                            '<tr>' +
                                            '<td style="vertical-align: middle;">Дата</td>' +
                                            '<td style="vertical-align: middle;"><input type="date" class="form-control" value="{{ $cur_date_val }}" id="date_cont_' + (i + 1) + '"  name="date_cont" required></td>' +
                                            '<td></td>' +
                                            '</tr>' +
                                            '<tr>' +
                                            '<td style="vertical-align: middle;">Время</td>' +
                                            '<td style="vertical-align: middle;"><input type="time" class="form-control" id="time_cont_' + (i + 1) + '" name="time_cont" required></td>' +
                                            '<td></td>' +
                                            '</tr>' +
                                            '<tr>' +
                                            '<td style="vertical-align: middle;"><input type="submit" class="btn btn-success" value="Запланировать"></td>' +
                                            '</tr>' +
                                            '</table>' +
                                            '</div>' +
                                            '</div>' +
                                            '</td>' +
                                            '</tr>';
                                    }
                                    if (resp.p_task[i].type == 'print_form') {
                                        content += '<td style="vertical-align: middle;"><a onclick="showCalendarPrintForm(' + (i + 1) + ',' + resp.p_task[i].id + ');" class="btn btn-success"><i class="fa fa-calendar-plus-o"></i></a></td>';
                                        content += '</tr>';
                                        content += '<tr style="display: none; background-color: #fff;" id="tr_print_form_' + (i + 1) + '">' +
                                            '<td colspan="6">' +
                                            '<div class="box-body">' +
                                            '<form action="/admin/calendar/add-project-task" method="post" id="add_project_task">' +
                                            '{{ csrf_field() }}' +
                                            '<input type="hidden" name="tid" value="'+resp.p_task[i].id+'">' +
                                            '<input type="hidden" name="type" value="'+resp.p_task[i].type+'">' +
                                            '<input type="hidden" name="pid" value="'+resp.p_task[i].p_id+'">' +
                                            '<div class="form-group">' +
                                            '<label for="formName">Название</label>' +
                                            '<input type="text" id="formName" name="formName" class="form-control">' +
                                            '</div>' +
                                            '<div class="form-group">' +
                                            '<label for="editor2">Коментарий</label>' +
                                            '<div id="editor2"></div>' +
                                            '</div>' +
                                            '<input type="submit" class="btn btn-success" value="Запланировать">' +
                                            '</div>' +
                                            '</div>' +
                                            '</td>' +
                                            '</tr>';
                                    }
                                    if (resp.p_task[i].type == 'screen_form') {
                                        content += '<td style="vertical-align: middle;"><a onclick="showCalendarScreenForm(' + (i + 1) + ',' + resp.p_task[i].id + ');" class="btn btn-success"><i class="fa fa-calendar-plus-o"></i></a></td>';
                                        content += '</tr>';
                                        content += '<tr style="display: none; background-color: #fff;" id="tr_screen_form_' + (i + 1) + '">' +
                                            '<td colspan="6">' +
                                            '<div class="box-body">' +
                                            '<form action="/admin/calendar/add-project-task" method="post" id="add_project_task">' +
                                            '{{ csrf_field() }}' +
                                            '<input type="hidden" name="tid" value="'+resp.p_task[i].id+'">' +
                                            '<input type="hidden" name="type" value="'+resp.p_task[i].type+'">' +
                                            '<input type="hidden" name="pid" value="'+resp.p_task[i].p_id+'">' +
                                            '<div class="form-group">' +
                                            '<label for="formName">Название</label>' +
                                            '<input type="text" id="formName" name="formName" class="form-control">' +
                                            '</div>' +
                                            '<div class="form-group">' +
                                            '<label for="editor3">Коментарий</label>' +
                                            '<div id="editor3"></div>' +
                                            '</div>' +
                                            '<input type="submit" class="btn btn-success" value="Запланировать">' +
                                            '</div>' +
                                            '</div>' +
                                            '</td>' +
                                            '</tr>';
                                    }
                                }
                            } else {
                                content += '<td style="vertical-align: middle;">\n' +
                                    '<a href="/admin/print-form/edit/'+resp.p_task[i].id+'" class="btn btn-primary"><i class="fa fa-search"></i></a>\n' +
                                    '</td>';
                            }
                            $('#tableTasks').append(content);
                            $('#cont_count_number ').html(resp.count_number);
                            $('#cont_count_fact ').html(0);
                            $('#cont_count_norm ').html(resp.count_norm);
                            $('#cont_remained ').html(0);
                            $('#cont_stock ').html(0);

                            $('#print_count_number ').html(resp.count_print_number);
                            $('#print_count_fact ').html(0);
                            $('#print_count_norm ').html(resp.count_print_norm);
                            $('#print_remained ').html(0);
                            $('#print_stock ').html(0);

                            $('#screen_count_number ').html(resp.count_screen_number);
                            $('#screen_count_fact ').html(0);
                            $('#screen_count_norm ').html(resp.count_screen_norm);
                            $('#screen_remained ').html(0);
                            $('#screen_stock ').html(0);
                        }
                    } else {
                        content = '<tr>' +
                            '<td colspan="6" style="text-align: center;">Задачи не найдены</td>' +
                            '</tr>';

                        $('#tableTasks').html(content);
                    }
                }
            });
        }

        function addAdditionalTask() {
            $('#inputTask').val('');
            $('#inputTaskCont').val('');
            $("#formAdditionalTask").slideToggle("fast");
        }

        $('#formAdditional').submit(function () {
            task = $('#inputTask').val();
            task_cont = $('#inputTaskCont').val();
            module = $('#selectModule option:selected').val();
            group_task = $('#selectGroupTask option:selected').val();
            pid = $("#project_id").val();

            return true;
        });

        function difDate(date) {
            Data = new Date();
            Year = Data.getFullYear();
            Month = Data.getMonth()+1;
            Day = Data.getDate();

            curDate = Year+'-'+Month+'-'+Day;
            if (curDate <= date) {
                return true;
            } else {
                return false;
            }
        }

        function showCalendarTask(i) {
            $("#tr_"+i).slideToggle("fast", function () {
                date = $("#date_cont_"+i).val();
                tasks = getObserverCalendar(i);
                user = $("#user_"+i+" option:selected").html();
                tasks = getObserverCalendar(i);

                if (tasks[0] != '') {
                    date = $("#date_cont_"+i).val();
                    console.log(tasks[0]['id']);
                    user = $("#user_"+i+" option:selected").html();
                    content = '<div class="am-calendar-header">' +
                        '<div id="date" style="float: left; width: 50%;">'+date+'</div>' +
                        '<div id="user">'+user+'</div>' +
                        '<div class="clearfix"></div>';
                    content += '<div><table>';
                    for (k=0; k<tasks.length; k++) {
                        content += '<tr>';
                        content += '<td style="padding-right: 10px;">'+tasks[k]['time']+'</td>';
                        content += '<td>'+tasks[k]['task']+'</td>';
                        content += '</tr>';
                    }
                    content += '</table></div>';
                } else {
                    content = '<div class="am-calendar-header">' +
                        '<div id="date" style="float: left; width: 50%;">'+date+'</div>' +
                        '<div id="user">'+user+'</div>' +
                        '<div class="clearfix"></div>';
                    content += '<div><table>';
                    content += '<tr><td colspan="2">Задачи отсутствуют</td></tr>';
                    content += '</table></div>';
                }


                $("#am-calendar-"+i).html(content);

                $("#date_cont_"+i).change(function () {
                    date = $(this).val();
                    if (difDate(date)) {
                        tasks = getObserverCalendar(i);

                        if (tasks[0] != '') {
                            date = $("#date_cont_"+i).val();
                            console.log(tasks[0]['id']);
                            user = $("#user_"+i+" option:selected").html();
                            content = '<div class="am-calendar-header">' +
                                '<div id="date" style="float: left; width: 50%;">'+date+'</div>' +
                                '<div id="user">'+user+'</div>' +
                                '<div class="clearfix"></div>';
                            content += '<div><table>';
                            for (k=0; k<tasks.length; k++) {
                                content += '<tr>';
                                content += '<td style="padding-right: 10px;">'+tasks[k]['time']+'</td>';
                                content += '<td>'+tasks[k]['task']+'</td>';
                                content += '</tr>';
                            }
                            content += '</table></div>';
                        } else {
                            content = '<div class="am-calendar-header">' +
                                '<div id="date" style="float: left; width: 50%;">'+date+'</div>' +
                                '<div id="user">'+user+'</div>' +
                                '<div class="clearfix"></div>';
                            content += '<div><table>';
                            content += '<tr><td colspan="2">Задачи отсутствуют</td></tr>';
                            content += '</table></div>';
                        }


                        $("#am-calendar-"+i).html(content);
                    } else {
                        content = '<div class="am-calendar-header">' +
                            '<div id="date" style="float: left; width: 50%;"></div>' +
                            '<div id="user">'+user+'</div>' +
                            '<div class="clearfix"></div>';
                        content += '<div><table>';
                        content += '<tr><td colspan="2">Ошибка! Выбранная дата меньше текущей</td></tr>';
                        content += '</table></div>';

                        $("#am-calendar-"+i).html(content);
                    };
                });

                $("#date_cont_"+i).change(function () {
                    date = $(this).val();
                    if (difDate(date)) {
                        tasks = getObserverCalendar(i);

                        if (tasks[0] != '') {
                            date = $("#date_cont_"+i).val();
                            console.log(tasks[0]['id']);
                            user = $("#user_"+i+" option:selected").html();
                            content = '<div class="am-calendar-header">' +
                                '<div id="date" style="float: left; width: 50%;">'+date+'</div>' +
                                '<div id="user">'+user+'</div>' +
                                '<div class="clearfix"></div>';
                            content += '<div><table>';
                            for (k=0; k<tasks.length; k++) {
                                content += '<tr>';
                                content += '<td style="padding-right: 10px;">'+tasks[k]['time']+'</td>';
                                content += '<td>'+tasks[k]['task']+'</td>';
                                content += '</tr>';
                            }
                            content += '</table></div>';
                        } else {
                            content = '<div class="am-calendar-header">' +
                                '<div id="date" style="float: left; width: 50%;">'+date+'</div>' +
                                '<div id="user">'+user+'</div>' +
                                '<div class="clearfix"></div>';
                            content += '<div><table>';
                            content += '<tr><td colspan="2">Задачи отсутствуют</td></tr>';
                            content += '</table></div>';
                        }


                        $("#am-calendar-"+i).html(content);
                    } else {
                        content = '<div class="am-calendar-header">' +
                            '<div id="date" style="float: left; width: 50%;"></div>' +
                            '<div id="user">'+user+'</div>' +
                            '<div class="clearfix"></div>';
                        content += '<div><table>';
                        content += '<tr><td colspan="2">Ошибка! Выбранная дата меньше текущей</td></tr>';
                        content += '</table></div>';

                        $("#am-calendar-"+i).html(content);
                    };
                });

                $("#user_"+i).change(function () {
                    tasks = getObserverCalendar(i);

                    if (tasks[0] != '') {
                        date = $("#date_cont_"+i).val();
                        console.log(tasks[0]['id']);
                        user = $("#user_"+i+" option:selected").html();
                        content = '<div class="am-calendar-header">' +
                            '<div id="date" style="float: left; width: 50%;">'+date+'</div>' +
                            '<div id="user">'+user+'</div>' +
                            '<div class="clearfix"></div>';
                        content += '<div><table>';
                        for (k=0; k<tasks.length; k++) {
                            content += '<tr>';
                            content += '<td style="padding-right: 10px;">'+tasks[k]['time']+'</td>';
                            content += '<td>'+tasks[k]['task']+'</td>';
                            content += '</tr>';
                        }
                        content += '</table></div>';
                    } else {
                        content = '<div class="am-calendar-header">' +
                            '<div id="date" style="float: left; width: 50%;">'+date+'</div>' +
                            '<div id="user">'+user+'</div>' +
                            '<div class="clearfix"></div>';
                        content += '<div><table>';
                        content += '<tr><td colspan="2">Задачи отсутствуют</td></tr>';
                        content += '</table></div>';
                    }


                    $("#am-calendar-"+i).html(content);
                });
            });

        }

        function showCalendarPrintForm(i) {
            $("#tr_print_form_"+i).slideToggle("fast");
        }

        function showCalendarScreenForm(i) {
            $("#tr_screen_form_"+i).slideToggle("fast");
        }

        function getObserverCalendar(i) {
            user = $("#user_"+i+" option:selected").html();
            user_id = $("#user_"+i+" option:selected").val();
            date = $("#date_cont_"+i).val();
            result = '';

            $.ajax({
                url: '/admin/tasks/get-taks-am-calendar',
                async:false,
                data: {'date': date, 'user_id': user_id, 'user': user},
                success: function (resp) {
                    result = resp;
                }
            });

            return result;
        }

        function setDateCont(id, i) {
            date = $('#'+id).attr('data-content');
            $('#date_cont_'+i).val(date);
            tasks = getObserverCalendar(i);

            if (tasks[0] != '') {
                date = $("#date_cont_"+i).val();
                console.log(tasks[0]['id']);
                user = $("#user_"+i+" option:selected").html();
                content = '<div class="am-calendar-header">' +
                    '<div id="date" style="float: left; width: 50%;">'+date+'</div>' +
                    '<div id="user">'+user+'</div>' +
                    '<div class="clearfix"></div>';
                content += '<div><table>';
                for (k=0; k<tasks.length; k++) {
                    content += '<tr>';
                    content += '<td style="padding-right: 10px;">'+tasks[k]['time']+'</td>';
                    content += '<td>'+tasks[k]['task']+'</td>';
                    content += '</tr>';
                }
                content += '</table></div>';
            } else {
                content = '<div class="am-calendar-header">' +
                    '<div id="date" style="float: left; width: 50%;">'+date+'</div>' +
                    '<div id="user">'+user+'</div>' +
                    '<div class="clearfix"></div>';
                content += '<div><table>';
                content += '<tr><td colspan="2">Задачи отсутствуют</td></tr>';
                content += '</table></div>';
            }


            $("#am-calendar-"+i).html(content);
        }

        function setTimeCont(id) {
            time = $('#time_cont').val();
            date = $('#date_cont').val();
            D = new Date(date+' '+time);
            new_time = $('#'+id).attr('data-content');
            val = D.toLocaleTimeString(D.setMinutes(D.getMinutes()+Number(new_time)));
            $("#next_time").val(val);
        }

        function saveToCalendar(tid, i) {
            date = $('#date_cont_'+i).val();
            time = $('#time_cont_'+i).val();
            user_id = $("#user_"+i+" option:selected").val();
            pid = $("#pid").val();

            $.ajax({
                url: '/admin/calendar/add-project-task',
                data: {'tid': tid, 'date': date, 'time': time, 'user_id': user_id, 'pid': pid},
                success: function (resp) {
                    if (resp.status == 'Ok') {
                        location.reload();
                    } else {
                        alert(resp.error);
                    }
                }
            });
        }
    </script>
@endpush