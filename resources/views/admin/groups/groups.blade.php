@extends('layouts.cabinet')

@section('title')
    Группы
@endsection

@section('content')
    @if (count($groups) > 0)
        @if(session('perm')['group.create'])
            <div style="padding-bottom: 20px;">
                <a href="/admin/groups/add" class="btn btn-primary">Добавить</a>
            </div>
        @endif
        @if(\Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
                {!! \Session::get('success') !!}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                {!! \Session::get('error') !!}
            </div>
        @endif
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Список групп</h3>

                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tbody>
                    <tr>
                        <th>№</th>
                        <th>Название</th>
                        <th>Псевдоним</th>
                        <th colspan="2">Действия</th>
                    </tr>
                    @foreach($groups as $g)
                        <tr>
                            <td style="vertical-align: middle;">{{ $loop->index+1 }}</td>
                            <td>{{ $g->name }}</td>
                            <td>{{ $g->slug }}</td>
                            @if(session('perm')['group.update'])
                                <td style="vertical-align: middle;">
                                    <a href="groups/edit/{{ $g->id }}" class="btn btn-primary">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                </td>
                            @else
                                <td style="vertical-align: middle;">
                                    <a href="javascript: void(0)" class="btn btn-primary" disabled="true">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                </td>
                            @endif
                            @if(session('perm')['group.delete'])
                                <td style="vertical-align: middle;">
                                    <a href="groups/delete/{{ $g->id }}" class="btn btn-danger">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            @else
                                <td style="vertical-align: middle;">
                                    <a href="javascript: void(0)" class="btn btn-danger" disabled="true">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    @else
        456
    @endif
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection