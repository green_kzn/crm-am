@extends('layouts.cabinet')

@section('title')
    Задачи для отдела разработки форм
@endsection

@section('content')
    @if(session('perm')['print_form.create'])
        <div style="padding-bottom: 20px;">
            <a href="/admin/print-form/add" class="btn btn-primary">Добавить</a>
        </div>
    @endif
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#raw" data-toggle="tab" aria-expanded="true">Необработанные</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="raw">
                <div class="box" style="overflow: hidden;">
                    <table class="table table-hover">
                        <thead>
                        <th style="vertical-align: middle;">Заказчик</th>
                        <th style="vertical-align: middle;">Проект</th>
                        <th style="vertical-align: middle;">Наименование работы</th>
                        <th style="vertical-align: middle;">Внедренец</th>
                        <th style="vertical-align: middle;" colspan="1">Действия</th>
                        </thead>
                        <tbody>
                        @if ($form_raw[0] != null)
                            @foreach($form_raw as $r)
                                <tr>
                                    <td>{{ $r['client'] }}</td>
                                    <td>{{ $r['project'] }}</td>
                                    <td>{{ $r['name'] }}</td>
                                    <td>{{ $r['observer'] }}</td>
                                    <td style="vertical-align: middle;">
                                        <a href="/admin/print-form/raw/{{ $r['id'] }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5" style="text-align: center; font-style: italic;">Ни чего не найдено</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.tab-pane -->
            </div>
        </div>
        <!-- /.tab-content -->
    </div>
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection

@push('scripts')

@endpush