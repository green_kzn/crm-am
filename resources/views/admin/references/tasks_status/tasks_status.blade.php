@extends('layouts.cabinet')

@section('title')
    Справочник "Статусы задач"
@endsection

@section('content')
    @if(session('perm')['status_task_ref.create'])
    <div style="padding-bottom: 20px;">
        <a href="/admin/tasks-status/add" class="btn btn-primary">Добавить</a>
    </div>
    @endif
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="col-md-6">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Список статусов</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tbody>
                    <tr>
                        <th width="5%">№</th>
                        <th>Название</th>
                        <th colspan="3" width="20%">Действия</th>
                    </tr>
                    @if(count($tstatus) > 0)
                        @foreach($tstatus as $ts)
                            <tr>
                                <td style="vertical-align: middle;">{{ $loop->index+1 }}</td>
                                <td style="vertical-align: middle;">{{ $ts->name }}</td>
                                @if(session('perm')['status_task_ref.update'])
                                    <td style="vertical-align: middle;">
                                        <a href="/admin/tasks-status/edit/{{ $ts->id }}" class="btn btn-primary">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </td>
                                @else
                                    <td style="vertical-align: middle;">
                                        <a href="javascript: void(0)" class="btn btn-primary" disabled="true">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </td>
                                @endif
                                @if(session('perm')['status_task_ref.delete'])
                                    <td style="vertical-align: middle;">
                                        <a href="/admin/tasks-status/del/{{ $ts->id }}" class="btn btn-danger">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                @else
                                    <td style="vertical-align: middle;">
                                        <a href="javascript: void(0)" class="btn btn-danger" disabled="true">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3" style="text-align: center;">Список статусов пуст</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">

    </script>
@endpush