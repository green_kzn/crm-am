@extends('layouts.cabinet')

@section('title')
    Справочник "Чек - лист": Редактирование вопроса
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="col-md-12">
        <div class="box" style="padding: 10px;">
            <form action="/admin/check-list/edit/{{ $question->id }}" method="post" id="editQuestion">
                {{ csrf_field() }}
                <div class="form-group" id="NameTask">
                    <label class="control-label" for="inputNameModule">Вопрос</label>
                    <textarea id="question" name="question" required>{{ $question->question }}</textarea>
                </div>

                <div class="form-group">
                    <input type="submit" value="Сохранить" class="btn btn-success">
                    <a href="/admin/check-list" class="btn btn-default">Отмена</a>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">
        CKEDITOR.replace('question');
        $(document).ready(function() {
            $("#addQuestion").validate({
                messages: {
                    question: {
                        required: "Это поле обязательно для заполнения"
                    }
                }
            });
        });
    </script>
@endpush