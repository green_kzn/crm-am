@extends('layouts.cabinet')

@section('title')
    Справочник "Клиенты": Добавление нового клиента
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="col-md-12">
        <div class="box" style="padding: 10px;">
            <form class="form-horizontal" id="formNewClient" action="/admin/clients/add" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="contacts" id="contacts">
                <input type="hidden" name="filials" id="filials">
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputNameClient" class="col-sm-2 control-label">Наименование клиента</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="name_client" id="inputNameClient">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputNameClient" class="col-sm-2 control-label">Город</label>
                        <div class="col-sm-6">
                            <select name="client_city" id="" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                @foreach($citys as $c)
                                    <option value="{{ $c->id }}">{{ $c->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <div class="col-sm-4">
                                <a onclick="showFilial();" class="btn btn-primary">Филиалы</a>
                            </div>
                        </div>
                    </div>
                    <div class="box" id="filial" style="display: none;">
                        <div class="box-header">
                            <h3 class="box-title">Филиалы</h3>
                        </div>
                        <div class="box-body">
                            <form action="" id="form-filial">
                                <table class="table" id="tableContact">
                                    <thead>
                                    <tr>
                                        <th>Наименование</th>
                                        <th>Город</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr id="new_filial"></tr>
                                    <tr>
                                        <td width="40%">
                                            <input type="text" class="form-control" id="filial_name" name="filial_name">
                                            <label for="filial_name" id="filial_name_error" style="color: #ff340d; display: none;"></label>
                                        </td>
                                        <td width="40%">
                                            <select name="filial_city" id="filial_city" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                @foreach($citys as $c)
                                                    <option value="{{ $c->id }}">{{ $c->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td width="40%">
                                            <a onclick="addFilial();" style="cursor: pointer;" class="btn btn-success">
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Контакты</h3>
                        </div>
                        <table class="table" id="tableContact">
                            <thead>
                            <tr>
                                <th>Фамилия</th>
                                <th>Имя*</th>
                                <th>Отчество</th>
                                <th>Должность</th>
                                <th>Телефон*</th>
                                <th>Email</th>
                                <th colspan="2">Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr id="contact_row"></tr>
                            <tr id="inputRow">
                                <td><input name="contact_surname" type="text" class="form-control contact_surname" ></td>
                                <td><input name="contact_name" type="text" class="form-control contact_name" ></td>
                                <td><input name="contact_patronymic" type="text" class="form-control contact_patronymic" ></td>
                                <td>
                                    <select class="form-control contact_post" name="contact_post">
                                        @foreach($posts as $p)
                                            <option id="{{ $p->id }}" value="{{ $p->name }}">{{ $p->name }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td><input name="contact_phone" type="text" class="form-control contact_phone" placeholder="7(999) 999-9999"></td>
                                <td><input name="contact_email" type="email" class="form-control contact_email" placeholder="email@site.ru" ></td>
                                <td></td>
                                <td>
                                    <a onclick="addContact();" class="btn btn-success">
                                        <i class="fa fa-check"></i>
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Сохранить" class="btn btn-primary">
                        <a href="/admin/clients" class="btn btn-default">Отмена</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">
        $(function(){
            //2. Получить элемент, к которому необходимо добавить маску
            $("input[name~='contact_phone']").inputmask("7(999) 999-9999");
        });
        $(document).ready(function () {
            filial = {};
            filials_ar = [];

            /*$("#formNewClient").validate({
                messages: {
                    name_client: {
                        required: "Это поле обязательно для заполнения"
                    },
                    contact_surname: {
                        required: "Это поле обязательно для заполнения"
                    },
                    contact_name: {
                        required: "Это поле обязательно для заполнения"
                    },
                    contact_patronymic: {
                        required: "Это поле обязательно для заполнения"
                    },
                    contact_phone: {
                        required: "Это поле обязательно для заполнения"
                    },
                    contact_email: {
                        required: "Это поле обязательно для заполнения"
                    }
                }
            });*/

            $('#formNewClient').submit(function () {
                var clients = [];

                num_clients = $('.num_row').length;

                for (i=1; i<=num_clients; i++) {
                    clients.push({
                        'name': $('span[name~="contacts_name_'+i+'"]').html(),
                        'surname': $('span[name~="contacts_surname_'+i+'"]').html(),
                        'patronymic': $('span[name~="contacts_patronymic_'+i+'"]').html(),
                        'post': $('span[name~="contacts_post_'+i+'"]').html(),
                        'phone': $('span[name~="contacts_phone_'+i+'"]').html(),
                        'email': $('span[name~="contacts_email_'+i+'"]').html(),
                    });
                }

                $('#contacts').val(JSON.stringify(clients));
                $('#filials').val(JSON.stringify(filials_ar));

                /*if (clients.length == 0) {
                    alert('Запоните контакты');
                    return false;
                } else {
                    return true;
                }*/
                return true;
            });
        });
        
        function showFilial() {
            $("#filial").slideToggle('slow');
        }

        Array.prototype.remove = function(value) {
            var idx = this.indexOf(value);
            if (idx != -1) {
                // Второй параметр - число элементов, которые необходимо удалить
                return this.splice(idx, 1);
            }
            return false;
        };
        
        function addFilial() {
            filial_name = $("#filial_name").val();
            filial_city_id = $("#filial_city option:selected").val();
            filial_city_name = $("#filial_city option:selected").html();
            i = $(".rowFilial").length;

            $("#filial_name_error").css('display', 'none');

            if (filial_name == '') {
                $("#filial_name_error").html('Это поле обязательно для заполнения');
                $("#filial_name_error").css('display', 'block');
                exit();
            }

            if (i == 0) {
                id = 0;
            } else {
                id = $('tr:eq('+i+')').attr('id');
            }

            filial = {
                'id': Number(id)+1,
                'name': filial_name,
                'city_id': filial_city_id
            };
            filials_ar.push(filial);

            content = '<tr class="rowFilial" id="'+(Number(id)+1)+'">' +
                '<td >'+filial_name+'</td>' +
                '<td>'+filial_city_name+'</td>' +
                '<td><a onclick="delFilial(this, '+(Number(id)+1)+');" style="cursor: pointer;" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>' +
                '</tr>';
            $("#new_filial").before(content);
            $("#filial_name").val('');
        }

        function delFilial(el, id) {
            $(el).parent('td').parent('tr').remove();
            for(i=0;i<filials_ar.length;i++) {
                if (filials_ar[i].id == id) {
                    filials_ar.remove(filials_ar[i]);
                }
            }
        }

        function addContact() {
            num_edit = $('.fa-check').length;

            if (num_edit == 1) {
                alert('Имеются не сохраненные данные. Сохраните сначала данные');
                exit();
            } else {
                surname = $('#inputRow > td > .contact_surname').val();
                name = $('#inputRow > td > .contact_name').val();
                patronymic = $('#inputRow > td > .contact_patronymic').val();
                post = $('#inputRow > td > .contact_post option:selected').val();
                phone = $('#inputRow > td > .contact_phone').val();
                email = $('#inputRow > td > .contact_email').val();
                var patt3 = new RegExp("/^(\\s*)?(\\+)?([- _():=+]?\\d[- _():=+]?){10,14}(\\s*)?$/");

                i = $('.num_row').length;

                if (!name) {
                    AMValidate($('.contact_name'));
                }

                if (!phone) {
                    AMValidate($('.contact_phone'));
                }

                if (name && phone) {
                    $('#contact_row').before('<tr class="num_row" id="rowContact_'+(i+1)+'">' +
                        '<td name="td_surname_'+(i+1)+'"><span name="contacts_surname_'+(i+1)+'">'+surname+'</span></td>' +
                        '<td name="td_name_'+(i+1)+'"><span name="contacts_name_'+(i+1)+'">'+name+'</td>' +
                        '<td name="td_patronymic_'+(i+1)+'"><span name="contacts_patronymic_'+(i+1)+'">'+patronymic+'</span></td>' +
                        '<td name="td_post_'+(i+1)+'"><span name="contacts_post_'+(i+1)+'">'+post+'</td></span>' +
                        '<td name="td_phone_'+(i+1)+'"><span name="contacts_phone_'+(i+1)+'">'+phone+'</td></span>' +
                        '<td name="td_email_'+(i+1)+'"><span name="contacts_email_'+(i+1)+'">'+email+'</td></span>' +
                        '<td><a onclick="delContact('+(i+1)+');" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>' +
                        '<td name="td_check_'+(i+1)+'"><a onclick="editContact('+(i+1)+');"class="btn btn-success"><i class="fa fa-pencil"></i></a></td>' +
                        '</tr>');
                }
            }

            $('#inputRow > td > .contact_surname').val('');
            $('#inputRow > td > .contact_name').val('');
            $('#inputRow > td > .contact_patronymic').val('');
            $('#inputRow > td > .contact_phone').val('');
            $('#inputRow > td > .contact_email').val('');
        }

        function delContact(i) {
            $('#rowContact_'+i).remove();
        }

        function editContact(i) {
            num_edit = $('.fa-check').length;

            if (num_edit == 1) {
                alert('Имеются не сохраненные данные. Сохраните сначала данные');
                exit();
            } else {
                name = $('#rowContact_' + i + ' > td > span[name~="contacts_name_' + i + '"]').html();
                surname = $('#rowContact_' + i + ' > td > span[name~="contacts_surname_' + i + '"]').html();
                patronymic = $('#rowContact_' + i + ' > td > span[name~="contacts_patronymic_' + i + '"]').html();
                post = $('#rowContact_' + i + ' > td > span[name~="contacts_post_' + i + '"]').html();
                phone = $('#rowContact_' + i + ' > td > span[name~="contacts_phone_' + i + '"]').html();
                email = $('#rowContact_' + i + ' > td > span[name~="contacts_email_' + i + '"]').html();

                $('#rowContact_' + i + ' > td[name~="td_surname_' + i + '"]').html('<input value="' + surname + '" name="contact_surname" type="text" class="form-control contact_surname">');
                $('#rowContact_' + i + ' > td[name~="td_name_' + i + '"]').html('<input value="' + name + '" name="contact_name" required type="text" class="form-control contact_name">');
                $('#rowContact_' + i + ' > td[name~="td_patronymic_' + i + '"]').html('<input value="' + patronymic + '" name="contact_patronymic" type="text" class="form-control contact_patronymic">');
                $('#rowContact_' + i + ' > td[name~="td_post_' + i + '"]').html('<select class="form-control contact_post" name="contact_post">' +
                    '@foreach($posts as $p)' +
                    '<option id="{{ $p->id }}" value="{{ $p->name }}">{{ $p->name }}</option>' +
                    '@endforeach' +
                    '</select>');
                $('#rowContact_' + i + ' > td[name~="td_phone_' + i + '"]').html('<input value="' + phone + '" name="contact_phone" required type="text" class="form-control contact_phone">');
                $('#rowContact_' + i + ' > td[name~="td_email_' + i + '"]').html('<input value="' + email + '" name="contact_email" type="text" class="form-control contact_email">');
                $('#rowContact_' + i + ' > td[name~="td_check_' + i + '"]').html('<a onclick="saveContact('+i+');" class="btn btn-success"><i class="fa fa-check"></i></a>');
            }
        }

        function saveContact(i) {
            name = $('#rowContact_' + i + ' > td[name~="td_name_'+i+'"] > input').val();
            surname = $('#rowContact_' + i + ' > td[name~="td_surname_'+i+'"] > input').val();
            patronymic = $('#rowContact_' + i + ' > td[name~="td_patronymic_'+i+'"] > input').val();
            post = $('#rowContact_' + i + ' > td[name~="td_post_'+i+'"] > select option:selected').val();
            phone = $('#rowContact_' + i + ' > td[name~="td_phone_'+i+'"] > input').val();
            email = $('#rowContact_' + i + ' > td[name~="td_email_'+i+'"] > input').val();

            $('#rowContact_' + i + ' > td[name~="td_surname_' + i + '"]').html('<span name="contacts_surname_'+i+'">'+surname+'</span>');
            $('#rowContact_' + i + ' > td[name~="td_name_' + i + '"]').html('<span name="contacts_name_'+i+'">'+name+'</span>');
            $('#rowContact_' + i + ' > td[name~="td_patronymic_' + i + '"]').html('<span name="contacts_patronymic_'+i+'">'+patronymic+'</span>');
            $('#rowContact_' + i + ' > td[name~="td_post_' + i + '"]').html('<span name="contacts_post_'+i+'">'+post+'</span>');
            $('#rowContact_' + i + ' > td[name~="td_phone_' + i + '"]').html('<span name="contacts_phone_'+i+'">'+phone+'</span>');
            $('#rowContact_' + i + ' > td[name~="td_email_' + i + '"]').html('<span name="contacts_email_'+i+'">'+email+'</span>');
            $('#rowContact_' + i + ' > td[name~="td_check_' + i + '"]').html('<a onclick="editContact('+i+');"class="btn btn-success"><i class="fa fa-pencil"></i></a>');
        }
    </script>
@endpush