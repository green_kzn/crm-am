@extends('layouts.cabinet')

@section('title')
    Справочник "Документы"
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="col-md-6">
        @if(session('perm')['documents_ref.create'])
            <div style="padding-bottom: 20px;">
                <a href="/admin/documents/add" class="btn btn-primary">Добавить</a>
            </div>
        @endif
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#input" data-toggle="tab">Входящие</a></li>
                <li><a href="#output" data-toggle="tab">Исходящие</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="input">
                    <div class="box-body table-responsive no-padding">
                        <table class="table" id="sort">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Наименование</th>
                                <th>Действие</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($documents_input != '')
                                @foreach($documents_input as $d)
                                    <tr>
                                        <td>{{ $loop->index+1 }}</td>
                                        <td>{{ $d['name'] }}</td>
                                        <td>
                                            @if(session('perm')['documents_ref.update'])
                                                <a href="/admin/documents/edit/{{ $d['id'] }}" class="btn btn-primary">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            @else
                                                <a href="javascript: void(0)" class="btn btn-primary" disabled>
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            @endif
                                            @if(session('perm')['documents_ref.delete'])
                                                <a href="/admin/documents/del/{{ $d['id'] }}" class="btn btn-danger">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            @else
                                                <a href="javascript: void(0)" class="btn btn-danger" disabled>
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="3">Документы не найдены</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.tab-pane -->

                <div class="tab-pane" id="output">
                    <div class="box-body table-responsive no-padding">
                        <table class="table" id="sort">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Наименование</th>
                                <th>Действие</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($documents_output != '')
                                @foreach($documents_output as $d2)
                                    <tr>
                                        <td>{{ $loop->index+1 }}</td>
                                        <td>{{ $d2['name'] }}</td>
                                        <td>
                                            @if(session('perm')['documents_ref.update'])
                                                <a href="/admin/documents/edit/{{ $d2['id'] }}" class="btn btn-primary">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            @else
                                                <a href="javascript: void(0)" class="btn btn-primary" disabled>
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            @endif
                                            @if(session('perm')['documents_ref.delete'])
                                                <a href="/admin/documents/del/{{ $d2['id'] }}" class="btn btn-danger">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            @else
                                                <a href="javascript: void(0)" class="btn btn-danger" disabled>
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="3">Документы не найдены</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
    </div>
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection

@push('scripts')
<script type="text/javascript">

</script>
@endpush