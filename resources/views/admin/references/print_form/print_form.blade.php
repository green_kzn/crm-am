@extends('layouts.cabinet')

@section('title')
    Справочник "Категории форм"
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="col-md-12">
        @if(session('perm')['module_ref.create'])
            <div style="padding-bottom: 20px;">
                <a href="/admin/printform-category/add" class="btn btn-primary">Добавить</a>
            </div>
        @endif
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Список категорий</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tbody>
                    <tr>
                        <th width="5%">№</th>
                        <th>Название</th>
                        <th>Норма</th>
                        <th colspan="2" width="10%">Действия</th>
                    </tr>
                    @if(count($category) > 0)
                        @foreach($category as $c)
                            <tr>
                                <td style="vertical-align: middle;">{{ $loop->index+1 }}</td>
                                <td style="vertical-align: middle;">{{ $c->category }}</td>
                                <td style="vertical-align: middle;">{{ $c->norm }}</td>
                                @if(session('perm')['module_ref.update'])
                                    <td style="vertical-align: middle;">
                                        <a href="/admin/printform-category/edit/{{ $c->id }}" class="btn btn-primary">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </td>
                                @else
                                    <td style="vertical-align: middle;">
                                        <a href="javascript: void(0)" class="btn btn-primary" disabled="true">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </td>
                                @endif
                                @if(session('perm')['module_ref.delete'])
                                    <td style="vertical-align: middle;">
                                        <a href="/admin/printform-category/del/{{ $c->id }}" class="btn btn-danger">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                @else
                                    <td style="vertical-align: middle;">
                                        <a href="javascript: void(0)" class="btn btn-danger" disabled="true">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4" style="text-align: center;">Список категорий пуст</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">

    </script>
@endpush