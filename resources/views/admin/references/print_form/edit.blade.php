@extends('layouts.cabinet')

@section('title')
    Справочник "Категории форм": Редактирование категории
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="box" style="padding: 10px;">
        <form action="/admin/printform-category/edit/{{ $category->id }}" method="post" id="edit_category">
            {{ csrf_field() }}

            <div class="form-group">
                <label class="control-label" for="inputNameCategory">Категория</label>
                <input type="text" class="form-control" name="name_category" id="inputNameCategory" style="width: 300px;" value="{{ $category->category }}" required>
            </div>

            <div class="form-group">
                <label class="control-label" for="inputNormCategory">Норма</label>
                <input type="text" class="form-control" name="norm_category" id="inputNormCategory" style="width: 300px;" value="{{ $category->norm }}" required>
            </div>

            <div class="form-group">
                <input type="submit" value="Сохранить" class="btn btn-success">
                <a href="/admin/printform-category" class="btn btn-default">Отмена</a>
            </div>
        </form>
    </div>
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#edit_category").validate({
                messages: {
                    name_category: {
                        required: "Это поле обязательно для заполнения"
                    },
                    norm_category: {
                        required: "Это поле обязательно для заполнения"
                    }
                }
            });
        });
    </script>
@endpush