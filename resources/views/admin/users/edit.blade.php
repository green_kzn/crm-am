@extends('layouts.cabinet')

@section('title')
    Редактирование пользователя "{{ $u->last_name }} {{ $u->first_name }}"
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            <form action="/admin/user/update/{{ $u->id }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group" id="NameGroup">
                    <label class="control-label" for="inputName" style="display: none;"></label>
                    <input type="text" class="form-control" name="inputName" placeholder="Имя" value="{{ $u->first_name }}">
                </div>
                <div class="form-group" id="LastNameGroup">
                    <label class="control-label" for="inputLastName" style="display: none;"></label>
                    <input type="text" class="form-control" name="inputLastName" placeholder="Фамилия" value="{{ $u->last_name }}">
                </div>
                <div class="form-group">
                    <img id="blah" src="{{ asset('public/avatars/'.$u->foto) }}" alt="your image" />
                </div>
                <div class="form-group">
                    <input type="file" name="img" accept="image/*" enctype="multipart/form-data">
                </div>
                <div class="form-group" id="EmailGroup">
                    <label class="control-label" for="inputEmail" style="display: none;"></label>
                    <input type="text" class="form-control" name="inputEmail" placeholder="Email" value="{{ $u->email }}">
                </div>
                <div class="form-group" id="PasswordGroup">
                    <label class="control-label" for="inputPassword" style="display: none;"></label>
                    <input type="password" class="form-control" name="inputPassword" placeholder="Пароль" value="{{ $u->password }}">
                </div>
                <div class="form-group">
                    <label>Группа</label>
                    <select class="form-control" name="user_role" id="role">
                        @foreach($roles as $r)
                            @if($r->id == $u_role->id)
                                <option value="{{ $r->id }}" selected>{{ $r->name }}</option>
                            @else
                                <option value="{{ $r->id }}">{{ $r->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <input type="submit" value="Сохранить" class="btn btn-success">
                    <a href="/admin/users" class="btn btn-default">Отмена</a>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection