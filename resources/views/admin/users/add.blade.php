@extends('layouts.cabinet')

@section('title')
    Добавление пользователя
@endsection

@section('content')
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif

    <div class="box" style="padding: 10px;">
        <div class="row">
            <div class="col-md-3">
                <form action="/admin/user/add" method="post" enctype="multipart/form-data" id="userAdd">
                    {{ csrf_field() }}
                    <div class="form-group" id="NameGroup">
                        <label class="control-label" for="inputName">Имя</label>
                        <input type="text" class="form-control" name="user_name" id="inputName" required>
                    </div>
                    <div class="form-group" id="LastNameGroup">
                        <label class="control-label" for="inputLastName">Фамилия</label>
                        <input type="text" class="form-control" name="user_surname" id="inputLastName" required>
                    </div>
                    <div class="form-group">
                        <label for="inputPicture">Фото</label><br>

                    </div>
                    <div class="form-group">
                        <input type="file" name="img" accept="image/*" enctype="multipart/form-data">
                    </div>
                    <div class="form-group" id="EmailGroup">
                        <label class="control-label" for="inputEmail">Email</label>
                        <input type="text" class="form-control" name="user_email" id="inputEmail" required>
                    </div>
                    <div class="form-group" id="PasswordGroup">
                        <label class="control-label" for="inputPassword">Пароль</label>
                        <input type="password" class="form-control" name="user_pass" id="inputPassword" required>
                    </div>
                    <div class="form-group">
                        <label>Группа</label>
                        <select class="form-control" name="user_role" id="role">
                            @foreach($roles as $r)
                                <option value="{{ $r->id }}">{{ $r->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <input type="submit" value="Сохранить" class="btn btn-success">
                        <a href="/admin/users" class="btn btn-default">Отмена</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#userAdd").validate({
                messages: {
                    user_name: {
                        required: 'Это поле обязательно для заполнения'
                    },
                    user_surname: {
                        required: 'Это поле обязательно для заполнения'
                    },
                    img: {
                        required: 'Это поле обязательно для заполнения'
                    },
                    user_email: {
                        required: 'Это поле обязательно для заполнения'
                    },
                    user_pass: {
                        required: 'Это поле обязательно для заполнения'
                    }
                }
            });
        });
    </script>
@endpush
