@extends('layouts.cabinet')

@section('title')
    Пользователи
@endsection

@section('content')
    @if(count($users) > 0)
        @if(session('perm')['user.create'])
            <div style="padding-bottom: 20px;">
                <a href="/admin/users/add" class="btn btn-primary">Добавить</a>
            </div>
        @endif
        @if(\Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
                {!! \Session::get('success') !!}
            </div>
        @endif
        @if(\Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                {!! \Session::get('error') !!}
            </div>
        @endif
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Список пользователей</h3>

                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tbody><tr>
                        <th>№</th>
                        <th>Фото</th>
                        <th>Имя</th>
                        <th>Фамилия</th>
                        <th>Email</th>
                        <th>Группа</th>
                        <th>Статус</th>
                        <th>Активация</th>
                        <th colspan="3">Действия</th>
                    </tr>
                    @foreach($users as $u)
                        <tr>
                            <td style="vertical-align: middle;">{{ $loop->index+1 }}</td>
                            @if($u->foto)
                                <td style="vertical-align: middle;">
                                    <img src="{{ asset('public/avatars/'.$u->foto) }}" class="img-circle" alt="User Image" style="width: 100%;max-width: 60px;height: auto;">
                                </td>
                            @else
                                <td style="vertical-align: middle;">Нет</td>
                            @endif
                            <td style="vertical-align: middle;">{{ $u->first_name }}</td>
                            <td style="vertical-align: middle;">{{ $u->last_name }}</td>
                            <td style="vertical-align: middle;">{{ $u->email }}</td>
                            <td style="vertical-align: middle;">{{ $u->role }}</td>
                            @if($u->status)
                                <td style="vertical-align: middle;">Активен</td>
                            @else
                                <td style="vertical-align: middle;">Заблокирован</td>
                            @endif
                            @if($u->active)
                                <td style="vertical-align: middle;">Учетная запись активирована</td>
                            @else
                                <td style="vertical-align: middle;">Учетная запись не активирована</td>
                            @endif
                            <td style="vertical-align: middle;">
                                @if($u->status)
                                    <button onclick="switchStatus({{ $u->status }},{{ $u->id }})" type="button" class="btn btn-default">
                                        <i class="fa fa-ban"></i>
                                    </button>
                                @else
                                    <button onclick="switchStatus({{ $u->status }},{{ $u->id }})" type="button" class="btn btn-default">
                                        <i class="fa fa-check"></i>
                                    </button>
                                @endif
                            </td>
                            @if(session('perm')['user.update'])
                                <td style="vertical-align: middle;">
                                    <a href="users/edit/{{ $u->id }}" class="btn btn-primary">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                </td>
                            @else
                                <td style="vertical-align: middle;">
                                    <a href="javascript: void(0)" class="btn btn-primary" disabled="true">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                </td>
                            @endif
                            @if(session('perm')['user.delete'])
                                <td style="vertical-align: middle;">
                                    <a href="users/delete/{{ $u->id }}" class="btn btn-danger">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            @else
                                <td style="vertical-align: middle;">
                                    <a href="javascript: void(0)" class="btn btn-danger" disabled="true">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody></table>
            </div>
            <!-- /.box-body -->
        </div>
    @else
        @if($user_perm['user_create'])
            <div style="padding-bottom: 20px;">
                <a href="/admin/events/add" class="btn btn-primary">Добавить</a>
            </div>
        @endif
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Список пользователей</h3>

                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tbody><tr>
                        <th>№</th>
                        <th>Фото</th>
                        <th>Имя</th>
                        <th>Фамилия</th>
                        <th>Email</th>
                        <th>Группа</th>
                        <th>Статус</th>
                        <th>Активация</th>
                        <th>Действия</th>
                    </tr>
                    <tr>
                        <td colspan="8" style="text-align: center;">Пользователи отсутствуют</td>
                    </tr>
                    </tbody></table>
            </div>
            <!-- /.box-body -->
        </div>
    @endif
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection

<script>
    function switchStatus(status, u_id) {
        $.ajax({
            url: "/admin/users/switch-status",
            data: {'id': u_id, 'status': status},
            method: "GET",
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (resp) {
                location.reload();
            }
        });
    }
</script>