@extends('layouts.cabinet')

@section('title')
    Обратная связь: Редактирование вопроса
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="box" style="padding: 10px;">
        <form action="/admin/callback/edit-work-questions/{{ $question->id }}" method="post" id="addWorkQuestion">
            {{ csrf_field() }}
            <div class="form-group" id="NameTask">
                <label class="control-label" for="inputQuestion">Вопрос</label>
                <input type="text" class="form-control" name="question" id="inputQuestion" value="{{ $question->question }}">
            </div>

            <div class="form-group">
                <input type="submit" value="Сохранить" class="btn btn-success">
                <a href="/admin/callback" class="btn btn-default">Отмена</a>
            </div>
        </form>
    </div>
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">

    </script>
@endpush