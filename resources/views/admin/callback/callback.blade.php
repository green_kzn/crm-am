@extends('layouts.cabinet')

@section('title')
    Обратная связь
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs" style="float: left; width: 20%; height: 100%;">
            <li class="active" style="width: 200px; background-color: white;"><a href="#work" data-toggle="tab">Вопросы в процессе внедрения</a></li>
            <li style="width: 200px; background-color: white;"><a href="#close" data-toggle="tab">Вопросы по закрытию проекта</a></li>
            <li style="width: 200px; background-color: white;"><a href="#params" data-toggle="tab">Параметры</a></li>
        </ul>
        <div class="tab-content" style="float: left; width: 80%;">
            <div class="tab-pane active" id="work" style="min-height: 600px;">
                <div class="box box-solid">
                    @if(session('perm')['callback_settings.create'])
                        <div style="padding-bottom: 20px;">
                            <a href="/admin/callback/add-work-questions" class="btn btn-primary">Добавить</a>
                        </div>
                    @endif
                    <table class="table">
                        <thead>
                        <tr>
                            <th style="width: 10%;">№</th>
                            <th>Вопрос</th>
                            <th colspan="2" style="width: 10%;">Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($work_q[0] != '')
                            @foreach($work_q as $wq)
                                <tr>
                                    <td>{{ $loop->index+1 }}</td>
                                    <td>{{ $wq['question'] }}</td>
                                    <td style="vertical-align: middle;">
                                        <a href="/admin/callback/edit-work-questions/{{ $wq['id'] }}" class="btn btn-primary">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <a href="/admin/callback/del-work-questions/{{ $wq['id'] }}" class="btn btn-danger">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="2" style="text-align: center"><i>Вопросы отсутствуют</i></td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="tab-pane" id="close" style="min-height: 600px;">
                <div class="box box-solid">
                    @if(session('perm')['callback_settings.create'])
                        <div style="padding-bottom: 20px;">
                            <a href="/admin/callback/add-close-questions" class="btn btn-primary">Добавить</a>
                        </div>
                    @endif
                    <table class="table">
                        <thead>
                        <tr>
                            <th style="width: 10%;">№</th>
                            <th>Вопрос</th>
                            <th colspan="2" style="width: 10%;">Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($close_q[0] != '')
                            @foreach($close_q as $cq)
                                <tr>
                                    <td>{{ $loop->index+1 }}</td>
                                    <td>{{ $cq['question'] }}</td>
                                    <td style="vertical-align: middle;">
                                        <a href="/admin/callback/edit-close-questions/{{ $cq['id'] }}" class="btn btn-primary">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <a href="/admin/callback/del-close-questions/{{ $cq['id'] }}" class="btn btn-danger">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="2" style="text-align: center"><i>Вопросы отсутствуют</i></td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="tab-pane" id="params" style="min-height: 600px;">
                <div class="box" style="border: none; box-shadow: none;">
                    <!-- form start -->
                    <form class="form-horizontal" action="/admin/callback/save-settings" method="post">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <table class="table table-striped">
                                <tr>
                                    <td>Периодичность сбора отзывов (в днях)</td>
                                    <td><input type="number" class="form-control" name="frequency_feedback" id="inputFrequencyFeedback" style="width: 300px;" value="{{ $frequency_feedback }}"></td>
                                </tr>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer" style="border: none;">
                            <input type="submit" value="Сохранить" class="btn btn-primary">
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </div>
        </div>
        <!-- /.tab-content -->
    </div>
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">

    </script>
@endpush