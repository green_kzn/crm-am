@extends('layouts.cabinet')

@section('title')
    Pusher
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Chats</div>

                <div class="panel-body">
                    <messagesform :messages="messages"></messagesform>
                </div>
                <div class="panel-footer">
                    <chatform v-on:sentmessage="addMessage()" :user="7"></chatform>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection

@push('scripts')
<script>
    import chatform from './components/ChatForm.vue';
    export default {
        methods: {
            addMessage(message) {
                console.log('123');
                /*this.messages.push(message);

                axios.post('/admin/pusher2', message).then(response => {
                    console.log(response.data);
                });*/
            }
        }
    }
</script>
@endpush