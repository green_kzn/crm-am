@extends('layouts.cabinet')

@section('title')
    Почта
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3">
                <a href="/admin/mail/new" class="btn btn-primary btn-block margin-bottom">Написать</a>

                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title"><b>Папки</b></h3>

                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body no-padding">
                        <ul class="nav nav-pills nav-stacked">
                            @foreach($folders as $f)
                                @if($f['active'])
                                    <li class="active mail-folder" style="cursor: pointer;"><a id="{{ $f['slug'] }}" href="/admin/mail/{{ $f['slug'] }}">{{ $f['name'] }}</a></li>
                                @else
                                    <li class="mail-folder" style="cursor: pointer;"><a id="{{ $f['slug'] }}" href="/admin/mail/{{ $f['slug'] }}">{{ $f['name'] }}</a></li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
            <form action="/admin/mail/new" id="newMessage" method="post">
                {{ csrf_field() }}
                <div class="col-md-9">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Новое письмо</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="form-group">
                                <input class="form-control" name="toMessage" placeholder="Кому" required>
                            </div>
                            <div class="form-group">
                                <input class="form-control" name="themeMessage" placeholder="Тема" required>
                            </div>
                            <div class="form-group">
                                <div id="editor" name="editor"></div>
                                <textarea name="message" id="message" style="display: none;"></textarea>
                            </div>
                            <div class="form-group">
                                <div class="btn btn-default btn-file">
                                    <i class="fa fa-paperclip"></i> Вложения
                                    <input type="file" name="attachment">
                                </div>
                                <p class="help-block">Max. 32MB</p>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="pull-right">
                                <input type="submit" class="btn btn-primary">
                            </div>
                            <a href="/admin/mail/inbox" class="btn btn-default">Отмена</a>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                </div>
                <!-- /.col -->
            </form>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor');
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5();
        });

        $(document).ready(function () {
            $("#newMessage").validate({
                rules: {
                    toMessage: {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    toMessage: {
                        required: "Это поле обязательно для заполнения",
                        email: "E-mail указан не верно"
                    },
                    themeMessage: {
                        required: "Это поле обязательно для заполнения"
                    }
                }
            });
            
            $("#newMessage").submit(function () {
                message = CKEDITOR.instances.editor.getData();
                $('#message').html(message);
            });
        });
    </script>
@endpush
