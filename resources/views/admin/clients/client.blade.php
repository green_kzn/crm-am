@extends('layouts.cabinet')

@section('title')
    {{ $client->name }}
@endsection

@section('content')
    <div class="box" style="padding: 10px;">
        <table width="100%" style="padding: 5px;">
            <tr>
                <td width="45%" style="padding: 5px;" colspan="2">
                    <div class="form-group">
                        <label>Название</label>
                        <input type="text" class="form-control" placeholder="{{ $client->name }}" disabled="">
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" placeholder="{{ $client->email }}" disabled="">
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="form-group">
                        <label>Телефон</label>
                        <input type="text" class="form-control" placeholder="{{ $client->phone }}" disabled="">
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="form-group">
                        <a href="/admin_reviews/admin/clients" class="btn btn-default">Назад</a>
                    </div>
                </td>
            </tr>
        </table>
    </div>
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection