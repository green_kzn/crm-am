@extends('layouts.cabinet')

@section('title')
    Клиенты
@endsection

@section('content')
    @if($event_perm['event_view'])
        @if(count($clients) > 0)
            @if(\Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
                    {!! \Session::get('success') !!}
                </div>
            @endif
            @if(\Session::has('error'))
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                    {!! \Session::get('error') !!}
                </div>
            @endif
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Список клиентов</h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" id="table_client_search" name="client_search" class="search form-control pull-right" placeholder="Search">

                            <div id="search_close" class="input-group-btn" style="display: none">
                                <a href="" class="btn btn-default"><i class="fa fa-close"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover" id="clients_table">
                        <tbody><tr>
                            <th>№</th>
                            <th>Клиент</th>
                            <th>Email</th>
                            <th>Телефон</th>
                            <th colspan="2">Действие</th>
                        </tr>
                        @foreach($clients as $c)
                            <tr>
                                <td>{{ $loop->index+1 }}</td>
                                <td>{{ $c->name }}</td>
                                <td>{{ $c->email }}</td>
                                <td>{{ $c->phone }}</td>
                                <td style="vertical-align: middle;">
                                    <a href="client/{{ $c->id }}" class="btn btn-primary">
                                        <i class="fa fa-search"></i>
                                    </a>
                                </td>
                                <td style="vertical-align: middle;">
                                    <a href="client/delete/{{ $c->id }}" class="btn btn-danger">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody></table>
                    <?php echo $clients->render(); ?>
                </div>
                <!-- /.box-body -->
            </div>
        @else
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Список клиентов</h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody><tr>
                            <th>№</th>
                            <th>Клиент</th>
                            <th>Email</th>
                            <th>Телефон</th>
                            <th colspan="2">Действие</th>
                        </tr>
                        <tr>
                            <td colspan="9" style="text-align: center;">Список клиентов пуст</td>
                        </tr>
                        </tbody></table>
                </div>
                <!-- /.box-body -->
            </div>
        @endif
    @else
        <p>Доступ запрещен</p>
    @endif
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection
