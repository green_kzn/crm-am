@extends('layouts.cabinet')

@section('title')
    Клиенты на обзвон
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#contact" data-toggle="tab" aria-expanded="true">Договориться на контакт</a></li>
            <li class=""><a href="#callback" data-toggle="tab" aria-expanded="false">Взять обратную связь</a></li>
            <li class=""><a href="#other" data-toggle="tab" aria-expanded="false">Другая причина</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="contact">
                <div class="box">
                    <table class="table">
                        <thead>
                        <th>Последний контакт</th>
                        <th>С какими задачами работали</th>
                        <th>Клиент</th>
                        <th>Проект</th>
                        <th>Город</th>
                        <th>Ответственный внедренец</th>
                        <th>Просили перезвонить</th>
                        <th>Действия</th>
                        </thead>
                        <tbody>
                        @if ($tasks != '')
                            @foreach($tasks as $t)
                            <tr>
                                <td style="vertical-align: middle;">{{ $t['prev_contact'] }}</td>
                                <td style="vertical-align: middle;">{{ $t['name'] }}</td>
                                <td style="vertical-align: middle;">{{ $t['client']->name }}</td>
                                <td style="vertical-align: middle;">{{ $t['project']->name }}</td>
                                <td style="vertical-align: middle;">{{ $t['city'] }}</td>
                                <td style="vertical-align: middle;">{{ $t['user']->last_name }} {{ $t['user']->first_name }}</td>
                                <td style="vertical-align: middle;">{{ $t['next_contact'] }}</td>
                                <td style="vertical-align: middle;">
                                    <a href="/admin/calls/edit/{{ $t['id'] }}" class="btn btn-primary">
                                        <i class="fa fa-search"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        @else
                        <tr>
                            <td colspan="8" style="text-align: center; font-style: italic;">Ни чего не найдено</td>
                        </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.tab-pane -->
            </div>
            <div class="tab-pane" id="callback">
                <div class="box">

                </div>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="other">
                <div class="box">

                </div>
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
    </div>
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection

@push('scripts')

@endpush