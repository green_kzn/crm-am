@extends('layouts.cabinet')

@section('title')
    Контакт: {{ $task->title }}
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="col-md-6">
        <input type="hidden" id="tid" value="{{ $task->id }}">
        <div class="box box-primary" style="padding: 10px;">
            <div class="box-header with-border">
                <h3 class="box-title">Общая информация</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body" style="">
                <table>
                    <tr>
                        <td style="padding-right: 10px; font-weight: bold;">Дата/время</td>
                        <td>{{ $task->start_date }}</td>
                    </tr>
                    <tr>
                        <td style="padding-right: 10px; font-weight: bold;">Клиент</td>
                        <td>{{ $client->name }}</td>
                    </tr>
                    <tr>
                        <td style="padding-right: 10px; font-weight: bold;">Проект</td>
                        <td>{{ $task->p_name }}</td>
                    </tr>
                    @if ($task->main_contact)
                        <tr>
                            <td style="padding-right: 10px; font-weight: bold;">Контактное лицо</td>
                            <td> {{ $task->main_contact->last_name }} {{ $task->main_contact->first_name }} {{ $task->main_contact->patronymic }} / Тел. {{ $task->main_contact->phone }}</td>
                        </tr>
                    @endif
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary" style="padding: 10px;">
            <div class="box-header with-border">
                <h3 class="box-title">История коментариев</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body" style="overflow-y: auto;height: 200px;">
                @if ($comments != 0)
                    @foreach($comments as $c)
                        <div class="post" style="padding-bottom: 5px;">
                            <div class="user-block" style="margin-bottom: 0px;">
                                <span class="username" style="margin-left: 0px; float: left;">
                                    <a href="#">{{ $c['user_name'] }} {{ $c['user_surname'] }}</a>
                                </span>
                                <span class="description" style="margin-left: 5px; font-size: 16px;">&nbsp;&nbsp;&nbsp;{{ $c['date'] }} в {{ $c['time'] }}</span>
                            </div>
                            <!-- /.user-block -->
                            <div id="comment_355">
                                {{ $c['text'] }}
                            </div>
                            <!--<ul class="list-inline">
                                <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Share</a></li>
                            </ul>

                            <input class="form-control input-sm" type="text" placeholder="Type a comment">-->
                        </div>
                    @endforeach
                @else
                    <p style="text-align: center; font-style: italic;">Коментарии отсутствуют</p>
                @endif
            </div>
        </div>
    </div>
    <div style="clear: both;"></div>
    <div class="col-md-12">
        <!--<div style="margin-bottom: 10px; font-weight: bold; text-decoration: underline;">
            <a href="">Информация о проекте</a><br>
            <a href="">Скрипт для разговора</a>
        </div>-->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Результат контакта</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="events-wrapper">
                    <div class="events">
                        <ol>
                            <li><a href="#0" data-step="step1" style="left: 100px;">Шаг 1</a></li>
                            <li><a href="#0" data-step="step3" style="left: 414px;">Шаг 2</a></li>
                            <li><a href="#0" data-step="step4" style="left: 571px;">Шаг 3</a></li>
                            <li><a href="#0" data-step="step5" style="left: 728px;">Шаг 4</a></li>
                            <li><a href="#0" data-step="step6" style="left: 885px;">Шаг 5</a></li>
                        </ol>

                        <span class="filling-line" aria-hidden="true"></span>
                    </div> <!-- .events -->
                </div>
                <div class="alert alert-danger alert-dismissible" id="hint" style="display: none;">
                    <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>-->
                    <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                    <span id="text"></span>
                </div>
                <div class="events-content">
                    <ol style="list-style-type: none; padding-left: 0; margin: 20px;" id="test">
                        <li data-step="step1">
                            <div class="form-group" style="margin-top: 10px;">
                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="work">
                                <label for="optionsRadios1">
                                    <h4 style="color: #3c8dbc;">Работали</h4>
                                </label>
                            </div>
                            <div class="form-group" style="margin-top: 10px;">
                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="next">
                                <label for="optionsRadios2">
                                    <h4 style="color: #3c8dbc;">Перенос на другое время</h4>
                                </label>
                            </div>
                            <div class="form-group" style="margin-top: 10px;">
                                <input type="radio" name="optionsRadios" id="optionsRadios3" value="later">
                                <label for="optionsRadios3">
                                    <h4 style="color: #3c8dbc;">Недозвон</h4>
                                </label>
                            </div>
                            <div class="form-group pull-right" style="margin-top: 10px;">
                                <a onclick="nextStep(this);" data-step="step1" class="btn btn-success">Далее</a>
                            </div>
                        </li>

                        <li data-step="step3">
                            <div class="row">
                                <form action="/admin/project-task/edit/{{ $task->id }}" method="post" id="transfer_task">
                                    {{ csrf_field() }}
                                    <input type="hidden" value="step3" name="step">
                                    <table class="table table-bordered">
                                        <tr>
                                            <td style="vertical-align: middle;">Дата</td>
                                            <td style="vertical-align: middle;">
                                                <input type="date" name="next_date2" class="form-control" value="{{ $cur_date_val }}" id="date_cont">
                                                <label id="date_cont_error" class="error" for="date_cont" style="display: none;">Это поле обязательно к заполнению</label>
                                            </td>
                                            <td style="vertical-align: middle;">Время</td>
                                            <td style="vertical-align: middle;">
                                                <input type="time" name="next_time2" class="form-control" id="time_cont">
                                                <label id="time_cont_error" class="error" for="time_cont" style="display: none;">Это поле обязательно к заполнению</label>
                                            </td>
                                        </tr>
                                    </table>

                                    <div class="col-md-12">
                                        <div class="form-group pull-right" style="margin-top: 10px;">
                                            <a onclick="setActiveSlide('step1');" class="btn btn-default">Отмена</a>
                                            <a onclick="nextStep(this);" data-step="step3" class="btn btn-success">Далее</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>

                        <li data-step="step4">
                            <form action="/admin/project-task/edit/{{ $task->id }}" method="post" id="work_task">
                                {{ csrf_field() }}
                                <input type="hidden" value="step4" name="step">
                                <div class="row">
                                    <div class="col-md-12" style="margin-bottom: 20px;">
                                        <span class="bold">Длительность контакта</span>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <a id="time1" onclick="setTimeCont2('time1');" data-content="15" class="btn btn-primary">15 мин</a>
                                                <a id="time2" onclick="setTimeCont2('time2');" data-content="30" class="btn btn-primary">30 мин</a>
                                                <a id="time3" onclick="setTimeCont2('time3');" data-content="45" class="btn btn-primary">45 мин</a>
                                                <a id="time4" onclick="setTimeCont2('time4');" data-content="60" class="btn btn-primary">1 час</a>
                                                <a id="time5" onclick="setTimeCont2('time5');" data-content="90" class="btn btn-primary">1 ч 30 мин</a>
                                            </div>
                                            <div class="col-md-7">
                                                <label id="durationTime_error" class="error" for="durationTime" style="display: none;">Это поле обязательно к заполнению</label>
                                                <input type="time" id="durationTime" name="durationTime" class="form-control" style="width: 100px;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="box">
                                            <div class="box-header">
                                                <h4><b>Текущая задача</b></h4>
                                            </div>
                                            <div class="box-body">
                                                <table class="table" id="tableTask">
                                                    @foreach($tasks as $t)
                                                        @if($t['id'] == $task->id)
                                                            <tr>
                                                                <td class="tasks" id="task_{{ $loop->index+1 }}" data-content="{{ $t['id'] }}">{{ $t['name'] }}</td>
                                                                <td>
                                                                    <select id="task_status_{{ $loop->index+1 }}" onchange="setCurTaskStatus(this);" class="form-control">
                                                                        @foreach($task_status as $ts)
                                                                            @if ($ts['name'] == $t['status_name'])
                                                                                <option value="{{ $ts['id'] }}" selected>{{ $ts['name'] }}</option>
                                                                            @else
                                                                                <option value="{{ $ts['id'] }}">{{ $ts['name'] }}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>

                                        <div class="box">
                                            <div class="box-header">
                                                <h4><b>Другие задачи</b></h4>
                                            </div>
                                            <div class="box-body">
                                                <table class="table" id="tableOtherTask">
                                                    <tr id="beforeOtherTask"></tr>
                                                </table>
                                            </div>
                                            <div class="box-footer">
                                                <a onclick="addOtherTask()" class="btn btn-primary">Добавить</a>
                                            </div>
                                        </div>

                                        <div class="box">
                                            <div class="box-header">
                                                <h4><b>Вопросы для уточнения</b></h4>
                                            </div>
                                            <div class="box-body">
                                                <table class="table" id="tableTask2">
                                                    <tbody>
                                                    <tr id="tr_task"></tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="box">
                                            <div class="box-header">
                                                <h4><b>Добавте дополнительные задачи, которые выявились при работе</b></h4>
                                            </div>
                                            <div class="box-body">
                                                <table class="table" id="tableDopTask">
                                                    <tr>
                                                        <td><b>Наименование задачи</b></td>
                                                        <td><b>Модуль</b></td>
                                                        <td><b>Кол - во контактов</b></td>
                                                        <td><b>Группа задач</b></td>
                                                        <td><b>Действия</b></td>
                                                    </tr>
                                                    <tr id="beforeDopTask"></tr>
                                                    <!--<tr>
                                                        <td>
                                                            <input type="text" class="form-control" name="nameDopTask">
                                                        </td>
                                                        <td>
                                                            <select name="moduleDopTask" id="" class="form-control">
                                                                @foreach($modules as $m)
                                                                    <option value="{{ $m['id'] }}">{{ $m['name'] }}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td><input type="text" class="form-control" name="numContDopTask"></td>
                                                        <td>
                                                            <select name="groupDopTask" id="" class="form-control">
                                                                @foreach($group_task as $gt)
                                                                    <option value="{{ $gt['id'] }}">{{ $gt['name'] }}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <a onclick="addDopTasks();" class="btn btn-success"><i class="fa fa-plus"></i></a>
                                                        </td>
                                                    </tr>-->
                                                </table>
                                            </div>
                                            <div class="box-footer">
                                                <a onclick="addDopTask()" class="btn btn-primary">Добавить</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group" style="margin-top: 10px;">
                                                <input type="radio" name="nextContact" id="nextContact" value="1">
                                                <label for="nextContact">
                                                    <h4 style="color: #3c8dbc;">Назначен следующий контакт</h4>
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <input type="radio" name="nextContact" id="laterNextContact" value="2">
                                                <label for="laterNextContact">
                                                    <h4 style="color: #3c8dbc;">Договориться о следующем контакте позже</h4>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <div class="form-group pull-right" style="margin-top: 10px;">
                                        <a onclick="setActiveSlide('step1');" class="btn btn-default">Отмена</a>
                                        <a onclick="nextStep(this);" data-step="step4" class="btn btn-success">Далее</a>
                                    </div>
                                </div>
                            </form>
                        </li>

                        <li data-step="step5">
                            <div class="row">
                                <form action="/admin/project-task/edit/{{ $task->id }}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" value="step5" name="step">
                                    <div class="box-body">
                                        <div style="padding-bottom: 20px;">
                                            <table class="table table-bordered">
                                                <tr>
                                                    <td style="vertical-align: middle;">Дата / Время</td>
                                                    <td style="vertical-align: middle;">
                                                        <a id="date1" onclick="setDateCont('date1');" data-content="{{ $tomorrow_val }}" class="btn btn-primary">Завтра</a>
                                                        <a id="date2" onclick="setDateCont('date2');" data-content="{{ $plus_one_day_val }}" class="btn btn-primary">Послезавтра</a>
                                                        <a id="date3" onclick="setDateCont('date3');" data-content="{{ $plus_two_day_val }}" class="btn btn-primary">{{ $plus_two_day }}</a>
                                                        <a id="date4" onclick="setDateCont('date4');" data-content="{{ $plus_three_day_val }}" class="btn btn-primary">{{ $plus_three_day }}</a>
                                                    </td>
                                                    <td style="vertical-align: middle;"><input type="date" name="next_date3" value="{{ $cur_date_val }}" id="date_cont2"></td>
                                                    <td style="vertical-align: middle;"><input type="time" name="next_time3" id="time_cont"></td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: middle;">Продолжительность</td>
                                                    <td style="vertical-align: middle;">
                                                        <div style="padding-bottom: 20px;">
                                                            <a id="ttime1" onclick="setTimeCont('time1');" data-content="30" class="btn btn-primary">30 мин.</a>
                                                            <a id="ttime2" onclick="setTimeCont('time2');" data-content="60" class="btn btn-primary">1 час</a>
                                                            <a id="ttime3" onclick="setTimeCont('time3');" data-content="90" class="btn btn-primary">1 час 30 мин.</a>
                                                        </div>
                                                    </td>
                                                    <td colspan="2" style="vertical-align: middle;">
                                                        <input type="time" id="next_time2" name="duration">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: middle;">Контактное лицо</td>
                                                    <td style="vertical-align: middle;">
                                                        <select class="form-control" name="contact">
                                                            @foreach($task->project_contacts as $c)
                                                                <option value="{{ $c['id'] }}">{{ $c['first_name'] }} {{ $c['last_name'] }} {{ $c['patronymic'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td style="vertical-align: middle;">Телефон</td>
                                                    <td colspan="2" style="vertical-align: middle;">
                                                        <select class="form-control" name="contact_phone">
                                                            @foreach($task->project_contacts as $c)
                                                                <option value="{{ $c['phone'] }}">{{ $c['phone'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: middle;">Внедренец</td>
                                                    <td style="vertical-align: middle;" colspan="3">
                                                        <select class="form-control" name="user_id">
                                                            @foreach($observers as $observer)
                                                                @if($observer['default'] == 1)
                                                                    <option value="{{ $observer['id'] }}" selected>{{ $observer['last_name'] }} {{ $observer['first_name'] }}</option>
                                                                @else
                                                                    <option value="{{ $observer['id'] }}">{{ $observer['last_name'] }} {{ $observer['first_name'] }}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: middle;">Задача</td>
                                                    <td style="vertical-align: middle;" colspan="3">
                                                        <select name="dopTask" id="dopTask" class="form-control">
                                                            @foreach($tasks as $t)
                                                                @if($t['id'] != $task->id)
                                                                    <option value="{{ $t['id'] }}">{{ $t['name'] }}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div style="margin-bottom: 10px; font-weight: bold; text-decoration: underline;">
                                                <a onclick="showCalendar()" style="cursor: pointer;">Открыть календарь</a><br>
                                            </div>
                                            <div id="amCalendar" style="display: none;">

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group pull-right" style="margin-top: 10px;">
                                            <a onclick="setActiveSlide('step1');" class="btn btn-default">Отмена</a>
                                            <a onclick="nextStep(this);" data-step="step5" class="btn btn-success">Далее</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>

                        <li data-step="step6">
                            <div class="row">
                                <form action="/admin/project-task/edit/{{ $task->id }}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" value="step6" name="step">
                                    <input type="hidden" name="res" id="res">
                                    <input type="hidden" name="data" id="data">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="optionsRadios2">
                                                <h4 style="color: #3c8dbc;">* Комментарий (для клиента)</h4>
                                            </label>
                                        </div>
                                        <label id="editor5_error" class="error" for="editor5" style="display: none;">Это поле обязательно к заполнению</label>
                                        <div id="editor5"></div>
                                        <textarea name="comment5" style="display: none;" cols="30" rows="10"></textarea>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="optionsRadios2">
                                                <h4 style="color: #3c8dbc;">Комментарий (для внутреннего использования)</h4>
                                            </label>
                                        </div>
                                        <div id="editor6"></div>
                                        <textarea name="comment6" style="display: none;" cols="30" rows="10"></textarea>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group pull-right" style="margin-top: 10px;">
                                            <a onclick="setActiveSlide('step1');" class="btn btn-default">Отмена</a>
                                            <input type="submit" value="Сохранить" class="btn btn-primary">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            result = [];
            dopT_ar = [];
            data = [];
            nextCont = {};
            obj = {};
            obj['cq'] = [];
            obj['cqOtherTask'] = [];
            cq_ar = [];
            cq = {};
            dopTask = {};
            obj['dopTask'] = [];
            dopTask_ar = [];
            otherTask = {};
            otherTask_ar = [];
            obj['otherTask'] = [];

            setActiveSlide('step1');
            $("#hint").css('display', 'none');

            tid = $("#tid").val();
            $.ajax({
                url: '/admin/project-task/getClarificationQuestions',
                data: {'tid': tid},
                success: function (resp) {
                    //console.log(resp);
                    content = '';
                    if (resp != '') {
                        for (i=0; i<resp.length; i++) {
                            cq = {};
                            cq['id'] = resp[i].id;
                            cq['question'] = resp[i].question;
                            cq['answer'] = resp[i].answer;
                            cq_ar.push(cq);
                            obj['cq'] = cq_ar;
                            content = '<tr>' +
                                '<td>'+resp[i].question+'</td>';
                            if (resp[i].answer == '') {
                                content += '<td>' +
                                        '<input type="text" name="answer-field_'+resp[i].id+'" class="form-control">' +
                                        '<label id="answer_field_error" class="error" for="answer-field" style="display: none;">Это поле обязательно к заполнению</label>' +
                                    '</td>';
                            } else {
                                content += '<td><input type="text" name="answer-field_'+resp[i].id+'" class="form-control" value="'+resp[i].answer+'" disabled></td>';
                            }
                            content += '<td><a onclick="saveAnswer('+resp[i].id+')" class="btn btn-success"><i class="fa fa-check"></i></a></td>' +
                                '</tr>';

                            $('#tableTask2 #tr_task').before(content);
                        }
                    } else {
                        content = '<tr id="noTask">' +
                            '<td>Уточняющие вопросы не найдены</td>' +
                            '</tr>' +
                            '<tr id="tr_task"></tr>';
                        $('#tableTask2 > tbody').html(content);
                    }
                    console.log(obj['cq']);
                }
            });

            $(function () {
                CKEDITOR.replace('editor');
                $('.textarea').wysihtml5();
            });

            $(function () {
                CKEDITOR.replace('editor2');
                $('.textarea').wysihtml5();
            });

            $(function () {
                CKEDITOR.replace('editor3');
                $('.textarea').wysihtml5();
            });

            $(function () {
                CKEDITOR.replace('editor4');
                $('.textarea').wysihtml5();
            });

            $(function () {
                CKEDITOR.replace('editor5');
                $('.textarea').wysihtml5();
            });

            $(function () {
                CKEDITOR.replace('editor6');
                $('.textarea').wysihtml5();
            });

            $("input[name~='next_date3']").on('paste', function () {
                alert("Ok");
            });
            $("select[name~='user_id']").on('change', function () {
                if ($("#amCalendar").hasClass('active')) {
                    123
                }
            });
        });
        function saveAnswer(id) {
            answer_val = $("input[name~='answer-field_"+id+"']").val();
            $("#answer_field_error").css('display', 'none');
            if (answer_val) {
                for (i=0;i<obj['cq'].length;i++) {
                    if (obj['cq'][i]['id'] == id) {
                        obj['cq'][i]['answer'] = answer_val;
                        //alert('Ваш ответ принят');
                        $("input[name~='answer-field_"+id+"']").attr('disabled', 'true');
                        //$('#inputAnswer_'+id).slideToggle('fast');
                    }
                }
            } else {
                alert("Ответ не может быть пустым");
            }
        };
        function saveAnswerOtherTask(id) {
            answer_val = $("input[name~='answer-field_"+id+"']").val();
            if (answer_val) {
                for (i=0;i<obj['cqOtherTask'].length;i++) {
                    if (obj['cqOtherTask'][i]['id'] == id) {
                        obj['cqOtherTask'][i]['answer'] = answer_val;
                        //alert('Ваш ответ принят');
                        $("input[name~='answer-field_"+id+"']").attr('disabled', 'true');
                        //$('#inputAnswer_'+id).slideToggle('fast');
                    }
                }
            } else {
                alert("Ответ не может быть пустым");
            }
        };
        function showCalendar() {
            $("#amCalendar").slideToggle("fast", function () {
                if ($(this).hasClass('active')) {
                    $(this).removeClass('active');
                } else {
                    $(this).addClass('active');
                    date = $("#date_cont2").val();
                    user = $("select[name~='user_id'] option:selected").html();
                    user_val = $("select[name~='user_id'] option:selected").val();

                    if (!date) {
                        alert('Укажите дату');
                        return false;
                    }

                    $.ajax({
                        url: '/admin/tasks/get-taks-am-calendar',
                        //async: false,
                        data: {'date': date, 'user_id': user_val, 'user': user},
                        success: function (resp) {
                            content = '';
                            if (resp[0] != '') {
                                content = '<div class="amCalendarHeader">' +
                                    '<table>' +
                                    '<tr>' +
                                    '<td>'+user+'</td>' +
                                    '<td>'+date+'</td>' +
                                    '</tr>' +
                                    '</table>' +
                                    '</div>' +
                                    '<div class="amCalendarContent">' +
                                    '<table>';

                                for (i=0;i<resp.length;i++) {
                                    content += '<tr>' +
                                        '<td>'+resp[i].time+'</td>' +
                                        '<td>'+resp[i].task+'</td>' +
                                        '</tr>';
                                }

                                content += '</table>' +
                                    '</div>';

                                $("#amCalendar").html(content);
                            } else {
                                content = '<div class="amCalendarHeader">' +
                                    '<table>' +
                                    '<tr>' +
                                    '<td>'+user+'</td>' +
                                    '<td>'+date+'</td>' +
                                    '</tr>' +
                                    '</table>' +
                                    '</div>' +
                                    '<div class="amCalendarContent">' +
                                    '<table>' +
                                    '<tr>' +
                                    '<td colspan="2">Задачи отсутствуют</td>' +
                                    '<tr>' +
                                    '</table>';
                                $("#amCalendar").html(content);
                            }
                        }
                    });
                }
            });
        }

        function answer(id) {
            for (i=0;i<obj['cq'].length;i++) {
                if (obj['cq'][i]['id'] == id) {
                    if ('answer' in obj['cq'][i]) {
                        $("input[name~='answer-field_"+id+"']").val(obj['cq'][i]['answer']);
                    }
                }
            }
            //$("#inputAnswer_"+id).slideToggle("fast");
        };
        function nextStep(el) {
            step = $(el).data('step');
            switch (step) {
                case 'step1':
                    $("#hint").css('display', 'none');
                    resultTask();
                    break;
                case 'step3':
                    $("#hint").css('display', 'none');
                    date = $("#date_cont").val();
                    $("#time_cont_error").css('display', 'none');
                    $("#date_cont_error").css('display', 'none');
                    if (!date) {
                        $("#date_cont_error").css('display', 'block');
                        break;
                    }
                    time = $("#time_cont").val();
                    if (!time) {
                        $("#time_cont_error").css('display', 'block');
                        break;
                    }

                    $.ajax({
                        url: '/admin/tasks/get-taks-by-datetime',
                        data: {'date': date, 'time': time},
                        success: function (resp) {
                            //alert(resp);
                            nextCont = {};
                            nextCont.date = date;
                            nextCont.time = time;
                            data['nextContDate'] = date;
                            data['nextContTime'] = time;
                            setActiveSlide('step6');
                        }
                    });
                    break;
                case 'step4':
                    $("#hint").css('display', 'none');
                    res = 1;
                    res2 = 1;
                    dopTask_ar = [];
                    otherTask_ar = [];
                    duration = $('#durationTime').val();
                    $("#durationTime_error").css('display', 'none');

                    if (!duration) {
                        $("#durationTime_error").css('display', 'block');
                    }

                    if (Object.keys(obj['cq']).length > 0) {
                        for (i=0;i<obj['cq'].length;i++) {
                            if ('answer' in obj['cq'][i] && obj['cq'][i]['answer'] != '') {
                                res = 0;
                            } else {
                                res = 1;
                            }
                        }
                    } else {
                        res = 0;
                    }

                    if (Object.keys(obj['cqOtherTask']).length > 0) {
                        console.log(obj['cqOtherTask']);
                        for (i=0;i<obj['cqOtherTask'].length;i++) {
                            if ('answer' in obj['cqOtherTask'][i] && obj['cqOtherTask'][i]['answer'] != '') {
                                res2 = 0;
                            } else {
                                res2 = 1;
                            }
                        }
                    } else {
                        res2 = 0;
                    }

                    count = $(".dopTaskRow").length;
                    if (count > 0) {
                        for(j=0;j<count;j++) {
                            id = $(".dopTaskRow:eq("+(j-1)+")").attr('id');
                            task = $("#dopTask_"+id).val();
                            module = $("#moduleDopTask_"+id+" option:selected").val();
                            num = $("#numTask_"+id).val();
                            group = $("#groupTask_"+id+" option:selected").val();
                            dopTask = {};
                            dopTask = {
                                'id': Number(id),
                                'name': task,
                                'module': module,
                                'numCont': num,
                                'group': group
                            };
                            dopTask_ar.push(dopTask);
                        }

                        obj['dopTask'] = dopTask_ar;
                    }

                    count2 = $(".otherTaskRow").length;
                    if (count2 > 0) {
                        for(k=1;k<=count2;k++) {
                            id2 = $(".otherTaskRow:eq("+(k-1)+")").attr('id');
                            otherTaskId = $("#selectOtherTask_"+id2+" option:selected").val();
                            otherTaskStatus = $("#otherTask_status_"+id2+" option:selected").val();
                            otherTask = {};
                            otherTask = {
                                'id': Number(id2),
                                'taskId': otherTaskId,
                                'status': otherTaskStatus
                            };
                            otherTask_ar.push(otherTask);
                        }

                        obj['otherTask'] = otherTask_ar;
                    }

                    console.log(res);
                    $("#answer_field_error").css('display', 'none');

                    if (res == 0 && res2 == 0) {
                        resultStep();
                    } else {
                        $("#answer_field_error").css('display', 'block');
                        break;
                    }
                    break;
                case 'step5':
                    $("#hint").css('display', 'none');
                    next_date = $("input[name~='next_date3']").val();
                    next_time = $("input[name~='next_time3']").val();
                    duration2 = $("#next_time2").val();
                    user_id = $("select[name~='user_id'] option:selected").val();
                    dopTask = $("select#dopTask option:selected").val();

                    if (next_time) {
                        obj['nextContDateStart'] = next_date;
                        obj['nextContTimeStart'] = next_time;
                        obj['nextContDuration'] = duration2;
                        obj['nextContUser'] = user_id;
                        obj['nextContTask'] = dopTask;

                        setActiveSlide('step6');
                    } else {
                        alert('Укажимте время');
                        break;
                    }
                    break;
            }
        }
        function resultTask() {
            t = $('input[name~="optionsRadios"]:checked').val();
            //$("#hint").hide();
            if (!t) {
                $("#hint #text").html('Выберите один из результатов контакта');
                $("#hint").css('display', 'block');
            } else {
                switch (t) {
                    case 'work':
                        result = [];
                        result.push(['work']);
                        setActiveSlide('step4');
                        break;
                    case 'next':
                        result = [];
                        result.push(['next']);
                        setActiveSlide('step3');
                        break;
                    case 'later':
                        result = [];
                        result.push(['later']);
                        setActiveSlide('step6');
                        break;
                }
            }
        }
        function resultStep() {
            step = $('input[name~="nextContact"]:checked').attr('id');
            if (!step) {
                $("#hint #text").html('Выберите результат следующего контакта');
                $("#hint").css('display', 'block');
            } else {
                durationTime = '';
                durationTime = $("#durationTime").val();
                obj.durationTime = '';
                obj.durationTime = durationTime;
                //data['durationTime'] = obj.durationTime;

                switch (step) {
                    case 'nextContact':
                        obj.nextContact = '';
                        obj.nextContact = 1;
                        //data['nextContact'] = obj.nextContact;
                        //data.push(obj);
                        setActiveSlide('step5');
                        break;
                    case 'laterNextContact':
                        obj.nextContact = '';
                        obj.nextContact = 0;
                        //data['nextContact'] = obj.nextContact;
                        //data.push(obj);
                        setActiveSlide('step6');
                        break;
                }
            }
        }
        function addOtherTask() {
            i = $(".otherTaskRow").length;
            ar_otherTaskId = [];
            content = '';
            if (i == 0) {
                id = 0;
            } else {
                id = $(".otherTaskRow:eq("+(i-1)+")").attr('id');
            }
            content = '<tr class="otherTaskRow" id="'+(Number(id)+1)+'">' +
                '<td class="tasks" id="task_'+(Number(id)+1)+'" data-content="{{ $t["id"] }}">' +
                '<select class="form-control" id="selectOtherTask_'+(Number(id)+1)+'" onchange="changeSelect(\''+(Number(id)+1)+'\')">' +
                 '@foreach($tasks as $t)' +
                '@if($t["id"] != $task->id)' +
                '@if($t["form"] == 1)' +
                '<option value="{{ $t["id"] }}" data-content="form">{{ $t["name"] }}</option>' +
                '@else' +
                '<option value="{{ $t["id"] }}" data-content="task">{{ $t["name"] }}</option>' +
                '@endif' +
                '@endif' +
                '@endforeach' +
                '</select>' +
                '</td>' +
                '<td>' +
                '<select id="otherTask_status_'+(Number(id)+1)+'" class="form-control">' +
                '@foreach($task_status as $ts)' +
                '<option value="{{ $ts["id"] }}">{{ $ts["name"] }}</option>' +
                '@endforeach' +
                '</select>' +
                '</td>' +
                '<td>' +
                '<a onclick="delOtherTask('+(Number(id)+1)+')" class="btn btn-danger"><i class="fa fa-trash"></i></a>' +
                '</td>' +
                '</tr>';
            $("#beforeOtherTask").before(content);
            addClarificationQuestions();
        }
        function changeSelect(id) {
            addClarificationQuestions();
            getStatusTask(id);
        }
        function delOtherTask(id) {
            /*console.log(obj['otherTask']);
            for (i=0;i<obj['otherTask'].length;i++) {
                if (obj['otherTask'][i]['id'] == id) {
                    delete obj['otherTask'][i];
                }
            }*/
            $("#tableOtherTask tr#"+id).remove();
            addClarificationQuestions();
        }
        function addClarificationQuestions() {
            content = '';
            count = $(".otherTaskRow").length;
            cq_ar2 = [];
            obj['cqOtherTask'] = [];

            for(j=1;j<=count;j++) {
                id = $(".otherTaskRow:eq("+(j-1)+")").attr('id');
                task_id = $("#selectOtherTask_"+id+" option:selected").val();

                $.ajax({
                    url: '/admin/project-task/getClarificationQuestions',
                    data: {'tid': task_id},
                    async: false,
                    success: function (resp) {
                        if (resp != '') {
                            for (i=0; i<resp.length; i++) {
                                $('#tableTask2 #noTask').remove();
                                cq2 = {};
                                cq2['id'] = resp[i].id;
                                cq2['question'] = resp[i].question;
                                cq2['answer'] = resp[i].answer;
                                cq_ar2.push(cq2);
                            }
                        }
                    }
                });
            }
            for(ind=0;ind<cq_ar2.length;ind++) {
                obj['cqOtherTask'].push(cq_ar2[ind]);
            }

            //console.log(obj['cq']);

            if (obj['cq'].length == 0 && obj['cqOtherTask'].length == 0) {
                content = '<tr id="noTask">' +
                    '<td>Уточняющие вопросы не найдены</td>' +
                    '</tr>' +
                    '<tr id="tr_task"></tr>';
                $('#tableTask2 > tbody').html(content);
            } else {
                content = '';
                for(k=0;k<obj['cq'].length;k++) {
                    content += '<tr>' +
                        '<td>'+obj['cq'][k]['question']+'</td>';
                    if (obj['cq'][k]['answer']) {
                        content += '<td>' +
                                '<input type="text" name="answer-field_'+obj['cq'][k]['id']+'" class="form-control" value="'+obj['cq'][k]['answer']+'" disabled>' +
                            '</td>';
                    } else {
                        content += '<td><input type="text" name="answer-field_'+obj['cq'][k]['id']+'" class="form-control"></td>';
                    }
                    content += '<td><a onclick="saveAnswer('+obj['cq'][k]['id']+')" class="btn btn-success"><i class="fa fa-check"></i></a></td>' +
                        '</tr>';
                }
                $('#tableTask2 tbody').html(content);

                content2 = '';
                for(k=0;k<obj['cqOtherTask'].length;k++) {
                    content2 += '<tr>' +
                        '<td>'+obj['cqOtherTask'][k]['question']+'</td>';
                    if (obj['cqOtherTask'][k]['answer']) {
                        content2 += '<td><input type="text" name="answer-field_'+obj['cqOtherTask'][k]['id']+'" class="form-control" value="'+obj['cqOtherTask'][k]['answer']+'" disabled></td>';
                    } else {
                        content2 += '<td><input type="text" name="answer-field_'+obj['cqOtherTask'][k]['id']+'" class="form-control"></td>';
                    }
                    content2 += '<td><a onclick="saveAnswerOtherTask('+obj['cqOtherTask'][k]['id']+')" class="btn btn-success"><i class="fa fa-check"></i></a></td>' +
                        '</tr>';
                }
                $('#tableTask2 tbody').append(content2);
            }
        }
        function getStatusTask(id) {
            type = $("#selectOtherTask_"+id+" option:selected").attr('data-content');
            content = '';
            switch(type) {
                case 'form':
                    $.ajax({
                        url: '/admin/print-form/get-status',
                        success: function (data) {
                            //console.log(data);
                            $("#otherTask_status_"+id).html('');
                            for(i=0;i<data.length;i++) {
                                content += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                            }
                            $("#otherTask_status_"+id).html(content);
                        }
                    });
                    break;
                case 'task':
                    $.ajax({
                        url: '/admin/tasks/get-status',
                        success: function (data) {
                            //console.log(data);
                            $("#otherTask_status_"+id).html('');
                            for(i=0;i<data.length;i++) {
                                content += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                            }
                            $("#otherTask_status_"+id).html(content);
                        }
                    });
                    break;
            }
        }
        function addDopTask() {
            i = $(".dopTaskRow").length;
            if (i == 0) {
                id = 0;
            } else {
                id = $(".dopTaskRow:eq("+(i-1)+")").attr('id');
            }
            $("#beforeDopTask").before(
                '<tr class="dopTaskRow" id="'+(Number(id)+1)+'">' +
                '<td class="tasks" data-content="{{ $t["id"] }}">' +
                '<input type="text" id="dopTask_'+(Number(id)+1)+'" class="form-control">' +
                '</td>' +
                '<td>' +
                '<select name="moduleDopTask" id="moduleDopTask_'+(Number(id)+1)+'" class="form-control">' +
                '@foreach($modules as $m)' +
                '<option value="{{ $m["id"] }}">{{ $m["name"] }}</option>' +
                '@endforeach' +
                '</select>' +
                '</td>' +
                '<td >' +
                '<input type="number" id="numTask_'+(Number(id)+1)+'" class="form-control">' +
                '</td>' +
                '<td>' +
                '<select name="groupDopTask" id="groupTask_'+(Number(id)+1)+'" class="form-control">' +
                '@foreach($group_task as $gt)' +
                '<option value="{{ $gt["id"] }}">{{ $gt["name"] }}</option>' +
                '@endforeach' +
                '</select>' +
                '</td>' +
                '<td>' +
                '<a onclick="delDopTask('+(Number(id)+1)+')" class="btn btn-danger"><i class="fa fa-trash"></i></a>' +
                '</td>' +
                '</tr>'
            );


        }
        Array.prototype.remove = function(value) {
            var idx = this.indexOf(value);
            if (idx != -1) {
                // Второй параметр - число элементов, которые необходимо удалить
                return this.splice(idx, 1);
            }
            return false;
        }
        function delDopTask(id) {
            /*for(i=0;i<obj['dopTask_ar'].length;i++) {
                if (obj['dopTask_ar'][i]['id'] == id) {
                    obj['dopTask_ar'].remove(obj['dopTask_ar'][i]);
                }
            }*/
            $("#tableDopTask tr#"+id).remove();
        }
        function showDopTasks() {
            $('#dopTasks').slideToggle('fast');
        }
        function saveDopTasks() {

        }
        function delDopTasks(id) {
            $('tr#'+id).remove();
        }
        function addDopTasks() {
            nameNewTask = $("input[name~='nameDopTask']").val();
            moduleNewTask = $("select[name~='moduleDopTask'] option:selected").html();
            moduleNewTask_val = $("select[name~='moduleDopTask'] option:selected").val();
            numContNewTask = $("input[name~='numContDopTask']").val();
            groupNewTask = $("select[name~='groupDopTask'] option:selected").html();
            groupNewTask_val = $("select[name~='groupDopTask'] option:selected").val();

            if ($(".newDopTask").length != 0) {
                i = $(".newDopTask").length;
                id = $(".newDopTask").find("tr:eq("+i+")").attr('id');
            } else {
                id = 0;
            }

            if (!nameNewTask) {
                alert('Укажите название задачи');
                exit();
            }
            if (!numContNewTask) {
                alert('Укажите количество контактов');
                exit();
            }

            dopTask = {};
            dopTask['name'] = nameNewTask;
            dopTask['module'] = moduleNewTask_val;
            dopTask['numCont'] = numContNewTask;
            dopTask['group'] = groupNewTask_val;

            dopTask_ar.push(dopTask);
            obj['dopTask'] = dopTask_ar;

            $("#newRowDopTask").before('<tr class="newDopTask" id="'+(Number(id)+1)+'">' +
                    '<td><span id="nameNewTask_'+(Number(id)+1)+'">'+nameNewTask+'</span></td>' +
                    '<td><span id="moduleNewTask_'+(Number(id)+1)+'">'+moduleNewTask+'</span></td>' +
                    '<td><span id="numContNewTask_'+(Number(id)+1)+'">'+numContNewTask+'</span></td>' +
                    '<td><span id="groupNewTask_'+(Number(id)+1)+'">'+groupNewTask+'</span></td>' +
                    '<td>' +
                        '<a onclick="" class="btn btn-success"><i class="fa fa-pencil"></i></a>' +
                        '<a onclick="delDopTasks('+(Number(id)+1)+')" class="btn btn-danger"><i class="fa fa-trash"></i></a>' +
                    '</td>' +
                '</tr>');
        }
        function setDateCont(id) {
            date = $('#'+id).attr('data-content');
            $('#date_cont2').val(date);
        }

        function setTimeCont(id) {
            date = $('#date_cont').val();
            D = new Date(date+' 00:00:00');
            new_time = $('#t'+id).attr('data-content');
            val = D.toLocaleTimeString('en-GB', D.setMinutes(D.getMinutes()+Number(new_time)));
            $("#next_time2").val(val);
        }

        $('form').submit(function () {
            $('#res').val('');
            $('#res').val(result);
            $("#editor5_error").css('display', 'none');

            //console.log(JSON.stringify(data));

            comment = CKEDITOR.instances.editor5.getData();
            comment2 = CKEDITOR.instances.editor6.getData();
            if (result != 'later') {
                /*if (!comment2) {
                    alert('Укажите комментарий (для внутреннего использования)');
                    return false;
                }*/
            }
            if (!comment) {
                $("#editor5_error").css('display', 'block');
                return false;
            }

            $('textarea[name~="comment5"]').html(comment);
            $('textarea[name~="comment6"]').html(comment2);

            if (result == 'work') {
                obj['comment'] = comment;
                obj['comment2'] = comment2;
                data.push(obj);
            }

            if (result == 'next') {
                data.push(nextCont);
            }

            $('#data').val('');
            $('#data').val(JSON.stringify(data));

            return true;
        });

        Array.prototype.in_array = function(p_val) {
            for(var i = 0, l = this.length; i < l; i++)  {
                if(this[i] == p_val) {
                    return true;
                }
            }
            return false;
        }

        function questionsClarification(tid) {
            $('#tableTask2 > tbody > tr').remove();
            $.ajax({
                url: '/admin/project-task/getClarificationQuestions',
                data: {'tid': tid},
                success: function (resp) {
                    id = [];
                    content = '';
                    if (resp != '') {
                        if (typeof obj['cq'] != 'undefined') {
                            for (j=0;j<obj['cq'].length;j++) {
                                id.push(obj['cq'][j]['id']);
                            }
                            for (i=0; i<resp.length; i++) {
                                if (id.in_array(resp[i].id) == false) {
                                    cq = {};
                                    cq['id'] = resp[i].id;
                                    cq['question'] = resp[i].question;
                                    cq_ar.push(cq);
                                    obj['cq'] = cq_ar;
                                }
                                content += '<tr>' +
                                    '<td>'+resp[i].question+'</td>' +
                                    '<td><a onclick="answer('+resp[i].id+')" style="cursor: pointer;">Заполнить ответ</a></td>' +
                                    '</tr>'+
                                    '<tr id="inputAnswer_'+resp[i].id+'" style="display: none;">' +
                                    '<td colspan="2"><input type="text" name="answer-field_'+resp[i].id+'" class="form-control"></td>' +
                                    '<td><a onclick="saveAnswer('+resp[i].id+')" class="btn btn-success"><i class="fa fa-check"></i></a></td>' +
                                    '</tr>';
                            }
                            $('#tableTask2 > tbody').html(content);
                        } else {
                            //console.log(resp);
                            for (i=0; i<resp.length; i++) {
                                cq = {};
                                cq['id'] = resp[i].id;
                                cq['question'] = resp[i].question;
                                cq_ar.push(cq);
                                obj['cq'] = cq_ar;
                                content = '<tr>' +
                                    '<td>'+resp[i].question+'</td>' +
                                    '<td><a onclick="answer('+resp[i].id+')" style="cursor: pointer;">Заполнить ответ</a></td>' +
                                    '</tr>'+
                                    '<tr id="inputAnswer_'+resp[i].id+'" style="display: none;">' +
                                    '<td colspan="2"><input type="text" name="answer-field_'+resp[i].id+'" class="form-control"></td>' +
                                    '<td><a onclick="saveAnswer('+resp[i].id+')" class="btn btn-success"><i class="fa fa-check"></i></a></td>' +
                                    '</tr>';
                                $('#tableTask2 > tbody').html(content);
                            }
                        }
                    } else {
                        content += '<tr>' +
                            '<td>Уточняющие вопросы не найдены</td>' +
                            '</tr>';
                        $('#tableTask2 > tbody').html(content);
                    }
                }
            });
        }
        function setTimeCont2(id) {
            time = $('#time_cont').val();
            date = $('#date_cont').val();
            D = new Date(date+' 00:00:00');
            new_time = $('#'+id).attr('data-content');
            val = D.toLocaleTimeString('en-GB', D.setMinutes(D.getMinutes()+Number(new_time)));
            $("#durationTime").val(val);
        }

        function setActiveSlide(el) {
            list = $("#test li");
            list.each(function (indx, element) {
                if ($(element).data('step') == el) {
                    $(element).removeClass('unselected');
                    $(element).addClass('selected');
                } else {
                    $(element).removeClass('selected');
                    $(element).addClass('unselected');
                }
            });
            list_a = $(".events ol li a");
            list_a.each(function (indx, element2) {
                if ($(element2).data('step') == el) {
                    $(element2).removeClass('unselected2');
                    $(element2).addClass('selected2');
                } else {
                    $(element2).removeClass('selected2');
                    $(element2).addClass('unselected2');
                }
            });
        }

        $("#task_status_1").change(function () {
            status = $("#task_status_1 option:selected").val();
            obj.cur_task_status = '';
            obj.cur_task_status = status;
            data['cur_task_status'] = obj.cur_task_status;
        });

        function setCurTaskStatus(el) {
            id = el.id;
            status = $("#"+id+" option:selected").val();

            obj['curTaskStatus'] = status;
        }

        function setOtherTaskStatus(el) {
            id = el.id;
            status = $("#"+id+" option:selected").val();
            alert(status);
            //console.log(obj);
            //obj['curTaskStatus'] = status;
        }
    </script>
@endpush