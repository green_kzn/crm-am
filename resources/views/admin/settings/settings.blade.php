@extends('layouts.cabinet')

@section('title')
    Общие настройки
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs" style="float: left; width: 20%; height: 100%;">
            <li class="active" style="width: 200px; background-color: white;"><a href="#mail" data-toggle="tab">Почта</a></li>
            <li style="width: 200px; background-color: white;"><a href="#main" data-toggle="tab">Основные настройки</a></li>
        </ul>
        <div class="tab-content" style="float: left; width: 80%;">
            <div class="tab-pane active" id="mail" style="min-height: 600px;">
                <div class="box box-solid">

                </div>
            </div>

            <div class="tab-pane" id="main" style="min-height: 600px;">
                <div class="box box-solid">
                    <form class="form-horizontal" action="/admin/settings/save" method="post" id="saveMainSettings">
                        {{ csrf_field() }}
                        <table class="table table-condensed">
                            <tbody>
                            <tr>
                                <td style="vertical-align: middle; width: 33%;">Использовать производственный календарь</td>
                                @if($production_calendar != NULL)
                                    <td style="vertical-align: middle; width: 33%;">
                                        <input type="radio" name="useCalendar" id="On" value="on" checked>
                                        <label for="On">
                                            <h4 style="color: #3c8dbc;">Да</h4>
                                        </label>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <input type="radio" name="useCalendar" id="off" value="off" checked="">
                                        <label for="off">
                                            <h4 style="color: #3c8dbc;">Нет</h4>
                                        </label>
                                    </td>
                                @else
                                    <td style="vertical-align: middle; width: 33%;">
                                        <input type="radio" name="useCalendar" id="On" value="on" checked="">
                                        <label for="On">
                                            <h4 style="color: #3c8dbc;">Да</h4>
                                        </label>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <input type="radio" name="useCalendar" id="off" value="off" checked>
                                        <label for="off">
                                            <h4 style="color: #3c8dbc;">Нет</h4>
                                        </label>
                                    </td>
                                @endif
                            </tr>
                            <tr>
                                <td style="vertical-align: middle;">Ссылка</td>
                                <td style="vertical-align: middle;">
                                    <input type="text" name="link" class="form-control">
                                </td>
                                <td>
                                    <a id="showResult" class="btn btn-primary">Результат <i class="fa fa-angle-down"></i></a>
                                </td>
                            </tr>
                            <tr id="result" style="display: none;">
                                <td colspan="3">
                                    <div id="holidays">

                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <!-- /.box-body -->
                        <div class="box-footer" style="border: none;">
                            <input type="submit" value="Сохранить" class="btn btn-primary">
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </div>
        </div>
        <!-- /.tab-content -->
    </div>
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        if ($("input[name~='useCalendar']:checked").val() == 'off') {
            $("input[name~='link']").attr('disabled', 'disabled');
            $("#showResult").attr('disabled', 'disabled');
        } else {
            $("input[name~='link']").removeAttr('disabled');
            $("#showResult").removeAttr('disabled');
        }

        $("input[name~='useCalendar']").change(function () {
            use = $("input[name~='useCalendar']:checked").val();
            if (use == 'off') {
                $("input[name~='link']").attr('disabled', 'disabled');
                $("#showResult").attr('disabled', 'disabled');
                $("#result").css('display', 'none');
            } else {
                $("input[name~='link']").removeAttr('disabled');
                $("#showResult").removeAttr('disabled');
            }
        });

        $("#showResult").click(function () {
            use = $("input[name~='useCalendar']:checked").val();
            if (use == 'on') {
                $("#result").slideToggle(function () {
                    show = $("#result").css('display');
                    if (show != 'none') {
                        link = $("input[name~='link']").val();
                        content = '';

                        $.ajax({
                            url: '/admin/settings/get-work-calendar',
                            data: {'link': link},
                            cache: false,
                            beforeSend: function () {
                                $("#result pre").text('');
                                $("#result pre").text('Подождите, идет загрузка ...');
                            },
                            success: function (resp) {
                                mas = [];
                                holidays_ar = [];
                                days_ar = [];
                                day = {};
                                realArray = $.map(resp, function (n) {
                                    if (typeof n == 'object') {
                                        for (i=0;i<=Object.keys(n).length;i++) {
                                            if (typeof n[i] == 'object') {
                                                for (j=0;j<=Object.keys(n[i]).length;j++) {
                                                    if (typeof n[i][j] !== 'undefined') {
                                                        if (n[i][j].tag === 'HOLIDAY') {
                                                            day = {
                                                                'id': n[i][j].ID,
                                                                'tag': n[i][j].tag,
                                                                'title': n[i][j].TITLE
                                                            };
                                                            holidays_ar.push(day);
                                                        } else {
                                                            if (n[i][j].T == 1) {
                                                                date = n[i][j].D.split('.');
                                                                for (k=0;k<holidays_ar.length;k++) {
                                                                    if (n[i][j].H) {
                                                                        if (holidays_ar[k].id == n[i][j].H) {
                                                                            day = {
                                                                                'type': 'выходной день',
                                                                                'tag': n[i][j].tag,
                                                                                'date': date[1]+'.'+date[0],
                                                                                'holiday': holidays_ar[k].title
                                                                            };
                                                                            days_ar.push(day);
                                                                        }
                                                                        //break;
                                                                    } else {
                                                                        day = {
                                                                            'type': 'выходной день',
                                                                            'tag': n[i][j].tag,
                                                                            'date': date[1]+'.'+date[0]
                                                                        };
                                                                        days_ar.push(day);
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                            if (n[i][j].T == 2) {
                                                                day = {
                                                                    'type': 'рабочий и сокращенный день',
                                                                    'tag': n[i][j].tag,
                                                                    'date': date[1]+'.'+date[0]
                                                                };
                                                                days_ar.push(day);
                                                            }
                                                            if (n[i][j].T == 3) {
                                                                day = {
                                                                    'type': 'рабочий день',
                                                                    'tag': n[i][j].tag,
                                                                    'date': date[1]+'.'+date[0]
                                                                };
                                                                days_ar.push(day);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                });

                                //for (i=0;i<=resp)
                                content = '<table cellspacing="5" cellpadding="10" width="100%">' +
                                    '<tr>' +
                                    '<td>Год: </td>' +
                                    '<td>'+resp['YEAR']+'</td>' +
                                    '<td>Дата загрузки: </td>' +
                                    '<td>'+resp['DATE']+'</td>' +
                                    '</tr></table>';
                                content += '<table cellspacing="5" cellpadding="10" width="100%"><tr>' +
                                        '<td>Тип</td>' +
                                        '<td>Дата</td>' +
                                        '<td>Праздник</td>' +
                                        '</tr>';
                                for (i=0;i<days_ar.length;i++) {
                                    //console.log(days_ar[i].type);
                                    content += '<tr>' +
                                            '<td>'+days_ar[i].type+'</td>' +
                                            '<td>'+days_ar[i].date+'</td>';
                                    if (days_ar[i].holiday) {
                                        content += '<td>'+days_ar[i].holiday+'</td>';
                                    }
                                    content += '</tr>';
                                }
                                content += '</table>';
                                $("#result #holidays").html('');
                                $("#result #holidays").html(content);
                            }
                        });
                    }
                });
            }
        });

        $("#saveMainSettings").submit(function () {
            use = $("input[name~='useCalendar']:checked").val();
            if (use == 'on' && $("input[name~='link']").val() == '') {
                alert("Указите ссылку");
                return false;
            }
            return true;
        });
    });
</script>
@endpush