@extends('layouts.cabinet')

@section('title')
    Редактирование
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="box">
        <form action="">
            <table class="table">
                <tr>
                    <td style="padding-right: 20px;" width="33%">
                        <div class="form-group">
                            <label class="control-label" for="inputSurname">Фамилия</label>
                            <input type="text" class="form-control" name="surname" id="inputSurname" value="{{ $contact->last_name }}">
                        </div>
                    </td>
                    <td style="padding-right: 20px;" width="33%">
                        <div class="form-group">
                            <label class="control-label" for="inputName">Имя</label>
                            <input type="text" class="form-control" name="name" id="inputName" value="{{ $contact->first_name }}">
                        </div>
                    </td>
                    <td style="padding-right: 20px;" width="33%">
                        <div class="form-group">
                            <label class="control-label" for="inputPatronymic">Отчество</label>
                            <input type="text" class="form-control" name="patronymic" id="inputPatronymic" value="{{ $contact->patronymic }}">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding-right: 20px;" width="33%">
                        <div class="form-group">
                            <label class="control-label" for="inputPost">Должность</label>
                            <select name="" id="" class="form-control">
                                <option value="">1</option>
                                <option value="">2</option>
                            </select>
                        </div>
                    </td>
                    <td style="padding-right: 20px;" width="33%">
                        <div class="form-group">
                            <label class="control-label" for="inputPhone">Телефон</label>
                            <input type="text" class="form-control" name="phone" id="inputPhone" value="{{ $contact->phone }}">
                        </div>
                    </td>
                    <td style="padding-right: 20px;" width="33%">
                        <div class="form-group">
                            <label class="control-label" for="inputEmail">Email</label>
                            <input type="text" class="form-control" name="email" id="inputEmail" value="{{ $contact->email }}">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <div class="form-group">
                            <input type="submit" value="Сохранить" class="btn btn-primary">
                            <a href="/admin/projects" class="btn btn-default">Отмена</a>
                        </div>
                    </td>
                </tr>
            </table>
        </form>
    </div>
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection
