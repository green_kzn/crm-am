@extends('layouts.cabinet')

@section('title')
    Мероприятия
@endsection

@section('content')
    @if($event_perm['event_view'])
        @if(count($events) > 0)

        @else
            @if($event_perm['event_create'])
                <div style="padding-bottom: 20px;">
                    <a href="/admin/events/add" class="btn btn-primary">Добавить</a>
                </div>
            @endif
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Список мероприятий</h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody><tr>
                            <th>№</th>
                            <th>Название</th>
                            <th>Описание</th>
                            <th>Изображение</th>
                            <th>Теги</th>
                            <th>Период активности</th>
                        </tr>
                        <tr>
                            <td colspan="6" style="text-align: center;">Мероприятия отсутствуют</td>
                        </tr>
                        </tbody></table>
                </div>
                <!-- /.box-body -->
            </div>
        @endif
    @else
        <p>Доступ запрещен</p>
    @endif
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection