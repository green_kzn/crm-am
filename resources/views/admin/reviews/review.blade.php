@extends('layouts.cabinet')

@section('title')
    Отзыв {{ $client->name or $review->cid }}
@endsection

@section('content')
    <div class="box" style="padding: 10px;">
        <table width="100%" style="padding: 5px;">
            <tr>
                <td width="45%" style="padding: 5px;" colspan="2">
                    <div class="form-group">
                        <label>Название</label>
                        <input type="text" class="form-control" placeholder="{{ $client->name or $review->cid }}" disabled="">
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" placeholder="{{ $client->email or 'Отсутствует' }}" disabled="">
                    </div>
                </td>
            </tr>
            <tr>
                <td width="22%" style="padding: 5px;">
                    <div class="form-group">
                        <label>Оценка</label>
                        <input type="text" class="form-control" placeholder="{{ $review->mark }}" disabled="">
                    </div>
                </td>
                <td width="22%" style="padding: 5px;">
                    <div class="form-group">
                        <label>Скидка, %</label>
                        <input type="text" class="form-control" placeholder="{{ $review->discount_value }}" disabled="">
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <label>Купон</label>
                        <input type="text" class="form-control" placeholder="{{ $review->cupon }}" disabled="">
                    </div>
                </td>
            </tr>
            <tr>
                <td style="padding: 5px;" colspan="3">
                    <div class="form-group">
                        <label>Отзыв</label>
                        <textarea class="form-control" rows="3" placeholder="{{ $review->review }}" disabled=""></textarea>
                    </div>
                </td>
            </tr>
            <tr>
                <td width="45%" style="padding: 5px;" colspan="2">
                    <div class="form-group">
                        <label>Публиковать</label>
                        @if($review->allow_publicate == 1)
                            <input type="text" class="form-control" placeholder="Да" disabled="">
                        @else
                            <input type="text" class="form-control" placeholder="Нет" disabled="">
                        @endif

                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <label>Дата написания</label>
                        <input type="text" class="form-control" placeholder="{{ $review->created_at }}" disabled="">
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="form-group">
                        <a href="/admin_reviews/admin/reviews" class="btn btn-default">Назад</a>
                    </div>
                </td>
            </tr>
        </table>
    </div>
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection