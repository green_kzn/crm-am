@extends('layouts.cabinet')

@section('title')
    Отзывы
@endsection

@section('content')
    @if($event_perm['event_view'])
        @if(count($reviews) > 0)
            @if(\Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
                    {!! \Session::get('success') !!}
                </div>
            @endif
            @if(\Session::has('error'))
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                    {!! \Session::get('error') !!}
                </div>
            @endif
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Список отзывов</h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody><tr>
                            <th>№</th>
                            <th>Клиент</th>
                            <th>Оценка</th>
                            <th>Отзыв</th>
                            <th>Купон</th>
                            <th>Скидка</th>
                            <th>Публикация</th>
                            <th>Дата</th>
                            <th colspan="2">Действие</th>
                        </tr>
                        @foreach($reviews as $r)
                        <tr>
                            <td>{{ $loop->index+1 }}</td>
                            <td>{{ $r->client_name or $r->cid }}</td>
                            <td>{{ $r->mark }}</td>
                            <td>@php echo substr($r['review'], 0, 50).'...'; @endphp</td>
                            <td>{{ $r->cupon }}</td>
                            <td>{{ $r->discount_value }}</td>
                            @if($r->allow_publicate == 1)
                                <td>Да</td>
                            @else
                                <td>Нет</td>
                            @endif
                            <td>{{ $r->created_at }}</td>
                            <td style="vertical-align: middle;">
                                <a href="review/{{ $r->id }}" class="btn btn-primary">
                                    <i class="fa fa-search"></i>
                                </a>
                            </td>
                            <td style="vertical-align: middle;">
                                <a href="review/delete/{{ $r->id }}" class="btn btn-danger">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody></table>
                </div>
                <!-- /.box-body -->
            </div>
        @else
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Список отзывов</h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody><tr>
                            <th>№</th>
                            <th>Клиент</th>
                            <th>Оценка</th>
                            <th>Отзыв</th>
                            <th>Купон</th>
                            <th>Скидка</th>
                            <th>Дата</th>
                            <th colspan="3">Действие</th>
                        </tr>
                        <tr>
                            <td colspan="9" style="text-align: center;">Отзывы отсутствуют</td>
                        </tr>
                        </tbody></table>
                </div>
                <!-- /.box-body -->
            </div>
        @endif
    @else
        <p>Доступ запрещен</p>
    @endif
@endsection

@section('main-menu')
    @include('admin.main-menu')
@endsection