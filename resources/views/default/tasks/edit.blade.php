@extends('layouts.cabinet')

@section('title')
    Контакт: {{ $task->title }}
@endsection

@section('content')
    <div class="col-md-6">
        <div class="box" style="padding: 10px;">
            <table>
                <tr>
                    <td>Дата/время</td>
                    <td>{{ $task->start_date }}/{{ $task->start_time }}</td>
                </tr>
                <tr>
                    <td>Клиент</td>
                </tr>
                <tr>
                    <td>Контактное лицо</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box" style="padding: 10px;">
            История коментариев
        </div>
    </div>
    <div style="clear: both;"></div>
    <div style="padding-bottom: 20px;">
        <a id="showResult" class="btn btn-primary">Результат <i class="fa fa-angle-down"></i></a>
    </div>
    <div class="clear"></div>
    <div class="wall_form" id="popup_message_form" style="display:none; margin-bottom: 20px;">
        <div class="box">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#one" data-toggle="tab" aria-expanded="true">Работали</a></li>
                    <li class=""><a href="#two" data-toggle="tab" aria-expanded="false">Недозвон</a></li>
                    <li class=""><a href="#three" data-toggle="tab" aria-expanded="false">Перенос на другое время</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="one">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Задача</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <form role="form">
                                <div class="box-body">
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <th style="width: 10px">№</th>
                                            <th>Задача</th>
                                            <th>Статус</th>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td id="task"><span style="text-decoration: underline; cursor: pointer;">Нажмите для выбора</span></td>
                                            <td><span id="status" style="text-decoration: underline; cursor: pointer;">Нажмите для выбора</span></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </form>
                        </div>

                        <div style="padding-bottom: 20px;">
                            <a id="showOtherTask" class="btn btn-primary">Дополнительные задачи <i class="fa fa-angle-down"></i></a>
                        </div>

                        <div id="formOtherTask" class="box box-primary" style="display: none;">
                            <div class="box-header with-border">
                                <h3 class="box-title">Дополнительные задачи</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <form role="form">
                                <div class="box-body">
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <th style="width: 10px">№</th>
                                            <th>Задача</th>
                                            <th>Модуль</th>
                                            <th>Кол-во контактов</th>
                                            <th>Группа задач</th>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td id="task"><span style="text-decoration: underline; cursor: pointer;">Нажмите для выбора</span></td>
                                            <td><span id="status" style="text-decoration: underline; cursor: pointer;">Нажмите для выбора</span></td>
                                            <td id="task"><span style="text-decoration: underline; cursor: pointer;">Нажмите для выбора</span></td>
                                            <td><span id="status" style="text-decoration: underline; cursor: pointer;">Нажмите для выбора</span></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </form>
                        </div>

                        <div class="box box-solid">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="box-group" id="accordion">
                                    <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                                    <div class="panel box box-primary">
                                        <div class="box-header with-border">
                                            <h4 class="box-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed">
                                                    Назначен следующий контакт
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                            <div class="box-body">
                                                <div style="padding-bottom: 20px;">
                                                    <a href="" class="btn btn-primary">Завтра</a>
                                                    <a href="" class="btn btn-primary">Послезавтра</a>
                                                    <a href="" class="btn btn-primary">Пн. 11.09.2017</a>
                                                    <a href="" class="btn btn-primary">Вт. 12.09.2017</a>
                                                    <a href="" class="btn btn-primary">Другая</a>
                                                </div>
                                                <div style="padding-bottom: 20px;">
                                                    <table class="table table-bordered">
                                                        <tr>
                                                            <td style="vertical-align: middle;">Дата</td>
                                                            <td style="vertical-align: middle;"><input type="date"></td>
                                                            <td style="vertical-align: middle;">Время</td>
                                                            <td style="vertical-align: middle;"><input type="time"></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="vertical-align: middle;">Продолжительность</td>
                                                            <td style="vertical-align: middle;"><input type="time"></td>
                                                            <td colspan="2" style="vertical-align: middle;">
                                                                <div style="padding-bottom: 20px;">
                                                                    <a href="" class="btn btn-primary">30 мин.</a>
                                                                    <a href="" class="btn btn-primary">1 час</a>
                                                                    <a href="" class="btn btn-primary">1 час 30 мин.</a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="vertical-align: middle;">Контактное лицо</td>
                                                            <td style="vertical-align: middle;">
                                                                <select class="form-control">
                                                                    <option value="">Евгения</option>
                                                                </select>
                                                            </td>
                                                            <td style="vertical-align: middle;">Телефон</td>
                                                            <td colspan="2" style="vertical-align: middle;">
                                                                <select class="form-control">
                                                                    <option value="">89374321234</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel box box-danger">
                                        <div class="box-header with-border">
                                            <h4 class="box-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed" aria-expanded="false">
                                                    Договориться о следующем контакте позже
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label>* Комментарий (будет отправлен клиенту), минимум 70 символов</label>
                                                    <textarea class="form-control" rows="3"></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label>Дополнительный комментарий (это отправляться не будет)</label>
                                                    <textarea class="form-control" rows="3"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div style="padding-bottom: 20px;">
                                <a href="" class="btn btn-primary">Сохранить</a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="two">

                    </div>
                    <div class="tab-pane" id="three">

                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
        </div>
    </div>
@endsection

@section('main-menu')
    @include('default.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#showResult").click(function(){
                $("#popup_message_form").slideToggle("slow");
                $(this).toggleClass("active");
                $('#add').css('display', 'none');
                $('#addComment').css('display', 'block');
                $('#addComment > a').css('display', 'block');
                return false;
            });

            $("#showOtherTask").click(function(){
                $("#formOtherTask").slideToggle("slow");
                $(this).toggleClass("active");
                return false;
            });

            $('#task').click(function () {
                $(this).attr('id', 'new-task');
                $.ajax({
                    'url': '/defaultcab/group-tasks/get-all-tasks',
                    success: function (resp) {
                        alert("Ok");
                        content = '<select id="select-task" class="form-control">';
                        for (i=0; i<resp.length; i++) {
                            content += '<option>'+resp[i].name+'</option>';
                        }
                        content += '</select>';

                        $('#new-task').html(content);
                    }
                });
            });
        });

        $(document).click(function(event) {
            if ($(event.target).closest("#select-task").length) return;
            $("#select-task").hide("fast");
            //$("#new-task").html("<span style='text-decoration: underline; cursor: pointer;'>123</span>");
            event.stopPropagation();
        });
    </script>
@endpush