@extends('layouts.cabinet')

@section('title')
    Новая задача
@endsection

@section('content')
    <div class="box" style="padding: 15px;">
        <form action="/defaultcab/tasks/add" method="post">
            {{ csrf_field() }}
            <div class="form-group" id="NameGroup">
                <label class="control-label" for="inputName">Название</label>
                <input type="text" class="form-control" name="task_name" id="inputName">
            </div>
            <div class="form-group" id="NameGroup">
                <label class="control-label" for="inputName">Задача</label>
                <textarea name="task_text" id="inputText" cols="172" rows="10" style="resize: vertical;"></textarea>
            </div>
            <table id="tasks">
                <tr>
                    <td>
                        <div class="form-group" id="NameGroup">
                            <label class="control-label" for="inputName">Исполнитель</label>
                            <select class="form-control">
                                @foreach($users as $u)
                                    <option value="{{ $u->id }}">{{ $u->last_name }} {{ $u->first_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </td>
                    <td>
                        <div class="form-group" id="NameGroup">
                            <div class="input-group">
                                <label class="control-label" for="inputName">Дата/время окончания</label>
                                <input type="text" id="default_datetimepicker"/>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>

            <div class="form-group">
                <input type="submit" value="Сохранить" class="btn btn-success">
                <a href="/defaultcab/tasks" class="btn btn-default">Отмена</a>
            </div>
        </form>
    </div>
@endsection

@section('main-menu')
    @include('default.main-menu')
@endsection

@push('scripts')
    <script src="{{ asset('/public/js/jquery.datetimepicker.js') }}"></script>
    <script>
        $('#default_datetimepicker').datetimepicker({
            
        });
    </script>
@endpush