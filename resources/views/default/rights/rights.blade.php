@extends('layouts.cabinet')

@section('title')
    Права доступа
@endsection

@section('content')
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            @foreach($groups as $g)
                @if($loop->index+1 == 1)
                    <li class="active"><a href="#{{ $g->slug }}" data-toggle="tab">{{ $g->name }}</a></li>
                @else
                    <li><a href="#{{ $g->slug }}" data-toggle="tab">{{ $g->name }}</a></li>
                @endif
            @endforeach
        </ul>
        <div class="tab-content">
            @foreach($groups as $g)
                @if($loop->index+1 == 1)
                    <div class="tab-pane active" id="{{ $g->slug }}">
                        <table>
                            <tr>
                                <td colspan="2">Пользователи</td>
                            </tr>
                            @foreach($right[$g->slug] as $k => $r)
                                @if (strstr($k, 'user.'))
                                    <tr>
                                        <td>{{ $k }}</td>
                                        <td>{{ $r }}</td>
                                    </tr>
                                @endif
                            @endforeach
                            <tr>
                                <td colspan="2">Группы</td>
                            </tr>
                            @foreach($right[$g->slug] as $k => $r)
                                @if (strstr($k, 'group.'))
                                    <tr>
                                        <td>{{ $k }}</td>
                                        <td>{{ $r }}</td>
                                    </tr>
                                @endif
                            @endforeach
                            <tr>
                                <td colspan="2">Календарь</td>
                            </tr>
                            @foreach($right[$g->slug] as $k => $r)
                                @if (strstr($k, 'calendar.'))
                                    <tr>
                                        <td>{{ $k }}</td>
                                        <td>{{ $r }}</td>
                                    </tr>
                                @endif
                            @endforeach
                        </table>
                    </div>
                @else
                    <div class="tab-pane" id="{{ $g->slug }}">
                        <table>
                            @foreach($right[$g->slug] as $k => $r)
                                <tr>
                                    <td>{{ $k }}</td>
                                    <td>{{ $r }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                @endif
            @endforeach
        </div>
        <!-- /.tab-content -->
    </div>
@endsection

@section('main-menu')
    @include('default.main-menu')
@endsection

@push('scripts')

@endpush