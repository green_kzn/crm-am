@extends('layouts.cabinet')

@section('title')
    Календарь
@endsection

@section('content')
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div style="padding-bottom: 20px;">
        <a href="/defaultcab/events/add" class="btn btn-primary">Добавить событие</a>
    </div>
    <div class="row">
        <!--<div class="col-md-3">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h4 class="box-title">События</h4>
                </div>
                <div class="box-body">

                    @if (count($events) > 0)
                        @foreach($events as $event)
                            <div class="external-event {{ $event->color }} ui-draggable ui-draggable-handle" style="position: relative;">
                                {{ $event->name }}
                                <div class="pull-right del-event">
                                    <a href="/defaultcab/calendar/del_event/{{ $event->id }}">
                                        <i class="fa fa-close"></i>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                        <div class="checkbox">
                            <label for="drop-remove">
                                <input type="checkbox" id="drop-remove">
                                удалить после перемещения в календарь
                            </label>
                        </div>
                    @else
                        Событий нет
                    @endif
                </div>

            </div>

            <form action="/defaultcab/calendar/add_event" method="post">
                {{ csrf_field() }}
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Добавить событие</h3>
                    </div>
                    <div class="box-body">
                        <div class="input-group">
                            <div class="form-group">
                                <label>Date:</label>

                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="datepicker">
                                </div>

                            </div>
                            <p><label for="event_type">Тип события</label><br>
                                <input type="text" id="event_type" name="nameEvent" value=""></p>
                            <p><label for="event_start">Начало</label><br>
                                <input type="text" name="event_start" id="event_start"/>
                            </p>
                            <p><label for="event_end">Конец</label><br>
                                <input type="text" name="event_end" id="event_end"/></p>
                            <input type="hidden" name="event_id" id="event_id" value="">
                            <input type="hidden" name="color" value="">
                        </div>
                        <div class="btn-group" style="width: 100%; margin-bottom: 10px;">

                            <ul class="fc-color-picker" name="color-chooser">
                                <li data="bg-aqua"><a class="text-aqua" href="#"><i class="fa fa-square"></i></a></li>
                                <li data="bg-blue"><a class="text-blue" href="#"><i class="fa fa-square"></i></a></li>
                                <li data="bg-light-blue"><a class="text-light-blue" href="#"><i class="fa fa-square"></i></a></li>
                                <li data="bg-teal"><a class="text-teal" href="#"><i class="fa fa-square"></i></a></li>
                                <li data="bg-yellow"><a class="text-yellow" href="#"><i class="fa fa-square"></i></a></li>
                                <li data="bg-orange"><a class="text-orange" href="#"><i class="fa fa-square"></i></a></li>
                                <li data="bg-green"><a class="text-green" href="#"><i class="fa fa-square"></i></a></li>
                                <li data="bg-lime"><a class="text-lime" href="#"><i class="fa fa-square"></i></a></li>
                                <li data="bg-red"><a class="text-red" href="#"><i class="fa fa-square"></i></a></li>
                                <li data="bg-purple"><a class="text-purple" href="#"><i class="fa fa-square"></i></a></li>
                                <li data="bg-fuchsia"><a class="text-fuchsia" href="#"><i class="fa fa-square"></i></a></li>
                                <li data="bg-muted"><a class="text-muted" href="#"><i class="fa fa-square"></i></a></li>
                                <li data="bg-navy"><a class="text-navy" href="#"><i class="fa fa-square"></i></a></li>
                            </ul>
                        </div>



                            <div class="input-group-btn">
                                <input id="add-new-event" type="submit" class="btn btn-primary btn-flat">Добавить</input>
                            </div>



                    </div>
                </div>
            </form>
            <button id="add_event_button">Добавить событие</button>
            <div id="dialog-form" title="Событие">
                <p class="validateTips"></p>
                <form>
                    <p><label for="event_type">Тип события</label>
                        <input type="text" id="event_type" name="event_type" value=""></p>
                    <p><label for="event_start">Начало</label>
                        <input type="text" name="event_start" id="event_start"/></p>
                    <p><label for="event_end">Конец</label>
                        <input type="text" name="event_end" id="event_end"/></p>
                    <input type="hidden" name="event_id" id="event_id" value="">
                </form>
            </div>
        </div> -->
        <!-- /.col -->
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body no-padding">
                    <!-- THE CALENDAR -->
                    <div id='calendar'></div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /. box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@endsection

@section('main-menu')
    @include('default.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            /* глобальные переменные */
            var event_start = $('#event_start');
            var event_end = $('#event_end');
            var event_type = $('#event_type');
            var calendar = $('#calendar');
            var form = $('#dialog-form');
            var event_id = $('#event_id');
            var format = "MM/dd/yyyy HH:mm";
            /* кнопка добавления события */
            $('#add_event_button').button().click(function(){
                formOpen('add');
            });
            /** функция очистки формы */
            function emptyForm() {
                event_start.val("");
                event_end.val("");
                event_type.val("");
                event_id.val("");
            }
            /* режимы открытия формы */
            function formOpen(mode) {
                if(mode == 'add') {
                    /* скрываем кнопки Удалить, Изменить и отображаем Добавить*/
                    $('#add').show();
                    $('#edit').hide();
                    $("#delete").button("option", "disabled", true);
                }
                else if(mode == 'edit') {
                    /* скрываем кнопку Добавить, отображаем Изменить и Удалить*/
                    $('#edit').show();
                    $('#add').hide();
                    $("#delete").button("option", "disabled", false);
                }
                form.dialog('open');
            }
            /* инициализируем Datetimepicker */
            event_start.datetimepicker({hourGrid: 4, minuteGrid: 10, dateFormat: 'mm/dd/yy'});
            event_end.datetimepicker({hourGrid: 4, minuteGrid: 10, dateFormat: 'mm/dd/yy'});
            /* инициализируем FullCalendar */
            calendar.fullCalendar({
                firstDay: 1,
                editable: true,
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                monthNamesShort: ['Янв.','Фев.','Март','Апр.','Май','Июнь','Июль','Авг.','Сент.','Окт.','Ноя.','Дек.'],
                dayNames: ["Воскресенье","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота"],
                dayNamesShort: ["ВС","ПН","ВТ","СР","ЧТ","ПТ","СБ"],
                buttonText: {
                    today: "Сегодня",
                    month: "Месяц",
                    week: "Неделя",
                    day: "День"
                },
                /* формат времени выводимый перед названием события*/
                timeFormat: 'H:mm',
                /* обработчик события клика по определенному дню */
                dayClick: function(date, allDay, jsEvent, view) {
                    var newDate = $.fullCalendar.formatDate(date, format);
                    event_start.val(newDate);
                    event_end.val(newDate);
                    formOpen('add');
                },
                /* обработчик кликак по событияю */
                eventClick: function(calEvent, jsEvent, view) {
                    event_id.val(calEvent.id);
                    event_type.val(calEvent.title);
                    event_start.val($.fullCalendar.formatDate(calEvent.start, format));
                    event_end.val($.fullCalendar.formatDate(calEvent.end, format));
                    formOpen('edit');
                },
                /* источник записей */
                eventSources: [{
                    url: '/defaultcab/tasks/gettasks/',
                    data: {
                        op: 'source'
                    },
                    error: function() {
                        alert('Ошибка соединения с источником данных!');
                    }
                }]
            });
            /* обработчик формы добавления */
            form.dialog({
                autoOpen: false,
                buttons: [{
                    id: 'add',
                    text: 'Добавить',
                    click: function() {
                        $.ajax({
                            type: "POST",
                            url: '/defaultcab/tasks/gettasks',
                            data: {
                                start: event_start.val(),
                                end: event_end.val(),
                                type: event_type.val(),
                                op: 'add'
                            },
                            success: function(id){
                                calendar.fullCalendar('renderEvent', {
                                    id: id,
                                    title: event_type.val(),
                                    start: event_start.val(),
                                    end: event_end.val(),
                                    allDay: false
                                });

                            }
                        });
                        emptyForm();
                    }
                },
                    {   id: 'edit',
                        text: 'Изменить',
                        click: function() {
                            $.ajax({
                                type: "POST",
                                url: '/defaultcab/tasks/gettasks',
                                data: {
                                    id: event_id.val(),
                                    start: event_start.val(),
                                    end: event_end.val(),
                                    type: event_type.val(),
                                    op: 'edit'
                                },
                                success: function(id){
                                    calendar.fullCalendar('refetchEvents');

                                }
                            });
                            $(this).dialog('close');
                            emptyForm();
                        }
                    },
                    {   id: 'cancel',
                        text: 'Отмена',
                        click: function() {
                            $(this).dialog('close');
                            emptyForm();
                        }
                    },
                    {   id: 'delete',
                        text: 'Удалить',
                        click: function() {
                            $.ajax({
                                type: "POST",
                                url: '/defaultcab/tasks/gettasks',
                                data: {
                                    id: event_id.val(),
                                    op: 'delete'
                                },
                                success: function(id){
                                    calendar.fullCalendar('removeEvents', id);
                                }
                            });
                            $(this).dialog('close');
                            emptyForm();
                        },
                        disabled: true
                    }]
            });
        });
    </script>
@endpush