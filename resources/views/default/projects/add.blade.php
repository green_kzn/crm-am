@extends('layouts.cabinet')

@section('title')
    Мои проекты: Добавление нового проекта
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="box" style="padding: 10px;">
        <form action="/defaultcab/projects/add" method="post" id="formAddProject">
            {{ csrf_field() }}
            <input type="hidden" name="contacts" id="contacts">
            <input type="hidden" name="row_modules" id="row_modules">
            <div class="form-group">
                <label for="projectName">Название проекта</label>
                <input type="text" class="form-control" name="projectName">
            </div>
            <table class="table" style="width: 55%">
                <tbody>
                    <tr>
                        <td style="vertical-align: middle;"><label>Дата начала</label></td>
                        <td style="vertical-align: middle;"><input type="date" name="startDate" class="form-control"></td>
                        <td style="vertical-align: middle;"><label>Дата окончания</label></td>
                        <td style="vertical-align: middle;"><input type="date" name="finishDate" class="form-control"></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="vertical-align: middle;"><label>Ответственный внедренец</label></td>
                        <td colspan="2" style="vertical-align: middle;">
                            <select name="user" class="form-control">
                                @foreach($users as $u)
                                    <option value="{{ $u->id }}">{{ $u->last_name }} {{ $u->first_name }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="vertical-align: middle;"><label>Город</label></td>
                        <td colspan="2" style="vertical-align: middle;">
                            <select name="city" class="form-control">
                                <option value="Казань">Казань</option>
                                <option value="Пенза">Пенза</option>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Контакты</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table table-striped" id="tableContacts">
                        <tbody><tr>
                            <th>Имя</th>
                            <th>Фамилия</th>
                            <th>Отчество</th>
                            <th>Должность</th>
                            <th>Телефон</th>
                            <th>E-mail</th>
                            <th>Ответственный за внедрение</th>
                            <th colspan="2">Действия</th>
                        </tr>
                        <tr id="contact_row"></tr>
                        <tr>
                            <td><input id="contact_name" type="text" class="form-control"></td>
                            <td><input id="contact_surname" type="text" class="form-control"></td>
                            <td><input id="contact_patronymic" type="text" class="form-control"></td>
                            <td>
                                <select class="form-control" id="contact_post">
                                    @foreach($posts as $p)
                                        <option id="{{ $p->id }}" value="{{ $p->name }}">{{ $p->name }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td><input id="contact_phone" type="text" class="form-control"></td>
                            <td><input id="contact_email" type="text" class="form-control"></td>
                            <td></td>
                            <td>
                                <a class="btn btn-primary" id="saveContact">
                                    <i class="fa fa-check"></i>
                                </a>
                            </td>
                        </tr></tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>

            <div class="box">
                <table>
                    <tr>
                        <td>
                            <div class="box-header">
                                <h3 class="box-title">
                                    Модули
                                </h3>
                            </div>
                        </td>
                        <td>
                            <select class="form-control" id="modules">
                                @foreach($modules as $m)
                                    <option id="{{ $m->id }}" value="{{ $m->name }}">{{ $m->name }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td style="padding-left: 10px;">
                            <input type="number" min="0" id="modules_num" placeholder="Кол-во" class="form-control">
                        </td>
                        <td style="padding-left: 10px;">
                            <a id="addModule" class="btn btn-primary">Добавить</a>
                        </td>
                    </tr>
                </table>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table table-striped" style="width: 50%" id="tableModule">
                        <tbody><tr>
                            <th>Название</th>
                            <th>Кол-во модулей</th>
                            <th>Кол-во контактов</th>
                            <th>с учетом запаса</th>
                            <th>Кол-во форм/отчетов</th>
                            <th>Действия</th>
                        </tr>
                        <tr id="modules_list"></tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>

            <div class="form-group">
                <input type="submit" value="Сохранить" class="btn btn-success">
                <a href="/defaultcab/projects" class="btn btn-default">Отмена</a>
            </div>
        </form>
    </div>
@endsection

@section('main-menu')
    @include('default.main-menu')
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $("#saveContact").click(function () {
                newContact();
            });

            $("#addModule").click(function () {
                num = $("#modules_num").val();
                id = $('#modules option:selected').attr('id');
                i = 0;

                if (!num) {
                    num = 1;
                }

                $.ajax({
                    url: '/defaultcab/module-rule/getRule',
                    method: 'POST',
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {'id': id},
                    success: function (resp) {
                        i = $(".row_index").length;
                        $("#modules_list").before('<tr class="row_index" id='+(i+1)+'>' +
                            '<td style="display: none;"><span class="module_id">'+$("#modules option:checked").attr('id')+'</span></td>' +
                            '<td>'+$("#modules").val()+'</td>' +
                            '<td><span class="module_num">'+num+'</span></td>' +
                            '<td>'+resp.norm_contacts+'</td>' +
                            '<td>'+resp.number_contacts+'</td>' +
                            '<td>4</td>' +
                            '<td><a href="">Уточнить нормы</a></td>' +
                            '</tr>');
                        i++;
                    },
                    error: function () {
                        alert("Ошибка заполнения справочника");
                    }
                });
            });
        });

        function delContact(id) {
            $('#'+id).remove();
        }

        function newContact() {
            name = $("#contact_name").val();
            surname = $("#contact_surname").val();
            patronymic = $("#contact_patronymic").val();
            post = $("#contact_post").val();
            phone = $("#contact_phone").val();
            email = $("#contact_email").val();
            er = 0;
            i = $("#tableContacts tr").length-2;

            if (!surname) {
                surname = '';
            }

            if (!patronymic) {
                patronymic = '';
            }

            if (!email) {
                email = '';
            }

            if (!name) {
                er++;
                alert('Введите имя контакта');
            }

            if (!post) {
                er++;
                alert('Введите должность контакта');
            }

            if (!phone) {
                er++;
                alert('Введите номер телефона контакта');
            }

            if (er == 0) {
                $("#contact_row").before('<tr id="'+i+'">' +
                        '<td><span name="contacts_name">'+name+'</span></td>' +
                        '<td><span name="contacts_surname">'+surname+'</span></td>' +
                        '<td><span name="contacts_patronymic">'+patronymic+'</span></td>' +
                        '<td><span name="contacts_post">'+post+'</span></td>' +
                        '<td><span name="contacts_phone">'+phone+'</span></td>' +
                        '<td><span name="contacts_email">'+email+'</span></td>' +
                        '<td><input type="radio" name="main"></td>' +
                        '<td><a class="btn btn-danger" onclick="delContact('+i+')"><i class="fa fa-trash"></i></a></td>' +
                    '</tr>');

                i++;

                $("#contact_name").val('');
                $("#contact_surname").val('');
                $("#contact_patronymic").val('');
                $("#contact_post").val('');
                $("#contact_phone").val('');
                $("#contact_email").val('');
            }
        }

        $("#contact_name").mousedown(function () {
            ///$(this).attr('id', 'new-name');
            $("#contact_name").html('<input type="text">');
        });

        /*$(document).click(function(event) {
            if ($(event.target).closest("#new-name").length) return;
            $("#new-name").hide("fast");
            //$("#new-task").html("<span style='text-decoration: underline; cursor: pointer;'>123</span>");
            event.stopPropagation();
        });*/

        $("#formAddProject").submit(function () {
            rowContacts = $("#tableContacts tr").length-3;
            rowModules = $("#tableModule tr").length-2;
            row = [];
            result = [];
            module = [];

            for (i=1; i<=rowContacts; i++) {
                row[i] = {'name': $('#'+i+' span[name~="contacts_name"]').text(),
                    'surname': $('#'+i+' span[name~="contacts_surname"]').text(),
                    'contacts_patronymic': $('#'+i+' span[name~="contacts_patronymic"]').text(),
                    'contacts_post': $('#'+i+' span[name~="contacts_post"]').text(),
                    'contacts_phone': $('#'+i+' span[name~="contacts_phone"]').text(),
                    'contacts_email': $('#'+i+' span[name~="contacts_email"]').text(),
                    'main': $('#'+i+' input[name~="main"]').prop('checked')
                };
            }

            for (var j = 0; j <= row.length; j++) {
                if (row[j] != null && row[j] != 'underfinded' && row[j] != '') {
                    result.push(row[j]);
                }
            }

            for (k=1; k<=rowModules; k++) {
                m = $("#tableModule #"+k+" .module_id").text();
                num = $("#tableModule #"+k+" .module_num").text();

                module[k] = {
                    'id': m,
                    'num': num
                };
            }

            $("#row_modules").val(JSON.stringify(module));

            $("#contacts").val(JSON.stringify(result));
            if ($("#contacts").val() == '') {
                alert('Error');
                return false;
            } else {
                return true;
            }
            return false;
        });
    </script>
@endpush