@extends('layouts.cabinet')

@section('title')
    Проект: {{ $project->name }}
@endsection

@section('content')
    <input type="hidden" value="{{ $project->id }}" id="project_id">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#history" data-toggle="tab" aria-expanded="true">История внедрения</a></li>
            <li class=""><a href="#contacts" data-toggle="tab" aria-expanded="false">Контакты</a></li>
            <li class=""><a href="#tasks" data-toggle="tab" aria-expanded="false">Список задач</a></li>
            <li class=""><a href="#card" data-toggle="tab" aria-expanded="false">Карточка проекта</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="history">
                <div style="padding-bottom: 20px; display: block;" id="add">
                    <a href="" class="btn btn-primary" id="click_mes_form">Добавить новый коментарий</a>
                </div>
                <div style="padding-bottom: 20px; display: none;" id="addComment">
                    <a class="btn btn-primary" id="addCommentButton" style="display: none; float: left; margin-right: 10px;">Добавить</a>&nbsp;
                    <a href="" class="btn btn-primary" style="display: none; float: left;">Отмена</a>
                </div>
                <div class="clear"></div>
                <div class="wall_form" id="popup_message_form" style="display:none; margin-bottom: 20px;">
                    <div id="editor"></div>
                </div>
                @if(\Session::has('success'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
                        {!! \Session::get('success') !!}
                    </div>
                @endif
                @if(\Session::has('error'))
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                        {!! \Session::get('error') !!}
                    </div>
                @endif
                @if($comments)
                    @foreach($comments as $c)
                        <div class="post">
                            <div class="user-block">
                                <img class="img-circle img-bordered-sm" src="{{ asset('public/avatars/'.$c['user_foto']) }}" alt="user image">
                                <span class="username">
                                    <a href="#">{{ $c['user_name'] }} {{ $c['user_surname'] }}</a>
                                    @if($c['autor'])
                                        <a onclick="editComment({{ $c['id'] }}, '{{ $c['text'] }}')" class="pull-right btn-box-tool"><i class="fa fa-pencil"></i></a>
                                        <a href="/defaultcab/comment/del/{{ $c['id'] }}" class="pull-right btn-box-tool"><i class="fa fa-trash"></i></a>
                                    @endif
                                </span>
                                <span class="description">Опубликовано - {{ $c['time'] }} {{ $c['date'] }}</span>
                            </div>
                            <!-- /.user-block -->
                            <div id="comment_{{ $c['id'] }}">
                                {{ $c['text'] }}
                            </div>
                            <ul class="list-inline">
                                <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Share</a></li>
                            </ul>

                            <input class="form-control input-sm" type="text" placeholder="Type a comment">
                        </div>
                    @endforeach
                @else
                    <p style="text-align: center">Коментарии отсутствуют</p>
                @endif
            </div>
            <div class="tab-pane" id="contacts">
                <div style="padding-bottom: 20px; display: block;" id="addCont">
                    <a href="" class="btn btn-primary" id="click_contact_add">Добавить контакт</a>
                </div>
                <div style="padding-bottom: 20px; display: none;" id="addContact">
                    <a class="btn btn-primary" id="addContactButton" style="display: none; float: left; margin-right: 10px;">Добавить</a>&nbsp;
                    <a onclick="closeContactForm()" class="btn btn-primary" style="display: none; float: left;">Отмена</a>
                </div>
                <div class="clear"></div>
                <div class="wall_form" id="popup_contact_form" style="display:none; margin-bottom: 20px;">
                    <table>
                        <tr>
                            <td>
                                <div class="form-group" id="Name" style="margin-right: 20px;">
                                    <label class="control-label" for="inputName" style="display: none;"></label>
                                    <input type="text" class="form-control" name="contact_name" id="inputName" placeholder="Имя">
                                </div>
                            </td>
                            <td>
                                <div class="form-group" id="LastName" style="margin-right: 20px;">
                                    <label class="control-label" for="inputLastName" style="display: none;"></label>
                                    <input type="text" class="form-control" name="contact_surname" id="inputLastName" placeholder="Фамилия">
                                </div>
                            </td>
                            <td>
                                <div class="form-group" id="Patronymic" style="margin-right: 20px;">
                                    <label class="control-label" for="inputPatronymic" style="display: none;"></label>
                                    <input type="text" class="form-control" name="contact_patronymic" id="inputPatronymic" placeholder="Отчество">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group" id="Post" style="margin-right: 20px;">
                                    <label class="control-label" for="inputPost" style="display: none;"></label>
                                    <input type="text" class="form-control" name="contact_post" id="inputPost" placeholder="Должность">
                                </div>
                            </td>
                            <td>
                                <div class="form-group" id="Phone" style="margin-right: 20px;">
                                    <label class="control-label" for="inputPhone" style="display: none;"></label>
                                    <input type="tel" class="form-control" name="contact_phone" id="inputPhone" placeholder="Телефон">
                                </div>
                            </td>
                            <td>
                                <div class="form-group" id="Email" style="margin-right: 20px;">
                                    <label class="control-label" for="inputEmail" style="display: none;"></label>
                                    <input type="email" class="form-control" name="contact_email" id="inputEmail" placeholder="E-mail">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                @if(\Session::has('success'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
                        {!! \Session::get('success') !!}
                    </div>
                @endif
                @if(\Session::has('error'))
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                        {!! \Session::get('error') !!}
                    </div>
                @endif
                @if(count($contacts) > 0)
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody><tr>
                                <th>№</th>
                                <th>Фамилия</th>
                                <th>Имя</th>
                                <th>Отчество</th>
                                <th>Должность</th>
                                <th>Телефон</th>
                                <th>Email</th>
                                <th>Действия</th>
                            </tr>
                            @foreach($contacts as $c)
                                <tr>
                                    <td style="vertical-align: middle;">{{ $loop->index+1 }}</td>
                                    <td style="vertical-align: middle;">{{ $c['last_name'] }}</td>
                                    <td style="vertical-align: middle;">{{ $c['first_name'] }}</td>
                                    <td style="vertical-align: middle;">{{ $c['patronymic'] }}</td>
                                    <td style="vertical-align: middle;">{{ $c['post'] }}</td>
                                    <td style="vertical-align: middle;"><a href="tel:{{ $c['phone'] }}">{{ $c['phone'] }}</a></td>
                                    <td style="vertical-align: middle;"><a href="mailto:{{ $c['email'] }}">{{ $c['email'] }}</a></td>
                                    <td style="vertical-align: middle;">
                                        <a href="/defaultcab/contact/del/{{ $c['id'] }}" class="btn btn-danger">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                @else
                    <p style="text-align: center">Контакты отсутствуют</p>
                @endif
            </div>
            <div class="tab-pane" id="tasks">
                <a href="" class="btn btn-primary">Все</a>
                <a href="" class="btn btn-primary">Основные работы</a>
                <a href="" class="btn btn-primary">Дополнительные работы</a>

                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Модуль</th>
                                <th>Выполнено</th>
                                <th>Наименование задачи</th>
                                <th>Группа работ</th>
                                <th>Дополнительная работа</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($project_task as $pt)
                                <tr>
                                    <td>{{ $pt['m_name'] }}</td>
                                    @if($pt['status'] == 1)
                                        <td><i class="fa fa-square-o"></i></td>
                                    @else
                                        <td><i class="fa fa-square"></i></td>
                                    @endif
                                    <td>{{ $pt['name'] }}</td>
                                    <td>{{ $pt['g_task'] }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <h4>ИТОГО контактов/форм по группам</h4>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th></th>
                            <th>ФАКТ</th>
                            <th>По договору</th>
                            <th>Осталось</th>
                            <th>Запас</th>
                            <th>Осталось с учетом запаса</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Контакты</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Печатные формы/отчеты</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane" id="card">
                    <div class="box-body table-responsive no-padding">
                        <table>
                            @if(count($contacts) > 0)
                                @foreach($contacts as $c)
                                    @if($c['director'])
                                    <tr>
                                        <td style="padding-right: 10px;"><span class="bold">{{ $c['post'] }}: </span></td>
                                        <td>{{ $c['last_name'] }} {{ $c['first_name'] }} {{ $c['patronymic'] }} Тел. {{ $c['phone'] }}</td>
                                    </tr>
                                    @endif
                                    @if($c['main'])
                                    <tr>
                                        <td style="padding-right: 10px;"><span class="bold">Ответственный за внедрение: </span></td>
                                        <td>{{ $c['last_name'] }} {{ $c['first_name'] }} {{ $c['patronymic'] }} Тел. {{ $c['phone'] }}</td>
                                    </tr>
                                    @endif
                                @endforeach
                            @endif
                            @if(count($modules) > 0)
                            <tr>
                                <td colspan="2"><span class="bold">Купленные модули</span></td>
                            </tr>
                            @foreach($modules as $m)
                                <tr>
                                    <td>{{ $m['name'] }}</td>
                                    <td>{{ $m['num'] }}</td>
                                </tr>
                            @endforeach
                            @endif
                        </table>
                        <table>
                            <tr>
                                <td style="padding-right: 10px;"><span class="bold">Дата начала внедрения</span></td>
                                <td style="padding-right: 20px;">{{ $project->start_date }}</td>
                                <td style="padding-right: 10px;"><span class="bold">Запланированная дата окончания</span></td>
                                <td>{{ $project->finish_date }}</td>
                            </tr>
                        </table>
                    </div>

            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
    </div>
@endsection

@section('main-menu')
    @include('default.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $("#click_mes_form").click(function(){
                $("#popup_message_form").slideToggle("slow");
                $(this).toggleClass("active");
                $('#add').css('display', 'none');
                $('#addComment').css('display', 'block');
                $('#addComment > a').css('display', 'block');
                return false;
            });

            $("#click_contact_add").click(function(){
                $("#popup_contact_form").slideToggle("slow");
                $(this).toggleClass("active");
                $('#addCont').css('display', 'none');
                $('#addContact').css('display', 'block');
                $('#addContact > a').css('display', 'block');
                return false;
            });

            $(function () {
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('editor')
                //bootstrap WYSIHTML5 - text editor
                $('.textarea').wysihtml5()
            });

            $('#addCommentButton').click(function () {
                comment = CKEDITOR.instances.editor.getData();
                p_id = $('#project_id').val();
                if (comment) {
                    $.ajax({
                        'url': '/defaultcab/comments/add',
                        'data': {'text': comment, 'project_id': p_id},
                        success: function (resp) {
                            location.reload();
                        }
                    });
                } else {
                    alert('Коментарий не может быть пустым');
                }
            });

            $('#addContactButton').click(function () {
                name = $('#inputName').val();
                last_name = $('#inputLastName').val();
                patronymic = $('#inputPatronymic').val();
                post = $('#inputPost').val();
                phone = $('#inputPhone').val();
                email = $('#inputEmail').val();
                p_id = $('#project_id').val();

                if (name) {
                    $.ajax({
                        'url': '/defaultcab/contact/add',
                        'data': {'name': name, 'last_name': last_name, 'patronymic': patronymic, 'post': post, 'phone': phone, 'email': email, 'p_id': p_id},
                        success: function (resp) {
                            location.reload();
                        }
                    });
                } else {
                    alert('Укажите имя контакта');
                }
            });
        });

        function editComment(id, comment) {
            $('#comment_'+id).html('<textarea id="textComment" cols="172" rows="10" style="resize: vertical;">'+comment+'</textarea>');
            $('#textComment').setFocus();
        }
        
        function closeContactForm() {
            $("#popup_contact_form").slideToggle("slow");
            $(this).toggleClass("active");
            $('#addCont').css('display', 'block');
            $('#addContact').css('display', 'none');
            $('#addContact > a').css('display', 'none');
            return false;
        }
    </script>
@endpush