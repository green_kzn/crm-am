@extends('layouts.cabinet')

@section('title')
    Мои проекты
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Проекты</a></li>
            <li class=""><a href="#timeline" data-toggle="tab" aria-expanded="false">Задачи по проектам</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="activity">

                <div class="box">
                    <div class="box-header">
                        <a href="" style="padding-right: 15px;">Спящие</a>
                        <a href="" style="padding-right: 15px;">Активные</a>
                        <a href="" style="padding-right: 15px;">Сегодня</a>
                        <a href="" style="padding-right: 15px;">Горящие</a>

                        <div class="box-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                        @if(session('perm')['project.create'])
                            <a href="/defaultcab/projects/add" class="btn btn-primary">Добавить проект</a>
                        @endif
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody>
                                <tr>
                                    <th>Предыдущий контакт</th>
                                    <th>Следующий контакт</th>
                                    <th>Название</th>
                                    <th>Город</th>
                                    <th>Готовность</th>
                                    <th>Активность</th>
                                    <th>Дата начала</th>
                                    <th>Планируемая дата окончания</th>
                                    <th>Действия</th>
                                </tr>
                                @if(count($projects) > 0)
                                    @foreach($projects as $p)
                                    <tr>
                                        <td style="vertical-align: middle;">{{ $p->prev_contact }}</td>
                                        <td style="vertical-align: middle;">{{ $p->next_contact }}</td>
                                        <td style="vertical-align: middle;">{{ $p->name }}</td>
                                        <td style="vertical-align: middle;">{{ $p->city }}</td>
                                        <td style="vertical-align: middle;">{{ $p->done }} %</td>
                                        <td style="vertical-align: middle;">{{ $p->active }} %</td>
                                        <td style="vertical-align: middle;">{{ $p->start_date }}</td>
                                        <td style="vertical-align: middle;">{{ $p->finish_date }}</td>
                                        <td style="vertical-align: middle;">
                                            @if(session('perm')['project.update'])
                                                <a href="project/edit/{{ $p->id }}" class="btn btn-primary">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            @else
                                                <a href="javascript: void(0)" class="btn btn-primary" disabled="true">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="9" style="text-align: center;">Проекты не найдены</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            <!-- /.tab-pane -->
            </div>
            <div class="tab-pane" id="timeline">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th>Предыдущий контакт</th>
                            <th>Следующий контакт</th>
                            <th>Название задачи</th>
                            <th>Наименование проекта</th>
                            <th>Город</th>
                            <th>Действия</th>
                        </tr>
                        @if($project_task[0])
                            @foreach($project_task as $pt)
                                <tr>
                                    <td style="vertical-align: middle;"></td>
                                    <td style="vertical-align: middle;"></td>
                                    <td style="vertical-align: middle;">{{ $pt['name'] }}</td>
                                    <td style="vertical-align: middle;">{{ $pt['p_name'] }}</td>
                                    <td style="vertical-align: middle;">{{ $pt['city'] }}</td>
                                    <td style="vertical-align: middle;">
                                        <a href="project/edit/{{ $p->id }}" class="btn btn-primary">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6" style="text-align: center;">Задачи не найдены</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
    </div>
@endsection

@section('main-menu')
    @include('default.main-menu')
@endsection

@push('scripts')

@endpush