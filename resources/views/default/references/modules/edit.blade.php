@extends('layouts.cabinet')

@section('title')
    Справочник "Модули": Редактирование модуля
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="box" style="padding: 10px;">
        <form action="/defaultcab/modules/edit/{{ $module->id }}" method="post" id="formModuleEdit">
            {{ csrf_field() }}
            <input type="hidden" id="row" name="row">
            <div class="form-group" id="NameTask">
                <label class="control-label" for="inputNameModule">Название модуля</label>
                <input type="text" class="form-control" name="name_module" value="{{ $module->name }}" id="inputNameModule" style="width: 300px;">
            </div>

            <table class="table table-striped" id="tableTask">
                <tbody><tr>
                    <th>Список задач</th>
                    <th>Норма кол-ва контактов по договору 1 контакт - 1 час</th>
                    <th>Кол - во разрешенных контактов</th>
                    <th>Группа задач</th>
                    <th>Вопросы для уточнения</th>
                    <th colspan="2">Действия</th>
                </tr>
                @if($module_rule)
                    @foreach($module_rule as $mr)
                        <tr id="{{ $loop->index+1 }}">
                            <td><span name="task_{{ $loop->index+1 }}">{{ $mr['task'] }}</span></td>
                            <td><span name="norm_{{ $loop->index+1 }}">{{ $mr['norm_contacts'] }}</span></td>
                            <td><span name="number_{{ $loop->index+1 }}">{{ $mr['number_contacts'] }}</span></td>
                            <td><span name="group_tasks_{{ $loop->index+1 }}">{{ $mr['grouptasks'] }}</span></td>
                            @if($mr['questions'])
                                <td><a id="showFormQuestions" onclick="showFormQuestions({{ $loop->index+1 }});">Редактировать</a></td>

                            @else
                                <td><a id="showFormQuestions" onclick="showNewFormQuestions();">Добавить</a></td>
                            @endif
                            <td><a href="" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                            <td><a href="/defaultcab/module-rule/del/{{ $mr['id'] }}" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>
                        </tr>
                        <tr>
                            <td colspan="7">
                                <div id="formQuestions_{{ $loop->index+1 }}" style="display: none;">
                                    <div class="box">
                                        <div class="form-group">
                                            <label>Текст вопроса</label>
                                            <textarea class="form-control" rows="3" id="questions" name="questions">{{ $mr['questions'] }}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>В карточчке организации виден: </label>
                                            <div class="checkbox">
                                                @foreach($further_questions as $fq)
                                                    <label>
                                                        <input type="checkbox" value="{{ $fq->id }}" name="fq[]" id="check_{{ $fq->id }}_{{ $mr['id'] }}">
                                                        {{ $fq->name }}
                                                    </label>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr id="no_row">
                        <td colspan="7">Список задач отсутствует</td>
                    </tr>
                @endif
                <tr id="norm_list"></tr>
                <tr>
                    <td colspan="7">
                        <div id="newFormQuestions" style="display: none;">
                            <div class="box">
                                <div class="form-group">
                                    <label>Текст вопроса</label>
                                    <textarea class="form-control" rows="3" id="questions" name="questions"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>В карточчке организации виден: </label>
                                    <div class="checkbox">
                                        @foreach($further_questions as $fq)
                                            <label>
                                                <input type="checkbox" class="test" name="fq[]" value="{{ $fq->id }}" id="check_{{ $fq->id }}">
                                                {{ $fq->name }}
                                            </label>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr id="add_row">
                    <td><input type="text" id="task" class="form-control"></td>
                    <td><input type="number" id="norm" class="form-control"></td>
                    <td><input type="number" id="number" class="form-control"></td>
                    <td>
                        <select class="form-control" id="group_tasks">
                            @foreach($tasks as $t)
                            <option value="{{ $t->name }}">{{ $t->name }}</option>
                            @endforeach
                        </select>
                    </td>
                    <td></td>
                    <td>
                        <a class="btn btn-primary" id="addNorm">
                            <i class="fa fa-check"></i>
                        </a>
                    </td>
                </tr>
                </tbody></table>

            <div class="form-group">
                <input type="submit" value="Сохранить" class="btn btn-success">
                <a href="/defaultcab/modules" class="btn btn-default">Отмена</a>
            </div>
        </form>
    </div>
@endsection

@section('main-menu')
    @include('default.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {

        });

        function showFormQuestions(id) {
            $("#formQuestions_"+id).slideToggle("slow");
            $(this).toggleClass("active");
            return false;
        }

        function showNewFormQuestions() {
            $("#newFormQuestions").slideToggle("slow");
            $(this).toggleClass("active");
            return false;
        }

        $("#addNorm").click(function () {
            $("#no_row").remove();

            task = $("#task").val();
            norm = $("#norm").val();
            number = $("#number").val();
            group_tasks = $("#group_tasks").val();
            tasks = [];
            i = $("#tableTask tr").length-2;

            if (!task) {
                alert('Введите задачу');
                exit();
            }

            if (!norm) {
                alert('Введите норму');
                exit();
            }

            if (!number) {
                alert('Введите количество разрешенных контактов');
                exit();
            }

            $('#test').val(JSON.stringify(tasks));

            $("#norm_list").before('<tr id="'+i+'">' +
                '<td><span name="task_'+i+'">'+task+'</span></td>' +
                '<td><span name="norm_'+i+'">'+norm+'</td>' +
                '<td><span name="number_'+i+'">'+number+'</td>' +
                '<td><span name="group_tasks_'+i+'">'+group_tasks+'</td>' +
                '<td><a onclick="showNewFormQuestions();">Добавить</a></td>' +
                '<td><a class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>' +
                '<td><a onclick="delTasks('+i+');" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>' +
                '</tr>');
            $("#task").val('');
            $("#norm").val('');
            $("#number").val('');
            i += 1;
        });

        $("#formModuleEdit").submit(function() {
            col_tr = $("#tableTask tr").length-2;
            num_check = $(".test").length;
            row = [];
            result = [];

            for(i = 1; i < col_tr; i++) {
                row[i] = {'task': $('#'+i+' span[name~="task_'+i+'"]').text(),
                        'norm': $('#'+i+' span[name~="norm_'+i+'"]').text(),
                        'number': $('#'+i+' span[name~="number_'+i+'"]').text(),
                        'group_tasks': $('#'+i+' span[name~="group_tasks_'+i+'"]').text()
                };
            }

            for (var j = 0; j <= row.length; j++) {
                if (row[j] != null && row[j] != 'underfinded' && row[j] != '') {
                    result.push(row[j]);
                }
            }

            /*for (var k = 1; k <= num_check; k++) {
                if($('#check_'+k).prop('checked')) {
                    alert($('#check_'+k).val());
                }
            }*/

            $("#row").val(JSON.stringify(result));
            if ($("#row").val() == '') {
                alert('Error');
                return false;
            } else {
                return true;
            }
        });

        function delTasks(id) {
            $('#'+id).remove();
        }
    </script>
@endpush