@extends('layouts.cabinet')

@section('title')
    Справочник "Статусы задач": Редактирование статуса
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="box" style="padding: 10px;">
        <form action="/defaultcab/tasks-status/edit/{{ $tstatus->id }}" method="post">
            {{ csrf_field() }}
            <div class="form-group" id="NameTaskStatus">
                <label class="control-label" for="inputNameTaskStatus">Название модуля</label>
                <input type="text" class="form-control" name="name_tstatus" value="{{ $tstatus->name }}" id="inputNameTaskStatus" style="width: 300px;">
            </div>
            <div class="form-group">
                <input type="submit" value="Сохранить" class="btn btn-success">
                <a href="/defaultcab/tasks-status" class="btn btn-default">Отмена</a>
            </div>
        </form>
    </div>
@endsection

@section('main-menu')
    @include('default.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">

    </script>
@endpush