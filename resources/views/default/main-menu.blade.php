@if(session('perm')['dashboard.view'])
<li>
    <a href="/defaultcab">
        <i class="fa fa-dashboard"></i> <span>Панель управления</span>
    </a>
</li>
@endif
@if(session('perm')['task.view'])
<li>
    <a href="/defaultcab/tasks">
        <i class="fa fa-tasks"></i> <span>Мои задачи</span>
    </a>
</li>
@endif
@if(session('perm')['project.view'])
<li>
    <a href="/defaultcab/projects">
        <i class="fa fa-folder"></i> <span>Мои проекты</span>
    </a>
</li>
@endif
@if(session('perm')['user.view'] || session('perm')['group.view'])
<li class="treeview">
    <a href="#">
        <i class="fa fa-cogs"></i> <span>Настройки</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu" style="display: none;">
        @if(session('perm')['user.view'])
        <li>
            <a href="/defaultcab/users">
                <i class="fa fa-user"></i> <span>Пользователи</span>
            </a>
        </li>
        @endif
        @if(session('perm')['group.view'])
        <li>
            <a href="/defaultcab/groups">
                <i class="fa fa-users"></i> <span>Группы пользователей</span>
            </a>
        </li>
        @endif
        @if(session('perm')['right.view'])
        <li>
            <a href="/defaultcab/rights">
                <i class="fa fa-circle-o"></i> <span>Права доступа</span>
            </a>
        </li>
        @endif
        @if(session('perm')['task_group_ref.view'] || session('perm')['status_task_ref.view'] || session('perm')['module_ref.view'])
        <li class="treeview">
            <a href="#"><i class="fa fa-circle-o"></i> Справочники
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu" style="display: none;">
                @if(session('perm')['task_group_ref.view'])
                    <li><a href="/defaultcab/group-tasks"><i class="fa fa-circle-o"></i> Группы задач</a></li>
                @endif
                @if(session('perm')['status_task_ref.view'])
                    <li><a href="/defaultcab/tasks-status"><i class="fa fa-circle-o"></i> Статусы задач</a></li>
                @endif
                @if(session('perm')['module_ref.view'])
                    <li><a href="/defaultcab/modules"><i class="fa fa-circle-o"></i> Модули</a></li>
                @endif
            </ul>
        </li>
        @endif
    </ul>
</li>
@endif
@if(session('perm')['calendar.view'])
<li>
    <a href="/defaultcab/calendar">
        <i class="fa fa-calendar"></i> <span>Календарь</span>
    </a>
</li>
@endif