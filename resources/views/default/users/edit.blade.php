@extends('layouts.cabinet')

@section('title')
    Редактирование пользователя "{{ $u->last_name }} {{ $u->first_name }}"
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            <form action="/defaultcab/user/update/{{ $u->id }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group" id="NameGroup">
                    <label class="control-label" for="inputName" style="display: none;"></label>
                    <input type="text" class="form-control" name="inputName" placeholder="Имя" value="{{ $u->first_name }}">
                </div>
                <div class="form-group" id="LastNameGroup">
                    <label class="control-label" for="inputLastName" style="display: none;"></label>
                    <input type="text" class="form-control" name="inputLastName" placeholder="Фамилия" value="{{ $u->last_name }}">
                </div>
                <div class="form-group">
                    <img id="blah" src="{{ asset('public/avatars/'.$u->foto) }}" alt="your image" />
                </div>
                <div class="form-group">
                    <input type="file" name="img" accept="image/*" enctype="multipart/form-data">
                </div>
                <div class="form-group" id="EmailGroup">
                    <label class="control-label" for="inputEmail" style="display: none;"></label>
                    <input type="text" class="form-control" name="inputEmail" placeholder="Email" value="{{ $u->email }}">
                </div>
                <div class="form-group" id="PasswordGroup">
                    <label class="control-label" for="inputPassword" style="display: none;"></label>
                    <input type="password" class="form-control" name="inputPassword" placeholder="Пароль">
                </div>
                <div class="form-group">
                    <label>Группа</label>
                    <select class="form-control" name="user_role" id="role">
                        @foreach($roles as $r)
                            <option value="{{ $r->id }}">{{ $r->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <input type="submit" value="Сохранить" class="btn btn-success">
                    <a href="/defaultcab/users" class="btn btn-default">Отмена</a>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('main-menu')
    @include('default.main-menu')
@endsection