@extends('layouts.cabinet')

@section('title')
    Добавление пользователя
@endsection

@section('content')
    <div class="alert alert-dismissible" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4></h4>
        <span></span>
    </div>

    <div class="row">
        <div class="col-md-3">
            <form action="/defaultcab/user/add" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group" id="NameGroup">
                    <label class="control-label" for="inputName" style="display: none;"></label>
                    <input type="text" class="form-control" name="user_name" id="inputName" placeholder="Имя">
                </div>
                <div class="form-group" id="LastNameGroup">
                    <label class="control-label" for="inputLastName" style="display: none;"></label>
                    <input type="text" class="form-control" name="user_surname" id="inputLastName" placeholder="Фамилия">
                </div>
                <div class="form-group">
                    <label for="inputPicture">Фото</label><br>

                </div>
                <div class="form-group">
                    <input type="file" name="img" accept="image/*" enctype="multipart/form-data">
                </div>
                <div class="form-group" id="EmailGroup">
                    <label class="control-label" for="inputEmail" style="display: none;"></label>
                    <input type="text" class="form-control" name="user_email" id="inputEmail" placeholder="Email">
                </div>
                <div class="form-group" id="PasswordGroup">
                    <label class="control-label" for="inputPassword" style="display: none;"></label>
                    <input type="password" class="form-control" name="user_pass" id="inputPassword" placeholder="Пароль">
                </div>
                <div class="form-group">
                    <label>Группа</label>
                    <select class="form-control" name="user_role" id="role">
                        @foreach($roles as $r)
                            <option value="{{ $r->id }}">{{ $r->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <input type="submit" value="Сохранить" class="btn btn-success">
                    <a href="/defaultcab/users" class="btn btn-default">Отмена</a>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('main-menu')
    @include('default.main-menu')
@endsection
