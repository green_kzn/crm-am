@if(session('perm')['dashboard.view'])
<li>
    <router-link to="/manager">
        <i class="fa fa-dashboard"></i> <span>Панель управления</span>
    </router-link>
</li>
@endif
@if(session('perm')['task.view'])
<li>
    <a href="/manager/tasks">
        <i class="fa fa-tasks"></i> <span>Мои задачи</span>
    </a>
</li>
@endif
@if(session('perm')['project.view'])
<li>
    <router-link to="/manager/projects">
        <i class="fa fa-folder"></i> <span>Мои проекты</span>
        <span class="pull-right-container">
          <small class="label pull-right bg-red">{{ session('raw_projects') }}</small>
        </span>
    </router-link>
</li>
@endif
@if(session('perm')['print_form.view'])
    <li>
        <a href="/manager/print-form">
            <i class="fa fa-print"></i> <span>Отдел разработки форм</span>
        </a>
    </li>
@endif
@if(session('perm')['doc.view'])
    <li>
        <a href="/manager/doc">
            <i class="fa fa-book"></i> <span>Документооборот</span>
        </a>
    </li>
@endif
@if(session('perm')['calls.view'])
    <li>
        <a href="/manager/calls">
            <i class="fa fa-phone"></i> <span>Клиенты на обзвон</span>
        </a>
    </li>
@endif
@if(session('perm')['user.view'] || session('perm')['group.view'])
<li class="treeview">
    <a href="#">
        <i class="fa fa-cogs"></i> <span>Настройки</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu" style="display: none;">
        <li>
            <a href="/manager/settings">
                <i class="fa fa-wrench"></i> <span>Общие настройки</span>
            </a>
        </li>
        @if(session('perm')['user.view'])
        <li>
            <a href="/manager/users">
                <i class="fa fa-user"></i> <span>Пользователи</span>
            </a>
        </li>
        @endif
        @if(session('perm')['group.view'])
        <li>
            <a href="/manager/groups">
                <i class="fa fa-users"></i> <span>Группы пользователей</span>
            </a>
        </li>
        @endif
        @if(session('perm')['right.view'])
        <li>
            <a href="/manager/rights">
                <i class="fa fa-circle-o"></i> <span>Права доступа</span>
            </a>
        </li>
        @endif
        @if(session('perm')['task_group_ref.view'] || session('perm')['status_task_ref.view'] || session('perm')['module_ref.view'])
        <li class="treeview">
            <a href="#"><i class="fa fa-circle-o"></i> Справочники
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu" style="display: none;">
                @if(session('perm')['task_group_ref.view'])
                    <li><a href="/manager/group-tasks"><i class="fa fa-circle-o"></i> Группы задач</a></li>
                @endif
                @if(session('perm')['status_task_ref.view'])
                    <li><a href="/manager/tasks-status"><i class="fa fa-circle-o"></i> Статусы задач</a></li>
                @endif
                @if(session('perm')['module_ref.view'])
                    <li><a href="/manager/modules"><i class="fa fa-circle-o"></i> Модули</a></li>
                @endif
                @if(session('perm')['clients_ref.view'])
                    <li><a href="/manager/clients"><i class="fa fa-circle-o"></i> Клиенты</a></li>
                @endif
                @if(session('perm')['citys_ref.view'])
                    <li><a href="/manager/citys"><i class="fa fa-circle-o"></i> Города</a></li>
                @endif
                @if(session('perm')['documents_ref.view'])
                    <li><a href="/manager/documents"><i class="fa fa-circle-o"></i> Документы</a></li>
                @endif
            </ul>
        </li>
        @endif
    </ul>
</li>
@endif
@if(session('perm')['calendar.view'])
<li>
    <a href="/manager/calendar">
        <i class="fa fa-calendar"></i> <span>Календарь</span>
    </a>
</li>
@endif