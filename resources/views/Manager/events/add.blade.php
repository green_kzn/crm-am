@extends('layouts.cabinet')

@section('title')
    Добавление события
@endsection

@section('content')
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <form action="/manager/events/add" method="post">
                {{ csrf_field() }}
                <div class="form-group" id="GNameGroup">
                    <label class="control-label" for="inputTitleEvent">Название</label>
                    <input type="text" name="inputTitleEvent" class="form-control" id="inputTitleEvent">
                    <input type="checkbox" name="fullDay"> Событие на весь день
                </div>
                <table id="newEvent">
                    <tr>
                        <td>
                            <div class="form-group">
                                <label>Дата</label>
                                <div class="input-group date" style="margin-left: -10px;">
                                    <input type="text" name="dateEvent" class="form-control pull-right" id="datepicker">
                                </div>
                                <!-- /.input group -->
                            </div>
                        </td>
                        <td>
                            <div class="bootstrap-timepicker"><div class="bootstrap-timepicker-widget dropdown-menu"><table><tbody><tr><td><a href="#" data-action="incrementHour"><i class="glyphicon glyphicon-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="incrementMinute"><i class="glyphicon glyphicon-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td class="meridian-column"><a href="#" data-action="toggleMeridian"><i class="glyphicon glyphicon-chevron-up"></i></a></td></tr><tr><td><span class="bootstrap-timepicker-hour">06</span></td> <td class="separator">:</td><td><span class="bootstrap-timepicker-minute">30</span></td> <td class="separator">&nbsp;</td><td><span class="bootstrap-timepicker-meridian">PM</span></td></tr><tr><td><a href="#" data-action="decrementHour"><i class="glyphicon glyphicon-chevron-down"></i></a></td><td class="separator"></td><td><a href="#" data-action="decrementMinute"><i class="glyphicon glyphicon-chevron-down"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="toggleMeridian"><i class="glyphicon glyphicon-chevron-down"></i></a></td></tr></tbody></table></div>
                                <div class="form-group">
                                    <label>Время</label>
                                    <div class="input-group" style="margin-left: -10px;">
                                        <input type="text" name="timeEvent" class="form-control timepicker">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <!-- /.form group -->
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <label>Повторять</label>
                                <select class="form-control" name="repeatEvent">
                                    <option value="no">Не повторять</option>
                                    <option value="week">Один раз в неделю</option>
                                    <option value="month">Один раз в месяц</option>
                                    <option value="year">Один раз в год</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                </table>

                <div class="form-group">
                    <input type="submit" value="Сохранить" class="btn btn-success">
                    <a href="/manager/calendar" class="btn btn-default">Отмена</a>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('main-menu')
    @include('Manager.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">
        $("#datepicker").datepicker({
            inline: true,
            changeYear: true,
            changeMonth: true
        });

        jQuery(function ($) {
            $.datepicker.regional['ru'] = {
                closeText: 'Закрыть',
                prevText: '&#x3c;Пред',
                nextText: 'След&#x3e;',
                currentText: 'Сегодня',
                monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                    'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                monthNamesShort: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                    'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
                dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
                dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                weekHeader: 'Нед',
                dateFormat: 'dd.mm.yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };
            $.datepicker.setDefaults($.datepicker.regional['ru']);
        });
        $('#datepicker').datepicker($.extend({
                inline: true,
                changeYear: true,
                changeMonth: true,
            },
            $.datepicker.regional['ru']
        ));
        $(function () {

        });

        //Timepicker
        $('.timepicker').timepicker({
            showInputs: false
        })
    </script>
@endpush
