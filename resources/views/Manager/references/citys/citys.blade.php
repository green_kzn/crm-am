@extends('layouts.cabinet')

@section('title')
    Справочник "Города"
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="col-md-6">
        @if(session('perm')['citys_ref.create'])
            <div style="padding-bottom: 20px;">
                <a href="/manager/citys/add" class="btn btn-primary">Добавить</a>
            </div>
        @endif
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Список городов</h3>
            </div>
            <div class="box-body table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Город</th>
                        <th>Разница во времени с Москвой</th>
                        <th>Действие</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($citys as $city)
                            <tr>
                                <td width="1%">{{ $loop->index+1 }}</td>
                                <td width="60%">{{ $city->name }}</td>
                                <td width="10%">{{ $city->name }}</td>
                                <td>
                                    @if(session('perm')['citys_ref.update'])
                                    <a href="/manager/citys/edit/{{ $city->id }}" class="btn btn-primary">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    @else
                                    <a href="javascript: void(0)" class="btn btn-primary" disabled>
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    @endif
                                    @if(session('perm')['citys_ref.delete'])
                                    <a href="/manager/citys/del/{{ $city->id }}" class="btn btn-danger">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                    @else
                                    <a href="javascript: void(0)" class="btn btn-danger" disabled>
                                        <i class="fa fa-trash"></i>
                                    </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <?php echo $citys->render(); ?>
    </div>
@endsection

@section('main-menu')
    @include('Manager.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">

    </script>
@endpush