@extends('layouts.cabinet')

@section('title')
    Справочник "Группы задач": Редактирование задачи
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="box" style="padding: 10px;">
        <form action="/manager/group-tasks/edit/{{ $task->id }}" method="post">
            {{ csrf_field() }}
            <div class="form-group" id="NameTask">
                <label class="control-label" for="inputNameTask">Название задачи</label>
                <input type="text" class="form-control" name="name_task" value="{{ $task->name }}" id="inputNameTask" style="width: 300px;">
            </div>
            <div class="form-group" id="NameTask">
                <label class="control-label" for="isPrintForm">Печатная форма? </label>
                @if($task->is_print_form == 1 )
                <input type="checkbox" name="isPrintForm" id="isPrintForm" checked>
                @else
                <input type="checkbox" name="isPrintForm" id="isPrintForm">
                @endif
            </div>
            <div class="form-group">
                <input type="submit" value="Сохранить" class="btn btn-success">
                <a href="/manager/group-tasks" class="btn btn-default">Отмена</a>
            </div>
        </form>
    </div>
@endsection

@section('main-menu')
    @include('Manager.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">

    </script>
@endpush