@extends('layouts.cabinet')

@section('title')
    Справочник "Клиенты": Добавление нового клиента
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="col-md-12">
        <div class="box" style="padding: 10px;">
            <form class="form-horizontal" id="formNewClient" action="/manager/clients/add" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="contacts" id="contacts">
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputNameClient" class="col-sm-2 control-label">Наименование клиента</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="name_client" id="inputNameClient">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputNameClient" class="col-sm-2 control-label">Город</label>
                        <div class="col-sm-6">
                            <select name="client_city" id="" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                @foreach($citys as $c)
                                    <option value="{{ $c->id }}">{{ $c->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <div class="col-sm-4">
                                <a class="btn btn-primary">Добавить</a>
                            </div>
                            <div class="col-sm-4">
                                <a class="btn btn-primary">Редактировать</a>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Контакты</h3>
                        </div>
                        <table class="table" id="tableContact">
                            <thead>
                            <tr>
                                <th>Фамилия</th>
                                <th>Имя*</th>
                                <th>Отчество</th>
                                <th>Должность</th>
                                <th>Телефон*</th>
                                <th>Email</th>
                                <th colspan="2">Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr id="contact_row"></tr>
                            <tr id="inputRow">
                                <td><input name="contact_surname" type="text" class="form-control contact_surname"></td>
                                <td><input name="contact_name" type="text" class="form-control contact_name"></td>
                                <td><input name="contact_patronymic" type="text" class="form-control contact_patronymic"></td>
                                <td>
                                    <select class="form-control contact_post" name="contact_post">
                                        @foreach($posts as $p)
                                            <option id="{{ $p->id }}" value="{{ $p->name }}">{{ $p->name }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td><input name="contact_phone" type="phone" class="form-control contact_phone" placeholder="+7 999 999-99-99"></td>
                                <td><input name="contact_email" type="email" class="form-control contact_email" placeholder="email@site.ru"></td>
                                <td></td>
                                <td>
                                    <a onclick="addContact();" class="btn btn-success">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Сохранить" class="btn btn-primary">
                        <a href="/manager/clients" class="btn btn-default">Отмена</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('main-menu')
    @include('Manager.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.contact_surname').focusout(function () {
                $('#for-surname').text('');
                AMValidate(this);
            });
            $('.contact_name').focusout(function () {
                $('#for-name').text('');
                AMValidate(this);
            });
            $('.contact_patronymic').focusout(function () {
                $('#for-patronymic').text('');
                AMValidate(this);
            });
            $('.contact_phone').focus(function () {
                $(this).attr('placeholder', '');
            });
            $('.contact_phone').focusout(function () {
                $(this).attr('placeholder', '+7 999 999-99-99');

                $('#for-phone').text('');
                AMValidate(this);
            });
            $('.contact_email').focusout(function () {
                $('#for-email').text('');
                AMValidate(this);
            });

            $('#formNewClient').submit(function () {
                var clients = [];
                num_clients = $('.num_row').length;

                for (i=1; i<=num_clients; i++) {
                    clients.push({
                        'name': $('span[name~="contacts_name_'+i+'"]').html(),
                        'surname': $('span[name~="contacts_surname_'+i+'"]').html(),
                        'patronymic': $('span[name~="contacts_patronymic_'+i+'"]').html(),
                        'post': $('span[name~="contacts_post_'+i+'"]').html(),
                        'phone': $('span[name~="contacts_phone_'+i+'"]').html(),
                        'email': $('span[name~="contacts_email_'+i+'"]').html(),
                    });
                }

                $('#contacts').val(JSON.stringify(clients));

                if (clients.length == 0) {
                    alert('Запоните контакты');
                    return false;
                } else {
                    return true;
                }
            });
        });

        function AMValidate(e) {
            name = $(e).attr('name');
            switch (name) {
                case 'contact_surname':
                    var patt1 = new RegExp("[A-Za-zА-Яа-яЁё]");
                    if ($(e).val().length < 3) {
                        $(e).after('<span id="for-surname" class="text-error">Фамилия должна быть больше 3 символов</span>');
                        break;
                    }
                    if (!patt1.test($(e).val())) {
                        $(e).after('<span id="for-surname" class="text-error">Фамилия может содержать только буквы</span>');
                        break;
                    }
                    break;
                case 'contact_name':
                    var patt2 = new RegExp("[A-Za-zА-Яа-яЁё]");
                    if ($(e).val().length < 2) {
                        $('#for-name').text('');
                        $(e).after('<span id="for-name" class="text-error">Имя должно содержать больше 2 символов</span>');
                        break;
                    }
                    if (!patt2.test($(e).val())) {
                        $('#for-name').text('');
                        $(e).after('<span id="for-name" class="text-error">Имя может содержать только буквы</span>');
                        break;
                    }
                    break;
                case 'contact_patronymic':
                    var patt3 = new RegExp("[A-Za-zА-Яа-яЁё]");
                    if ($(e).val().length < 3) {
                        $('#for-patronymic').text('');
                        $(e).after('<span id="for-patronymic" class="text-error">Отчество должно быть больше 3 символов</span>');
                        break;
                    }
                    if (!patt3.test($(e).val())) {
                        $('#for-patronymic').text('');
                        $(e).after('<span id="for-patronymic" class="text-error">Отчество может содержать только буквы</span>');
                        break;
                    }
                    break;
                case 'contact_phone':
                    var patt3 = new RegExp("((8|\\+7)-?)?\\(?\\d{3,5}\\)?-?\\d{1}-?\\d{1}-?\\d{1}-?\\d{1}-?\\d{1}((-?\\d{1})?-?\\d{1})?");
                    if (!patt3.test($(e).val())) {
                        $('#for-phone').text('');
                        $(e).after('<span id="for-phone" class="text-error">Номер телефона указан не верно</span>');
                        break;
                    }
                    break;
                case 'contact_email':
                    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                    if (!reg.test($(e).val())) {
                        $('#for-email').text('');
                        $(e).after('<span id="for-email" class="text-error">Email указан не верно</span>');
                        break;
                    }
                    break;
            }
        }

        function addContact() {
            num_edit = $('.fa-check').length;

            if (num_edit == 1) {
                alert('Имеются не сохраненные данные. Сохраните сначала данные');
                exit();
            } else {
                surname = $('#inputRow > td > .contact_surname').val();
                name = $('#inputRow > td > .contact_name').val();
                patronymic = $('#inputRow > td > .contact_patronymic').val();
                post = $('#inputRow > td > .contact_post option:selected').val();
                phone = $('#inputRow > td > .contact_phone').val();
                email = $('#inputRow > td > .contact_email').val();
                var patt3 = new RegExp("((8|\\+7)-?)?\\(?\\d{3,5}\\)?-?\\d{1}-?\\d{1}-?\\d{1}-?\\d{1}-?\\d{1}((-?\\d{1})?-?\\d{1})?");

                i = $('.num_row').length;

                if (!name) {
                    AMValidate($('.contact_name'));
                }

                if (!phone) {
                    AMValidate($('.contact_phone'));
                }

                if (name && patt3.test(phone)) {
                    $('#contact_row').before('<tr class="num_row" id="rowContact_'+(i+1)+'">' +
                        '<td name="td_surname_'+(i+1)+'"><span name="contacts_surname_'+(i+1)+'">'+surname+'</span></td>' +
                        '<td name="td_name_'+(i+1)+'"><span name="contacts_name_'+(i+1)+'">'+name+'</td>' +
                        '<td name="td_patronymic_'+(i+1)+'"><span name="contacts_patronymic_'+(i+1)+'">'+patronymic+'</span></td>' +
                        '<td name="td_post_'+(i+1)+'"><span name="contacts_post_'+(i+1)+'">'+post+'</td></span>' +
                        '<td name="td_phone_'+(i+1)+'"><span name="contacts_phone_'+(i+1)+'">'+phone+'</td></span>' +
                        '<td name="td_email_'+(i+1)+'"><span name="contacts_email_'+(i+1)+'">'+email+'</td></span>' +
                        '<td><a onclick="delContact('+(i+1)+');" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>' +
                        '<td name="td_check_'+(i+1)+'"><a onclick="editContact('+(i+1)+');"class="btn btn-success"><i class="fa fa-pencil"></i></a></td>' +
                        '</tr>');
                }
            }

            $('#inputRow > td > .contact_surname').val('');
            $('#inputRow > td > .contact_name').val('');
            $('#inputRow > td > .contact_patronymic').val('');
            $('#inputRow > td > .contact_phone').val('');
            $('#inputRow > td > .contact_email').val('');
        }

        function delContact(i) {
            $('#rowContact_'+i).remove();
        }

        function editContact(i) {
            num_edit = $('.fa-check').length;

            if (num_edit == 1) {
                alert('Имеются не сохраненные данные. Сохраните сначала данные');
                exit();
            } else {
                name = $('#rowContact_' + i + ' > td > span[name~="contacts_name_' + i + '"]').html();
                surname = $('#rowContact_' + i + ' > td > span[name~="contacts_surname_' + i + '"]').html();
                patronymic = $('#rowContact_' + i + ' > td > span[name~="contacts_patronymic_' + i + '"]').html();
                post = $('#rowContact_' + i + ' > td > span[name~="contacts_post_' + i + '"]').html();
                phone = $('#rowContact_' + i + ' > td > span[name~="contacts_phone_' + i + '"]').html();
                email = $('#rowContact_' + i + ' > td > span[name~="contacts_email_' + i + '"]').html();

                $('#rowContact_' + i + ' > td[name~="td_surname_' + i + '"]').html('<input value="' + surname + '" name="contact_surname" type="text" class="form-control contact_surname">');
                $('#rowContact_' + i + ' > td[name~="td_name_' + i + '"]').html('<input value="' + name + '" name="contact_name" required type="text" class="form-control contact_name">');
                $('#rowContact_' + i + ' > td[name~="td_patronymic_' + i + '"]').html('<input value="' + patronymic + '" name="contact_patronymic" type="text" class="form-control contact_patronymic">');
                $('#rowContact_' + i + ' > td[name~="td_post_' + i + '"]').html('<select class="form-control contact_post" name="contact_post">' +
                    '@foreach($posts as $p)' +
                    '<option id="{{ $p->id }}" value="{{ $p->name }}">{{ $p->name }}</option>' +
                    '@endforeach' +
                    '</select>');
                $('#rowContact_' + i + ' > td[name~="td_phone_' + i + '"]').html('<input value="' + phone + '" name="contact_phone" required type="text" class="form-control contact_phone">');
                $('#rowContact_' + i + ' > td[name~="td_email_' + i + '"]').html('<input value="' + email + '" name="contact_email" type="text" class="form-control contact_email">');
                $('#rowContact_' + i + ' > td[name~="td_check_' + i + '"]').html('<a onclick="saveContact('+i+');" class="btn btn-success"><i class="fa fa-check"></i></a>');
            }
        }

        function saveContact(i) {
            name = $('#rowContact_' + i + ' > td[name~="td_name_'+i+'"] > input').val();
            surname = $('#rowContact_' + i + ' > td[name~="td_surname_'+i+'"] > input').val();
            patronymic = $('#rowContact_' + i + ' > td[name~="td_patronymic_'+i+'"] > input').val();
            post = $('#rowContact_' + i + ' > td[name~="td_post_'+i+'"] > select option:selected').val();
            phone = $('#rowContact_' + i + ' > td[name~="td_phone_'+i+'"] > input').val();
            email = $('#rowContact_' + i + ' > td[name~="td_email_'+i+'"] > input').val();

            $('#rowContact_' + i + ' > td[name~="td_surname_' + i + '"]').html('<span name="contacts_surname_'+i+'">'+surname+'</span>');
            $('#rowContact_' + i + ' > td[name~="td_name_' + i + '"]').html('<span name="contacts_name_'+i+'">'+name+'</span>');
            $('#rowContact_' + i + ' > td[name~="td_patronymic_' + i + '"]').html('<span name="contacts_patronymic_'+i+'">'+patronymic+'</span>');
            $('#rowContact_' + i + ' > td[name~="td_post_' + i + '"]').html('<span name="contacts_post_'+i+'">'+post+'</span>');
            $('#rowContact_' + i + ' > td[name~="td_phone_' + i + '"]').html('<span name="contacts_phone_'+i+'">'+phone+'</span>');
            $('#rowContact_' + i + ' > td[name~="td_email_' + i + '"]').html('<span name="contacts_email_'+i+'">'+email+'</span>');
            $('#rowContact_' + i + ' > td[name~="td_check_' + i + '"]').html('<a onclick="editContact('+i+');"class="btn btn-success"><i class="fa fa-pencil"></i></a>');
        }
    </script>
@endpush