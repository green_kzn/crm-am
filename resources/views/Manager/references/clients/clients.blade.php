@extends('layouts.cabinet')

@section('title')
    Справочник "Клиенты"
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="col-md-6">
        @if(session('perm')['clients_ref.create'])
            <div style="padding-bottom: 20px;">
                <a href="/manager/clients/add" class="btn btn-primary">Добавить</a>
            </div>
        @endif
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Список клиентов</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Клиент</th>
                        <th>Город</th>
                        <th colspan="2" width="10%">Действие</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($clients != '')
                        @foreach($clients as $c)
                        <tr>
                            <td>{{ $loop->index+1 }}</td>
                            <td>{{ $c['name'] }}</td>
                            <td>{{ $c['city'] }}</td>
                            <td>
                                @if(session('perm')['clients_ref.update'])
                                <a href="/manager/clients/edit/{{ $c['id'] }}" class="btn btn-success">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                @else
                                <a href="javascript: void(0)" class="btn btn-success" disabled>
                                    <i class="fa fa-pencil"></i>
                                </a>
                                @endif
                            </td>
                            <td>
                                @if(session('perm')['clients_ref.delete'])
                                <a href="/manager/clients/del/{{ $c['id'] }}" class="btn btn-danger">
                                    <i class="fa fa-trash"></i>
                                </a>
                                @else
                                <a href="javascript: void(0)" class="btn btn-danger" disabled>
                                    <i class="fa fa-trash"></i>
                                </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3"><p style="text-align: center;">Клиенты не найдены</p></td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('main-menu')
    @include('Manager.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">

    </script>
@endpush