@extends('layouts.cabinet')

@section('title')
    Справочник "Модули": Редактирование модуля
@endsection

@section('content')
    <div class="alert alert-success alert-dismissible" id="success" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
        Уточняющий вопрос успешно обновлен
    </div>
    <div class="alert alert-danger alert-dismissible" id="error" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
        Произошла ошибка при обновлении уточняющего вопроса
    </div>
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="box" style="padding: 10px;">
        <form action="/manager/modules/edit/{{ $module->id }}" method="post" id="formModuleEdit">
            {{ csrf_field() }}
            <input type="hidden" id="row" name="row">
            <input type="hidden" id="module_id" value="{{ $module->id }}">
            <div class="form-group" id="NameTask">
                <label class="control-label" for="inputNameModule">Название модуля</label>
                <input type="text" class="form-control" name="name_module" value="{{ $module->name }}" id="inputNameModule" style="width: 300px;">
            </div>

            <table class="table table-hover" id="tableTask">
                <tbody><tr>
                    <th>Список задач</th>
                    <th>Норма кол-ва контактов по договору 1 контакт - 1 час</th>
                    <th>Кол - во разрешенных контактов</th>
                    <th>Группа задач</th>
                    <th>Вопросы для уточнения</th>
                    <th colspan="2">Действия</th>
                </tr>
                @if($module_rule)
                    @foreach($module_rule as $k => $mr)
                        <tr id="{{ $loop->index+1 }}">
                            <td id="task_{{ $loop->index+1 }}"><span name="task_{{ $loop->index+1 }}">{{ $mr['task'] }}</span></td>
                            <td id="norm_{{ $loop->index+1 }}"><span name="norm_{{ $loop->index+1 }}">{{ $mr['norm_contacts'] }}</span></td>
                            <td id="number_{{ $loop->index+1 }}"><span name="number_{{ $loop->index+1 }}">{{ $mr['number_contacts'] }}</span></td>
                            <td id="group_tasks_{{ $loop->index+1 }}"><span name="group_tasks_{{ $loop->index+1 }}">{{ $mr['grouptasks'] }}</span></td>
                            @if($mr['questions'])
                                <td><a style="cursor: pointer;" id="showFormQuestions" onclick="showFormQuestions({{ $loop->index+1 }});">Редактировать</a></td>
                            @else
                                <td><a style="cursor: pointer;" id="showFormQuestions" onclick="showNewFormQuestions({{ $loop->index+1 }});">Добавить</a></td>
                            @endif
                            <td id="edit_rule_{{ $loop->index+1 }}"><a onclick="editRule({{ $mr['id'] }}, {{ $loop->index+1 }});" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                            <td><a href="/manager/module-rule/del/{{ $mr['id'] }}" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>
                        </tr>
                        <tr id="formQuestions_{{ $loop->index+1 }}" style="display: none;">
                            <td colspan="7">
                                <table class="table">
                                    @if($mr['questions'] != '')
                                        @foreach($mr['questions'] as $q)
                                            <tr>
                                                <td><input type="text" onkeyup="editQuestion({{ $mr['id'] }}, {{$loop->index}});" id="question_{{ $mr['id'] }}_{{$loop->index}}" class="form-control" value="{{ $q }}"></td>
                                                <td>
                                                    <a id="btnSaveQuestion_{{ $mr['id'] }}_{{$loop->index}}" class="btn btn-primary" disabled><i class="fa fa-check"></i></a>
                                                    <a href="/manager/module-rule/del-question/{{ $mr['id'] }}/{{$loop->index}}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td><input id="inputNewQ_{{ $mr['id'] }}" type="text" class="form-control"></td>
                                            <td>
                                                <a class="btn btn-primary" onclick="addQuestion({{ $mr['id'] }});"><i class="fa fa-plus"></i></a>
                                            </td>
                                        </tr>
                                    @endif
                                </table>
                            </td>
                        </tr>
                        <tr id="newFormQuestions_{{ $loop->index+1 }}" style="display: none;">
                            <td colspan="7">
                                <table class="table">
                                    <tr>
                                        <td><input id="inputNewQ_{{ $mr['id'] }}" type="text" class="form-control"></td>
                                        <td>
                                            <a class="btn btn-primary" onclick="addQuestion({{ $mr['id'] }});"><i class="fa fa-plus"></i></a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr id="no_row">
                        <td colspan="7">Список задач отсутствует</td>
                    </tr>
                @endif
                <tr id="add_row">
                    <td><input type="text" id="task" class="form-control"></td>
                    <td><input type="number" id="norm" class="form-control"></td>
                    <td><input type="number" id="number" class="form-control"></td>
                    <td>
                        <select class="form-control" id="group_tasks">
                            @foreach($tasks as $t)
                            <option value="{{ $t->name }}">{{ $t->name }}</option>
                            @endforeach
                        </select>
                    </td>
                    <td></td>
                    <td>
                        <a class="btn btn-primary" id="addNorm">
                            <i class="fa fa-check"></i>
                        </a>
                    </td>
                </tr>
                </tbody></table>

            <div class="form-group">
                <input type="submit" value="Сохранить" class="btn btn-success">
                <a href="/manager/modules" class="btn btn-default">Отмена</a>
            </div>
        </form>
    </div>
@endsection

@section('main-menu')
    @include('Manager.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {

        });

        function addQuestion(qid) {
            new_q = $("#inputNewQ_"+qid).val();
            $.ajax({
                url: '/manager/module-rule/add-question',
                data: {'qid': qid, 'new_q': new_q},
                success: function (resp) {
                    location.reload();
                }
            });
        }

        function editQuestion(qid, loop) {
            $("#btnSaveQuestion_"+qid+'_'+loop).removeAttr('disabled');
            $("#btnSaveQuestion_"+qid+'_'+loop).attr('onclick', 'saveQuestion('+qid+', '+loop+');');
        }

        function saveQuestion(qid, loop) {
            new_q = $("#question_"+qid+"_"+loop).val();

            $.ajax({
                url: '/manager/module-rule/update-question',
                data: {'qid': qid, 'loop': loop, 'new_q': new_q},
                success: function (resp) {
                    if (resp == 'success') {
                        $("#success").css('display', 'block');
                    } else {
                        $("#error").css('display', 'block');
                    }
                }
            });
        }

        function editRule(id, loop) {
            active_btn = $('.btn-success').length;
            if ((active_btn-1) >= 1) {
                alert('Имеются не сохраненные данные. Сохраните сначала изменения');
            } else {
                $.ajax({
                    'url': '/manager/group-tasks/get-task-groups',
                    success: function (resp) {
                        task = $("#task_"+loop+' > span').text();
                        norm = $("#norm_"+loop+' > span').text();
                        number = $("#number_"+loop+' > span').text();
                        group_tasks = $("#group_tasks_"+loop+' > span').text();

                        group_tasks_content = '<select id="inputGroupTask" class="form-control">';
                        for (i=0; i<resp.length; i++) {
                            group_tasks_content += '<option>'+resp[i].name+'</option>';
                        }
                        group_tasks_content += '</select>';

                        $("#task_"+loop).html('<input id="inputTask" type="text" class="form-control" value="'+task+'">');
                        $("#norm_"+loop).html('<input id="inputNorm" type="text" class="form-control" value="'+norm+'">');
                        $("#number_"+loop).html('<input id="inputNumber" type="text" class="form-control" value="'+number+'">');
                        $("#group_tasks_"+loop).html(group_tasks_content);
                        $("#edit_rule_"+loop).html('<a onclick="saveRule('+id+', '+loop+');" class="btn btn-success"><i class="fa fa-check"></i></a>');
                    }
                });
            }
        }

        function saveRule(id, loop) {
            task = $("#inputTask").val();
            norm = $("#inputNorm").val();
            number = $("#inputNumber").val();
            group_tasks = $("#inputGroupTask option:selected").text();
            module_id = $("#module_id").val();

            $.ajax({
                'url': '/manager/module-rule/update-rule',
                'data': {'task': task, 'norm': norm, 'number': number, 'group_tasks': group_tasks, 'module_id': module_id, 'rule_id': id},
                success: function () {
                    $("#task_"+loop).html('<span name="task_'+loop+'">'+task+'</span>');
                    $("#norm_"+loop).html('<span name="norm_'+loop+'">'+norm+'</span>');
                    $("#number_"+loop).html('<span name="number_'+loop+'">'+number+'</span>');
                    $("#group_tasks_"+loop).html('<span name="group_tasks_'+loop+'">'+group_tasks+'</span>');
                    $("#edit_rule_"+loop).html('<a onclick="editRule('+id+', '+loop+');" class="btn btn-primary"><i class="fa fa-pencil"></i></a>');
                    //alert(group_tasks);
                }
            });
        }

        function showFormQuestions(id) {
            $("#formQuestions_"+id).slideToggle("fast");
        }

        function showNewFormQuestions(id) {
            $("#newFormQuestions_"+id).slideToggle("slow");
        }

        $("#addNorm").click(function () {
            $("#no_row").remove();

            task = $("#task").val();
            norm = $("#norm").val();
            number = $("#number").val();
            group_tasks = $("#group_tasks").val();
            module_id = $("#module_id").val();
            tasks = [];
            i = $("#tableTask tr").length-2;

            if (!task) {
                alert('Введите задачу');
                exit();
            }

            if (!norm) {
                alert('Введите норму');
                exit();
            }

            if (!number) {
                alert('Введите количество разрешенных контактов');
                exit();
            }

            $('#test').val(JSON.stringify(tasks));

            $.ajax({
                'url': '/manager/module-rule/add-rule',
                'data': {'task': task, 'norm': norm, 'number': number, 'group_tasks': group_tasks, 'module_id': module_id},
                success: function () {
                    $("#norm_list").before('<tr id="'+i+'">' +
                        '<td id="task_'+i+'"><span name="task_'+i+'">'+task+'</span></td>' +
                        '<td id="norm_'+i+'"><span name="norm_'+i+'">'+norm+'</td>' +
                        '<td id="number_'+i+'"><span name="number_'+i+'">'+number+'</td>' +
                        '<td id="group_tasks_'+i+'"><span name="group_tasks_'+i+'">'+group_tasks+'</td>' +
                        '<td><a onclick="showNewFormQuestions();">Добавить</a></td>' +
                        '<td id="edit_rule_'+i+'"><a onclick="editRule(0, '+i+');" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>' +
                        '<td><a onclick="delTasks('+i+');" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>' +
                        '</tr>');
                    $("#task").val('');
                    $("#norm").val('');
                    $("#number").val('');
                    i += 1;
                    location.reload();
                }
            });
        });

        $("#formModuleEdit").submit(function() {
            col_tr = $("#tableTask tr").length-2;
            num_check = $(".test").length;
            row = [];
            result = [];

            for(i = 1; i < col_tr; i++) {
                row[i] = {'task': $('#'+i+' span[name~="task_'+i+'"]').text(),
                        'norm': $('#'+i+' span[name~="norm_'+i+'"]').text(),
                        'number': $('#'+i+' span[name~="number_'+i+'"]').text(),
                        'group_tasks': $('#'+i+' span[name~="group_tasks_'+i+'"]').text()
                };
            }

            for (var j = 0; j <= row.length; j++) {
                if (row[j] != null && row[j] != 'underfinded' && row[j] != '') {
                    result.push(row[j]);
                }
            }

            /*for (var k = 1; k <= num_check; k++) {
                if($('#check_'+k).prop('checked')) {
                    alert($('#check_'+k).val());
                }
            }*/

            $("#row").val(JSON.stringify(result));
            if ($("#row").val() == '') {
                alert('Error');
                return false;
            } else {
                return true;
            }
        });

        function delTasks(id) {
            $('#'+id).remove();
        }
    </script>
@endpush