@extends('layouts.cabinet')

@section('title')
    Справочник "Документы": Добавление нового документа
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="box" style="padding: 10px;">
        <form action="/manager/documents/add" method="post">
            {{ csrf_field() }}
            <div class="form-group" id="NameTask">
                <label class="control-label" for="isPrintForm">Самый первый документ </label>
                <input type="checkbox" name="isFirstDoc" id="isFirstDoc">
            </div>
            <div class="form-group" id="NameTask">
                <label class="control-label" for="isPrintForm">Нужен для всех проектов </label>
                <input type="checkbox" name="forAllProjects" id="forAllProjects">
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="inputNameDoc">Наименование</label>
                        <input type="text" class="form-control" name="name_doc" id="inputNameDoc">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="inputNameCity">Тип документа</label>
                        <select name="typeDoc" id="typeDoc" class="form-control">
                            @foreach($type as $t)
                            <option value="{{ $t->slug }}">{{ $t->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <input type="submit" value="Сохранить" class="btn btn-success">
                <a href="/manager/documents" class="btn btn-default">Отмена</a>
            </div>
        </form>
    </div>
@endsection

@section('main-menu')
    @include('Manager.main-menu')
@endsection

@push('scripts')
<script type="text/javascript">

</script>
@endpush