@extends('layouts.cabinet')

@section('title')
    Справочник "Документы"
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="col-md-6">
        @if(session('perm')['documents_ref.create'])
            <div style="padding-bottom: 20px;">
                <a href="/manager/documents/add" class="btn btn-primary">Добавить</a>
            </div>
        @endif
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                @foreach($types as $t)
                    @if ($loop->index+1 == 1)
                        <li class="active"><a href="#{{ $t['slug'] }}" data-toggle="tab">{{ $t['name'] }}</a></li>
                    @else
                        <li><a href="#{{ $t['slug'] }}" data-toggle="tab">{{ $t['name'] }}</a></li>
                    @endif
                @endforeach
            </ul>
            <div class="tab-content">
                @foreach($types as $t)
                    @if ($loop->index+1 == 1)
                        <div class="tab-pane active" id="{{ $t['slug'] }}">
                            <div class="box-body table-responsive no-padding">
                                <table class="table" id="sort">
                                    <thead>
                                    <tr>
                                        <th>№</th>
                                        <th>Наименование</th>
                                        <th>Действие</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($documents != '')
                                        @foreach($documents as $d)
                                            @if($d['type_id'] == $t['id'])
                                                <tr>
                                                    <td>{{ $loop->index+1 }}</td>
                                                    <td>{{ $d['name'] }}</td>
                                                    <td>
                                                        @if(session('perm')['documents_ref.update'])
                                                            <a href="/manager/documents/edit/{{ $d['id'] }}" class="btn btn-primary">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                        @else
                                                            <a href="javascript: void(0)" class="btn btn-primary" disabled>
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                        @endif
                                                        @if(session('perm')['documents_ref.delete'])
                                                            <a href="/manager/documents/del/{{ $d['id'] }}" class="btn btn-danger">
                                                                <i class="fa fa-trash"></i>
                                                            </a>
                                                        @else
                                                            <a href="javascript: void(0)" class="btn btn-danger" disabled>
                                                                <i class="fa fa-trash"></i>
                                                            </a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="3">Документы не найдены</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                    @else
                        <div class="tab-pane" id="{{ $t['slug'] }}">
                            <div class="box-body table-responsive no-padding">
                                <table class="table" id="sort">
                                    <thead>
                                    <tr>
                                        <th>№</th>
                                        <th>Наименование</th>
                                        <th>Действие</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($documents != '')
                                        @foreach($documents as $d)
                                            @if($d['type_id'] == $t['id'])
                                                <tr>
                                                    <td>{{ $loop->index+1 }}</td>
                                                    <td>{{ $d['name'] }}</td>
                                                    <td>
                                                        @if(session('perm')['documents_ref.update'])
                                                            <a href="/manager/documents/edit/{{ $d['id'] }}" class="btn btn-primary">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                        @else
                                                            <a href="javascript: void(0)" class="btn btn-primary" disabled>
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                        @endif
                                                        @if(session('perm')['documents_ref.delete'])
                                                            <a href="/manager/documents/del/{{ $d['id'] }}" class="btn btn-danger">
                                                                <i class="fa fa-trash"></i>
                                                            </a>
                                                        @else
                                                            <a href="javascript: void(0)" class="btn btn-danger" disabled>
                                                                <i class="fa fa-trash"></i>
                                                            </a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="3">Документы не найдены</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                    @endif
                @endforeach
            </div>
            <!-- /.tab-content -->
        </div>
    </div>
@endsection

@section('main-menu')
    @include('Manager.main-menu')
@endsection

@push('scripts')
<script type="text/javascript">

</script>
@endpush