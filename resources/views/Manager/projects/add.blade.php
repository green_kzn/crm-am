@extends('layouts.cabinet')

@section('title')
    Мои проекты: Добавление нового проекта
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="box" style="padding: 10px;">
        <form action="/manager/projects/add" method="post" id="formAddProject">
            {{ csrf_field() }}
            <input type="hidden" name="contacts" id="contacts">
            <input type="hidden" name="row_modules" id="row_modules">
            <input type="hidden" name="module_rule" id="module_rule">
            <input type="hidden" name="data" id="data">
            <input type="hidden" name="documents" id="documents">
            <input type="hidden" name="check_list" id="check_list">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-10">
                        <label for="projectName">Наименование клиента</label>
                        <select name="client" id="clientList" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true">
                            @foreach($clients as $c)
                                <option value="{{ $c->id }}">{{ $c->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <a href="/manager/projects/clients/add" class="btn btn-success" style="margin-top: 25px;"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="projectName">Название проекта</label>
                <input type="text" class="form-control" name="projectName" required>
            </div>
            <div class="box">
                <table class="table">
                    <tbody>
                    <tr>
                        <td colspan="4">
                            <div class="form-group" style="margin-top: 10px;">
                                <input type="checkbox" name="optionsRadios" id="optionsRadios1">
                                <label for="optionsRadios1">
                                    <h4 style="color: #3c8dbc;">Клиент выслал комплект наших документов нам обратно?</h4>
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <div class="form-group" style="margin-top: 10px;">
                                <input type="radio" name="transComp" id="PostOfRussia" value="PostOfRussia" disabled>
                                <label for="PostOfRussia">
                                    <h4 style="color: #3c8dbc;">Почта России</h4>
                                </label>
                            </div>
                        </td>
                        <td >
                            <div class="form-group" style="margin-top: 10px;">
                                <input type="radio" name="transComp" id="otherCompany" value="otherCompany" disabled>
                                <label for="otherCompany">
                                    <h4 style="color: #3c8dbc;">Другая компания</h4>
                                </label>
                            </div>
                        </td>
                        <td style="vertical-align: middle;">
                            <label>Название компании</label>
                        </td>
                        <td style="vertical-align: middle;">
                            <div class="form-group">
                                <input type="text" class="form-control" name="companyName" disabled>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <label>Идентификатор письма</label>
                        </td>
                        <td>
                            <div class="form-group">
                                <input type="text" class="form-control" name="MessageId" disabled>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Контакты</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table table-striped" id="tableContacts">
                        <tbody><tr>
                            <th>Фамилия</th>
                            <th>Имя</th>
                            <th>Отчество</th>
                            <th>Должность</th>
                            <th>Телефон</th>
                            <th>E-mail</th>
                            <th>Ответственный за внедрение</th>
                            <th>Отправлять отчет на почту</th>
                            <th colspan="2">Действия</th>
                        </tr>
                        <tr id="contact_row"></tr>
                        <tr>
                            <td><input id="contact_surname" type="text" class="form-control"></td>
                            <td><input id="contact_name" type="text" class="form-control"></td>
                            <td><input id="contact_patronymic" type="text" class="form-control"></td>
                            <td>
                                <select class="form-control" id="contact_post">
                                    @foreach($posts as $p)
                                        <option id="{{ $p->id }}" value="{{ $p->name }}">{{ $p->name }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td><input id="contact_phone" type="text" class="form-control"></td>
                            <td><input id="contact_email" type="text" class="form-control"></td>
                            <td></td>
                            <td><input type="checkbox" id="mail_rep"></td>
                            <td>
                                <a class="btn btn-primary" id="saveContact">
                                    <i class="fa fa-check"></i>
                                </a>
                            </td>
                        </tr></tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>

            <div class="box">
                <table>
                    <tr>
                        <td>
                            <div class="box-header">
                                <h3 class="box-title">
                                    Модули
                                </h3>
                            </div>
                        </td>
                        <td>
                            <select class="form-control" id="modules">
                                @foreach($modules as $m)
                                    <option id="{{ $m->id }}" value="{{ $m->name }}">{{ $m->name }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td style="padding-left: 10px;">
                            <input type="number" min="0" name="modules_num" id="modules_num" placeholder="Кол-во" value="1" class="form-control" required>
                        </td>
                        <td style="padding-left: 10px;">
                            <a id="addModule" class="btn btn-primary">Добавить</a>
                        </td>
                    </tr>
                </table>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table table-bordered table-striped" style="width: 50%" id="tableModule">
                        <thead>
                        <tr>
                            <th>Название</th>
                            <th>Кол-во модулей</th>
                            <th>Кол-во контактов</th>
                            <th>с учетом запаса</th>
                            <th>Кол-во форм/отчетов</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr id="modules_list"></tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div id="update-block" class="box" style="margin-top: 30px; display: none;">
                    <div class="box-body no-padding">
                        <table class="table table-striped" id="tableModuleTasks">
                            <thead>
                            <tr>
                                <th>Список задач</th>
                                <th>Норма кол-ва контактов по договору 1 контакт - 1 час</th>
                                <th>Кол - во разрешенных контактов</th>
                                <th>Группа задач</th>
                                <th>Категория формы (только для группы задач "Печатные формы/отчеты")</th>
                                <th colspan="2">Действия</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyTasks">
                            <tr id="task_list"></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        Документы по проекту
                    </h3>
                </div>

                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="alert alert-danger alert-dismissible" id="error" style="display: none;">
                        <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>-->
                        <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                        <span id="error-text"></span>
                    </div>
                    <table class="table table-bordered table-striped" style="width: 50%" id="tableModule">
                        <thead>
                        <tr>
                            <th>Тип документа</th>
                            <th>Документ</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($documents as $d)
                            <tr id="doc_{{ $loop->index+1 }}" class="documents">
                                <td class="doc_isFirst" style="display: none;">{{ $d['isFirst'] }}</td>
                                <td class="doc_type">{{ $d['type'] }}</td>
                                <td class="doc_name">{{ $d['name'] }}</td>
                                <td>
                                    <a class="btn btn-danger" onclick="delDocuments({{ $loop->index+1 }});"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        Чек - лист
                    </h3>
                </div>

                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="alert alert-danger alert-dismissible" id="error" style="display: none;">
                        <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>-->
                        <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                        <span id="error-text"></span>
                    </div>
                    <table class="table table-bordered table-striped" id="tableModule">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Вопрос</th>
                            <th>Ответ</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($check_question) > 0)
                            @foreach($check_question as $cq)
                                <tr id="check_answer_{{ $loop->index+1 }}" class="check_list">
                                    <td>{{ $loop->index+1 }}</td>
                                    <td>{{ $cq['question'] }}</td>
                                    <td><input type="text" id="{{ $cq['id'] }}" name="check_answer[]" class="form-control"></td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="3"><i>Вопросы отсутствуют</i></td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>

            <div class="form-group">
                <input type="submit" value="Сохранить" class="btn btn-success">
                <a href="/manager/projects" class="btn btn-default">Отмена</a>
            </div>
        </form>
    </div>
@endsection

@section('main-menu')
    @include('Manager.main-menu')
@endsection

@push('scripts')
    <script>
        $("#formAddProject").validate({
            rules: {
                "check_answer[]": "required"
            },
            messages: {
                projectName: {
                    required: "Это поле обязательно к заполнению",
                    minlength: "Введите не менее 2-х символов"
                },
                modules_num: {
                    required: "Это поле обязательно к заполнению"
                },
                email: {
                    required: "Поле 'Email' обязательно к заполнению",
                    email: "Необходим формат адреса email"
                },
                url: "Поле 'Сайт' обязательно к заполнению"
            }
        });
        var res = [];
        var res2 = {};
        var data = [];
        var obj = {};
        var doc = {};
        var doc_ar = [];

        var task_mod_ar = [];
        var task_mod = {};

        var mod = {};
        var mod_ar = [];

        $(document).ready(function () {
            doc_length = $(".documents").length;
            for(k=1;k<=doc_length;k++) {
                type = $("tr#doc_"+k+" .doc_type").html();
                doc_name = $("tr#doc_"+k+" .doc_name").html();
                is_first = $("tr#doc_"+k+" .doc_isFirst").html();

                doc = {};
                doc = {
                    'id': k,
                    'name': doc_name,
                    'type': type,
                    'isFirst': is_first
                };
                doc_ar.push(doc);
            }

            $("select[name~='user']").change(function () {
                user_id = $("select[name~='user'] option:selected").val();
                user_name = $("select[name~='user'] option:selected").html();

                $.ajax({
                    url: "/manager/user/getUserProjects",
                    data: {'id': user_id},
                    method: "POST",
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function () {
                        $("#workload").html('Загрузка ...');
                    },
                    success: function (resp) {
                        contentWorkload = "<table>" +
                            "<tr>" +
                            "<td>Внедренец:&nbsp;&nbsp;<td>" +
                            "<td>"+user_name+"<td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Активных проектов:&nbsp;&nbsp;<td>" +
                            "<td>"+resp.projects+"<td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Общее время задач:&nbsp;&nbsp;<td>" +
                            "<td>"+resp.tasks+"<td>" +
                            "</tr>" +
                            "</table>";
                        $("#workload").html(contentWorkload);
                    }
                });
            });

            $("#saveContact").click(function () {
                newContact();
            });

            $("#optionsRadios1").click(function () {
                if ($(this).prop('checked')) {
                    $("#PostOfRussia").removeAttr('disabled');
                    $("#otherCompany").removeAttr('disabled');
                    $("input[name~='MessageId']").removeAttr('disabled');
                    $("input[name~='companyName']").removeAttr('disabled');
                } else {
                    $("#PostOfRussia").attr('disabled', 'true');
                    $("#otherCompany").attr('disabled', 'true');
                    $("input[name~='MessageId']").attr('disabled', 'true');
                    $("input[name~='companyName']").attr('disabled', 'true');
                }
            });

            $("input[name~='transComp']").on('change', function() {
                company = $("input[name~='transComp']:checked").val();

                if (company == 'otherCompany') {
                    $("input[name~='companyName']").removeAttr('disabled');
                } else {
                    $("input[name~='companyName']").attr('disabled', 'true');
                    res2 = {};
                    res2.companyName = company;
                }
            });

            getClients();

            /*$('#clientList').change(function () {
                $(".contacts").remove();
                getClients();
            });*/

            $("#addModule").click(function () {
                num = $("#modules_num").val();
                id = $('#modules option:selected').attr('id');
                i = 0;

                if (!num) {
                    num = 1;
                }

                $.ajax({
                    url: '/manager/module-rule/getRule',
                    method: 'POST',
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {'id': id},
                    success: function (resp) {
                        i = $(".row_index").length;
                        id = $("#modules option:checked").attr('id');
                        $("#modules_list").before('<tr class="row_index" id="row_index_'+(i+1)+'">' +
                            '<td style="display: none;"><span class="module_id">'+id+'</span></td>' +
                            '<td>'+$("#modules").val()+'</td>' +
                            '<td><span class="module_num">'+num+'</span></td>' +
                            '<td><span class="module_norm">'+resp.norm_contacts+'</span></td>' +
                            '<td><span class="module_number">'+resp.number_contacts+'</span></td>' +
                            '<td><span class="module_form">'+resp.ps_form+'</span></td>' +
                            '<td><a class="btn btn-primary" onclick="update_norm('+id+');">Уточнить нормы</a></td>' +
                            '<td><a class="btn btn-danger" onclick="del_norm('+(i+1)+');"><i class="fa fa-trash"></i></a></td>' +
                            '</tr>');
                        i++;
                    },
                    error: function () {
                        alert("Ошибка заполнения справочника");
                    }
                });
            });
        });

        function del_norm(id) {
            $("#row_index_"+id).remove();
        }

        function getClients() {
            client_id = $('#clientList option:selected').attr('value');
            $.ajax({
                url: '/manager/contact/get-contact',
                async: false,
                data: {'id': client_id},
                success: function (resp) {
                    for(i=0; i<resp.length; i++) {
                        content = '';
                        content = '<tr id="'+(i+1)+'" class="contacts">' +
                            '<td style="display: none;"><span name="contacts_id">'+resp[i].id+'</span></td>' +
                            '<td><span name="contacts_surname">'+resp[i].last_name+'</span></td>' +
                            '<td><span name="contacts_name">'+resp[i].first_name+'</span></td>' +
                            '<td><span name="contacts_patronymic">'+resp[i].patronymic+'</span></td>' +
                            '<td><span name="contacts_post">'+resp[i].post+'</span></td>' +
                            '<td><span name="contacts_phone">'+resp[i].phone+'</span></td>' +
                            '<td><span name="contacts_email">'+resp[i].email+'</span></td>' +
                            '<td><input type="radio" name="main"></td>' +
                            '<td><input type="checkbox" name="mail_rep"></td>' +
                            '<td><a class="btn btn-danger" onclick="delContact('+(i+1)+')"><i class="fa fa-trash"></i></a></td>' +
                            '</tr>';
                        $("#contact_row").before(content);
                    }
                }
            });
        }

        function delContact(id) {
            $('#'+id).remove();
        }

        function search(nameKey, myArray){
            for (var i=0; i < myArray.length; i++) {
                if (myArray[i].id === nameKey) {
                    return i;
                }
            }
            return -1;
        }

        function update_norm(id) {
            display = $("#update-block").css('display');
            flag = 0;
            i = 0;
            console.log(task_mod_ar);
            if (display == 'none') {
                $("#tbodyTasks").text('');
                $("#tbodyTasks").append('<tr id="task_list"></tr>');
                task_mod = '';
                $.ajax({
                    'url': '/manager/module-rule/getTasks',
                    'data': {'id': id},
                    success: function (resp) {
                        if (task_mod_ar.length > 0) {
                            for (i=0;i<resp.length;i++) {
                                ind = search(resp[i].id, task_mod_ar);
                                if (ind != -1) {
                                    if (task_mod_ar[ind].deleted == 0) {
                                        content = '<tr class="row_index" id="'+ind+'">' +
                                            '<td style="display: none;" id="task_id"><span>'+task_mod_ar[ind].id+'</span></td>' +
                                            '<td id="task_'+ind+'"><span>'+task_mod_ar[ind].task+'</span></td>' +
                                            '<td id="norm_'+ind+'"><span>'+task_mod_ar[ind].norm_contacts+'</span></td>' +
                                            '<td id="number_'+ind+'"><span>'+task_mod_ar[ind].number_contacts+'</span></td>' +
                                            '<td id="group_tasks_'+ind+'"><span>'+task_mod_ar[ind].grouptasks+'</span></td>' +
                                            '<td id="categoryPrintForm_'+ind+'"></td>' +
                                            '<td id="edit_rule_'+ind+'"><a class="btn btn-primary" onclick="editNorm('+task_mod_ar[ind].id+', '+ind+');"><i class="fa fa-pencil"></i></a></td>' +
                                            '<td><a class="btn btn-danger" onclick="delNorm('+ind+');"><i class="fa fa-trash"></i></a></td>' +
                                            '</tr>';
                                        $("#task_list").before(content);
                                    }
                                } else {
                                    task_mod = {
                                        'id': resp[i].id,
                                        'task': resp[i].task,
                                        'norm_contacts': resp[i].norm_contacts,
                                        'number_contacts': resp[i].number_contacts,
                                        'grouptasks': resp[i].grouptasks,
                                        'deleted': 0
                                    };
                                    task_mod_ar.push(task_mod);

                                    content = '<tr class="row_index" id="'+i+'">' +
                                        '<td style="display: none;" id="task_id"><span>'+resp[i].id+'</span></td>' +
                                        '<td id="task_'+i+'"><span>'+resp[i].task+'</span></td>' +
                                        '<td id="norm_'+i+'"><span>'+resp[i].norm_contacts+'</span></td>' +
                                        '<td id="number_'+i+'"><span>'+resp[i].number_contacts+'</span></td>' +
                                        '<td id="group_tasks_'+i+'"><span>'+resp[i].grouptasks+'</span></td>' +
                                        '<td id="categoryPrintForm_'+i+'"></td>' +
                                        '<td id="edit_rule_'+i+'"><a class="btn btn-primary" onclick="editNorm('+resp[i].id+', '+i+');"><i class="fa fa-pencil"></i></a></td>' +
                                        '<td><a class="btn btn-danger" onclick="delNorm('+i+');"><i class="fa fa-trash"></i></a></td>' +
                                        '</tr>';
                                    $("#task_list").before(content);
                                }
                            }
                        } else {
                            console.log(resp);
                            for (i=0;i<resp.length;i++) {
                                task_mod = {
                                    'id': resp[i].id,
                                    'task': resp[i].task,
                                    'norm_contacts': resp[i].norm_contacts,
                                    'number_contacts': resp[i].number_contacts,
                                    'grouptasks': resp[i].grouptasks,
                                    'deleted': 0
                                };
                                task_mod_ar.push(task_mod);

                                content = '<tr class="row_index" id="'+i+'">' +
                                    '<td style="display: none;" id="task_id"><span>'+resp[i].id+'</span></td>' +
                                    '<td id="task_'+i+'"><span>'+resp[i].task+'</span></td>' +
                                    '<td id="norm_'+i+'"><span>'+resp[i].norm_contacts+'</span></td>' +
                                    '<td id="number_'+i+'"><span>'+resp[i].number_contacts+'</span></td>' +
                                    '<td id="group_tasks_'+i+'"><span>'+resp[i].grouptasks+'</span></td>' +
                                    '<td id="categoryPrintForm_'+i+'"></td>' +
                                    '<td id="edit_rule_'+i+'"><a class="btn btn-primary" onclick="editNorm('+resp[i].id+', '+i+');"><i class="fa fa-pencil"></i></a></td>' +
                                    '<td><a class="btn btn-danger" onclick="delNorm('+i+');"><i class="fa fa-trash"></i></a></td>' +
                                    '</tr>';
                                $("#task_list").before(content);
                            }
                        }
                    }
                });
                $.ajax({
                    'url': '/manager/group-tasks/get-task-groups',
                    success: function (resp) {
                        content2 = '<tr id="addTaskRow">' +
                            '<td><input id="taskName" type="text" class="form-control"></td>' +
                            '<td><input id="taskNorm" type="number" class="form-control"></td>' +
                            '<td><input id="taskCol" type="number" class="form-control"></td>' +
                            '<td><select id="taskGroup" onchange="getCategoryForm()" class="form-control">';
                        for (l=0; l<resp.length; l++) {
                            content2 += '<option value="'+resp[l].id+'">'+resp[l].name+'</option>';
                        }
                        content2 += '</select></td>' +
                            '<td id="categoryPrintForm"></td>' +
                            '<td><a onclick="addRowNorm();" class="btn btn-primary"><i class="fa fa-check"></a></td>' +
                            '</tr>';
                        $("#task_list").after(content2);
                    }
                });
            }
            $( "#update-block" ).slideToggle( "fast");
        }

        function delNorm(id) {
            console.log(task_mod_ar);
            task_mod_ar[id].deleted = 1;
            console.log(task_mod_ar);
            $("#tableModuleTasks #"+id).remove();
        }

        function getCategoryForm() {
            task = $("#taskGroup option:selected").val();
            content = '';
            if (task == 11) {
                $.ajax({
                    url: '/manager/group-tasks/get-category-printform',
                    success: function (resp) {
                        console.log(resp);
                        content = '<select class="form-control" id="category">';
                        for (i=0;i<resp.length;i++) {
                            content += '<option id="'+resp[i].id+'" value="'+resp[i].id+'">'+resp[i].category+'</option>';
                        }
                        content += '</select>';
                        $("#categoryPrintForm").html(content);
                    }
                });
            } else {
                $("#categoryPrintForm").html('');
            }
        }

        function getCategoryForm2(id) {
            task = $("#inputGroupTask option:selected").val();
            content = '';
            if (task == 11) {
                $.ajax({
                    url: '/manager/group-tasks/get-category-printform',
                    success: function (resp) {
                        console.log(resp);
                        content = '<select class="form-control" id="category">';
                        for (i=0;i<resp.length;i++) {
                            content += '<option id="'+resp[i].id+'" value="'+resp[i].id+'">'+resp[i].category+'</option>';
                        }
                        content += '</select>';
                        $("#categoryPrintForm_"+id).html(content);
                    }
                });
            } else {
                $("#categoryPrintForm_"+id).html('');
            }
        }

        function addRowNorm() {
            id = $(".row_index").length;
            name = $("#addTaskRow #taskName").val();
            norm = $("#addTaskRow #taskNorm").val();
            col = $("#addTaskRow #taskCol").val();
            norm_form = $("#addTaskRow #category option:selected").text();
            g_task = $("#addTaskRow #taskGroup option:selected").text();
            content = '<tr class="row_index" id="'+id+'">' +
                '<td style="display: none;" id="task_id"><span>0</span></td>' +
                '<td id="task_'+id+'"><span>'+name+'</span></td>' +
                '<td id="norm_'+id+'"><span>'+norm+'</span></td>' +
                '<td id="number_'+id+'"><span>'+col+'</span></td>' +
                '<td id="group_tasks_'+id+'"><span>'+g_task+'</span></td>' +
                '<td id="categoryPrintForm_'+id+'"><span>'+norm_form+'</span></td>' +
                '<td id="edit_rule_'+id+'"><a class="btn btn-primary" onclick="editNorm(0, '+id+');"><i class="fa fa-pencil"></i></a></td><td><a onclick="delNorm('+id+')" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>' +
                '</tr>';
            $('#task_list').before(content);
        }

        function editNorm(id, loop) {
            $.ajax({
                'url': '/manager/group-tasks/get-task-groups',
                success: function (resp) {
                    task = $("#task_"+loop+' > span').text();
                    norm = $("#norm_"+loop+' > span').text();
                    number = $("#number_"+loop+' > span').text();
                    group_tasks = $("#group_tasks_"+loop+' > span').text();

                    group_tasks_content2 = '<select id="inputGroupTask" onchange="getCategoryForm2('+loop+')" class="form-control">';
                    for (m=0; m<resp.length; m++) {
                        if (resp[m].name == group_tasks) {
                            group_tasks_content2 += '<option value="'+resp[m].id+'" selected>'+resp[m].name+'</option>';
                        } else {
                            group_tasks_content2 += '<option value="'+resp[m].id+'">'+resp[m].name+'</option>';
                        }
                    }
                    group_tasks_content2 += '</select>';

                    $("#task_"+loop).html('<input id="inputTask" type="text" class="form-control" value="'+task+'">');
                    $("#norm_"+loop).html('<input id="inputNorm" type="text" class="form-control" value="'+norm+'">');
                    $("#number_"+loop).html('<input id="inputNumber" type="text" class="form-control" value="'+number+'">');
                    $("#group_tasks_"+loop).html(group_tasks_content2);
                    $("#group_tasks_"+loop).html();
                    $("#edit_rule_"+loop).html('<a onclick="saveRule('+id+', '+loop+');" class="btn btn-success"><i class="fa fa-check"></i></a>');
                }
            });
        }

        function saveRule(id, loop) {
            task = $("#inputTask").val();
            norm = $("#inputNorm").val();
            number = $("#inputNumber").val();
            group_tasks = $("#inputGroupTask option:selected").text();
            category_norm = $("#category option:selected").text();
            module_id = $("#module_id").val();

            obj = {
                'id': id,
                'task': task,
                'norm': norm,
                'number': number,
                'group_tasks': group_tasks
            };
            res.push(obj);
            for (k=0;k<task_mod_ar.length;k++) {
                if (task_mod_ar[k].id == id) {
                    task_mod_ar[k] = {
                        'deleted': 0,
                        'grouptasks': group_tasks,
                        'id': id,
                        'norm_contacts': norm,
                        'number_contacts': number,
                        'task': task
                    };
                }
            }
            console.log(task_mod_ar);

            count_norm = 0;
            for (l=0;l<task_mod_ar.length;l++) {
                count_norm += Number(task_mod_ar[l].norm_contacts);
                //count_number += Number(task_mod_ar[l].norm_contacts);
            }

            $(".module_norm").html(count_norm);
            //$(".module_number").html(count_number);

            $("#task_"+loop).html('<span name="task_'+loop+'">'+task+'</span>');
            $("#norm_"+loop).html('<span name="norm_'+loop+'">'+norm+'</span>');
            $("#number_"+loop).html('<span name="number_'+loop+'">'+number+'</span>');
            $("#group_tasks_"+loop).html('<span name="group_tasks_'+loop+'">'+group_tasks+'</span>');
            $("#categoryPrintForm_"+loop).html('<span name="categoryPrintForm_'+loop+'">'+category_norm+'</span>');
            $("#edit_rule_"+loop).html('<a onclick="editNorm('+id+', '+loop+');" class="btn btn-primary"><i class="fa fa-pencil"></i></a>');
        }

        function newContact() {
            name = $("#contact_name").val();
            surname = $("#contact_surname").val();
            patronymic = $("#contact_patronymic").val();
            post = $("#contact_post").val();
            phone = $("#contact_phone").val();
            email = $("#contact_email").val();
            mail_rep = $("#mail_rep").prop('checked');
            er = 0;
            i = $("#tableContacts tr").length-2;
            var contact = [];

            if (!surname) {
                surname = '';
            }

            if (!patronymic) {
                patronymic = '';
            }

            if (!email) {
                email = '';
            }

            if (!name) {
                er++;
                alert('Введите имя контакта');
            }

            if (!post) {
                er++;
                alert('Введите должность контакта');
            }

            if (!phone) {
                er++;
                alert('Введите номер телефона контакта');
            }

            if (er == 0) {
                if(mail_rep == true) {
                    $("#contact_row").before('<tr id="'+i+'" class="contacts">' +
                        '<td><span name="contacts_surname">'+surname+'</span></td>' +
                        '<td><span name="contacts_name">'+name+'</span></td>' +
                        '<td><span name="contacts_patronymic">'+patronymic+'</span></td>' +
                        '<td><span name="contacts_post">'+post+'</span></td>' +
                        '<td><span name="contacts_phone">'+phone+'</span></td>' +
                        '<td><span name="contacts_email">'+email+'</span></td>' +
                        '<td><input type="radio" name="main"></td>' +
                        '<td><input type="checkbox" name="mail_rep" checked></td>' +
                        '<td><a class="btn btn-danger" onclick="delContact('+i+')"><i class="fa fa-trash"></i></a></td>' +
                        '</tr>');
                } else {
                    $("#contact_row").before('<tr id="'+i+'" class="contacts">' +
                        '<td><span name="contacts_surname">'+surname+'</span></td>' +
                        '<td><span name="contacts_name">'+name+'</span></td>' +
                        '<td><span name="contacts_patronymic">'+patronymic+'</span></td>' +
                        '<td><span name="contacts_post">'+post+'</span></td>' +
                        '<td><span name="contacts_phone">'+phone+'</span></td>' +
                        '<td><span name="contacts_email">'+email+'</span></td>' +
                        '<td><input type="radio" name="main"></td>' +
                        '<td><input type="checkbox" name="mail_rep"></td>' +
                        '<td><a class="btn btn-danger" onclick="delContact('+i+')"><i class="fa fa-trash"></i></a></td>' +
                        '</tr>');
                }

                i++;



                $("#contact_name").val('');
                $("#contact_surname").val('');
                $("#contact_patronymic").val('');
                $("#contact_post").val('');
                $("#contact_phone").val('');
                $("#contact_email").val('');
            }
        }

        function bouncer(arr) {
            return arr.filter( function(v){return !(v !== v);});
        }

        $("#formAddProject").submit(function () {
            rowContacts = $("#tableContacts tr").length-3;
            rowModules = $("#tableModule .row_index").length;
            check = $("#optionsRadios1").prop('checked');
            company = $("input[name~='transComp']:checked").val();
            var contact = [];
            var modules = [];
            var check_list = [];

            for(i = 1; i <= rowContacts; i++) {
                cont_id = $('#'+i+' > td > span[name~="contacts_id"]').text();
                first_name = $('#'+i+' > td > span[name~="contacts_name"]').text();
                last_name = $('#'+i+' > td > span[name~="contacts_surname"]').text();
                patronymic = $('#'+i+' > td > span[name~="contacts_patronymic"]').text();
                post = $('#'+i+' > td > span[name~="contacts_post"]').text();
                phone = $('#'+i+' > td > span[name~="contacts_phone"]').text();
                email = $('#'+i+' > td > span[name~="contacts_email"]').text();
                main = $('#'+i+' > td > input[name~="main"] ').prop("checked");
                email_rep = $('#'+i+' > td > input[name~="mail_rep"] ').prop("checked");

                contact.push({
                    'id': cont_id,
                    'first_name': first_name,
                    'last_name': last_name,
                    'patronymic': patronymic,
                    'post': post,
                    'phone': phone,
                    'email': email,
                    'main': main,
                    'email_rep': email_rep
                });
            }

            for (j = 1; j <= rowModules; j++) {
                mod_id = $('#tableModule #row_index_'+j+' .module_id').text();
                num = $('#tableModule #row_index_'+j+' .module_num').text();

                modules.push({
                    'id': mod_id,
                    'num': num
                });
            }

            const_check_list = $(".check_list").length;
            for (k=1;k<=const_check_list;k++) {
                check_id = $("#check_answer_"+k).find('input').attr('id');
                check_answer = $("#check_answer_"+k).find('input').val();
                check_list.push({
                    'id': check_id,
                    'answer': check_answer
                });
            }

            res2 = {};
            res2.check = check;

            if (check) {
                if (company == 'otherCompany') {
                    companyName = $("input[name~='companyName']").val();
                    res2.companyName = companyName;
                } else {
                    res2.companyName = company;
                }
                messageId = $("input[name~='MessageId']").val();
                res2.messageId = messageId;
            }

            res = task_mod_ar;
            data = [];
            data.push(res);

            $("#contacts").val(JSON.stringify(contact));
            $("#row_modules").val(JSON.stringify(modules));
            $("#module_rule").val(JSON.stringify(res));
            $("#data").val(JSON.stringify(data));
            $("#documents").val(JSON.stringify(doc_ar));
            $("#check_list").val(JSON.stringify(check_list));

            return true;
        });

        function delDocuments(doc_id) {
            $("#error").css('display', 'none');
            for (i=0;i<doc_ar.length;i++) {
                if (doc_ar[i].id == doc_id) {
                    if (doc_ar[i].isFirst != 1) {
                        doc_ar.splice(i, 1);
                        $("tr#doc_"+doc_id).remove();
                    } else {
                        $("#error-text").html('Этот документ является первым. Его нельзя удалять');
                        $("#error").css('display', 'block');
                        break;
                    }
                }
            }
        }

        function showWorkload() {
            $("#workload").slideToggle("fast", function () {
                if ($("#workload").css('display') != 'none') {
                    user_id = $("select[name~='user'] option:selected").val();
                    user_name = $("select[name~='user'] option:selected").html();

                    $.ajax({
                        url: "/manager/user/getUserProjects",
                        data: {'id': user_id},
                        method: "POST",
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        },
                        beforeSend: function () {
                            $("#workload").html('Загрузка ...');
                        },
                        success: function (resp) {
                            contentWorkload = "<table>" +
                                "<tr>" +
                                "<td>Внедренец:&nbsp;&nbsp;<td>" +
                                "<td>"+user_name+"<td>" +
                                "</tr>" +
                                "<tr>" +
                                "<td>Активных проектов:&nbsp;&nbsp;<td>" +
                                "<td>"+resp.projects+"<td>" +
                                "</tr>" +
                                "<tr>" +
                                "<td>Общее время задач:&nbsp;&nbsp;<td>" +
                                "<td>"+resp.tasks+"<td>" +
                                "</tr>" +
                                "</table>";
                            $("#workload").html(contentWorkload);
                        }
                    });
                }
            });
        }
    </script>
@endpush