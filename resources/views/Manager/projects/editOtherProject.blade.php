@extends('layouts.cabinet')

@section('title')
    Взять обратную связь
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="col-md-6">
        <div class="box box-primary" style="padding: 10px;">
            <div class="box-header with-border">
                <h3 class="box-title">Общая информация</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body" style="">
                <table>
                    <tr>
                        <td style="padding-right: 10px; font-weight: bold;">Клиент</td>
                        <td>{{ $client }}</td>
                    </tr>
                    <tr>
                        <td style="padding-right: 10px; font-weight: bold;">Проект</td>
                        <td>{{ $project }}</td>
                    </tr>
                    @if ($main_contact)
                        <tr>
                            <td style="padding-right: 10px; font-weight: bold;">Контактное лицо</td>
                            <td></td>
                        </tr>
                    @endif
                    <tr>
                        <td style="padding-right: 10px; font-weight: bold;">Специалист по внедрению</td>
                        <td>{{ $user }}</td>
                    </tr>
                    <tr>
                        <td style="padding-right: 10px; font-weight: bold;">Тип</td>
                        @if($type == 'mail')
                        <td>Почта</td>
                        @else
                        <td>Телефон</td>
                        @endif
                    </tr>
                    <tr>
                        <td style="padding-right: 10px; font-weight: bold;">Комментарий</td>
                        <td>{{ $comment }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary" style="padding: 10px;">
            <div class="box-header with-border">
                <h3 class="box-title">История коментариев</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body" style="overflow-y: auto;height: 200px;">

            </div>
        </div>
    </div>
    <div style="clear: both;"></div>
    <div class="col-md-12">
        <div style="margin-bottom: 10px; font-weight: bold; text-decoration: underline;">
            <a href="/manager/message/{{ $other_task->message_id }}" target="_blank">Открыть письмо</a><br>
        </div>
        <div style="padding-bottom: 20px;">
            <a id="showResult" class="btn btn-primary">Результат <i class="fa fa-angle-down"></i></a>
        </div>
        <div class="clear"></div>
        <div class="wall_form" id="popup_message_form" style="display:none; margin-bottom: 20px;">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Результат</h3>
                </div>
                <div class="content">
                    <form action="/manager/other/edit/{{ $other_task->id }}" method="post" id="resultContact">
                        {{ csrf_field() }}
                        <div class="form-group" style="margin-top: 10px;">
                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="no">
                            <label for="optionsRadios1">
                                <h4 style="color: #3c8dbc;">Недозвон</h4>
                            </label>
                        </div>
                        <div class="form-group" style="margin-top: 10px;">
                            <input type="radio" name="optionsRadios" id="optionsRadios2" value="yes">
                            <label for="optionsRadios2">
                                <h4 style="color: #3c8dbc;">Позвонили</h4>
                            </label>
                        </div>

                        <div class="form-group">
                            <h4 style="color: #3c8dbc;">*Комментарий</h4>
                        </div>
                        <div id="editor"></div>
                        <textarea name="comment" style="display: none;" cols="30" rows="10"></textarea>
                        
                        <div class="form-group" style="margin-top: 10px;">
                            <input type="submit" value="Сохранить" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main-menu')
    @include('Manager.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#showResult").click(function () {
                $("#popup_message_form").slideToggle('fast');
            });
            
            $("#resultContact").submit(function () {
                comment = CKEDITOR.instances.editor.getData();
                $('textarea[name~="comment"]').html(comment);

                return true;
            });
        });
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor');
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5();
        });
    </script>
@endpush