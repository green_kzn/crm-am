@extends('layouts.cabinet')

@section('title')
    Мои проекты
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Проекты</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="activity">

                <div class="box">
                    <div class="box-header">
                        <a href="" style="padding-right: 15px;">Спящие</a>
                        <a href="" style="padding-right: 15px;">Активные</a>
                        <a href="" style="padding-right: 15px;">Сегодня</a>
                        <a href="" style="padding-right: 15px;">Горящие</a>

                        <div class="box-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                        @if(session('perm')['project.create'])
                            <a href="/manager/projects/add" class="btn btn-primary">Добавить проект</a>
                        @endif
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Название клиники</th>
                                    <th>Название проекта</th>
                                    <th>Предыдущий контакт</th>
                                    <th>Следующий контакт</th>
                                    <th>Город</th>
                                    <th>Готовность</th>
                                    <th>Активность</th>
                                    <th>Дата начала</th>
                                    <th>Планируемая дата окончания</th>
                                    <th>Действия</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($projects)
                                    @foreach($projects as $p)
                                    <tr>
                                        <td style="vertical-align: middle;">{{ $p['client_name'] }}</td>
                                        <td style="vertical-align: middle;">{{ $p['name'] }}</td>
                                        @if($p['prev_contact'])
                                            <td style="vertical-align: middle;">{{ $p['prev_contact'] }}</td>
                                        @else
                                            <td style="vertical-align: middle;">История контактов отсутствует</td>
                                        @endif
                                        @if($p['next_contact'])
                                            <td style="vertical-align: middle;">{{ $p['next_contact'] }}</td>
                                        @else
                                            <td style="vertical-align: middle;">История контактов отсутствует</td>
                                        @endif
                                        <td style="vertical-align: middle;">{{ $p['city'] }}</td>
                                        <td style="vertical-align: middle;">{{ $p['done'] }} %</td>
                                        <td style="vertical-align: middle;">{{ $p['active'] }} %</td>
                                        @if ($p['raw'] == 1)
                                        <td style="vertical-align: middle;" colspan="2">Проект еще не обработан</td>
                                        @else
                                        <td style="vertical-align: middle;">{{ $p['start_date'] }}</td>
                                        <td style="vertical-align: middle;">{{ $p['finish_date'] }}</td>
                                        @endif
                                        <td style="vertical-align: middle;">
                                            @if(session('perm')['project.update'])
                                                <a href="project/edit/{{ $p['id'] }}" class="btn btn-primary">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            @else
                                                <a href="javascript: void(0)" class="btn btn-primary" disabled="true">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="9" style="text-align: center;">Проекты не найдены</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            <!-- /.tab-pane -->
            </div>
        </div>
        <!-- /.tab-content -->
    </div>
@endsection

@section('main-menu')
    @include('Manager.main-menu')
@endsection

@push('scripts')

@endpush