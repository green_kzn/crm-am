@extends('layouts.cabinet')

@section('title')
    Взять обратную связь
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="col-md-6">
        <div class="box box-primary" style="padding: 10px;">
            <div class="box-header with-border">
                <h3 class="box-title">Общая информация</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body" style="">
                <table>
                    <tr>
                        <td style="padding-right: 10px; font-weight: bold;">Клиент</td>
                        <td>{{ $client->name }}</td>
                    </tr>
                    <tr>
                        <td style="padding-right: 10px; font-weight: bold;">Проект</td>
                        <td>{{ $project->name }}</td>
                    </tr>
                    @if ($main_contact)
                        <tr>
                            <td style="padding-right: 10px; font-weight: bold;">Контактное лицо</td>
                            <td> {{ $main_contact->last_name }} {{ $main_contact->first_name }} {{ $main_contact->patronymic }} / Тел. {{ $main_contact->phone }}</td>
                        </tr>
                    @endif
                    <tr>
                        <td style="padding-right: 10px; font-weight: bold;">Специалист по внедрению</td>
                        <td>{{ $user->last_name }} {{ $user->first_name }}</td>
                    </tr>
                    <tr>
                        <td style="padding-right: 10px; font-weight: bold;">Комментарий от директора</td>
                        <td>{{ $comment_close }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary" style="padding: 10px;">
            <div class="box-header with-border">
                <h3 class="box-title">История коментариев</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body" style="overflow-y: auto;height: 200px;">
                @if ($comments != 0)
                    @foreach($comments as $c)
                        <div class="post" style="padding-bottom: 5px;">
                            <div class="user-block" style="margin-bottom: 0px;">
                                <span class="username" style="margin-left: 0px; float: left;">
                                    <a href="#">{{ $c['user_name'] }} {{ $c['user_surname'] }}</a>
                                </span>
                                <span class="description" style="margin-left: 5px; font-size: 16px;">&nbsp;&nbsp;&nbsp;{{ $c['date'] }} в {{ $c['time'] }}</span>
                            </div>
                            <!-- /.user-block -->
                            <div id="comment_355">
                                {{ $c['text'] }}
                            </div>
                            <!--<ul class="list-inline">
                                <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Share</a></li>
                            </ul>

                            <input class="form-control input-sm" type="text" placeholder="Type a comment">-->
                        </div>
                    @endforeach
                @else
                    <p style="text-align: center; font-style: italic;">Коментарии отсутствуют</p>
                @endif
            </div>
        </div>
    </div>
    <div style="clear: both;"></div>
    <div class="col-md-12">
        <div style="margin-bottom: 10px; font-weight: bold; text-decoration: underline;">
            <a href="">Информация о проекте</a><br>
            <a href="">Скрипт для разговора</a>
        </div>
        <div style="padding-bottom: 20px;">
            <a id="showResult" class="btn btn-primary">Результат <i class="fa fa-angle-down"></i></a>
        </div>
        <div class="clear"></div>
        <div class="wall_form" id="popup_message_form" style="display:none; margin-bottom: 20px;">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Результат</h3>
                </div>
                <div class="content">
                    <form action="/manager/close/edit/{{ $project->id }}" method="post" id="resultContact">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="1">
                            <label for="optionsRadios1">
                                <h4 style="color: #3c8dbc;">Недозвон</h4>
                            </label>
                        </div>
                        <div class="form-group">
                            <input type="radio" name="optionsRadios" id="optionsRadios2" value="2">
                            <label for="optionsRadios2">
                                <h4 style="color: #3c8dbc;">Дали обратную связь</h4>
                            </label>
                        </div>
                        <div id="collapseTwo" class="disabledbutton" style="display: none;">
                            <div class="box-body">
                                <input type="hidden" name="close">
                                <table class="table table-bordered">
                                    <tr>
                                        <td style="width: 20%; vertical-align: middle;">Можно закрыть проект?</td>
                                        <td style="width: 10%;">
                                            <input type="radio" name="closeProject" id="yes" value="1">
                                            <label for="yes">
                                                <h4 style="color: #3c8dbc;">Да</h4>
                                            </label>
                                        </td>
                                        <td>
                                            <input type="radio" name="closeProject" id="no" value="2">
                                            <label for="no">
                                                <h4 style="color: #3c8dbc;">Нет</h4>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">Показатели</td>
                                    </tr>
                                    @foreach($close_question as $cq)
                                        <tr class="close-questions" id="{{ $cq->id }}">
                                            <td style="vertical-align: middle;">* {{ $cq->question }}</td>
                                            <td colspan="2" style="vertical-align: middle;">
                                                <input type="radio" name="rating_{{ $loop->index+1 }}" id="one{{ $loop->index+1 }}" value="1">
                                                <label for="one{{ $loop->index+1 }}">
                                                    <h4 style="color: #3c8dbc;">1</h4>
                                                </label>
                                                <input type="radio" name="rating_{{ $loop->index+1 }}" id="two{{ $loop->index+1 }}" value="2">
                                                <label for="two{{ $loop->index+1 }}">
                                                    <h4 style="color: #3c8dbc;">2</h4>
                                                </label>
                                                <input type="radio" name="rating_{{ $loop->index+1 }}" id="three{{ $loop->index+1 }}" value="3">
                                                <label for="three{{ $loop->index+1 }}">
                                                    <h4 style="color: #3c8dbc;">3</h4>
                                                </label>
                                                <input type="radio" name="rating_{{ $loop->index+1 }}" id="four{{ $loop->index+1 }}" value="4">
                                                <label for="four{{ $loop->index+1 }}">
                                                    <h4 style="color: #3c8dbc;">4</h4>
                                                </label>
                                                <input type="radio" name="rating_{{ $loop->index+1 }}" id="five{{ $loop->index+1 }}" value="5">
                                                <label for="five{{ $loop->index+1 }}">
                                                    <h4 style="color: #3c8dbc;">5</h4>
                                                </label>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="form-group">
                            <h4 style="color: #3c8dbc;">*Комментарий</h4>
                        </div>
                        <div id="editor"></div>
                        <textarea name="comment" style="display: none;" cols="30" rows="10"></textarea>
                        <div class="form-group" style="margin-top: 10px;">
                            <input type="submit" value="Сохранить" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main-menu')
    @include('Manager.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#resultContact").submit(function () {
                rez = $("input[name~='optionsRadios']:checked").val();
                comment = CKEDITOR.instances.editor.getData();
                ans = 0;
                close = {};
                q_id_ar = [];

                if (!rez) {
                    alert('Выберите результат');
                    return false;
                }
                if (!comment) {
                    alert('Укажите коментарий');
                    return false;
                } else {
                    $('textarea[name~="comment"]').html(comment);
                }

                switch (rez) {
                    case '1':
                        return true;
                        break;
                    case '2':
                        close = $("input[name~='closeProject']:checked").val();
                        close_questions = $('.close-questions').length;
                        //alert(close_questions);
                        if (!close) {
                            alert('Укажите можно ли закрывать проект');
                            return false;
                        }

                        for (i=1;i<=close_questions;i++) {
                            q_id = $(".close-questions").eq(i-1).attr('id');
                            a_id = $("input[name~='rating_"+i+"']:checked").val();
                            close = {
                                'q_id': q_id,
                                'a_id': a_id
                            };
                            q_id_ar.push(close);
                            ans = 1;
                        }

                        for (j=0;j<q_id_ar.length;j++) {
                            if (typeof q_id_ar[j].a_id == 'undefined') {
                                alert('Все вопросы обязательны для заполнения');
                                return false;
                            }
                        }

                        $("input[name~='close']").val(JSON.stringify(q_id_ar));
                        return true;
                        break;
                }

                return false;
            });
        });
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor')
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        });

        $("#showResult").click(function(){
            $("#popup_message_form").slideToggle("slow");
            $(this).toggleClass("active");
            $('#add').css('display', 'none');
            $('#addComment').css('display', 'block');
            $('#addComment > a').css('display', 'block');
            return false;
        });

        $('#optionsRadios1').click(function () {
            $('#collapseOne').addClass('disabledbutton');
            $('#collapseTwo').addClass('disabledbutton');
            $('#collapseFour').addClass('disabledbutton');
            $('#collapseOne').css('display', 'none');
            $('#collapseTwo').css('display', 'none');
        });

        $('#optionsRadios2').click(function () {
            $('#collapseOne').addClass('disabledbutton');
            $('#collapseTwo').removeClass('disabledbutton');
            $('#collapseFour').removeClass('disabledbutton');
            $('#collapseTwo').slideToggle('fast');
            $('#collapseOne').css('display', 'none');
        });
    </script>
@endpush