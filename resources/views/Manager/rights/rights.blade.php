@extends('layouts.cabinet')

@section('title')
    Права доступа
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            @foreach($groups as $g)
                @if($loop->index+1 == 1)
                    <li class="active"><a href="#{{ $g->slug }}" data-toggle="tab">{{ $g->name }}</a></li>
                @else
                    <li><a href="#{{ $g->slug }}" data-toggle="tab">{{ $g->name }}</a></li>
                @endif
            @endforeach
        </ul>
        <div class="tab-content">
            @foreach($groups as $g)
                @if($loop->index+1 == 1)
                    <div class="tab-pane active" id="{{ $g->slug }}">
                        <form action="/manager/rights/save" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="slug" value="{{ $g->slug }}">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><span>Модуль CRM</span></th>
                                    <th><span>Просмотр</span></th>
                                    <th><span>Создание</span></th>
                                    <th><span>Редактирование</span></th>
                                    <th><span>Удаление</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Панель управления</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'dashboard.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Пользователи</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'user.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Группы</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'group.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>События</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'event.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Календарь</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'calendar.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Задачи</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'task.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Проекты</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'project.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Отдел разработки форм</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'print_form.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Документооборот</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'doc.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Клиенты на обзвон</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'calls.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Права</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'right.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Справочник "Группы задач"</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'task_group_ref.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Справочник "Статусы задач"</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'status_task_ref.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Справочник "Модули"</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'module_ref.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Справочник "Клиенты"</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'clients_ref.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Справочник "Города"</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'citys_ref.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Справочник "Документы"</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'documents_ref.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                </tbody>
                            </table>

                            <div class="form-group">
                                <input type="submit" value="Сохранить" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                @else
                    <div class="tab-pane" id="{{ $g->slug }}">
                        <form class="tab-pane active" id="{{ $g->slug }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="slug" value="{{ $g->slug }}">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><span>Модуль CRM</span></th>
                                    <th><span>Просмотр</span></th>
                                    <th><span>Создание</span></th>
                                    <th><span>Редактирование</span></th>
                                    <th><span>Удаление</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Панель управления</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'dashboard.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Пользователи</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'user.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Группы</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'group.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>События</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'event.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Календарь</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'calendar.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Задачи</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'task.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Проекты</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'project.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Права</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'right.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Справочник "Группы задач"</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'task_group_ref.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Справочник "Статусы задач"</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'status_task_ref.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Справочник "Модули"</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'module_ref.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Справочник "Клиенты"</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'clients_ref.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Справочник "Города"</td>
                                    @foreach($right[$g->slug] as $k => $r)
                                        @if (strstr($k, 'citys_ref.'))
                                            <td>
                                                @if( $r == 1 )
                                                    <input type="checkbox" name="{{ $k }}" checked>
                                                @else
                                                    <input type="checkbox" name="{{ $k }}">
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                </tbody>
                            </table>

                            <div class="form-group">
                                <input type="submit" value="Сохранить" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                @endif
            @endforeach
        </div>
        <!-- /.tab-content -->
    </div>
@endsection

@section('main-menu')
    @include('Manager.main-menu')
@endsection

@push('scripts')

@endpush