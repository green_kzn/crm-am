@extends('layouts.cabinet')

@section('title')
    Добавление группы
@endsection

@section('content')
    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                {{ $error }}
            </div>
        @endforeach
    @endif

    <div class="row">
        <div class="col-md-3">
            <form action="/manager/group/add" method="get">
                {{ csrf_field() }}
                <div class="form-group" id="GNameGroup">
                    <label class="control-label" for="inputNameGroup" style="display: none;"></label>
                    <input type="text" name="group_name" class="form-control" id="inputNameGroup" placeholder="Название">
                </div>
                <div class="form-group" id="GSlugGroup">
                    <label class="control-label" for="inputSlugGroup" style="display: none;"></label>
                    <input type="text" name="group_slug" class="form-control" id="inputSlugGroup" placeholder="Псевдоним">
                </div>

                <div class="form-group">
                    <input type="submit" value="Сохранить" class="btn btn-success">
                    <a href="/manager/groups" class="btn btn-default">Отмена</a>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('main-menu')
    @include('Manager.main-menu')
@endsection
