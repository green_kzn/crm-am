@extends('layouts.cabinet')

@section('title')
    Клиенты на обзвон
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#contact" data-toggle="tab" aria-expanded="true">Договориться на контакт</a></li>
            <li class=""><a href="#callback" data-toggle="tab" aria-expanded="false">Взять обратную связь</a></li>
            <li class=""><a href="#other" data-toggle="tab" aria-expanded="false">Другая причина</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="contact">
                <div class="box">
                    <table class="table">
                        <thead>
                        <th>Клиент</th>
                        <th>Проект</th>
                        <th>Город</th>
                        <th>Ответственный внедренец</th>
                        <th>Последний контакт</th>
                        <th>С какими задачами работали</th>
                        <th>Просили перезвонить</th>
                        <th>Действия</th>
                        </thead>
                        <tbody>
                        @if ($tasks != '')
                            @foreach($tasks as $t)
                            <tr>
                                <td style="vertical-align: middle;">{{ $t['client']->name }}</td>
                                <td style="vertical-align: middle;">{{ $t['project']->name }}</td>
                                <td style="vertical-align: middle;">{{ $t['city'] }}</td>
                                <td style="vertical-align: middle;">{{ $t['user']->last_name }} {{ $t['user']->first_name }}</td>
                                <td style="vertical-align: middle;">{{ $t['prev_contact'] }}</td>
                                <td style="vertical-align: middle;">{{ $t['name'] }}</td>
                                <td style="vertical-align: middle;">{{ $t['next_contact'] }}</td>
                                <td style="vertical-align: middle;">
                                    <a href="/manager/calls/edit/{{ $t['id'] }}" class="btn btn-primary">
                                        <i class="fa fa-search"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        @else
                        <tr>
                            <td colspan="8" style="text-align: center; font-style: italic;">Ни чего не найдено</td>
                        </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.tab-pane -->
            </div>
            <div class="tab-pane" id="callback">
                <div class="form-group">
                    <input name="type" id="work" value="work" type="radio" checked>
                    <label for="work" style="margin-right: 20px;">
                        <h4 style="color: #3c8dbc;">На внедрении</h4>
                    </label>
                    <input name="type" id="close" value="close" type="radio">
                    <label for="close">
                        <h4 style="color: #3c8dbc;">Ожидают закрытия</h4>
                    </label>
                </div>
                <div class="box">
                    <table class="table" id="callback_projects">
                        <thead>
                        <th>Клиент</th>
                        <th>Наименование проекта</th>
                        <th>Последний контакт внедренца</th>
                        <th>С какими задачами работали</th>
                        <th>Последняя обратная связь</th>
                        <th>Город</th>
                        <th>Действия</th>
                        </thead>
                        <tbody>
                        @if($feedback_projects[0] != '')
                            @foreach($feedback_projects as $fp)
                            <tr>
                                <td>{{ $fp['client'] }}</td>
                                <td>{{ $fp['p_name'] }}</td>
                                @if($fp['prev_contact'])
                                <td>{{ $fp['prev_contact'] }}</td>
                                @else
                                <td>Отсутствует</td>
                                @endif
                                <td>{{ $fp['task'] }}</td>
                                @if($fp['callback_prev_contact'] == null)
                                <td>Отсутствует</td>
                                @else
                                <td>{{ $fp['callback_prev_contact'] }}</td>
                                @endif
                                <td>{{ $fp['city'] }}</td>
                                <td>
                                    <a href="/manager/callback/edit/{{ $fp['id'] }}" class="btn btn-primary">
                                        <i class="fa fa-search"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="7" style="text-align: center; font-style: italic;">Ни чего не найдено</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane" id="other">
                <div class="box">
                    <table class="table">
                        <thead>
                        <th>Клиент</th>
                        <th>Проект</th>
                        <th>Город</th>
                        <th>Ответственный внедренец</th>
                        <th>Тип обращения</th>
                        <th>Комментарий</th>
                        <th>Действия</th>
                        </thead>
                        <tbody>
                        @if ($other_task[0] != '')
                            @foreach($other_task as $t)
                                <tr>
                                    <td>{{ $t['client'] }}</td>
                                    <td>{{ $t['project'] }}</td>
                                    <td>{{ $t['city'] }}</td>
                                    <td>{{ $t['user'] }}</td>
                                    @if($t['type'] == 'mail')
                                    <td>Почта</td>
                                    @else
                                    <td>Телефон</td>
                                    @endif
                                    <td>{{ $t['comment'] }}</td>
                                    <td>
                                        <a href="/manager/other/edit/{{ $t['id'] }}" class="btn btn-primary">
                                            <i class="fa fa-search"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="7" style="text-align: center; font-style: italic;">Ни чего не найдено</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
    </div>
@endsection

@section('main-menu')
    @include('Manager.main-menu')
@endsection

@push('scripts')
<script>
    $(document).ready(function () {
        $("input[name~='type']").change(function () {
            val = $("input[name~='type']:checked").val();
            switch (val) {
                case 'work':
                    $("#callback_projects thead tr th").remove();
                    $("#callback_projects thead tr").html(
                        '<th>Клиент</th>' +
                        '<th>Наименование проекта</th>' +
                        '<th>Последний контакт внедренца</th>' +
                        '<th>С какими задачами работали</th>' +
                        '<th>Последняя обратная связь</th>' +
                        '<th>Город</th>' +
                        '<th>Действия</th>');
                    $.ajax({
                        url: '/manager/projects/get-work-projects',
                        success: function (resp) {
                            console.log(resp);
                            content = '';
                            $("#callback_projects tbody tr").remove();
                            if (resp) {
                                for(i=0;i<resp.length;i++) {
                                    content += '<tr>' +
                                        '<td>'+resp[i].client+'</td>' +
                                        '<td>'+resp[i].p_name+'</td>';
                                    if (resp[i].prev_contact == null) {
                                        content += '<td>Отсутствует</td>';
                                    } else {
                                        content += '<td>'+resp[i].prev_contact+'</td>';
                                    }
                                    content += '<td>'+resp[i].task+'</td>';
                                    if (resp[i].callback_prev_contact == null) {
                                        content += '<td>Отсутствует</td>';
                                    } else {
                                        content += '<td>'+resp[i].callback_prev_contact+'</td>';
                                    }
                                    content += '<td>'+resp[i].city+'</td>' +
                                        '<td>' +
                                        '<a href="/manager/callback/edit/'+resp[i].id+'" class="btn btn-primary">' +
                                        '<i class="fa fa-search"></i>' +
                                        '</a>' +
                                        '</td>' +
                                        '</tr>';
                                }
                            } else {
                                content += '<tr><td colspan="7" style="text-align: center; font-style: italic;">Ни чего не найдено</td></tr>';
                            }
                            $("#callback_projects tbody").html(content);
                        }
                    });
                    break;
                case 'close':
                    $("#callback_projects thead tr th").remove();
                    $("#callback_projects thead tr").html('<th>Клиент</th>' +
                        '<th>Наименование проекта</th>' +
                        '<th>Город</th>' +
                        '<th>Контакт внедренца с директором</th>' +
                        '<th>Комментарий</th>' +
                        '<th>Действия</th>');
                    $.ajax({
                        url: '/manager/projects/get-close-projects',
                        success: function (resp) {
                            console.log(resp);
                            content = '';
                            $("#callback_projects tbody tr").remove();
                            if (resp != '') {
                                for(i=0;i<resp.length;i++) {
                                    if (resp[i] != '') {
                                        content += '<tr>' +
                                            '<td>'+resp[i].client+'</td>' +
                                            '<td>'+resp[i].p_name+'</td>' +
                                            '<td>'+resp[i].city+'</td>' +
                                            '<td>'+resp[i].done_datetime+'</td>' +
                                            '<td>'+resp[i].comment+'</td>' +
                                            '<td>' +
                                            '<a href="/manager/close/edit/'+resp[i].id+'" class="btn btn-primary">' +
                                            '<i class="fa fa-search"></i>' +
                                            '</a>' +
                                            '</td>' +
                                            '</tr>';
                                    }
                                }
                            } else {
                                content += '<tr><td colspan="5" style="text-align: center; font-style: italic;">Ни чего не найдено</td></tr>';
                            }
                            $("#callback_projects tbody").html(content);
                        }
                    });
                    break;
            }
        });
    });
</script>
@endpush