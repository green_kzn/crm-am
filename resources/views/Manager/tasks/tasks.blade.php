@extends('layouts.cabinet')

@section('title')
    Мои задачи
@endsection

@section('content')
    @if(session('perm')['task.create'])
    <div style="padding-bottom: 20px;">
        <a href="/manager/tasks/add" class="btn btn-primary">Добавить</a>
    </div>
    @endif
    <div class="box">
        @if ($tasks != '')
            <div class="box-header">
                <h3 class="box-title">Список задач</h3>

                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th>№</th>
                            <th>Название</th>
                            <th>Постановщик</th>
                            <th>Дата/время начала</th>
                            <th>Дата/время окончания</th>
                            <th>Дата/время постановки</th>
                            <th colspan="2">Действия</th>
                        </tr>
                        @foreach($tasks as $t)
                            <tr>
                                <td>{{ $loop->index+1 }}</td>
                                <td>{{ $t['name'] }}</td>
                                <td>{{ $t['observer'] }}</td>
                                <td></td>
                                <td></td>
                                <td>{{ $t['created'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        @else
            <div class="box-header">
                <h3 class="box-title">Список задач</h3>

                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tbody>
                    <tr>
                        <th>№</th>
                        <th>Название</th>
                        <th>Постановщик</th>
                        <th>Дата/время начала</th>
                        <th>Дата/время окончания</th>
                        <th>Дата/время постановки</th>
                        <th colspan="2">Действия</th>
                    </tr>
                    <td colspan="8" style="text-align: center;">Список задач пуст</td>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        @endif
    </div>
@endsection

@section('main-menu')
    @include('Manager.main-menu')
@endsection

@push('scripts')

@endpush