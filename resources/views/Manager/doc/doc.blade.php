@extends('layouts.cabinet')

@section('title')
    Контроль документов от клиентов
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Невыполненные</a></li>
                    <li><a href="#tab_2" data-toggle="tab">Выполненные</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="form-group">
                            <a onclick="sort('input');" style="cursor: pointer;">Входящие</a>&nbsp;&nbsp;&nbsp;&nbsp;<a onclick="sort('output');" style="cursor: pointer;">Исходящие</a>&nbsp;&nbsp;&nbsp;&nbsp;<a onclick="sort('all');" style="cursor: pointer;">Все</a>
                        </div>
                        <div class="form-group">
                            @foreach($doc_type as $dt)
                                <a onclick="sort_type('{{$dt}}');" style="cursor: pointer;">{{$dt}}</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            @endforeach
                        </div>
                        <table class="table" id="doc">
                            <thead>
                            <tr><th>Клиент</th>
                                <th>Наименование проект</th>
                                <th>Город</th>
                                <th>Тип документа</th>
                                <th>Документ</th>
                                <th>Дата создания этапа</th>
                                <th>Когда должны прислать/отправить</th>
                                <th>Действия</th>
                            </tr></thead>
                            <tbody>
                            @if($docs != '')
                                @foreach($docs as $d)
                                    <tr>
                                        <td>{{ $d['client'] }}</td>
                                        <td>{{ $d['project_name'] }}</td>
                                        <td>{{ $d['city'] }}</td>
                                        <td>{{ $d['doc_type'] }}</td>
                                        <td>{{ $d['doc'] }}</td>
                                        <td>{{ $d['start_date'] }}</td>
                                        <td>{{ $d['finish_date'] }}</td>
                                        <td>
                                            <a href="doc/edit/{{ $d['id'] }}" class="btn btn-primary">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="8" style="text-align: center; font-style: italic;">Ни чего не найдено</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                        <div class="form-group">
                            <input type="radio" name="type2" id="input" value="input" checked>
                            <label for="input" style="margin-right: 20px;">
                                <h4 style="color: #3c8dbc;">Входящий</h4>
                            </label>
                            <input type="radio" name="type2" id="output" value="output">
                            <label for="output">
                                <h4 style="color: #3c8dbc;">Исходящий</h4>
                            </label>
                        </div>
                        <div class="form-group">
                            @foreach($doc_type as $dt)
                                <a href="">{{$dt}}</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            @endforeach
                        </div>
                        <table class="table">
                            <thead>
                            <tr><th>Клиент</th>
                                <th>Наименование проект</th>
                                <th>Город</th>
                                <th>Тип документа</th>
                                <th>Документ</th>
                                <th>Дата создания этапа</th>
                                <th>Когда должны прислать/отправить</th>
                                <th>Действия</th>
                            </tr></thead>
                            <tbody>
                            <tr>
                                <td colspan="8" style="text-align: center; font-style: italic;">Ни чего не найдено</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
        </div>
    </div>
@endsection

@section('main-menu')
    @include('Manager.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">
        function sort(type) {
            content = '';

            $.ajax({
                url: '/manager/doc/sort',
                data: {'type': type},
                success: function (resp) {
                    $("#doc tbody tr").remove();
                    if (resp != '') {
                        for (i=0;i<resp.length;i++) {
                            content += '<tr>' +
                                '<td>'+resp[i].client+'</td>' +
                                '<td>'+resp[i].project_name+'</td>' +
                                '<td>'+resp[i].city+'</td>' +
                                '<td>'+resp[i].doc_type+'</td>' +
                                '<td>'+resp[i].doc+'</td>' +
                                '<td>'+resp[i].start_date+'</td>' +
                                '<td>'+resp[i].finish_date+'</td>' +
                                '<td><a href="doc/edit/'+resp[i].id+'" class="btn btn-primary"> <i class="fa fa-pencil"></i> </a></td>' +
                                '</tr>';
                        }
                    } else {
                        content += '<tr><td colspan="8" style="text-align: center; font-style: italic;">Документы не найдены<td></tr>';
                    }
                    $("#doc tbody").html(content);
                }
            });
        }

        function sort_type(slug) {
            content = '';
            console.log(slug);

            $.ajax({
                url: '/manager/doc/sort_type',
                data: {'type': slug},
                success: function (resp) {
                    console.log(resp);
                    $("#doc tbody tr").remove();
                    if (resp != '') {
                        for (i=0;i<resp.length;i++) {
                            content += '<tr>' +
                                '<td>'+resp[i].client+'</td>' +
                                '<td>'+resp[i].project_name+'</td>' +
                                '<td>'+resp[i].city+'</td>' +
                                '<td>'+resp[i].doc_type+'</td>' +
                                '<td>'+resp[i].doc+'</td>' +
                                '<td>'+resp[i].start_date+'</td>' +
                                '<td>'+resp[i].finish_date+'</td>' +
                                '<td><a href="doc/edit/'+resp[i].id+'" class="btn btn-primary"> <i class="fa fa-pencil"></i> </a></td>' +
                                '</tr>';
                        }
                    } else {
                        content += '<tr><td colspan="8" style="text-align: center; font-style: italic;">Документы не найдены<td></tr>';
                    }
                    $("#doc tbody").html(content);
                }
            });
        }
    </script>
@endpush