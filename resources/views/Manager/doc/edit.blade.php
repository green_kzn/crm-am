@extends('layouts.cabinet')

@section('title')
    Контроль документов от клиентов
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="col-md-6">
        <div class="box box-primary" style="padding: 10px;">
            <div class="box-header with-border">
                <h3 class="box-title">Общая информация</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body" style="">
                <table>
                    <tr>
                        <td style="padding-right: 10px; font-weight: bold;">Клиент</td>
                        <td>{{ $client }}</td>
                    </tr>
                    <tr>
                        <td style="padding-right: 10px; font-weight: bold;">Проект</td>
                        <td>{{ $project_name }}</td>
                    </tr>
                    <tr>
                        <td style="padding-right: 10px; font-weight: bold;">Тип документа</td>
                        <td>{{ $doc_type }}</td>
                    </tr>
                    <tr>
                        <td style="padding-right: 10px; font-weight: bold;">Документ</td>
                        <td>{{ $doc }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="box box-primary" style="padding: 10px;">
            <form action="" id="edit_doc">
                {{ csrf_field() }}
                <table class="table table-condensed">
                    <tbody>
                    <tr>
                        <td style="vertical-align: middle; width: 20%;">Прислали/отправили</td>
                        <td style="vertical-align: middle; width: 2%;">
                            <input type="checkbox" id="send">
                        </td>
                        <td style="vertical-align: middle;">
                            <input type="date" class="form-control" name="date_send" id="date_send" style="width: 200px" disabled required>
                            <label id="date_send-error" class="error" for="date_send" style="display: none;"></label>
                        </td>
                    </tr>
                    @if($doc_type_id == 2)
                    <tr>
                        <td style="vertical-align: middle; width: 20%;">Индентификатор</td>
                        <td style="vertical-align: middle; width: 2%;" colspan="2">
                            <input type="text" name="doc_id" required>
                        </td>
                    </tr>
                    @endif
                    </tbody>
                </table>
                <table class="table table-condensed" style="width: 50%;">
                    <tbody>
                    <tr>
                        <td colspan="2" style="vertical-align: middle;">
                            Следующий документ
                        </td>
                        <td colspan="3">
                            <a onclick="addDoc();" class="btn btn-primary">Добавить</a>
                        </td>
                    </tr>
                    <tr id="newRow"></tr>
                    </tbody>
                </table>

                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Сохранить">
                    <a href="/manager/doc" class="btn btn-default">Отмена</a>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('main-menu')
    @include('Manager.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#send").click(function () {
                if ($("#send").prop("checked")) {
                    $("#date_send").removeAttr('disabled');
                    $("#date_send").addAttr('required');
                } else {
                    $("#date_send").attr('disabled', 'true');
                }
            });

            $("#edit_doc").validate({
                messages: {
                    date_send: {
                        required: "Это поле обязательно для заполнения"
                    },
                    doc_id: {
                        required: "Это поле обязательно для заполнения"
                    }
                }
            });
        });
        
        function addDoc() {
            $("#date_send-error").css('display', 'none');

            send = $("#date_send").attr('disabled');
            val = $("#date_send").val();

            if (send !== 'disabled' && val !== '') {
                $.ajax({
                    url: '/manager/doc/getQueueDoc',
                    success: function (resp) {
                        console.log(resp);
                        content = '<tr>' +
                            '<td><select class="form-control">';

                        content += '<option>1</option>';

                        content += '</select></td>' +
                            '<td><select class="form-control">' +
                            '<option>1</option>' +
                            '</select></td>' +
                            '<td><input type="date" class="form-control"></td>' +
                            '<td>' +
                            '<a onclick="" class="btn btn-primary" style="margin-right: 5px;"><i class="fa fa-pencil"></i></a>' +
                            '<a onclick="delRow();" class="btn btn-danger"><i class="fa fa-trash"></i></a>' +
                            '</td>' +
                            '</tr>';
                        $('#newRow').after(content);
                    }
                });
            } else {
                $("#date_send-error").html('Укажите сначала дату отправки/приема');
                $("#date_send-error").css('display', 'block');
            }
        }
        
        function delRow() {
            
        }
    </script>
@endpush