<html><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>

    <style type="text/css">
        .ExternalClass *,img {
            line-height:100%
        }
        body,hr {
            margin:0
        }
        table,td,th,tr {
            border-color:transparent
        }
        #outlook a,
        .content-cell table.social td,
        .content-cell table.social th,
        .content-cell table.sp-button td,
        .content-cell table.sp-button th, body{
            padding:0
        }
        a,img {
            text-decoration:none
        }
        .content-cell
        .link_img,
        .content-cell table.social .social_element img.social,
        .social_element img.social {
            display:block
        }
        .content-cell em,
        .content-cell span>a,
        .email-text em,
        .email-text pre,
        .email-wrapper span>a {
            color:inherit
        }
        .content-cell .sp-button-text a img,
        .sp-video img {
            max-width:100%
        }
        body {
            -webkit-text-size-adjust:100%;
            -ms-text-size-adjust:100%
        }
        table,td {
            mso-table-lspace:0;
            mso-table-rspace:0;
            border-collapse:collapse
        }
        .ExternalClass,
        .ReadMsgBody {
            width:100%
        }
        img {
            -ms-interpolation-mode:bicubic;
            border:0;
            height:auto;
            outline:0
        }
        table {
            margin-bottom:0!important
        }
        td[class^=xfmc] {
            width:inherit!important
        }
        p {
            font-size:14px;
            line-height:1.5;
            margin:0 0 10px
        }
        h1,h2,h3,h4,h5 {
            line-height:1.2;
            margin:0 0 10px;
            font-weight:400
        }
        .content-cell .sp-button table td,
        .content-cell table.social {
            line-height:1
        }
        h1 {
            font-size:36px
        }
        h2 {
            font-size:30px
        }
        h3{
            font-size:24px
        }
        h4 {
            font-size:20px
        }
        h5 {
            font-size:14px
        }
        th.social_element,
        th.tc {
            font-weight:400;
            text-align:left
        }
        .content-cell {
            vertical-align:top
        }
        .content-cell table.social,
        .content-cell table.social table,
        .content-cell table.social td,
        .content-cell table.social th,
        .content-cell table.sp-button,
        .content-cell table.sp-button table,
        .content-cell table.sp-button td,
        .content-cell table.sp-button th {
            border:0
        }
        .content-cell > center > .sp-button {
            margin-left:auto;
            margin-right:auto
        }
        .content-cell .social,
        .content-cell .social_element,
        .content-cell .sp-button-side-padding,
        .content-cell .sp-button-text {
            border-color:transparent;
            border-width:0;
            border-style:none
        }
        .content-cell .sp-button-side-padding {
            width:21px;
            -premailer-width:21
        }
        .content-cell .sp-button-text a {
            text-decoration:none;display:block
        }
        .content-cell>div>.sp-img,
        .content-cell>div>a>.sp-img {
            margin:0
        }
        .content-cell>p,
        .email-text>p {
            line-height:inherit;
            color:inherit;
            font-size:inherit
        }
        .content-cell>table,
        .content-cell>table>tbody>tr>td,
        .content-cell>table>tbody>tr>th,
        .content-cell>table>tr>td,
        .content-cell>table>tr>th,
        .email-text>table,
        .email-text>table>tbody>tr>td,
        .email-text>table>tbody>tr>th,
        .email-text>table>tr>td,
        .email-text>table>tr>th {
            border-color:#ddd;
            border-width:1px;
            border-style:solid
        }
        .content-cell>table td,
        .content-cell>table th,
        .email-text>table td,
        .email-text>table th {
            padding:3px
        }
        .content-cell table.social .social_element,
        .social_element {
            padding:2px 5px;
            font-size:13px;
            font-family:Arial,
            sans-serif;
            line-height:32px
        }
        .content-cell table.social .social_element_t_3 img.social,
        .content-cell table.social .social_element_t_4 img.social,
        .content-cell table.social .social_element_t_5 img.social,
        .content-cell table.social .social_element_v_i_t img.social {
            display:inline
        }
        .email-text table th {
            text-align:center
        }
        .email-text pre {
            background-color:transparent;
            border:0;
            padding:0;
            margin:1em 0
        }
        .sp-video a {
            display:block;
            overflow:auto
        }
        @media only screen and (max-width:640px){
            div,
            table,
            td {
                width:100%!important
            }
            .sp-button,
            hr,
            input,
            table {
                max-width:100%!important
            }
            .wrapper-table {
                min-width:296px
            }
            table {
                border-width:1px
            }
            hr,
            table {
                width:100%
            }
            div,
            td {
                height:auto!important;
                box-sizing:border-box
            }
            .content-cell img,
            img:not(.p100_img) {
                width:auto;
                height:auto;
                max-width:100%!important
            }
            td,
            th {
                display:block!important;
                margin-bottom:0;
                height:inherit!important
            }
            td.content-cell .social,
            th.content-cell .social {
                width:auto!important
            }
            td.content-cell .share td,
            td.content-cell .share th,
            td.content-cell .social td,
            td.content-cell .social th,
            th.content-cell .share td,
            th.content-cell .share th,
            th.content-cell .social td,
            th.content-cell .social th {
                display:inline!important;
                display:inline-block!important
            }
            td.content-cell .share td.social_element_t_3,
            td.content-cell .share td.social_element_t_4,
            td.content-cell .share th.social_element_t_3,
            td.content-cell .share th.social_element_t_4,
            td.content-cell .social td.social_element_t_3,
            td.content-cell .social td.social_element_t_4,
            td.content-cell .social th.social_element_t_3,
            td.content-cell .social th.social_element_t_4,
            th.content-cell .share td.social_element_t_3,
            th.content-cell .share td.social_element_t_4,
            th.content-cell .share th.social_element_t_3,
            th.content-cell .share th.social_element_t_4,
            th.content-cell .social td.social_element_t_3,
            th.content-cell .social td.social_element_t_4,
            th.content-cell .social th.social_element_t_3,
            th.content-cell .social th.social_element_t_4 {
                display:block!important
            }
            td.content-cell .share td a>img,
            td.content-cell .share th a>img,
            td.content-cell .social td a>img,
            td.content-cell .social th a>img,
            th.content-cell .share td a>img,
            th.content-cell .share th a>img,
            th.content-cell .social td a>img,
            th.content-cell .social th a>img {
                width:32px!important;
                height:32px!important
            }
            td.content-cell>td,
            th.content-cell>td {
                width:100%
            }
            td.content-cell>p,
            th.content-cell>p {
                width:100%!important
            }
            td.expander,
            th.expander {
                height:15px!important;
                margin-bottom:1px
            }
            td.expander[height="0"],
            th.expander[height="0"] {
                height:0!important;
                margin-bottom:0!important
            }
            td.gutter,
            th.gutter {
                display:none!important
            }
            .content-row>.gutter+.content-cell {
                padding-left:15px;padding-right:15px
            }
            .sp-video {
                padding-left:15px!important;
                padding-right:15px!important
            }
            .wrapper-table>tbody>tr>td {
                padding:0
            }
            .block-divider {
                padding:2px 15px!important
            }
            .social_share {
                width:16px!important;
                height:16px!important
            }
            .sp-button td {
                display:table-cell!important;
                width:initial!important
            }
            .tc {
                width:100%!important
            }
            .small_img,
            table.email-checkout.email-checkout-yandex {
                width:auto!important
            }
            .inline-item,
            table.smallImg td.smallImg {
                display:inline!important
            }
            table.origin-table {
                width:95%!important
            }
            table.origin-table td {
                display:table-cell!important;
                padding:0!important
            }
            table.origin-table td img.small_img {
                max-width:120px!important
            }
            .p100_img {
                width:100%!important;
                max-width:100%!important;
                height:auto!important
            }
            /*! prevent replacing brackets */
        }
        @media only screen and (max-width:640px) and screen and (-ms-high-contrast:active),
        only screen and (max-width:640px) and (-ms-high-contrast:none) {
            td,th {
                float:left;
                width:100%;
                clear:both
            }
            .content-cell img,
            img:not(.p100_img) {
                width:auto;
                height:auto;
                max-width:269px!important;
                margin-right:auto;
                display:block!important;
                margin-left:auto
            }
        }
        .content-cell * {
            box-sizing:border-box
        }
    </style>
</head>
<body style="color: #444;font-family: Arial, Helvetica Neue, Helvetica, sans-serif;font-size:14px;line-height:1.5;margin:0;">
<table cellpadding="5" cellspacing="0" width="100%" border="0" style="border-collapse:collapse;color:#444;font-family:Arial,Helvetica Neue,Helvetica,sans-serif;font-size:14px;line-height:1.5;background-color:#eee;background-image:url(https://resize.yandex.net/mailservice?url=http%3A%2F%2Fimg.stat-pulse.com%2F9dae6d62c816560a842268bde2cd317d%2Ffiles%2Femailservice%2Fuserfiles%2F25da5a0cc32a3e82a36ff565eb52f2b8585362%2FSeryy_pattern_81650f58a27e36bd60.png&amp;proxy=yes&amp;key=2000dbdf47c4d21ed445802397892398);background-repeat:repeat-x;" bgcolor="#eeeeee"><tbody><tr style="border-color:transparent;"><td align="center" style="border-collapse:collapse;border-color:transparent;"><table cellpadding="0" cellspacing="0" width="600px" id="bodyTable895a22116d5651975c803dcedd50ce5c" border="0" bgcolor="#ffffff" style="border-collapse:collapse;color:#444;font-family: Arial,Helvetica Neue,Helvetica,sans-serif;font-size:14px;line-height:1.5;"><tbody><tr style="border-color:transparent;"><td border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-color:transparent;"><table cellpadding="0" cellspacing="0" style="border-collapse:collapse;color:#444;font-family:Arial,Helvetica Neue,Helvetica,sans-serif;font-size:14px;line-height:1.5;width:100%;" border="0" width="100%"><tbody><tr style="border-color:transparent;"><th width="600" style="border-color:transparent;font-weight:400;text-align:left;vertical-align:top;" cellpadding="0" cellspacing="0" align="left" valign="top"><table border="0" width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse;color:#444;font-family:Arial,Helvetica Neue,Helvetica,sans-serif;font-size:14px;line-height:1.5;background-color:#ecebec;" bgcolor="#ecebec"><tbody><tr style="border-color:transparent;"><td cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-color:transparent;"><table width="100%" cellpadding="0" cellspacing="0" id="wd7cd9886042accfa096c75f0aafe489e" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;background-color:#ecebec;font-weight:normal;margin:0;" bgcolor="#ecebec"><tbody><tr style="border-color:transparent;"><td colspan="1" width="100%" height="15" style="border-collapse:collapse;border-color:transparent;"></td></tr><tr style="border-color:transparent;"><td width="600" style="border-collapse:collapse;border-color:transparent;vertical-align:top;" valign="top"></td></tr><tr style="border-color:transparent;"><td colspan="1" width="100%" height="15" style="border-collapse:collapse;border-color:transparent;"></td></tr></tbody></table></td></tr></tbody></table></th></tr></tbody></table></td></tr><tr style="border-color:transparent;"><td border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-color:transparent;"><table cellpadding="0" cellspacing="0" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;width:100%;" border="0" width="100%"><tbody><tr style="border-color:transparent;"><th width="600" style="border-color:transparent;font-weight:400;text-align:left;vertical-align:top;" cellpadding="0" cellspacing="0" align="left" valign="top"><table border="0" width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse;color:#444;font-family:Arial,Helvetica Neue,Helvetica,sans-serif;font-size:14px;line-height:1.5;background-color:#ecebec;" bgcolor="#ecebec"><tbody><tr style="border-color:transparent;"><td cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-color:transparent;"><table width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;background-color:#ecebec;height:20px;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;" bgcolor="#ecebec" height="20"><tbody><tr style="border-color:transparent;"><td height="20" style="border-collapse:collapse;border-color:transparent;"></td></tr></tbody></table></td></tr></tbody></table></th></tr></tbody></table></td></tr><tr style="border-color:transparent;"><td border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-color:transparent;"><table cellpadding="0" cellspacing="0" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;width:100%;" border="0" width="100%"><tbody><tr style="border-color:transparent;"><th width="600" style="border-color:transparent;font-weight:400;text-align:left;vertical-align:top;" cellpadding="0" cellspacing="0" align="left" valign="top"><table border="0" width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;background-color:#ecebec;" bgcolor="#ecebec"><tbody><tr style="border-color:transparent;"><td cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-color:transparent;"><table width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;background-color:#ecebec;" bgcolor="#ecebec"><tbody><tr style="border-color:transparent;"><td colspan="1" width="100%" height="0" style="border-collapse:collapse;border-color:transparent;"></td></tr><tr style="border-color:transparent;"><td width="600" style="border-collapse:collapse;border-color:transparent;vertical-align:top;" valign="top"><div style="color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;display:block;height:63;width:100%;" height="63" width="100%"><a href="http://track.stat-pulse.com/go/ec/f542699145a31d289c1cdc36a9eb22e9/ci/OTYwMjg4Nw==/ui/NTg1MzYy/li/MjE5MDA5MDA4/re/eWFtYWxldGRpbm92LnJ1c2xAeWFuZGV4LnJ1/l/aHR0cHMlM0ElMkYlMkZhcmNoaW1lZC5wcm8lMkY=/" style="text-decoration:none;color:#0089bf;display:block;" data-vdir-href="https://mail.yandex.ru/re.jsx?uid=161250520&amp;c=LIZA&amp;cv=17.1.101&amp;mid=168884986026461166&amp;h=a,iSfqAu5v5X06cyAAF4InOA&amp;l=aHR0cDovL3RyYWNrLnN0YXQtcHVsc2UuY29tL2dvL2VjL2Y1NDI2OTkxNDVhMzFkMjg5YzFjZGMzNmE5ZWIyMmU5L2NpL09UWXdNamc0Tnc9PS91aS9OVGcxTXpZeS9saS9NakU1TURBNU1EQTQvcmUvZVdGdFlXeGxkR1JwYm05MkxuSjFjMnhBZVdGdVpHVjRMbkoxL2wvYUhSMGNITWxNMEVsTWtZbE1rWmhjbU5vYVcxbFpDNXdjbThsTWtZPS8" data-orig-href="http://track.stat-pulse.com/go/ec/f542699145a31d289c1cdc36a9eb22e9/ci/OTYwMjg4Nw==/ui/NTg1MzYy/li/MjE5MDA5MDA4/re/eWFtYWxldGRpbm92LnJ1c2xAeWFuZGV4LnJ1/l/aHR0cHMlM0ElMkYlMkZhcmNoaW1lZC5wcm8lMkY=/" class="daria-goto-anchor" target="_blank" rel="noopener noreferrer"><img border="0" width="299" height="auto" align="left" alt="" style="text-decoration:none;border:0;height:auto;line-height:100%;margin:0;display:block;" src="https://resize.yandex.net/mailservice?url=https%3A%2F%2Fimg.stat-pulse.com%2F9dae6d62c816560a842268bde2cd317d%2Ffiles%2Femailservice%2Fuserfiles%2F25da5a0cc32a3e82a36ff565eb52f2b8585362%2Fna_prozfonemal1.png&amp;proxy=yes&amp;key=9502c2776f63f5a886fa40c3f05223c2"></a></div></td></tr><tr style="border-color:transparent;"><td colspan="1" width="100%" height="0" style="border-collapse:collapse;border-color:transparent;"></td></tr></tbody></table></td></tr></tbody></table><table border="0" width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;background-color:#fff;" bgcolor="#ffffff"><tbody><tr style="border-color:transparent;"><td cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-color:transparent;"><table width="100%" cellpadding="0" cellspacing="0" id="wd7cd9886042accfa096c75f0aafe489e" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;font-weight:normal;margin:0;"><tbody><tr style="border-color:transparent;"><td colspan="3" width="100%" height="0" style="border-collapse:collapse;border-color:transparent;"></td></tr><tr style="border-color:transparent;"><td style="border-collapse:collapse;border-color:transparent;width:30px !important;" width="30" height="100%"></td><td width="540" style="border-collapse:collapse;border-color:transparent;vertical-align:top;" valign="top"><p style="font-size:inherit;line-height:inherit;margin:0 0 10px;color:inherit;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-weight:normal;padding:0;">&nbsp;</p><h3 style="font-weight:normal;line-height:1.2;margin:0 0 10px;font-size:24px;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;"><span style="font-weight:400;font-family:verdana,geneva,sans-serif;">Здравствуйте, {{$client}}!</span></h3><p style="font-size:inherit;line-height:inherit;margin:0 0 10px;color:inherit;font-family:Arial,Helvetica Neue,Helvetica,sans-serif;font-weight:normal;padding:0;">В процессе внедрения были проведены следующие работы:</p><p style="font-size:inherit;line-height:inherit;margin:0 0 10px;color:inherit;font-family:Arial,Helvetica Neue,Helvetica,sans-serif;font-weight:normal;padding:0;">{!! $comment !!}</p></td><td style="border-collapse:collapse;border-color:transparent;width:30px !important;" width="30" height="100%"></td></tr><tr style="border-color:transparent;"><td colspan="3" width="100%" height="15" style="border-collapse:collapse;border-color:transparent;"></td></tr></tbody></table></td></tr></tbody></table><table border="0" width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;background-color:#fff;" bgcolor="#ffffff"><tbody><tr style="border-color:transparent;"><td cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-color:transparent;"><table width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;"><tbody><tr style="border-color:transparent;"><td colspan="3" width="100%" height="10" style="border-collapse:collapse;border-color:transparent;"></td></tr><tr style="border-color:transparent;"><td colspan="3" width="100%" height="20" style="border-collapse:collapse;border-color:transparent;"></td></tr></tbody></table></td></tr></tbody></table></th></tr></tbody></table></td></tr><tr style="border-color:transparent;"><td border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-color:transparent;"><table cellpadding="0" cellspacing="0" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;width:100%;" border="0" width="100%"><tbody><tr style="border-color:transparent;"><th width="200" style="border-color:transparent;font-weight:400;text-align:left;vertical-align:top;" cellpadding="0" cellspacing="0" align="left" valign="top"><table border="0" width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;background-color:#fff;" bgcolor="#ffffff"><tbody><tr style="border-color:transparent;"><td cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-color:transparent;"><table width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;"><tbody><tr style="border-color:transparent;"><td colspan="3" width="100%" height="30" style="border-collapse:collapse;border-color:transparent;"></td></tr><tr style="border-color:transparent;"><td style="border-collapse:collapse;border-color:transparent;width:15px !important;" width="15" height="100%"></td><td width="168" style="border-collapse:collapse;border-color:transparent;vertical-align:top;" valign="top"><div style="color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;display:block;height:97;text-align:center;width:100%;" height="97" align="center" width="100%"><a href="http://track.stat-pulse.com/go/ec/f542699145a31d289c1cdc36a9eb22e9/ci/OTYwMjg4Nw==/ui/NTg1MzYy/li/MjE5MDA5MDEw/re/eWFtYWxldGRpbm92LnJ1c2xAeWFuZGV4LnJ1/l/aHR0cHMlM0ElMkYlMkZhcmNoaW1lZC5wcm8lMkY=/" style="text-decoration:none;color:#0089bf;display:block;" data-vdir-href="https://mail.yandex.ru/re.jsx?uid=161250520&amp;c=LIZA&amp;cv=17.1.101&amp;mid=168884986026461166&amp;h=a,T7tZhJ77hL6mjNmwqmxOxQ&amp;l=aHR0cDovL3RyYWNrLnN0YXQtcHVsc2UuY29tL2dvL2VjL2Y1NDI2OTkxNDVhMzFkMjg5YzFjZGMzNmE5ZWIyMmU5L2NpL09UWXdNamc0Tnc9PS91aS9OVGcxTXpZeS9saS9NakU1TURBNU1ERXcvcmUvZVdGdFlXeGxkR1JwYm05MkxuSjFjMnhBZVdGdVpHVjRMbkoxL2wvYUhSMGNITWxNMEVsTWtZbE1rWmhjbU5vYVcxbFpDNXdjbThsTWtZPS8" data-orig-href="http://track.stat-pulse.com/go/ec/f542699145a31d289c1cdc36a9eb22e9/ci/OTYwMjg4Nw==/ui/NTg1MzYy/li/MjE5MDA5MDEw/re/eWFtYWxldGRpbm92LnJ1c2xAeWFuZGV4LnJ1/l/aHR0cHMlM0ElMkYlMkZhcmNoaW1lZC5wcm8lMkY=/" class="daria-goto-anchor" target="_blank" rel="noopener noreferrer"><center><img border="0" width="168" height="auto" align="center" alt="" style="text-decoration:none;border:0;height:auto;line-height:100%;display:block;" src="https://resize.yandex.net/mailservice?url=https%3A%2F%2Fimg.stat-pulse.com%2F9dae6d62c816560a842268bde2cd317d%2Ffiles%2Femailservice%2Fuserfiles%2F25da5a0cc32a3e82a36ff565eb52f2b8585362%2FLogotip.png&amp;proxy=yes&amp;key=439eaecb6a273b3bdb2f61808f6695a6"></center></a></div></td><td style="border-collapse:collapse;border-color:transparent;width:15px !important;" width="15" height="100%"></td></tr><tr style="border-color:transparent;"><td colspan="3" width="100%" height="15" style="border-collapse:collapse;border-color:transparent;"></td></tr></tbody></table></td></tr></tbody></table></th><th width="400" style="border-color:transparent;font-weight:400;text-align:left;vertical-align:top;" cellpadding="0" cellspacing="0" align="left" valign="top"><table border="0" width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;background-color:#fff;" bgcolor="#ffffff"><tbody><tr style="border-color:transparent;"><td cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-color:transparent;"><table width="100%" cellpadding="0" cellspacing="0" id="wd7cd9886042accfa096c75f0aafe489e" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;font-weight:normal;margin:0;"><tbody><tr style="border-color:transparent;"><td colspan="3" width="100%" height="15" style="border-collapse:collapse;border-color:transparent;"></td></tr><tr style="border-color:transparent;"><td style="border-collapse:collapse;border-color:transparent;width:15px !important;" width="15" height="100%"></td><td width="372" style="border-collapse:collapse;border-color:transparent;vertical-align:top;" valign="top"><h3 style="font-weight:normal;line-height:1.5;margin:0 0 10px;font-size:24px;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;"><span style="font-family:verdana,geneva,sans-serif;font-size:18px;"><strong>С уважением, </strong></span></h3><h3 style="font-weight:normal;line-height:1.5;margin:0 0 10px;font-size:24px;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;"><span style="font-family:verdana,geneva,sans-serif;font-size:18px;"><strong>Специалист отдела внедрения {{$imp}}</strong></span></h3><p style="font-size:inherit;line-height:1.5;margin:0 0 10px;color:inherit;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-weight:normal;padding:0;"><span style="font-family:verdana,geneva,sans-serif;">T&nbsp; <span class="wmi-callto">8-495-369-12-71</span></span></p><p style="font-size:inherit;line-height:1.5;margin:0 0 10px;color:inherit;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-weight:normal;padding:0;"><span style="font-family:verdana,geneva,sans-serif;">T&nbsp; <span class="wmi-callto">8-800-301-03-73</span></span></p><p style="font-size:inherit;line-height:1.5;margin:0 0 10px;color:inherit;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-weight:normal;padding:0;"><span style="font-family:verdana,geneva,sans-serif;">WWW&nbsp; &nbsp;<a href="http://track.stat-pulse.com/go/ec/f542699145a31d289c1cdc36a9eb22e9/ci/OTYwMjg4Nw==/ui/NTg1MzYy/li/MjE5MDA5MDEx/re/eWFtYWxldGRpbm92LnJ1c2xAeWFuZGV4LnJ1/l/aHR0cHMlM0ElMkYlMkZhcmNoaW1lZC5wcm8lMkY=/" target="_blank" rel="noopener noreferrer" style="text-decoration:none;color:inherit;" data-vdir-href="https://mail.yandex.ru/re.jsx?uid=161250520&amp;c=LIZA&amp;cv=17.1.101&amp;mid=168884986026461166&amp;h=a,3GgN2BhpqlU0VnwVHXPiTw&amp;l=aHR0cDovL3RyYWNrLnN0YXQtcHVsc2UuY29tL2dvL2VjL2Y1NDI2OTkxNDVhMzFkMjg5YzFjZGMzNmE5ZWIyMmU5L2NpL09UWXdNamc0Tnc9PS91aS9OVGcxTXpZeS9saS9NakU1TURBNU1ERXgvcmUvZVdGdFlXeGxkR1JwYm05MkxuSjFjMnhBZVdGdVpHVjRMbkoxL2wvYUhSMGNITWxNMEVsTWtZbE1rWmhjbU5vYVcxbFpDNXdjbThsTWtZPS8" data-orig-href="http://track.stat-pulse.com/go/ec/f542699145a31d289c1cdc36a9eb22e9/ci/OTYwMjg4Nw==/ui/NTg1MzYy/li/MjE5MDA5MDEx/re/eWFtYWxldGRpbm92LnJ1c2xAeWFuZGV4LnJ1/l/aHR0cHMlM0ElMkYlMkZhcmNoaW1lZC5wcm8lMkY=/" class="daria-goto-anchor">archimed.pro</a></span></p></td><td style="border-collapse:collapse;border-color:transparent;width:15px !important;" width="15" height="100%"></td></tr><tr style="border-color:transparent;"><td colspan="3" width="100%" height="15" style="border-collapse:collapse;border-color:transparent;"></td></tr></tbody></table></td></tr></tbody></table></th></tr></tbody></table></td></tr><tr style="border-color:transparent;"><td border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-color:transparent;"><table cellpadding="0" cellspacing="0" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;width:100%;" border="0" width="100%"><tbody><tr style="border-color:transparent;"><th width="600" style="border-color:transparent;font-weight:400;text-align:left;vertical-align:top;" cellpadding="0" cellspacing="0" align="left" valign="top"><table border="0" width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;background-color:#ecebec;" bgcolor="#ecebec"><tbody><tr style="border-color:transparent;"><td cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-color:transparent;"><table width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;background-color:#ecebec;height:35px;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;" bgcolor="#ecebec" height="35"><tbody><tr style="border-color:transparent;"><td height="35" style="border-collapse:collapse;border-color:transparent;"></td></tr></tbody></table></td></tr></tbody></table></th></tr></tbody></table></td></tr><tr style="border-color:transparent;"><td border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-color:transparent;"><table cellpadding="0" cellspacing="0" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;width:100%;" border="0" width="100%"><tbody><tr style="border-color:transparent;"><th width="600" style="border-color:transparent;font-weight:400;text-align:left;vertical-align:top;" cellpadding="0" cellspacing="0" align="left" valign="top"><table border="0" width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;background-color:#eee;" bgcolor="#eeeeee"><tbody><tr style="border-color:transparent;"><td cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-color:transparent;"><table width="100%" cellpadding="0" cellspacing="0" id="wd7cd9886042accfa096c75f0aafe489e" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;background-color:#eee;font-weight:normal;margin:0;" bgcolor="#eeeeee"><tbody><tr style="border-color:transparent;"><td colspan="3" width="100%" height="0" style="border-collapse:collapse;border-color:transparent;"></td></tr><tr style="border-color:transparent;"><td style="border-collapse:collapse;border-color:transparent;width:30px !important;" width="30" height="100%"></td><td width="540" style="border-collapse:collapse;border-color:transparent;vertical-align:top;" valign="top"><p style="font-size:inherit;line-height:inherit;margin:0 0 10px;color:inherit;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-weight:normal;padding:0;text-align:center;" align="center"><span style="font-size:13px;line-height:19.5px;">© Copyright, 2019, ArchiMed+ • Пенза, Каракозова 44. Казань, Космонавтов 39а</span></p></td><td style="border-collapse:collapse;border-color:transparent;width:30px !important;" width="30" height="100%"></td></tr><tr style="border-color:transparent;"><td colspan="3" width="100%" height="0" style="border-collapse:collapse;border-color:transparent;"></td></tr></tbody></table></td></tr></tbody></table><table border="0" width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;background-color:#ecebec;" bgcolor="#ecebec"><tbody><tr style="border-color:transparent;"><td cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-color:transparent;"><table width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;background-color:#ecebec;text-align:center;" bgcolor="#ecebec" align="center"><tbody><tr style="border-color:transparent;"><td colspan="3" width="100%" height="0" style="border-collapse:collapse;border-color:transparent;"></td></tr><tr style="border-color:transparent;"><td style="border-collapse:collapse;border-color:transparent;width:30px !important;" width="30" height="100%"></td><td width="540" style="border-collapse:collapse;border-color:transparent;vertical-align:top;" valign="top"><table cellpadding="5" border="0" cellspacing="0" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1;border-color:transparent;border-style:none;border-width:0;border:0;border-spacing:0;display:inline-block;"><tbody><tr style="border-color:transparent;"><th style="border-color:transparent;font-family:Arial,sans-serif;font-size:13px;line-height:32px;padding:2px 5px;font-weight:400;text-align:left;border-style:none;border-width:0;border:0;" align="left"><a href="http://track.stat-pulse.com/go/ec/f542699145a31d289c1cdc36a9eb22e9/ci/OTYwMjg4Nw==/ui/NTg1MzYy/li/MjE5MDA5MDEy/re/eWFtYWxldGRpbm92LnJ1c2xAeWFuZGV4LnJ1/l/aHR0cHMlM0ElMkYlMkZ3d3cuaW5zdGFncmFtLmNvbSUyRm1pc19hcmNoaW1lZCUyRg==/" style="text-decoration:none;color:#0089bf;" data-vdir-href="https://mail.yandex.ru/re.jsx?uid=161250520&amp;c=LIZA&amp;cv=17.1.101&amp;mid=168884986026461166&amp;h=a,JGzKtC4LrpL0MnDLbW0wpA&amp;l=aHR0cDovL3RyYWNrLnN0YXQtcHVsc2UuY29tL2dvL2VjL2Y1NDI2OTkxNDVhMzFkMjg5YzFjZGMzNmE5ZWIyMmU5L2NpL09UWXdNamc0Tnc9PS91aS9OVGcxTXpZeS9saS9NakU1TURBNU1ERXkvcmUvZVdGdFlXeGxkR1JwYm05MkxuSjFjMnhBZVdGdVpHVjRMbkoxL2wvYUhSMGNITWxNMEVsTWtZbE1rWjNkM2N1YVc1emRHRm5jbUZ0TG1OdmJTVXlSbTFwYzE5aGNtTm9hVzFsWkNVeVJnPT0v" data-orig-href="http://track.stat-pulse.com/go/ec/f542699145a31d289c1cdc36a9eb22e9/ci/OTYwMjg4Nw==/ui/NTg1MzYy/li/MjE5MDA5MDEy/re/eWFtYWxldGRpbm92LnJ1c2xAeWFuZGV4LnJ1/l/aHR0cHMlM0ElMkYlMkZ3d3cuaW5zdGFncmFtLmNvbSUyRm1pc19hcmNoaW1lZCUyRg==/" class="daria-goto-anchor" target="_blank" rel="noopener noreferrer"><img border="0" alt="Instagram" style="text-decoration:none;border:0;height:auto;line-height:100%;border-color:transparent;border-style:none;border-width:0;display:block;margin:5px;" vspace="5" hspace="5" title="Instagram" width="32" height="auto" src="https://resize.yandex.net/mailservice?url=https%3A%2F%2Fimg.stat-pulse.com%2F9dae6d62c816560a842268bde2cd317d%2Fimg%2Fconstructor%2Fsocial%2Fround%2Finstagram.png&amp;proxy=yes&amp;key=dd62a835666f8bcaf12870fbe5a59f1d"></a></th><th style="border-color:transparent;font-family:Arial,sans-serif;font-size:13px;line-height:32px;padding:2px 5px;font-weight:400;text-align:left;border-style:none;border-width:0;border:0;" align="left"><a href="http://track.stat-pulse.com/go/ec/f542699145a31d289c1cdc36a9eb22e9/ci/OTYwMjg4Nw==/ui/NTg1MzYy/li/MjE5MDA5MDEz/re/eWFtYWxldGRpbm92LnJ1c2xAeWFuZGV4LnJ1/l/aHR0cHMlM0ElMkYlMkZ2ay5jb20lMkZtaXNfYXJjaGltZWQ=/" style="text-decoration:none;color:#0089bf;" data-vdir-href="https://mail.yandex.ru/re.jsx?uid=161250520&amp;c=LIZA&amp;cv=17.1.101&amp;mid=168884986026461166&amp;h=a,P9mcV-HMnJwzFDbW-o3sNw&amp;l=aHR0cDovL3RyYWNrLnN0YXQtcHVsc2UuY29tL2dvL2VjL2Y1NDI2OTkxNDVhMzFkMjg5YzFjZGMzNmE5ZWIyMmU5L2NpL09UWXdNamc0Tnc9PS91aS9OVGcxTXpZeS9saS9NakU1TURBNU1ERXovcmUvZVdGdFlXeGxkR1JwYm05MkxuSjFjMnhBZVdGdVpHVjRMbkoxL2wvYUhSMGNITWxNMEVsTWtZbE1rWjJheTVqYjIwbE1rWnRhWE5mWVhKamFHbHRaV1E9Lw" data-orig-href="http://track.stat-pulse.com/go/ec/f542699145a31d289c1cdc36a9eb22e9/ci/OTYwMjg4Nw==/ui/NTg1MzYy/li/MjE5MDA5MDEz/re/eWFtYWxldGRpbm92LnJ1c2xAeWFuZGV4LnJ1/l/aHR0cHMlM0ElMkYlMkZ2ay5jb20lMkZtaXNfYXJjaGltZWQ=/" class="daria-goto-anchor" target="_blank" rel="noopener noreferrer"><img border="0" alt="Вконтакте" style="text-decoration:none;border:0;height:auto;line-height:100%;border-color:transparent;border-style:none;border-width:0;display:block;margin:5px;" vspace="5" hspace="5" title="Вконтакте" width="32" height="auto" src="https://resize.yandex.net/mailservice?url=https%3A%2F%2Fimg.stat-pulse.com%2F9dae6d62c816560a842268bde2cd317d%2Fimg%2Fconstructor%2Fsocial%2Fround%2Fvk.png&amp;proxy=yes&amp;key=52ef19c4f502d4e87681a1e6e7d525c2"></a></th><th style="border-color:transparent;font-family:Arial,sans-serif;font-size:13px;line-height:32px;padding:2px 5px;font-weight:400;text-align:left;border-style:none;border-width:0;border:0;" align="left"><a href="http://track.stat-pulse.com/go/ec/f542699145a31d289c1cdc36a9eb22e9/ci/OTYwMjg4Nw==/ui/NTg1MzYy/li/MjE5MDA5MDE0/re/eWFtYWxldGRpbm92LnJ1c2xAeWFuZGV4LnJ1/l/aHR0cHMlM0ElMkYlMkZ3d3cuZmFjZWJvb2suY29tJTJGJUQwJTlFJUQwJTlFJUQwJTlFLSVEMCU5MCVEMSU4MCVEMSU4NSVEMCVCOCVEMCVCQyVEMCVCNSVEMCVCNC0lRDAlQkYlRDAlQkIlRDElOEUlRDElODEtMTMwNTIzMTEyOTUwNTA2MyUyRg==/" style="text-decoration:none;color:#0089bf;" data-vdir-href="https://mail.yandex.ru/re.jsx?uid=161250520&amp;c=LIZA&amp;cv=17.1.101&amp;mid=168884986026461166&amp;h=a,OOV5iLV9mQPLggngPcFUTw&amp;l=aHR0cDovL3RyYWNrLnN0YXQtcHVsc2UuY29tL2dvL2VjL2Y1NDI2OTkxNDVhMzFkMjg5YzFjZGMzNmE5ZWIyMmU5L2NpL09UWXdNamc0Tnc9PS91aS9OVGcxTXpZeS9saS9NakU1TURBNU1ERTAvcmUvZVdGdFlXeGxkR1JwYm05MkxuSjFjMnhBZVdGdVpHVjRMbkoxL2wvYUhSMGNITWxNMEVsTWtZbE1rWjNkM2N1Wm1GalpXSnZiMnN1WTI5dEpUSkdKVVF3SlRsRkpVUXdKVGxGSlVRd0pUbEZMU1ZFTUNVNU1DVkVNU1U0TUNWRU1TVTROU1ZFTUNWQ09DVkVNQ1ZDUXlWRU1DVkNOU1ZFTUNWQ05DMGxSREFsUWtZbFJEQWxRa0lsUkRFbE9FVWxSREVsT0RFdE1UTXdOVEl6TVRFeU9UVXdOVEEyTXlVeVJnPT0v" data-orig-href="http://track.stat-pulse.com/go/ec/f542699145a31d289c1cdc36a9eb22e9/ci/OTYwMjg4Nw==/ui/NTg1MzYy/li/MjE5MDA5MDE0/re/eWFtYWxldGRpbm92LnJ1c2xAeWFuZGV4LnJ1/l/aHR0cHMlM0ElMkYlMkZ3d3cuZmFjZWJvb2suY29tJTJGJUQwJTlFJUQwJTlFJUQwJTlFLSVEMCU5MCVEMSU4MCVEMSU4NSVEMCVCOCVEMCVCQyVEMCVCNSVEMCVCNC0lRDAlQkYlRDAlQkIlRDElOEUlRDElODEtMTMwNTIzMTEyOTUwNTA2MyUyRg==/" class="daria-goto-anchor" target="_blank" rel="noopener noreferrer"><img border="0" alt="Facebook" style="text-decoration:none;border:0;height:auto;line-height:100%;border-color:transparent;border-style:none;border-width:0;display:block;margin:5px;" vspace="5" hspace="5" title="Facebook" width="32" height="auto" src="https://resize.yandex.net/mailservice?url=https%3A%2F%2Fimg.stat-pulse.com%2F9dae6d62c816560a842268bde2cd317d%2Fimg%2Fconstructor%2Fsocial%2Fround%2Ffacebook.png&amp;proxy=yes&amp;key=a7bc89936d35c08ad6ed37f77957e342"></a></th></tr></tbody></table></td><td style="border-collapse:collapse;border-color:transparent;width:30px !important;" width="30" height="100%"></td></tr><tr style="border-color:transparent;"><td colspan="3" width="100%" height="5" style="border-collapse:collapse;border-color:transparent;"></td></tr></tbody></table></td></tr></tbody></table><table border="0" width="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;background-color:#ecebec;" bgcolor="#ecebec"><tbody><tr style="border-color:transparent;"><td cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-color:transparent;"><table width="100%" cellpadding="0" cellspacing="0" id="wd7cd9886042accfa096c75f0aafe489e" style="border-collapse:collapse;color:#444;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;line-height:1.5;background-color:#ecebec;font-weight:normal;margin:0;" bgcolor="#ecebec"><tbody><tr style="border-color:transparent;"><td colspan="3" width="100%" height="0" style="border-collapse:collapse;border-color:transparent;"></td></tr><tr style="border-color:transparent;"><td style="border-collapse:collapse;border-color:transparent;width:30px !important;" width="30" height="100%"></td><td width="540" style="border-collapse:collapse;border-color:transparent;vertical-align:top;" valign="top"><p style="font-size:inherit;line-height:inherit;margin:0 0 10px;color:inherit;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-weight:normal;padding:0;text-align:center;" align="center"><span style="font-size:13px;">Вы получили данную рассылку, так как являетесь клиентом или подписчиком ArchiMed+. <a href="#" style="text-decoration:none;color:inherit;" class="daria-goto-anchor" target="_blank" rel="noopener noreferrer">Отказаться от рассылки</a></span></p></td><td style="border-collapse:collapse;border-color:transparent;width:30px !important;" width="30" height="100%"></td></tr><tr style="border-color:transparent;"><td colspan="3" width="100%" height="30" style="border-collapse:collapse;border-color:transparent;"></td></tr></tbody></table></td></tr></tbody></table></th></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>

</body></html>