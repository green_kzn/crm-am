@extends('layouts.cabinet')

@section('title')
    Договориться на следующий контакт
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="col-md-6">
        <div class="box box-primary" style="padding: 10px;">
            <div class="box-header with-border">
                <h3 class="box-title">Общая информация</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body" style="">
                <table>
                    <tr>
                        <td style="padding-right: 10px; font-weight: bold;">Дата/время</td>
                        <td>{{ $task->start_date }}</td>
                    </tr>
                    <tr>
                        <td style="padding-right: 10px; font-weight: bold;">Клиент</td>
                        <td>{{ $client->name }}</td>
                    </tr>
                    <tr>
                        <td style="padding-right: 10px; font-weight: bold;">Проект</td>
                        <td>{{ $task->p_name }}</td>
                    </tr>
                    @if ($task->main_contact)
                        <tr>
                            <td style="padding-right: 10px; font-weight: bold;">Контактное лицо</td>
                            <td> {{ $task->main_contact->last_name }} {{ $task->main_contact->first_name }} {{ $task->main_contact->patronymic }} / Тел. {{ $task->main_contact->phone }}</td>
                        </tr>
                    @endif
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary" style="padding: 10px;">
            <div class="box-header with-border">
                <h3 class="box-title">История коментариев</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body" style="overflow-y: auto;height: 200px;">
                @if ($comments != 0)
                    @foreach($comments as $c)
                        <p>
                            {{ $c['date'] }} {{ $c['text'] }}
                        </p>
                    @endforeach
                @else
                    <p style="text-align: center; font-style: italic;">Коментарии отсутствуют</p>
                @endif
            </div>
        </div>
    </div>
    <div style="clear: both;"></div>
    <div class="col-md-12">
        <div style="margin-bottom: 10px; font-weight: bold; text-decoration: underline;">
            <a href="">Информация о проекте</a><br>
            <a href="">Скрипт для разговора</a>
        </div>
        <div style="padding-bottom: 20px;">
            <a id="showResult" class="btn btn-primary">Результат <i class="fa fa-angle-down"></i></a>
        </div>
        <div class="clear"></div>
        <div class="wall_form" id="popup_message_form" style="display:none; margin-bottom: 20px;">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Результат</h3>
                </div>
                <div class="content">
                    <form action="/print_form/project-task/edit/{{ $task->id }}" method="post" id="resultContact">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="1">
                            <label for="optionsRadios1">
                                <h4 style="color: #3c8dbc;">Недозвон</h4>
                            </label>
                        </div>
                        <div class="form-group">
                            <input type="radio" name="optionsRadios" id="optionsRadios2" value="2">
                            <label for="optionsRadios2">
                                <h4 style="color: #3c8dbc;">Назначен следующий контакт</h4>
                            </label>
                        </div>
                        <div id="collapseTwo" class="disabledbutton" style="display: none;">
                            <div class="box-body">
                                <div style="padding-bottom: 20px;">
                                    <a id="date1" onclick="setDateCont('date1');" data-content="{{ $tomorrow_val }}" class="btn btn-primary">Завтра</a>
                                    <a id="date2" onclick="setDateCont('date2');" data-content="{{ $plus_one_day_val }}" class="btn btn-primary">Послезавтра</a>
                                    <a id="date3" onclick="setDateCont('date3');" data-content="{{ $plus_two_day_val }}" class="btn btn-primary">{{ $plus_two_day }}</a>
                                    <a id="date4" onclick="setDateCont('date4');" data-content="{{ $plus_three_day_val }}" class="btn btn-primary">{{ $plus_three_day }}</a>
                                </div>
                                <div style="padding-bottom: 20px;">
                                    <table class="table table-bordered">
                                        <tr>
                                            <td style="vertical-align: middle;">Дата</td>
                                            <td style="vertical-align: middle;"><input type="date" name="next_date2" value="{{ $cur_date_val }}" id="date_cont"></td>
                                            <td style="vertical-align: middle;">Время</td>
                                            <td style="vertical-align: middle;"><input type="time" name="next_time2" id="time_cont"></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle;">Продолжительность</td>
                                            <td style="vertical-align: middle;"><input type="time" id="next_time" name="duration"></td>
                                            <td colspan="2" style="vertical-align: middle;">
                                                <div style="padding-bottom: 20px;">
                                                    <a id="time1" onclick="setTimeCont('time1');" data-content="30" class="btn btn-primary">30 мин.</a>
                                                    <a id="time2" onclick="setTimeCont('time2');" data-content="60" class="btn btn-primary">1 час</a>
                                                    <a id="time3" onclick="setTimeCont('time3');" data-content="90" class="btn btn-primary">1 час 30 мин.</a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle;">Контактное лицо</td>
                                            <td style="vertical-align: middle;">
                                                <select class="form-control" name="contact">

                                                </select>
                                            </td>
                                            <td style="vertical-align: middle;">Телефон</td>
                                            <td colspan="2" style="vertical-align: middle;">
                                                <select class="form-control" name="contact_phone">

                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle;">Внедренец</td>
                                            <td style="vertical-align: middle;" colspan="3">
                                                <select class="form-control" name="user_id">

                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="radio" name="optionsRadios" id="optionsRadios3" value="3">
                            <label for="optionsRadios3">
                                <h4 style="color: #3c8dbc;">Попросили перезвонить</h4>
                            </label>
                        </div>
                        <div id="collapseOne" class="disabledbutton" style="display: none;">
                            <div class="box-body">
                                <div style="padding-bottom: 20px;">
                                    <a id="date1_2" onclick="setDateCont2('date1_2');" data-content="{{ $tomorrow_val }}" class="btn btn-primary">Завтра</a>
                                    <a id="date2_2" onclick="setDateCont2('date2_2');" data-content="{{ $plus_one_day_val }}" class="btn btn-primary">Послезавтра</a>
                                    <a id="date3_2" onclick="setDateCont2('date3_2');" data-content="{{ $plus_two_day_val }}" class="btn btn-primary">{{ $plus_two_day }}</a>
                                    <a id="date4_2" onclick="setDateCont2('date4_2');" data-content="{{ $plus_three_day_val }}" class="btn btn-primary">{{ $plus_three_day }}</a>
                                </div>
                                <div style="padding-bottom: 20px;">
                                    <table class="table table-bordered">
                                        <tr>
                                            <td style="vertical-align: middle;">Дата</td>
                                            <td style="vertical-align: middle;"><input type="date" value="{{ $cur_date_val }}" id="date_cont_2" name="next_date"></td>
                                            <td style="vertical-align: middle;">Время</td>
                                            <td style="vertical-align: middle;"><input type="time" id="time_cont_2" name="next_time"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="optionsRadios2">
                                <h4 style="color: #3c8dbc;">Комментарий</h4>
                            </label>
                        </div>
                        <div id="editor"></div>
                        <textarea name="comment" style="display: none;" cols="30" rows="10"></textarea>
                        <div class="form-group" style="margin-top: 10px;">
                            <input type="submit" value="Сохранить" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main-menu')
    @include('Print_form.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('editor')
                //bootstrap WYSIHTML5 - text editor
                $('.textarea').wysihtml5()
            });

            $("#showResult").click(function(){
                $("#popup_message_form").slideToggle("slow");
                $(this).toggleClass("active");
                $('#add').css('display', 'none');
                $('#addComment').css('display', 'block');
                $('#addComment > a').css('display', 'block');
                return false;
            });

            $("#showOtherTask").click(function(){
                $("#formOtherTask").slideToggle("slow");
                $(this).toggleClass("active");
                return false;
            });
        });

        $('#resultContact').submit(function () {
            comment = CKEDITOR.instances.editor.getData();
            $('textarea[name~="comment"]').html(comment);

            return true;
        });

        $('#optionsRadios1').click(function () {
            $('#collapseOne').addClass('disabledbutton');
            $('#collapseTwo').addClass('disabledbutton');
            $('#collapseFour').addClass('disabledbutton');
            $('#collapseOne').css('display', 'none');
            $('#collapseTwo').css('display', 'none');
        });

        $('#optionsRadios2').click(function () {
            $('#collapseOne').addClass('disabledbutton');
            $('#collapseTwo').removeClass('disabledbutton');
            $('#collapseFour').removeClass('disabledbutton');
            $('#collapseTwo').slideToggle('fast');
            $('#collapseOne').css('display', 'none');
        });

        $('#optionsRadios3').click(function () {
            $('#collapseOne').removeClass('disabledbutton');
            $('#collapseTwo').addClass('disabledbutton');
            $('#collapseTwo').css('display', 'none');
            $('#collapseFour').addClass('disabledbutton');
            $('#collapseOne').slideToggle('fast');
        });

        function setDateCont(id) {
            date = $('#'+id).attr('data-content');
            $('#date_cont').val(date);
        }

        function setTimeCont(id) {
            time = $('#time_cont').val();
            date = $('#date_cont').val();
            D = new Date(date+' 00:00:00');
            new_time = $('#'+id).attr('data-content');
            val = D.toLocaleTimeString('en-GB', D.setMinutes(D.getMinutes()+Number(new_time)));
            $("#next_time").val(val);
        }

        function setTimeCont2(id) {
            time = $('#time_cont').val();
            date = $('#date_cont').val();
            D = new Date(date+' 00:00:00');
            new_time = $('#'+id).attr('data-content');
            val = D.toLocaleTimeString('en-GB', D.setMinutes(D.getMinutes()+Number(new_time)));
            $("#durationTime").val(val);
        }

        function setDateCont2(id) {
            date = $('#'+id).attr('data-content');
            $('#date_cont_2').val(date);
        }

        function setTimeDuration(id) {
            time = '00:00';
            date = $('#date_cont').val();
            D = new Date(date+' '+time);
            new_time = $('#'+id).attr('data-content');
            val = D.toLocaleTimeString('en-GB', D.setMinutes(D.getMinutes()+Number(new_time)));
            $("#time_duration").val(val);
        }

        function select_task() {
            $(this).remove();
            $.ajax({
                'url': '/print_form/group-tasks/get-all-tasks',
                success: function (resp) {
                    content = '<select id="select-task" class="form-control">';
                    for (i=0; i<resp.length; i++) {
                        content += '<option>'+resp[i].name+'</option>';
                    }
                    content += '</select>';

                    $('#task-name').html(content);

                    $("#select-task").on('blur', function () {
                        text = $("#select-task").val();
                        $(this).remove();
                        $('#task-name').html("<span onclick='select_task()' style='text-decoration: underline; cursor: pointer;'>"+text+"</span>");
                    });
                }
            });
        };

        function select_task_dop() {
            $(this).remove();
            $.ajax({
                'url': '/print_form/group-tasks/get-all-tasks',
                success: function (resp) {
                    content = '<select id="select-task" class="form-control">';
                    for (i=0; i<resp.length; i++) {
                        content += '<option>'+resp[i].name+'</option>';
                    }
                    content += '</select>';

                    $('#task-name-dop').html(content);

                    $("#select-task").on('blur', function () {
                        text = $("#select-task").val();
                        $(this).remove();
                        $('#task-name-dop').html("<span onclick='select_task_dop()' style='text-decoration: underline; cursor: pointer;'>"+text+"</span>");
                    });
                }
            });
        };

        function select_module() {
            $(this).remove();
            $.ajax({
                'url': '/print_form/modules/get-all-modules',
                success: function (resp) {
                    content = '<select id="select-module" class="form-control">';
                    for (i=0; i<resp.length; i++) {
                        content += '<option>'+resp[i]['name']+'</option>';
                    }
                    content += '</select>';

                    $('#task-module').html(content);

                    $("#select-module").on('blur', function () {
                        text = $("#select-module").val();
                        $(this).remove();
                        $('#task-module').html("<span onclick='select_module()' style='text-decoration: underline; cursor: pointer;'>"+text+"</span>");
                    });
                }
            });
        };

        function select_task_status() {
            $(this).remove();
            $.ajax({
                'url': '/print_form/group-tasks/get-all-tasks-status',
                success: function (resp) {
                    content = '<select id="select-task-status" class="form-control">';
                    for (i=0; i<resp.length; i++) {
                        content += '<option>'+resp[i].name+'</option>';
                    }
                    content += '</select>';

                    $('#task-status').html(content);

                    $("#select-task-status").on('blur', function () {
                        text = $("#select-task-status").val();
                        $(this).remove();
                        $('#task-status').html("<span onclick='select_task_status()' style='text-decoration: underline; cursor: pointer;'>"+text+"</span>");
                    });
                }
            });
        };

        function span_num_cont() {
            val = $("#task-num-cont > input").val();
            if (!val) {
                alert('Введите кол-во контактов');
            } else {
                $("#task-num-cont").html('<span onclick="input_num_cont()" style="text-decoration: underline; cursor: pointer;">'+val+'</span>');
            }
        }

        function input_num_cont() {
            $("#task-num-cont > span").remove();
            $("#task-num-cont").append('<input onfocusOut="span_num_cont();" type="text" class="form-control">');
        }

        function select_task_group() {
            $(this).remove();
            $.ajax({
                'url': '/print_form/group-tasks/get-task-groups',
                success: function (resp) {
                    content = '<select id="select-task-group" class="form-control">';
                    for (i=0; i<resp.length; i++) {
                        content += '<option>'+resp[i]['name']+'</option>';
                    }
                    content += '</select>';

                    $('#task-group').html(content);

                    $("#select-task-group").on('blur', function () {
                        text = $("#select-task-group").val();
                        $(this).remove();
                        $('#task-group').html("<span onclick='select_task_group()' style='text-decoration: underline; cursor: pointer;'>"+text+"</span>");
                    });
                }
            });
        };

        function showTasks() {
            $('#tasks').slideToggle("fast");
        }
    </script>
@endpush