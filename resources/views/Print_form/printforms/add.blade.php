@extends('layouts.cabinet')

@section('title')
    Задачи для отдела разработки форм: Новая задача
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="box" style="padding: 15px;">
        <form action="/print_form/print-form/add" method="post" id="formAddTask" enctype="multipart/form-data">
            <input type="hidden" name="typeTask">
            <input type="hidden" name="isPrintForm">
            <textarea name="comment" style="display: none;" cols="30" rows="10"></textarea>
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs" id="typeTask">
                    <li class="active"><a href="#tab_1" id="introduction" data-toggle="tab" aria-expanded="true">Внедрение</a></li>
                    <li><a href="#tab_2" id="single_task" data-toggle="tab" aria-expanded="false">Разовая задача</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-condensed">
                                    <tr>
                                        <td width="40%" style="vertical-align: middle;">Номер задачи</td>
                                        <td colspan="2" style="vertical-align: middle;">{{ $count_task + 1 }}</td>
                                    </tr>
                                    <tr>
                                        <td width="40%" style="vertical-align: middle;">Очередь</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <input type="number" name="queue_introduction" class="form-control" value="{{ $count_task + 1 }}">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" style="vertical-align: middle;">Статус</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <select name="status_introduction" class="form-control">
                                                @foreach($status as $s)
                                                    <option value="{{ $s->id }}">{{ $s->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" style="vertical-align: middle;">Заказчик</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <select name="customer_introduction" class="form-control">
                                                @foreach($clients as $c)
                                                    <option value="{{ $c->id }}">{{ $c->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" style="vertical-align: middle;">Название проекта</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <select name="projectName_introduction" class="form-control">
                                                @foreach($projects as $p)
                                                    <option value="{{ $p->id }}">{{ $p->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Наименование задачи</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <input type="text" name="taskName_introduction" class="form-control" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Печатная\экранная форма?</td>
                                        <td style="vertical-align: middle;">
                                            <input type="radio" name="isPrintForm_introduction" id="On" value="on" checked>
                                            <label for="On">
                                                <h4 style="color: #3c8dbc;">Да</h4>
                                            </label>
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <input type="radio" name="isPrintForm_introduction" id="off" value="off">
                                            <label for="off">
                                                <h4 style="color: #3c8dbc;">Нет</h4>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Категория</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <select name="category_introduction" onchange="getCategoryNorm();" class="form-control">
                                                @foreach($category as $c)
                                                    <option value="{{ $c->id }}">{{ $c->category }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Максимальное количество часов</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <input type="number" name="maxCountHours_introduction" class="form-control">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="table table-condensed">
                                    <tr>
                                        <td width="40%" style="vertical-align: middle;">Внедренец</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <select name="implementer_introduction" class="form-control">
                                                @foreach($users as $u)
                                                    <option value="{{ $u->id }}">{{ $u->last_name }} {{ $u->first_name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Дата реализации</td>
                                        <td style="vertical-align: middle;">
                                            <input type="date" name="dateFinish_introduction" class="form-control" width="100px" required>
                                        </td>
                                        <td><a onclick="calculate()" class="btn btn-primary">Расчитать на текущий момент</a></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Дата реализации для заказчика</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <input type="date" name="dateFinishForCustomers_introduction" class="form-control" width="100px" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" style="vertical-align: middle;">Кто будет делать</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <select name="user_introduction" class="form-control">
                                                @foreach($users as $u)
                                                    <option value="{{ $u->id }}">{{ $u->last_name }} {{ $u->first_name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Платно ?</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <input name="forPaid_introduction" type="checkbox">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Оплачено ?</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <input name="paided_introduction" type="checkbox">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-condensed">
                                    <tr>
                                        <td width="40%" style="vertical-align: middle;">Номер задачи</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            {{ $count_task + 1 }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" style="vertical-align: middle;">Очередь</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <input type="number" class="form-control">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" style="vertical-align: middle;">Заказчик</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <select name="customer2" class="form-control">
                                                @foreach($clients as $c)
                                                    <option value="{{ $c->id }}">{{ $c->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Наименование задачи</td>
                                        <td colspan="2" style="vertical-align: middle;"><input type="text" class="form-control"></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Печатная\экранная форма?</td>
                                        <td style="vertical-align: middle;">
                                            <input type="radio" name="isPrintForm" id="isPrintForm">Да
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <input type="radio" name="isPrintForm" id="isPrintForm">Нет
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Категория</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <select name="category" class="form-control">
                                                @foreach($category as $c)
                                                    <option value="{{ $c->id }}">{{ $c->category }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Максимальное количество часов</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <input type="number" class="form-control">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="table table-condensed">
                                    <tr>
                                        <td style="vertical-align: middle;">Дата реализации</td>
                                        <td style="vertical-align: middle;">
                                            <input type="date" class="form-control" width="100px">
                                        </td>
                                        <td><a href="" class="btn btn-primary">Расчитать на текущий момент</a></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Дата реализации для заказчика</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <input type="date" class="form-control" width="100px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" style="vertical-align: middle;">Кто будет делать</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <select name="implementer" class="form-control">
                                                @foreach($users as $u)
                                                    <option value="{{ $u->id }}">{{ $u->last_name }} {{ $u->first_name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Платно ?</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <input type="checkbox">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <div class="form-group">
                <b>Прикрепленные файлы</b>
                <!--<div id="file-uploader">
                    <form id="upload-form" method="post" name="FileUpload" enctype="multipart/form-data">
                        <input type="file" name="img[]" multiple="multiple" />
                    </form>
                </div>-->
                <input type="file" name="img[]" accept="image/*" enctype="multipart/form-data" multiple="true" required>
            </div>
            <div class="form-group">
                <b>Комментарий</b>
                <div id="editor"></div>
            </div>

            <div class="form-group">
                <input type="submit" onclick="formAddTask_submit();" value="Сохранить" class="btn btn-success">
                <a href="/print_form/print-form" class="btn btn-default">Отмена</a>
            </div>
        </form>
    </div>
@endsection

@section('main-menu')
    @include('Print_form.main-menu')
@endsection

@push('scripts')
    <script src="{{ asset('/public/js/jquery.datetimepicker.js') }}"></script>
    <script src="{{ asset('/public/js/jquery-upload/jquery.knob.js') }}"></script>
    <script src="{{ asset('/public/js/jquery-upload/jquery.ui.widget.js') }}"></script>
    <script src="{{ asset('/public/js/jquery-upload/jquery.iframe-transport.js') }}"></script>
    <script src="{{ asset('/public/js/jquery-upload/jquery.fileupload.js') }}"></script>
    <script>
        category = $("select[name~='category_introduction'] option:selected").val();
        $.ajax({
            url: '/print_form/printform-category/get-category-norm',
            data: {'category': category},
            success: function (resp) {
                $("input[name~='maxCountHours_introduction']").val(resp);
            }
        });

        function formAddTask_submit() {
            $("#formAddTask").submit();
        }

        $(document).ready(function () {
            $("#formAddTask").validate({
                messages: {
                    taskName_introduction: {
                        required: "Это поле обязательно для заполнения"
                    },
                    dateFinishForCustomers_introduction: {
                        required: "Это поле обязательно для заполнения"
                    },
                    dateFinish_introduction: {
                        required: "Это поле обязательно для заполнения"
                    },
                    img: {
                        required: "Это поле обязательно для заполнения"
                    }
                }
            });
            $("select[name~='customer_introduction']").change(function() {
                client = $(this).find('option:selected').val();
                content = '';
                $.ajax({
                    url: '/print_form/projects/getProjectByClientId',
                    data: {'id': client},
                    success: function (data) {
                        console.log(data);
                        if (data != '') {
                            for (i=0;i<data.length;i++) {
                                content += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                            }
                            $("select[name~='projectName_introduction']").html(content);
                        } else {
                            content += '<option disabled>Проекты отсутствуют</option>';
                            $("select[name~='projectName_introduction']").html(content);
                        }
                    }
                });
            });
            $("input[name~='isPrintForm_introduction']").change(function () {
                p_form = $("input[name~='isPrintForm_introduction']:checked").val();
                if (p_form == 'off') {
                    $("select[name~='category_introduction']").attr('disabled', 'disabled');
                } else {
                    $("select[name~='category_introduction']").removeAttr('disabled');
                }
            });


            /*defaults = {
                formDataKey: "files",
                buttonText: "Прикрепить файл",
                buttonClass: "file-preview-button",
                shadowClass: "file-preview-shadow",
                tableCss: "file-preview-table",
                tableRowClass: "file-preview-row",
                placeholderClass: "file-preview-placeholder",
                loadingCss: "file-preview-loading",
                tableTemplate: function() {
                    return "<table class='table table-striped file-preview-table' id='file-preview-table'>" +
                        "<tbody></tbody>" +
                        "</table>";
                },
                rowTemplate: function(options) {
                    return "<tr class='" + config.tableRowClass + "'>" +
                        "<td>" + "<img src='" + options.src + "' class='" + options.placeholderCssClass + "' />" + "</td>" +
                        "<td class='filename'>" + options.name + "</td>" +
                        "<td class='filesize'>" + options.size + "</td>" +
                        "<td class='remove-file'><button class='btn btn-danger'>&times;</button></td>" +
                        "</tr>";
                },
                loadingTemplate: function() {
                    return "<div id='file-preview-loading-container'>" +
                        "<div id='"+config.loadingCss+"' class='loader-inner ball-clip-rotate-pulse no-show'>" +
                        "<div></div>" +
                        "<div></div>" +
                        "</div>" +
                        "</div>";
                }
            };

            //NOTE: Depends on Humanize-plus (humanize.js)
            $.getScript("https://cdnjs.cloudflare.com/ajax/libs/humanize-plus/1.5.0/humanize.min.js");
            var getFileSize = function(filesize) {
                return Humanize.fileSize(filesize);
            };

            // NOTE: Ensure a required filetype is matching a MIME type
            // (partial match is fine) and not matching against file extensions.
            //
            // Quick ref:  http://www.sitepoint.com/web-foundations/mime-types-complete-list/
            //
            // NOTE: For extended support of mime types, we should use https://github.com/broofa/node-mime
            var getFileTypeCssClass = function(filetype) {
                var fileTypeCssClass;
                fileTypeCssClass = (function() {
                    switch (true) {
                        case /video/.test(filetype):
                            return 'video';
                        case /audio/.test(filetype):
                            return 'audio';
                        case /pdf/.test(filetype):
                            return 'pdf';
                        case /csv|excel/.test(filetype):
                            return 'spreadsheet';
                        case /powerpoint/.test(filetype):
                            return 'powerpoint';
                        case /msword|text/.test(filetype):
                            return 'document';
                        case /zip/.test(filetype):
                            return 'zip';
                        case /rar/.test(filetype):
                            return 'rar';
                        default:
                            return 'default-filetype';
                    }
                })();
                return defaults.placeholderClass + " " + fileTypeCssClass;
            };

            $.fn.uploadPreviewer = function(options, callback) {
                var that = this;

                if (!options) {
                    options = {};
                }
                config = $.extend({}, defaults, options);
                var buttonText,
                    previewRowTemplate,
                    previewTable,
                    previewTableBody,
                    previewTableIdentifier,
                    currentFileList = [];

                if (window.File && window.FileReader && window.FileList && window.Blob) {

                    this.wrap("<span class='btn btn-primary " + config.shadowClass + "'></span>");
                    buttonText = this.parent("." + config.shadowClass);
                    buttonText.prepend("<span>" + config.buttonText + "</span>");
                    buttonText.wrap("<span class='" + config.buttonClass + "'></span>");

                    previewTableIdentifier = options.preview_table;
                    if (!previewTableIdentifier) {
                        $("span." + config.buttonClass).after(config.tableTemplate());
                        previewTableIdentifier = "table." + config.tableCss;
                    }

                    previewTable = $(previewTableIdentifier);
                    previewTable.addClass(config.tableCss);
                    previewTableBody = previewTable.find("tbody");

                    previewRowTemplate = options.preview_row_template || config.rowTemplate;

                    previewTable.after(config.loadingTemplate());

                    previewTable.on("click", ".remove-file", function() {
                        var parentRow = $(this).parent("tr");
                        var filename = parentRow.find(".filename").text();
                        for (var i = 0; i < currentFileList.length; i++) {
                            if (currentFileList[i].name == filename) {
                                currentFileList.splice(i, 1);
                                break;
                            }
                        }
                        parentRow.remove();
                    });

                    this.on('change', function(e) {
                        var loadingSpinner = $("#" + config.loadingCss);
                        loadingSpinner.show();

                        var reader;
                        var filesCount = e.currentTarget.files.length;
                        $.each(e.currentTarget.files, function(index, file) {
                            currentFileList.push(file);

                            reader = new FileReader();
                            reader.onload = function(fileReaderEvent) {
                                var filesize, filetype, imagePreviewRow, placeholderCssClass, source;
                                if (previewTableBody) {
                                    filetype = file.type;
                                    if (/image/.test(filetype)) {
                                        source = fileReaderEvent.target.result;
                                        placeholderCssClass = config.placeholderClass + " image";
                                    } else {
                                        source = "";
                                        placeholderCssClass = getFileTypeCssClass(filetype);
                                    }
                                    filesize = getFileSize(file.size);
                                    imagePreviewRow = previewRowTemplate({
                                        src: source,
                                        name: file.name,
                                        placeholderCssClass: placeholderCssClass,
                                        size: filesize
                                    });

                                    previewTableBody.append(imagePreviewRow);

                                    if (index == filesCount - 1) {
                                        loadingSpinner.hide();
                                    }
                                }
                                if (callback) {
                                    callback(fileReaderEvent);
                                }
                            };
                            reader.readAsDataURL(file);
                        });
                    });

                    this.fileList = function() {
                        return currentFileList;
                    };

                    this.url = function(url) {
                        if (url != undefined) {
                            config.url = url;
                        } else {
                            return config.url;
                        }
                    };

                    this._onComplete = function(eventData) {
                        $.event.trigger('file-preview:submit:complete', eventData);
                    };

                    this.submit = function(successCallback, errorCallback) {
                        //if (config.url == undefined) throw('Please set the URL to which I shall post the files');

                        if (currentFileList.length > 0) {
                            var filesFormData = new FormData();
                            currentFileList.forEach(function(file) {
                                filesFormData.append("img[]", file);
                            });

                            $.ajax({
                                type: "POST",
                                url: '/print_form/print-form/add',
                                headers: {
                                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                                },
                                data: filesFormData,
                                contentType:false,
                                cache: false,
                                processData:false,
                                success: function(data, status, jqXHR) {
                                    if (typeof successCallback == "function") {
                                        successCallback(data, status, jqXHR);
                                    }
                                    that._onComplete({ data: data, status: status, jqXHR: jqXHR });
                                },
                                error: function(jqXHR, status, error) {
                                    if (typeof errorCallback == "function") {
                                        errorCallback(jqXHR, status, error);
                                    }
                                    that._onComplete({ error: error, status: status, jqXHR: jqXHR });
                                }
                            });
                        } else {
                            console.log("There are no selected files, please select at least one file before submitting.");
                            that._onComplete({ status: 'no-files' });
                        }
                    }

                    return this;

                } else {
                    throw "The File APIs are not fully supported in this browser.";
                }
            };

            myUploadInput = $("input[type=file]").uploadPreviewer();*/

            /*Add new catagory Event*/
            $('#formAddTask').submit(function () {
                comment = CKEDITOR.instances.editor.getData();
                $('textarea[name~="comment"]').html(comment);

                typeTask = $('#typeTask li.active a').attr('id');
                $('input[name="typeTask"]').val(typeTask);

                isPrintForm = $("input[name~='isPrintForm_introduction']:checked").val();
                $('input[name="isPrintForm"]').val(isPrintForm);

                return true;
            });

            $("input[name~='isPrintForm_introduction']").on('change', function() {
                isPrintForm = $("input[name~='isPrintForm_introduction']:checked").val();

                if (isPrintForm == 'off') {
                    $("input[name~='category_introduction']").attr('disabled', 'true');
                } else {
                    $("input[name~='category_introduction']").removeAttr('disabled');
                }
            });
        });
        
        function getCategoryNorm() {
            category = $("select[name~='category_introduction'] option:selected").val();
            $.ajax({
                url: '/print_form/printform-category/get-category-norm',
                data: {'category': category},
                success: function (resp) {
                    $("input[name~='maxCountHours_introduction']").val(resp);
                }
            });
        }
        
        function calculate() {
            $.ajax({
                url: '/print_form/calculate-print-form',
                success: function (resp) {
                    console.log(resp);
                    $("input[name~='dateFinish_introduction']").val(resp);
                }
            });
        }

        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor')
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        });
        $(function(){

            var ul = $('#upload ul');

            $('#drop a').click(function(){
                // имитация нажатия на поле выбора файла
                $(this).parent().find('input').click();
            });

            // инициализация плагина jQuery File Upload
            $('#upload').fileupload({

                // этот элемент будет принимать перетаскиваемые на него файлы
                dropZone: $('#drop'),

                // Функция будет вызвана при помещении файла в очередь
                add: function (e, data) {

                    var tpl = $('<li><input type="text" value="0" data-width="48" data-height="48"'+
                        ' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" /><p></p><span></span></li>');

                    // вывод имени и размера файла
                    tpl.find('p').text(data.files[0].name)
                        .append('<i>' + formatFileSize(data.files[0].size) + '</i>');

                    data.context = tpl.appendTo(ul);

                    // инициализация плагина jQuery Knob
                    tpl.find('input').knob();

                    // отслеживание нажатия на иконку отмены
                    tpl.find('span').click(function(){

                        if(tpl.hasClass('working')){
                            jqXHR.abort();
                        }

                        tpl.fadeOut(function(){
                            tpl.remove();
                        });

                    });

                    // Автоматически загружаем файл при добавлении в очередь
                    var jqXHR = data.submit();
                },

                progress: function(e, data){

                    // Вычисление процента загрузки
                    var progress = parseInt(data.loaded / data.total * 100, 10);

                    // обновляем шкалу
                    data.context.find('input').val(progress).change();

                    if(progress == 100){
                        data.context.removeClass('working');
                    }
                },

                fail:function(e, data){
                    // что-то пошло не так
                    data.context.addClass('error');
                }

            });

            $(document).on('drop dragover', function (e) {
                e.preventDefault();
            });

            // вспомогательная функция, которая форматирует размер файла
            function formatFileSize(bytes) {
                if (typeof bytes !== 'number') {
                    return '';
                }

                if (bytes >= 1000000000) {
                    return (bytes / 1000000000).toFixed(2) + ' GB';
                }

                if (bytes >= 1000000) {
                    return (bytes / 1000000).toFixed(2) + ' MB';
                }

                return (bytes / 1000).toFixed(2) + ' KB';
            }

        });
    </script>
@endpush