@extends('layouts.cabinet')

@section('title')
    Задачи для отдела разработки форм
@endsection

@section('content')
    @if(session('perm')['print_form.create'])
        <div style="padding-bottom: 20px;">
            <a href="/print_form/print-form/add" class="btn btn-primary">Добавить</a>
        </div>
    @endif
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#raw" data-toggle="tab" aria-expanded="true">Необработанные</a></li>
            <li><a href="#contact" data-toggle="tab" aria-expanded="false">Горящие задачи</a></li>
            <li><a href="#callback" data-toggle="tab" aria-expanded="false">По очереди</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="raw">
                <div class="pull-right" style="margin-bottom: 15px;">
                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="raw_form_input" class="form-control pull-right" placeholder="Поиск">

                            <div class="input-group-btn">
                                <button onclick="raw_form_search();" type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box" style="overflow: hidden;">
                    <table class="table table-hover" id="tableRawForms">
                        <thead>
                        <th style="vertical-align: middle;">Заказчик</th>
                        <th style="vertical-align: middle;">Проект</th>
                        <th style="vertical-align: middle;">Наименование работы</th>
                        <th style="vertical-align: middle;">Статус</th>
                        <th style="vertical-align: middle;">Внедренец</th>
                        <th style="vertical-align: middle;" colspan="1">Действия</th>
                        </thead>
                        <tbody>
                        @if ($form_raw[0] != null)
                            @foreach($form_raw as $r)
                                <tr>
                                    <td>{{ $r['client'] }}</td>
                                    <td>{{ $r['project'] }}</td>
                                    <td>{{ $r['name'] }}</td>
                                    <td>{{ $r['status'] }}</td>
                                    <td>{{ $r['observer'] }}</td>
                                    <td style="vertical-align: middle;">
                                        <a href="/print_form/print-form/raw/{{ $r['id'] }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6" style="text-align: center; font-style: italic;">Ни чего не найдено</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.tab-pane -->
            </div>
            <div class="tab-pane" id="contact">
                <div style="margin-bottom: 10px; float: left;">
                    <a onclick="slideFilter('contact');" class="btn btn-success" style="cursor: pointer;">
                        <span>Фильтр</span>
                        <i class="fa fa-angle-down pull-right" style="width: 20px; margin-top: 3px;"></i>
                    </a>
                    <a onclick="filterContact('all');" style="cursor: pointer; margin-left: 15px;">Все</a>
                    <a onclick="filterContact('myForms');" style="cursor: pointer; margin-left: 15px;">Мои формы</a>
                    <a onclick="filterContact('myCloseForms');" style="cursor: pointer; margin-left: 15px;">Мои закрытые формы</a>
                </div>
                <div class="pull-right" style="margin-bottom: 10px;">
                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="hot_form_input" class="form-control pull-right" placeholder="Поиск">

                            <div class="input-group-btn">
                                <button onclick="hot_form_search();" type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div id="filter_contact" style="display: none; margin-bottom: 10px; border: 1px solid silver; padding: 10px;">
                    <table class="table table-striped" id="tableFilterHotForms">
                        <tr>
                            <td>Статус</td>
                            <td>
                                <select name="filter_status" id="filter_status" class="form-control">
                                    <option value="0">Все</option>
                                    @foreach($all_statuses as $statuses)
                                        <option value="{{ $statuses->id }}">{{ $statuses->name }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Исполнитель</td>
                            <td>
                                <select name="filter_user" id="filter_user" class="form-control">
                                    <option value="0">Все</option>
                                    @foreach($performers as $performer)
                                        <option value="{{ $performer->id }}">{{ $performer->last_name }} {{ $performer->first_name }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                    </table>
                    <a onclick="applyFilter('contact');" style="cursor: pointer; margin-right: 15px;" class="btn btn-success">Применить</a>
                    <a onclick="clearFilter('contact');" style="cursor: pointer; color: #3c8dbc;">Сбросить</a>
                </div>
                <div class="box" style="overflow: hidden; overflow-x: scroll;">
                    <table class="table table-hover" id="tableHotForms">
                        <thead>
                        <th style="vertical-align: middle;">Номер задачи</th>
                        <th style="vertical-align: middle;">Очередь</th>
                        <th style="vertical-align: middle;">Заказчик</th>
                        <th style="vertical-align: middle;">Проект</th>
                        <th style="vertical-align: middle;">Наименование работ</th>
                        <th style="vertical-align: middle;">Максимальное количество часов</th>
                        <th style="vertical-align: middle;">Внедренец</th>
                        <th style="vertical-align: middle;">Дополнительная плата</th>
                        <th style="vertical-align: middle;">Дата постановки</th>
                        <th style="vertical-align: middle;">Дата реализации</th>
                        <th style="vertical-align: middle;">Дата реализации для заказчика</th>
                        <th style="vertical-align: middle;">Статус</th>
                        <th style="vertical-align: middle;">Ответственный</th>
                        <th style="vertical-align: middle;" colspan="2">Действия</th>
                        </thead>
                        <tbody>
                        @if ($tasks_date[0] != null)
                            @foreach($tasks_date as $t)
                                <tr class="rowlink">
                                    <td class="item">{{ $t['id'] }}</td>
                                    <td>{{ $t['number'] }}</td>
                                    <td>{{ $t['client'] }}</td>
                                    <td>{{ $t['project'] }}</td>
                                    <td>{{ $t['name'] }}</td>
                                    <td>{{ $t['max_hours'] }}</td>
                                    <td>{{ $t['observer'] }}</td>
                                    @if($t['is_free'])
                                        <td>Да</td>
                                    @else
                                        <td>Нет</td>
                                    @endif
                                    <td>{{ $t['date'] }}</td>
                                    <td>{{ $t['date_implement'] }}</td>
                                    <td>{{ $t['date_implement_for_client'] }}</td>
                                    <td>{{ $t['status'] }}</td>
                                    <td>{{ $t['user'] }}</td>
                                    <td style="vertical-align: middle;">
                                        <a href="/print_form/print-form/edit/{{ $t['id'] }}" class="btn btn-primary">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <a href="/print_form/print-form/del/{{ $t['id'] }}" class="btn btn-danger">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="12" style="text-align: center; font-style: italic;">Ни чего не найдено</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.tab-pane -->
            </div>
            <div class="tab-pane" id="callback">
                <div style="margin-bottom: 10px; float: left;">
                    <a onclick="slideFilter('callback');" class="btn btn-success" style="cursor: pointer;">
                        <span>Фильтр</span>
                        <i class="fa fa-angle-down pull-right" style="width: 20px; margin-top: 3px;"></i>
                    </a>
                    <a onclick="filterCallback('all');" style="cursor: pointer; margin-left: 15px;">Все</a>
                    <a onclick="filterCallback('myForms');" style="cursor: pointer; margin-left: 15px;">Мои формы</a>
                    <a onclick="filterCallback('myCloseForms');" style="cursor: pointer; margin-left: 15px;">Мои закрытые формы</a>
                </div>
                <div class="pull-right" style="margin-bottom: 10px;">
                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="queue_form_input" class="form-control pull-right" placeholder="Поиск">

                            <div class="input-group-btn">
                                <button onclick="queue_form_search();" type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div id="filter_callback" style="display: none; margin-bottom: 10px; border: 1px solid silver; padding: 10px;">
                    <table class="table table-striped" id="tableFilterQueueForms">
                        <tr>
                            <td>Статус</td>
                            <td>
                                <select name="filter_status" id="filter_status" class="form-control">
                                    <option value="0">Все</option>
                                    @foreach($all_statuses as $statuses)
                                        <option value="{{ $statuses->id }}">{{ $statuses->name }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Исполнитель</td>
                            <td>
                                <select name="filter_user" id="filter_user" class="form-control">
                                    <option value="0">Все</option>
                                    @foreach($performers as $performer)
                                        <option value="{{ $performer->id }}">{{ $performer->last_name }} {{ $performer->first_name }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                    </table>
                    <a onclick="applyFilter('callback');" style="cursor: pointer; margin-right: 15px;" class="btn btn-success">Применить</a>
                    <a onclick="clearFilter('callback');" style="cursor: pointer; color: #3c8dbc;">Сбросить</a>
                </div>
                <div class="box" style="overflow: hidden; overflow-x: scroll;">
                    <table class="table table-hover" id="tableQueueForms">
                        <thead>
                        <th style="vertical-align: middle;">Номер задачи</th>
                        <th style="vertical-align: middle;">Очередь</th>
                        <th style="vertical-align: middle;">Заказчик</th>
                        <th style="vertical-align: middle;">Проект</th>
                        <th style="vertical-align: middle;">Наименование работ</th>
                        <th style="vertical-align: middle;">Максимальное количество часов</th>
                        <th style="vertical-align: middle;">Внедренец</th>
                        <th style="vertical-align: middle;">Дополнительная плата</th>
                        <th style="vertical-align: middle;">Дата постановки</th>
                        <th style="vertical-align: middle;">Дата реализации</th>
                        <th style="vertical-align: middle;">Дата реализации для заказчика</th>
                        <th style="vertical-align: middle;">Статус</th>
                        <th style="vertical-align: middle;">Ответственный</th>
                        <th style="vertical-align: middle;" colspan="2">Действия</th>
                        </thead>
                        <tbody>
                        @if ($tasks_number[0] != null)
                            @foreach($tasks_number as $t)
                                <tr>
                                    <td>{{ $t['id'] }}</td>
                                    <td>{{ $t['number'] }}</td>
                                    <td>{{ $t['client'] }}</td>
                                    <td>{{ $t['project'] }}</td>
                                    <td>{{ $t['name'] }}</td>
                                    <td>{{ $t['max_hours'] }}</td>
                                    <td>{{ $t['observer'] }}</td>
                                    @if($t['is_free'])
                                        <td>Да</td>
                                    @else
                                        <td>Нет</td>
                                    @endif
                                    <td>{{ $t['date'] }}</td>
                                    <td>{{ $t['date_implement'] }}</td>
                                    <td>{{ $t['date_implement_for_client'] }}</td>
                                    <td>{{ $t['status'] }}</td>
                                    <td>{{ $t['user'] }}</td>
                                    <td style="vertical-align: middle;">
                                        <a href="/print_form/print-form/edit/{{ $t['id'] }}" class="btn btn-primary">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <a href="/print_form/print-form/del/{{ $t['id'] }}" class="btn btn-danger">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="12" style="text-align: center; font-style: italic;">Ни чего не найдено</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
    </div>
@endsection

@section('main-menu')
    @include('Print_form.main-menu')
@endsection

@push('scripts')
    <script>
        document.oncontextmenu = function() {return false;};
        $(document).ready(function () {
            $('#tableHotForms .rowlink').mousedown(function(event) {                  // Убираем css класс selected-html-element у абсолютно всех элементов на странице с помощью селектора "*":
                 $('*').removeClass('selected-html-element');         // Удаляем предыдущие вызванное контекстное меню:
                 id = $(this).find('.item').html();
                 $('.context-menu').remove();                  // Проверяем нажата ли именно правая кнопка мыши:
                 if (event.which === 3)  {                          // Получаем элемент на котором был совершен клик:
                     var target = $(event.target);                          // Добавляем класс selected-html-element что бы наглядно показать на чем именно мы кликнули (исключительно для тестирования):
                     target.addClass('selected-html-element');              // Создаем меню:
                     $('<div/>', {
                         class: 'context-menu' // Присваиваем блоку наш css класс контекстного меню:
                     }).css({
                         left: event.pageX+'px', // Задаем позицию меню на X
                         top: event.pageY+'px' // Задаем позицию меню по Y
                     }).appendTo('body') // Присоединяем наше меню к body документа:
                     .append( // Добавляем пункты меню:
                         $('<ul/>').append('<li><a href="/print_form/print-form/edit/'+id+'">Перейти</a></li>')
                             .append('<li><a onclick="fastEdit('+id+')" style="cursor: pointer;">Быстрое редактирование</a></li>')
                     ).show('fast'); // Показываем меню с небольшим стандартным эффектом jQuery. Как раз очень хорошо подходит для меню
                 }
            });

            $("#tableFilterHotForms #filter_status").change(function () {
                $("#tableFilterHotForms #filter_status option:first").removeAttr("selected");
            });

            $("#tableFilterHotForms #filter_user").change(function () {
                $("#tableFilterHotForms #filter_user option:first").removeAttr("selected");
            });
        });

        function fastEdit(id) {
            $('.context-menu').remove();
            //alert(id);
        }

        $(function(){
            $('#tableHotForms').on('dblclick', '.rowlink', function(){
                id = $(this).find('.item').html();
                location.href = '/print_form/print-form/edit/'+id;
            });
        });

        function slideFilter(page) {
            $("#filter_"+page).slideToggle('fast');
        }

        function filterContact(flag) {
            content = '';
            $.ajax({
                url: '/print_form/print-form/filter_form',
                data: {'flag': flag},
                success: function (resp) {
                    $("#tableHotForms tbody tr").remove();
                    console.log(resp);
                    if (resp != '') {
                        for(i=0;i<resp.length;i++) {
                            content += '<tr>' +
                                '<td style="vertical-align: middle;">'+resp[i].id+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].number+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].client+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].project+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].name+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].max_hours+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].observer+'</td>';
                            if (resp[i].is_free == 0) {
                                content += '<td style="vertical-align: middle;">Нет</td>';
                            } else {
                                content += '<td style="vertical-align: middle;">Да</td>';
                            }
                            content += '<td style="vertical-align: middle;">'+resp[i].date+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].date_implement+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].date_implement_for_client+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].status+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].user+'</td>' +
                                '<td style="vertical-align: middle;"><a href="/print_form/print-form/edit/'+resp[i].id+'" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>' +
                                '</tr>';
                        }
                    } else {
                        content += '<tr>' +
                            '<td colspan="11" style="vertical-align: middle; text-align: center;"><i>По вашему запросу ни чего не найдено</i></td>' +
                            '</tr>';
                    }

                    $("#tableHotForms tbody").append(content);
                }
            });
        }

        function filterCallback(flag) {
            content = '';
            $.ajax({
                url: '/print_form/print-form/filter_form_callback',
                data: {'flag': flag},
                success: function (resp) {
                    $("#tableQueueForms tbody tr").remove();
                    console.log(resp);
                    if (resp != '') {
                        for(i=0;i<resp.length;i++) {
                            content += '<tr>' +
                                '<td style="vertical-align: middle;">'+resp[i].id+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].number+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].client+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].project+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].name+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].max_hours+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].observer+'</td>';
                            if (resp[i].is_free == 0) {
                                content += '<td style="vertical-align: middle;">Нет</td>';
                            } else {
                                content += '<td style="vertical-align: middle;">Да</td>';
                            }
                            content += '<td style="vertical-align: middle;">'+resp[i].date+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].date_implement+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].date_implement_for_client+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].status+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].user+'</td>' +
                                '<td style="vertical-align: middle;"><a href="/print_form/print-form/edit/'+resp[i].id+'" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>' +
                                '</tr>';
                        }
                    } else {
                        content += '<tr>' +
                            '<td colspan="11" style="vertical-align: middle; text-align: center;"><i>По вашему запросу ни чего не найдено</i></td>' +
                            '</tr>';
                    }

                    $("#tableQueueForms tbody").append(content);
                }
            });
        }

        function raw_form_search() {
            text = $("input[name~='raw_form_input']").val();

            content = '';
            $.ajax({
                url: '/print_form/print-form/raw_search',
                data: {'text': text},
                success: function (resp) {
                    $("#tableRawForms tbody tr").remove();
                    console.log(resp);
                    if (resp != '') {
                        for(i=0;i<resp.length;i++) {
                            content += '<tr>' +
                                '<td style="vertical-align: middle;">'+resp[i].client+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].project+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].name+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].status+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].observer+'</td>' +
                                '<td style="vertical-align: middle;"><a href="/print_form/print-form/raw/'+resp[i].id+'" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>' +
                                '</tr>';
                        }
                    } else {
                        content += '<tr>' +
                            '<td colspan="11" style="vertical-align: middle; text-align: center;"><i>По вашему запросу ни чего не найдено</i></td>' +
                            '</tr>';
                    }

                    $("#tableRawForms tbody").append(content);
                }
            });
        }

        function hot_form_search() {
            text = $("input[name~='hot_form_input']").val();

            content = '';
            $.ajax({
                url: '/print_form/print-form/hot_search',
                data: {'text': text},
                success: function (resp) {
                    $("#tableHotForms tbody tr").remove();
                    console.log(resp);
                    if (resp != '') {
                        for(i=0;i<resp.length;i++) {
                            content += '<tr>' +
                                '<td style="vertical-align: middle;">'+resp[i].id+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].number+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].client+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].project+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].name+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].max_hours+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].observer+'</td>';
                            if (resp[i].is_free == 1) {
                                content += '<td style="vertical-align: middle;">Да</td>';
                            } else {
                                content += '<td style="vertical-align: middle;">Нет</td>';
                            }
                            content += '<td style="vertical-align: middle;">'+resp[i].date2.date+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].date_implement+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].date_implement_for_client+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].status+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].user+'</td>' +
                                '<td style="vertical-align: middle;"><a href="/print_form/print-form/edit/'+resp[i].id+'" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>' +
                                '<td style="vertical-align: middle;"><a href="/print_form/print-form/del/'+resp[i].id+'" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>' +
                                '</tr>';
                        }
                    } else {
                        content += '<tr>' +
                            '<td colspan="11" style="vertical-align: middle; text-align: center;"><i>По вашему запросу ни чего не найдено</i></td>' +
                            '</tr>';
                    }

                    $("#tableHotForms tbody").append(content);
                }
            });
        }

        function queue_form_search() {
            text = $("input[name~='queue_form_input']").val();

            content = '';
            $.ajax({
                url: '/print_form/print-form/queue_search',
                data: {'text': text},
                success: function (resp) {
                    $("#tableQueueForms tbody tr").remove();
                    console.log(resp);
                    if (resp != '') {
                        for(i=0;i<resp.length;i++) {
                            content += '<tr>' +
                                '<td style="vertical-align: middle;">'+resp[i].id+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].number+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].client+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].project+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].name+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].max_hours+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].observer+'</td>';
                            if (resp[i].is_free == 1) {
                                content += '<td style="vertical-align: middle;">Да</td>';
                            } else {
                                content += '<td style="vertical-align: middle;">Нет</td>';
                            }
                            content += '<td style="vertical-align: middle;">'+resp[i].date2.date+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].date_implement+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].date_implement_for_client+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].status+'</td>' +
                                '<td style="vertical-align: middle;">'+resp[i].user+'</td>' +
                                '<td style="vertical-align: middle;"><a href="/print_form/print-form/edit/'+resp[i].id+'" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>' +
                                '<td style="vertical-align: middle;"><a href="/print_form/print-form/del/'+resp[i].id+'" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>' +
                                '</tr>';
                        }
                    } else {
                        content += '<tr>' +
                            '<td colspan="11" style="vertical-align: middle; text-align: center;"><i>По вашему запросу ни чего не найдено</i></td>' +
                            '</tr>';
                    }

                    $("#tableQueueForms tbody").append(content);
                }
            });
        }

        function applyFilter(page) {
            switch (page) {
                case 'contact':
                    status = $("#tableFilterHotForms #filter_status option:selected").val();
                    user = $("#tableFilterHotForms #filter_user option:selected").val();

                    content = '';
                    $.ajax({
                        url: '/print_form/print-form/hot_filter',
                        data: {'status': status, 'user': user},
                        success: function (resp) {
                            $("#tableHotForms tbody tr").remove();
                            console.log(resp);
                            if (resp != '') {
                                for(i=0;i<resp.length;i++) {
                                    content += '<tr>' +
                                        '<td style="vertical-align: middle;">'+resp[i].id+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].number+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].client+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].project+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].name+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].max_hours+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].observer+'</td>';
                                    if (resp[i].is_free == 1) {
                                        content += '<td style="vertical-align: middle;">Да</td>';
                                    } else {
                                        content += '<td style="vertical-align: middle;">Нет</td>';
                                    }
                                    content += '<td style="vertical-align: middle;">'+resp[i].date2.date+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].date_implement+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].date_implement_for_client+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].status+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].user+'</td>' +
                                        '<td style="vertical-align: middle;"><a href="/print_form/print-form/edit/'+resp[i].id+'" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>' +
                                        '<td style="vertical-align: middle;"><a href="/print_form/print-form/del/'+resp[i].id+'" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>' +
                                        '</tr>';
                                }
                            } else {
                                content += '<tr>' +
                                    '<td colspan="11" style="vertical-align: middle; text-align: center;"><i>По вашему запросу ни чего не найдено</i></td>' +
                                    '</tr>';
                            }

                            $("#tableHotForms tbody").append(content);
                        }
                    });
                    break;
                case 'callback':
                    status = $("#tableFilterQueueForms #filter_status option:selected").val();
                    user = $("#tableFilterQueueForms #filter_user option:selected").val();

                    content = '';
                    $.ajax({
                        url: '/print_form/print-form/queue_filter',
                        data: {'status': status, 'user': user},
                        success: function (resp) {
                            $("#tableQueueForms tbody tr").remove();
                            console.log(resp);
                            if (resp != '') {
                                for(i=0;i<resp.length;i++) {
                                    content += '<tr>' +
                                        '<td style="vertical-align: middle;">'+resp[i].id+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].number+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].client+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].project+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].name+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].max_hours+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].observer+'</td>';
                                    if (resp[i].is_free == 1) {
                                        content += '<td style="vertical-align: middle;">Да</td>';
                                    } else {
                                        content += '<td style="vertical-align: middle;">Нет</td>';
                                    }
                                    content += '<td style="vertical-align: middle;">'+resp[i].date2.date+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].date_implement+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].date_implement_for_client+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].status+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].user+'</td>' +
                                        '<td style="vertical-align: middle;"><a href="/print_form/print-form/edit/'+resp[i].id+'" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>' +
                                        '<td style="vertical-align: middle;"><a href="/print_form/print-form/del/'+resp[i].id+'" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>' +
                                        '</tr>';
                                }
                            } else {
                                content += '<tr>' +
                                    '<td colspan="11" style="vertical-align: middle; text-align: center;"><i>По вашему запросу ни чего не найдено</i></td>' +
                                    '</tr>';
                            }

                            $("#tableQueueForms tbody").append(content);
                        }
                    });
                    break;
            }
        }

        function clearFilter(page) {
            switch (page) {
                case 'contact':
                    $("#tableFilterHotForms #filter_status option:first").attr("selected", "selected");
                    $("#tableFilterHotForms #filter_user option:first").attr("selected", "selected");

                    status = 0;
                    user = 0;

                    content = '';
                    $.ajax({
                        url: '/print_form/print-form/hot_filter',
                        data: {'status': status, 'user': user},
                        success: function (resp) {
                            $("#tableHotForms tbody tr").remove();
                            if (resp != '') {
                                for(i=0;i<resp.length;i++) {
                                    content += '<tr>' +
                                        '<td style="vertical-align: middle;">'+resp[i].id+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].number+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].client+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].project+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].name+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].max_hours+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].observer+'</td>';
                                    if (resp[i].is_free == 1) {
                                        content += '<td style="vertical-align: middle;">Да</td>';
                                    } else {
                                        content += '<td style="vertical-align: middle;">Нет</td>';
                                    }
                                    content += '<td style="vertical-align: middle;">'+resp[i].date2.date+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].date_implement+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].date_implement_for_client+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].status+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].user+'</td>' +
                                        '<td style="vertical-align: middle;"><a href="/print_form/print-form/edit/'+resp[i].id+'" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>' +
                                        '<td style="vertical-align: middle;"><a href="/print_form/print-form/del/'+resp[i].id+'" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>' +
                                        '</tr>';
                                }
                            } else {
                                content += '<tr>' +
                                    '<td colspan="11" style="vertical-align: middle; text-align: center;"><i>По вашему запросу ни чего не найдено</i></td>' +
                                    '</tr>';
                            }

                            $("#tableHotForms tbody").append(content);
                        }
                    });
                    break;
                case 'callback':
                    $("#tableFilterQueueForms #filter_status option:first").attr("selected", "selected");
                    $("#tableFilterQueueForms #filter_user option:first").attr("selected", "selected");

                    status = 0;
                    user = 0;

                    content = '';
                    $.ajax({
                        url: '/print_form/print-form/queue_filter',
                        data: {'status': status, 'user': user},
                        success: function (resp) {
                            $("#tableQueueForms tbody tr").remove();
                            if (resp != '') {
                                for(i=0;i<resp.length;i++) {
                                    content += '<tr>' +
                                        '<td style="vertical-align: middle;">'+resp[i].id+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].number+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].client+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].project+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].name+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].max_hours+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].observer+'</td>';
                                    if (resp[i].is_free == 1) {
                                        content += '<td style="vertical-align: middle;">Да</td>';
                                    } else {
                                        content += '<td style="vertical-align: middle;">Нет</td>';
                                    }
                                    content += '<td style="vertical-align: middle;">'+resp[i].date2.date+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].date_implement+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].date_implement_for_client+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].status+'</td>' +
                                        '<td style="vertical-align: middle;">'+resp[i].user+'</td>' +
                                        '<td style="vertical-align: middle;"><a href="/print_form/print-form/edit/'+resp[i].id+'" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>' +
                                        '<td style="vertical-align: middle;"><a href="/print_form/print-form/del/'+resp[i].id+'" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>' +
                                        '</tr>';
                                }
                            } else {
                                content += '<tr>' +
                                    '<td colspan="11" style="vertical-align: middle; text-align: center;"><i>По вашему запросу ни чего не найдено</i></td>' +
                                    '</tr>';
                            }

                            $("#tableQueueForms tbody").append(content);
                        }
                    });
                    break;
            }
        }
    </script>
@endpush