@extends('layouts.cabinet')

@section('title')
    Задачи для отдела разработки форм: Редактирование задачи
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <input type="hidden" id="project_id" value="{{ $form->project_id }}">
    <div class="box" style="padding: 15px;" id="maininfo">
        <div class="box-header with-border">
            <h3 class="box-title">Общая информация</h3>
            <div class="pull-right">
                <a onclick="edit();" style="cursor: pointer;" id="edit">Редактировать</a>
            </div>
        </div>
        <form action="/print_form/print-form/edit" method="post" id="formAddTask">
            {{ csrf_field() }}
            <input type="hidden" id="fid" value="{{ $form->id }}">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-condensed">
                        <tr>
                            <td width="40%" style="vertical-align: middle;">Номер задачи</td>
                            <td width="70%" style="vertical-align: middle;">{{ $form->id }}</td>
                        </tr>
                        <tr>
                            <td width="40%" style="vertical-align: middle;">Очередность</td>
                            <td width="70%" style="vertical-align: middle;" id="number">{{ $form->number }}</td>
                        </tr>
                        <tr>
                            <td width="40%" style="vertical-align: middle;">Статус</td>
                            <td width="70%" style="vertical-align: middle;" id="status">{{ $status }}</td>
                        </tr>
                        <tr>
                            <td width="40%" style="vertical-align: middle;">Заказчик</td>
                            <td width="70%" style="vertical-align: middle;" id="client_name">{{ $client->name }}</td>
                        </tr>
                        <tr>
                            <td width="40%" style="vertical-align: middle;">Название проекта</td>
                            <td width="70%" style="vertical-align: middle;" id="project_name">{{ $project->name }}</td>
                        </tr>
                        <tr>
                            <td width="40%" style="vertical-align: middle;">Наименование задачи</td>
                            <td width="70%" style="vertical-align: middle;" id="form_name">{{ $form->name }}</td>
                        </tr>
                        <tr>
                            <td width="40%" style="vertical-align: middle;">Печатная\экранная форма?</td>
                            @if($form->is_printform == 0)
                            <td width="70%" style="vertical-align: middle;" id="is_form">Нет</td>
                            @else
                            <td width="70%" style="vertical-align: middle;" id="is_form">Да</td>
                            @endif
                        </tr>
                        <tr>
                            <td width="40%" style="vertical-align: middle;">Категория</td>
                            <td width="70%" style="vertical-align: middle;" id="category">{{ $category['category'] }}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-condensed">
                        <tr>
                            <td width="40%" style="vertical-align: middle;">Максимальное количество часов</td>
                            <td width="70%" style="vertical-align: middle;" id="max_hour">{{ $form->max_hours }}</td>
                        </tr>
                        <tr>
                            <td width="40%" style="vertical-align: middle;">Внедренец</td>
                            <td width="70%" style="vertical-align: middle;" id="implement">{{ $implement->first_name }} {{ $implement->last_name }}</td>
                        </tr>
                        <tr>
                            <td width="40%" style="vertical-align: middle;">Дата реализации</td>
                            <td width="70%" style="vertical-align: middle;" id="date_implement">{{ $form->date_implement }}</td>
                        </tr>
                        <tr>
                            <td width="40%" style="vertical-align: middle;">Дата реализации для заказчика</td>
                            <td width="70%" style="vertical-align: middle;" id="date_implement_for_client">{{ $form->date_implement_for_client }}</td>
                        </tr>
                        <tr>
                            <td width="40%" style="vertical-align: middle;">Кто будет делать</td>
                            @if ($user != '')
                            <td width="70%" style="vertical-align: middle;" id="user">{{ $user->first_name }} {{ $user->last_name }}</td>
                            @else
                            <td width="70%" style="vertical-align: middle;" id="user">Не выбран</td>
                            @endif
                        </tr>
                        <tr>
                            <td width="40%" style="vertical-align: middle;">Платно ?</td>
                            @if($form->is_free == 1)
                                <td width="70%" style="vertical-align: middle;" id="is_paid">Да</td>
                            @else
                                <td width="70%" style="vertical-align: middle;" id="is_paid">Нет</td>
                            @endif
                        </tr>
                        <tr>
                            <td width="40%" style="vertical-align: middle;">Оплачено ?</td>
                            @if($form->paid == 0)
                                <td width="70%" style="vertical-align: middle;" id="ia_paided">Нет</td>
                            @else
                                <td width="70%" style="vertical-align: middle;" id="ia_paided">Да</td>
                            @endif
                        </tr>
                    </table>
                </div>
            </div>
        </form>
    </div>

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Прикрепленные файлы</h3>
        </div>
        <div class="box-footer">
            <ul class="mailbox-attachments clearfix">
                @if($file != '')
                    @foreach($file as $f)
                        <li>
                            <span class="mailbox-attachment-icon has-img"><img src="{{ asset($f['img']) }}"></span>

                            <div class="mailbox-attachment-info">
                                <a href="{{ asset($f['img']) }}" target="_blank" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{ $f['name'] }}</a>
                                <span class="mailbox-attachment-size">{{ $f['size'] }} KB</span>
                            </div>

                            <a href="{{ asset($f['img']) }}" class="btn btn-success" download>Скачать</a>
                            <a href="{{ asset($f['img']) }}" class="btn btn-primary" target="_blank">Просмотр</a>
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>
    </div>

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Готовые формы</h3>
        </div>
        <div class="box-footer">
            <ul class="mailbox-attachments clearfix">
                @if($done_files != '')
                    @foreach($done_files as $f)
                        <li>
                            <span class="mailbox-attachment-icon has-img"><img src="{{ asset($f['img']) }}"></span>

                            <div class="mailbox-attachment-info">
                                <a href="{{ asset($f['img']) }}" target="_blank" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{ $f['name'] }}</a>
                                <span class="mailbox-attachment-size">{{ $f['size'] }} KB</span>
                            </div>

                            <a href="{{ asset($f['img']) }}" class="btn btn-success" download>Скачать</a>
                            <a href="{{ asset($f['img']) }}" class="btn btn-primary" target="_blank">Просмотр</a>
                        </li>
                    @endforeach
                @else
                    <li style="border: none; width: 100%; text-align: center;">
                        <i>Файлы отсутствуют</i>
                    </li>
                @endif
            </ul>

            <div class="form-group">
                <form method="post" action="/print_form/print-form/upload/{{ $form->id }}" enctype="multipart/form-data">
                    <input name="_token" type="hidden" value="{{ csrf_token() }}">
                    <input type="file" multiple name="file[]">
                    <button type="submit" class="btn btn-success">Загрузить</button>
                </form>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Комментарии</h3>
        </div>
        <div class="box-body">
            <div style="padding-bottom: 20px; display: block;" id="add">
                <a href="" class="btn btn-primary" id="click_mes_form">Добавить новый коментарий</a>
            </div>
            <div class="clear"></div>
            <div class="wall_form" id="popup_message_form" style="display:none; margin-bottom: 20px;">
                <div id="editor"></div>
            </div>
            <div style="padding-bottom: 20px; display: none;" id="addComment">
                <a class="btn btn-success" id="addCommentButton" style="display: none; float: left; margin-right: 10px;">Сохранить</a>&nbsp;
                <a href="" class="btn btn-primary" style="display: none; float: left;">Отмена</a>
            </div>
            @if($comments)
                @foreach($comments as $c)
                    <div class="post">
                        <div class="user-block">
                            <img class="img-circle img-bordered-sm" src="{{ asset('public/avatars/'.$c['user_foto']) }}" alt="user image">
                            <span class="username">
                                    <a href="#">{{ $c['user_name'] }} {{ $c['user_surname'] }}</a>
                                @if($c['autor'])
                                    <a onclick="editComment({{ $c['id'] }}, '{{ $c['text'] }}')" class="pull-right btn-box-tool"><i class="fa fa-pencil"></i></a>
                                    <a href="/print_form/comment/del/{{ $c['id'] }}" class="pull-right btn-box-tool"><i class="fa fa-trash"></i></a>
                                @endif
                                </span>
                            <span class="description">Опубликовано - {{ $c['time'] }} {{ $c['date'] }}</span>
                        </div>
                        <!-- /.user-block -->
                        <div id="comment_{{ $c['id'] }}">
                            @php echo html_entity_decode($c['text']) @endphp
                        </div>
                        <!--<ul class="list-inline">
                            <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Share</a></li>
                        </ul>

                        <input class="form-control input-sm" type="text" placeholder="Type a comment">-->
                    </div>
                @endforeach
            @else
                <p style="text-align: center"><i>Комментарии отсутствуют</i></p>
            @endif
        </div>
    </div>
@endsection

@section('main-menu')
    @include('Print_form.main-menu')
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $("#click_mes_form").click(function(){
                $("#popup_message_form").slideToggle("slow");
                $(this).toggleClass("active");
                $('#add').css('display', 'none');
                $('#addComment').css('display', 'block');
                $('#addComment > a').css('display', 'block');
                return false;
            });

            $('#addCommentButton').click(function () {
                status = $('#status').text();
                comment = CKEDITOR.instances.editor.getData();
                f_id = $('#fid').val();
                if (comment) {
                    $.ajax({
                        'url': '/print_form/comments/add',
                        'data': {'text': comment, 'form_id': f_id, 'status': status},
                        success: function (resp) {
                            location.reload();
                        }
                    });
                } else {
                    alert('Коментарий не может быть пустым');
                }
            });

            $(function () {
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('editor')
                //bootstrap WYSIHTML5 - text editor
                $('.textarea').wysihtml5()
            });
        });

        function edit() {
            number = $('#number').text();
            status = $('#status').text();
            client = $('#client_name').text();
            project_name = $('#project_name').text();
            form_name = $('#form_name').text();
            is_form = $('#is_form').text();
            category = $('#category').text();
            max_hour = $('#max_hour').text();
            implement = $('#implement').text();
            date_implement = $('#date_implement').text();
            date_implement_for_client = $('#date_implement_for_client').text();
            user = $('#user').text();
            is_paid = $('#is_paid').text();
            ia_paided = $('#ia_paided').text();

            $('#number').html('<input type="number" id="new_number" class="form-control" value="'+number+'">');

            content_status = '';
            $.ajax({
                url: '/print_form/print-form/get-status',
                success: function (data) {
                    //console.log(data);
                    content_status += '<select id="new_status" class="form-control">';
                    for (i=0;i<data.length;i++) {
                        status2 = data[i].name;
                        if (status == status2) {
                            content_status += '<option id="'+data[i].id+'" selected>'+data[i].name+'</option>';
                        } else {
                            content_status += '<option id="'+data[i].id+'">'+data[i].name+'</option>';
                        }
                    }
                    content_status += '</select>';
                    $('#status').html(content_status);
                }
            });

            content_client = '';
            $.ajax({
                url: '/print_form/clients/get-client-by-project-id',
                success: function (data) {
                    content_client += '<select id="new_client" class="form-control">';
                    for (i=0;i<data.length;i++) {
                        if (client == data[i].name) {
                            content_client += '<option id="'+data[i].id+'" selected>'+data[i].name+'</option>';
                        } else {
                            content_client += '<option id="'+data[i].id+'">'+data[i].name+'</option>';
                        }
                    }
                    content_client += '</select>';
                    $('#client_name').html(content_client);
                }
            });

            content_project = '';
            $.ajax({
                url: '/print_form/projects/getProjectByClientName',
                data: {'client': client},
                success: function (data) {
                    content_project += '<select id="new_project" class="form-control">';
                    for (i=0;i<data.length;i++) {
                        if (form_name == data[i].name) {
                            content_project += '<option id="' + data[i].id + '" selected>' + data[i].name + '</option>';
                        } else {
                            content_project += '<option id="' + data[i].id + '">' + data[i].name + '</option>';
                        }
                    }
                    content_project += '</select>';
                    $('#project_name').html(content_project);
                }
            });

            $('#form_name').html('<input type="text" id="new_form_name" class="form-control" value="'+form_name+'">');

            content_is_form = '';
            if (is_form == 'Да') {
                content_is_form = '<input type="radio" name="new_is_form" id="On" value="on" checked>' +
                    '<label for="On">' +
                    '<h4 style="color: #3c8dbc; margin-right: 30px;">Да</h4>' +
                    '</label>';
                content_is_form += '<input type="radio" name="new_is_form" id="Off" value="off">' +
                    '<label for="Off">' +
                    '<h4 style="color: #3c8dbc;">Нет</h4>' +
                    '</label>';
            } else {
                content_is_form = '<input type="radio" name="new_is_form" id="On" value="on">' +
                    '<label for="On">' +
                    '<h4 style="color: #3c8dbc; margin-right: 30px;">Да</h4>' +
                    '</label>';
                content_is_form += '<input type="radio" name="new_is_form" id="Off" value="off" checked>' +
                    '<label for="Off">' +
                    '<h4 style="color: #3c8dbc;">Нет</h4>' +
                    '</label>';
            }
            $("#is_form").html(content_is_form);

            content_category = '';
            $.ajax({
                url: '/print_form/print-form/get-category',
                success: function (data) {
                    content_category += '<select id="new_category" class="form-control">';
                    for (i=0;i<data.length;i++) {
                        content_category += '<option id="'+data[i].id+'">'+data[i].category+'</option>';
                    }
                    content_category += '</select>';
                    $('#category').html(content_category);

                    new_is_form = $('input:radio[name=new_is_form]:checked').val();
                    if (new_is_form == 'off') {
                        $("#new_category").attr('disabled', 'true');
                    } else {
                        $("#new_category").removeAttr('disabled');
                    }
                }
            });

            $('#max_hour').html('<input type="text" id="new_max_hour" class="form-control" value="'+max_hour+'">');

            implement_content = '';
            $.ajax({
                url: '/print_form/users/get-implement',
                success: function (data) {
                    console.log(data);
                    implement_content += '<select id="new_implement" class="form-control">';
                    for (i=0;i<data.length;i++) {
                        user2 = data[i].first_name+' '+data[i].last_name;
                        if (implement == user2) {
                            implement_content += '<option id="'+data[i].id+'" selected>'+data[i].first_name+' '+data[i].last_name+'</option>';
                        } else {
                            implement_content += '<option id="'+data[i].id+'">'+data[i].first_name+' '+data[i].last_name+'</option>';
                        }
                    }
                    implement_content += '</select>';
                    $('#implement').html(implement_content);
                }
            });

            di = date_implement.slice(0, -9);
            $('#date_implement').html('<input type="date" id="new_date_implement" class="form-control" value="'+di+'">');

            difc = date_implement_for_client.slice(0, -9);
            $('#date_implement_for_client').html('<input type="date" id="new_date_implement_for_client" class="form-control" value="'+difc+'">');

            user_content = '';
            $.ajax({
                url: '/print_form/users/get-implement',
                success: function (data) {
                    console.log(data);
                    user_content += '<select id="new_user" class="form-control">';
                    for (i=0;i<data.length;i++) {
                        user3 = data[i].first_name+' '+data[i].last_name;
                        if (user == user3) {
                            user_content += '<option id="'+data[i].id+'" selected>'+data[i].first_name+' '+data[i].last_name+'</option>';
                        } else {
                            user_content += '<option id="'+data[i].id+'">'+data[i].first_name+' '+data[i].last_name+'</option>';
                        }
                    }
                    user_content += '</select>';
                    $('#user').html(user_content);
                }
            });

            content_is_paid = '';
            if (is_paid == 'Да') {
                content_is_paid = '<input type="radio" name="new_is_paid" id="On_new_is_paid" value="on" checked>' +
                    '<label for="On_new_is_paid">' +
                    '<h4 style="color: #3c8dbc; margin-right: 30px;">Да</h4>' +
                    '</label>';
                content_is_paid += '<input type="radio" name="new_is_paid" id="Off_new_is_paid" value="off">' +
                    '<label for="Off_new_is_paid">' +
                    '<h4 style="color: #3c8dbc;">Нет</h4>' +
                    '</label>';
            } else {
                content_is_paid = '<input type="radio" name="new_is_paid" id="On_new_is_paid" value="on">' +
                    '<label for="On_new_is_paid">' +
                    '<h4 style="color: #3c8dbc; margin-right: 30px;">Да</h4>' +
                    '</label>';
                content_is_paid += '<input type="radio" name="new_is_paid" id="Off_new_is_paid" value="off" checked>' +
                    '<label for="Off_new_is_paid">' +
                    '<h4 style="color: #3c8dbc;">Нет</h4>' +
                    '</label>';
            }

            $("#is_paid").html(content_is_paid);

            content_is_paided = '';
            if (ia_paided == 'Да') {
                content_is_paided = '<input type="radio" name="new_is_paided" id="On_new_is_paided" value="on" checked>' +
                    '<label for="On_new_is_paided">' +
                    '<h4 style="color: #3c8dbc; margin-right: 30px;">Да</h4>' +
                    '</label>';
                content_is_paided += '<input type="radio" name="new_is_paided" id="Off_new_is_paided" value="off">' +
                    '<label for="Off_new_is_paided">' +
                    '<h4 style="color: #3c8dbc;">Нет</h4>' +
                    '</label>';
            } else {
                content_is_paided = '<input type="radio" name="new_is_paided" id="On_new_is_paided" value="on">' +
                    '<label for="On_new_is_paided">' +
                    '<h4 style="color: #3c8dbc; margin-right: 30px;">Да</h4>' +
                    '</label>';
                content_is_paided += '<input type="radio" name="new_is_paided" id="Off_new_is_paided" value="off" checked>' +
                    '<label for="Off_new_is_paided">' +
                    '<h4 style="color: #3c8dbc;">Нет</h4>' +
                    '</label>';
            }
            $("#ia_paided").html(content_is_paided);

            //$("#edit").html('Сохранить').attr('onclick', 'save()').attr('id', 'save');
            $("#edit").css('display', 'none');
            $("div #maininfo").append('<a onclick="save()" id="save" style="cursor: pointer" class="btn btn-success">Сохранить</a>');

            $("input[name~='new_is_form']").change(function () {
                new_is_form = $('input:radio[name=new_is_form]:checked').val();
                if (new_is_form == 'off') {
                    $("#new_category").attr('disabled', 'true');
                } else {
                    $("#new_category").removeAttr('disabled');
                }
            });

            $("#new_category").on('change', function () {
                alert('Ok');
            });
        }

        function save() {
            fid = $("#fid").val();

            new_number = $('#new_number').val();
            new_status = $('#new_status option:selected').attr('id');
            new_status_val = $('#new_status option:selected').text();
            new_client = $('#new_client option:selected').attr('id');
            new_client_val = $('#new_client option:selected').text();
            new_project_name = $('#new_project option:selected').attr('id');
            new_project_name_val = $('#new_project option:selected').text();
            new_form_name = $('#new_form_name').val();
            new_is_form = $('input:radio[name=new_is_form]:checked').val();
            if (new_is_form == 'off') {
                new_category = null;
            } else {
                new_category = $('#new_category option:selected').attr('id');
            }
            new_category_val = $('#new_category option:selected').text();
            new_max_hour = $('#new_max_hour').val();
            new_implement = $('#new_implement option:selected').attr('id');
            new_implement_val = $('#new_implement option:selected').text();
            new_date_implement = $('#new_date_implement').val();
            new_date_implement_for_client = $('#new_date_implement_for_client').val();
            new_user = $('#new_user option:selected').attr('id');
            new_user_val = $('#new_user option:selected').text();
            new_is_paid = $('input:radio[name=new_is_paid]:checked').val();
            new_ia_paided = $('input:radio[name=new_is_paided]:checked').val();

            $.ajax({
                url: '/print_form/print-form/save-field',
                data: {
                    'fid': fid,
                    'new_number': new_number,
                    'new_status': new_status,
                    'new_client': new_client,
                    'new_project_id': new_project_name,
                    'new_form_name': new_form_name,
                    'new_is_form': new_is_form,
                    'new_category': new_category,
                    'new_max_hour': new_max_hour,
                    'new_implement': new_implement,
                    'new_date_implement': new_date_implement,
                    'new_date_implement_for_client': new_date_implement_for_client,
                    'new_user': new_user,
                    'new_is_paid': new_is_paid,
                    'new_ia_paided': new_ia_paided
                },
                success: function (data) {
                    if (data = 'Ok') {
                        $('#new_number').parent('td').html(new_number);
                        $('#new_status').parent('td').html(new_status_val);
                        $('#new_client').parent('td').html(new_client_val);
                        $('#new_project').parent('td').html(new_project_name_val);
                        $('#new_form_name').parent('td').html(new_form_name);
                        if (new_is_form == 'off') {
                            $('input:radio[name=new_is_form]').parent('td').html('Нет');
                        } else {
                            $('input:radio[name=new_is_form]').parent('td').html('Да');
                        }
                        if (new_is_form == 'off') {
                            $('#new_category').parent('td').html('');
                        } else {
                            $('#new_category').parent('td').html(new_category_val);
                        }
                        $('#new_max_hour').parent('td').html(new_max_hour);
                        $('#new_implement').parent('td').html(new_implement_val);
                        $('#new_date_implement').parent('td').html(new_date_implement+' 00:00:00');
                        $('#new_date_implement_for_client').parent('td').html(new_date_implement_for_client+' 00:00:00');
                        $('#new_user').parent('td').html(new_user_val);
                        if (new_is_paid == 'off') {
                            $('input:radio[name=new_is_paid]').parent('td').html('Нет');
                        } else {
                            $('input:radio[name=new_is_paid]').parent('td').html('Да');
                        }
                        if (new_ia_paided == 'off') {
                            $('input:radio[name=new_is_paided]').parent('td').html('Нет');
                        } else {
                            $('input:radio[name=new_is_paided]').parent('td').html('Да');
                        }

                        //$("#save").html('Редактрировать').attr('onclick', 'edit();').attr('id', 'edit');
                        $("#save").css('display', 'none');
                        $("#edit").css('display', 'block');
                    }
                }
            });
        }
    </script>
@endpush