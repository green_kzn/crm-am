@extends('layouts.cabinet')

@section('title')
    Контроль документов от клиентов
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Невыполненные</a></li>
                    <li><a href="#tab_2" data-toggle="tab">Выполненные</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="form-group">
                            <input type="radio" name="type" id="input" value="input">
                            <label for="input" style="margin-right: 20px;">
                                <h4 style="color: #3c8dbc;">Входящий</h4>
                            </label>
                            <input type="radio" name="type" id="output" value="output">
                            <label for="output">
                                <h4 style="color: #3c8dbc;">Исходящий</h4>
                            </label>
                        </div>
                        <div class="form-group">

                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                        <div class="form-group">
                            <input type="radio" name="type" id="input" value="input">
                            <label for="input" style="margin-right: 20px;">
                                <h4 style="color: #3c8dbc;">Входящий</h4>
                            </label>
                            <input type="radio" name="type" id="output" value="output">
                            <label for="output">
                                <h4 style="color: #3c8dbc;">Исходящий</h4>
                            </label>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
        </div>
    </div>
@endsection

@section('main-menu')
    @include('Print_form.main-menu')
@endsection

@push('scripts')

@endpush