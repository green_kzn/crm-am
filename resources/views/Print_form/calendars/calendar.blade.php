@extends('layouts.cabinet')

@section('title')
    Календарь
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <!--<div style="padding-bottom: 20px;">
        <a href="/print_form/events/add" class="btn btn-primary">Добавить событие</a>
    </div>-->
    <div class="row">
        <div class="col-md-3">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h4 class="box-title">Статусы задач</h4>
                </div>
                <div class="box-body">
                    <!-- the events -->
                    <div id="external-events">
                        <div class="external-event bg-green ui-draggable ui-draggable-handle" style="position: relative;">Новая задача</div>
                        <div class="external-event bg-yellow ui-draggable ui-draggable-handle" style="position: relative;">Задача выполнена</div>
                        <div class="external-event bg-aqua ui-draggable ui-draggable-handle" style="position: relative;">Задача выполнена частично</div>
                        <div class="external-event bg-red ui-draggable ui-draggable-handle" style="position: relative;">Задача в клиентском отделе</div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /. box -->
        </div>
        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-body no-padding">
                    <!-- THE CALENDAR -->
                    <div id='calendar'></div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /. box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@endsection

@section('main-menu')
    @include('Print_form.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            console.log(new Date('2017', '10', '09'));
            /* глобальные переменные */
            var event_start = $('#event_start');
            var event_end = $('#event_end');
            var event_type = $('#event_type');
            var calendar = $('#calendar');
            var form = $('#dialog-form');
            var event_id = $('#event_id');
            var format = "MM/dd/yyyy HH:mm";

            /* кнопка добавления события */
            $('#add_event_button').button().click(function(){
                formOpen('add');
            });
            /** функция очистки формы */
            function emptyForm() {
                event_start.val("");
                event_end.val("");
                event_type.val("");
                event_id.val("");
            }
            /* режимы открытия формы */
            function formOpen(mode) {
                if(mode == 'add') {
                    /* скрываем кнопки Удалить, Изменить и отображаем Добавить*/
                    $('#add').show();
                    $('#edit').hide();
                    $("#delete").button("option", "disabled", true);
                }
                else if(mode == 'edit') {
                    /* скрываем кнопку Добавить, отображаем Изменить и Удалить*/
                    $('#edit').show();
                    $('#add').hide();
                    $("#delete").button("option", "disabled", false);
                }
                form.dialog('open');
            }
            /* инициализируем Datetimepicker */
            //event_start.datetimepicker({hourGrid: 4, minuteGrid: 10, dateFormat: 'MM/dd/yyyy HH:mm'});
            //event_end.datetimepicker({hourGrid: 4, minuteGrid: 10, dateFormat: 'MM/dd/yyyy HH:mm'});
            /* инициализируем FullCalendar */
            calendar.fullCalendar({
                firstDay: 1,
                editable: true,
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                monthNamesShort: ['Янв.','Фев.','Март','Апр.','Май','Июнь','Июль','Авг.','Сент.','Окт.','Ноя.','Дек.'],
                dayNames: ["Воскресенье","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота"],
                dayNamesShort: ["ВС","ПН","ВТ","СР","ЧТ","ПТ","СБ"],
                buttonText: {
                    today: "Сегодня",
                    month: "Месяц",
                    week: "Неделя",
                    day: "День"
                },
                /* формат времени выводимый перед названием события*/
                timeFormat: 'H:mm',
                defaultView: 'month',
                /* обработчик события клика по определенному дню */
                dayClick: function(date, allDay, jsEvent, view) {
                    var newDate = $.fullCalendar.formatDate(date, format);
                    event_start.val(newDate);
                    event_end.val(newDate);
                    formOpen('add');
                },
                /* обработчик кликак по событияю */
                eventClick: function(calEvent, jsEvent, view) {
                    event_id.val(calEvent.id);
                    event_type.val(calEvent.title);
                    event_start.val($.fullCalendar.formatDate(calEvent.start, format));
                    event_end.val($.fullCalendar.formatDate(calEvent.end, format));
                    formOpen('edit');
                },
                /* источник записей */
                eventSources: [{
                    url: '/print_form/tasks/gettasks',
                    data: {
                        op: 'source'
                    },
                    error: function() {
                        console.log('Ошибка соединения с источником данных!');
                    }
                }]
            });
            /* обработчик формы добавления */
            form.dialog({
                autoOpen: false,
                buttons: [{
                    id: 'add',
                    text: 'Добавить',
                    click: function() {
                        $.ajax({
                            type: "POST",
                            url: '/print_form/tasks/gettasks',
                            data: {
                                start: event_start.val(),
                                end: event_end.val(),
                                type: event_type.val(),
                                op: 'add'
                            },
                            success: function(id){
                                calendar.fullCalendar('renderEvent', {
                                    id: id,
                                    title: event_type.val(),
                                    start: event_start.val(),
                                    end: event_end.val(),
                                    allDay: false
                                });
                            }
                        });
                        emptyForm();
                    }
                },
                    {   id: 'edit',
                        text: 'Изменить',
                        click: function() {
                            $.ajax({
                                type: "POST",
                                url: '/print_form/tasks/gettasks',
                                data: {
                                    id: event_id.val(),
                                    start: event_start.val(),
                                    end: event_end.val(),
                                    type: event_type.val(),
                                    op: 'edit'
                                },
                                success: function(id){
                                    calendar.fullCalendar('refetchEvents');

                                }
                            });
                            $(this).dialog('close');
                            emptyForm();
                        }
                    },
                    {   id: 'cancel',
                        text: 'Отмена',
                        click: function() {
                            $(this).dialog('close');
                            emptyForm();
                        }
                    },
                    {   id: 'delete',
                        text: 'Удалить',
                        click: function() {
                            $.ajax({
                                type: "POST",
                                url: '/print_form/tasks/gettasks',
                                data: {
                                    id: event_id.val(),
                                    op: 'delete'
                                },
                                success: function(id){
                                    calendar.fullCalendar('removeEvents', id);
                                }
                            });
                            $(this).dialog('close');
                            emptyForm();
                        },
                        disabled: true
                    }]
            });
        });

    </script>
@endpush