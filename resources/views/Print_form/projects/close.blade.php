@extends('layouts.cabinet')

@section('title')
    Клиент: {{ $client->name }}<br>
    Проект: {{ $project->name }}
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="box" style="padding: 10px;">
        <div class="box-header">
            <h3 class="box-title">Закрытие проекта</h3>
        </div>
        <form action="/print_form/project/close/{{ $project->id }}" method="post" id="formCloseProject">
            {{ csrf_field() }}

            <table class="table table-striped">
                <tr>
                    <td style="vertical-align: middle;">* Дата и время созвона с директором</td>
                    <td style="vertical-align: middle; width: 20%;"><input type="date" name="next_date3" class="form-control" id="date_close" style="width: 260px;" required></td>
                    <td style="vertical-align: middle;"><input type="time" name="next_time3" class="form-control" id="time_close" style="width: 100px;" required></td>
                </tr>
                <tr>
                    <td style="vertical-align: middle;">* Комментарий от директора</td>
                    <td style="vertical-align: middle;" colspan="2">
                        <label id="editor-error" class="error" for="editor" style="display: none;">Это поле обязательно для заполнения</label>
                        <div id="editor"></div>
                        <textarea name="comment" style="display: none;" cols="30" rows="10" required></textarea>
                    </td>
                </tr>
            </table>

            <div class="box-footer">
                <div class="form-group">
                    <input type="submit" value="Проект завершен" class="btn btn-success">
                    <a href="/print_form/project/edit/{{ $project->id }}" class="btn btn-default">Отмена</a>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('main-menu')
    @include('Print_form.main-menu')
@endsection

@push('scripts')
    <script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor')
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        });

        $("document").ready(function () {
            $("#formCloseProject").validate({
                messages: {
                    next_date3: {
                        required: "Это поле обязательно для заполнения"
                    },
                    next_time3: {
                        required: "Это поле обязательно для заполнения"
                    }
                }
            });
        });

        $("#formCloseProject").submit(function () {
            date = $("#date_close").val();
            time = $("#time_close").val();
            comment = CKEDITOR.instances.editor.getData();
            //$("#editor-error").attr('display', 'false');

            if (!comment) {
                alert("Укажите коментарий");
                return false;
            }

            $('textarea[name~="comment"]').html(comment);

            return true;
        });
    </script>
@endpush