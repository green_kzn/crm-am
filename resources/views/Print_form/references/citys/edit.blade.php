@extends('layouts.cabinet')

@section('title')
    Справочник "Города": Редактирование города "{{ $city->name }}"
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="box" style="padding: 10px;">
        <form action="/print_form/citys/edit/{{ $city->id }}" method="post" id="edit_citys">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="control-label" for="inputNameCity">Название города</label>
                <input type="text" class="form-control" name="name_city" value="{{ $city['name'] }}" id="inputNameCity" style="width: 300px;" required>
            </div>
            <div class="form-group">
                <input type="submit" value="Сохранить" class="btn btn-success">
                <a href="/print_form/citys" class="btn btn-default">Отмена</a>
            </div>
        </form>
    </div>
@endsection

@section('main-menu')
    @include('Print_form.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#edit_citys").validate({
                messages: {
                    name_city: {
                        required: "Это поле обязательно для заполнения"
                    }
                }
            });
        });
    </script>
@endpush