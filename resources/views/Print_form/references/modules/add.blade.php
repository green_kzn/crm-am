@extends('layouts.cabinet')

@section('title')
    Справочник "Модули": Добавление нового модуля
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="box" style="padding: 10px;">
        <form action="/print_form/modules/add" method="post" id="addModule">
            {{ csrf_field() }}
            <input type="hidden" id="tasks" name="tasks">
            <div class="form-group" id="NameTask">
                <label class="control-label" for="inputNameModule">Название модуля</label>
                <input type="text" class="form-control" name="name_module" id="inputNameModule" style="width: 300px;" required>
            </div>
            <table class="table table-hover" id="tableTask">
                <thead>
                <tr>
                    <th>Список задач</th>
                    <th>Норма кол-ва контактов по договору 1 контакт - 1 час</th>
                    <th>Кол - во разрешенных контактов</th>
                    <th>Группа задач</th>
                    <th>Вопросы для уточнения</th>
                    <th colspan="2">Действия</th>
                </tr>
                </thead>
                <tbody>
                <tr id="add_row">
                    <td><input type="text" id="task" name="task" class="form-control" required></td>
                    <td><input type="number" id="norm" name="norm" class="form-control" required></td>
                    <td><input type="number" id="number" name="number" class="form-control" required></td>
                    <td>
                        <select class="form-control" id="group_tasks">
                            @foreach($tasks as $t)
                                <option value="{{ $t->name }}">{{ $t->name }}</option>
                            @endforeach
                        </select>
                    </td>
                    <td></td>
                    <td>
                        <a class="btn btn-primary" id="addNorm">
                            <i class="fa fa-check"></i>
                        </a>
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="form-group">
                <input type="submit" value="Сохранить" class="btn btn-success">
                <a href="/print_form/modules" class="btn btn-default">Отмена</a>
            </div>
        </form>
    </div>
@endsection

@section('main-menu')
    @include('Print_form.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#addModule").validate({
                messages: {
                    name_module: {
                        required: "Это поле обязательно для заполнения"
                    },
                    task: {
                        required: "Это поле обязательно для заполнения"
                    },
                    norm: {
                        required: "Это поле обязательно для заполнения"
                    },
                    number: {
                        required: "Это поле обязательно для заполнения"
                    }
                }
            });
            $("#addNorm").click(function () {
                task = $("#task").val();
                norm = $("#norm").val();
                number = $("#number").val();
                group_tasks = $("#group_tasks").val();
                ar = [];
                max_id = 0;

                result = [];
                i = $("#tableTask .tasks").length;
                j = 1;

                if (i == 0) {
                    ar.push(1);
                } else {
                    id = $("#tableTask > tbody > tr.tasks:eq(-1)").attr('id');
                    //console.log(id);
                    if (typeof id == 'undefined' && isNaN(id)) {
                        ar.push(i+1);
                    } else {
                        ar.push(parseInt(id)+1);
                    }
                }
                ar = bouncer(ar);
                max_id = Math.max.apply(Math, ar);


                if (!task) {
                    alert('Введите задачу');
                    exit();
                }

                if (!norm) {
                    alert('Введите норму');
                    exit();
                }

                if (!number) {
                    alert('Введите количество разрешенных контактов');
                    exit();
                }

                $("#add_row").before('<tr id="' + (max_id) + '" class="tasks">' +
                        '<td id="task_' + (max_id) + '"><span name="task_' + (max_id) + '">' + task + '</span></td>' +
                        '<td id="norm_' + (max_id) + '"><span name="norm_' + (max_id) + '">' + norm + '</td>' +
                        '<td id="number_' + (max_id) + '"><span name="number_' + (max_id) + '">' + number + '</td>' +
                        '<td id="group_tasks_' + (max_id) + '"><span name="group_tasks_' + (max_id) + '">' + group_tasks + '</td>' +
                        '<td><a style="cursor: pointer;" onclick="showNewFormQuestions('+(max_id)+','+j+');">Добавить</a></td>' +
                        '<td id="edit_rule_' + (max_id) + '"><a onclick="editRule(0, ' + (max_id) + ');" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>' +
                        '<td><a onclick="delTasks(' + (max_id) + ');" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>' +
                    '</tr>' +
                    '<tr id="clarifyingQuestions'+(max_id)+'_'+j+'" style="display: none;" class="clarifyingQuestions'+(max_id)+'">' +
                        '<td colspan="5" id=""><input type="text" class="form-control"></td>' +
                        '<td colspan="2"><a id="addQuestions" onclick="addClarifyingQuestions('+(max_id)+','+j+');" class="btn btn-success"><i class="fa fa-plus"></i></a></td>' +
                    '</tr>');

                $("#task").val('');
                $("#norm").val('');
                $("#number").val('');
            });

            $("#addModule").submit(function () {
                col_tr = $("#tableTask tr.tasks").length;
                clarifyingQuestions = new Array();
                test = new Array();
                res_clarifyingQuestions = [];
                row = [];
                result = [];

                for(i = 1; i <= col_tr; i++) {
                    clarifyingQuestions[i] = [];
                    for (k=1; k<=$(".clarifyingQuestions"+i).length; k++) {
                        clarifyingQuestions[i][k] = $("#clarifyingQuestions"+i+"_"+k+" input[type~='text']").val();
                    }

                    for (var m = 0; m <= clarifyingQuestions.length; m++) {
                        if (clarifyingQuestions[m] != null && clarifyingQuestions[m] != 'underfinded' && clarifyingQuestions[m] != '') {
                            res_clarifyingQuestions.push(clarifyingQuestions[m]);
                        }
                    }
                    row[i] = {'task': $('#'+i+' span[name~="task_'+i+'"]').text(),
                        'norm': $('#'+i+' span[name~="norm_'+i+'"]').text(),
                        'number': $('#'+i+' span[name~="number_'+i+'"]').text(),
                        'group_tasks': $('#'+i+' span[name~="group_tasks_'+i+'"]').text(),
                        'clarifyingQuestions': [clarifyingQuestions[i]]
                    };
                }

                console.log(res_clarifyingQuestions);

                for (var j = 0; j <= row.length; j++) {
                    if (row[j] != null && row[j] != 'underfinded' && row[j] != '') {
                        result.push(row[j]);
                    }
                }

                $("#tasks").val(JSON.stringify(result));
                if ($("#tasks").val() == null && $("#tasks").val() == 'underfinded' && $("#tasks").val() == '') {
                    return false;
                } else {
                    return true;
                }
            });
        });

        function bouncer(arr) {
            return arr.filter( function(v){return !(v !== v);});
        }

        function editRule(id, loop) {
            active_btn = $('.btn-success').length;
            if ((active_btn-1) >= 1) {
                alert('Имеются не сохраненные данные. Сохраните сначала изменения');
            } else {
                $.ajax({
                    'url': '/print_form/group-tasks/get-task-groups',
                    success: function (resp) {
                        task = $("#task_"+loop+' > span').text();
                        norm = $("#norm_"+loop+' > span').text();
                        number = $("#number_"+loop+' > span').text();
                        group_tasks = $("#group_tasks_"+loop+' > span').text();

                        group_tasks_content = '<select id="inputGroupTask" class="form-control">';
                        for (i=0; i<resp.length; i++) {
                            group_tasks_content += '<option>'+resp[i].name+'</option>';
                        }
                        group_tasks_content += '</select>';

                        $("#task_"+loop).html('<input id="inputTask" type="text" class="form-control" value="'+task+'">');
                        $("#norm_"+loop).html('<input id="inputNorm" type="text" class="form-control" value="'+norm+'">');
                        $("#number_"+loop).html('<input id="inputNumber" type="text" class="form-control" value="'+number+'">');
                        $("#group_tasks_"+loop).html(group_tasks_content);
                        $("#edit_rule_"+loop).html('<a onclick="saveRule('+id+', '+loop+');" class="btn btn-success"><i class="fa fa-check"></i></a>');
                    }
                });
            }
        }

        function saveRule(id, loop) {
            task = $("#inputTask").val();
            norm = $("#inputNorm").val();
            number = $("#inputNumber").val();
            group_tasks = $("#inputGroupTask option:selected").text();
            module_id = $("#module_id").val();

            $("#task_"+loop).html('<span name="task_'+loop+'">'+task+'</span>');
            $("#norm_"+loop).html('<span name="norm_'+loop+'">'+norm+'</span>');
            $("#number_"+loop).html('<span name="number_'+loop+'">'+number+'</span>');
            $("#group_tasks_"+loop).html('<span name="group_tasks_'+loop+'">'+group_tasks+'</span>');
            $("#edit_rule_"+loop).html('<a onclick="editRule('+id+', '+loop+');" class="btn btn-primary"><i class="fa fa-pencil"></i></a>');
        }

        function delTasks(id) {
            $('#'+id).remove();
            $('.clarifyingQuestions'+id).remove();
        }

        function showNewFormQuestions(id, j) {
            $(".clarifyingQuestions"+id).slideToggle('fast');
        }
        
        function addClarifyingQuestions(id,j) {
            $("#clarifyingQuestions"+id+'_'+j+" a").removeClass('btn-sucess');
            $("#clarifyingQuestions"+id+'_'+j+" a").addClass('btn-danger');
            $("#clarifyingQuestions"+id+'_'+j+" a").attr('id', 'delQuestions');
            $("#clarifyingQuestions"+id+'_'+j+" a").attr('onclick', 'delClarifyingQuestions('+id+','+j+')');
            $("#clarifyingQuestions"+id+'_'+j+" a").html('<i class="fa fa-minus"></i>');
            $("#clarifyingQuestions"+id+'_'+j).after('<tr id="clarifyingQuestions'+id+'_'+(j+1)+'" class="clarifyingQuestions'+id+'">' +
                '<td colspan="5" id=""><input type="text" class="form-control"></td>' +
                '<td colspan="2"><a onclick="addClarifyingQuestions('+id+','+(j+1)+');" class="btn btn-success"><i class="fa fa-plus"></i></a></td>' +
            '</tr>');
        }

        function delClarifyingQuestions(id,j) {
            $("#clarifyingQuestions"+id+"_"+j).remove();
        }
    </script>
@endpush