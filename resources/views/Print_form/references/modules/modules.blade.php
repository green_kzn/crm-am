@extends('layouts.cabinet')

@section('title')
    Справочник "Модули"
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="col-md-12">
        @if(session('perm')['module_ref.create'])
            <div style="padding-bottom: 20px;">
                <a href="/print_form/modules/add" class="btn btn-primary">Добавить</a>
            </div>
        @endif
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Список модулей</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tbody>
                    <tr>
                        <th width="5%">№</th>
                        <th>Название</th>
                        <th colspan="2" width="10%">Действия</th>
                    </tr>
                    @if(count($modules) > 0)
                        @foreach($modules as $m)
                            <tr>
                                <td style="vertical-align: middle;">{{ $loop->index+1 }}</td>
                                <td style="vertical-align: middle;">{{ $m->name }}</td>
                                @if(session('perm')['module_ref.update'])
                                    <td style="vertical-align: middle;">
                                        <a href="/print_form/modules/edit/{{ $m->id }}" class="btn btn-primary">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </td>
                                @else
                                    <td style="vertical-align: middle;">
                                        <a href="javascript: void(0)" class="btn btn-primary" disabled="true">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </td>
                                @endif
                                @if(session('perm')['module_ref.delete'])
                                    <td style="vertical-align: middle;">
                                        <a href="/print_form/modules/del/{{ $m->id }}" class="btn btn-danger">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                @else
                                    <td style="vertical-align: middle;">
                                        <a href="javascript: void(0)" class="btn btn-danger" disabled="true">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3" style="text-align: center;">Список модулей пуст</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('main-menu')
    @include('Print_form.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">

    </script>
@endpush