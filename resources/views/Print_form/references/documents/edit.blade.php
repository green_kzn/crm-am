@extends('layouts.cabinet')

@section('title')
    Справочник "Документы": Редактирование "{{ $doc->name }}"
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <div class="box" style="padding: 10px;">
        <form action="/print_form/documents/edit/{{ $doc->id }}" method="post" id="formDocuments">
            {{ csrf_field() }}
            <input type="hidden" id="doc_ar" name="doc_ar">
            <input type="hidden" id="doc_id" name="doc_id" value="{{ $doc->id }}">
            <div class="form-group" id="NameTask">
                <label class="control-label" for="isPrintForm">Самый первый документ </label>
                @if($doc->is_first)
                    <input type="checkbox" name="isFirstDoc" id="isFirstDoc" checked>
                @else
                    <input type="checkbox" name="isFirstDoc" id="isFirstDoc">
                @endif
            </div>
            <div class="form-group" id="NameTask">
                <label class="control-label" for="isPrintForm">Нужен для всех проектов </label>
                @if($doc->for_all_projects)
                    <input type="checkbox" name="forAllProjects" id="forAllProjects" checked>
                @else
                    <input type="checkbox" name="forAllProjects" id="forAllProjects">
                @endif
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="inputNameDoc">Наименование</label>
                        <input type="text" class="form-control" name="name_doc" id="inputNameDoc" value="{{ $doc->name }}" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="inputNameCity">Тип документа</label>
                        <select name="typeDoc" id="typeDoc" class="form-control">
                            @foreach($type as $t)
                                @if($cur_type->id == $t->id)
                                    <option value="{{ $t->slug }}" selected>{{ $t->name }}</option>
                                @else
                                    <option value="{{ $t->slug }}">{{ $t->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="box">
                <table>
                    <tbody>
                    <tr>
                        <td>
                            <div class="box-header">
                                <h3 class="box-title">
                                    Следующий документ (по-умолчанию)
                                </h3>
                            </div>
                        </td>

                        <td style="padding-left: 10px;">
                            <a id="addDoc" class="btn btn-primary">Добавить</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table table-bordered table-striped" style="width: 50%" id="tableModule">
                        <thead>
                        <tr>
                            <th>Тип документа</th>
                            <th>Наименование</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="trNewDoc" id="1">
                            <td>
                                <select name="nextTypeDoc" onchange="getDoc(this);" class="form-control">
                                    @foreach($type as $t)
                                        <option value="{{ $t->slug }}">{{ $t->name }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <select name="next_doc" id="next_doc_1" class="form-control"></select>
                            </td>
                            <td>
                                <a onclick="delRowDoc(1);" class="btn btn-danger">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        <tr id="newRowDoc"></tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <div class="form-group">
                <input type="submit" value="Сохранить" class="btn btn-success">
                <a href="/print_form/documents" class="btn btn-default">Отмена</a>
            </div>
        </form>
    </div>
@endsection

@section('main-menu')
    @include('Print_form.main-menu')
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $("#formDocuments").validate({
            messages: {
                name_doc: {
                    required: "Это поле обязательно для заполнения"
                }
            }
        });
    });
    var docs2 = {};
    var docs_ar2 = [];
    var data = [];

    $('document').ready(function () {
        next_type = $("tr#1 select[name~='nextTypeDoc'] option:selected").val();
        doc_id = $("#doc_id").val();
        $.ajax({
            url: '/print_form/documents/getTypeDoc',
            data: {'next_type': next_type, 'doc_id': doc_id},
            success: function (resp) {
                content = '';
                type = $("select[name~='nextTypeDoc'] option:selected").val();
                for (i=0; i<resp.length; i++) {
                    content += '<option value="'+resp[i].id+'">'+resp[i].name+'</option>';
                }
                $("#next_doc_1").html(content);
            }
        });
        $("#formDocuments").submit(function () {
            var row = $('.trNewDoc');
            var numRows = row.length;

            for(i=1;i<=numRows;i++) {
                type2 = $("#tableModule #"+i+" select[name~='nextTypeDoc'] option:selected").val();
                doc2 = $("#tableModule #"+i+" select[name~='next_doc'] option:selected").val();

                docs2 = {};
                docs2.id = i;
                docs2.type = type2;
                docs2.doc = doc2;

                docs_ar2.push(docs2);
            }
            $("#doc_ar").val(JSON.stringify(docs_ar2));

            return true;
        });

        $("#addDoc").click(function () {
            i = $(".trNewDoc").length;
            doc_id = $("#doc_id").val();
            if (i > 0) {
                id = $("#tableModule tr").eq(i).attr('id');
            } else {
                id = 0;
            }

            $("#newRowDoc").before('<tr class="trNewDoc" id='+(Number(id)+1)+'>' +
                '<td>' +
                '<select name="nextTypeDoc" onchange="getDoc(this);" class="form-control">' +
                '@foreach($type as $t)' +
                '<option value="{{ $t->slug }}">{{ $t->name }}</option>' +
                '@endforeach' +
                '</select>' +
                '</td>' +
                '<td>' +
                '<select name="next_doc" id="next_doc_'+(Number(id)+1)+'" class="form-control"></select>' +
                '</td>' +
                '<td>' +
                '<a onclick="delRowDoc('+(Number(id)+1)+');" class="btn btn-danger"><i class="fa fa-trash"></i></a>' +
                '</td>' +
                '</tr>');

            next_type = $("tr#"+id+" select[name~='nextTypeDoc'] option:selected").val();
            $.ajax({
                url: '/print_form/documents/getTypeDoc',
                data: {'next_type': next_type, 'doc_id': doc_id},
                success: function (resp) {
                    content = '';
                    type = $("select[name~='nextTypeDoc'] option:selected").val();
                    for (i=0; i<resp.length; i++) {
                        content += '<option value="'+resp[i].id+'">'+resp[i].name+'</option>';
                    }
                    $("#next_doc_"+(Number(id)+1)).html(content);
                }
            });
        });

        $('#formDocuments').submit(function () {

            return true;
        });
    });
    
    function delRowDoc(i) {
        for (j=0;j<docs_ar2.length;j++) {
            if (docs_ar2[j].id == i) {
                docs_ar2.splice(j, 1);
            }
        }

        $("tr#"+i).remove();
    }

    function getDoc(el) {
        id = $(el).closest('tr').attr('id');
        doc_id = $("#doc_id").val();
        next_type = $("tr#"+id+" select[name~='nextTypeDoc'] option:selected").val();
        $.ajax({
            url: '/print_form/documents/getTypeDoc',
            data: {'next_type': next_type, 'doc_id': doc_id},
            success: function (resp) {
                console.log(resp);
                content = '';
                for (i=0; i<resp.length; i++) {
                    content += '<option value="'+resp[i].id+'">'+resp[i].name+'</option>';
                }
                console.log(id);
                $("tr#"+id+" #next_doc_"+id).html(content);
            }
        });
    }


</script>
@endpush