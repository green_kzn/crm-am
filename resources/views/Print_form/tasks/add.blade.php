@extends('layouts.cabinet')

@section('title')
    Новая задача
@endsection

@section('content')
    <div class="box" style="padding: 15px;">
        <form action="/print_form/tasks/add" method="post" id="formAddTask">
            <input type="hidden" name="typeTask">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs" id="typeTask">
                    <li class="active"><a href="#tab_1" id="introduction" data-toggle="tab" aria-expanded="true">Внедрение</a></li>
                    <li><a href="#tab_2" id="single_task" data-toggle="tab" aria-expanded="false">Разовая задача</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-condensed">
                                    <tr>
                                        <td width="40%" style="vertical-align: middle;">Номер задачи</td>
                                        <td colspan="2" style="vertical-align: middle;">#11</td>
                                    </tr>
                                    <tr>
                                        <td width="40%" style="vertical-align: middle;">Очередь</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <input type="number" name="queue" class="form-control">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" style="vertical-align: middle;">Заказчик</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <select name="customer" class="form-control">
                                                @foreach($clients as $c)
                                                    <option value="{{ $c->id }}">{{ $c->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" style="vertical-align: middle;">Название проекта</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <select name="projectName" class="form-control">
                                                @foreach($projects as $p)
                                                    <option value="{{ $p->id }}">{{ $p->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Наименование задачи</td>
                                        <td colspan="2" style="vertical-align: middle;"><input type="text" name="TaskName" class="form-control"></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Печатная\экранная форма?</td>
                                        <td style="vertical-align: middle;">
                                            <input type="radio" name="isPrintForm" value="on" checked>Да
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <input type="radio" name="isPrintForm" value="off">Нет
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Категория</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <input type="number" name="category" class="form-control">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Максимальное количество часов</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <input type="number" name="maxCountHours" class="form-control">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="table table-condensed">
                                    <tr>
                                        <td width="40%" style="vertical-align: middle;">Внедренец</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <select name="implementer" class="form-control">
                                            @foreach($users as $u)
                                                <option value="{{ $u->id }}">{{ $u->last_name }} {{ $u->first_name }}</option>
                                            @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Дата реализации</td>
                                        <td style="vertical-align: middle;">
                                            <input type="date" name="dateFinish" class="form-control" width="100px">
                                        </td>
                                        <td><a href="" class="btn btn-primary">Расчитать на текущий момент</a></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Дата реализации для заказчика</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <input type="date" name="dateFinishForCustomers" class="form-control" width="100px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" style="vertical-align: middle;">Кто будет делать</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <select name="contractor" class="form-control">
                                                <option value="">1</option>
                                                <option value="">2</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Платно ?</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <input name="forPaid" type="checkbox">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-condensed">
                                    <tr>
                                        <td width="40%" style="vertical-align: middle;">Номер задачи</td>
                                        <td colspan="2" style="vertical-align: middle;">#11</td>
                                    </tr>
                                    <tr>
                                        <td width="40%" style="vertical-align: middle;">Очередь</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <input type="number" class="form-control">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" style="vertical-align: middle;">Заказчик</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <select name="" id="" class="form-control">
                                                <option value="">1</option>
                                                <option value="">2</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Наименование задачи</td>
                                        <td colspan="2" style="vertical-align: middle;"><input type="text" class="form-control"></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Печатная\экранная форма?</td>
                                        <td style="vertical-align: middle;">
                                            <input type="radio" name="isPrintForm" id="isPrintForm">Да
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <input type="radio" name="isPrintForm" id="isPrintForm">Нет
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Категория</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <input type="number" name="category" class="form-control">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Максимальное количество часов</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <input type="number" class="form-control">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="table table-condensed">
                                    <tr>
                                        <td style="vertical-align: middle;">Дата реализации</td>
                                        <td style="vertical-align: middle;">
                                            <input type="date" class="form-control" width="100px">
                                        </td>
                                        <td><a href="" class="btn btn-primary">Расчитать на текущий момент</a></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Дата реализации для заказчика</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <input type="date" class="form-control" width="100px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" style="vertical-align: middle;">Кто будет делать</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <select name="" id="" class="form-control">
                                                <option value="">1</option>
                                                <option value="">2</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Платно ?</td>
                                        <td colspan="2" style="vertical-align: middle;">
                                            <input type="checkbox">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <div class="form-group">
                <b>Прикрепленные файлы</b>
                <div>

                        <div id="drop">
                            Перетащите сюда файлы или нажмите

                            <a>Обзор</a>
                            <input type="file" name="upl" multiple />
                        </div>

                        <ul>
                            <!-- загрузки будут показаны здесь -->
                        </ul>


                </div>
            </div>
            <div class="form-group">
                <b>Комментарий</b>
                <div id="editor"></div>
            </div>

            <div class="form-group">
                <input type="submit" value="Сохранить" class="btn btn-success">
                <a href="/print_form/tasks" class="btn btn-default">Отмена</a>
            </div>
        </form>
    </div>
@endsection

@section('main-menu')
    @include('Print_form.main-menu')
@endsection

@push('scripts')
    <script src="{{ asset('/public/js/jquery.datetimepicker.js') }}"></script>
    <script src="{{ asset('/public/js/jquery-upload/jquery.knob.js') }}"></script>
    <script src="{{ asset('/public/js/jquery-upload/jquery.ui.widget.js') }}"></script>
    <script src="{{ asset('/public/js/jquery-upload/jquery.iframe-transport.js') }}"></script>
    <script src="{{ asset('/public/js/jquery-upload/jquery.fileupload.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#formAddTask').submit(function () {
                typeTask = $('#typeTask li.active a').attr('id');
                $('input[name="typeTask"]').val(typeTask);
                return true;
            });
        });
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor')
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        });
        $(function(){

            var ul = $('#upload ul');

            $('#drop a').click(function(){
                // имитация нажатия на поле выбора файла
                $(this).parent().find('input').click();
            });

            // инициализация плагина jQuery File Upload
            $('#upload').fileupload({

                // этот элемент будет принимать перетаскиваемые на него файлы
                dropZone: $('#drop'),

                // Функция будет вызвана при помещении файла в очередь
                add: function (e, data) {

                    var tpl = $('<li><input type="text" value="0" data-width="48" data-height="48"'+
                        ' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" /><p></p><span></span></li>');

                    // вывод имени и размера файла
                    tpl.find('p').text(data.files[0].name)
                        .append('<i>' + formatFileSize(data.files[0].size) + '</i>');

                    data.context = tpl.appendTo(ul);

                    // инициализация плагина jQuery Knob
                    tpl.find('input').knob();

                    // отслеживание нажатия на иконку отмены
                    tpl.find('span').click(function(){

                        if(tpl.hasClass('working')){
                            jqXHR.abort();
                        }

                        tpl.fadeOut(function(){
                            tpl.remove();
                        });

                    });

                    // Автоматически загружаем файл при добавлении в очередь
                    var jqXHR = data.submit();
                },

                progress: function(e, data){

                    // Вычисление процента загрузки
                    var progress = parseInt(data.loaded / data.total * 100, 10);

                    // обновляем шкалу
                    data.context.find('input').val(progress).change();

                    if(progress == 100){
                        data.context.removeClass('working');
                    }
                },

                fail:function(e, data){
                    // что-то пошло не так
                    data.context.addClass('error');
                }

            });

            $(document).on('drop dragover', function (e) {
                e.preventDefault();
            });

            // вспомогательная функция, которая форматирует размер файла
            function formatFileSize(bytes) {
                if (typeof bytes !== 'number') {
                    return '';
                }

                if (bytes >= 1000000000) {
                    return (bytes / 1000000000).toFixed(2) + ' GB';
                }

                if (bytes >= 1000000) {
                    return (bytes / 1000000).toFixed(2) + ' MB';
                }

                return (bytes / 1000).toFixed(2) + ' KB';
            }

        });
    </script>
@endpush