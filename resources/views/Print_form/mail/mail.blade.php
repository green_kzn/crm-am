@extends('layouts.cabinet')

@section('title')
    Почта
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3">
                <a href="/print_form/mail/new" class="btn btn-primary btn-block margin-bottom">Написать</a>

                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title"><b>Папки</b></h3>

                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body no-padding">
                        <ul class="nav nav-pills nav-stacked">
                            @foreach($folders as $f)
                                @if($f['active'])
                                    <li class="active mail-folder" style="cursor: pointer;"><a id="{{ $f['slug'] }}" href="/print_form/mail/{{ $f['slug'] }}">{{ $f['name'] }}</a></li>
                                @else
                                    <li class="mail-folder" style="cursor: pointer;"><a id="{{ $f['slug'] }}" href="/print_form/mail/{{ $f['slug'] }}">{{ $f['name'] }}</a></li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ $header }}</h3>

                        <div class="box-tools pull-right">
                            <div class="has-feedback">
                                <input type="text" class="form-control input-sm" placeholder="Search Mail">
                                <span class="fa fa-search form-control-feedback"></span>
                            </div>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="mailbox-controls">
                            <!-- Check all button -->
                            <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
                            </button>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                            </div>
                            <!-- /.btn-group -->
                            <button type="button" onclick="location.reload();" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                            <div class="pull-right">
                                1-50/200
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                                </div>
                                <!-- /.btn-group -->
                            </div>
                            <!-- /.pull-right -->
                        </div>
                        <div class="table-responsive mailbox-messages">
                            <table class="table table-hover table-striped">
                                <tbody>
                                @if($mail[0] != '')
                                    @foreach($mail as $m)
                                        @if($m['seen'] == 1)
                                        <tr>
                                            <td>
                                                <input type="checkbox" style="position: relative; opacity: 100;">
                                            </td>
                                            <td class="mailbox-star" style="width: 2%;"><a href="#"><i class="fa fa-star-o text-yellow"></i></a></td>
                                            <td class="mailbox-subject" style="width: 50%;"><a href="/print_form/message/{{ $m['uid'] }}">{{ $m['subject'] }}</a></td>
                                            @if($m['attachment'])
                                            <td class="mailbox-attachment"><i class="fa fa-paperclip"></i></td>
                                            @else
                                            <td class="mailbox-attachment"></td>
                                            @endif
                                            <td class="mailbox-client">{{ $m['client'] }}</td>
                                            <td class="mailbox-name" style="width: 20%;">{{ $m['sender'] }}</td>
                                            <td class="mailbox-date">{{ $m['date'] }}</td>
                                        </tr>
                                        @else
                                        <tr>
                                            <td>
                                                <input type="checkbox">
                                            </td>
                                            <td class="mailbox-star" style="width: 2%;"><a href="#"><i class="fa fa-star-o text-yellow"></i></a></td>
                                            <td class="mailbox-subject" style="width: 50%;"><a href="/print_form/message/{{ $m['uid'] }}"><b>{{ $m['subject'] }}</b></a></td>
                                            <td class="mailbox-attachment"></td>
                                            <td class="mailbox-client"></td>
                                            <td class="mailbox-name" style="width: 20%;">{{ $m['sender'] }}</td>
                                            <td class="mailbox-date">{{ $m['date'] }}</td>
                                        </tr>
                                        @endif
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6" style="text-align: center;"><i>В папке нет писем</i></td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            <!-- /.table -->
                        </div>
                        <!-- /.mail-box-messages -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer no-padding">
                        <div class="mailbox-controls">
                            <!-- Check all button -->
                            <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
                            </button>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                            </div>
                            <!-- /.btn-group -->
                            <button type="button" onclick="location.reload();" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                            <div class="pull-right">
                                1-50/200
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                                </div>
                                <!-- /.btn-group -->
                            </div>
                            <!-- /.pull-right -->
                        </div>
                    </div>
                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        </section>
    <!-- /.content -->
@endsection

@section('main-menu')
    @include('Print_form.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">
        $(function () {
            $("body > div.wrapper > div > section.content > section > div > div.col-md-9 > div > div.box-body.no-padding > div.table-responsive.mailbox-messages > table > tbody > tr > td:nth-child(1) > div > input[type~='checkbox']").css('opacity: 100;');

            folder = location.pathname.split('/');
            $(".mail-folder").removeClass('active');
            $("a#"+folder[3]).parent('li').addClass('active');
            //Enable iCheck plugin for checkboxes
            //iCheck for checkbox and radio inputs
            $('.mailbox-messages input[type="checkbox"]').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });

            //Enable check and uncheck all functionality
            $(".checkbox-toggle").click(function () {
                var clicks = $(this).data('clicks');
                if (clicks) {
                    //Uncheck all checkboxes
                    $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
                    $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
                } else {
                    //Check all checkboxes
                    $(".mailbox-messages input[type='checkbox']").iCheck("check");
                    $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
                }
                $(this).data("clicks", !clicks);
            });

            //Handle starring for glyphicon and font awesome
            $(".mailbox-star").click(function (e) {
                e.preventDefault();
                //detect type
                var $this = $(this).find("a > i");
                var glyph = $this.hasClass("glyphicon");
                var fa = $this.hasClass("fa");

                //Switch states
                if (glyph) {
                    $this.toggleClass("glyphicon-star");
                    $this.toggleClass("glyphicon-star-empty");
                }

                if (fa) {
                    $this.toggleClass("fa-star");
                    $this.toggleClass("fa-star-o");
                }
            });
        });

        function openMailFolder(slug) {
            $.ajax({
                url: '/print_form/mail/get-mail',
                data: {'slug': slug},
                success: function (resp) {
                    console.log(resp);
                    $(".mail-folder").removeClass('active');
                    $("a#"+slug).parent('li').addClass('active');
                    $(".table tbody tr").remove();
                    if (resp == '') {
                        $(".table tbody").html('<tr>' +
                            '<td colspan="6" style="text-align: center;"><i>Письма не найдены</i></td>' +
                            '</tr>');
                    } else  {
                        context = '';
                        for (i=0;i<resp.length;i++) {
                            context += '<tr>' +
                                '<td><div class="icheckbox_flat-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></td>' +
                                '<td class="mailbox-star" style="width: 2%;"><a href="#"><i class="fa fa-star-o text-yellow"></i></a></td>' +
                                '<td class="mailbox-name" style="width: 20%;"><a href="/print_form/message/'+resp[i].id+'">'+resp[i].sender+'</a></td>' +
                                '<td class="mailbox-subject" style="width: 60%;"><b>'+resp[i].subject+'</b></td>' +
                                '<td class="mailbox-attachment"></td>' +
                                '<td class="mailbox-date">'+resp[i].date+'</td>' +
                                '</tr>';
                        }
                        $(".table tbody").html(context);
                    }
                }
            });
        }
    </script>
@endpush
