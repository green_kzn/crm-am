@extends('layouts.cabinet')

@section('title')
    Почта
@endsection

@section('content')
    @if(\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Выполнено!</h4>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if(\Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            {!! \Session::get('error') !!}
        </div>
    @endif
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3">
                <a href="/print_form/mail/new" class="btn btn-primary btn-block margin-bottom">Написать</a>

                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title"><b>Папки</b></h3>

                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body no-padding">
                        <ul class="nav nav-pills nav-stacked">
                            @foreach($folders as $f)
                                @if($f['active'])
                                    <li class="active mail-folder" style="cursor: pointer;"><a id="{{ $f['slug'] }}" href="/print_form/mail/{{ $f['slug'] }}">{{ $f['name'] }}</a></li>
                                @else
                                    <li class="mail-folder" style="cursor: pointer;"><a id="{{ $f['slug'] }}" href="/print_form/mail/{{ $f['slug'] }}">{{ $f['name'] }}</a></li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3>{{ $subject }}</h3>
                        <h5>Отправитель: <span id="sender">{{ $fromaddress }}</span>
                            <span class="mailbox-read-time pull-right">{{ $date }}</span>
                        </h5>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="mailbox-read-message" id="message">
                            <?php echo htmlspecialchars_decode($text) ?>
                        </div>
                        <!-- /.mailbox-read-message -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        @if($attachment[0] != '')
                            <ul class="mailbox-attachments clearfix">
                                @foreach($attachment as $a)
                                    <li>
                                        @if($a['type'] == 'jpg')
                                            <span class="mailbox-attachment-icon has-img"><img src="{{ asset($a['filePath']) }}" ></span>
                                        @elseif($a['type'] == 'doc')
                                            <span class="mailbox-attachment-icon"><i class="fa fa-file-word-o"></i></span>
                                        @elseif($a['type'] == 'docx')
                                            <span class="mailbox-attachment-icon"><i class="fa fa-file-word-o"></i></span>
                                        @elseif($a['type'] == 'xls')
                                            <span class="mailbox-attachment-icon"><i class="fa fa-file-excel-o"></i></span>
                                        @elseif($a['type'] == 'xlsx')
                                            <span class="mailbox-attachment-icon"><i class="fa fa-file-excel-o"></i></span>
                                        @elseif($a['type'] == 'pdf')
                                            <span class="mailbox-attachment-icon"><i class="fa fa-file-pdf-o"></i></span>
                                        @elseif($a['type'] == 'png')
                                            <span class="mailbox-attachment-icon has-img"><img src="{{ asset($a['filePath']) }}" ></span>
                                        @else
                                            <span class="mailbox-attachment-icon"><i class="fa fa-file-pdf-o"></i></span>
                                        @endif

                                        <div class="mailbox-attachment-info">
                                            <a href="{{ asset($a['filePath']) }}" target="_blank" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{ $a['name'] }}</a>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                    <div class="box-footer">
                        <div class="pull-right">
                            <button type="button" class="btn btn-default"><i class="fa fa-reply"></i> Ответить</button>
                            <button type="button" class="btn btn-default"><i class="fa fa-share"></i> Переслать</button>
                        </div>
                        <a href="" class="btn btn-default"><i class="fa fa-trash-o"></i> Удалить</a>
                        <a href="#task-create" class="btn btn-default"><i class="fa fa-tasks"></i> Создать задачу</a>
                        <button type="button" class="btn btn-default"><i class="fa fa-tasks"></i> Письмо обработано</button>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /. box -->
                <div class="box box-primary" id="task-create">
                    <div class="box-header with-border">
                        <h4>Создание задачи</h4>
                    </div>
                    @if(!$task_exists)
                    <form action="/print_form/callback/create-client-from-mail" method="post" id="formCreateTask">
                        {{ csrf_field() }}
                        <input type="hidden" name="data" id="data">
                        <span id="files" name="files" style="display: none;">@php echo json_encode($attachment); @endphp</span>
                        <input type="hidden" name="message_id" id="message_id" value="{{ $message_id }}">
                        <div class="box-body">
                            <div class="alert alert-danger alert-dismissible" id="error" style="display: none;">
                                <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>-->
                                <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                                <span id="error-text"></span>
                            </div>
                            <div class="form-group" style="margin-top: 10px;">
                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="client">
                                <label for="optionsRadios1">
                                    <h4 style="color: #3c8dbc;">Клиенты на обзвон</h4>
                                </label>
                            </div>
                            <div id="client-block" style="display: none;">
                                <table class="table table-condensed">
                                    <tbody>
                                    <tr>
                                        <td style="vertical-align: middle;">Клиент</td>
                                        <td style="vertical-align: middle;">
                                            <select id="clients" class="form-control">
                                                @foreach($clients as $c)
                                                    <option id="{{ $c->id }}">{{ $c->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Проект</td>
                                        <td style="vertical-align: middle;">
                                            <select id="projects" class="form-control">
                                                @foreach($project as $p)
                                                    <option id="{{ $p->id }}">{{ $p->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Коментарий</td>
                                        <td>
                                            <div id="editor2"></div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="form-group" style="margin-top: 10px;">
                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="form">
                                <label for="optionsRadios2">
                                    <h4 style="color: #3c8dbc;">Отдел разработки форм</h4>
                                </label>
                            </div>
                            <div id="form-block" style="display: none;">
                                <table class="table table-condensed">
                                    <tbody>
                                    <tr>
                                        <td style="vertical-align: middle;">Наименование задачи</td>
                                        <td style="vertical-align: middle;" colspan="2">
                                            <input type="text" name="task_name" id="task_name" class="form-control">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Клиент</td>
                                        <td style="vertical-align: middle;" colspan="2">
                                            <select id="clients2" class="form-control" name="customer_introduction">
                                                @foreach($clients as $c)
                                                    <option id="{{ $c->id }}">{{ $c->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Проект</td>
                                        <td style="vertical-align: middle;" colspan="2">
                                            <select id="projects2" class="form-control" name="projectName_introduction">
                                                @foreach($project as $p)
                                                    <option id="{{ $p->id }}">{{ $p->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Категория</td>
                                        <td style="vertical-align: middle;" colspan="2">
                                            <select id="category" class="form-control" onclick="getCategoryNorm();">
                                                @foreach($category as $c)
                                                    <option id="{{ $c['id'] }}">{{ $c['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Максимальное кол-во часов</td>
                                        <td style="vertical-align: middle;" colspan="2">
                                            <input type="number" name="max_hours" id="max_hours" class="form-control">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Кто будет делать</td>
                                        <td style="vertical-align: middle;" colspan="2">
                                            <select id="user" class="form-control">
                                                @foreach($user as $u)
                                                    <option id="{{ $u->id }}">{{ $u->last_name }} {{ $u->first_name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Дата реализации</td>
                                        <td style="vertical-align: middle;">
                                            <input type="date" name="dateFinish_introduction" class="form-control" width="100px">
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <a onclick="calculate()" class="btn btn-primary">Расчитать на текущий момент</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Дата реализации для заказчика</td>
                                        <td style="vertical-align: middle;" colspan="2">
                                            <input type="date" name="dateFinish_introduction_for_client" class="form-control" width="100px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Коментарий</td>
                                        <td colspan="2">
                                            <div id="editor3"></div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="box-footer">
                            <input type="submit" value="Создать" class="btn btn-primary">
                        </div>
                    </form>
                    @else
                    <span>Задача уже создана</span>
                    @endif
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@section('main-menu')
    @include('Print_form.main-menu')
@endsection

@push('scripts')
    <script type="text/javascript">
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor2');
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5();
        });
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor3');
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5();
        });
        $(function () {
            //Enable iCheck plugin for checkboxes
            //iCheck for checkbox and radio inputs
            $('.mailbox-messages input[type="checkbox"]').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });

            $("#formCreateTask").validate({
                messages: {
                    task_name: {
                        required: 'Это поле обязательно для заполнения'
                    },
                    max_hours: {
                        required: 'Это поле обязательно для заполнения'
                    },
                    dateFinish_introduction: {
                        required: 'Это поле обязательно для заполнения'
                    },
                    dateFinish_introduction_for_client: {
                        required: 'Это поле обязательно для заполнения'
                    }
                }
            });

            $("select[name~='customer_introduction']").change(function() {
                client = $(this).find('option:selected').attr('id');
                content = '';
                $.ajax({
                    url: '/print_form/projects/getProjectByClientId',
                    data: {'id': client},
                    success: function (data) {
                        console.log(data);
                        if (data != '') {
                            for (i=0;i<data.length;i++) {
                                content += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                            }
                            $("select[name~='projectName_introduction']").html(content);
                        } else {
                            content += '<option disabled>Проекты отсутствуют</option>';
                            $("select[name~='projectName_introduction']").html(content);
                        }
                    }
                });
            });

            //Enable check and uncheck all functionality
            $(".checkbox-toggle").click(function () {
                var clicks = $(this).data('clicks');
                if (clicks) {
                    //Uncheck all checkboxes
                    $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
                    $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
                } else {
                    //Check all checkboxes
                    $(".mailbox-messages input[type='checkbox']").iCheck("check");
                    $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
                }
                $(this).data("clicks", !clicks);
            });

            //Handle starring for glyphicon and font awesome
            $(".mailbox-star").click(function (e) {
                e.preventDefault();
                //detect type
                var $this = $(this).find("a > i");
                var glyph = $this.hasClass("glyphicon");
                var fa = $this.hasClass("fa");

                //Switch states
                if (glyph) {
                    $this.toggleClass("glyphicon-star");
                    $this.toggleClass("glyphicon-star-empty");
                }

                if (fa) {
                    $this.toggleClass("fa-star");
                    $this.toggleClass("fa-star-o");
                }
            });

            $('#formCreateTask').submit(function () {
                $("#error").css('display', 'none');
                task = $("input[name~='optionsRadios']:checked").val();
                if (!task) {
                    $("#error-text").html('Выберите тип задачи');
                    $("#error").css('display', 'block');
                    return false;
                }

                data = {};
                message = $("#message").html();
                sender = $("#sender").html();
                clientId = $("#clients option:selected").attr('id');
                projectId = $("#projects option:selected").attr('id');
                messageId = $("#message_id").val();
                comment2 = CKEDITOR.instances.editor2.getData();

                clientId2 = $("#clients2 option:selected").attr('id');
                projectId2 = $("#projects2 option:selected").attr('id');
                task_name = $("#task_name").val();
                max_hours = $("#max_hours").val();
                category = $("#category option:selected").attr('id');
                user = $("#user option:selected").attr('id');
                finish_date = $("input[name~='dateFinish_introduction']").val();
                finish_date_for_client = $("input[name~='dateFinish_introduction_for_client']").val();
                files = $("#files").html();
                comment3 = CKEDITOR.instances.editor3.getData();

                if (task == 'client') {
                    data = {
                        'sender': sender,
                        'message': message,
                        'clientId': clientId,
                        'projectId': projectId,
                        'messageId': messageId,
                        'comment': comment2
                    };
                } else {
                    data = {
                        'sender': sender,
                        'message': message,
                        'clientId': clientId2,
                        'projectId': projectId2,
                        'messageId': messageId,
                        'task_name': task_name,
                        'max_hours': max_hours,
                        'category': category,
                        'user': user,
                        'finish_date': finish_date,
                        'finish_date_for_client': finish_date_for_client,
                        'files': files,
                        'comment': comment3
                    };
                }

                $("input[name~='data']").val(JSON.stringify(data));

                return true;
            });

            $("input[name~='optionsRadios']").change(function() {
                task = $("input[name~='optionsRadios']:checked").val();
                $("#error").css('display', 'none');
                if (task == 'client') {
                    $("#client-block").slideToggle('fast');
                    $("#form-block").css('display', 'none');
                } else {
                    $("#form-block").slideToggle('fast');
                    $("#task_name").attr('required', 'true');
                    $("#max_hours").attr('required', 'true');
                    $("input[name~='dateFinish_introduction']").attr('required', 'true');
                    $("input[name~='dateFinish_introduction_for_client']").attr('required', 'true');
                    $("#client-block").css('display', 'none');
                }
            });
        });

        function calculate() {
            $.ajax({
                url: '/print_form/calculate-print-form',
                success: function (resp) {
                    console.log(resp);
                    $("input[name~='dateFinish_introduction']").val(resp);
                }
            });
        }

        function getCategoryNorm() {
            category = $("#category").val();
            $.ajax({
                url: '/print_form/printform-category/get-category-norm',
                data: {'category': category},
                success: function (resp) {
                    $("#max_hours").val(resp);
                }
            });
        }
    </script>
@endpush
