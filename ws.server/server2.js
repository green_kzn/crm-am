var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var fs = require('fs');
var https = require('https');
var Redis = require('ioredis'),
    redis = new Redis();

app.set('views', __dirname + '/views')
app.set('view engine', 'pug')
app.use(express.static(__dirname + '/public')) 

server.listen(4022, function () {
    console.log('Server listening at port %d', 4022);
});

const opts = {
    key: fs.readFileSync('/etc/letsencrypt/live/ruslan.stage.88003010373.ru/privkey.pem'),
    cert: fs.readFileSync('/etc/letsencrypt/live/ruslan.stage.88003010373.ru/fullchain.pem')
}

var httpsServer = https.createServer(opts, app);
httpsServer.listen(4023, function(){
    console.log("HTTPS on port " + 4023);
})

app.get('/', function (req, res) {
    res.render('index');
})

io.attach(httpsServer);
io.attach(server);

io.on('connection', function(client) {
    //console.log(client);
    console.log('Client connected...');
    // client.join('private-message123');
    // client.broadcast.to('private-message123:message123').emit('message123', 'Ok');
    // client.on('join', function(room) {
    //     client.emit('private-message123:message123', "this is a test");
    //     console.log(room);
        
    //     console.log(client.rooms)
    //     client.broadcast.to(room).emit('message123', 'Ok');
    // });
});

io.to('private-message123').emit('private-message123');

redis.psubscribe('*', function (pattern, count) {
    console.log('psubscribe: '+pattern, count);
});

redis.on('pmessage', function (pattern, channel, message) {
    console.log('pattern: '+pattern);
    // console.log('message: '+message);
    if (channel == 'chat') {
        message = JSON.parse(message);
        io.emit(channel+':'+message.event, message.data);
        console.log(channel+':'+message.event, message.data);
    }
    if (channel == 'client') {
        message = JSON.parse(message);
        io.emit(channel+':'+message.event, message.data);
        console.log(channel+':'+message.event, message.data.client);
    }
    if (channel == 'project') {
        message = JSON.parse(message);
        io.emit(channel+':'+message.event, message.data);
        console.log(channel+':'+message.event, message.data);
    }
    if (channel == 'raw') {
        message = JSON.parse(message);
        io.emit(channel+':'+message.event, message.data);
        console.log(channel+':'+message.event, message.data);
    }
    if (channel == 'impEvent') {
        message = JSON.parse(message);
        io.emit(channel+':'+message.event, message.data);
        console.log(channel+':'+message.event, message.data);
    }
    if (channel == 'changeImp') {
        message = JSON.parse(message);
        io.emit(channel+':'+message.event, message.data);
        console.log(channel+':'+message.event, message.data);
    }
    if (channel == 'private-message123') {
        message = JSON.parse(message);
        io.emit(channel+':'+message.event, message.data);
        console.log(channel+':'+message.event, message.data);
    }
    if (channel == 'private-addFormToQueue') {
        message = JSON.parse(message);
        io.emit(channel+':'+message.event, message.data);
        console.log(channel+':'+message.event, message.data);
    }
    if (channel == 'private-addFormError') {
        message = JSON.parse(message);
        io.emit(channel+':'+message.event, message.data);
        console.log(channel+':'+message.event, message.data);
    }
    if (channel == 'private-formChangeStatus') {
        message = JSON.parse(message);
        io.emit(channel+':'+message.event, message.data);
        console.log(channel+':'+message.event, message.data);
    }
    if (channel == 'private-impFormChangeStatus') {
        message = JSON.parse(message);
        io.emit(channel+':'+message.event, message.data);
        console.log(channel+':'+message.event, message.data);
    }
    if (channel == 'private-ImpCloseProject') {
        message = JSON.parse(message);
        io.emit(channel+':'+message.event, message.data);
        console.log(channel+':'+message.event, message.data);
    }
    if (channel == 'private-ManagerCloseProject') {
        message = JSON.parse(message);
        io.emit(channel+':'+message.event, message.data);
        console.log(channel+':'+message.event, message.data);
    }
});