<?php

return [
    'accounts' => [
        'archimed-soft' => [
            //'host' => '{37.61.179.77:143/imap/tls/novalidate-cert}',
            'host' => '37.61.179.77',
            'port' => 143,
            'encryption' => 'tls', // Supported: false, 'ssl', 'tls'
            'validate_cert' => 'novalidate-cert',
            'username' => 'yamaletdinov@archimed-soft.ru',
            'password' => 'yMCKECh345',
            'encoding' => 'UTF-8'
        ],

        'gmail' => [
            'host' => 'imap.gmail.com',
            'port' => 993,
            'encryption' => 'ssl', // Supported: false, 'ssl', 'tls'
            'validate_cert' => true,
            'username' => 'example@gmail.com',
            'password' => 'PASSWORD',
        ],

        'another' => [
            'host' => '',
            'port' => 993,
            'encryption' => false, // Supported: false, 'ssl', 'tls'
            'validate_cert' => true,
            'username' => '',
            'password' => '',
        ]
    ],
];
