<?php

use App\User;
use App\Project;

URL::forceScheme('https');

Route::get('/', 'MainController@index');

Route::group(['middleware' => 'visitors'], function() {
    Route::get('/login', 'Auth\LoginController@login');
    Route::post('/login', 'Auth\LoginController@postLogin');

    Route::get('/forgot-password', 'Auth\ForgotPasswordController@forgotPassword');
    Route::post('/forgot-password', 'Auth\ForgotPasswordController@postForgotPassword');

    Route::get('/reset/{email}/{resetCode}', 'Auth\ForgotPasswordController@resetPassword');
    Route::post('/reset/{email}/{resetCode}', 'Auth\ForgotPasswordController@postResetPassword');
});

Route::group(['middleware' => 'admin', 'prefix' => 'admin', 'namespace' => 'Admin'], function() {
    Route::get('/', 'AdminController@admin');
    
    Route::get('/users-group', 'AdminController@admin');
    Route::get('/getGroups', 'GroupsController@getGroups');

    // Route::get('/pusher', 'ChatsController@index');
    // Route::get('/pusher2', 'ChatsController@fetchMessages');
    // Route::post('/pusher2', 'ChatsController@sendMessage');

    // Route::get('/mail2', 'AdminController@mail');

    // Route::get('/markasread', 'AdmNotificationController@markAsRead');

    // //Route::get('/mail/{folder}', 'AdmMailController@mail');
    // Route::get('/mail/inbox', 'AdmMail2Controller@inboxMail');
    // Route::get('/mail/archive', 'AdmMail2Controller@archiveMail');
    // Route::get('/mail/trash', 'AdmMail2Controller@trashMail');
    // Route::get('/mail/drafts', 'AdmMail2Controller@draftsMail');
    // Route::get('/mail/sent', 'AdmMail2Controller@sentMail');
    // Route::get('/mail/junk', 'AdmMail2Controller@junkMail');
    // Route::get('/mail/get-mail', 'AdmMail2Controller@getMailInFolder');
    // Route::get('/message/{id}', 'AdmMail2Controller@getMessage');
    // Route::get('/mail/new', 'AdmMail2Controller@newMessage');
    // Route::post('/mail/new', 'AdmMail2Controller@postNewMessage');

    // Route::get('/users', 'AdmUser2Controller@users');
    // Route::get('/user/{id}', 'AdmUser2Controller@getUserInfo');
    // Route::post('/user/getUserProjects', 'AdmUser2Controller@getUserProjects');
    // Route::get('/users/switch-status', 'AdmUser2Controller@switchStatus');
    // Route::get('/users/upload', 'AdmUser2Controller@upload');
    // Route::get('/users/delete/{id}', 'AdmUser2Controller@userDel');
    // Route::post('/user/update/{id}', 'AdmUser2Controller@userUpdate');
    // Route::get('/users/edit/{id}', 'AdmUser2Controller@userEdit');
    // Route::get('/users/add', 'AdmUser2Controller@add');
    // Route::post('/user/add', 'AdmUser2Controller@store');
    // Route::get('/users/get-implement', 'AdmUser2Controller@getUser');

    // Route::get('/posts/get-all-post', 'AdmPostsController@getAllPostsAjax');

    // Route::get('/groups', 'AdmGroupController@groups');
    // Route::get('/groups/edit/{id}', 'AdmGroupController@groupEdit');
    // Route::post('/groups/update/{id}', 'AdmGroupController@groupUpdate');
    // Route::get('/groups/add', 'AdmGroupController@groupAdd');
    // Route::get('/group/add', 'AdmGroupController@postGroupAdd');
    // Route::get('/groups/delete/{id}', 'AdmGroupController@groupDel');

    // Route::get('/rights', 'AdmRightController@rights');
    // Route::post('/rights/save', 'AdmRightController@rightsSave');

    // Route::get('/tasks', 'AdmTasks2Controller@index');
    // Route::get('/tasks/add', 'AdmTasks2Controller@addTask');
    // Route::post('/tasks/add', 'AdmTasks2Controller@postAddTask');
    // Route::get('/task/edit/{id}', 'AdmTasks2Controller@editTask');
    // Route::get('/tasks/gettasks', 'AdmTasks2Controller@getTasks');
    // Route::post('/tasks/getWeekends', 'AdmTasks2Controller@getWeekends');
    // Route::get('/tasks/get-taks-am-calendar', 'AdmTasks2Controller@getTasksForAmcalendar');
    // Route::get('/tasks/get-taks-by-datetime', 'AdmTasks2Controller@getTasksByDateTime');
    // Route::get('/tasks/get-status', 'AdmTasks2Controller@getStatus');

    // Route::get('/project-task/edit/{id}', 'AdmTasks2Controller@editProjectTask');
    // Route::post('/project-task/edit/{id}', 'AdmTasks2Controller@postEditProjectTask');
    // Route::get('/project-task/getClarificationQuestions', 'AdmTasks2Controller@getClarificationQuestions');

    // Route::get('/project-additional-task/edit/{id}', 'AdmTasks2Controller@editProjectAdditionalTask');

    // Route::get('/calendar', 'AdmCalendar2Controller@index');
    // Route::post('/calendar/add_event', 'AdmCalendar2Controller@addEvent');
    // Route::get('/calendar/del_event/{id}', 'AdmCalendar2Controller@delEvent');
    // Route::post('/calendar/add-project-task', 'AdmCalendar2Controller@addProjectTask');
    // Route::get('/calendar/add-project-additional-task', 'AdmCalendar2Controller@addProjectAdditionalTask');

    // Route::get('/events/add', 'AdmEventController@addEvent');
    // Route::post('/events/add', 'AdmEventController@postAddEvent');

    // Route::get('/projects', 'AdmProjects2Controller@index');
    // Route::get('/project/edit/{id}', 'AdmProjects2Controller@edit');
    // Route::get('/project/close/{id}', 'AdmProjects2Controller@close');
    // Route::post('/project/close/{id}', 'AdmProjects2Controller@postClose');
    // Route::get('/projects/add', 'AdmProjects2Controller@add');
    // Route::post('/projects/add', 'AdmProjects2Controller@postAdd');
    // Route::get('/projects/sort', 'AdmProjects2Controller@sortProjectTasks');
    // Route::get('/projects/sort_task', 'AdmProjects2Controller@sortTasks');
    // Route::get('/projects/task_search', 'AdmProjects2Controller@searchTasks');
    // Route::get('/projects/project_search', 'AdmProjects2Controller@searchProjects');
    // Route::get('/projects/getProjectByClientId', 'AdmProjects2Controller@getProjectByClientId');
    // Route::get('/projects/getProjectByClientName', 'AdmProjects2Controller@getProjectByClientName');
    // Route::post('/projects/add-additional-task', 'AdmProjects2Controller@addAdditionalTask');

    // Route::get('/comments/add', 'AdmComment2Controller@add');
    // Route::get('/comment/del/{id}', 'AdmComment2Controller@del');

    // Route::get('/contact/add', 'AdmContact2Controller@add');
    // Route::get('/contact/del/{id}', 'AdmContact2Controller@del');
    // Route::get('/contact/edit/{id}', 'AdmContact2Controller@update');
    // Route::get('/contact/save', 'AdmContact2Controller@saveContact');
    // Route::get('/contact/get-contact', 'AdmContact2Controller@getClientsContact');

    // Route::get('/group-tasks', 'AdmGroupTasksController@index');
    // Route::get('/group-tasks/get-task-groups', 'AdmGroupTasksController@getAllTaskGroup');
    // Route::get('/group-tasks/get-category-printform', 'AdmGroupTasksController@getCategoryPrintform');
    // Route::get('/group-tasks/add', 'AdmGroupTasksController@add');
    // Route::post('/group-tasks/add', 'AdmGroupTasksController@postAdd');
    // Route::get('/group-tasks/edit/{id}', 'AdmGroupTasksController@edit');
    // Route::post('/group-tasks/edit/{id}', 'AdmGroupTasksController@postEdit');
    // Route::get('/group-tasks/del/{id}', 'AdmGroupTasksController@del');
    // Route::get('/group-tasks/get-all-tasks', 'AdmGroupTasksController@getAllTasks');
    // Route::get('/group-tasks/get-all-tasks-status', 'AdmGroupTasksController@getAllTasksStatus');

    // Route::get('/modules', 'AdmModules2Controller@index');
    // Route::get('/modules/get-all-modules', 'AdmModules2Controller@getAllModule');
    // Route::get('/modules/add', 'AdmModules2Controller@add');
    // Route::post('/modules/add', 'AdmModules2Controller@postAdd');
    // Route::get('/modules/edit/{id}', 'AdmModules2Controller@edit');
    // Route::post('/modules/edit/{id}', 'AdmModules2Controller@postEdit');
    // Route::get('/modules/del/{id}', 'AdmModules2Controller@del');
    // Route::get('/module-rule/del/{id}', 'AdmModules2Controller@delModuleRule');
    // Route::get('/module-rule/del-question/{mid}/{qid}', 'AdmModules2Controller@delModuleQuestion');
    // Route::get('/module-rule/update-question', 'AdmModules2Controller@updateModuleQuestion');
    // Route::get('/module-rule/add-question', 'AdmModules2Controller@addModuleQuestion');
    // Route::post('/module-rule/getRule', 'AdmModules2Controller@getModuleRule');
    // Route::get('/module-rule/getTasks', 'AdmModules2Controller@getModuleTasks');
    // Route::get('/module-rule/add-rule', 'AdmModules2Controller@addModulleRule');
    // Route::get('/module-rule/update-rule', 'AdmModules2Controller@updateModulleRule');
    // Route::get('/module-rule/update-project-rule', 'AdmModules2Controller@updateProjectModulleRule');

    // Route::get('/tasks-status', 'AdmTasksStatusController@index');
    // Route::get('/tasks-status/add', 'AdmTasksStatusController@add');
    // Route::post('/tasks-status/add', 'AdmTasksStatusController@postAdd');
    // Route::get('/tasks-status/edit/{id}', 'AdmTasksStatusController@edit');
    // Route::post('/tasks-status/edit/{id}', 'AdmTasksStatusController@postEdit');
    // Route::get('/tasks-status/del/{id}', 'AdmTasksStatusController@del');

    // Route::get('/settings', 'AdmSettings2Controller@index');
    // Route::post('/settings/save', 'AdmSettings2Controller@save');
    // Route::get('/settings/get-work-calendar', 'AdmSettings2Controller@getWorkCalendar');

    // Route::get('/clients', 'AdmClients2Controller@index');
    // Route::get('/clients/add', 'AdmClients2Controller@add');
    // Route::post('/clients/add', 'AdmClients2Controller@postAdd');
    // Route::get('/clients/del/{id}', 'AdmClients2Controller@del');
    // Route::get('/clients/edit/{id}', 'AdmClients2Controller@edit');
    // Route::post('/clients/edit/{id}', 'AdmClients2Controller@postEdit');
    // Route::get('/clients/get-client-by-project-id', 'AdmClients2Controller@getClients');

    // Route::get('/citys', 'AdmCitysController@index');
    // Route::get('/citys/add', 'AdmCitysController@add');
    // Route::post('/citys/add', 'AdmCitysController@postAdd');
    // Route::get('/citys/del/{id}', 'AdmCitysController@delete');
    // Route::get('/citys/edit/{id}', 'AdmCitysController@edit');
    // Route::post('/citys/edit/{id}', 'AdmCitysController@postEdit');

    // Route::get('/calls', 'AdmCalls2Controller@index');
    // Route::get('/calls/edit/{id}', 'AdmCalls2Controller@edit');

    // Route::get('/printform-category', 'AdmPrintForms2Controller@categoryRef');
    // Route::get('/printform-category/get-category-norm', 'AdmPrintForms2Controller@getCategoryNorm');
    // Route::get('/printform-category/add', 'AdmPrintForms2Controller@addCategoryRef');
    // Route::post('/printform-category/add', 'AdmPrintForms2Controller@postAddCategoryRef');
    // Route::get('/printform-category/del/{id}', 'AdmPrintForms2Controller@delCategoryRef');
    // Route::get('/printform-category/edit/{id}', 'AdmPrintForms2Controller@editCategoryRef');
    // Route::post('/printform-category/edit/{id}', 'AdmPrintForms2Controller@postEdittCategoryRef');
    // Route::get('/print-form', 'AdmPrintForms2Controller@index');
    // Route::get('/print-form/add', 'AdmPrintForms2Controller@add');
    // Route::post('/print-form/add', 'AdmPrintForms2Controller@postAdd');
    // Route::get('/print-form/upload', 'AdmPrintForms2Controller@upload');
    // Route::get('/print-form/edit/{id}', 'AdmPrintForms2Controller@edit');
    // Route::get('/print-form/raw/{id}', 'AdmPrintForms2Controller@raw');
    // Route::post('/print-form/raw/{id}', 'AdmPrintForms2Controller@postRaw');
    // Route::get('/print-form/del/{id}', 'AdmPrintForms2Controller@delete');
    // Route::get('/print-form/save-field', 'AdmPrintForms2Controller@saveField');
    // Route::get('/print-form/get-status', 'AdmPrintForms2Controller@getStatus');
    // Route::get('/print-form/get-category', 'AdmPrintForms2Controller@getCategory');
    // Route::get('/print-form/get-form-hisstory', 'AdmPrintForms2Controller@getFormHistory');

    // Route::get('/documents', 'AdmDocuments2Controller@index');
    // Route::get('/documents/add', 'AdmDocuments2Controller@add');
    // Route::post('/documents/add', 'AdmDocuments2Controller@postAdd');
    // Route::get('/documents/del/{id}', 'AdmDocuments2Controller@del');
    // Route::get('/documents/edit/{id}', 'AdmDocuments2Controller@update');
    // Route::post('/documents/edit/{id}', 'AdmDocuments2Controller@postUpdate');
    // Route::get('/documents/getTypeDoc', 'AdmDocuments2Controller@getTypeDoc');

    // Route::get('/doc', 'AdmDocController@index');

    // Route::get('/callback', 'AdmCallbackSettingsController@index');
    // Route::post('/callback/save-settings', 'AdmCallbackSettingsController@seveSettings');
    // Route::get('/callback/add-work-questions', 'AdmCallbackSettingsController@addWorkQuestions');
    // Route::get('/callback/add-close-questions', 'AdmCallbackSettingsController@addCloseQuestions');
    // Route::post('/callback/add-work-questions', 'AdmCallbackSettingsController@postAddWorkQuestions');
    // Route::post('/callback/add-close-questions', 'AdmCallbackSettingsController@postAddCloseQuestions');
    // Route::get('/callback/edit-work-questions/{id}', 'AdmCallbackSettingsController@editWorkQuestions');
    // Route::post('/callback/edit-work-questions/{id}', 'AdmCallbackSettingsController@postEditWorkQuestions');
    // Route::get('/callback/edit-close-questions/{id}', 'AdmCallbackSettingsController@editCloseQuestions');
    // Route::post('/callback/edit-close-questions/{id}', 'AdmCallbackSettingsController@postEditCloseQuestions');
    // Route::get('/callback/del-work-questions/{id}', 'AdmCallbackSettingsController@delWorkQuestions');
    // Route::get('/callback/del-close-questions/{id}', 'AdmCallbackSettingsController@delCloseQuestions');
    // Route::post('/callback/create-client-from-mail', 'AdmCalls2Controller@createClientFromMail');

    // Route::get('/calculate-print-form', 'AdmPrintForms2Controller@calculate');

    // Route::get('/get-notifications', 'AdmNotificationController@getNotification');

    // Route::get('/check-list', 'AdmCheckListController@index');
    // Route::get('/check-list/add', 'AdmCheckListController@add');
    // Route::post('/check-list/add', 'AdmCheckListController@postAdd');
    // Route::get('/check-list/del/{id}', 'AdmCheckListController@del');
    // Route::get('/check-list/edit/{id}', 'AdmCheckListController@edit');
    // Route::post('/check-list/edit/{id}', 'AdmCheckListController@postEdit');
});

Route::get('/test', 'YaCloudController@test');
Route::post('/test', 'YaCloudController@test2');
Route::post('/logout', 'Auth\LoginController@logout');
Route::get('/activate/{email}/{activationCode}', 'ActivationController@activate');
Route::get('/getClients', 'MainController@getClients');
Route::get('/getContact/{id}', 'MainController@getContact');
Route::get('/getClientProject/{id}', 'MainController@getClientProject');
Route::get('/getProjectTask/{id}', 'MainController@getProjectTask');
Route::post('/project-task/addTaskFromCalendar', 'MainController@addTaskFromCalendar');
Route::post('/project-task/addTaskFromCalendar2', 'MainController@addTaskFromCalendar2');
Route::post('/project-task/addEvent', 'MainController@addEvent');
Route::post('/project-task/addEvent2', 'MainController@addEvent2');
Route::get('/getProjectModules/{id}', 'MainController@getProjectModules');
Route::get('/getGroupTask', 'MainController@getGroupTask');
Route::get('/getImplementers', 'MainController@getImplementers');
Route::get('/getUsers', 'MainController@getUsers');
Route::get('/getQualityAll', 'MainController@getQualityAll');
Route::get('/getQuality', 'MainController@getQuality');
Route::get('/calendar/{user_id}/{date}', 'MainController@CalendarRedirect');

Route::get('/files/{type}/{id}', 'FileController@index');
Route::post('/files/add', 'FileController@store');
Route::post('/files/getForms/{id}', 'FileController@getForms');
Route::post('/files/print-form/add', 'FileController@storePrintForm');
Route::post('/files/attendant/add', 'FileController@storeAttendant'); 
Route::post('/files/edit/{id}', 'FileController@edit');
Route::post('/files/delete/{id}', 'FileController@destroy');





Route::group(['middleware' => 'implementer', 'prefix' => 'implementer', 'namespace' => 'Implementer'], function() {
    Route::get('/', 'ImplementerController@implementer');
    Route::get('/test', 'ImplementerController@test');
    Route::get('/calls', 'ImplementerController@implementer');
    Route::get('/project-task/edit/{id}/{date}', 'ImplementerController@implementer');
    Route::post('/project-task/edit/{id}', 'ImpTasks2Controller@postEditProjectTask'); 
    Route::get('/users/getUserId', 'ImpUser2Controller@getUserId');
    Route::get('/project/raw/{id}', 'ImplementerController@implementer');
    Route::get('/project/edit/{id}', 'ImplementerController@implementer');
    Route::get('/project/getRaw/{id}', 'ImpProjects2Controller@getRaw');
    Route::post('/project/getChekList', 'ImpProjects2Controller@getChekList');
    Route::post('/project/saveRaw', 'ImpProjects2Controller@saveRaw');
    Route::post('/form/saveRaw', 'ImpPrintForms2Controller@formSaveRaw');
    Route::post('/users/getLoad', 'ImpUser2Controller@getLoadUser');
    Route::post('/users/get-implementers', 'ImpUser2Controller@getImplementers');
    Route::post('/calendar/add-project-task', 'ImpCalendar2Controller@addProjectTask');
    Route::get('/getTask/{id}', 'ImpTasks2Controller@getTask');
    Route::post('/project-task/getClarificationQuestions', 'ImpTasks2Controller@getClarificationQuestions');
    Route::get('/tasks/gettasks/{id}', 'ImpTasks2Controller@getTasks');
    Route::get('/settings', 'ImplementerController@implementer');
    Route::get('/users', 'ImplementerController@implementer');
    Route::get('/users/add', 'ImplementerController@implementer');
    Route::get('/users/edit/{id}', 'ImplementerController@implementer');
    Route::get('/getUsers', 'ImpUser2Controller@getUsers');
    Route::get('/getGroups', 'ImpUser2Controller@getGroups');
    Route::post('/users/add', 'ImpUser2Controller@store');
    Route::post('/users/switch-status', 'ImpUser2Controller@switchStatus');
    Route::post('/users/delete', 'ImpUser2Controller@userDel');
    Route::post('/tasks/editevent', 'ImpTasks2Controller@editEvent');
    Route::post('/user/update', 'ImpUser2Controller@userUpdate');
    Route::get('/user/get-user/{id}', 'ImpUser2Controller@userEdit');
    Route::get('/callback', 'ImplementerController@implementer');
    Route::get('/group-tasks', 'ImplementerController@implementer');
    Route::get('/getGroupTasks', 'ImpGroupTasksController@getGroupTasks');
    Route::get('/group-tasks/add', 'ImplementerController@implementer');
    Route::post('/group-tasks/add', 'ImpGroupTasksController@postAdd');
    Route::get('/group-tasks/del/{id}', 'ImpGroupTasksController@del');
    Route::get('/group-tasks/editGroupTask/{id}', 'ImpGroupTasksController@edit');
    Route::get('/group-tasks/edit/{id}', 'ImplementerController@implementer');
    Route::post('/group-tasks/edit/{id}', 'ImpGroupTasksController@postEdit');
    Route::get('/tasks-status', 'ImplementerController@implementer');
    Route::get('/getTaskStatus', 'ImpTasksStatusController@index');
    Route::get('/tasks-status/add', 'ImplementerController@implementer');
    Route::post('/tasks-status/add', 'ImpTasksStatusController@postAdd');
    Route::post('/project-task/delEvent', 'ImpTasks2Controller@delEvent');
    Route::post('/project-task/delCont', 'ImpTasks2Controller@delCont');
    Route::post('/projects/editCont', 'ImpProjects2Controller@editCont');
    Route::get('/tasks-status/edit/{id}', 'ImplementerController@implementer');
    Route::get('/editTaskStatus/{id}', 'ImpTasksStatusController@edit');
    Route::post('/tasks-status/edit/{id}', 'ImpTasksStatusController@postEdit');
    Route::get('/tasks-status/del/{id}', 'ImpTasksStatusController@del');
    Route::get('/modules', 'ImplementerController@implementer');
    Route::get('/getModules', 'ImpModules2Controller@index');
    Route::get('/modules/add', 'ImplementerController@implementer');
    Route::post('/saveModules', 'ImpModules2Controller@postAdd');
    Route::get('/modules/edit/{id}', 'ImplementerController@implementer');
    Route::post('/editModules/{id}', 'ImpModules2Controller@postEdit');
    Route::get('/getModule/{id}', 'ImpModules2Controller@getModule');
    Route::post('/delModuleRule', 'ImpModules2Controller@delModuleRule');
    Route::post('/delModule', 'ImpModules2Controller@del');
    Route::get('/clients', 'ImplementerController@implementer');
    Route::get('/clients/add', 'ImplementerController@implementer');
    Route::get('/projects/get-comments/{id}', 'ImpComment2Controller@getCommentsByProjectId');
    Route::get('/projects/get-all-comments/{id}', 'ImpComment2Controller@getAllCommentsByProjectId');
    Route::get('/projects/get-project/{id}', 'ImpProjects2Controller@getProject');
    Route::post('/projects/editQuestions', 'ImpProjects2Controller@editQuestions');
    Route::get('/project/close/{id}', 'ImplementerController@implementer');
    Route::post('/project/close/{id}', 'ImpProjects2Controller@postClose');
    Route::get('/get-all-implementer', 'ImplementerController@getAllImplementer');
    Route::get('/getClients', 'ImpClients2Controller@getClients');
    Route::get('/getClientProject/{id}', 'ImpClients2Controller@getClientProject');
    Route::get('/getProjectTask/{id}', 'ImpProjects2Controller@getProjectTask');
    Route::post('/projects/add-additional-task', 'ImpProjects2Controller@addAdditionalTask');
    Route::post('/projects/add-comment', 'ImpProjects2Controller@addComment');
    Route::get('/getcloseproject', 'ImpProjects2Controller@getCloseProject');
    Route::get('/getslipproject', 'ImpProjects2Controller@getSlipProject');
    Route::get('/gettodayproject', 'ImpProjects2Controller@getTodayProject');
    Route::get('/getfireproject', 'ImpProjects2Controller@getFireProject');
    Route::post('/projects/add-contact', 'ImpContact2Controller@addContact');
    Route::post('/projects/changeFinishDate', 'ImpProjects2Controller@changeFinishDate');
    Route::post('/projects/changeImp', 'ImpProjects2Controller@changeImp');
    Route::get('/calendar/{id}/{date}', 'ImplementerController@implementer');
    Route::get('/getprojects', 'ImpProjects2Controller@getProjects');
    Route::post('/search-project', 'ImpProjects2Controller@searchProjects');
    Route::post('/getclosedateprojects', 'ImpProjects2Controller@getCloseDateProjects');
    Route::post('/getstatusprojects', 'ImpProjects2Controller@getStatusProjects');
    Route::get('/getprojectsbyimpid/{id}', 'ImpProjects2Controller@getProjectsByImpId');
    Route::post('/projects/saveEditComment', 'ImpComment2Controller@saveEditComment');
    Route::post('/getControlComments', 'ImpComment2Controller@getControlComments');
    Route::post('/getCloseComments', 'ImpComment2Controller@getCloseComments');
    Route::post('/tasks/settask', 'ImpTasks2Controller@setTasks');
    Route::get('/projects', 'ImplementerController@implementer');
    Route::get('/comment/del/{id}', 'ImpComment2Controller@del');
    Route::get('/waiting-list', 'ImplementerController@implementer');
    Route::get('/waiting-list/add', 'ImplementerController@implementer');
    Route::post('/waiting-list/add', 'WaitingListController@postAdd');
    Route::get('/getWaitingList', 'WaitingListController@getWaitingList');
    Route::post('/delWaitingList', 'WaitingListController@del');
    Route::get('/waiting-list/edit/{id}', 'ImplementerController@implementer');
    Route::get('/getList/{id}', 'WaitingListController@getList');
    Route::post('/waiting-list/edit', 'WaitingListController@postEdit');
    Route::get('/print-form', 'ImplementerController@implementer');
    Route::get('/getrawforms', 'ImpPrintForms2Controller@getRawForms');
    Route::post('/delForm', 'ImpPrintForms2Controller@delForm');
    Route::get('/getAttendant', 'ImpPrintForms2Controller@getAttendant');
    Route::get('/getProjectsForm', 'ImpPrintForms2Controller@getProjectsForm');
    Route::get('/getRaw/{id}', 'ImpPrintForms2Controller@getRaw');
    Route::get('/getDoneForm/{id}', 'ImpPrintForms2Controller@getDoneForm');
    Route::get('/print-form/raw/{id}', 'ImplementerController@implementer');
    Route::post('/print-form/raw/{id}', 'ImpPrintForms2Controller@postRaw');
    Route::post('/getstatuspform', 'ImpPrintForms2Controller@getstatuspform');
    Route::post('/add-comment', 'ImpPrintForms2Controller@addComment');
    Route::post('/edit-comment', 'ImpPrintForms2Controller@editComment');
    Route::get('/getProjectForms/{id}', 'ImpPrintForms2Controller@getProjectForms');
    Route::get('/print-form/attendant/add', 'ImplementerController@implementer');
    Route::post('/attendant/save', 'ImpPrintForms2Controller@postAttendant');
    Route::get('/print-form/attendant/{id}', 'ImplementerController@implementer');
    Route::get('/print-form/project/{id}', 'ImplementerController@implementer');
    Route::post('/print-form/attendant/edit', 'ImpPrintForms2Controller@postAttendantEdit'); 
    Route::post('/print-form/saveEditComment', 'ImpComment2Controller@saveEditComment'); 
    Route::get('/print-form/getQueueForm', 'ImpPrintForms2Controller@getQueueForm'); 
    Route::get('/quality-control', 'ImplementerController@implementer');
    Route::get('/contact-posts', 'ImplementerController@implementer');
    Route::get('/quality-control/add', 'ImplementerController@implementer');
    Route::get('/quality-control/edit/{id}', 'ImplementerController@implementer');
    Route::get('/calls-result', 'ImplementerController@implementer');
    Route::post('/quality-control/edit', 'QualityControlController@postEdit');
    Route::get('/quality-control/getQuestion/{id}', 'QualityControlController@getQuestion');
    Route::post('/quality-control/add', 'QualityControlController@postAdd');
    Route::get('/getQualityList', 'QualityControlController@getQualityList'); 
    Route::post('/delQualityList', 'QualityControlController@delQualityList');
    Route::get('/getFields', 'ImpUser2Controller@getFields'); 
    Route::post('/changePerm', 'ImpUser2Controller@changePerm');
    Route::post('/changePermProj', 'ImpUser2Controller@changePermProj');
    Route::post('/projects/delCont', 'ImpContact2Controller@delCont');
    Route::get('/projects/get-task/{id}', 'ImpProjects2Controller@getPTask');
    Route::get('/projects/get-card/{id}', 'ImpProjects2Controller@getPCard');
    Route::get('/projects/get-modules', 'ImpProjects2Controller@getModules');
    Route::post('/projects/set-modules', 'ImpProjects2Controller@setModules');
    Route::post('/searchProjects', 'ImpProjects2Controller@searchProjects2');
    Route::get('/project-categories', 'ImplementerController@implementer');
    //Route::get('/project/{id}/categories', 'ImpProjects2Controller@getCategory');
    //-----------------------------------------------


    Route::get('/markasread', 'ImpNotificationController@markAsRead');

    Route::get('/check-list', 'ImpCheckListController@index');
    Route::get('/check-list/add', 'ImpCheckListController@add');
    Route::post('/check-list/add', 'ImpCheckListController@postAdd');
    Route::get('/check-list/del/{id}', 'ImpCheckListController@del');
    Route::get('/check-list/edit/{id}', 'ImpCheckListController@edit');
    Route::post('/check-list/edit/{id}', 'ImpCheckListController@postEdit');

    //Route::get('/mail/{folder}', 'ImpMailController@mail');
    Route::get('/mail/inbox', 'ImpMail2Controller@inboxMail');
    Route::get('/mail/archive', 'ImpMail2Controller@archiveMail');
    Route::get('/mail/trash', 'ImpMail2Controller@trashMail');
    Route::get('/mail/drafts', 'ImpMail2Controller@draftsMail');
    Route::get('/mail/sent', 'ImpMail2Controller@sentMail');
    Route::get('/mail/junk', 'ImpMail2Controller@junkMail');
    Route::get('/mail/get-mail', 'ImpMail2Controller@getMailInFolder');
    Route::get('/message/{id}', 'ImpMail2Controller@getMessage');
    Route::get('/mail/new', 'ImpMail2Controller@newMessage');
    Route::post('/mail/new', 'ImpMail2Controller@postNewMessage');


    Route::get('/user/{id}', 'ImpUser2Controller@getUserInfo');
    Route::post('/user/getUserProjects', 'ImpUser2Controller@getUserProjects');
    Route::get('/users/upload', 'ImpUser2Controller@upload');
    //Route::get('/users/add', 'ImpUser2Controller@add');
    Route::get('/users/get-implement', 'ImpUser2Controller@getUser');

    

    Route::get('/groups', 'ImpGroupController@groups');
    Route::get('/groups/edit/{id}', 'ImpGroupController@groupEdit');
    Route::post('/groups/update/{id}', 'ImpGroupController@groupUpdate');
    Route::get('/groups/add', 'ImpGroupController@groupAdd');
    Route::get('/group/add', 'ImpGroupController@postGroupAdd');
    Route::get('/groups/delete/{id}', 'ImpGroupController@groupDel');

    Route::get('/rights', 'ImpRightController@rights');
    Route::post('/rights/save', 'ImpRightController@rightsSave');

    Route::get('/tasks', 'ImpTasks2Controller@index');
    Route::get('/gettaskslist', 'ImpTasks2Controller@getTasksList');
    Route::get('/tasks/add', 'ImpTasks2Controller@addTask');
    Route::post('/tasks/add', 'ImpTasks2Controller@postAddTask');
    Route::get('/task/edit/{id}', 'ImpTasks2Controller@editTask');
    Route::post('/tasks/edittask', 'ImpTasks2Controller@edit');
    Route::post('/tasks/getWeekends', 'ImpTasks2Controller@getWeekends');
    Route::get('/tasks/get-taks-am-calendar', 'ImpTasks2Controller@getTasksForAmcalendar');
    Route::get('/tasks/get-taks-by-datetime', 'ImpTasks2Controller@getTasksByDateTime');
    Route::get('/tasks/get-status', 'ImpTasks2Controller@getStatus');

    //Route::get('/project-task/getClarificationQuestions', 'ImpTasks2Controller@getClarificationQuestions');
    Route::get('/project-task/getProjectTask', 'ImpTasks2Controller@getProjectTask');
    Route::get('/project-task/getClientProject', 'ImpTasks2Controller@getClientProject');

    Route::get('/project-task/get-history', 'ImpTasks2Controller@getHistory');

    Route::get('/project-additional-task/edit/{id}', 'ImpTasks2Controller@editProjectAdditionalTask');

    //Route::get('/calendar/{id}', 'ImpCalendar2Controller@index');
    Route::get('/calendar/add-new-event/{date}', 'ImpCalendar2Controller@addNewEvent');
    Route::post('/calendar/add_event', 'ImpCalendar2Controller@addEvent');
    Route::post('/calendar/addTask', 'ImpCalendar2Controller@addTask');
    Route::get('/calendar/del_event/{id}', 'ImpCalendar2Controller@delEvent');
    Route::get('/calendar/add-project-additional-task', 'ImpCalendar2Controller@addProjectAdditionalTask');

    Route::get('/events/add', 'ImpEventController@addEvent');
    Route::post('/events/add', 'ImpEventController@postAddEvent');




    //Route::get('/project/edit/{id}', 'ImpProjects2Controller@edit');
    //Route::get('/project/raw/{id}', 'ImpProjects2Controller@raw');
    //Route::post('/project/raw/{id}', 'ImpProjects2Controller@postRaw');
    Route::get('/projects/add', 'ImpProjects2Controller@add');
    Route::post('/projects/add', 'ImpProjects2Controller@postAdd');
    Route::get('/projects/sort', 'ImpProjects2Controller@sortProjectTasks');
    Route::get('/projects/sort_task', 'ImpProjects2Controller@sortTasks');
    Route::get('/projects/task_search', 'ImpProjects2Controller@searchTasks');
    Route::get('/projects/project_search', 'ImpProjects2Controller@searchProjects');
    Route::get('/projects/getProjectByClientId', 'ImpProjects2Controller@getProjectByClientId');
    Route::get('/projects/getProjectByClientName', 'ImpProjects2Controller@getProjectByClientName');


    Route::get('/comments/add', 'ImpComment2Controller@add');
    Route::get('/comments/add2', 'ImpComment2Controller@add2');


    Route::get('/contact/add', 'ImpContact2Controller@add');
    Route::get('/contact/del/{id}', 'ImpContact2Controller@del');
    Route::get('/contact/edit/{id}', 'ImpContact2Controller@update');
    Route::get('/contact/save', 'ImpContact2Controller@saveContact');
    Route::get('/contact/get-contact', 'ImpContact2Controller@getClientsContact');

    Route::get('/group-tasks/get-task-groups', 'ImpGroupTasksController@getAllTaskGroup');
    Route::get('/group-tasks/get-category-printform', 'ImpGroupTasksController@getCategoryPrintform');

    Route::post('/group-tasks/add', 'ImpGroupTasksController@postAdd');
    Route::get('/group-tasks/get-all-tasks', 'ImpGroupTasksController@getAllTasks');
    Route::get('/group-tasks/get-all-tasks-status', 'ImpGroupTasksController@getAllTasksStatus');

    Route::get('/modules/get-all-modules', 'ImpModules2Controller@getAllModule');

    Route::get('/module-rule/del/{id}', 'ImpModules2Controller@delModuleRule');
    Route::get('/module-rule/del-question/{mid}/{qid}', 'ImpModules2Controller@delModuleQuestion');
    Route::get('/module-rule/update-question', 'ImpModules2Controller@updateModuleQuestion');
    Route::get('/module-rule/add-question', 'ImpModules2Controller@addModuleQuestion');
    Route::post('/module-rule/getRule', 'ImpModules2Controller@getModuleRule');
    Route::get('/module-rule/getTasks', 'ImpModules2Controller@getModuleTasks');
    Route::get('/module-rule/add-rule', 'ImpModules2Controller@addModulleRule');
    Route::get('/module-rule/update-rule', 'ImpModules2Controller@updateModulleRule');
    Route::get('/module-rule/update-project-rule', 'ImpModules2Controller@updateProjectModulleRule');




    Route::post('/settings/save', 'ImpSettings2Controller@save');
    Route::get('/settings/get-work-calendar', 'ImpSettings2Controller@getWorkCalendar');

    Route::get('/clients/add', 'ImpClients2Controller@add');
    Route::post('/clients/add', 'ImpClients2Controller@postAdd');
    Route::get('/clients/del/{id}', 'ImpClients2Controller@del');
    Route::get('/clients/edit/{id}', 'ImpClients2Controller@edit');
    Route::post('/clients/edit/{id}', 'ImpClients2Controller@postEdit');
    Route::get('/clients/get-client-by-project-id', 'ImpClients2Controller@getClients');

    Route::get('/citys', 'ImpCitysController@index');
    Route::get('/citys/add', 'ImpCitysController@add');
    Route::post('/citys/add', 'ImpCitysController@postAdd');
    Route::get('/citys/del/{id}', 'ImpCitysController@delete');
    Route::get('/citys/edit/{id}', 'ImpCitysController@edit');
    Route::post('/citys/edit/{id}', 'ImpCitysController@postEdit');

    Route::get('/printform-category', 'ImpPrintForms2Controller@categoryRef');
    Route::get('/printform-category/get-category-norm', 'ImpPrintForms2Controller@getCategoryNorm');
    Route::get('/printform-category/add', 'ImpPrintForms2Controller@addCategoryRef');
    Route::post('/printform-category/add', 'ImpPrintForms2Controller@postAddCategoryRef');
    Route::get('/printform-category/del/{id}', 'ImpPrintForms2Controller@delCategoryRef');
    Route::get('/printform-category/edit/{id}', 'ImpPrintForms2Controller@editCategoryRef');
    Route::post('/printform-category/edit/{id}', 'ImpPrintForms2Controller@postEdittCategoryRef');
    Route::get('/print-form/add', 'ImpPrintForms2Controller@add');
    Route::post('/print-form/add', 'ImpPrintForms2Controller@postAdd');
    Route::get('/print-form/upload', 'ImpPrintForms2Controller@upload');
    Route::get('/print-form/edit/{id}', 'ImpPrintForms2Controller@edit');
    Route::get('/print-form/del/{id}', 'ImpPrintForms2Controller@delete');
    Route::get('/print-form/save-field', 'ImpPrintForms2Controller@saveField');
    Route::get('/print-form/get-status', 'ImpPrintForms2Controller@getStatus');
    Route::get('/print-form/get-category', 'ImpPrintForms2Controller@getCategory');
    Route::get('/print-form/get-form-hisstory', 'ImpPrintForms2Controller@getFormHistory');

    Route::get('/documents', 'ImpDocuments2Controller@index');
    Route::get('/documents/add', 'ImpDocuments2Controller@add');
    Route::post('/documents/add', 'ImpDocuments2Controller@postAdd');
    Route::get('/documents/del/{id}', 'ImpDocuments2Controller@del');
    Route::get('/documents/edit/{id}', 'ImpDocuments2Controller@update');
    Route::post('/documents/edit/{id}', 'ImpDocuments2Controller@postUpdate');
    Route::get('/documents/getTypeDoc', 'ImpDocuments2Controller@getTypeDoc');

    Route::get('/doc', 'ImpDocController@index');

    Route::post('/callback/save-settings', 'ImpCallbackSettingsController@seveSettings');
    Route::get('/callback/add-work-questions', 'ImpCallbackSettingsController@addWorkQuestions');
    Route::get('/callback/add-close-questions', 'ImpCallbackSettingsController@addCloseQuestions');
    Route::post('/callback/add-work-questions', 'ImpCallbackSettingsController@postAddWorkQuestions');
    Route::post('/callback/add-close-questions', 'ImpCallbackSettingsController@postAddCloseQuestions');
    Route::get('/callback/edit-work-questions/{id}', 'ImpCallbackSettingsController@editWorkQuestions');
    Route::post('/callback/edit-work-questions/{id}', 'ImpCallbackSettingsController@postEditWorkQuestions');
    Route::get('/callback/edit-close-questions/{id}', 'ImpCallbackSettingsController@editCloseQuestions');
    Route::post('/callback/edit-close-questions/{id}', 'ImpCallbackSettingsController@postEditCloseQuestions');
    Route::get('/callback/del-work-questions/{id}', 'ImpCallbackSettingsController@delWorkQuestions');
    Route::get('/callback/del-close-questions/{id}', 'ImpCallbackSettingsController@delCloseQuestions');
    Route::post('/callback/create-client-from-mail', 'ImpCalls2Controller@createClientFromMail');

    Route::get('/calculate-print-form', 'ImpPrintForms2Controller@calculate');

    Route::get('/get-notifications', 'ImpNotificationController@getNotification');
});

Route::group(['middleware' => 'defaultcab', 'prefix' => 'defaultcab', 'namespace' => 'Defaultcab'], function() {
    Route::get('/', 'DefaultcabController@admin');

    Route::get('/users', 'UserController@users');
    Route::get('/user/{id}', 'UserController@getUserInfo');
    Route::post('/user/getUserProjects', 'UserController@getUserProjects');
    Route::get('/users/switch-status', 'UserController@switchStatus');
    Route::get('/users/upload', 'UserController@upload');
    Route::get('/users/delete/{id}', 'UserController@userDel');
    Route::post('/user/update/{id}', 'UserController@userUpdate');
    Route::get('/users/edit/{id}', 'UserController@userEdit');
    Route::get('/users/add', 'UserController@add');
    Route::post('/user/add', 'UserController@store');

    Route::get('/posts/get-all-post', 'PostsController@getAllPostsAjax');

    Route::get('/groups', 'GroupController@groups');
    Route::get('/groups/edit/{id}', 'GroupController@groupEdit');
    Route::post('/groups/update/{id}', 'GroupController@groupUpdate');
    Route::get('/groups/add', 'GroupController@groupAdd');
    Route::get('/group/add', 'GroupController@postGroupAdd');
    Route::get('/groups/delete/{id}', 'GroupController@groupDel');

    Route::get('/rights', 'RightController@rights');
    Route::post('/rights/save', 'RightController@rightsSave');

    Route::get('/tasks', 'TasksController@index');
    Route::get('/tasks/add', 'TasksController@addTask');
    Route::post('/tasks/add', 'TasksController@postAddTask');
    Route::get('/task/edit/{id}', 'TasksController@editTask');
    Route::get('/tasks/gettasks', 'TasksController@getTasks');
    Route::get('/tasks/get-taks-am-calendar', 'TasksController@getTasksForAmcalendar');

    Route::get('/project-task/edit/{id}', 'TasksController@editProjectTask');
    Route::post('/project-task/edit/{id}', 'TasksController@postEditProjectTask');
    //Route::get('/project-task/getClarificationQuestions', 'TasksController@getClarificationQuestions');

    Route::get('/project-additional-task/edit/{id}', 'TasksController@editProjectAdditionalTask');

    Route::get('/calendar', 'CalendarController@index');
    Route::post('/calendar/add_event', 'CalendarController@addEvent');
    Route::get('/calendar/del_event/{id}', 'CalendarController@delEvent');
    Route::get('/calendar/add-project-task', 'CalendarController@addProjectTask');

    Route::get('/events/add', 'EventController@addEvent');
    Route::post('/events/add', 'EventController@postAddEvent');

    Route::get('/projects', 'ProjectsController@index');
    Route::get('/project/edit/{id}', 'ProjectsController@edit');
    Route::get('/projects/add', 'ProjectsController@add');
    Route::post('/projects/add', 'ProjectsController@postAdd');
    Route::get('/projects/sort', 'ProjectsController@sortProjectTasks');
    Route::post('/projects/add-additional-task', 'ProjectsController@addAdditionalTask');

    Route::get('/comments/add', 'CommentController@add');
    Route::get('/comment/del/{id}', 'CommentController@del');

    Route::get('/contact/add', 'ContactController@add');
    Route::get('/contact/del/{id}', 'ContactController@del');
    Route::get('/contact/edit/{id}', 'ContactController@update');
    Route::get('/contact/save', 'ContactController@saveContact');
    Route::get('/contact/get-contact', 'ContactController@getClientsContact');

    Route::get('/group-tasks', 'GroupTasksController@index');
    Route::get('/group-tasks/get-task-groups', 'GroupTasksController@getAllTaskGroup');
    Route::get('/group-tasks/add', 'GroupTasksController@add');
    Route::post('/group-tasks/add', 'GroupTasksController@postAdd');
    Route::get('/group-tasks/edit/{id}', 'GroupTasksController@edit');
    Route::post('/group-tasks/edit/{id}', 'GroupTasksController@postEdit');
    Route::get('/group-tasks/del/{id}', 'GroupTasksController@del');
    Route::get('/group-tasks/get-all-tasks', 'GroupTasksController@getAllTasks');
    Route::get('/group-tasks/get-all-tasks-status', 'GroupTasksController@getAllTasksStatus');

    Route::get('/modules', 'ModulesController@index');
    Route::get('/modules/get-all-modules', 'ModulesController@getAllModule');
    Route::get('/modules/add', 'ModulesController@add');
    Route::post('/modules/add', 'ModulesController@postAdd');
    Route::get('/modules/edit/{id}', 'ModulesController@edit');
    Route::post('/modules/edit/{id}', 'ModulesController@postEdit');
    Route::get('/modules/del/{id}', 'ModulesController@del');
    Route::get('/module-rule/del/{id}', 'ModulesController@delModuleRule');
    Route::get('/module-rule/del-question/{mid}/{qid}', 'ModulesController@delModuleQuestion');
    Route::get('/module-rule/update-question', 'ModulesController@updateModuleQuestion');
    Route::get('/module-rule/add-question', 'ModulesController@addModuleQuestion');
    Route::post('/module-rule/getRule', 'ModulesController@getModuleRule');
    Route::get('/module-rule/getTasks', 'ModulesController@getModuleTasks');
    Route::get('/module-rule/add-rule', 'ModulesController@addModulleRule');
    Route::get('/module-rule/update-rule', 'ModulesController@updateModulleRule');
    Route::get('/module-rule/update-project-rule', 'ModulesController@updateProjectModulleRule');

    Route::get('/tasks-status', 'TasksStatusController@index');
    Route::get('/tasks-status/add', 'TasksStatusController@add');
    Route::post('/tasks-status/add', 'TasksStatusController@postAdd');
    Route::get('/tasks-status/edit/{id}', 'TasksStatusController@edit');
    Route::post('/tasks-status/edit/{id}', 'TasksStatusController@postEdit');
    Route::get('/tasks-status/del/{id}', 'TasksStatusController@del');

    Route::get('/settings', 'SettingsController@index');

    Route::get('/clients', 'ClientsController@index');
    Route::get('/clients/add', 'ClientsController@add');
    Route::post('/clients/add', 'ClientsController@postAdd');
    Route::get('/clients/del/{id}', 'ClientsController@del');
    Route::get('/clients/edit/{id}', 'ClientsController@edit');
    Route::post('/clients/edit/{id}', 'ClientsController@postEdit');

    Route::get('/citys', 'CitysController@index');
    Route::get('/citys/add', 'CitysController@add');
    Route::post('/citys/add', 'CitysController@postAdd');
    Route::get('/citys/del/{id}', 'CitysController@delete');
    Route::get('/citys/edit/{id}', 'CitysController@edit');
    Route::post('/citys/edit/{id}', 'CitysController@postEdit');

    Route::get('/calls', 'CallsController@index');
    Route::get('/calls/edit/{id}', 'CallsController@edit');

    Route::get('/print-form', 'PrintFormsController@index');

    Route::get('/documents', 'DocumentsController@index');
    Route::get('/documents/add', 'DocumentsController@add');
    Route::post('/documents/add', 'DocumentsController@postAdd');
    Route::get('/documents/del/{id}', 'DocumentsController@del');
    Route::get('/documents/edit/{id}', 'DocumentsController@update');
    Route::post('/documents/edit/{id}', 'DocumentsController@postUpdate');
    Route::get('/documents/getTypeDoc', 'DocumentsController@getTypeDoc');

    Route::get('/doc', 'DocController@index');
});

Route::group(['middleware' => 'manager', 'prefix' => 'manager', 'namespace' => 'Manager'], function() {
    Route::get('/', 'ManagerController@admin');
    Route::get('/calls', 'ManagerController@admin');
    Route::get('/projects/add', 'ManagerController@admin');
    Route::get('/clients/getClients', 'ClientsController@getClients');
    Route::get('/modules/getModules', 'ModulesController@getModules');
    Route::get('/documents/getDocuments', 'DocumentsController@getDocuments');
    Route::get('/checklist/getCheckList', 'Projects2Controller@getCheckList');
    Route::post('/projects/create', 'Projects2Controller@create');
    Route::get('/projects/clients/add', 'ManagerController@admin');
    Route::post('/projects/clients/add', 'Projects2Controller@postAddClients');
    Route::get('/posts/getPosts', 'PostsController@getPosts');
    Route::get('/citys/getCities', 'CitysController@getCities');
    Route::get('/clients/add', 'ClientsController@add');
    Route::post('/clients/add', 'ClientsController@postAdd');
    Route::post('/clients/getClient', 'ClientsController@getClient'); 
    Route::post('/modules/getNorm', 'ModulesController@getNorm');
    Route::get('/getProjects', 'Projects2Controller@getProjects');
    Route::get('/project/edit/{id}', 'ManagerController@admin');
    Route::get('/project/get-project/{id}', 'Projects2Controller@getProject');
    Route::get('/project/projects-count', 'Projects2Controller@getProjectsCount');
    Route::get('/calls', 'ManagerController@admin');
    Route::get('/getTask', 'CallsController@getTask');
    Route::get('/getFrequency', 'CallsController@getFrequency');
    Route::get('/getClose', 'CallsController@getClose');
    Route::get('/getQuality', 'CallsController@getQuality');
    Route::get('/getRequest', 'CallsController@getRequest');
    Route::post('/getRequestFilter', 'CallsController@getRequestFilter');
    Route::get('/getCall/{id}', 'CallsController@getCall');
    Route::get('/calls/edit/{id}', 'ManagerController@admin');
    Route::post('/calls/edit/{id}', 'CallsController@postEdit');
    Route::post('/request/edit', 'CallsController@postRequest');
    Route::get('/request/edit/{id}', 'ManagerController@admin');
    Route::get('/request/view/{id}', 'ManagerController@admin');
    Route::get('/callback/edit/{id}', 'ManagerController@admin');
    Route::get('/getCloseProject/{id}', 'CallsController@getCloseProject');
    Route::get('/getRequestProject/{id}', 'CallsController@getRequestProject');
    Route::get('/getRequestProjectView/{id}', 'CallsController@getRequestProjectView');
    Route::get('/getQualityProject/{id}', 'CallsController@getQualityProject');
    Route::post('/close/edit/{id}', 'Projects2Controller@postEditCloseProject');
    Route::post('/quality/edit/{id}', 'CallsController@postEditQualityProject');
    Route::get('/get-all-implementer', 'ManagerController@getAllImplementer');
    Route::get('/get-all-implementer-for-select', 'ManagerController@getAllImplementerForSelect');
    Route::get('/tasks/gettasks/{id}', 'TasksController@getTasks2');
    Route::post('/tasks/settask', 'Tasks2Controller@setTasks');
    Route::post('/tasks/getcurtask', 'TasksController@getCurTask');
    Route::post('/tasks/editevent', 'Tasks2Controller@editEvent');
    Route::post('/tasks/edittask', 'Tasks2Controller@editCont');
    Route::post('/project-task/delEvent', 'Tasks2Controller@delEvent');
    Route::post('/project-task/delCont', 'Tasks2Controller@delCont');
    Route::post('/changeImp', 'ManagerController@changeImp');
    Route::post('/changeEventName', 'ManagerController@changeEventName');
    Route::get('/calendar/{id}/{date}', 'ManagerController@admin');
    Route::get('/get-first-imp', 'ManagerController@getFirstImp');
    Route::get('/forms', 'ManagerController@admin');
    Route::get('/forms/pay/{id}', 'ManagerController@admin');
    Route::post('/forms/pay', 'PrintFormsController@postPay');
    Route::get('/add-form', 'ManagerController@admin');
    Route::get('/projects', 'ManagerController@admin');
    Route::post('/projects/changeImp', 'Projects2Controller@changeImp');
    Route::get('/getPay', 'PrintFormsController@getPay'); 
    Route::get('/getPayForm/{id}', 'PrintFormsController@getPForm');
    Route::post('/add-comment', 'PrintFormsController@addComment');
    Route::get('/mail', 'ManagerController@admin');
    Route::get('/callback/quality-edit/{id}', 'ManagerController@admin');
    Route::get('/mail/getFolders', 'MailController@getFolders');
    Route::post('/contact/add', 'ContactController@add');
    Route::post('/contact/del', 'ContactController@del');
    Route::post('/contact/edit/{id}', 'ContactController@edit');

    Route::get('/posts', 'PostsController@getPosts');

    Route::get('/projects/get-comments/{id}', 'CommentController@getCommentsByProjectId');
    Route::get('/projects/get-all-comments/{id}', 'CommentController@getAllCommentsByProjectId');
    Route::post('/projects/add-comment', 'CommentController@addComment');
    Route::post('/projects/comment/del/{id}', 'CommentController@del');
    Route::post('/projects/comment/edit', 'CommentController@edit');

    Route::get('/projects/get-contact/{id}', 'ContactController@getPContacts');
    Route::post('/projects/add-contact', 'ContactController@addPContacts');
    Route::post('/projects/del-contact', 'ContactController@delPContacts');
    Route::post('/projects/edit-contact', 'ContactController@editPContacts');

    Route::get('/projects/get-task/{id}', 'Projects2Controller@getPTask');

    Route::get('/projects/get-card/{id}', 'Projects2Controller@getPCard');

    Route::post('/projects/editQuestions', 'Projects2Controller@editQuestions');

    Route::post('/searchProjects', 'Projects2Controller@searchProjects2');


    Route::post('/getControlComments', 'CommentController@getControlComments');

    //Route::get('/', 'ManagerController@admin');
    /*Route::get('/checklist/getCheckList', 'Projects2Controller@getCheckList');

    Route::get('/users', 'UserController@users');
    Route::get('/user/{id}', 'UserController@getUserInfo');
    Route::post('/user/getUserProjects', 'UserController@getUserProjects');
    Route::get('/users/switch-status', 'UserController@switchStatus');
    Route::get('/users/upload', 'UserController@upload');
    Route::get('/users/delete/{id}', 'UserController@userDel');
    Route::post('/user/update/{id}', 'UserController@userUpdate');
    Route::get('/users/edit/{id}', 'UserController@userEdit');
    Route::get('/users/add', 'UserController@add');
    Route::post('/user/add', 'UserController@store');

    Route::get('/posts/get-all-post', 'PostsController@getAllPostsAjax');
    Route::get('/posts/getPosts', 'PostsController@getPosts');

    Route::get('/groups', 'GroupController@groups');
    Route::get('/groups/edit/{id}', 'GroupController@groupEdit');
    Route::post('/groups/update/{id}', 'GroupController@groupUpdate');
    Route::get('/groups/add', 'GroupController@groupAdd');
    Route::get('/group/add', 'GroupController@postGroupAdd');
    Route::get('/groups/delete/{id}', 'GroupController@groupDel');

    Route::get('/rights', 'RightController@rights');
    Route::post('/rights/save', 'RightController@rightsSave');

    Route::get('/tasks', 'TasksController@index');
    Route::get('/tasks/add', 'TasksController@addTask');
    Route::post('/tasks/add', 'TasksController@postAddTask');
    Route::get('/task/edit/{id}', 'TasksController@editTask');
    Route::get('/tasks/gettasks', 'TasksController@getTasks');
    Route::get('/tasks/get-taks-am-calendar', 'TasksController@getTasksForAmcalendar');

    Route::get('/project-task/edit/{id}', 'TasksController@editProjectTask');
    Route::post('/project-task/edit/{id}', 'TasksController@postEditProjectTask');
    Route::get('/project-task/getClarificationQuestions', 'TasksController@getClarificationQuestions');
    Route::get('/project-task/get-history', 'TasksController@getHistory');

    Route::get('/calendar', 'CalendarController@index');
    Route::post('/calendar/add_event', 'CalendarController@addEvent');
    Route::get('/calendar/del_event/{id}', 'CalendarController@delEvent');
    Route::post('/calendar/add-project-task', 'CalendarController@addProjectTask');

    Route::get('/events/add', 'EventController@addEvent');
    Route::post('/events/add', 'EventController@postAddEvent');


    Route::post('/projects/create', 'Projects2Controller@create');
    Route::get('/projects/clients/add', 'Projects2Controller@addClients');
    Route::post('/projects/clients/add', 'Projects2Controller@postAddClients');
    Route::get('/project/edit/{id}', 'Projects2Controller@edit');
    Route::get('/project/close/{id}', 'Projects2Controller@close');
    Route::post('/project/close/{id}', 'Projects2Controller@postClose');
    Route::get('/projects/add', 'Projects2Controller@add');
    Route::post('/projects/add', 'Projects2Controller@postAdd');
    Route::get('/projects/sort', 'Projects2Controller@sortProjectTasks');
    Route::get('/projects/get-close-projects', 'Projects2Controller@getCloseProjects');
    Route::get('/projects/get-work-projects', 'Projects2Controller@getWorkProjects');
    Route::post('/projects/add-additional-task', 'Projects2Controller@addAdditionalTask');

    Route::get('/comments/add', 'CommentController@add');
    Route::get('/comment/del/{id}', 'CommentController@del');

    Route::get('/contact/add', 'ContactController@add');
    Route::get('/contact/del/{id}', 'ContactController@del');
    Route::get('/contact/edit/{id}', 'ContactController@update');
    Route::get('/contact/save', 'ContactController@saveContact');
    Route::get('/contact/get-contact', 'ContactController@getClientsContact');

    Route::get('/group-tasks', 'GroupTasksController@index');
    Route::get('/group-tasks/get-task-groups', 'GroupTasksController@getAllTaskGroup');
    Route::get('/group-tasks/add', 'GroupTasksController@add');
    Route::post('/group-tasks/add', 'GroupTasksController@postAdd');
    Route::get('/group-tasks/edit/{id}', 'GroupTasksController@edit');
    Route::post('/group-tasks/edit/{id}', 'GroupTasksController@postEdit');
    Route::get('/group-tasks/del/{id}', 'GroupTasksController@del');
    Route::get('/group-tasks/get-all-tasks', 'GroupTasksController@getAllTasks');
    Route::get('/group-tasks/get-all-tasks-status', 'GroupTasksController@getAllTasksStatus');

    Route::get('/modules', 'ModulesController@index');
    Route::post('/modules/getNorm', 'ModulesController@getNorm');
    Route::get('/modules/getModules', 'ModulesController@getModules');
    Route::get('/modules/get-all-modules', 'ModulesController@getAllModule');
    Route::get('/modules/add', 'ModulesController@add');
    Route::post('/modules/add', 'ModulesController@postAdd');
    Route::get('/modules/edit/{id}', 'ModulesController@edit');
    Route::post('/modules/edit/{id}', 'ModulesController@postEdit');
    Route::get('/modules/del/{id}', 'ModulesController@del');
    Route::get('/module-rule/del/{id}', 'ModulesController@delModuleRule');
    Route::get('/module-rule/del-question/{mid}/{qid}', 'ModulesController@delModuleQuestion');
    Route::get('/module-rule/update-question', 'ModulesController@updateModuleQuestion');
    Route::get('/module-rule/add-question', 'ModulesController@addModuleQuestion');
    Route::post('/module-rule/getRule', 'ModulesController@getModuleRule');
    Route::get('/module-rule/getTasks', 'ModulesController@getModuleTasks');
    Route::get('/module-rule/add-rule', 'ModulesController@addModulleRule');
    Route::get('/module-rule/update-rule', 'ModulesController@updateModulleRule');
    Route::get('/module-rule/update-project-rule', 'ModulesController@updateProjectModulleRule');

    Route::get('/tasks-status', 'TasksStatusController@index');
    Route::get('/tasks-status/add', 'TasksStatusController@add');
    Route::post('/tasks-status/add', 'TasksStatusController@postAdd');
    Route::get('/tasks-status/edit/{id}', 'TasksStatusController@edit');
    Route::post('/tasks-status/edit/{id}', 'TasksStatusController@postEdit');
    Route::get('/tasks-status/del/{id}', 'TasksStatusController@del');

    Route::get('/settings', 'SettingsController@index');

    Route::get('/clients', 'ClientsController@index');
    Route::get('/clients/getClients', 'ClientsController@getClients');
    Route::post('/clients/getClient', 'ClientsController@getClient');
    Route::get('/clients/add', 'ClientsController@add');
    Route::post('/clients/add', 'ClientsController@postAdd');
    Route::get('/clients/del/{id}', 'ClientsController@del');
    Route::get('/clients/edit/{id}', 'ClientsController@edit');
    Route::post('/clients/edit/{id}', 'ClientsController@postEdit');

    Route::get('/citys', 'CitysController@index');
    Route::get('/citys/getCities', 'CitysController@getCities');
    Route::get('/citys/add', 'CitysController@add');
    Route::post('/citys/add', 'CitysController@postAdd');
    Route::get('/citys/del/{id}', 'CitysController@delete');
    Route::get('/citys/edit/{id}', 'CitysController@edit');
    Route::post('/citys/edit/{id}', 'CitysController@postEdit');

    Route::get('/documents', 'DocumentsController@index');
    Route::get('/documents/getDocuments', 'DocumentsController@getDocuments');
    Route::get('/documents/add', 'DocumentsController@add');
    Route::post('/documents/add', 'DocumentsController@postAdd');
    Route::get('/documents/del/{id}', 'DocumentsController@del');
    Route::get('/documents/edit/{id}', 'DocumentsController@update');
    Route::post('/documents/edit/{id}', 'DocumentsController@postUpdate');
    Route::get('/documents/getTypeDoc', 'DocumentsController@getTypeDoc');

    Route::get('/close/edit/{id}', 'Projects2Controller@editCloseProject');

    Route::get('/callback/edit/{id}', 'Projects2Controller@editCallbackProject');
    Route::get('/other/edit/{id}', 'Projects2Controller@editOtherProject');
    Route::post('/other/edit/{id}', 'Projects2Controller@postEditOtherProject');
    Route::post('/callback/edit/{id}', 'Projects2Controller@postEditCallbackProject');

    Route::get('/doc', 'DocController@index');
    Route::get('/doc/edit/{id}', 'DocController@edit');
    Route::get('/doc/sort', 'DocController@sort');
    Route::get('/doc/sort_type', 'DocController@sortType');
    Route::get('/doc/getQueueDoc', 'DocController@getQueueDoc');*/
});

Route::group(['middleware' => 'print_form', 'prefix' => 'print_form', 'namespace' => 'Print_form'], function() {
    Route::get('/', 'PrintController@admin');
    Route::get('/getPermForms', 'PrintController@getPermForms');
    Route::post('/changeFormPerm', 'PrintController@changeFormPerm');
    Route::get('/forms', 'PrintController@admin');
    Route::get('/tasks', 'PrintController@admin');
    Route::get('/calls-result', 'PrintController@admin');
    Route::get('/getRaw', 'PrintForms2Controller@getRaw'); 
    Route::get('/getPay', 'PrintForms2Controller@getPay');
    Route::post('/filterForms', 'PrintForms2Controller@filterForms');
    Route::get('/getHotForms', 'PrintForms2Controller@getHotForms');
    Route::get('/getQForms', 'PrintForms2Controller@getQForms');
    Route::get('/getClose', 'PrintForms2Controller@getClose');
    Route::get('/getAttendant', 'PrintForms2Controller@getAttendant');
    Route::get('/getAttendantById/{id}', 'PrintForms2Controller@getAttendantById');
    Route::get('/getPFormUser', 'User2Controller@getPFormUser');
    Route::get('/getPForm/{id}', 'PrintForms2Controller@getPForm');
    Route::get('/getEditForm/{id}', 'PrintForms2Controller@getEditForm');
    Route::get('/getPayForm/{id}', 'PrintForms2Controller@getPayForm');
    Route::get('/forms/raw/{id}', 'PrintController@admin');
    Route::get('/forms/pay/{id}', 'PrintController@admin');
    Route::get('/forms/edit/{id}', 'PrintController@admin');
    Route::get('/forms/returnInQueue/{id}', 'PrintForms2Controller@returnInQueue');
    Route::post('/forms/edit/{id}', 'PrintForms2Controller@postEdit');
    Route::post('/add-comment', 'PrintForms2Controller@addComment');
    Route::post('/forms/raw/{id}', 'PrintForms2Controller@postRaw');
    Route::post('/forms/pay', 'PrintForms2Controller@postPay');
    Route::get('/forms/attendant/edit/{id}', 'PrintController@admin');
    Route::post('/attendant/edit', 'PrintForms2Controller@postAttendantEdit');
    Route::get('/calculate-print-form/{id}', 'PrintForms2Controller@calculate');
    Route::get('/mail', 'PrintController@admin');
    Route::get('/mail/getFolders', 'MailController@getFolders');
    Route::get('/mail/getLeters', 'MailController@getLeters');
    Route::get('/mail/getLeter/{id}', 'MailController@getLeter');
    //---------------------------------------------------------------------------------------------

    Route::get('/markasread', 'NotificationController@markAsRead');

    //Route::get('/mail/{folder}', 'MailController@mail');
    Route::get('/mail/inbox', 'Mail2Controller@inboxMail');
    Route::get('/mail/archive', 'Mail2Controller@archiveMail');
    Route::get('/mail/trash', 'Mail2Controller@trashMail');
    Route::get('/mail/drafts', 'Mail2Controller@draftsMail');
    Route::get('/mail/sent', 'Mail2Controller@sentMail');
    Route::get('/mail/junk', 'Mail2Controller@junkMail');
    Route::get('/mail/get-mail', 'Mail2Controller@getMailInFolder');
    Route::get('/message/{id}', 'Mail2Controller@getMessage');
    Route::get('/mail/new', 'Mail2Controller@newMessage');
    Route::post('/mail/new', 'Mail2Controller@postNewMessage');

    Route::get('/users', 'User2Controller@users');
    Route::get('/user/{id}', 'User2Controller@getUserInfo');
    Route::post('/user/getUserProjects', 'User2Controller@getUserProjects');
    Route::get('/users/switch-status', 'User2Controller@switchStatus');
    Route::get('/users/upload', 'User2Controller@upload');
    Route::get('/users/delete/{id}', 'User2Controller@userDel');
    Route::post('/user/update/{id}', 'User2Controller@userUpdate');
    Route::get('/users/edit/{id}', 'User2Controller@userEdit');
    Route::get('/users/add', 'User2Controller@add');
    Route::post('/user/add', 'User2Controller@store');
    Route::get('/users/get-implement', 'User2Controller@getUser');

    Route::get('/posts/get-all-post', 'PostsController@getAllPostsAjax');

    Route::get('/groups', 'GroupController@groups');
    Route::get('/groups/edit/{id}', 'GroupController@groupEdit');
    Route::post('/groups/update/{id}', 'GroupController@groupUpdate');
    Route::get('/groups/add', 'GroupController@groupAdd');
    Route::get('/group/add', 'GroupController@postGroupAdd');
    Route::get('/groups/delete/{id}', 'GroupController@groupDel');

    Route::get('/rights', 'RightController@rights');
    Route::post('/rights/save', 'RightController@rightsSave');


    Route::get('/tasks/add', 'Tasks2Controller@addTask');
    Route::post('/tasks/add', 'Tasks2Controller@postAddTask');
    Route::get('/task/edit/{id}', 'Tasks2Controller@editTask');
    Route::get('/tasks/gettasks', 'Tasks2Controller@getTasks');
    Route::post('/tasks/getWeekends', 'Tasks2Controller@getWeekends');
    Route::get('/tasks/get-taks-am-calendar', 'Tasks2Controller@getTasksForAmcalendar');
    Route::get('/tasks/get-taks-by-datetime', 'Tasks2Controller@getTasksByDateTime');
    Route::get('/tasks/get-status', 'Tasks2Controller@getStatus');

    Route::get('/project-task/edit/{id}', 'Tasks2Controller@editProjectTask');
    Route::post('/project-task/edit/{id}', 'Tasks2Controller@postEditProjectTask');
    Route::get('/project-task/getClarificationQuestions', 'Tasks2Controller@getClarificationQuestions');

    Route::get('/project-additional-task/edit/{id}', 'Tasks2Controller@editProjectAdditionalTask');

    Route::get('/calendar', 'Calendar2Controller@index');
    Route::post('/calendar/add_event', 'Calendar2Controller@addEvent');
    Route::get('/calendar/del_event/{id}', 'Calendar2Controller@delEvent');
    Route::post('/calendar/add-project-task', 'Calendar2Controller@addProjectTask');
    Route::get('/calendar/add-project-additional-task', 'Calendar2Controller@addProjectAdditionalTask');

    Route::get('/events/add', 'EventController@addEvent');
    Route::post('/events/add', 'EventController@postAddEvent');

    Route::get('/projects', 'Projects2Controller@index');
    Route::get('/project/edit/{id}', 'Projects2Controller@edit');
    Route::get('/project/close/{id}', 'Projects2Controller@close');
    Route::post('/project/close/{id}', 'Projects2Controller@postClose');
    Route::get('/projects/add', 'Projects2Controller@add');
    Route::post('/projects/add', 'Projects2Controller@postAdd');
    Route::get('/projects/sort', 'Projects2Controller@sortProjectTasks');
    Route::get('/projects/sort_task', 'Projects2Controller@sortTasks');
    Route::get('/projects/task_search', 'Projects2Controller@searchTasks');
    Route::get('/projects/project_search', 'Projects2Controller@searchProjects');
    Route::get('/projects/getProjectByClientId', 'Projects2Controller@getProjectByClientId');
    Route::get('/projects/getProjectByClientName', 'Projects2Controller@getProjectByClientName');
    Route::post('/projects/add-additional-task', 'Projects2Controller@addAdditionalTask');

    Route::get('/comments/add', 'Comment2Controller@add');
    Route::get('/comment/del/{id}', 'Comment2Controller@del');

    Route::get('/contact/add', 'Contact2Controller@add');
    Route::get('/contact/del/{id}', 'Contact2Controller@del');
    Route::get('/contact/edit/{id}', 'Contact2Controller@update');
    Route::get('/contact/save', 'Contact2Controller@saveContact');
    Route::get('/contact/get-contact', 'Contact2Controller@getClientsContact');

    Route::get('/group-tasks', 'GroupTasksController@index');
    Route::get('/group-tasks/get-task-groups', 'GroupTasksController@getAllTaskGroup');
    Route::get('/group-tasks/get-category-printform', 'GroupTasksController@getCategoryPrintform');
    Route::get('/group-tasks/add', 'GroupTasksController@add');
    Route::post('/group-tasks/add', 'GroupTasksController@postAdd');
    Route::get('/group-tasks/edit/{id}', 'GroupTasksController@edit');
    Route::post('/group-tasks/edit/{id}', 'GroupTasksController@postEdit');
    Route::get('/group-tasks/del/{id}', 'GroupTasksController@del');
    Route::get('/group-tasks/get-all-tasks', 'GroupTasksController@getAllTasks');
    Route::get('/group-tasks/get-all-tasks-status', 'GroupTasksController@getAllTasksStatus');

    Route::get('/modules', 'Modules2Controller@index');
    Route::get('/modules/get-all-modules', 'Modules2Controller@getAllModule');
    Route::get('/modules/add', 'Modules2Controller@add');
    Route::post('/modules/add', 'Modules2Controller@postAdd');
    Route::get('/modules/edit/{id}', 'Modules2Controller@edit');
    Route::post('/modules/edit/{id}', 'Modules2Controller@postEdit');
    Route::get('/modules/del/{id}', 'Modules2Controller@del');
    Route::get('/module-rule/del/{id}', 'Modules2Controller@delModuleRule');
    Route::get('/module-rule/del-question/{mid}/{qid}', 'Modules2Controller@delModuleQuestion');
    Route::get('/module-rule/update-question', 'Modules2Controller@updateModuleQuestion');
    Route::get('/module-rule/add-question', 'Modules2Controller@addModuleQuestion');
    Route::post('/module-rule/getRule', 'Modules2Controller@getModuleRule');
    Route::get('/module-rule/getTasks', 'Modules2Controller@getModuleTasks');
    Route::get('/module-rule/add-rule', 'Modules2Controller@addModulleRule');
    Route::get('/module-rule/update-rule', 'Modules2Controller@updateModulleRule');
    Route::get('/module-rule/update-project-rule', 'Modules2Controller@updateProjectModulleRule');

    Route::get('/tasks-status', 'TasksStatusController@index');
    Route::get('/tasks-status/add', 'TasksStatusController@add');
    Route::post('/tasks-status/add', 'TasksStatusController@postAdd');
    Route::get('/tasks-status/edit/{id}', 'TasksStatusController@edit');
    Route::post('/tasks-status/edit/{id}', 'TasksStatusController@postEdit');
    Route::get('/tasks-status/del/{id}', 'TasksStatusController@del');

    Route::get('/settings', 'Settings2Controller@index');
    Route::post('/settings/save', 'Settings2Controller@save');
    Route::get('/settings/get-work-calendar', 'Settings2Controller@getWorkCalendar');

    Route::get('/clients', 'Clients2Controller@index');
    Route::get('/clients/add', 'Clients2Controller@add');
    Route::post('/clients/add', 'Clients2Controller@postAdd');
    Route::get('/clients/del/{id}', 'Clients2Controller@del');
    Route::get('/clients/edit/{id}', 'Clients2Controller@edit');
    Route::post('/clients/edit/{id}', 'Clients2Controller@postEdit');
    Route::get('/clients/get-client-by-project-id', 'Clients2Controller@getClients');

    Route::get('/citys', 'CitysController@index');
    Route::get('/citys/add', 'CitysController@add');
    Route::post('/citys/add', 'CitysController@postAdd');
    Route::get('/citys/del/{id}', 'CitysController@delete');
    Route::get('/citys/edit/{id}', 'CitysController@edit');
    Route::post('/citys/edit/{id}', 'CitysController@postEdit');

    Route::get('/calls', 'Calls2Controller@index');
    Route::get('/calls/edit/{id}', 'Calls2Controller@edit');

    Route::get('/printform-category', 'PrintForms2Controller@categoryRef');
    Route::get('/printform-category/get-category-norm', 'PrintForms2Controller@getCategoryNorm');
    Route::get('/printform-category/add', 'PrintForms2Controller@addCategoryRef');
    Route::post('/printform-category/add', 'PrintForms2Controller@postAddCategoryRef');
    Route::get('/printform-category/del/{id}', 'PrintForms2Controller@delCategoryRef');
    Route::get('/printform-category/edit/{id}', 'PrintForms2Controller@editCategoryRef');
    Route::post('/printform-category/edit/{id}', 'PrintForms2Controller@postEdittCategoryRef');
    Route::get('/print-form/add', 'PrintForms2Controller@add');
    Route::post('/print-form/add', 'PrintForms2Controller@postAdd');
    Route::get('/print-form/upload', 'PrintForms2Controller@upload');
    Route::get('/print-form/edit/{id}', 'PrintForms2Controller@edit');
    Route::post('/print-form/upload/{id}', 'PrintForms2Controller@upload2');
    Route::get('/print-form/raw/{id}', 'PrintForms2Controller@raw');

    Route::get('/print-form/del/{id}', 'PrintForms2Controller@delete');
    Route::get('/print-form/save-field', 'PrintForms2Controller@saveField');
    Route::get('/print-form/get-status', 'PrintForms2Controller@getStatus');
    Route::get('/print-form/get-category', 'PrintForms2Controller@getCategory');
    Route::get('/print-form/raw_search', 'PrintForms2Controller@searchRawForms');
    Route::get('/print-form/hot_search', 'PrintForms2Controller@searchHotForms');
    Route::get('/print-form/queue_search', 'PrintForms2Controller@searchQueueForms');
    Route::get('/print-form/hot_filter', 'PrintForms2Controller@filterHotForms');
    Route::get('/print-form/queue_filter', 'PrintForms2Controller@filterQueueForms');
    Route::get('/print-form/filter_form', 'PrintForms2Controller@filterForms');
    Route::get('/print-form/filter_form_callback', 'PrintForms2Controller@filterFormsCallback');

    Route::get('/documents', 'Documents2Controller@index');
    Route::get('/documents/add', 'Documents2Controller@add');
    Route::post('/documents/add', 'Documents2Controller@postAdd');
    Route::get('/documents/del/{id}', 'Documents2Controller@del');
    Route::get('/documents/edit/{id}', 'Documents2Controller@update');
    Route::post('/documents/edit/{id}', 'Documents2Controller@postUpdate');
    Route::get('/documents/getTypeDoc', 'Documents2Controller@getTypeDoc');

    Route::get('/doc', 'DocController@index');

    Route::get('/callback', 'CallbackSettingsController@index');
    Route::post('/callback/save-settings', 'CallbackSettingsController@seveSettings');
    Route::get('/callback/add-work-questions', 'CallbackSettingsController@addWorkQuestions');
    Route::get('/callback/add-close-questions', 'CallbackSettingsController@addCloseQuestions');
    Route::post('/callback/add-work-questions', 'CallbackSettingsController@postAddWorkQuestions');
    Route::post('/callback/add-close-questions', 'CallbackSettingsController@postAddCloseQuestions');
    Route::get('/callback/edit-work-questions/{id}', 'CallbackSettingsController@editWorkQuestions');
    Route::post('/callback/edit-work-questions/{id}', 'CallbackSettingsController@postEditWorkQuestions');
    Route::get('/callback/edit-close-questions/{id}', 'CallbackSettingsController@editCloseQuestions');
    Route::post('/callback/edit-close-questions/{id}', 'CallbackSettingsController@postEditCloseQuestions');
    Route::get('/callback/del-work-questions/{id}', 'CallbackSettingsController@delWorkQuestions');
    Route::get('/callback/del-close-questions/{id}', 'CallbackSettingsController@delCloseQuestions');
    Route::post('/callback/create-client-from-mail', 'Calls2Controller@createClientFromMail');

    Route::get('/get-notifications', 'NotificationController@getNotification');
});

Route::group(['middleware' => 'dir', 'prefix' => 'dir', 'namespace' => 'Dir'], function() {
    Route::get('/', 'DirController@index');
    Route::get('/projects', 'DirController@index');
    Route::get('/getProjects', 'ProjectsController@getProjects');
    Route::get('/project/edit/{id}', 'DirController@index');
    Route::get('/projects/get-project/{id}', 'ProjectsController@getProject');
});

Route::group(['middleware' => 'tp', 'prefix' => 'tp', 'namespace' => 'Tp'], function() {
    Route::get('/', 'TpController@index');
    Route::get('/forms', 'TpController@index');
    Route::get('/mail', 'TpController@index');
    Route::get('/getrawforms', 'PrintFormController@getRawForms');
    Route::get('/getProjectsForm', 'PrintFormController@getProjectsForm');
    Route::get('/getAttendant', 'PrintFormController@getAttendant');
    Route::get('/mail/getFolders', 'MailController@getFolders');
    Route::get('/mail/getLeters', 'MailController@getLeters');
    Route::get('/mail/getLeter/{id}', 'MailController@getLeter');
});

Route::group(['namespace' => 'Common'], function() {
    Route::get('/get-comments/{id}', 'CommentController@getCommentsByProjectId');
    Route::get('/get-all-comments/{id}', 'CommentController@getAllCommentsByProjectId');
    Route::post('/comment/add', 'CommentController@add');
    Route::post('/comment/del', 'CommentController@del');
    Route::post('/comment/save', 'CommentController@save');

    Route::get('/posts', 'PostsController@getPosts');

    Route::put('/projects/{id}', 'ProjectController@update');

    Route::delete('/projects/{p_id}/modules/{m_id}', 'ModuleController@delete');

    Route::get('/projects/{id}/contacts', 'ContactController@getAll');
    Route::post('/projects/{id}/contacts', 'ContactController@add');
    Route::delete('/projects/{p_id}/contacts/{id}', 'ContactController@del');
    Route::put('/projects/{p_id}/contacts/{id}', 'ContactController@update');

    Route::get('/projects/{id}/w-list', 'ProjectWaitingListController@getAll');
    Route::post('/projects/{id}/w-list', 'ProjectWaitingListController@add');
    Route::delete('/projects/{p_id}/w-list/{id}', 'ProjectWaitingListController@del');
    Route::put('/projects/{p_id}/w-list/{id}', 'ProjectWaitingListController@update');
    Route::put('/projects/{p_id}/w-list/{id}/solved', 'ProjectWaitingListController@solved');

    Route::get('/projects/{id}/forms', 'PrintFormController@getAll');
    Route::put('/projects/{p_id}/forms/{id}', 'PrintFormController@update');
    Route::put('/projects/{p_id}/forms/{id}/close', 'PrintFormController@close');

    Route::get('/projects/forms/statuses', 'PrintFormController@getStatuses');
//    Route::post('/projects/{id}/contacts', 'ContactController@add');
//    Route::delete('/projects/{p_id}/contacts/{id}', 'ContactController@del');

    Route::get('/projects/{id}/callback-answers', 'CallbackWorkAnswerController@getByProjectId');

    Route::get('/print_form/{id}/comments', 'PrintFormComments@get');
    Route::post('/print_form/{id}/comments', 'PrintFormComments@save');
    Route::delete('/print_form/{id}/comments/{c_id}', 'PrintFormComments@delete');
    Route::put('/print_form/{id}/comments/{c_id}', 'PrintFormComments@update');

    Route::get('/contact-posts', 'ContactPostController@get');
    Route::post('/contact-posts', 'ContactPostController@save');
    Route::put('/contact-posts/{id}', 'ContactPostController@update');
    Route::delete('/contact-posts/{id}', 'ContactPostController@delete');

    Route::get('/project-categories', 'ProjectCategoriesController@get');
    Route::post('/project-categories', 'ProjectCategoriesController@save');
    Route::put('/project-categories/{id}', 'ProjectCategoriesController@update');
    Route::delete('/project-categories/{id}', 'ProjectCategoriesController@delete');

    Route::get('/task-contacts', 'TaskContactController@index');
    Route::get('/task-contacts/{id}', 'TaskContactController@get');
    Route::post('/task-contacts', 'TaskContactController@save');
//    Route::post('/contact-posts', 'ContactPostController@save');
//    Route::put('/contact-posts/{id}', 'ContactPostController@update');
//    Route::delete('/contact-posts/{id}', 'ContactPostController@delete');

    Route::get('/table/{table}', 'Search\SearchController@get');
    Route::get('/table/config/{table}', 'Search\SearchController@config');
    Route::get('/table/excel/{table}', 'Search\SearchController@excel');

    Route::put('/events/{id}', 'EventController@save');


    Route::get('/{model}', 'ModelController@get');
    Route::post('/{model}', 'ModelController@save');
    Route::put('/{model}/{id}', 'ModelController@update');
    Route::delete('/{model}/{id}', 'ModelController@delete');

    Route::get('/redmine/issues/{id}', 'RedmineController@getIssue');
});
