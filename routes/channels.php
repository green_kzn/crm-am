<?php
use Illuminate\Support\Facades\Log;
/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('user.{toUserId}', function ($user, $toUserId) {
    return (int) $user->UserID === (int) $toUserId;
});

Broadcast::channel('private-message123', function ($user, $id) {
    return auth()->user()->id === (int)$id;
});

Broadcast::channel('private-addFormToQueue', function ($user, $id) {
    return auth()->user()->id === (int)$id;
});

Broadcast::channel('private-addFormError', function ($user, $id) {
    return auth()->user()->id === (int)$id;
});

Broadcast::channel('private-formChangeStatus', function ($user, $id) {
    return auth()->user()->id === (int)$id;
});

Broadcast::channel('private-impFormChangeStatus', function ($user, $id) {
    return auth()->user()->id === (int)$id;
});
Broadcast::channel('private-ImpCloseProject', function ($user, $id) {
    return auth()->user()->id === (int)$id;
});

Broadcast::channel('survey.{survey_id}', function ($user, $survey_id) {
    return [
        'id' => $user->id,
        'image' => $user->image(),
        'full_name' => $user->full_name
    ];
});
