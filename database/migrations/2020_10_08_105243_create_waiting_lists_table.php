<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaitingListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waiting_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('waiting_list_type_id')->unsigned();
            $table->boolean('has_redmine')->default(false);
            $table->boolean('has_info')->default(false);
//            $table->string('info')->nullable();
//            $table->boolean('is_solved')->default(false);;
//            $table->string('redmine_task_id')->nullable();
//            $table->timestamp('solved_at');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('waiting_list_type_id')->references('id')->on('waiting_list_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waiting_lists');
    }
}
