<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectWaitingListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_waiting_list', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->integer('waiting_list_id');
            $table->string('info')->nullable();
            $table->boolean('is_solved')->default(false);;
            $table->string('redmine_task_id')->nullable();
            $table->timestamp('solved_at')->nullable();
            $table->timestamps();

//            $table->foreign('project_id')->references('id')->on('projects');
//            $table->foreign('waiting_list_id')->references('id')->on('waiting_lists');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_waiting_list');
    }
}
